package com.yscoco.dianli.dto.company;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;
import java.util.Set;

import lombok.Getter;
import lombok.Setter;

import com.yscoco.dianli.dto.BaseDto;
import com.yscoco.dianli.dto.member.MemberInfoDto;

@ApiModel("支线信息")
@Setter
@Getter
public class LinesOnBranchDto extends BaseDto{

	private static final long serialVersionUID = 284932413414055417L;
	
	@ApiModelProperty("传感器动作次数")
	private String sensorNumber;
	
	@ApiModelProperty("支线名称")
	private String branchName;
	
	@ApiModelProperty("支线地址")
	private String branchAddress;
	
	@ApiModelProperty("支线类型")
	private String branchType;
	
	@ApiModelProperty("经纬度")
	private String longitudeAndlatitude;
	
	@ApiModelProperty("柜号")
	private String cabin_no;
	
	@ApiModelProperty("管理人员")
	private MemberInfoDto memberInfoDto;
	
	@ApiModelProperty("所属母线")
	private LinesOnMasterDto linesOnMasterDto;
	
	@ApiModelProperty("支线下的监测点")
	private List<MonitorPointDto> monitorPointDtos ;
	
	@ApiModelProperty("支线所属监测站")
	private MonitorStationDto monitorStationDto ;
	
}
