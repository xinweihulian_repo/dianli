package com.yscoco.dianli.dto.company;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import com.yscoco.dianli.dto.BaseDto;

@ApiModel("故障处理信息提交")
@Setter
@Getter
public class FaultGroundInfoDto extends BaseDto{

	private static final long serialVersionUID = 284932413414055417L;
	
	@ApiModelProperty("母线的id")
	private String deviceId;
	
	@ApiModelProperty("故障信息自增Id")
	private String autoId;
	
	@ApiModelProperty("记录填写")
	private String recordMessage;
	
	@ApiModelProperty("故障类型；1:保护器异常，2:过电压故障，3：接地故障")
	private String type;
	
	@ApiModelProperty("故障发生的时间戳")
	private String timestamp;
	
	@ApiModelProperty("发生故障的相位；1：A相； 2：B相；4：C相")
	private String valueFlag;
	
	@ApiModelProperty("处理人")
	private String createByName;
	
}
