package com.yscoco.dianli.dto.company;


import com.yscoco.dianli.dto.BaseDto;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class MonitorStationDianJiDto extends BaseDto{
	
	
	private static final long serialVersionUID = 6561842066638345901L;

	private String stationId;
	
	private String deviceId ;
	
	private String deviceName ;
	
	private String deviceType ;
	
	private String position;
	
	private String stationType ;

	private String devid;
	private float elec_a;
	private float elec_b;
	private float elec_c;
	private float elec_z;
	private float vol_a;
	private float vol_b;
	private float vol_c;
	private float temp_a;
	private float temp_b;
	private float temp_c;
	private float temp_n;
	private byte status;
	private short type;
	private byte power_a;
	private byte power_b;
	private byte power_c;
	private float temp_machine;
	private float gravity_x;
	private float gravity_y;
	private float gravity_z;
	private long timestamp;
	

	

}
