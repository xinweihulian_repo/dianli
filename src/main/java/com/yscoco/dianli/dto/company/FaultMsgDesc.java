package com.yscoco.dianli.dto.company;

import java.io.Serializable;

import com.yscoco.dianli.service.comon.Transition;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class FaultMsgDesc implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer autoId ;
	@ApiModelProperty("监测站名")
	private String monitorStationName;
	@ApiModelProperty("母线设备唯一标识")
	private String devid;
	@ApiModelProperty("母线名称")
	private String masterName;
	@ApiModelProperty("所在地区")
	private String masterAddress;
	@ApiModelProperty("相位 ：1 A相 2：B相  3：C相  ")
	private Integer phase; 
	@ApiModelProperty("故障发生的时间戳")
	private long timestamp;
	@ApiModelProperty("故障发生的具体时间")
	private String times;
	@ApiModelProperty("故障类型 :接地故障:10,系统过电压:11 ,过电压:12,系统断线:13,欠压数据包:14,系统短路15,弧光接地故障16,20,21,22,23")
	private int faultType;
	@ApiModelProperty("报警级别 :中级[11,23],高级[10,12,13,14,15,16,20,21,22]")
	private String alarmLeve;
	@ApiModelProperty("故障解除时间戳")
	private long distimestamp;
	@ApiModelProperty("故障解除的具体时间")
	private String distimes;
	@ApiModelProperty("调用Transition.changFaultTypeByTypeId将faultType解析成可读的具体故障")
	private String fault;
	private String branchName;
	/**
	 * 保护器类型
	 */
	private String protectorType;
	/**
	 * 温度保护器具体子类型
	 */
	private String type;
	/**
	 * 设备名称
	 */
	private String deviceName;
	/**
	 * 故障是否已经解除
	 * true 已解除   false 未解除  
	 */
	private Boolean isSolve;
	private String imageUrl="";
     
}
