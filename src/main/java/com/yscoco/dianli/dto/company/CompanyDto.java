package com.yscoco.dianli.dto.company;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;
import java.util.Set;

import lombok.Getter;
import lombok.Setter;

import com.yscoco.dianli.constants.YN;
import com.yscoco.dianli.dto.BaseDto;

@ApiModel("公司信息")
@Setter
@Getter
public class CompanyDto extends BaseDto{

	private static final long serialVersionUID = 284932413414055417L;
	
	@ApiModelProperty("公司名称")
	private String companyName;
	
	@ApiModelProperty("公司简称")
	private String companyNameShort;
	
	@ApiModelProperty("公司地址")
	private String companyAddress;
	
	@ApiModelProperty("公司所属行业")
	private String industry;
	
	@ApiModelProperty("公司性质")
	private String businessType;
	
	@ApiModelProperty("公司规模")
	private String companySize;
	
	@ApiModelProperty("主要产品以及产能")
	private String mainProducts;
	
	@ApiModelProperty("监测站")
	private List<MonitorStationDto> monitorStationDtos ;
	
	
	
	// 冗余数据 作为统计用的
	@ApiModelProperty("监测点总数")
	private int monitorPointCount=0 ;
	
	@ApiModelProperty("监测站总数")
	private int monitorStationCount=0 ;
	
	@ApiModelProperty("母线加上支线的总数")
	private int linesOnMasterAndBranchCount=0 ;
		
	@ApiModelProperty("温度采集器数量")
	private int tempPointCount=0 ;
	
	@ApiModelProperty("过电压保护器数量")
	private int elecCount=0 ;
	
	@ApiModelProperty("极地电压保护器数量")
	private int polarElecCount=0 ;
	
	
	// 故障类型消息
	@ApiModelProperty("接地故障消息数量")
	private int jiediProblemCount=0 ;
	
	@ApiModelProperty("温度故障消息数量")
	private int tempProblemCount=0 ;
	
	@ApiModelProperty("过电压消息数量")
	private int elecProblemCount=0 ;
	
	@ApiModelProperty("故障总数量")
	private int problemCount=0 ;
	
	@ApiModelProperty("是否存在故障的标志位:Y表示存在,N表示不存在")
	private YN  flagProblem=YN.N ;
 
	
}
