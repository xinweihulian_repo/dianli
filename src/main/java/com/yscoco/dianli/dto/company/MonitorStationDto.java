package com.yscoco.dianli.dto.company;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;
import java.util.Set;

import javax.persistence.Column;

import lombok.Getter;
import lombok.Setter;

import com.yscoco.dianli.dto.BaseDto;
import com.yscoco.dianli.entity.company.Company;

@ApiModel("公司监测站信息")
@Setter
@Getter
public class MonitorStationDto extends BaseDto{

	private static final long serialVersionUID = 284932413414055417L;
	
	@ApiModelProperty("监测站名称")
	private String stationName;
	
	@ApiModelProperty("所在地区")
	private String stationAddress;
	
	@ApiModelProperty("所在地区经纬度")
	private String longitudeAndlatitude;
	
	@ApiModelProperty("所属公司")
	private CompanyDto companyDto;
	
	private String city ;
	
	private String cityCode ;
	
	private String stationType;
	
	//private Company company;
	

	
	
//	@ApiModelProperty("母线条数")
//	private Set<LinesOnMasterDto> linesOnMasterDtos;
	@ApiModelProperty("母线条数")
	private List<LinesOnMasterDto> linesOnMasterDtos;
	
	@ApiModelProperty("母线排序")
	private List<LinesOnMasterDto> linesOnMasterSortList ;
	
	// 各种线路的种类 setMonitorStationDtos

	private int linesOnMasterCount = 0;//母线条数
	
	private int linesOnBranchCount = 0;//支线条数
	
	private int monitorPointCount = 0;//监测点数
	
	private int totalProblem = 0;//故障总数
	
	private int totalLines = 0;//线路总数
	
 
}
