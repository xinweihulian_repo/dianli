package com.yscoco.dianli.dto.company;



import com.yscoco.dianli.dto.BaseDto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class MonitorStationDeviceDto  extends BaseDto{
	
	
	private static final long serialVersionUID = 6561842066638345901L;

	private String stationId;
	
	private String deviceId ;
	
	private String deviceName ;
	
	private String deviceType ;
	
	private String position;
	
	private String stationType ;
	
 	private float temper; 
	

}
