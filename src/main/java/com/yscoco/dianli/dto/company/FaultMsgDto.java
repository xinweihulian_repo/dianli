package com.yscoco.dianli.dto.company;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import com.yscoco.dianli.dto.BaseDto;

@ApiModel("母线")
@Setter
@Getter
public class FaultMsgDto extends BaseDto{

	private static final long serialVersionUID = 284932413414055417L;
	
	@ApiModelProperty("故障信息自增Id")
	private int autoId;
	
	@ApiModelProperty("母线的id")
	private String devid;
	
	@ApiModelProperty("母线名称")
	private String masterName;
	
	@ApiModelProperty("所在地区")
	private String masterAddress;
	
	@ApiModelProperty("故障类型；1:保护器异常，2:过电压故障，3：接地故障")
	private int type;
	
	@ApiModelProperty("故障发生的时间戳")
	private long timestamp;
	
	@ApiModelProperty("故障发生的具体时间")
	private String times;
	
	@ApiModelProperty("发生故障的相位；1：A相； 2：B相；4：C相")
	private int valueFlag;
	
}
