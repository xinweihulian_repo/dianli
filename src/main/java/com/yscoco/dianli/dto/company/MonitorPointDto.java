package com.yscoco.dianli.dto.company;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

import com.yscoco.dianli.dto.BaseDto;

@ApiModel("公司监测点")
@Setter
@Getter
public class MonitorPointDto extends BaseDto{

	private static final long serialVersionUID = 284932413414055417L;
	
	@ApiModelProperty("监测点名称")
	private String pointName;
	
	@ApiModelProperty("所在地区")
	private String pointAddress;
	
	@ApiModelProperty("经纬度")
	private String longitudeAndlatitude;
	
	@ApiModelProperty("监测设备标识")
	private String monitorFlag;
	
	@ApiModelProperty("位置")
	private String position;
	
	@ApiModelProperty("位置名称")
	private String positionName;
	
	@ApiModelProperty("所属分类")
	private String hitchType; 
	
	@ApiModelProperty("所属类型")
	private String type;
	@ApiModelProperty("是否在线")
	private Boolean online=false;
	@ApiModelProperty("监测点上面的支线")
	private LinesOnBranchDto linesOnBranchDto;
	@ApiModelProperty("保护器数据")
	private List<com.batise.device.voltage.Voltage_data> Voltage_data;
	@ApiModelProperty("温度采集器数据")
	private  List<com.batise.device.temper.Temper_data> Temper_data;
}
