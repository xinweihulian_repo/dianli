package com.yscoco.dianli.dto.company;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;
import java.util.Set;

import lombok.Getter;
import lombok.Setter;

import com.yscoco.dianli.dto.BaseDto;

@ApiModel("母线")
@Setter
@Getter
public class LinesOnMasterDto extends BaseDto{

	private static final long serialVersionUID = 284932413414055417L;
	
	@ApiModelProperty("母线名称")
	private String masterName;
	
	@ApiModelProperty("所在地区")
	private String masterAddress;
	
	@ApiModelProperty("上一段母线")
	private String preMasterId;
	
	@ApiModelProperty("下一段母线")
	private String nextMasterId;
	
	@ApiModelProperty("上一段母线名称")
	private String preMasterName;
	
	@ApiModelProperty("监测设备标识")
	private String monitorFlag;
	
	@ApiModelProperty("经纬度")
	private String longitudeAndlatitude ;
	
	@ApiModelProperty("关联方式")
	private String relatedType;
	
	@ApiModelProperty("所属分类")
	private String hitchType;
	
	@ApiModelProperty("所属监测站")
	private MonitorStationDto monitorStationDto;
	
	@ApiModelProperty("支线")
	private List<LinesOnBranchDto> linesOnBranchDtos ;

	@ApiModelProperty("支线数量")
	private String linesOnBranchCount ; 
	@ApiModelProperty("是否有故障")
	private String isProblem="N" ; 
	
	@ApiModelProperty("是否在线")
	private Boolean isOnline=false ; 
	
}
