package com.yscoco.dianli.dto.company;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Set;

import lombok.Getter;
import lombok.Setter;

import com.yscoco.dianli.dto.BaseDto;

@ApiModel("公司信息")
@Setter
@Getter
public class ItemFaultDataDto extends BaseDto{

	private static final long serialVersionUID = 284932413414055417L;
	
	@ApiModelProperty("公司名称")
	private String Wave_UA;
	
	@ApiModelProperty("公司简称")
	private String companyNameShort;
	
	@ApiModelProperty("公司地址")
	private String companyAddress;
	
	@ApiModelProperty("公司所属行业")
	private String industry;
	
	@ApiModelProperty("公司性质")
	private String businessType;
	
	@ApiModelProperty("公司规模")
	private String companySize;
	
	@ApiModelProperty("主要产品以及产能")
	private String mainProducts;
	
	@ApiModelProperty("监测站")
	private Set<MonitorStationDto> monitorStationDtos ;
}
