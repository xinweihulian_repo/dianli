package com.yscoco.dianli.dto.option;

import lombok.Getter;
import lombok.Setter;

import com.yscoco.dianli.dto.BaseDto;

@Setter
@Getter
public class QuestionsDto extends BaseDto{

	private static final long serialVersionUID = -2042874745820095365L;
	private String title;
	
	private String digest;//摘要内容
	
	private String type;//1:连接不上,2:常见问题,3:健康咨询,4其他,5链接帮助
	
	private String titleImg;
	
	private String html;
	 
	
}
