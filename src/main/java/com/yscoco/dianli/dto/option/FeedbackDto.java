package com.yscoco.dianli.dto.option;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import com.yscoco.dianli.dto.BaseDto;

@Setter
@Getter
public class FeedbackDto extends BaseDto{

	private static final long serialVersionUID = 6943668866255221004L;
	
	@ApiModelProperty("反馈内容")
	private String content;
	
	@ApiModelProperty("反馈账号")
	private String mobileOremail;
	
	@ApiModelProperty("回复内容")
	private String reply;
	
	@ApiModelProperty("回复账号")
	private String replyAccount;
	
}
