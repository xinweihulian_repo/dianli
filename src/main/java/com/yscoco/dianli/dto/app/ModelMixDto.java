package com.yscoco.dianli.dto.app;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 母线信息 AS （ 所有设备信息）
 * @author liuDong.
 *
 */
@Getter
@Setter
@ToString
public class ModelMixDto {
	 private String id;
	 /**
	  * 设备标识
	  */
	 private String monitorFlag;
	 /**
	  * 名称
	  */
	 private String name;
	 /**
	  * 所属监测站id
	  */
	 private String stationId;
	 /**
	  * 所属监测站名称
	  */
	 private String stationName;
	  /**
	   * 公司id
	   */
	 private String companyId;
	 /**
	   * 公司名称
	   */
	 private String companyName;
	 
	 /**
	  * 监测站地址
	  */
	private String stationAddress;

	/**
     * 监测站类型(1.800,2.耐张线夹测温,3机电健康管理4充电柜)
	 */
	private String stationType;

    /**
     * 监测设备分类 （温度传感器，过电压保护器,电机）
      */
    private String hitchType;
	 
	 
}
