package com.yscoco.dianli.dto.app;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import com.yscoco.dianli.dto.BaseDto;

@ApiModel("app版本信息")
@Setter
@Getter
public class AppVersionDto extends BaseDto{

	private static final long serialVersionUID = 284932413414055417L;
	
	@ApiModelProperty("版本号")
	private String version;
	@ApiModelProperty("版本名称")
	private String versionName;
	@ApiModelProperty("Y/N 是否必须更新")
	private String isMust;
	@ApiModelProperty("链接地址")
	private String url;
	@ApiModelProperty("版本更新内容")
	private String remarks;
	/*@ApiModelProperty("应用标识")
	private String identify;*/
	
}
