package com.yscoco.dianli.dto.app;



import com.yscoco.dianli.dto.BaseDto;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
@ApiModel("app监测点列表信息数据模型")
@Getter
@Setter
public class AppMontorPointDetailInfoDto extends BaseDto {
	private static final long serialVersionUID = 1L;
	private String monitorPointName;
	private String monitorStationName;
	private String masterLineName;
	private String branceLineName;
	private String mointorFlag;
	private String hitchType;
	private String position;
	private String type;
}
