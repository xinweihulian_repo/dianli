package com.yscoco.dianli.dto.web;

import java.io.Serializable;

import lombok.Data;
/**
 * 首页左侧报警统计数据模型
 * @author liuDong.
 *
 */
@Data
public class IndexAlarmDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/*
	 *金属接地故障
	 */
	private Integer jsjd=0; 
	/**
	 * 系统过电压（属于中级报警）
	 */
	private Integer xtgdy=0;
	/**
	 * 过电压故障
	 */
	private Integer gdy=0;
	/**
	 * PT断线故障
	 */
	private Integer ptdx=0;
	/**
	 * 欠压数据包
	 */
	private Integer qysjb=0;
	/**
	 * 系统短路
	 */
	private Integer xtdl=0;
	/**
	 * 弧光接地故障
	 */
	private Integer hgjd=0;
	/**
	 * 过热
	 */
	private Integer gr=0;
	/**
	 * 湿度超标
	 */
	private Integer sdcb=0;
	/**
	 * 泄漏
	 */
	private Integer xl=0;
	/**
	 * 电量不足  （属于中级报警）
	 */
	private Integer dlbz=0;
	/**
	 * 红外线测温故障 （属于中级报警）
	 */
	private Integer hwx=0;
	
	
	/**
	 * 中级报警总数
	 */
	private Integer zhongJiCount=0;
	/**
	 * 高级报警总数
	 */
	private Integer gaoJiCount=0;

}
