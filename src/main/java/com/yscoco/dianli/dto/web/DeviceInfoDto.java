package com.yscoco.dianli.dto.web;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
/**
 * 智能网关数据传输对象
 * @author liuDong.
 *
 */
@Data
public class DeviceInfoDto implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	 private String id;
	@ApiModelProperty("监测站ID")
	private String stationId;
	@ApiModelProperty("监测站名")
	private String stationName;
	@ApiModelProperty("母线设备唯一标识")
	private String monitorFlag;
	@ApiModelProperty("设备分类（智能网关或者监测设备）")
	private String devType;
	@ApiModelProperty("母线名称")
	private String masterName;
	@ApiModelProperty("是否在线")
	private Boolean online;
	@ApiModelProperty("上线时间")
	private String onlineTime="";
	@ApiModelProperty("离线时间")
	private String offlineTime="";
	/**
	   * 公司id
	   */
	 private String companyId;
	 /**
	   * 公司名称
	   */
	 private String companyName;
	

}
