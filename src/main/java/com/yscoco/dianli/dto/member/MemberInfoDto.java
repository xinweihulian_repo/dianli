package com.yscoco.dianli.dto.member;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Set;

import lombok.Getter;
import lombok.Setter;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.yscoco.dianli.common.utils.JsonStringSerialize;
import com.yscoco.dianli.dto.BaseDto;
import com.yscoco.dianli.dto.company.MonitorStationDto;

@ApiModel("用户信息")
@Setter
@Getter
public class MemberInfoDto extends BaseDto{

	private static final long serialVersionUID = 284932413414055417L;
	
	@ApiModelProperty("用户名")
	private String userName;
	
	@ApiModelProperty("账号")
	private String account;
	
	@ApiModelProperty("手机号码")
	private String mobile;
	
	@ApiModelProperty("邮箱")
	private String email;

	@ApiModelProperty("密码")
	private String password;
	
	@ApiModelProperty("旧密码")
	private String oldPassword;
	
	@ApiModelProperty("性别 男/女")
	private String sex;
	
	@ApiModelProperty("登录后反馈的唯一值")
	private String token;//
	
	@ApiModelProperty("第三方id")
	private String openId;//
	
	@ApiModelProperty("pushId")
	private String pushId;
	
	@ApiModelProperty("用户其他属性")
	@JsonSerialize(using=JsonStringSerialize.class)
	private String setting;
	
	@ApiModelProperty("companyId即公司Id")
	private String companyId;
	
	@ApiModelProperty("支线id")
	private String linesOnBranchId;// 支线id,逗号隔开
	
	@ApiModelProperty("支线名称")
	private String linesOnBranchName;// 支线名称
	
	@ApiModelProperty("创建人姓名")
    private String createByAccount;// 创建人信息
   
	@ApiModelProperty("创建人电话")
    private String createByMobile;
	
	@ApiModelProperty("监测站名称")
	private String monitorStationName;// 支线名称
	
	@ApiModelProperty("监测站Ids")
	private String monitorStationId;
	
	@ApiModelProperty("监测站")
	private Set<MonitorStationDto> monitorStationDtos ;
	
}
