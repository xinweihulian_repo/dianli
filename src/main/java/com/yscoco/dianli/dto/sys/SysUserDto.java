package com.yscoco.dianli.dto.sys;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import com.yscoco.dianli.dto.BaseDto;

@Setter
@Getter
@ToString
public class SysUserDto extends BaseDto{
	private static final long serialVersionUID = -6002487819294889609L;

	@ApiModelProperty("token唯一标识")
	private String token;
	
	@ApiModelProperty("联系电话")
	private String mobilePhone;
	
	@ApiModelProperty("位置")
	private String position;
	
	@ApiModelProperty("账户")
	private String account;
	
	@ApiModelProperty("密码")
	private String password;
	
	@ApiModelProperty("是否是超管")
	private	Boolean isAdmin;//是否是超管
	
	@ApiModelProperty("权限")
    private String permission;
	
	 // 创建人信息
	@ApiModelProperty("创建人姓名")
    private String createByAccount;
    
	@ApiModelProperty("创建人电话")
    private String createByMobile;
	
	
	
    
}