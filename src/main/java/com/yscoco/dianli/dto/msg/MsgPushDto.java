package com.yscoco.dianli.dto.msg;

import lombok.Getter;
import lombok.Setter;
import io.swagger.annotations.ApiParam;

import com.yscoco.dianli.constants.MsgType;
import com.yscoco.dianli.constants.YN;
import com.yscoco.dianli.dto.BaseDto;

@Setter
@Getter
public class MsgPushDto extends BaseDto{

    private static final long serialVersionUID = -2773383059983028698L;

    @ApiParam("推送的内容")
    private String content;
    
    @ApiParam("推送跳转地址，预留")
    private String jumpUrl;
    
    @ApiParam("推送的图片，如用户头像等")
    private String img;
    
    @ApiParam("推送的标题")
    private String title;
    
    @ApiParam("关联的id，如圈子的id")
    private String relId;//
    
    @ApiParam(" CIRCLE_LIKE,//圈子点赞  CIRCLE_COMMENT,//圈子评论 SYSTEM,//系统消息 FRIEND_ADD//添加好友申请")
    private MsgType msgType;
    
    @ApiParam("是否已读，N,Y")
    private YN isRead = YN.N;
    
    @ApiParam("推送消息的来源 id，如触发推送的用户id")
    private String fromId;
    
}
