package com.yscoco.dianli.dto;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonFormat;

@Setter
@Getter
public class BaseDto implements Serializable {

	private static final long serialVersionUID = 5103083567079158561L;
	
	
	@ApiModelProperty("唯一标识Id")
	private String id;
	
	@ApiModelProperty("创建时间")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	private Date createTime;

	@ApiModelProperty("创建人")
	private String createBy;
}
