package com.yscoco.dianli.controller.wx;

import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.yscoco.dianli.common.utils.SpringUtils;
import com.yscoco.dianli.other.cache.RedisCacheUtil;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @Author: onion..
 * @Description: TODO
 * @Date: 2020/9/24 17:38
 */
@Component
public class WxApi {

    @Resource
    private RedisCacheUtil redisCache;

    /**
     *获取小程序全局唯一后台接口调用凭据（access_token）
     * access_token 的有效期目前为 2 个小时，需定时刷新，重复获取将导致上次获取的 access_token 失效；
     * auth.getAccessToken
     * GET https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET
     */
    public   String  getAccessToken(){
      //  RedisCacheUtil bean = SpringUtils.getBean(RedisCacheUtil.class);
        String res = redisCache.get(WXAPIConts.ACCESS_TOKEN_CACHKEY);
        if(null==res){
            HashMap<String, Object> paramMap = new HashMap<>();
            
                paramMap.put("grant_type", "client_credential");
                paramMap.put("appid", WXAPIConts.AppID);
                paramMap.put("secret",WXAPIConts.AppSecret);
            
            res = HttpUtil.get(WXAPIConts.AUTH_GETACCESS_TOKEN_URL, paramMap);

            System.out.println("getAccessToken:"+ JSON.toJSON(res));

            JSONObject parse = JSONObject.parseObject(res);
            Map<String,Object> map = (Map<String,Object>)parse;
            Object access_token = map.get("access_token");
            
            if(null!=access_token){
                redisCache.set(WXAPIConts.ACCESS_TOKEN_CACHKEY,access_token.toString(),100, TimeUnit.MINUTES);
            }
            res=access_token.toString();
        }

        return  res;
    }
}
