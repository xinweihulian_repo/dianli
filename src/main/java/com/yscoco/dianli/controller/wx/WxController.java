package com.yscoco.dianli.controller.wx;

import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.hyc.smart.log.ExLogUtil;
import com.yscoco.dianli.common.base.BaseController;
import com.yscoco.dianli.common.vo.Message;
import com.yscoco.dianli.entity.member.MemberInfo;
import com.yscoco.dianli.other.cache.RedisCacheUtil;
import com.yscoco.dianli.service.member.MemberService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @Author: onion..
 * @Description: TODO
 * @Date: 2020/9/23 17:33
 */
@Controller
@RequestMapping("/wx/")
public class WxController extends BaseController{

    @Autowired
    private MemberService memberService;
    @Resource
    private RedisCacheUtil redisCache;
    @Autowired
    private WxApi wxApi;
    @Autowired
    private  TestSubscribe testSubscribe;



    /**
     *  获取openid
     * @param js_code
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "auth.code2Session")
    public Message<?> code2Session(String js_code,String phoneNum){
        //MemberInfo memberInfo= this.getMemberInfoBytoken();
         MemberInfo memberInfo = memberService.getByMobile(phoneNum);

        HashMap<String, Object> paramMap = new HashMap<>();
            paramMap.put("grant_type", "authorization_code");
            paramMap.put("appid", WXAPIConts.AppID);
            paramMap.put("secret",WXAPIConts.AppSecret);
            paramMap.put("js_code", js_code);

        String result = HttpUtil.get(WXAPIConts.AUTH_CODE2SESSION_URL, paramMap);

        JSONObject parse = JSONObject.parseObject(result);

        Map<String,Object> map = (Map<String,Object>)parse;
            Object openid = map.get("openid");
            if(null!=openid){
                memberInfo.setOpenId(openid.toString());
                memberService.edit(memberInfo);
                System.out.println("openid:"+openid.toString());
            }
        return  Message.success(result);
    }

    /**
     *获取小程序全局唯一后台接口调用凭据（access_token）
     * access_token 的有效期目前为 2 个小时，需定时刷新，重复获取将导致上次获取的 access_token 失效；
     * auth.getAccessToken
     * GET https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET
     */
    public  String  getAccessToken(){
         String res = redisCache.get(WXAPIConts.ACCESS_TOKEN_CACHKEY);
         if(null==res){
             HashMap<String, Object> paramMap = new HashMap<>();
             paramMap.put("grant_type", "client_credential");
             paramMap.put("appid", WXAPIConts.AppID);
             paramMap.put("secret",WXAPIConts.AppSecret);
             res = HttpUtil.get(WXAPIConts.AUTH_GETACCESS_TOKEN_URL, paramMap);

             JSONObject parse = JSONObject.parseObject(res);
                Map<String,Object> map = (Map<String,Object>)parse;
                Object errcode = map.get("errcode");
                Object access_token = map.get("access_token");
                if(errcode.equals("0")){
                    redisCache.set(WXAPIConts.ACCESS_TOKEN_CACHKEY,access_token.toString(),100, TimeUnit.MINUTES);
                }
         }
        System.out.println("getAccessToken:"+JSON.toJSON(res));
        return  res;
    }

    @ResponseBody
    @RequestMapping(value = "auth.test")
    public Message<?> test(){

         String res = testSubscribe.sends("o5cCg4paiDGWlCdyuovEee7fWzA0");
//         String accessToken = wxApi.getAccessToken();
//
//        String url = WXAPIConts.SEND_URL.replace("ACCESS_TOKEN", accessToken);
//
//        SubscribeDataBean bean = new SubscribeDataBean();
//        bean.setDate1(new SubscribeDataBean.Date1("2019-10-24 16:00:59"));
//        bean.setThing4(new SubscribeDataBean.Thing4("未知"));
//        bean.setThing5(new SubscribeDataBean.Thing5("高级报警"));
//        bean.setThing6(new SubscribeDataBean.Thing6("未知"));
//        bean.setNumber2(new SubscribeDataBean.Number2("123"));
//
//        //组装接口所需对象
//        SubscribeBean sendBean = new SubscribeBean();
//        sendBean.setData(bean);//这里的订阅消息对象 不需要额外转json
//
//        sendBean.setTemplate_id(WXAPIConts.TEMPLATE_ID);
//        sendBean.setPage("pages/police/police");
//        //sendBean.setMiniprogram_state("developer");
//
//
//            sendBean.setTouser("o5cCg4pqx_xKPqV0vU--L5ZwSXhQ");
//
//
//            String param = JSON.toJSONString(sendBean);
//
//            String result = HttpUtil.post(url, param);


        return Message.success(res);
    }


}

