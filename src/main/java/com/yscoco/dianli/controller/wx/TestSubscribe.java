package com.yscoco.dianli.controller.wx;

import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSON;
import com.hyc.smart.log.ExLogUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Author: onion..
 * @Description: TODO
 * @Date: 2020/9/23 13:32
 */

@Component
public class TestSubscribe  {

    @Autowired
    private WxApi wxApi;



    public  String sends(String openId){
        if(openId.isEmpty())
            openId="o5cCg4paiDGWlCdyuovEee7fWzA0";
        

         String accessToken = wxApi.getAccessToken();

        String url = WXAPIConts.SEND_URL.replace("ACCESS_TOKEN", accessToken);
        //模板ID
        String template_id = WXAPIConts.TEMPLATE_ID;

        SubscribeDataBean bean = new SubscribeDataBean();
        bean.setDate1(new SubscribeDataBean.Date1("2019-10-24"));
        bean.setThing4(new SubscribeDataBean.Thing4("办公室708"));
        bean.setThing5(new SubscribeDataBean.Thing5("温度告警-高于40C"));
        bean.setThing6(new SubscribeDataBean.Thing6("液相质谱仪"));
        bean.setNumber2(new SubscribeDataBean.Number2("456456"));

        //组装接口所需对象
        SubscribeBean sendBean = new SubscribeBean();
        sendBean.setData(bean);//这里的订阅消息对象 不需要额外转json

        sendBean.setTouser(openId);
        sendBean.setTemplate_id(template_id);
        sendBean.setPage("pages/police/police");
        sendBean.setMiniprogram_state("developer");


        //对象转json
        String param = JSON.toJSONString(sendBean);

        String result = HttpUtil.post(url, param);

        ExLogUtil.getLogger().info("wx订阅消息:"+result);

        return  result;

    }


//    public static void main(String[] args) {
//        String accessToken =  WxApi.getAccessToken();
////        WxController wx = new WxController();
////         String accessToken = wx.getAccessToken();
//        JSONObject parse = JSONObject.parseObject(accessToken);
//
//        Map<String,Object> map = (Map<String,Object>)parse;
//
//         Object access_token = map.get("access_token");
//
//      //  send(access_token.toString());
//        //访问过小程序的openid
//        String OPENID = "o5cCg4kq9pdWevyjIUO1Z7SR59oQ";
//
       // sends(access_token.toString(),OPENID);
//
//    }


}
