package com.yscoco.dianli.controller.wx;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @Author: onion..
 * @Description: TODO
 * @Date: 2020/9/23 16:59
 *       日期
 *       {{date1.DATA}}
 *      设备编号
 *      {{number2.DATA}}
 *       安装位置
 *       {{thing4.DATA}}
 *       告警内容
 *       {{thing5.DATA}}
 *      设备名称
 *       {{thing6.DATA}}
 */
@Data
public class SubscribeDataBean {
    //具体的订阅消息的key {{thing4.DATA}} 则key为thing4
    private Thing4 thing4;
    private Thing5 thing5;
    private Thing6 thing6;

    private Date1 date1;
    private Number2 number2;


    @Data
    @AllArgsConstructor
    public static class Thing4{
        private String value;
    }

    @Data
    @AllArgsConstructor
    public static class Thing5{
        private String value;
    }

    @Data
    @AllArgsConstructor
    public static class Thing6{
        private String value;
    }
    @Data
    @AllArgsConstructor
    public static class Date1{
        private String value;
    }

    @Data
    @AllArgsConstructor
    public static class Number2{
        private String value;
    }



}
