package com.yscoco.dianli.controller.api;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.alibaba.fastjson.JSON;
import com.batise.device.eric900.eric900_data;
import com.batise.device.fault.fault_data_new;
import com.batise.device.motor.Motor_data;
import com.hyc.smart.XWDataEntry;
import com.hyc.smart.eric900.eric900_config;
import com.hyc.smart.motor.MotorConfig;
import com.yscoco.dianli.common.aspect.SystemLog;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yscoco.dianli.common.base.BaseController;
import com.yscoco.dianli.common.exception.BizException;
import com.yscoco.dianli.common.utils.BeanUtils;
import com.yscoco.dianli.common.vo.Code;
import com.yscoco.dianli.common.vo.Message;
import com.yscoco.dianli.common.vo.PageData;
import com.yscoco.dianli.entity.company.DianJi;
import com.yscoco.dianli.pojo.DianJiModel;
import com.yscoco.dianli.pojo.MemberModel;
import com.yscoco.dianli.pojo.PointModel;
import com.yscoco.dianli.service.company.DianJiService;
import com.yscoco.dianli.service.company.MonitorPointService;
import com.yscoco.dianli.service.member.MemberService;

/**
 * @author onion
 * @date 2020/08/21
 */
@Controller
@RequestMapping("/sys/dianji/")
public class ApiDianJiControl extends BaseController{
	
	@Autowired
	private MonitorPointService monitorPointService;
	@Autowired
	private DianJiService dianJiService;
	@Autowired
	private MemberService memberService;






	/**
	 * 同步电机传感器的报警设置
	 * @param data
	 */
	@ResponseBody
	@RequestMapping("updateMotorSet.v1")
	@SystemLog(module="/sys/dianji/updateMotorSet",methods = "同步电机传感器的报警设置")
	public Message<?> updateMotorSet(MotorConfig data){
		 XWDataEntry.updateMotorSet(data);
		return Message.success();
	}

	/**
	 * 获取电机传感器的报警设置
	 * @param sensorId
	 * @return
	 */
	@ResponseBody
	@RequestMapping("getMotorSet.v1")
	@SystemLog(module="/sys/dianji/getMotorSet",methods = "获取电机传感器的报警设置")
	public Message<MotorConfig> getMotorSet(String sensorId){
		MotorConfig motorSet = XWDataEntry.getMotorSet(sensorId);
		
		if(null==motorSet){motorSet = new MotorConfig();}
		
		return Message.success(motorSet);
	}





	@ResponseBody
	@RequestMapping("dianJiInfo.v1")
	public Message<DianJi> getDianJiInfoByDeviceNo(String deviceNo){
		if(StringUtils.isEmpty(deviceNo))
			throw new BizException(Code.NOT_PARAM);
		
		DianJi dianJi = dianJiService.getByDeviceNo(deviceNo);
			if(dianJi==null){
			//	MonitorPoint byPointFlag = monitorPointService.getByPointFlag(deviceNo);
				dianJi = new DianJi();
				dianJi.setCreateTime(new Date());
				dianJi.setDeviceNo(deviceNo);
				//dianJi.setCreateBy(byPointFlag.getCreateBy);
				dianJi = dianJiService.addOrUpdate(dianJi);
			}
		return Message.success(dianJi);
	}
	
	@ResponseBody
	@RequestMapping("setting.v1")
	@SystemLog(module="/sys/dianji/setting",methods = "setting")
	public Message<DianJi> addOrUpdate(DianJi dianJi){
		if(StringUtils.isEmpty(dianJi.getId())&&StringUtils.isEmpty(dianJi.getDeviceNo()))
			throw new BizException(Code.NOT_PARAM);
		
		DianJi addOrUpdate = dianJiService.addOrUpdate(dianJi);
		return Message.success(addOrUpdate);
	}
	
	@ResponseBody
	@RequestMapping("get900DataList.v1")
	public Message<List<eric900_data>> get900DataList(String monitorFlag, Long timeStart, Long timeEnd){
		if(StringUtils.isEmpty(monitorFlag))
			throw new BizException(Code.NOT_PARAM);
		
		 List<eric900_data> dataList = XWDataEntry.get900DataList(monitorFlag, timeStart==null?0:timeStart,timeEnd==null?System.currentTimeMillis():timeEnd);
		return Message.success(dataList);
	}

    @ResponseBody
    @RequestMapping("getConfig900.v1")
    public Message<?> getConfig900(String monitorFlag){
        if(StringUtils.isEmpty(monitorFlag))
            throw new BizException(Code.NOT_PARAM);
        
        if(XWDataEntry.getConfig900(monitorFlag)==null)
            throw new BizException(Code.ERR_DIANJI_CONFIG);

        return Message.success(XWDataEntry.getConfig900(monitorFlag));
    }

    /**
     *电机数据显示的曲线
     *power_a;//A相功率因数
     *power_b;//B相功率因数
     *power_c;//C相功率因数
     *temp_machine;//电机温度
     *gravity_x;//x轴重力
     *gravity_y;//y轴重力
     *gravity_z;//z轴重力
     * @param config
     * @return
     */
    @ResponseBody
    @RequestMapping("sync900Config.v1")
    public Message<?> Sync900Config(eric900_config config){
    	config.setGravity((byte)(config.getGravity() & 0xff));
    	System.out.print("config:"+JSON.toJSON(config));
        if(XWDataEntry.Sync900Config(config))
        	return Message.success();
        
        throw new BizException(Code.ERR_DIANJISYNC);
    }
	
	
    /**
     * 电机负责人下拉列表
     * @param companyId
     * @return
     */
	@ResponseBody
	@RequestMapping("memberInfoSelectList.v1")
	public Message<List<MemberModel>> memberInfoList(String  companyId){
		Message<List<MemberModel>> memberInfoListByCompanyId = memberService.getMemberInfoListByCompanyId(companyId);
		return memberInfoListByCompanyId;
	}

	/**
	 * 电机列表
	 * @param token
	 * @param pointName
	 * @return
	 */
	@ResponseBody
	@RequestMapping("dianJiList.v1")
	public Message<PageData<DianJiModel>> getDianJiList(String token,String pointName,String status){
		PageData<DianJiModel> page = new PageData<>();
		List<DianJiModel>  listDianJiModel  = new ArrayList<>();
	    PageData<PointModel> dianJiListByPrincipal = monitorPointService.getDianJiListWebUser(token,pointName,status);
	    DianJiModel  dianJiModel  = null;
	    List<PointModel> list = dianJiListByPrincipal.getList();
	    	if(list==null||list.isEmpty())
	    		return  Message.success(page);

	    	for(int i=0;i< list.size();i++){
	    		dianJiModel = new DianJiModel();
		    	BeanUtils.copyProperties(list.get(i), dianJiModel);
		    	listDianJiModel.add(dianJiModel);
	    	}
		    
	      page.setCount(listDianJiModel.size());
	      page.setList(listDianJiModel);
		return Message.success(page);
	}

	/**
	 * 电机列表数据表格
	 * @param token
	 * @param pointName
	 * @return
	 */
	@ResponseBody
	@RequestMapping("dianJiListData.v1")
	public Message<List<eric900_data>> getDianJiLists(String token,String pointName,String status){
		PageData<PointModel> dianJiListByPrincipal = monitorPointService.getDianJiListWebUser(token,pointName,status);

        if(dianJiListByPrincipal.getList()==null||dianJiListByPrincipal.getList().isEmpty())
            return  Message.success(new ArrayList<>());

        List<String> pointFlagList = dianJiListByPrincipal.getList().stream().map(PointModel::getPointFlag).collect(Collectors.toList());
        List<eric900_data> result = pointFlagList == null ? new ArrayList<eric900_data>() : XWDataEntry.getLast900Data(pointFlagList);
            return  Message.success(result);
	}
	/**
	 *  id集合的所有最新一条数据	 * @param Ids
	 * @param token
	 * @param pointName
	 * @return
	 */
	@ResponseBody
	@RequestMapping("dianJiListData.v2")
	public Message<List<Motor_data>> getMotorList(String token,String pointName,String status){
		PageData<PointModel> dianJiListByPrincipal = monitorPointService.getDianJiListWebUser(token,pointName,status);

        if(dianJiListByPrincipal.getList()==null||dianJiListByPrincipal.getList().isEmpty())
            return  Message.success(new ArrayList<>());

        List<String> pointFlagList = dianJiListByPrincipal.getList().stream().map(PointModel::getPointFlag).collect(Collectors.toList());
        List<Motor_data> result = pointFlagList == null ? new ArrayList<Motor_data>() : XWDataEntry.getLastmotorData(pointFlagList);
        
            return  Message.success(result);
	}
	
	
	/**
	 * 获取电机某一段时间的所有数据
	 * @param monitorFlag
	 * @param timeStart
	 * @param timeEnd
	 * @return
	 */
	@ResponseBody
	@RequestMapping("get900DataList.v2")
	public Message<List<Motor_data>> getLastmotorData(String monitorFlag, Long timeStart, Long timeEnd){
		if(StringUtils.isEmpty(monitorFlag))
			throw new BizException(Code.NOT_PARAM);
		 List<Motor_data> dataList = XWDataEntry.getMotorList(monitorFlag, timeStart==null?0:timeStart,timeEnd==null?System.currentTimeMillis():timeEnd);
		return Message.success(dataList);
	}

	/**
	 * 获取某个设备的告警列表
	 * @param monitorFlag
	 * @param page
	 * @param rows
	 * @return
	 */
	@ResponseBody
	@RequestMapping("getFaultInfoForId.v1")
	public Message<List<fault_data_new>> getFaultInfoForId(String monitorFlag, Integer page, Integer rows){
		List<fault_data_new> faultInfoForId = XWDataEntry.getFaultInfoForId(monitorFlag, page, rows);
		return Message.success(faultInfoForId);
	}



	public static void main(String[] args) {

		List<fault_data_new> faultInfoForId = XWDataEntry.getFaultInfoForId("ERIC0900191100000001", 0, 10);

		MotorConfig motorSet = XWDataEntry.getMotorSet("abc123");
		System.err.println(JSON.toJSON(motorSet));
		//System.err.println(JSON.toJSON(faultInfoForId));
	

	}


}
