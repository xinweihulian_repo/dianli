package com.yscoco.dianli.controller.api;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.yscoco.dianli.common.aspect.SystemLog;
import com.yscoco.dianli.common.vo.Message;
import com.yscoco.dianli.dto.company.LinesOnBranchDto;
import com.yscoco.dianli.entity.company.LinesOnBranch;
import com.yscoco.dianli.entity.company.MonitorPoint;
import com.yscoco.dianli.entity.sys.SysUser;
import com.yscoco.dianli.form.ImportSensorForm;
import com.yscoco.dianli.form.ParseSeneorData;
import com.yscoco.dianli.service.company.MonitorPointService;
import com.yscoco.dianli.service.sys.SysUserService;

import cn.hutool.poi.excel.ExcelReader;
import cn.hutool.poi.excel.ExcelUtil;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.yscoco.dianli.common.exception.BizException;
import com.yscoco.dianli.common.vo.Code;
import com.yscoco.dianli.service.company.LinesOnBranchService;


import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toCollection;

/**
 * @author dong
 * @date 2019/9/27
 */
@Controller
@RequestMapping("/sys/sensorImport/")
public class ImportSensorDataByExcelController {

    @Autowired
    private LinesOnBranchService linesOnBranchService;
    @Autowired
    private MonitorPointService monitorPointService;
    @Autowired
    private SysUserService sysUserService;

    /**
     * 从选中母线开始导入数据
     * @param form
     * @param request
     * @param response
     * @return
     * @throws IOException
     */
    @ResponseBody
    @RequestMapping(value = "upload", method = {RequestMethod.GET, RequestMethod.POST})
    @SystemLog(module="sys/sensorImport",methods = "从选中母线开始导入数据")
    public Message<?> readExcel(ImportSensorForm form, HttpServletRequest request, HttpServletResponse response) throws IOException {
        SysUser user = this.getSySuserByToken(form.getToken());
        InputStream in = null;
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        List<ParseSeneorData> parseSeneorDataList = null;
        try {
        	 MultipartFile file = multipartRequest.getFile("upfile"); 
             in = file.getInputStream();
             //ExcelUtil.readBySax(in, -1, createRowHandler());
             ExcelReader reader = ExcelUtil.getReader(in);
             parseSeneorDataList = reader.readAll(ParseSeneorData.class);
        } catch (Exception e) {
        	System.err.print( e.getMessage());
            throw new BizException(Code.EXCEL_NOTEXIST);
        } finally {
            if (in != null)
                try {
                    in.close();
                } catch (IOException e) {
                    throw new BizException(Code.SYS_ERR);
                }
        }
        parseSeneorDataList = checkParseSeneorDataFormat(parseSeneorDataList);
        addSeneorData(form, user, parseSeneorDataList);
        return Message.success();
    }

    private void addSeneorData(ImportSensorForm form, SysUser user, List<ParseSeneorData> parseSeneorDataList) {
        LinesOnBranch branch = null;
        MonitorPoint monitorPoint = null;
        Message<LinesOnBranchDto> linesOnBranchDtoMessage = null;
        for (int i = 0; i < parseSeneorDataList.size(); i++) {
            System.out.println(parseSeneorDataList.get(i));
            monitorPoint = monitorPointService.getByPointFlag(parseSeneorDataList.get(i).getMonitorFlag());

            if (monitorPoint != null)
                monitorPointService.delete(monitorPoint);

            branch = linesOnBranchService.getByBranchName(parseSeneorDataList.get(i).getBranchName());

            linesOnBranchDtoMessage = linesOnBranchService.addOrUpdateLinesOnBranch(
                    branch == null ? null : branch.getId(),
                    parseSeneorDataList.get(i).getBranchName(),
                    "",
                    "",
                    "",
                    form.getMasterId(),
                    "",
                    parseSeneorDataList.get(i).getCabin_no(),
                    parseSeneorDataList.get(i).getBranchType(),
                    parseSeneorDataList.get(i).getSensorNumber(),
                    user.getId(),
                    "");

            monitorPointService.addOrUpdateMonitorPoint(
                    null,
                    parseSeneorDataList.get(i).getPointName(),
                    "",
                    "",
                    parseSeneorDataList.get(i).getMonitorFlag(),
                    parseSeneorDataList.get(i).getHitchType(),
                    parseSeneorDataList.get(i).getType(),
                    parseSeneorDataList.get(i).getPosition(),
                    linesOnBranchDtoMessage.getData().getId(),
                    user.getId(), "");
        }
    }

    private List<ParseSeneorData> checkParseSeneorDataFormat(List<ParseSeneorData> parseSeneorDataList) {
        parseSeneorDataList.forEach(m -> {
            if (StringUtils.isEmpty(m.getMonitorFlag()) || StringUtils.isEmpty(m.getBranchName())) {
                throw new BizException(Code.EXCEL_FORMATERROR);
            }
        });
        parseSeneorDataList = parseSeneorDataList.stream().collect(collectingAndThen(toCollection(() -> new TreeSet<>(Comparator.comparing(ParseSeneorData::getMonitorFlag))), ArrayList::new));
        return parseSeneorDataList;
    }

    private SysUser getSySuserByToken(String token) {
        SysUser sysUserBytoken = sysUserService.getByToken(token);
        if (sysUserBytoken == null)
            throw new BizException(Code.NOT_LOGIN);
        return sysUserBytoken;
    }


//    public Message<?> nThreadAdd(ImportSensorForm form, SysUser user, List<ParseSeneorData> resources) {
//
//        int nThreads = 5;
//        int size = resources.size();
//        List<Future<Integer>> futures = new ArrayList<Future<Integer>>(nThreads);
//        ExecutorService executorService = Executors.newFixedThreadPool(nThreads);
//
//        for (int i = 0; i < nThreads; i++) {
//            final List<ParseSeneorData> resourcesList = resources.subList(size / nThreads * i, size / nThreads * (i + 1));
//            Callable<Integer> task1 = () -> {
//                addSeneorData(form, user, resourcesList);
//                return 1;
//            };
//            Future<Integer> submit = executorService.submit(task1);
//            futures.add(submit);
//        }
//
//        if (size % nThreads != 0) {
//            final List<ParseSeneorData> resourcesList = resources.subList(size - (size % nThreads), size);
//            Callable<Integer> task1 = () -> {
//                addSeneorData(form, user, resourcesList);
//                return 1;
//
//            };
//
//            Future<Integer> submit = (Future<Integer>) executorService.submit(task1);
//            futures.add(submit);
//        }
//
//        executorService.shutdown();
//        if (!futures.isEmpty() && futures != null) {
//            return Message.success();
//        }
//        return Message.success();
//
//    }

//   private RowHandler createRowHandler() {
//        return new RowHandler() {
//            @Override
//            public void handle(int sheetIndex, int rowIndex, List<Object> rowlist) {
//
//					for (int i = 0; i <rowlist.size() ; i++) {
//						//this...begin
//
//					}
//      //  Console.log("[{}] [{}] {}", sheetIndex, rowIndex, rowlist);
//        }
//    };
//}
    

}