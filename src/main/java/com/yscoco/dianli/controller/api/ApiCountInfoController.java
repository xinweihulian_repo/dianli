package com.yscoco.dianli.controller.api;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

import com.hyc.smart.XWDataEntry;
import com.yscoco.dianli.entity.company.MonitorStationDevice;
import com.yscoco.dianli.service.company.MonitorStationDeviceService;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.yscoco.dianli.common.base.BaseController;
import com.yscoco.dianli.common.utils.DateUtils;
import com.yscoco.dianli.common.utils.PoiUtil;
import com.yscoco.dianli.common.vo.Message;
import com.yscoco.dianli.dto.company.CompanyDto;
import com.yscoco.dianli.entity.member.MemberInfo;
import com.yscoco.dianli.pojo.MasterModel;
import com.yscoco.dianli.pojo.PointModel;
import com.yscoco.dianli.service.company.CompanyService;
import com.yscoco.dianli.service.member.MemberService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Controller
@RequestMapping("/api/count/")
public class ApiCountInfoController extends BaseController {
	@Autowired
	private CompanyService companyService;

	@Autowired
	private MemberService memberService;

	@Autowired
	private MonitorStationDeviceService monitorStationDeviceService;


	//private static Logger logger = Logger.getLogger(ApiCountInfoController.class);

	// 统计信息：下拉框拼接
	@ResponseBody
	@RequestMapping(value = "queryStaticThree.v1")
	@ApiOperation(value = "统计信息：下拉框拼接", httpMethod = "POST")
	public Message<List<CompanyDto>> queryStaticThree(@ApiParam(required = false, value = "公司Id") String companyId,
			@ApiParam(required = false, value = "创建人Id") String createBy,
			@ApiParam(required = false, value = "type:3级联动(3),5级联动(5)") String type) {
		List<CompanyDto> queryStaticThree = this.companyService.queryStaticThree(companyId, createBy, type);
		return Message.success(queryStaticThree);
	}

	@ResponseBody
	@RequestMapping(value = "getStatisticsData")
	@ApiOperation(value = "统计信息：")
	public Message<Map<String, Object>> queryMasterAndmonitorPointData(String companyId, String userId, String token,
			String monitorStationId, String masterId,
			@ApiParam(required = false, value = "masterFlag:('Y'（显示母线信息）.N（不显示母线信息）)母线信息 AS 800设备") String masterFlag,
			@ApiParam(required = false, value = "voltageFlag:('Y'（显示过电压保护器信息）.N过电压保护器") String voltageFlag,
			@ApiParam(required = false, value = "temperFlag:('Y'（显示温度采集器信息）.N温度采集器)") String temperFlag,
			String timeStart, String timeEnd, HttpServletResponse respons, String isDown) throws InvocationTargetException, IllegalAccessException {
		onGetRequestParse(); 
		if(isDown==null||isDown.equals("")){isDown="N";}

		timeStart = StringUtils.isEmpty(timeStart) ? DateUtils.get1970() : timeStart;
		timeEnd = StringUtils.isEmpty(timeEnd) ? DateUtils.getPlanEnd() : timeEnd;

		long timeStart_ = DateUtils.dateToUnixTimestamp(timeStart, "yyyy-MM-dd HH:mm:ss");
		long timeEnd_ = DateUtils.dateToUnixTimestamp(timeEnd, "yyyy-MM-dd HH:mm:ss");

		Map<String, Object> mapData = new HashMap<>(); // 返回前端的数据
		List<MasterModel> masterList = this.companyService.queryAllMasterFlag(companyId);
		List<PointModel> pointList = this.companyService.queryAllPointFlag(companyId);
		//System.out.println("masterList:"+masterList);
		
		//System.out.println("pointList:"+pointList);
		List<String> responsibleMasterList = new ArrayList<>(); // 所负责的母线设备标识
		List<String> responsiblePointList = new ArrayList<>(); // 所负责的监测点设备标识
		MemberInfo member = memberService.getById(userId);// 判断是否是巡检员登陆后台
		if (member != null) {
			Map<String, Object> memberGetStatisticsData = companyService.memberGetStatisticsData(monitorStationId,masterId, masterFlag, voltageFlag, temperFlag, timeStart_, timeEnd_, mapData, masterList, pointList,responsibleMasterList, responsiblePointList, member);
			try {
				if (isDown.equalsIgnoreCase("Y")) {
					PoiUtil.exportExcel("母线", "过电压保护器", "温度采集器", "报警统计", memberGetStatisticsData, response,request);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			return Message.success(memberGetStatisticsData);
		} else {
			Map<String, Object> managerGetStatisticsData = companyService.managerGetStatisticsData(monitorStationId,masterId, masterFlag, voltageFlag, temperFlag, timeStart_, timeEnd_, mapData, masterList,pointList);
			try {
				if (isDown.equalsIgnoreCase("Y")) {
					PoiUtil.exportExcel("母线", "过电压保护器", "温度采集器", "报警统计", managerGetStatisticsData, response,request);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			return Message.success(managerGetStatisticsData);
		}
	}


	/**
	 *
	 * @param stationId
	 * @param timeStart
	 * @param timeEnd
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "nzjMaxValueTemper.v1")
	public Message<?> queryNzjMaxValueTemper( String stationId, Long timeStart, Long timeEnd) {
		Map<String,Object> result = new HashMap<>(2);

			List<Map<String,Object>> temperTable = new ArrayList<>();

			Integer sumCount = 0;

			List<Map<String, Object>> maxValueTemper=null ;

				List<MonitorStationDevice> byStationId = monitorStationDeviceService.getByStationId(stationId);

				List<String> ids = byStationId.stream().map(MonitorStationDevice::getDeviceId).collect(Collectors.toList());
                    if(ids!=null&&!ids.isEmpty())
                        maxValueTemper = XWDataEntry.getmaxValueTemper(ids, (timeStart == null ? 0 : timeStart), (timeEnd == null ? System.currentTimeMillis() : timeEnd));

                    if(maxValueTemper!=null&&!maxValueTemper.isEmpty())
                        maxValueTemper.forEach(m->{
                            byStationId.forEach(s->{
                                if(m.get("sensorId").toString().equals(s.getDeviceId())){
                                    Map<String,Object> dto = new HashMap<>();
                                    dto.put("temper",m.get("temper"));
                                    dto.put("deviceName",s.getDeviceName());
                                    dto.put("position",s.getPosition());
                                    dto.put("deviceId",s.getDeviceId());
                                    temperTable.add(dto);
                                }
                            });
                        });

				//报警数量统计
				List<Map<String, Object>> allCountByFaultType = XWDataEntry.getAllCountByFaultType(ids,(timeStart == null ? 0 : timeStart), (timeEnd == null ? System.currentTimeMillis() : timeEnd));

				for (Map<String, Object> m : allCountByFaultType) {
					sumCount += Integer.valueOf(m.get("cnt").toString());
				}

		result.put("temperTable", temperTable);
		result.put("sumCount", sumCount);

		return Message.success(result);
	}


	public static void main(String[] args) {
		List<String> list = new ArrayList<>();
			list.add("M8002");
			list.add("C867");
			list.add("A3815");

		List<Map<String, Object>> maxValueTemper = XWDataEntry.getmaxValueTemper(list, 0,System.currentTimeMillis());
		for (Map<String, Object> stringObjectMap : maxValueTemper) {
			System.out.println(stringObjectMap);
		}
		//System.out.println(JSON.toJSON(maxValueTemper));
		List<Map<String, Object>> allCountByFaultType = XWDataEntry.getAllCountByFaultType(list, 0,System.currentTimeMillis());
		System.out.println(allCountByFaultType);
		for (Map<String, Object> s : allCountByFaultType) {
			System.out.println(s);
		}
	}

}
