package com.yscoco.dianli.controller.api;

import com.yscoco.dianli.common.aspect.SystemLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.yscoco.dianli.common.base.BaseController;
import com.yscoco.dianli.common.exception.BizException;
import com.yscoco.dianli.common.vo.Code;
import com.yscoco.dianli.common.vo.Message;
import com.yscoco.dianli.common.vo.PageData;
import com.yscoco.dianli.constants.ProtectorType;
import com.yscoco.dianli.dto.app.AppMontorPointDetailInfoDto;
import com.yscoco.dianli.dto.company.LinesOnBranchDto;
import com.yscoco.dianli.dto.company.MonitorPointDto;
import com.yscoco.dianli.entity.company.DianJi;
import com.yscoco.dianli.entity.company.MonitorPoint;
import com.yscoco.dianli.service.company.DianJiService;
import com.yscoco.dianli.service.company.LinesOnBranchService;
import com.yscoco.dianli.service.company.MonitorPointService;

@Controller
@RequestMapping("/sys/monitorPoint/")
@Api(tags = "后台业务:5、监测点管理", description = "监测点增删查改", produces = MediaType.APPLICATION_JSON_VALUE, position = 1)
public class ApiMonitorPointController extends BaseController {
   
	@Autowired
	private MonitorPointService monitorPointService;
	@Autowired
	private LinesOnBranchService linesOnBranchService;
	@Autowired
	private DianJiService dianJiService;

	@ResponseBody
	@RequestMapping(value = "queryMonitorPointsForApp.v1")
	@ApiOperation(value = "app监测点查询", httpMethod = "POST")
	public Message<PageData<AppMontorPointDetailInfoDto>> queryMonitorPointsForApp(
			@ApiParam(required = false, value = "是否是列表请求数据：Y， N") String flag,
			@ApiParam(required = false, value = "用户Id") String memberId,
			@ApiParam(required = false, value = "所属公司") String companyId,
			@ApiParam(required = false, value = "母线Id") String linesOnMasterId,
			@ApiParam(required = false, value = "支线Id") String linesOnBranchId,
			@ApiParam(required = false, value = "支线名称查询") String branchName,
			@ApiParam(required = false, value = "页数") Integer page,
			@ApiParam(required = false, value = "行数") Integer rows) {
		PageData<LinesOnBranchDto> listPage = linesOnBranchService.listPage(flag, memberId, companyId, linesOnMasterId,linesOnBranchId, branchName, page, rows);
		List<AppMontorPointDetailInfoDto> AppMontorPointDetailInfoDtoList = new ArrayList<AppMontorPointDetailInfoDto>();
		List<LinesOnBranchDto> list = listPage.getList();
	//	System.out.println("LinesOnBranchDto:" + JSON.toJSONString(list));
		for (LinesOnBranchDto lob : list) {
			List<MonitorPointDto> monitorPointDtos = lob.getMonitorPointDtos();
			for (MonitorPointDto monitorPointDto : monitorPointDtos) {
				AppMontorPointDetailInfoDto data = new AppMontorPointDetailInfoDto();
				data.setPosition(monitorPointDto.getPosition());
				data.setMonitorPointName(monitorPointDto.getPointName());
				data.setMointorFlag(monitorPointDto.getMonitorFlag());
				data.setHitchType(monitorPointDto.getHitchType());
				data.setBranceLineName(lob.getBranchName());
				data.setType(monitorPointDto.getType());
				data.setMasterLineName(lob.getLinesOnMasterDto().getMasterName());
			//    data.setMonitorStationName(linesOnMasterService.getEntityByMasterName(lob.getLinesOnMasterDto().getMasterName()).getMonitorStation().getStationName());
			    data.setMonitorStationName(lob.getMonitorStationDto().getStationName()); 
			    
			    	
				/*
				 * String id = lob.getLinesOnMasterDto().getId(); LinesOnMaster
				 * entityById = linesOnMasterService.getEntityById(id);
				 * System.out.println("entityById:"+JSON.toJSONString(entityById
				 * ));
				 */
				AppMontorPointDetailInfoDtoList.add(data);
				continue;
			}
		}
		PageData<AppMontorPointDetailInfoDto> pageData = new PageData<>();
		pageData.setList(AppMontorPointDetailInfoDtoList);
		pageData.setCount(AppMontorPointDetailInfoDtoList.size());
		return new Message<>(Code.SUCCESS, pageData);
	}

	@ResponseBody
	@RequestMapping(value = "queryMonitorPoints.v1")
	@ApiOperation(value = "监测点查询", httpMethod = "POST")
	public Message<PageData<MonitorPointDto>> queryMonitorPoints(
			@ApiParam(required = false, value = "支线Id") String linesOnBranchId,
			@ApiParam(required = false, value = "监测点Id") String monitorPointId,
			@ApiParam(required = false, value = "监测点名称查询") String pointName,
			@ApiParam(required = false, value = "页数") Integer page,
			@ApiParam(required = false, value = "行数") Integer rows) {
		PageData<MonitorPointDto> data = monitorPointService.listPage(linesOnBranchId, monitorPointId, pointName, page,
				rows);
		return new Message<>(Code.SUCCESS, data);
	}

	@ResponseBody
	@RequestMapping("addOrUpdateMonitorPoint.v1")
	@SystemLog(module="/sys/monitorPoint/addOrUpdateMonitorPoint",methods = "监测点新增或修改")
	public Message<MonitorPointDto> addOrUpdateMonitorPoint(
			@ApiParam(required = false, value = "监测点Id") String monitorPointId,
			@ApiParam(required = false, value = "监测点名称") String pointName,
			@ApiParam(required = false, value = "所在地区") String pointAddress,
			@ApiParam(required = false, value = "经纬度") String longitudeAndlatitude,
			@ApiParam(required = false, value = "监测设备标识") String monitorFlag,
			@ApiParam(required = false, value = "所属分类:温度采集器	,过电压保护器 ") String hitchType,
			@ApiParam(required = false, value = "所属类型:电站型，电机型，电容型，有源表带式，无源表带式，红外式,...") String type,
			@ApiParam(required = false, value = "所属位置") String position,
			@ApiParam(required = false, value = "监测点所属支线") String linesOnBranchId) {
	//	Map<String, String[]> parameterMap = super.request.getParameterMap();
	//	System.out.println("parameterMap:"+JSON.toJSON(parameterMap));
		Message<MonitorPointDto> addOrUpdateMonitorPoint = monitorPointService.addOrUpdateMonitorPoint(monitorPointId, pointName, pointAddress,
				longitudeAndlatitude, monitorFlag, hitchType,type ,position, linesOnBranchId, this.getLoginManagerId(),
				request.getRemoteAddr());
	//	 System.out.println("addOrUpdateMonitorPoint:127行"+JSON.toJSONString(addOrUpdateMonitorPoint));
		if(!StringUtils.isEmpty(hitchType)&&ProtectorType.dianji.getName().equals(hitchType.trim())){
			MonitorPoint byPointFlag = monitorPointService.getByPointFlag(monitorFlag);
			
			DianJi dianJi = new DianJi();
			dianJi.setCreateTime(new Date());
			dianJi.setDeviceNo(hitchType);
			dianJi.setCreateBy(byPointFlag.getCreateBy());
		    dianJi = dianJiService.addOrUpdate(dianJi);
		}
		
		return addOrUpdateMonitorPoint;
	}

	@ResponseBody
	@RequestMapping("deleteMonitorPoint.v1")
	@SystemLog(module="/sys/monitorPoint/deleteMonitorPoint",methods = "监测点删除")
	public Message<?> deleteMonitorPoint(@ApiParam(required = false, value = "监测点Ids") String monitorPointIds) {
		 if(StringUtils.isBlank(getLoginManagerId())){
          	 throw new BizException(Code.NOT_LOGIN);   }
		return monitorPointService.delete(monitorPointIds, this.getLoginManagerId(), request.getRemoteAddr());
	}

	@ResponseBody
	@RequestMapping("addOrUpdateMonitorPointForApp.v1")
	@SystemLog(module="/sys/monitorPoint/addOrUpdateMonitorPointForApp",methods = "监测点新增或修改")
	public Message<MonitorPointDto> addOrUpdateMonitorPointForApp(
			@ApiParam(required = false, value = "监测点Id") String monitorPointId,
			@ApiParam(required = false, value = "监测点名称") String pointName,
			@ApiParam(required = false, value = "所在地区") String pointAddress,
			@ApiParam(required = false, value = "经纬度") String longitudeAndlatitude,
			@ApiParam(required = false, value = "监测设备标识") String monitorFlag,
			@ApiParam(required = false, value = "所属分类:温度采集器	,过电压保护器 ") String hitchType,
			@ApiParam(required = false, value = "所属类型:电站型，电机型，电容型，有源表带式，无源表带式，红外式,...") String type,
			@ApiParam(required = false, value = "所属位置") String position,
			@ApiParam(required = false, value = "监测点所属支线") String linesOnBranchId) {
		Message<MonitorPointDto> addOrUpdateMonitorPointForApp = monitorPointService.addOrUpdateMonitorPointForApp(
				monitorPointId, pointName, pointAddress, longitudeAndlatitude, monitorFlag, hitchType,type, position,
				linesOnBranchId, this.getLoginMemberId(), request.getRemoteAddr());
	//	System.out.println("addOrUpdateMonitorPointForApp:" + JSON.toJSONString(addOrUpdateMonitorPointForApp));
		return addOrUpdateMonitorPointForApp;
	}

}
