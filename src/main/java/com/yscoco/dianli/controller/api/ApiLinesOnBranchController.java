package com.yscoco.dianli.controller.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.batise.device.temperSet.Temper_set;
import com.hyc.smart.XWDataEntry;
import com.yscoco.dianli.common.aspect.SystemLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yscoco.dianli.common.base.BaseController;
import com.yscoco.dianli.common.exception.BizException;
import com.yscoco.dianli.common.vo.Code;
import com.yscoco.dianli.common.vo.Message;
import com.yscoco.dianli.common.vo.PageData;
import com.yscoco.dianli.dto.company.LinesOnBranchDto;
import com.yscoco.dianli.dto.company.MonitorPointDto;
import com.yscoco.dianli.service.company.LinesOnBranchService;
import com.yscoco.dianli.service.company.LinesOnMasterService;

@Controller
@RequestMapping("/sys/linesOnBranch/")
@Api(tags="后台业务:4、支线",description="支线增删查改", produces = MediaType.APPLICATION_JSON_VALUE,position=1)
public class ApiLinesOnBranchController extends BaseController {

	@Autowired
	private LinesOnBranchService linesOnBranchService;
	
	@Autowired
	private LinesOnMasterService linesOnMasterService;
	
	@ResponseBody
	@RequestMapping(value="queryLinesOnBranch.v1")
	@ApiOperation(value = "支线查询" ,httpMethod = "POST" )
	public Message<PageData<LinesOnBranchDto>> queryLinesOnBranch(
			@ApiParam(required = false,  value = "是否是列表请求数据：Y， N") String flag,
			@ApiParam(required = false,  value = "所属公司") String memberId,
			@ApiParam(required = false,  value = "所属公司") String companyId,
			@ApiParam(required = false,  value = "母线Id") String linesOnMasterId,
			@ApiParam(required = false,  value = "支线Id") String linesOnBranchId, 
			@ApiParam(required = false,  value = "支线名称查询") String branchName,
			@ApiParam(required = false,  value = "页数") Integer page, 
			@ApiParam(required = false,  value = "行数") Integer rows) {
			PageData<LinesOnBranchDto> data = linesOnBranchService.listPage(flag,memberId,companyId,linesOnMasterId,linesOnBranchId,branchName,page,rows);
			//System.out.println("支线查询"+JSON.toJSON(data));
			return new Message<>(Code.SUCCESS,data);
	}
	
	
	@ResponseBody
	@RequestMapping(value="queryLinesOnBranchBySelect.v1")
	@ApiOperation(value = "为select下拉框做支线查询")
	public Message<List<MonitorPointDto>> queryLinesOnBranchBySelect(
			@ApiParam(required = false,  value = "支线Id") String linesOnBranchId) {
			List<MonitorPointDto> data = linesOnBranchService.queryLinesOnBranchBySelect(linesOnBranchId);
		//	System.out.println("为select下拉框做支线查询"+JSON.toJSON(data));
			return new Message<>(Code.SUCCESS,data);
	}
	
	
	@ResponseBody
	@RequestMapping("addOrUpdateLinesOnBranch.v1")
	@SystemLog(module="/sys/linesOnBranch/addOrUpdateLinesOnBranch",methods = "支线新增或修改")
	public Message<LinesOnBranchDto> addOrUpdateLinesOnBranch(
			@ApiParam(required = false,  value = "支线Id") String linesOnBranchId, 
			@ApiParam(required = false,  value = "支线名称") String branchName,
			@ApiParam(required = false,  value = "支线地址") String branchAddress,
			@ApiParam(required = false,  value = "经纬度") String longitudeAndlatitude,
			@ApiParam(required = false,  value = "管理人员") String memberInfoId,
			@ApiParam(required = false,  value = "所属母线") String linesOnMasterId,
			@ApiParam(required = false,  value = "支线下的监测点") String monitorPointsIds,
			@ApiParam(required = false,  value = "柜号") String cabin_no,
			@ApiParam(required = false,  value = "支线类型") String branchType,
			@ApiParam(required = false,  value = "传感器动作次数（3,4,9,12,6-1,6-2）") String sensorNumber){
		   if(StringUtils.isBlank(getLoginManagerId())){
          	 throw new BizException(Code.NOT_LOGIN);   }
		   Message<LinesOnBranchDto> addOrUpdateLinesOnBranch = linesOnBranchService.addOrUpdateLinesOnBranch(linesOnBranchId,branchName,branchAddress,longitudeAndlatitude,memberInfoId,linesOnMasterId,monitorPointsIds,cabin_no,branchType,sensorNumber,this.getLoginManagerId(),request.getRemoteAddr());
			return addOrUpdateLinesOnBranch;
	}
	 
	
	@ResponseBody
	@RequestMapping("deleteLinesOnBranch.v1")
	@SystemLog(module="/sys/linesOnBranch/deleteLinesOnBranch",methods = "支线删除")
	public Message<?> deleteLinesOnBranch(
			@ApiParam(required = false,  value = "支线Ids") String linesOnBranchIds) {
		   if(StringUtils.isBlank(getLoginManagerId())){
          	 throw new BizException(Code.NOT_LOGIN);   }
			return linesOnBranchService.delete(linesOnBranchIds,this.getLoginManagerId(),request.getRemoteAddr());
	}
 
	@ResponseBody
	@RequestMapping("SyncTopol.v1")
	@ApiOperation(value = "同步一次接线图到终端" ,httpMethod = "POST" )
	public Message<?> SyncTopol(
			@ApiParam(required = false,  value = "支线Id") String linesOnBranchId) {
		   if(StringUtils.isBlank(getLoginManagerId())){
          	 throw new BizException(Code.NOT_LOGIN);   }
			return linesOnBranchService.SyncTopol(linesOnBranchId,this.getLoginManagerId());
	}
	 
	
	/**
	 * 根据母线id查询支线集合
	 * @param type
	 * @param masterId
	 */
	@ResponseBody
	@RequestMapping(value="queryLinesOnBranchByMasterId.v1")
	public Message<List<LinesOnBranchDto>> queryLinesOnMasterByMonitorIdAndType(String type,String masterId) {

		if(StringUtils.isBlank(masterId)){
			 throw new BizException(Code.ERR_PARAM);
		}
		
        if(null== linesOnMasterService.getEntityById(masterId)){
			 throw new BizException(Code.NOTEXIST);
		 }

		 List<LinesOnBranchDto> linesOnMasterByMonitorStationStationId = linesOnBranchService.getLinesOnBranchByOnMasterId(masterId);
		return Message.success(linesOnMasterByMonitorStationStationId);
	}

	@ResponseBody
	@RequestMapping("getTemperSetList.v1")
	@ApiOperation(value = "获取温度传感器的报警阀值数据")
	public Message<?> getTemperSetList(@ApiParam(required = true,  value = "温度传感器的ID集合")  String Ids) {

        if(StringUtils.isBlank(getLoginManagerId())){ throw new BizException(Code.NOT_LOGIN);}

        List<Temper_set> temperSetList=null;

        try {
            if (StringUtils.isNotEmpty(Ids)) {
                temperSetList = XWDataEntry.getTemperSetList( Arrays.asList(Ids.split(",")));
            }
        }catch (Exception e){
            return Message.newError(Code.XWDataEntry_ERROR);
        }

        return Message.success(temperSetList);
	}

	@ResponseBody
	@RequestMapping("updateTemperSet.v1")
	@CrossOrigin
	@SystemLog(module="/sys/linesOnBranch/updateTemperSet",methods = "更新传感器的报警阀值")
	public Message<?> updateTemperSet(@ApiParam(required = true,  value = "温度传感器的ID集合")  @RequestBody List<Temper_set> list) {
	//	if(StringUtils.isBlank(getLoginManagerId())){ throw new BizException(Code.NOT_ACTION);}
        try {
            if(null!=list){XWDataEntry.updateTemperSet(list);}
        }catch (Exception e){
            return Message.newError(Code.XWDataEntry_ERROR);
        }
		return Message.success();
	}

}


