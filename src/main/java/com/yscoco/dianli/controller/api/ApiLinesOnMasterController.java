package com.yscoco.dianli.controller.api;

import com.yscoco.dianli.common.aspect.SystemLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yscoco.dianli.common.base.BaseController;
import com.yscoco.dianli.common.exception.BizException;
import com.yscoco.dianli.common.vo.Code;
import com.yscoco.dianli.common.vo.Message;
import com.yscoco.dianli.common.vo.PageData;
import com.yscoco.dianli.dto.company.LinesOnMasterDto;
import com.yscoco.dianli.service.company.LinesOnMasterService;
import com.yscoco.dianli.service.company.MonitorStationService;

@Controller
@RequestMapping("/sys/linesOnMaster/")
@Api(tags="后台业务:3、母线",description="母线增删查改", produces = MediaType.APPLICATION_JSON_VALUE,position=1)
public class ApiLinesOnMasterController extends BaseController {
	@Autowired
	private MonitorStationService monitorStationService;
	@Autowired
	private LinesOnMasterService linesOnMasterService;
	
	@ResponseBody
	@RequestMapping(value="queryLinesOnMaster.v1")
	@ApiOperation(value = "母线查询" ,httpMethod = "POST" )
	public Message<PageData<LinesOnMasterDto>> queryLinesOnMaster(
			@ApiParam(required = false,  value = "公司监测站Id") String monitorStationId, 
			@ApiParam(required = false,  value = "母线Id") String linesOnMasterId, 
			@ApiParam(required = false,  value = "母线名称查询") String masterName,
			@ApiParam(required = false,  value = "页数") Integer page, 
			@ApiParam(required = false,  value = "行数") Integer rows) {
			PageData<LinesOnMasterDto> pageData = linesOnMasterService.listPage(monitorStationId,linesOnMasterId,masterName,page,rows);
			return Message.success(pageData);
	}
	
	@ResponseBody
	@RequestMapping("addOrUpdateLinesOnMaster.v1")
	@ApiOperation(value = "母线新增或修改" ,httpMethod = "POST" )
	@SystemLog(module="/sys/linesOnMaster/addOrUpdateLinesOnMaster",methods = "母线新增或修改")
	public Message<LinesOnMasterDto> addOrUpdateLinesOnMaster(
			@ApiParam(required = false,  value = "母线Id") String linesOnMasterId, 
			@ApiParam(required = false,  value = "母线名称") String masterName,
			@ApiParam(required = false,  value = "母线所在地区") String masterAddress,
			@ApiParam(required = false,  value = "母线经纬度") String longitudeAndlatitude,
			@ApiParam(required = false,  value = "上一段母线") String preMasterId,
//			@ApiParam(required = false,  value = "下一段母线") String nextMasterId,
			@ApiParam(required = false,  value = "关联方式:无关联 母线关联 支线关联") String relatedType,
			@ApiParam(required = false,  value = "监测设备标识") String monitorFlag,
			@ApiParam(required = false,  value = "所属分类:温度 过电压 极地短路") String hitchType,
			@ApiParam(required = false,  value = "所属监测站") String monitorStationId,
			@ApiParam(required = false,  value = "母线所拥有的支线") String linesOnBranchsIds){
		   if(StringUtils.isBlank(getLoginManagerId())){
          	 throw new BizException(Code.NOT_LOGIN);   }
			return linesOnMasterService.addOrUpdateLinesOnMaster(linesOnMasterId,masterName,masterAddress,longitudeAndlatitude,
					preMasterId,relatedType,monitorFlag,hitchType,monitorStationId,linesOnBranchsIds,
					this.getLoginManagerId(),request.getRemoteAddr());
	}
	
	@ResponseBody
	@RequestMapping("deleteLinesOnMaster.v1")
	@SystemLog(module="/sys/linesOnMaster/deleteLinesOnMaster",methods = "母线删除")
	@ApiOperation(value = "母线删除" ,httpMethod = "POST" )
	public Message<?> deleteLinesOnMaster(
			@ApiParam(required = false,  value = "公司监测站Ids") String monitorStationIds) {
		   if(StringUtils.isBlank(getLoginManagerId())){
          	 throw new BizException(Code.NOT_LOGIN);   }
			return linesOnMasterService.delete(monitorStationIds,this.getLoginManagerId(),request.getRemoteAddr());
	}
	
	
	@ResponseBody
	@RequestMapping(value="queryLinesOnMasterIds.v1")
	@ApiOperation(value = "获取母线标识Id集合：即母线标志" ,httpMethod = "POST" )
	public Message<List<?>> queryLinesOnMasterIds(
			@ApiParam(required = false,  value = "那个端：app pc ") String type,
			@ApiParam(required = false,  value = "公司Id:不为空返回所有母线标识Id,参数都空表示所有母线标识Id") String companyId,
			@ApiParam(required = false,  value = "监测站Id:不为空返回当前监测站母线标识Id,为空返回companyId公司所有母线标识Id") String monitorStationId,
			@ApiParam(required = false,  value = "母线Id") String linesOnMasterId,
			@ApiParam(required = false,  value = "回显数据时候的母线Id") String linesOnMasterId_edit) {
			List<LinesOnMasterDto> pageData = linesOnMasterService.queryLinesOnMasterIds(type,companyId,monitorStationId,linesOnMasterId,linesOnMasterId_edit,null);
			return Message.success(pageData);
	}


	/**
	 * 根据监测站查询母线集合
	 * @param type
	 * @param monitorStationId
	 * @return
	 * /sys/linesOnMaster/queryLinesOnMasterByMonitorIdAndType.v1
	 */
	@ResponseBody
	@RequestMapping(value="queryLinesOnMasterByMonitorIdAndType.v1")
	public Message<List<LinesOnMasterDto>> queryLinesOnMasterByMonitorIdAndType(String type,String monitorStationId) {

		if(StringUtils.isBlank(monitorStationId)){throw new BizException(Code.ERR_PARAM); }
		
		if(null== monitorStationService.getEntityById(monitorStationId)){ throw new BizException(Code.NOTEXIST); }

		List<LinesOnMasterDto> linesOnMasterByMonitorStationStationId = linesOnMasterService.getLinesOnMasterByMonitorStationStationId(monitorStationId);

		return Message.success(linesOnMasterByMonitorStationStationId);
	}
}
