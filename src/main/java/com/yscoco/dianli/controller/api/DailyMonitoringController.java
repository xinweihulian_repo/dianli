package com.yscoco.dianli.controller.api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.poi.excel.ExcelUtil;
import cn.hutool.poi.excel.ExcelWriter;
import com.batise.device.eric900.eric900_data;
import com.yscoco.dianli.common.aspect.SystemLog;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.batise.device.temper.Temper_data;
import com.batise.device.voltage.Voltage_data;
import com.hyc.smart.XWDataEntry;
import com.yscoco.dianli.common.base.BaseController;
import com.yscoco.dianli.common.exception.BizException;
import com.yscoco.dianli.common.form.PointExportForm;
import com.yscoco.dianli.common.utils.DateUtils;
import com.yscoco.dianli.common.utils.PoiUtil;
import com.yscoco.dianli.common.vo.Code;
import com.yscoco.dianli.constants.ProtectorType;

import javax.servlet.ServletOutputStream;


/**
 *
 * @author liuDong.
 *
 */
@Controller
@RequestMapping("/daily/statement/")
public class DailyMonitoringController extends BaseController  {
	
	

	@ResponseBody
	@RequestMapping(value = "pointExportExcel")
	@SystemLog(module="daily/statement",methods = "Excel 导出")
	public void pointExportExcel(PointExportForm form)throws IOException{
		checkLogionStatus();
		onGetRequestParse(); 
		String timeStart = StringUtils.isEmpty(form.getTimeStart()) ? DateUtils.getFirstDayOfTheMonth() : form.getTimeStart();
		String timeEnd = StringUtils.isEmpty(form.getTimeEnd()) ? DateUtils.getPlanEnd() : form.getTimeEnd();
		
		if(form.getProtectorType()!=null&&!StringUtils.isEmpty(form.getProtectorType())){
			if(form.getProtectorType().equals(ProtectorType.guodianya.getName())){
				String[] sheetNames ={ProtectorType.guodianya.getName()};
				List<Voltage_data> voltageList = XWDataEntry.getVoltageList(form.getSensorId(), DateUtils.dateToUnixTimestamp(timeStart, "yyyy-MM-dd HH:mm:ss"), DateUtils.dateToUnixTimestamp(timeEnd, "yyyy-MM-dd HH:mm:ss"));
					if(voltageList==null||voltageList.isEmpty()){
						throw new BizException(Code.NO_DATAS);
					}
				//List<Voltage_data> voltageList = XWDataEntry.getVoltageList("B1", 0l, System.currentTimeMillis());
				PoiUtil.exportPointExcel(sheetNames, voltageList,form, response,request);
			}else if(form.getProtectorType().equals(ProtectorType.wendu.getName())){
				String[] sheetNamess ={ProtectorType.wendu.getName()};
				List<Temper_data> temperatureList = XWDataEntry.getTemperatureList(form.getSensorId(),  DateUtils.dateToUnixTimestamp(timeStart, "yyyy-MM-dd HH:mm:ss"), DateUtils.dateToUnixTimestamp(timeEnd, "yyyy-MM-dd HH:mm:ss"));
					temperatureList.forEach(v->{
						v.setSensorId(form.getSensorId());
					});
				if(temperatureList==null||temperatureList.isEmpty()){
						throw new BizException(Code.NO_DATAS);
					}
				//List<Temper_data> temperatureList = XWDataEntry.getTemperatureList("A101", 1535764488000l, System.currentTimeMillis());
			    PoiUtil.exportPointExcel(sheetNamess, temperatureList,form,response, request);
			}else if(form.getProtectorType().equals(ProtectorType.dianji.getName())){
				dianjiExportExcel(form);
			}
		}else{
			throw new BizException(Code.NOT_PARAM);
		}
	
	}


	private void dianjiExportExcel(PointExportForm form)throws IOException {
		onGetRequestParse();
		//checkLogionStatus();
		String timeStart = StringUtils.isEmpty(form.getTimeStart()) ? DateUtils.getFirstDayOfTheMonth() : form.getTimeStart();
		String timeEnd = StringUtils.isEmpty(form.getTimeEnd()) ? DateUtils.getPlanEnd() : form.getTimeEnd();

		List<eric900_data> get900DataList = XWDataEntry.get900DataList(form.getSensorId(), DateUtils.dateToUnixTimestamp(timeStart, "yyyy-MM-dd HH:mm:ss"), DateUtils.dateToUnixTimestamp(timeEnd, "yyyy-MM-dd HH:mm:ss"));
			if(get900DataList==null||get900DataList.size()==0){
				throw new BizException(Code.NO_DATAS);
			}
	
        List<Map<String, Object>> rows = new ArrayList<>();
        get900DataList.forEach(m->{
			Map<String, Object> row = new LinkedHashMap<>();
			row.put("id",m.getId());
			row.put("设备标识",m.getDevid());
			row.put("A相电流(mA)",m.getElec_a());
			row.put("B相电流(mA)",m.getElec_b());
			row.put("C相电流(mA)",m.getElec_c());
			row.put("泄漏电流(mA)",m.getElec_z());
			
			row.put("A相电压(V)",m.getVol_a());
			row.put("B相电压(V)",m.getVol_b());
			row.put("C相电压(V)",m.getVol_c());

			row.put("A相温度(℃)",m.getTemp_a());
			row.put("B相温度(℃)",m.getTemp_b());
			row.put("C相温度(℃)",m.getTemp_c());
			//row.put("N相温度(℃)",m.getTemp_n());
			
			row.put("电机温度(℃)",m.getTemp_machine());

			row.put("X轴振数(mm/s)",m.getGravity_x());
			row.put("Y轴振数(mm/s)",m.getGravity_y());
			row.put("Z轴振数(mm/s)",m.getGravity_z());

			row.put("上报时间",DateUtils.unixTimestampToDate(m.getTimestamp()));
			rows.add(row);

		});
      
		ExcelWriter writer = ExcelUtil.getWriter();

		//writer.write(get900DataList, true);
		writer.write(CollUtil.newArrayList(rows), true);
		response.setContentType("application/vnd.ms-excel;charset=utf-8");
		response.setHeader("Content-Disposition","attachment;filename="+get900DataList.get(0).getDevid()+".xls");
		ServletOutputStream out=response.getOutputStream();
		writer.flush(out, true);
		writer.close();
		IoUtil.close(out);
	}

}
