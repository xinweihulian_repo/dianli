package com.yscoco.dianli.controller.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import javax.annotation.Resource;

import com.yscoco.dianli.common.aspect.SystemLog;
import com.yscoco.dianli.common.utils.SpringUtils;

import com.yscoco.dianli.entity.member.MemberInfo;
import com.yscoco.dianli.service.comon.GetResponsibleDeviceInnerCache;
import com.yscoco.dianli.service.company.MonitorPointService;
import com.yscoco.dianli.service.member.MemberService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.batise.device.device_info;
import com.hyc.smart.XWDataEntry;
import com.hyc.smart.log.ExLogUtil;
import com.yscoco.dianli.common.base.BaseController;
import com.yscoco.dianli.common.utils.DateUtils;
import com.yscoco.dianli.common.vo.Message;
import com.yscoco.dianli.constants.AlarmLeve;
import com.yscoco.dianli.constants.FaultType;
import com.yscoco.dianli.dto.company.FaultMsgDesc;
import com.yscoco.dianli.dto.web.DeviceInfoDto;
import com.yscoco.dianli.dto.web.IndexAlarmDto;
import com.yscoco.dianli.other.cache.RedisCacheUtil;
import com.yscoco.dianli.pojo.MasterModel;
import com.yscoco.dianli.pojo.PointModel;
import com.yscoco.dianli.service.comon.GetResponsibleDeviceService;
import com.yscoco.dianli.service.comon.Transition;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Controller
public class IndexController extends BaseController {
	
    @Resource
    private RedisCacheUtil redisCache;

	@Autowired
	private GetResponsibleDeviceService  getResponsibleDeviceService;

	@Autowired
	private GetResponsibleDeviceInnerCache getResponsibleDeviceInnerCache;


	/**
	 * 首页左侧报警统计
	 * 
	 * @param token
	 * @param timeStart
	 * @param timeEnd
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/index/alarmCount")
	@SystemLog(module="index",methods = "左侧报警统计")
	public Message<IndexAlarmDto> getAlarmCount(String token, Long timeStart, Long timeEnd) {
//		List<MasterModel> masterModelList = getResponsibleDeviceService.getMasterModelByLoginUser(token);
//        List<PointModel> pointModelList = getResponsibleDeviceService.getPointModelByLoginUser(token);
        List<MasterModel> masterModelList =  getResponsibleDeviceInnerCache.getMasterModelByLoginUser(token);

        List<PointModel> pointModelList = getResponsibleDeviceInnerCache.getPointModelByLoginUser(token);

        List<String> masterDevCodeList = masterModelList.stream().map(MasterModel::getMonitorFlag).collect(Collectors.toList());

	    List<String> pointDevCodeList =pointModelList.stream().map(PointModel::getPointFlag).collect(Collectors.toList());

		ArrayList<String> pointDevCodeListRest = new ArrayList<>(new HashSet<>(pointDevCodeList));
		ArrayList<String> masterDevCodeListRest = new ArrayList<>(new HashSet<>(masterDevCodeList));
	
		if(pointDevCodeListRest!=null&&!pointDevCodeListRest.isEmpty()){ masterDevCodeListRest.addAll(pointDevCodeListRest); }

		List<Map<String, Object>> allCountByFaultType = XWDataEntry.getAllCountByFaultType(masterDevCodeListRest,timeStart, timeEnd);
		//System.out.println("allCountByFaultType:"+allCountByFaultType);
		/**
		 * 10,12,13,14,15,16,20,21,22 ,24属于高级报警 11,23 属于中级报警
		 */
		IndexAlarmDto indexAlarmDto = new IndexAlarmDto();
		for (Map<String, Object> map : allCountByFaultType) {
			map.forEach((k, v) -> {
				int valueOf = 0;
				if (k.equals("fault_type")) {
					Integer typeValue = Integer.valueOf(v.toString());
					switch (typeValue) {// FaultTypeFor10(10, "金属接地故障"),
					case 10:
						valueOf = Integer.valueOf(map.get("cnt").toString());
						indexAlarmDto.setJsjd(valueOf);
						break;
					case 11:// FaultTypeFor11(11, "系统过电压")
						valueOf = Integer.valueOf(map.get("cnt").toString());
						indexAlarmDto.setXtgdy(valueOf);
						break;
					case 12:// FaultTypeFor12(12, "过电压故障"),
						valueOf = Integer.valueOf(map.get("cnt").toString());
						indexAlarmDto.setGdy(valueOf);
						break;
					case 13:// FaultTypeFor13(13, "PT断线故障"),
						valueOf = Integer.valueOf(map.get("cnt").toString());
						indexAlarmDto.setPtdx(valueOf);
						break;
					case 14:// FaultTypeFor14(14, "欠压数据包"),
						valueOf = Integer.valueOf(map.get("cnt").toString());
						indexAlarmDto.setQysjb(valueOf);
						break;
					case 15:// FaultTypeFor15(15, "系统短路"),
						valueOf = Integer.valueOf(map.get("cnt").toString());
						indexAlarmDto.setXtdl(valueOf);
						break;
					case 16:// FaultTypeFor16(16, "弧光接地故障");
						valueOf = Integer.valueOf(map.get("cnt").toString());
						indexAlarmDto.setHgjd(valueOf);
						break;
					case 20:// FaultTypeFor20(20, "过热"),
						valueOf = Integer.valueOf(map.get("cnt").toString());
						indexAlarmDto.setGr(valueOf);
						break;
					case 21:// FaultTypeFor21(21, "湿度超标"),
						valueOf = Integer.valueOf(map.get("cnt").toString());
						indexAlarmDto.setSdcb(valueOf);
						break;
					case 22:// FaultTypeFor22(22, "泄漏"),
						valueOf = Integer.valueOf(map.get("cnt").toString());
						indexAlarmDto.setXl(valueOf);
						break;
					case 23:// FaultTypeFor23(23, "电量不足");
						valueOf = Integer.valueOf(map.get("cnt").toString());
						indexAlarmDto.setDlbz(valueOf);
						break;
					case 24:// FaultTypeFor24(24, "红外线测温故障");
						valueOf = Integer.valueOf(map.get("cnt").toString());
						indexAlarmDto.setHwx(valueOf);
						break;
					default:
						;
					}
				}
			});
		}
		int gaojiAlarmCount = indexAlarmDto.getQysjb() + indexAlarmDto.getXtdl() + indexAlarmDto.getGdy()+ indexAlarmDto.getHgjd() + indexAlarmDto.getJsjd() + indexAlarmDto.getPtdx()+indexAlarmDto.getGr()+indexAlarmDto.getXl()+indexAlarmDto.getSdcb()+indexAlarmDto.getHwx();
		
		int zhongjiAralmCount = indexAlarmDto.getXtgdy()+indexAlarmDto.getDlbz();
		indexAlarmDto.setGaoJiCount(gaojiAlarmCount);
		indexAlarmDto.setZhongJiCount(zhongjiAralmCount);
		// List<String> pointDevCodeList =
		// pointModelList.stream().map(PointModel::getPointFlag).collect(Collectors.toList());
		// HashSet<String> pointDevCodeListSet = new
		// HashSet<>(pointDevCodeList); //后面监测点有故障时启用
		return Message.success(indexAlarmDto);
	}

	@ResponseBody
	@RequestMapping("/index/getIndexRealFault")
	@ApiOperation(value = "首页实时报警信息")
	public Message<?> getIndexRealFault(
			@ApiParam(required = false, value = "beginTime:报警开始时间") Long beginTime,
			@ApiParam(required = false, value = "endTime:报警结束时间") Long endTime,
			@ApiParam(required = false, value = "isSolve是否已处理:false：未处理的故障;true：已处理的故障") Boolean isSolve,
			@ApiParam(required = false, value = "faultType:故障类型 :0:全部故障,接地故障:10,系统过电压:11 ,过电压:12,系统断线:13,欠压数据包:14,系统短路15,弧光接地故障16") Integer faultType,
			@ApiParam(required = false, value = "用来查询出登陆用户的信息") String token,
			@ApiParam(required = false, value = "页数:默认0") Integer page,
			@ApiParam(required = false, value = "行数:默认10") Integer rows) {

        String indexRealFaultList = redisCache.get("IndexRealFault:" + token);

        if(StringUtils.isNotBlank(indexRealFaultList)){
			 List<FaultMsgDesc> faultMsgDescs = JSON.parseArray(indexRealFaultList, FaultMsgDesc.class);
			return 	Message.success(faultMsgDescs);
		}

		List<String> deviceIds=new ArrayList<>();
		FaultMsgDesc faultMsgDesc = null;
		List<FaultMsgDesc> faultMsgDescList = new ArrayList<FaultMsgDesc>();

//		List<PointModel> pointModelByLoginUser = getResponsibleDeviceService.getPointModelByLoginUser(token);
//		List<MasterModel> masterModelByLoginUser = getResponsibleDeviceService.getMasterModelByLoginUser(token);
		List<MasterModel> masterModelByLoginUser =  getResponsibleDeviceInnerCache.getMasterModelByLoginUser(token);
		List<PointModel> pointModelByLoginUser = getResponsibleDeviceInnerCache.getPointModelByLoginUser(token);

		List<String>	pointFlagList = pointModelByLoginUser.stream().map(PointModel::getPointFlag).collect(Collectors.toList());
	    List<String>	masterFlagList = masterModelByLoginUser.stream().map(MasterModel::getMonitorFlag).collect(Collectors.toList());

	    if(pointFlagList!=null&&!pointFlagList.isEmpty()){
	 		deviceIds.addAll(pointFlagList);
	 	}
		if(masterFlagList!=null&&!masterFlagList.isEmpty()){
			deviceIds.addAll(masterFlagList);
	 	}
	    if (!deviceIds.isEmpty()) {
			isSolve = false;
			page = (page == null || page == 0) ? 0 : page - 1;
			rows = (rows == null) ? 10 : rows;
			faultType=(faultType == null)?0:faultType;
			beginTime=(beginTime == null)?0l:beginTime;
			endTime=(endTime == null)?System.currentTimeMillis():endTime;
			//long dateToUnixTimestamp = DateUtils.dateToUnixTimestamp(DateUtils.getPlanStart());//获取当天开始时间 00:00:00:000
//			System.out.println("dateToUnixTimestamp:"+dateToUnixTimestamp);
			List<Map<String, Object>> faultInfoForPage = XWDataEntry.getFaultInfoForTimePage(deviceIds, faultType,
					isSolve, beginTime, endTime, page, rows);
			//System.err.println("faultInfoForPage:"+JSON.toJSON(faultInfoForPage));
			if (faultInfoForPage == null) {
				//throw new BizException(Code.NO_DATA);
				Message.success(faultMsgDescList);
				}
		//	System.out.println("faultInfoForPage:" + JSON.toJSONString(faultInfoForPage));
			for (Map<String, Object> faultMsg : faultInfoForPage) {
				//System.out.println("faultMsg:" + JSON.toJSONString(faultMsg));	
				faultMsgDesc = new FaultMsgDesc();
				faultMsgDesc.setAutoId(Integer.valueOf(faultMsg.get("id").toString()));
				String devid = faultMsg.get("devid").toString();
				faultMsgDesc.setDevid(devid);
				Integer faultTypes = Integer.valueOf(faultMsg.get("fault_type").toString());
				faultMsgDesc.setFaultType(faultTypes);
				faultMsgDesc.setTimes(DateUtils.unixTimestampToDate(Long.valueOf(faultMsg.get("timestamp").toString())));
				faultMsgDesc.setTimestamp(Long.valueOf(faultMsg.get("timestamp").toString()));
				faultMsgDesc.setPhase(faultMsg.get("phase")==null?4:(Integer.valueOf(faultMsg.get("phase").toString())));
				// faultType:故障类型 :系统过电压:11   电量不足:23 属于中级报警 
				if (faultTypes == FaultType.FaultTypeFor11.getValue()||faultTypes == FaultType.FaultTypeFor23.getValue()) {
					faultMsgDesc.setAlarmLeve(AlarmLeve.zhongji.getName());
				} else {
					// faultType:故障类型
					// :接地故障:10,过电压:12,系统断线:13,欠压数据包:14,系统短路15,弧光接地故障16,过热20,湿度超标21,泄漏 22)
					faultMsgDesc.setAlarmLeve(AlarmLeve.gaoji.getName());
				}
				if (faultMsg.get("distimestamp") != null && faultMsg.get("distimestamp") != "") {
					String distimes= DateUtils.unixTimestampToDate(Long.valueOf(faultMsg.get("distimestamp").toString()));
					faultMsgDesc.setDistimes(distimes);
					faultMsgDesc.setDistimestamp(Long.valueOf(faultMsg.get("distimestamp").toString()));
				} 
				faultMsgDesc.setFault(Transition.changFaultTypeByTypeId(faultTypes));
				faultMsgDescList.add(faultMsgDesc);
				continue;
			}
			
//			for (FaultMsgDesc fault : faultMsgDescList) {
//				for (PointModel pointModel : pointModelByLoginUser) {
//					if(fault.getDevid().equals(pointModel.getPointFlag())){
//						fault.setMasterName(pointModel.getMasterName()==null?"":pointModel.getMasterName());
//						fault.setMonitorStationName(pointModel.getStationName());
//						fault.setMasterAddress(pointModel.getStationAddress());
//						fault.setProtectorType(pointModel.getHitchType()==null?"":pointModel.getHitchType());
//						fault.setType(pointModel.getType()==null?"":pointModel.getType());
//						fault.setDeviceName(pointModel.getPointName());
//						fault.setIsSolve(isSolve);
//						continue;
//					}
//				}
//			}

			for (int i = 0; i < faultMsgDescList.size(); i++) {
				for (int i1 = 0; i1 < pointModelByLoginUser.size(); i1++) {
					if(faultMsgDescList.get(i).getDevid().equals(pointModelByLoginUser.get(i1).getPointFlag())){
						faultMsgDescList.get(i).setMasterName(pointModelByLoginUser.get(i1).getMasterName()==null?"":pointModelByLoginUser.get(i1).getMasterName());
						faultMsgDescList.get(i).setMonitorStationName(pointModelByLoginUser.get(i1).getStationName());
						faultMsgDescList.get(i).setMasterAddress(pointModelByLoginUser.get(i1).getStationAddress());
						faultMsgDescList.get(i).setProtectorType(pointModelByLoginUser.get(i1).getHitchType()==null?"":pointModelByLoginUser.get(i1).getHitchType());
						faultMsgDescList.get(i).setType(pointModelByLoginUser.get(i1).getType()==null?"":pointModelByLoginUser.get(i1).getType());
						faultMsgDescList.get(i).setDeviceName(pointModelByLoginUser.get(i1).getPointName());
						faultMsgDescList.get(i).setIsSolve(isSolve);
						continue;
					}
				}
			}


			for (int i = 0; i < faultMsgDescList.size(); i++) {
				for (int i1 = 0; i1 < masterModelByLoginUser.size(); i1++) {
					if(faultMsgDescList.get(i).getDevid().equals(masterModelByLoginUser.get(i1).getMonitorFlag())){
						faultMsgDescList.get(i).setMasterName(masterModelByLoginUser.get(i1).getMasterName());
						faultMsgDescList.get(i).setMonitorStationName(masterModelByLoginUser.get(i1).getStationName());
						faultMsgDescList.get(i).setMasterAddress(masterModelByLoginUser.get(i1).getStationAddress());
						faultMsgDescList.get(i).setProtectorType("智能网关");
						faultMsgDescList.get(i).setDeviceName(masterModelByLoginUser.get(i1).getMasterName());
						faultMsgDescList.get(i).setIsSolve(isSolve);
						continue;
					}
				}
			}

//			for (FaultMsgDesc fault : faultMsgDescList) {
//				for (MasterModel masterModel : masterModelByLoginUser) {
//					if(fault.getDevid().equals(masterModel.getMonitorFlag())){
//						fault.setMasterName(masterModel.getMasterName());
//						fault.setMonitorStationName(masterModel.getStationName());
//						fault.setMasterAddress(masterModel.getStationAddress());
//						fault.setProtectorType("智能网关");
//						fault.setDeviceName(masterModel.getMasterName());
//						fault.setIsSolve(isSolve);
//						continue;
//					}
//				}getDevieStatuInfo
//			}

			redisCache.set("IndexRealFault:"+token,JSON.toJSONString(faultMsgDescList),1, TimeUnit.MINUTES);
			
		} else {
			//throw new BizException(Code.NO_DATA);
			Message.success(faultMsgDescList);
		}
		return Message.success(faultMsgDescList);
	}

	@ResponseBody
	@RequestMapping("/index/getDevieStatuInfo")
	@ApiOperation(value = "设备状态详情信息")
	public Message<List<DeviceInfoDto>> getDevieStatuInfo(@ApiParam(required = false, value = "登陆用户的信息") String token) {
		String deviceListString = redisCache.get("deviceList:"+token);
		if(StringUtils.isNotEmpty(deviceListString)){
			List<DeviceInfoDto> deviceInfoDtos = JSON.parseArray(deviceListString, DeviceInfoDto.class);
			return 	Message.success(deviceInfoDtos);
		}

		List<DeviceInfoDto> deviceList  =new ArrayList<DeviceInfoDto>();

//		List<MasterModel> masterModelList = getResponsibleDeviceService.getMasterModelByLoginUser(token);
//		List<PointModel> pointModelByLoginUser = getResponsibleDeviceService.getPointModelByLoginUser(token);

		List<MasterModel> masterModelList =  getResponsibleDeviceInnerCache.getMasterModelByLoginUser(token);
		List<PointModel> pointModelByLoginUser = getResponsibleDeviceInnerCache.getPointModelByLoginUser(token);



//		log.info("masterModelList:"+JSON.toJSON(masterModelList));
		if(masterModelList!=null&&!masterModelList.isEmpty()){
			masterModelList.stream().forEach(mm->{
				DeviceInfoDto temp  =new DeviceInfoDto();
				temp.setDevType("智能网关");
				BeanUtils.copyProperties(mm, temp);
				deviceList.add(temp);
			});
		}
		if(pointModelByLoginUser!=null&&!pointModelByLoginUser.isEmpty()){
			pointModelByLoginUser.stream().forEach(mm->{
				DeviceInfoDto temp  =new DeviceInfoDto();
					temp.setMonitorFlag(mm.getPointFlag());
					temp.setId(mm.getMasterId());
					temp.setStationName(mm.getStationName());
					temp.setMonitorFlag(mm.getPointFlag());
					temp.setStationId(mm.getStationId());
					temp.setCompanyId(mm.getCompanyId());
					temp.setCompanyName(mm.getCompanyName());
					temp.setDevType("监测设备");
				BeanUtils.copyProperties(mm, temp);
				deviceList.add(temp);
			});
		}
		
	List<String> deviceIds = deviceList.stream().map(DeviceInfoDto::getMonitorFlag).collect(Collectors.toList());

		// long start = RunTimeCount.start();
		List<device_info> deviceInfo = XWDataEntry.getDeviceInfo(deviceIds);
//		 long end = RunTimeCount.end();
//		RunTimeCount.print("XWDataEntry.getDeviceInfo:",end,start);



	 //System.out.println("deviceInfo:"+JSON.toJSONString(deviceInfo));
		for (int i = 0; i < deviceInfo.size(); i++) {
			for (int i1 = 0; i1 < deviceList.size(); i1++) {
				if(deviceInfo.get(i).getDevid().equals(deviceList.get(i1).getMonitorFlag())){
					deviceList.get(i1).setOnline(deviceInfo.get(i).isOnline());
					deviceList.get(i1).setOnlineTime(deviceInfo.get(i).getOnlineTime()==0?"":DateUtils.unixTimestampToDate(deviceInfo.get(i).getOnlineTime()));
					deviceList.get(i1).setOfflineTime(deviceInfo.get(i).getOfflineTime()==0?"":DateUtils.unixTimestampToDate(deviceInfo.get(i).getOfflineTime()));
				}else{
					deviceList.get(i1).setOnlineTime(deviceInfo.get(i).getOnlineTime()==0?"":DateUtils.unixTimestampToDate(deviceInfo.get(i).getOnlineTime()));
					deviceList.get(i1).setOfflineTime(deviceInfo.get(i).getOfflineTime()==0?"":DateUtils.unixTimestampToDate(deviceInfo.get(i).getOfflineTime()));
					deviceList.get(i1).setOnline(false);
				}
				continue;
			}
		}
//	try{
//		deviceInfo.stream().forEach(dd->{
//			deviceList.stream().forEach(mm->{
//				if(dd.getDevid().equals(mm.getMonitorFlag())){
//					mm.setOnline(dd.isOnline());
//					mm.setOnlineTime(dd.getOnlineTime()==0?"":DateUtils.unixTimestampToDate(dd.getOnlineTime()));
//					mm.setOfflineTime(dd.getOfflineTime()==0?"":DateUtils.unixTimestampToDate(dd.getOfflineTime()));
//				}else{
//					mm.setOnlineTime(dd.getOnlineTime()==0?"":DateUtils.unixTimestampToDate(dd.getOnlineTime()));
//					mm.setOfflineTime(dd.getOfflineTime()==0?"":DateUtils.unixTimestampToDate(dd.getOfflineTime()));
//					mm.setOnline(false);
//				}
//			});
//
//		});
//	}catch(Exception e){
//		logger.info("XWDataEntry.getDeviceInfo(deviceIds) is null");
//	}
		
		HashSet<DeviceInfoDto> set = new HashSet<>(deviceList);
		deviceList.clear();
		deviceList.addAll(set);

	    List<DeviceInfoDto> collect = deviceList.stream().limit(20).collect(Collectors.toList());
		//System.out.println("collect:"+JSON.toJSON(collect));

		redisCache.set("deviceList:"+token,JSON.toJSONString(collect),1, TimeUnit.MINUTES);

	return Message.success(collect);
	
	}
	
	
	@ResponseBody
	@RequestMapping("/index/getDevieStatuCount")
	@ApiOperation(value = "设备状态统计信息")
	public Message<Map<String,Object>> getDevieStatuCount(@ApiParam(required = false, value = "登陆用户的信息") String token) {
		Map<String,Object> resultMap = new HashMap<>(); 
		int sumCount=0,zhiNengWangGuanIsOnline=0,zhiNengWangGuanNotOnline=0,jianCeDeviceIsOnline=0,jianCeDeviceNotOnline=0;
		List<DeviceInfoDto> deviceList  =new ArrayList<DeviceInfoDto>();
	//	List<MasterModel> masterModelList = this.getMasterModelByLoginUser(token);
		List<MasterModel> masterModelList = getResponsibleDeviceService.getMasterModelByLoginUser(token);
		if(masterModelList!=null&&!masterModelList.isEmpty()){
			masterModelList.stream().forEach(mm->{
				DeviceInfoDto temp  =new DeviceInfoDto();
//				temp.setDevType("智能网关");
				BeanUtils.copyProperties(mm, temp);
				deviceList.add(temp);
			});
		}
		if(masterModelList==null||masterModelList.isEmpty()){
			resultMap.put("sumCount", sumCount);
			resultMap.put("zhiNengWangGuanIsOnline", zhiNengWangGuanIsOnline);
			resultMap.put("zhiNengWangGuanNotOnline", zhiNengWangGuanNotOnline);
			resultMap.put("jianCeDeviceIsOnline", jianCeDeviceIsOnline);
			resultMap.put("jianCeDeviceNotOnline", jianCeDeviceNotOnline);
			return Message.success(resultMap);
		}
	    List<String> deviceIds = deviceList.stream().map(DeviceInfoDto::getMonitorFlag).collect(Collectors.toList());
	    List<device_info> deviceInfo = XWDataEntry.getDeviceInfo(deviceIds);
		deviceInfo.stream().forEach(dd->{
			deviceList.stream().forEach(mm->{
				if(dd.getDevid().equals(mm.getMonitorFlag())){
					mm.setOnline(dd.isOnline());
					mm.setOnlineTime(dd.getOnlineTime()==0?"":DateUtils.unixTimestampToDate(dd.getOnlineTime()));
					mm.setOfflineTime(dd.getOfflineTime()==0?"":DateUtils.unixTimestampToDate(dd.getOfflineTime()));
				}else{
					mm.setOnlineTime(dd.getOnlineTime()==0?"":DateUtils.unixTimestampToDate(dd.getOnlineTime()));
					mm.setOfflineTime(dd.getOfflineTime()==0?"":DateUtils.unixTimestampToDate(dd.getOfflineTime()));
					mm.setOnline(false);
				}
			});
		});
		  for (device_info dd : deviceInfo) {
				if(dd.isOnline()){
					zhiNengWangGuanIsOnline++;
				}
			}
		HashSet<DeviceInfoDto> set = new HashSet<>(deviceList);
		deviceList.clear();
		deviceList.addAll(set);
		resultMap.put("sumCount", deviceList.size());
		resultMap.put("zhiNengWangGuanIsOnline", zhiNengWangGuanIsOnline);
		resultMap.put("zhiNengWangGuanNotOnline", deviceList.size()-zhiNengWangGuanIsOnline);
		resultMap.put("jianCeDeviceIsOnline", jianCeDeviceIsOnline);
		resultMap.put("jianCeDeviceNotOnline", jianCeDeviceNotOnline);
	return Message.success(resultMap);
	}

  /*  @ResponseBody
    @RequestMapping("/index/weather")
    public Message<?> weather(String cityCode){
        HttpClient httpClient  = new HttpClient();
        Map<String,String> map = new HashMap<>();
        map.put("city",cityCode);
        map.put("key","9e7f1a6301bfe9042cbebdb81b43d828");
        String s = httpClient.get("https://restapi.amap.com/v3/weather/weatherInfo", map);
        String unescapeJavaScript = StringEscapeUtils.unescapeJavaScript(s);
        
        return Message.success(unescapeJavaScript);
    }
*/


    @ResponseBody
	@RequestMapping("/index/test1")
	public Message<?> test1(){
		redisCache.hset("tb_student", "stu_id", "liudong");
		  String re = redisCache.hget("tb_student", "stu_id");
		return Message.success(re);
	}
			
    @ResponseBody
	@RequestMapping("/index/test11")
	public Message<?> test2221(){
		List<PointModel> pointList = SpringUtils.getBean(MonitorPointService.class).getPointModelList();
		/*
		List<MemberModel> memberModels = SpringUtils.getBean(MemberInfoDao.class).queryAllMemberByCompanyId(null);*/
	//	List<PointModel> pointList = monitorPointService.getPointModelList();
		//List<MemberModel> memberModels = memberInfoDao.queryAllMemberByCompanyId(null);

		List<MemberInfo> list = SpringUtils.getBean(MemberService.class).list();

		ExLogUtil.getLogger().info("pointList:"+JSON.toJSON(pointList));
		ExLogUtil.getLogger().info("list:"+JSON.toJSON(list));
		return Message.success("nihao");
	}
	
	
}
