package com.yscoco.dianli.controller.api;

import com.alibaba.fastjson.JSON;
import com.batise.device.eric900.eric900_data;
import com.batise.device.eric900b.eric900b_data;
import com.hyc.smart.XWDataEntry;
import com.hyc.smart.eric900b.SendMsg900b;
import com.hyc.smart.eric900b.eric900b_config;
import com.yscoco.dianli.common.aspect.SystemLog;
import com.yscoco.dianli.common.base.BaseController;
import com.yscoco.dianli.common.exception.BizException;
import com.yscoco.dianli.common.vo.Code;
import com.yscoco.dianli.common.vo.Message;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;


/**
 * @author  onion
 *2：电机eric900_DATA 3：900B eric900b_data
 */
@Controller
@RequestMapping("/sys/900b/")
public class ApiEric900bMonitorStationDeviceController extends BaseController {
	
	//private static final Logger logger = Logger.getLogger(ApiEric900bMonitorStationDeviceController.class.getName());
	

	/**
	 * 获取900b一段时间内的数据
	 * @param deviceId
	 * @param timeStart
	 * @param timeEnd
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "get900bDataList.v1")
	public Message<List<eric900b_data>> get900bDataList(
            String deviceId, Long timeStart, Long timeEnd) {
        List<eric900b_data> bDataList =null;
        try {
            if(StringUtils.isBlank(deviceId)){
                throw new BizException(Code.NOT_PARAM);
            }
            bDataList = XWDataEntry.get900bDataList(deviceId, timeStart, (timeEnd == null ? System.currentTimeMillis() : timeEnd));

        }catch (Exception e){
            throw new BizException(Code.XWDataEntry_ERROR);
        }
        return new Message<>(Code.SUCCESS,bDataList);
	}

    /**
     * 获取900b的配置信息
     * @param deviceId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "getConfig900b.v1")
    public Message<eric900b_config> getConfig900b(String deviceId) {
        eric900b_config config900b = null;
        try {
            if(StringUtils.isBlank(deviceId)){
                throw new BizException(Code.NOT_PARAM);
            }
            config900b = XWDataEntry.getConfig900b(deviceId);
        }catch (Exception e){
            throw new BizException(Code.XWDataEntry_ERROR);
        }
        System.out.println("config900b:"+JSON.toJSON(config900b));
        return new Message<>(Code.SUCCESS,config900b);
    }

    /**
     * 同步900b设置信息
     * @param data
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "sync900bConfig.v1")
    @SystemLog(module="/sys/900b/sync900bConfig",methods = "同步900b设置信息")
    public Message<Boolean> sync900bConfig(eric900b_config data) {
    	 System.out.println("eric900b_config:"+JSON.toJSONString(data));
        boolean isOk;
        try {
            if(null==data){
                throw new BizException(Code.NOT_PARAM);
            }
            isOk = XWDataEntry.Sync900bConfig(data);
        }catch (Exception e){
            throw new BizException(Code.XWDataEntry_ERROR);
        }
        System.out.println("isOk:"+isOk);
        return new Message<>(Code.SUCCESS,isOk);
    }


    /**
     /**
     * 向900b发送控制命令
     * @param deviceId
     * @param control
     *            1:复位；2:断电 3:通电
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "set900bControl.v1")
    @SystemLog(module="/sys/900b/set900bControl",methods = "向900b发送控制命令1:复位；2:断电 3:通电")
    public Message<Boolean> set900bControl(String deviceId, Byte control) {
        boolean isOk;
        System.out.println("control:"+control);
        try {
            if(StringUtils.isBlank(deviceId)||control==null||control==0){
                throw new BizException(Code.NOT_PARAM);
            }
            isOk = SendMsg900b.SetControl(deviceId, control);
        }catch (Exception e){
            throw new BizException(Code.XWDataEntry_ERROR);
        }
        System.out.println("isOk:"+isOk);
        return new Message<>(Code.SUCCESS,isOk);
    }

	public static void main(String[] args) {
	//	C871, C876,C87 m8002
//		List<Temper_data> m8002 = XWDataEntry.getTemperatureList("M8002", 0l, System.currentTimeMillis());
//		System.out.println(JSON.toJSON(m8002));
//		List<String> list= new ArrayList<String>();
//		list.add("ERIC0900191100000001");
//		List<eric900_data> last900Data = XWDataEntry.getLast900Data(list);
//		System.out.println("last900Data:"+JSON.toJSON(last900Data));
		
		
		  List<eric900_data> get900DataList = XWDataEntry.get900DataList("ERIC0900191100000001", 0, System.currentTimeMillis());
		  System.out.println("get900DataList:"+JSON.toJSON(get900DataList));

		  //  ERIC0900180900000103

        List<eric900b_data> eric0900180900000103 = XWDataEntry.get900bDataList("ERIC0900180900000103", 0, System.currentTimeMillis());
        System.out.println("eric0900180900000103:"+JSON.toJSON(eric0900180900000103));
    }
	
}
