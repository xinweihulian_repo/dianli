package com.yscoco.dianli.controller.api;

import com.yscoco.dianli.common.aspect.SystemLog;
import com.yscoco.dianli.entity.member.MemberInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import java.io.UnsupportedEncodingException;
import java.security.AlgorithmParameters;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.security.spec.InvalidParameterSpecException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.yscoco.dianli.common.base.BaseController;
import com.yscoco.dianli.common.exception.BizException;
import com.yscoco.dianli.common.handler.PageData;
import com.yscoco.dianli.common.utils.MatcheUtils;
import com.yscoco.dianli.common.vo.Code;
import com.yscoco.dianli.common.vo.Message;
import com.yscoco.dianli.common.vo.VcodeType;
import com.yscoco.dianli.constants.WxConstant;
import com.yscoco.dianli.dto.member.MemberInfoDto;
import com.yscoco.dianli.service.app.VcodeVerifyService;
import com.yscoco.dianli.service.member.MemberService;


 

/**
 * @author lingxiang
 * app用户
 */
@Controller
@RequestMapping("/api/member/")
@Api(tags="app+后台 ：1、用户操作接口",description="app用户添加+app登录+忘记密码+更新信息+查看详情+后台查询列表+push极光")
public class ApiMemberController extends BaseController{

	@Autowired
	private MemberService memberService;
	
	@Autowired
	private VcodeVerifyService vcodeVerifyService;
	

	/**
     * 解密用户信息
     */
    private static JSONObject getUserInfo(String encryptedData,String sessionkey,String iv){
        // 被加密的数据
        byte[] dataByte = Base64.decodeBase64(encryptedData);
        // 加密秘钥
        byte[] keyByte = Base64.decodeBase64(sessionkey);
        // 偏移量
        byte[] ivByte = Base64.decodeBase64(iv);
        try {
               // 如果密钥不足16位，那么就补足.  这个if 中的内容很重要
            int base = 16;
            if (keyByte.length % base != 0) {
                int groups = keyByte.length / base + (keyByte.length % base != 0 ? 1 : 0);
                byte[] temp = new byte[groups * base];
                Arrays.fill(temp, (byte) 0);
                System.arraycopy(keyByte, 0, temp, 0, keyByte.length);
                keyByte = temp;
            }
            // 初始化
            Security.addProvider(new BouncyCastleProvider());
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7Padding","BC");
            SecretKeySpec spec = new SecretKeySpec(keyByte, "AES");
            AlgorithmParameters parameters = AlgorithmParameters.getInstance("AES");
            parameters.init(new IvParameterSpec(ivByte));
            cipher.init(Cipher.DECRYPT_MODE, spec, parameters);// 初始化
            byte[] resultByte = cipher.doFinal(dataByte);
            if (null != resultByte && resultByte.length > 0) {
                String result = new String(resultByte, "UTF-8");
                return JSONObject.parseObject(result);
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidParameterSpecException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        }
        return null;
    }

	
	@ResponseBody
	@RequestMapping("login.v1")
	@ApiOperation(value="App账号密码登录",httpMethod="POST")
	public Message<MemberInfoDto> login(
			@ApiParam(required = true,  value = "账号/手机号码/eamil") String account ,
			@ApiParam(required = true,  value = "密码--必须md5加密,小写")  String password,
			String channel
			,String openId){
		 if(!StringUtils.isBlank(channel)&&WxConstant.loginChannel.equalsIgnoreCase(channel))
		    return  memberService.wxLogin(account,openId);
		 
			return  memberService.login(account, password,request.getRemoteAddr(),openId);
			
	}
    
    @ResponseBody
	@RequestMapping("/auth/phone")
	@SystemLog(module="/api/member/auth/phone",methods = "授权手机号")
    public  Message<?> authPhone(String encryptedData, String sessionKey, String iv) throws Exception {
    	Map<String, Object> map = new HashMap<>();
    	JSONObject userInfo = getUserInfo(encryptedData,sessionKey,iv);
        map.put("data", userInfo);
        return Message.success(map);
    }

	
	
	@ResponseBody
	@RequestMapping("sendSmsCode.v1")
	@ApiOperation(value="app用户发送验证码")
	public Message<?> sendSmsCode(
			@ApiParam(required = true,  value = "手机号码或者邮箱")  String mobileOrEmail,
			@ApiParam(required = true,  value = "验证类型1:注册，2:改绑定手机 3.忘记密码") String type) throws Exception{
			if(StringUtils.isEmpty(mobileOrEmail)||StringUtils.isEmpty(type)){
				return new Message<>(Code.NOT_PARAM);
			}
			if(!MatcheUtils.isMobile(mobileOrEmail)){
				return new Message<>(Code.Error_MOBILE);
			}
			if(memberService.getByUserNameOrMobileOrEmail(mobileOrEmail)==null){
				return new Message<>(Code.NOTEXIST_MOBILE);
			}
			
			if("1".equals(type) || "2".equals(type)){
				memberService.checkMobileOrEmail(mobileOrEmail);
			}
			
			VcodeType codeType = VcodeType.fromInt(Integer.valueOf(type));
			vcodeVerifyService.saveAndSendVcode(mobileOrEmail, codeType);
			return Message.success();
	}
	
	@ResponseBody
	@RequestMapping("addOrUpdateAPPUser.v1")
	@ApiOperation(value = "前台APP用户添加或更新" ,httpMethod = "POST" )
	@SystemLog(module="/api/member/addOrUpdateAPPUser",methods = "前台APP用户添加或更新")
	public Message<?> addOrUpdateAPPUser(
			@ApiParam(required = false,  value = "用户id") String memberId , 
			@ApiParam(required = false,value = "用户名   ---需要包含数字和字母8-15个 正则:(?!([a-zA-Z]+|\\d+)$)[a-zA-Z\\d]{8,15}") String userName,
			@ApiParam(required = false,value = "账号") String account, 
			@ApiParam(required = false,value = "手机号码/email") String mobile , 
			@ApiParam(required = false,value = "邮箱") String email,
			@ApiParam(required = true,value = "密码--必须md5加密,小写") String password,
			@ApiParam(required = false,value = "所属公司") String companyId,
			@ApiParam(required = false,value = "监测点ID") String monitorStationId,
			@ApiParam(required = false,value = "监测支线") String linesOnBranchId) throws Exception{
		   if(StringUtils.isBlank(getLoginManagerId())){
          	 throw new BizException(Code.NOT_LOGIN);   }
			return this.sysUserService.addOrUpdateAPPUser(memberId,account,userName,mobile,email,password,
					companyId,monitorStationId,linesOnBranchId,this.getLoginManagerId(),request.getRemoteAddr());
	}
	
	
	

	
	@ResponseBody
	@RequestMapping("updatePassword.v1")
	@ApiOperation(value="App修改密码",httpMethod="POST")
	@SystemLog(module="/api/member/updatePassword",methods = "App修改密码")
	public Message<MemberInfoDto> updatePassword(
			@ApiParam(required = true,  value = "新密码--必须md5加密,小写") String password, 
			@ApiParam(required = true,  value = "旧密码--必须md5加密,小写") String oldPassword){
			return memberService.updatePassword(password, oldPassword,this.getLoginMemberId());
	}
	

	@ResponseBody
	@RequestMapping("updatePasswordBySmsCode.v1")
	@ApiOperation(value="App根据验证码修改密码---忘记密码",httpMethod="POST")
	@SystemLog(module="/api/member/updatePasswordBySmsCode",methods = "App根据验证码修改密码---忘记密码")
	public Message<?> updatePasswordBySmsCode(
			@ApiParam(required = true,  value = "验证码") String smsCode,
			@ApiParam(required = true,  value = "手机号码或者邮箱") String mobileOrEmail,
			@ApiParam(required = true,  value = "新密码--必须md5加密,小写") String password){
			memberService.updatePasswordBySmsCode(mobileOrEmail, password, smsCode);
			return Message.success();
	}
	
	@ResponseBody
	@RequestMapping("updateMemberInfo.v1")
	@ApiOperation(value="app用户修改个人信息",httpMethod="POST")
	@SystemLog(module="/api/member/updateMemberInfo",methods = "app用户修改个人信息")
	public Message<MemberInfoDto> updateMemberInfo(
			@ApiParam(required = false,  value = "账号") String account, 
			@ApiParam(required = false,  value = "用户名") String userName, 
			@ApiParam(required = false,  value = "邮箱") String email, 
			@ApiParam(required = false,  value = "扩展属性 json格式") String setting){
			MemberInfoDto dto = new MemberInfoDto();
			dto.setId(this.getLoginMemberId());
			dto.setAccount(account);
			dto.setUserName(userName);
			dto.setEmail(email);
			dto.setSetting(setting);
			return memberService.update(dto);
	}
	
	
	@ResponseBody
	@RequestMapping("pushById.v1")
	@ApiOperation(value="保存极光Id来推送消息 ",httpMethod="POST")
	@SystemLog(module="/api/member/pushById",methods = "保存极光Id来推送消息")
	public Message<MemberInfoDto> pushById(
			@ApiParam(value = "token")   String token,
			@ApiParam(value = "推送的Id")  String pushId) throws Exception{
			return  this.memberService.pushById(token,pushId);
	}
	
	@ResponseBody
	@RequestMapping("queryMembers.v1")
	@ApiOperation(value = "App查询用户" ,httpMethod = "POST" )
	public Message<PageData<MemberInfoDto>> queryUsers(
			@ApiParam("用户Id") String memberId, 
			@ApiParam("电话号码") String mobile,
			@ApiParam("用户名称") String userName,
			@ApiParam("用户名称") String companyId,
			@ApiParam("行数") Integer rows,
			@ApiParam("页数") Integer page){
			PageData<MemberInfoDto> data = memberService.queryPage(memberId,mobile,userName,companyId,rows,page);
			return new Message<>(Code.SUCCESS,data);
	}
	
	@ResponseBody
	@RequestMapping("deleteMembers.v1")
	@SystemLog(module="/api/member/deleteMembers",methods = "后台删除app用户")
	public Message<?> deleteMembers(
			@ApiParam("用户Id") String memberIds){
			return memberService.deleteMembers(memberIds);
	}


}
