package com.yscoco.dianli.controller.api;

import com.alibaba.fastjson.JSON;
import com.batise.device.eric900.eric900_data;
import com.batise.device.motor.Motor_data;
import com.batise.device.temper.Temper420_data;
import com.batise.device.temper.Temper_data;
import com.hyc.smart.XWDataEntry;
import com.yscoco.dianli.common.aspect.SystemLog;
import com.yscoco.dianli.dto.company.MonitorStationDeviceDto;
import com.yscoco.dianli.dto.company.MonitorStationDianJiDto;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.yscoco.dianli.common.base.BaseController;
import com.yscoco.dianli.common.exception.BizException;
import com.yscoco.dianli.common.vo.Code;
import com.yscoco.dianli.common.vo.Message;
import com.yscoco.dianli.common.vo.PageData;
import com.yscoco.dianli.controller.websocket.MsgScoketHandle;
import com.yscoco.dianli.entity.company.MonitorStationDevice;
import com.yscoco.dianli.entity.sys.SysUser;
import com.yscoco.dianli.form.ImportSensorForm;
import com.yscoco.dianli.form.ParseSeneorData;
import com.yscoco.dianli.service.company.MonitorStationDeviceService;

import cn.hutool.poi.excel.ExcelReader;
import cn.hutool.poi.excel.ExcelUtil;

import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toCollection;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author  onion
 *2：电机eric900_DATA 3：900B eric900b_data  testTemper_420DataPush
 */
@Controller
@RequestMapping("/sys/monitorStationDevice/")
public class ApiMonitorStationDeviceController extends BaseController {
	
	private static final Logger logger = Logger.getLogger(ApiMonitorStationDeviceController.class.getName());
	
	@Autowired
	private MonitorStationDeviceService  monitorStationDeviceService;

	
	/**
	 * 耐张线夹类型的监测站，添加监测设备增加一个批量导入的功能，主要参数：所属位置，监测设备名称，监测设备标识，传感器类型（默认磁吸附式C）
	 * @param formRequest
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 */
        @ResponseBody
	    @RequestMapping(value = "upload", method = {RequestMethod.GET, RequestMethod.POST})
		@SystemLog(module="/sys/monitorStationDevice/upload",methods = "耐张线夹类型的监测站，添加监测设备批量导入")
	    public Message<?> readExcel(MonitorStationDevice formRequest, HttpServletRequest request, HttpServletResponse response) throws IOException {
	       // SysUser user = this.getSySuserByToken(form.getToken());
	        InputStream in = null;
	        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
	        List<MonitorStationDevice> parseSeneorDataList = null;
	        try {
	        	 MultipartFile file = multipartRequest.getFile("upfile"); 
	             in = file.getInputStream();
	             //ExcelUtil.readBySax(in, -1, createRowHandler());
	             ExcelReader reader = ExcelUtil.getReader(in);
	             parseSeneorDataList = reader.readAll(MonitorStationDevice.class);
	        } catch (Exception e) {
	        	System.err.print( e.getMessage());
	            throw new BizException(Code.EXCEL_NOTEXIST);
	        } finally {
	            if (in != null)
	                try {
	                    in.close();
	                } catch (IOException e) {
	                    throw new BizException(Code.SYS_ERR);
	                }
	        }
	        parseSeneorDataList = checkParseSeneorDataFormat(parseSeneorDataList);
	        addSeneorData(parseSeneorDataList,formRequest);
	        return Message.success();
	    }
	
        private List<MonitorStationDevice> checkParseSeneorDataFormat(List<MonitorStationDevice> form) {
        	form.forEach(m -> {
                if (StringUtils.isEmpty(m.getDeviceId())) {
                    throw new BizException(Code.EXCEL_FORMATERROR);
                }
            });
        	form = form.stream().collect(collectingAndThen(toCollection(() -> new TreeSet<>(Comparator.comparing(MonitorStationDevice::getDeviceId))), ArrayList::new));
        	return form;
        }
        
        private void addSeneorData(List<MonitorStationDevice>  form,MonitorStationDevice formRequest) {
			 Date date = new Date();
			 for (int i = 0; i < form.size(); i++) {
				 date.setTime(date.getTime()+i);
				 form.get(i).setStationType("2");
				 form.get(i).setDeviceType("磁吸附式C");
				 form.get(i).setStationId(formRequest.getStationId());
				 form.get(i).setCreateTime(date);
				 System.out.println(form.get(i));
				 MonitorStationDevice byDeviceId = monitorStationDeviceService.getByDeviceId(form.get(i).getDeviceId());
				 if(null!=byDeviceId)
					 monitorStationDeviceService.del(byDeviceId.getId());

				 monitorStationDeviceService.add(form.get(i));

			 }

		 }
        
        

	@ResponseBody
	@RequestMapping(value = "list.v1")
	public Message<PageData<?>> queryMonitorStationDevice(
		 String stationId,
		 Integer page,
		 Integer limit) {
		//PageData<MonitorStationDeviceDto> listPage = monitorStationDeviceService.listPage(stationId,page,limit);
		PageData<MonitorStationDevice> listPage = monitorStationDeviceService.listPagePuls(stationId,page,limit);
		//System.err.println(JSON.toJSON(listPage));
		return new Message<>(Code.SUCCESS, listPage);
	}


	@ResponseBody
	@RequestMapping(value = "addOrUpdate.v1")
	@SystemLog(module="/sys/monitorStationDevice/addOrUpdate",methods = "新增或修改")
	public Message<MonitorStationDevice> addOrEdit(MonitorStationDevice monitorStationDevice) {
		MonitorStationDevice res = monitorStationDeviceService.addOrUpdate( monitorStationDevice);
	    System.out.println(JSON.toJSON(monitorStationDevice));
		return new Message<>(Code.SUCCESS, res);
	}
	
	@ResponseBody
	@RequestMapping(value = "delete.v1")
	@SystemLog(module="/sys/monitorStationDevice/delete",methods = "删除")
	public Message<MonitorStationDevice> delete(String id) {
		 monitorStationDeviceService.del(id);
		return new Message<>(Code.SUCCESS);
	}


	/**
	 *  获取耐张佳表格
	 * @param stationId
	 * @param page
	 * @param limit
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "table.v1")
	public Message<PageData<MonitorStationDeviceDto>> getTableData(
			String stationId,
			Integer page,
			Integer limit) {
		PageData<MonitorStationDeviceDto> listPage = monitorStationDeviceService.listPage(stationId,page,limit);
		List<MonitorStationDeviceDto> list = listPage.getList();
		if(list==null){
			return new Message<>(Code.SUCCESS, listPage);
		}
		return new Message<>(Code.SUCCESS, listPage);
	}

	@ResponseBody
	@RequestMapping(value = "tableData.v1")
	public Message<List<Temper_data> > getLastTemperDatas(
			String deviceIds) {
		if(StringUtils.isNotBlank(deviceIds)){
			List<String>	result = Arrays.asList(deviceIds.split(","));
			List<Temper_data> lastTemperData = XWDataEntry.getLastTemperData(result);
			System.out.println(JSON.toJSON(lastTemperData));
			return new Message<>(Code.SUCCESS, lastTemperData);
		}
		return new Message<>(Code.SUCCESS, new ArrayList<>());
	}


	/**
	 *  获取耐张佳表格内 数据
	 * @param stationId
	 * @param page
	 * @param rows
	 * @return
	 */
	/*@ResponseBody
	@RequestMapping(value = "tableData.v1")
	public Message<PageData<MonitorStationDeviceDto>> getLastTemperData(
		 String stationId,
		 Integer page,
		 Integer limit) {
		PageData<MonitorStationDeviceDto> listPage = monitorStationDeviceService.listPage(stationId,page,limit);
		List<MonitorStationDeviceDto> list = listPage.getList();
		if(list==null){
			return new Message<>(Code.SUCCESS, listPage);
		}
		List<String> deviceIds = list.stream().map(MonitorStationDeviceDto->MonitorStationDeviceDto.getDeviceId()).collect(Collectors.toList());
		
		//long start = System.currentTimeMillis();
		List<Temper_data> lastTemperData = XWDataEntry.getLastTemperData(deviceIds);
		//long end = System.currentTimeMillis();
		
		//System.out.println("XWDataEntry.getLastTemperData(deviceIds)耗时:" + (end - start) + "ms");
		
		if(lastTemperData!=null){
			for (int i = 0; i < lastTemperData.size(); i++) {
				for (int i1 = 0; i1 < list.size(); i1++) {
					if(	lastTemperData.get(i).getSensorId().equals(list.get(i1).getDeviceId())){
						list.get(i1).setTemper(lastTemperData.get(i).getTemper());
						break;
                  }
				}
			}
		}
		

		return new Message<>(Code.SUCCESS, listPage);
	}*/
	
	
	
	
	
	/**
	 * 
	 * 耐张夹设备曲线数据
	 * @param sensorId
	 * @param timeStart
	 * @param timeEnd
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "getTemperatureList.v1")
	public Message<List<Temper_data>> getTemperatureList(
			@RequestParam(required = true)String sensorId, 
			@RequestParam(required = false,defaultValue = "0")Long timeStart,
			Long timeEnd) {
		List<Temper_data> temperatureList = XWDataEntry.getTemperatureList(sensorId, timeStart, (timeEnd==null?System.currentTimeMillis():timeEnd));
		return new Message<>(Code.SUCCESS,temperatureList);
	}
	
	
	//public static synchronized List<eric900_data> getLast900Data(List<String> Ids)
	/**
	 *  获取电机数据表格
	 * @param stationId
	 * @param page
	 * @param limit
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "tableDataBydianJi.v1")
	public Message<PageData<?>> getLast900Data(
		 String stationId,
		 Integer page,
		 Integer limit) {
        PageData<MonitorStationDianJiDto> res = new PageData<>();
		PageData<MonitorStationDeviceDto> listPage = monitorStationDeviceService.listPage(stationId,page,limit);
		List<MonitorStationDianJiDto> dtoList = new ArrayList<>();
        List<MonitorStationDeviceDto> list = listPage.getList();
        if(list==null||list.size()==0){
        	    res.setList(dtoList);
		        res.setCount(listPage.getCount());
				return new Message<>(Code.SUCCESS, res);
		}
		List<String> deviceIds = list.stream().map(MonitorStationDeviceDto->MonitorStationDeviceDto.getDeviceId()).collect(Collectors.toList());
		
		//long start = System.currentTimeMillis();
		 List<eric900_data> last900Data = XWDataEntry.getLast900Data(deviceIds);
		// System.err.println("XWDataEntry.last900Data" +JSON.toJSONString(last900Data));
		 if(last900Data==null||last900Data.size()==0){
			    res.setList(dtoList);
		        res.setCount(listPage.getCount());
				return new Message<>(Code.SUCCESS, res);
		}
		//long end = System.currentTimeMillis();
        //System.out.println("XWDataEntry.getLastTemperData(deviceIds)耗时:" + (end - start) + "ms");
        MonitorStationDianJiDto dto = null;
        for (int i = 0; i < last900Data.size(); i++) {
            dto =   new MonitorStationDianJiDto();
            BeanUtils.copyProperties(last900Data.get(i),dto);
            dtoList.add(dto);
        }

        List<MonitorStationDianJiDto> dtoTemp = new ArrayList<>();

        for (MonitorStationDeviceDto m : list) {
            for (MonitorStationDianJiDto d : dtoList) {
                if(m.getDeviceId().equals(d.getDevid())){
                    d.setDeviceId(m.getDeviceId());
                    d.setDeviceName(m.getDeviceName());
                    d.setPosition(m.getPosition());
                    d.setStationType(m.getStationType());
                    d.setStationId(m.getStationId());
                }else {
                	dto = new MonitorStationDianJiDto();
                    BeanUtils.copyProperties(m,dto);
                    dtoTemp.add(dto);
                }
            }
        }

        dtoList.addAll(dtoTemp);
        res.setList(dtoList);
        res.setCount(listPage.getCount());
		return new Message<>(Code.SUCCESS, res);
	}

    /**
     *public static synchronized List<eric900_data> getEric900DataList(String sensorId, long timeStart, long timeEnd)
     * 电机设备曲线数据
     * @param sensorId
     * @param timeStart
     * @param timeEnd
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "getEric900DataList.v1")
    public Message<?> getEric900DataList(
            @RequestParam(required = true)String sensorId,
            @RequestParam(required = false,defaultValue = "0")Long timeStart,
            Long timeEnd) {
        List<eric900_data> get900DataList = XWDataEntry.get900DataList(sensorId, timeStart, (timeEnd==null?System.currentTimeMillis():timeEnd));
       return new Message<>(Code.SUCCESS,get900DataList);
    }
	
	
	
	
	/**
	 * 
	 * 耐张夹设备曲线数据
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "test")
	public void test() {
		List<MonitorStationDevice> list = monitorStationDeviceService.list();
		   Map <String,Object> puhsMap = new HashMap<>();
		   Map <String,Object> dianJi = new HashMap<>();
		  
		try {
            for(int i=0;i<400000;i++){
                Thread.sleep(5000);
                list.forEach(m->{
                    Temper_data data =  new  Temper_data();
                    data.setSensorId(m.getDeviceId());
                    //data.setId(((int)Math.random() * 100000) % 100);
                    data.setTimestamp(System.currentTimeMillis());
                    data.setTemper( ((float)Math.random() * 100000) % 100);
                 
                    puhsMap.put("data",data);
                    puhsMap.put("dev_type",1);
                    MsgScoketHandle.sendMessage(data.getSensorId(), JSON.toJSONString(puhsMap));
                    MsgScoketHandle.sendMessage("A90255", JSON.toJSONString(puhsMap));
					logger.info("Temper_data："+JSON.toJSONString(puhsMap));

                    //O1156   电机的
					Motor_data  motor_data  = new Motor_data();
						motor_data.setSensorId("O1156");
						motor_data.setBattery(1f);
						motor_data.setId(110);
						motor_data.setSpeedX(2f);
						motor_data.setSpeedY(3f);
						motor_data.setSpeedZ(4f);
						motor_data.setTemper(5);
						motor_data.setTimestamp(1005556666);
						dianJi.put("data",motor_data);
						dianJi.put("dev_type",4);

					MsgScoketHandle.sendMessage(motor_data.getSensorId(), JSON.toJSONString(dianJi));
                    
                    logger.info("Motor_data："+JSON.toJSONString(dianJi));
                });
            }
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block eric900_data
			e.printStackTrace();
		}
	}

    /**
     *
     * 耐张夹设备曲线数据
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "testdianji")
    public void testdianji() {
        List<MonitorStationDevice> list = monitorStationDeviceService.list();
        try {
            for(int i=0;i<40;i++){
                Thread.sleep(5000);
                list.forEach(m->{
                    eric900_data data =  new  eric900_data();
	                    data.setDevid(m.getDeviceId());
	                    data.setId(((int)Math.random() * 100000) % 100);
	                    data.setTimestamp(System.currentTimeMillis());
	                    data.setElec_a(((float)Math.random() * 100000) % 100);
	                    data.setElec_b(((float)Math.random() * 100000) % 100);
	                    data.setElec_c(((float)Math.random() * 100000) % 100);
	                    data.setElec_z(((float)Math.random() * 100000) % 100);
	
	                    data.setGravity_x(((float)Math.random() * 100000) % 100);
	                    data.setGravity_y(((float)Math.random() * 100000) % 100);
	                    data.setGravity_z(((float)Math.random() * 100000) % 100);
	
	                    data.setPower_a((byte) 1);
	                    data.setPower_b((byte) 2);
	                    data.setPower_c((byte) 3);
	
	                    data.setVol_a(((float)Math.random() * 100000) % 100);
	                    data.setVol_b(((float)Math.random() * 100000) % 100);
	                    data.setVol_c(((float)Math.random() * 100000) % 100);
	
	                    data.setTemp_a(((float)Math.random() * 100000) % 100);
	                    data.setTemp_b(((float)Math.random() * 100000) % 100);
	                    data.setTemp_c(((float)Math.random() * 100000) % 100);

                    MsgScoketHandle.sendMessage(data.getDevid(), JSON.toJSONString(data));
                    logger.info(JSON.toJSONString(data));
                });
            }
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block eric900_data
            e.printStackTrace();
        }
    }

    @ResponseBody
    @RequestMapping(value = "testTemper_420DataPush")
    public void test1() {

        try {
            for(int i=0;i<4;i++){
                Thread.sleep(5000);
                    Temper420_data data =  new  Temper420_data();
                    //data.setId(((int)Math.random() * 100000) % 100);
                    data.setTimestamp(System.currentTimeMillis());
                    data.setAverageTemper(45f);
                    data.setSensorId("ERIC0408200400000001");
                    data.setPathBitmap("1596159571240.jpg");
                    //data.setTemper( ((float)Math.random() * 100000) % 100);
                    MsgScoketHandle.sendMessage("ERIC0408200400000001", JSON.toJSONString(data));
                    logger.info(JSON.toJSONString(data));

            }
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block eric900_data
            e.printStackTrace();
        }
    }


	public static void main(String[] args) {
	//	C871, C876,C87 m8002
//		List<Temper_data> m8002 = XWDataEntry.getTemperatureList("M8002", 0l, System.currentTimeMillis());
//		System.out.println(JSON.toJSON(m8002));
//		List<String> list= new ArrayList<String>();
//		list.add("ERIC0900191100000001");
//		List<eric900_data> last900Data = XWDataEntry.getLast900Data(list);
//		System.out.println("last900Data:"+JSON.toJSON(last900Data));
		
		
		  List<eric900_data> get900DataList = XWDataEntry.get900DataList("ERIC0900191100000001", 0, System.currentTimeMillis());
		  System.out.println("get900DataList:"+JSON.toJSON(get900DataList));
	}
	
}
