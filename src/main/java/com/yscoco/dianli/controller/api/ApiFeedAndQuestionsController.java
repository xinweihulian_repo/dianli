package com.yscoco.dianli.controller.api;

import com.yscoco.dianli.common.aspect.SystemLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.yscoco.dianli.common.base.BaseController;
import com.yscoco.dianli.common.exception.BizException;
import com.yscoco.dianli.common.vo.Code;
import com.yscoco.dianli.common.vo.Message;
import com.yscoco.dianli.common.vo.PageData;
import com.yscoco.dianli.dto.option.FeedbackDto;
import com.yscoco.dianli.dto.option.QuestionsDto;
import com.yscoco.dianli.service.option.FeedbackService;
import com.yscoco.dianli.service.option.QuestionsService;


@Controller
@RequestMapping("/api/feedAndQuests/")
@Api(tags="app+后台 ：4、意见反馈",description="常见问题、意见反馈", produces = MediaType.APPLICATION_JSON_VALUE)
public class ApiFeedAndQuestionsController extends BaseController{

	@Autowired
	private QuestionsService questionsService;
	
	@Autowired
	private FeedbackService feedbackService;

	@ResponseBody
	@RequestMapping("addOrUpdateFeedback.v1")
	@SystemLog(module="/api/feedAndQuests/addOrUpdateFeedback",methods = "意见反馈新增和反馈")
	public Message<?> addOrUpdateFeedback(
			@ApiParam(required = false, value = "反馈信息Id") String feedbackId, 
			@ApiParam(required = false, value = "反馈内容") String content, 
			@ApiParam(required = false, value = "回复内容") String reply, 
			@ApiParam(required = false, value ="联系人邮箱或手机") String mobileOremail){
			return this.feedbackService.addOrUpdateQtBack(feedbackId,content,reply,mobileOremail,(feedbackId != null && feedbackId.length()>=0)?this.getLoginManagerId():this.getLoginMemberId(),request.getRemoteAddr());
	}
	
	
	@ResponseBody
	@RequestMapping("queryFeedback.v1")
	@ApiOperation(value="查询意见反馈列表",httpMethod="POST")
	public Message<PageData<FeedbackDto>> queryFeedback(
			@ApiParam(required = false, value = "反馈信息Id") String feedbackId, 
			@ApiParam("行数") Integer rows,
			@ApiParam("页数") Integer page){
			PageData<FeedbackDto> list = feedbackService.listPage(page,rows,feedbackId);
			return Message.success(list);
	}
	

	@ResponseBody
	@RequestMapping("deleteFeedback.v1")
	@SystemLog(module="/api/feedAndQuests/deleteFeedback",methods = "意见反馈删除")
	public Message<?> deleteFeedback(
			@ApiParam(required = false,  value = "意见反馈列表ids") String feedBackIds){
		   if(StringUtils.isBlank(getLoginManagerId())){
          	 throw new BizException(Code.NOT_LOGIN);   }
			this.feedbackService.delete(feedBackIds,this.getLoginManagerId(),request.getRemoteAddr());
			return new Message<>(Code.SUCCESS);
	}
	
//  ======================  消息列表   ======================
	
	@ResponseBody
	@RequestMapping("queryQuestions.v1")
	@ApiOperation(value="问题反馈列表",httpMethod="POST")
	public Message<PageData<QuestionsDto>> queryQuestions(
			@ApiParam(required = false, value = "问题反馈Id") String questionsId, 
			@ApiParam("行数") Integer rows,
			@ApiParam("页数") Integer page){
			PageData<QuestionsDto> list = questionsService.listPage(page,rows,questionsId);
			return Message.success(list);
	}
	

	@RequestMapping("getQuestions.v1")
	@ApiOperation(value="问题反馈 ， 反馈网页html页面",httpMethod="POST")
	public ModelAndView get(String id,ModelMap map){
		QuestionsDto qt = questionsService.get(id);
		   map.put("qt", qt);
		   return new ModelAndView("qt",map);
	}
	
	
	@ResponseBody
	@RequestMapping("deleteQuestions.v1")
	@SystemLog(module="/api/feedAndQuests/deleteQuestions",methods = "问题反馈删除")
	@ApiOperation(value = "问题反馈删除" ,httpMethod = "POST" )
	public Message<?> deleteNews(
			@ApiParam(required = false,  value = "问题反馈ids") String questionIds){
			this.questionsService.delete(questionIds);
			return new Message<>(Code.SUCCESS);
	}
	
	
}
