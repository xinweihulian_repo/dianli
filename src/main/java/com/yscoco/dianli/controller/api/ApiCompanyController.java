package com.yscoco.dianli.controller.api;

import com.yscoco.dianli.common.aspect.SystemLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.yscoco.dianli.common.base.BaseController;
import com.yscoco.dianli.common.exception.BizException;
import com.yscoco.dianli.common.vo.Code;
import com.yscoco.dianli.common.vo.Message;
import com.yscoco.dianli.common.vo.PageData;
import com.yscoco.dianli.dto.company.CompanyDto;
import com.yscoco.dianli.other.cache.RedisCacheUtil;
import com.yscoco.dianli.service.company.CompanyService;

@Controller
@RequestMapping("/sys/company/")
@Api(tags="后台业务:1、公司管理",description="公司增删查改", produces = MediaType.APPLICATION_JSON_VALUE,position=1)
public class ApiCompanyController extends BaseController {
	
	private static final Logger logger = Logger.getLogger(ApiCompanyController.class.getName());
	 @Resource
    private RedisCacheUtil redisCache;
	@Autowired
	private CompanyService companyService;
	
	@ResponseBody
	@RequestMapping(value="queryCompany.v1") 
	@ApiOperation(value = "公司查询")
	public Message<PageData<CompanyDto>> queryCompany(
			@ApiParam(required = false,  value = "公司Id") String companyId, 
			@ApiParam(required = false,  value = "公司名称查询") String companyName,
			@ApiParam(required = false,  value = "创建公司人Id") String createBy,
			@ApiParam(required = false,  value = "页数") Integer page, 
			@ApiParam(required = false,  value = "行数") Integer rows) {
			String loginMemberId = this.getLoginMemberId();
			String loginManagerId = this.getLoginManagerId();
			if(loginMemberId==null&&loginManagerId==null){
				throw new BizException(Code.NOT_LOGIN);
			}
		
		//	String loginManagerId = this.getLoginManagerId();
			//long start,end;
		//	start = System.currentTimeMillis();
			//PageData<CompanyDto> pageData = companyService.listPage(companyId,companyName,createBy,page,rows);
				PageData<CompanyDto> pageData = companyService.getCompanylistPage(companyId,companyName,createBy,page,rows);

		//	end = System.currentTimeMillis();
		//	System.out.println("queryCompany.v1:"+"start time:" + start+ "; end time:" + end+ "; Run Time:" + (end - start) + "(ms)");
			return Message.success(pageData);
	}
	
	
	@ResponseBody
	@RequestMapping("addOrUpdateCompany.v1")
	@SystemLog(module="/sys/company/addOrUpdateCompany",methods = "公司新增或添加")
	public Message<CompanyDto> addOrUpdateCompany(
			@ApiParam(required = false,  value = "公司Id") String companyId, 
			@ApiParam(required = false,  value = "公司名称") String companyName,
			@ApiParam(required = false,  value = "公司名称简称") String companyNameShort,
			@ApiParam(required = false,  value = "公司地址") String companyAddress,
			@ApiParam(required = false,  value = "公司所属行业") String industry,
			@ApiParam(required = false,  value = "公司性质:私营 ,国营, 合资") String businessType,
			@ApiParam(required = false,  value = "公司规模:0-20,TWO 100以上 ") String companySize,
			@ApiParam(required = false,  value = "主要产品以及产能") String mainProducts,
			@ApiParam(required = false,  value = "公司的监测站Ids") String monitorStationsIds){
	
             if(StringUtils.isBlank(getLoginManagerId())){
            	 throw new BizException(Code.NOT_LOGIN);   }
		
			return companyService.addOrUpdateCompany(companyId,companyName,companyNameShort,
			companyAddress,industry,businessType,companySize,mainProducts,monitorStationsIds,
			this.getLoginManagerId(),request.getRemoteAddr());
	}
	 
	
	@ResponseBody
	@RequestMapping("deleteCompany.v1")
	@SystemLog(module="/sys/company/deleteCompany",methods = "公司删除")
	public Message<?> deleteCompany(
			@ApiParam(required = false,  value = "公司Id") String companyIds) {
		   if(StringUtils.isBlank(getLoginManagerId())){
          	 throw new BizException(Code.NOT_LOGIN);   }
			return companyService.delete(companyIds,this.getLoginManagerId(),request.getRemoteAddr());
	}
	
	
	// 公司相关统计
	@ResponseBody
	@RequestMapping(value="queryStaticApp.v1") 
	@ApiOperation(value = "查询App本公司的导航统计数据")
	public Message<CompanyDto> queryStaticApp(
			@ApiParam(required = false,  value = "公司Id：后台管理则不需要传,查得所有公司的监测站等信息") String companyId) {
		    String loginMemberId = this.getLoginMemberId();
			CompanyDto companyDto = companyService.queryStaticApp(companyId,loginMemberId);
			return Message.success(companyDto);
	}
	
	// 后台所有公司相关统计
	@ResponseBody
	@RequestMapping(value="queryStaticByBack.v1") 
	@ApiOperation(value = "查询后台所有公司的导航统计数据" )
	public Message<List<CompanyDto>> queryStaticByBack(
			@ApiParam(required = false,  value = "创建Id") String createBy) {
			List<CompanyDto> result = new ArrayList<CompanyDto>();
			List<String> queryAllCompanyId = this.companyService.queryAllCompanyId(createBy);
			CompanyDto companyDto = null ;
			for (String companyId : queryAllCompanyId) {
				 companyDto = companyService.queryStaticApp(companyId,null);
				 result.add(companyDto);
			}
			return Message.success(result);
	}
	
	
	// 统计信息：下拉框拼接
	@ResponseBody
	@RequestMapping(value="queryStaticThree.v1") 
	public Message<List<CompanyDto>> queryStaticThree(
			@ApiParam(required = false,  value = "公司Id") String companyId,
			@ApiParam(required = false,  value = "创建人Id") String createBy,
			@ApiParam(required = false,  value = "type:3级联动(3),5级联动(5)") String type) {
			String loginMemberId = this.getLoginMemberId();
			String loginManagerId = this.getLoginManagerId();
			if(loginMemberId==null&&loginManagerId==null){
				throw new BizException(Code.NOT_LOGIN);
			}
			List<CompanyDto> queryStaticThree = this.companyService.queryStaticThree(companyId,createBy,type);
			return Message.success(queryStaticThree);
			
	}

	// 统计信息：下拉框拼接
	@ResponseBody
	@RequestMapping(value="queryStaticThree.v2")
	public Message<List<CompanyDto>> queryStaticThree2(
			@ApiParam(required = false,  value = "公司Id") String companyId,
			@ApiParam(required = false,  value = "创建人Id") String createBy,
			@ApiParam(required = false,  value = "type:3级联动(3),5级联动(5)") String type) {
		boolean flag=false;
	 if(companyId==null||StringUtils.isEmpty(companyId)){
		 companyId=createBy;
		 flag=true;
	 }
		List<CompanyDto> queryStaticThree =null;
		  String re = redisCache.hget(createBy, companyId);
		  if(re!=null){
			     queryStaticThree = JSON.parseArray(re,CompanyDto.class);
		  }else {
			  long start = System.currentTimeMillis();
				String loginMemberId = this.getLoginMemberId();
				String loginManagerId = this.getLoginManagerId();
				if(loginMemberId==null&&loginManagerId==null){
					throw new BizException(Code.NOT_LOGIN);
				}
				if(flag){
					companyId=null;
				}
				 queryStaticThree = this.companyService.queryStaticThree2(companyId,createBy,type);
				long end = System.currentTimeMillis();
				if(flag){
					companyId=createBy;
				}
				logger.info("queryStaticThree.v2:" + (end - start) + "ms");
				redisCache.hset(createBy, companyId, JSON.toJSONString(queryStaticThree));
				
		  }

		return Message.success(queryStaticThree);

	}


	
}
