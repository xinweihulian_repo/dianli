package com.yscoco.dianli.controller.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yscoco.dianli.common.base.BaseController;
import com.yscoco.dianli.common.handler.PageData;
import com.yscoco.dianli.common.vo.Code;
import com.yscoco.dianli.common.vo.Message;
import com.yscoco.dianli.dto.msg.MsgPushDto;
import com.yscoco.dianli.other.push.PushClientI;
import com.yscoco.dianli.service.app.AppLogService;
import com.yscoco.dianli.service.msg.MsgPushService;

@Controller
@RequestMapping("/api/msg/")
@Api(tags="app+后台 ：3、消息",description="增删查看+是否阅读+定时推送")
public class ApiMsgPushController extends BaseController{
	/*
	@Autowired
	private Timer pushTipTimer;*/
	
	@Autowired
	private PushClientI pushClientI;
	
    @Autowired
    private MsgPushService msgPushService;
    
    @Autowired
    private AppLogService appLogService;
 
    @ResponseBody
    @RequestMapping("queryMsg.v1")
    @ApiOperation(value="查询消息列表",httpMethod="POST")
    public Message<PageData<MsgPushDto>> queryMsg(
    		@ApiParam("消息id:查看详情")String msgId,
    		@ApiParam("消息id:仅看自己的")String loginId,
            @ApiParam("页码")Integer page,
            @ApiParam("页数")Integer rows){
	    	PageData<MsgPushDto> pageData = msgPushService.listPage(msgId,loginId,page,rows);
			return Message.success(pageData);
			
    }
    
    
    @ResponseBody
    @RequestMapping("deletemsg.v1")
    @ApiOperation(value="删除消息",httpMethod="POST")
    public Message<String> deletemsg(
            @ApiParam("消息id,多个用英文逗号隔开")String ids){
	        String loginId = this.getLoginMemberId();
	        msgPushService.delete(ids,loginId,this.getLoginManagerId(),request.getRemoteAddr());
	        return Message.success();
    }
    
    
    @ResponseBody
    @RequestMapping("readMsg.v1")
    @ApiOperation(value="查看消息",httpMethod="POST")
    public Message<String> readMsg(
            @ApiParam("消息id")String id){
	        String loginId = this.getLoginMemberId();
	        msgPushService.view(id,loginId,this.getLoginManagerId(),request.getRemoteAddr());
	        return Message.success();
    }
    
    
    //   ===============    系统功能   ================
    @ResponseBody
	@RequestMapping("push.v1")
    @ApiOperation(value="推送消息",httpMethod="POST")
	public Message<?> push(
			@ApiParam("类型")String type,
			@ApiParam("消息Id")String  id){
		 Map<String,String> extra = new HashMap<String, String>();
		 extra.put("type", type);
		 pushClientI.push(id, null, "亲，您已经有两天未登陆【THL穿戴】了哦，赶紧上去看看您的健康状态吧！", extra);
		 return new Message<>(Code.SUCCESS);
	}
    
    
	/*@ResponseBody
	@RequestMapping("test.v1")
	public Message<?> test(String type){
		switch (type) {
		case "1":
			pushTipTimer.pushTwodayNotlogin();
			break;
		case "2":
			pushTipTimer.pushEverydayCheckblood();
			break;
		case "3":
			pushTipTimer.pushFriendNotsync();
			break;
		case "4":
			pushTipTimer.saveMemerHistory();
			break;
		default:
			break;
		}
		return new Message<>(Code.SUCCESS);
	}
    */
}
