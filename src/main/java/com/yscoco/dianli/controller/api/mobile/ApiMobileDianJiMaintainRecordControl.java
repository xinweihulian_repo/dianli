package com.yscoco.dianli.controller.api.mobile;


import com.yscoco.dianli.common.aspect.SystemLog;
import com.yscoco.dianli.common.base.BaseController;
import com.yscoco.dianli.common.exception.BizException;
import com.yscoco.dianli.common.vo.Code;
import com.yscoco.dianli.common.vo.Message;
import com.yscoco.dianli.common.vo.PageData;
import com.yscoco.dianli.entity.company.DianJiMaintainRecord;
import com.yscoco.dianli.service.company.DianJiMaintainService;
import com.yscoco.dianli.service.company.MonitorPointService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;



/**
 * @author onion
 * @date 2019/10/26
 */
@Controller
@RequestMapping("/dianji/mobile/maintain")
public class ApiMobileDianJiMaintainRecordControl extends BaseController{
	
	@Autowired
	private MonitorPointService monitorPointService;
	@Autowired
	private DianJiMaintainService dianJiMaintainService ;


	@ResponseBody
	@RequestMapping("list.v1")
	public Message<PageData<DianJiMaintainRecord>> list(String maintainTitle, String deviceId, Integer page, Integer rows){
		if(StringUtils.isEmpty(deviceId))
			throw new BizException(Code.NOT_PARAM);
		 PageData<DianJiMaintainRecord> dianJiMaintainRecordPageData = dianJiMaintainService.listPage(maintainTitle, deviceId, page, rows);
		return Message.success(dianJiMaintainRecordPageData);
	}


	@ResponseBody
	@RequestMapping("addOrUpdate.v1")
	@SystemLog(module="/dianji/mobile/maintain/addOrUpdate",methods = "addOrUpdate")
	public Message<DianJiMaintainRecord> addOrUpdate(DianJiMaintainRecord from,String token){
		 DianJiMaintainRecord dianJiMaintainRecord = dianJiMaintainService.addOrUpdate(from,token);
		return Message.success(dianJiMaintainRecord);
	}


	@ResponseBody
	@RequestMapping("delete.v1")
	@SystemLog(module="/dianji/mobile/maintain/delete",methods = "addOrUpdate")
	public Message<?> delete(String id){
		 if(dianJiMaintainService.del(id)==0)
		 	return Message.newError("删除失败");
		return Message.success();
	}






}
