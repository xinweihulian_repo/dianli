package com.yscoco.dianli.controller.api;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.yscoco.dianli.common.aspect.SystemLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.batise.device.device_info;
import com.hyc.smart.XWDataEntry;
import com.yscoco.dianli.common.base.BaseController;
import com.yscoco.dianli.common.exception.BizException;
import com.yscoco.dianli.common.vo.Code;
import com.yscoco.dianli.common.vo.Message;
import com.yscoco.dianli.common.vo.PageData;
import com.yscoco.dianli.dto.company.MonitorPointDto;
import com.yscoco.dianli.dto.company.MonitorStationDto;
import com.yscoco.dianli.service.company.MonitorStationService;
import com.yscoco.dianli.service.member.MemberService;

@Controller
@RequestMapping("/sys/monitorStation/")
@Api(tags = "后台业务:2.监测站", description = "监测站增删查改", produces = MediaType.APPLICATION_JSON_VALUE, position = 1)
public class ApiMonitorStationController extends BaseController {
	@Autowired
	private MonitorStationService monitorStationService;
	
	@Autowired
	private MemberService memberService;

	@ResponseBody
	@RequestMapping(value = "queryMonitorStation.v1")
	@ApiOperation(value = "公司监测站查询", httpMethod = "POST")
	public Message<PageData<MonitorStationDto>> queryMonitorStation(
			@ApiParam(required = false, value = "所属公司") String companyId,
			@ApiParam(required = false, value = "公司监测站Id") String monitorStationId,
			@ApiParam(required = false, value = "公司监测站所在地区查询") String stationAddress,
			@ApiParam(required = false, value = "公司监测站名称查询") String stationName,
			@ApiParam(required = false, value = "创建人Id") String createBy,
			@ApiParam(required = false, value = "页数") Integer page,
			@ApiParam(required = false, value = "行数") Integer rows,
			@ApiParam(required = false, value = "标志位:默认获取监测站信息  传入MonitorStation") String annotation,
			@ApiParam(required = false, value = "城市") String city,
			@ApiParam(required = false, value = "标志位:默认获取监测站信息,one获取母线层以上信息,two获取支线层以上信息,three获取监测设备层以上信息") String flag) {
//		String loginMemberId = this.getLoginMemberId();
//		String loginManagerId = this.getLoginManagerId();
//		if(loginMemberId==null&&loginManagerId==null){
//			throw new BizException(Code.NOT_LOGIN);
//		}
		checkLogionStatus();
		//		long start, end;
//		start = System.currentTimeMillis();
		PageData<MonitorStationDto> data = monitorStationService.listPage(companyId, monitorStationId, stationAddress,stationName, page, rows, flag, createBy, annotation,city);
//		end = System.currentTimeMillis();
//		System.out.println("queryMonitorStation.v1:" + "start time:" + start + "; end time:" + end + "; Run Time:"
//				+ (end - start) + "(ms)");
		return new Message<>(Code.SUCCESS, data);
	}

	@ResponseBody
	@RequestMapping(value = "queryMonitorAddress.v1")
	@ApiOperation(value = "查询公司监测站所有省份地址", httpMethod = "POST")
	public Message<Set<String>> queryMonitorAddress(@ApiParam(required = false, value = "pc端创建者：pc端") String createBy,
			@ApiParam(required = false, value = "app登陆者：companyId") String companyId) {
		Set<String> data = monitorStationService.queryMonitorAddress(createBy, companyId);
		return new Message<>(Code.SUCCESS, data);
	}

	@ResponseBody
	@RequestMapping("addOrUpdateMonitorStation.v1")
	@ApiOperation(value = "公司监测站新增或修改", httpMethod = "POST")
	@SystemLog(module="/sys/monitorStation/addOrUpdateMonitorStation",methods = "公司监测站新增或修改")
	public Message<MonitorStationDto> addOrUpdateMonitorStation(
			@ApiParam(required = false, value = "公司监测站Id") String monitorStationId,
			@ApiParam(required = false, value = "监测站名称") String stationName,
			@ApiParam(required = false, value = "监测站所在地区") String stationAddress,
			@ApiParam(required = false, value = "监测站所在地区") String longitudeAndlatitude,
			@ApiParam(required = false, value = "所属公司") String companyId,
			@ApiParam(required = false, value = "公司拥有的母线") String linesOnMastersIds,
			@ApiParam(required = false, value = "城市") String city,
			@ApiParam(required = false, value = "城市编码") String cityCode,
			@ApiParam(required = false, value = "监测站类型") String stationType) {
		 if(StringUtils.isBlank(getLoginManagerId())){
          	 throw new BizException(Code.NOT_LOGIN);   }
		return monitorStationService.addOrUpdateMonitorStation(monitorStationId, stationName, stationAddress,
				longitudeAndlatitude, companyId, linesOnMastersIds, this.getLoginManagerId(), request.getRemoteAddr(),city,cityCode,stationType);
	}

	@ResponseBody
	@RequestMapping("deleteMonitorStation.v1")
	@SystemLog(module="/sys/monitorStation/deleteMonitorStation",methods = "公司监测站删除")
	public Message<?> deleteMonitorStation(@ApiParam(required = false, value = "监测站Ids") String monitorStationIds) {
		 if(StringUtils.isBlank(getLoginManagerId())){
          	 throw new BizException(Code.NOT_LOGIN);   }
		 memberService.updateMemberManageMonitorStationId(monitorStationIds);
		return monitorStationService.delete(monitorStationIds, this.getLoginManagerId(), request.getRemoteAddr());
	}

	@ResponseBody
	@RequestMapping(value = "queryMonitorStationForApp.v1")
	@ApiOperation(value = "app监测站查询", httpMethod = "POST")
	public Message<PageData<MonitorStationDto>> queryMonitorStationInfo(
			@ApiParam(required = false, value = "公司监测站Id") String monitorStationId,
			@ApiParam(required = false, value = "标志位:默认获取监测站信息,one获取母线层以上信息,two获取支线层以上信息,three获取监测设备层以上信息") String flag,
			@ApiParam(required = false, value = "所属公司") String companyId,
			@ApiParam(required = false, value = "公司监测站所在地区查询") String stationAddress,
			@ApiParam(required = false, value = "公司监测站名称查询") String stationName,
			@ApiParam(required = false, value = "创建人Id") String createBy,
			@ApiParam(required = false, value = "页数") Integer page,
			@ApiParam(required = false, value = "行数") Integer rows,
			@ApiParam(required = false, value = "标志位:默认获取监测站信息  传入 MonitorStation") String annotation,
			@ApiParam(required = false, value = "城市") String city) {
		long start, end;
		start = System.currentTimeMillis();
		PageData<MonitorStationDto> data = monitorStationService.listPageForApp(companyId, monitorStationId,
				stationAddress, stationName, page, rows, flag, createBy, annotation,city);
		end = System.currentTimeMillis();
		System.out.println("queryMonitorStation.v1:" + "start time:" + start + "; end time:" + end + "; Run Time:"
				+ (end - start) + "(ms)");
		return new Message<>(Code.SUCCESS, data);
	}

	/**
	 * 一次接线图
	 * 
	 * @param companyId
	 * @param monitorStationId
	 * @param stationAddress
	 * @param stationName
	 * @param createBy
	 * @param page
	 * @param rows
	 * @param annotation
	 * @param flag
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "queryWiringDiagramData.v1")
	@ApiOperation(value = "一次接线图", httpMethod = "POST")
	public Message<PageData<MonitorStationDto>> queryWiringDiagramData(  
			@ApiParam(required = false, value = "所属公司") String companyId,
			@ApiParam(required = false, value = "公司监测站Id") String monitorStationId,
			@ApiParam(required = false, value = "公司监测站所在地区查询") String stationAddress,
			@ApiParam(required = false, value = "公司监测站名称查询") String stationName,
			@ApiParam(required = false, value = "创建人Id") String createBy,
			@ApiParam(required = false, value = "页数") Integer page,
			@ApiParam(required = false, value = "行数") Integer rows,
			@ApiParam(required = false, value = "标志位:默认获取监测站信息  传入 MonitorStation") String annotation,
			@ApiParam(required = false, value = "标志位:默认获取监测站信息,one获取母线层以上信息,two获取支线层以上信息,three获取监测设备层以上信息") String flag) {
		long start, end;
		start = System.currentTimeMillis();
		String loginMemberId = this.getLoginMemberId();
		PageData<MonitorStationDto> data = monitorStationService.queryWiringDiagram(companyId, monitorStationId,stationAddress, stationName, page, rows, flag, createBy, annotation, loginMemberId);
		end = System.currentTimeMillis();
		System.out.println("一次接线图：queryWiringDiagramData.v1:" + "start time:" + start + "; end time:" + end+ "; Run Time:" + (end - start) + "(ms)");
		return new Message<>(Code.SUCCESS, data);
	}

	/**
	 * 一次接线图数据
	 * 
	 * @param temperIds
	 *            温度采集器 monitorFlag 设备标识，多个逗号隔开
	 * @param voltageIds
	 *            过电压保护器 monitorFlag 设备标识 ，多个逗号隔开
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "getWiringDiagramData.v2")
	@ApiOperation(value = "一次接线图数据", httpMethod = "POST")
	public Message<PageData<MonitorPointDto>> getWiringDiagramDataV2(
			@ApiParam(required = false, value = "温度采集器 monitorFlag (A104,A105,)逗号分隔方式") String temperIds,
			@ApiParam(required = false, value = "过电压保护器 monitorFlag (B1,B2,)逗号分隔方式") String voltageIds) {
		long start, end;
//		System.out.println("temperIds:"+temperIds);
//		System.out.println("voltageIds:"+voltageIds);
		start = System.currentTimeMillis();
		PageData<MonitorPointDto> wiringDiagramData = monitorStationService.getWiringDiagramDataV2(temperIds,voltageIds);
		end =   System.currentTimeMillis();
		System.out.println("一次接线图数据：queryWiringDiagramData.v2:" + "start time:" + start + "; end time:" + end
				+ "; Run Time:" + (end - start) + "(ms)");
		return new Message<>(Code.SUCCESS, wiringDiagramData);
	}

	
	@ResponseBody
	@RequestMapping(value = "queryMonitorStation.v2")
	public Message<PageData<MonitorStationDto>> queryMonitorStation2(
			@ApiParam(required = false, value = "所属公司") String companyId,
			@ApiParam(required = false, value = "公司监测站Id") String monitorStationId,
			@ApiParam(required = false, value = "公司监测站所在地区查询") String stationAddress,
			@ApiParam(required = false, value = "公司监测站名称查询") String stationName,
			@ApiParam(required = false, value = "创建人Id") String createBy,
			@ApiParam(required = false, value = "页数") Integer page,
			@ApiParam(required = false, value = "行数") Integer rows,
			@ApiParam(required = false, value = "标志位:默认获取监测站信息  传入MonitorStation") String annotation,
			@ApiParam(required = false, value = "城市") String city,
			@ApiParam(required = false, value = "标志位:默认获取监测站信息,one获取母线层以上信息,two获取支线层以上信息,three获取监测设备层以上信息") String flag) {
//		String loginMemberId = this.getLoginMemberId();
//		String loginManagerId = this.getLoginManagerId();
//		if(loginMemberId==null&&loginManagerId==null){
//			throw new BizException(Code.NOT_LOGIN);
//		}
		checkLogionStatus();
		//		long start, end;
//		start = System.currentTimeMillis();
		PageData<MonitorStationDto> data = monitorStationService.listPage2(companyId, monitorStationId, stationAddress,
				stationName, page, rows, flag, createBy, annotation,city);
//		end = System.currentTimeMillis();
//		System.out.println("queryMonitorStation.v1:" + "start time:" + start + "; end time:" + end + "; Run Time:"
//				+ (end - start) + "(ms)");
		return new Message<>(Code.SUCCESS, data);
	}


	@ResponseBody
	@RequestMapping(value = "queryMonitorStationById.v1")
	public Message<MonitorStationDto> queryMonitorStation2(
			@ApiParam(required = false, value = "公司监测站Id") String monitorStationId) {
		MonitorStationDto entityById = monitorStationService.getEntityById(monitorStationId);
		return new Message<MonitorStationDto>(Code.SUCCESS, entityById);
	}
	
	
	/**
	 * 查询鹰眼设备是否在线离线
	 * @param deviceId
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "queryDeviceIsOnline.v1")
	public Message<List<device_info>> queryDeviceIsOnline(String deviceId) {
		if(null==deviceId||StringUtils.isBlank(deviceId))
			throw new BizException(Code.NOT_PARAM);
			List<String> list = new ArrayList<>();
			list.add(deviceId);
			List<device_info> deviceInfo = XWDataEntry.getDeviceInfo(list);
		return new Message<List<device_info>>(Code.SUCCESS, deviceInfo);
	}
	
	
}
