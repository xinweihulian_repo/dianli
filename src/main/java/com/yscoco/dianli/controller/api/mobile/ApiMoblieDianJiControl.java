package com.yscoco.dianli.controller.api.mobile;



import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.alibaba.fastjson.JSON;
import com.batise.device.eric900.eric900_data;
import com.batise.device.fault.fault_data_new;
import com.batise.device.motor.Motor_data;
import com.hyc.smart.XWDataEntry;
import com.hyc.smart.eric900.eric900_config;
import com.yscoco.dianli.common.exception.BizException;
import com.yscoco.dianli.common.vo.Code;
import com.yscoco.dianli.dto.company.MonitorStationDeviceDto;
import com.yscoco.dianli.service.company.MonitorStationDeviceService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yscoco.dianli.common.base.BaseController;
import com.yscoco.dianli.common.utils.BeanUtils;
import com.yscoco.dianli.common.vo.Message;
import com.yscoco.dianli.common.vo.PageData;
import com.yscoco.dianli.entity.company.DianJi;
import com.yscoco.dianli.entity.company.MonitorPoint;
import com.yscoco.dianli.pojo.DianJiModel;
import com.yscoco.dianli.pojo.PointModel;
import com.yscoco.dianli.service.company.DianJiService;
import com.yscoco.dianli.service.company.MonitorPointService;


@Controller
@RequestMapping("/eric/mobile/")
public class ApiMoblieDianJiControl extends BaseController {
	
	@Autowired
	private MonitorPointService monitorPointService;
	@Autowired
	private DianJiService dianJiService;

	@Autowired
	private MonitorStationDeviceService monitorStationDeviceService;


	/**
	 *  获取耐张佳表格
	 * @param stationId
	 * @param page
	 * @param rows
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "nzj/table.v1")
	public Message<PageData<MonitorStationDeviceDto>> getTableData(
			String stationId,
			Integer page,
			Integer rows) {
		PageData<MonitorStationDeviceDto> listPage = monitorStationDeviceService.listPage(stationId,page,rows);
		List<MonitorStationDeviceDto> list = listPage.getList();
		if(list==null){
			return new Message<>(Code.SUCCESS, listPage);
		}
		return new Message<>(Code.SUCCESS, listPage);
	}


	@ResponseBody
	@RequestMapping("dianJiList.v1")
	public Message<PageData<DianJiModel>> getDianJiList(String token,String pointName){
		List<DianJiModel>  listDianJiModel  = new ArrayList<>();
	    PageData<PointModel> dianJiListByPrincipal = monitorPointService.getDianJiListByPrincipal(token,pointName);
	    DianJiModel  dianJiModel  = null;
	    List<PointModel> list = dianJiListByPrincipal.getList();
	    	for(int i=0;i< list.size();i++){
	    		dianJiModel = new DianJiModel();
		    	BeanUtils.copyProperties(list.get(i), dianJiModel);
		    	listDianJiModel.add(dianJiModel);
	    	}
		    
	    PageData<DianJiModel> page = new PageData<>();
	      page.setCount(listDianJiModel.size());
	      page.setList(listDianJiModel);
		return Message.success(page);
	}

	
	@ResponseBody
	@RequestMapping("dianJiInfo.v1")
	public Message<DianJi> getDianJiInfoByDeviceNo(String pointFlag){
		if(StringUtils.isEmpty(pointFlag))
			throw new BizException(Code.NOT_PARAM);
		
		DianJi dianJi = dianJiService.getByDeviceNo(pointFlag);
		if(dianJi==null){
			MonitorPoint byPointFlag = monitorPointService.getByPointFlag(pointFlag);
			dianJi = new DianJi();
			dianJi.setCreateTime(new Date());
			dianJi.setDeviceNo(pointFlag);
			dianJi.setCreateBy(byPointFlag.getCreateBy());
		    dianJi = dianJiService.addOrUpdate(dianJi);
		}
		return Message.success(dianJi);
	}

	@ResponseBody
	@RequestMapping("get900DataList.v1")
	public Message<List<eric900_data>> get900DataList(String monitorFlag, Long timeStart, Long timeEnd){
		if(StringUtils.isEmpty(monitorFlag)){
			throw new BizException(Code.NOT_PARAM);
		}
		 List<eric900_data> dataList = XWDataEntry.get900DataList(monitorFlag, timeStart==null?0:timeStart,timeEnd==null?System.currentTimeMillis():timeEnd);
		return Message.success(dataList);
	}

	@ResponseBody
    @RequestMapping("getConfig900.v1")
    public Message<?> getConfig900(String monitorFlag){
        if(StringUtils.isEmpty(monitorFlag))
            throw new BizException(Code.NOT_PARAM);
        
        if(XWDataEntry.getConfig900(monitorFlag)==null)
            throw new BizException(Code.ERR_DIANJI_CONFIG);

        return Message.success(XWDataEntry.getConfig900(monitorFlag));
    }

	/**
     *数据显示的曲线
     *power_a;//A相功率因数
     *power_b;//B相功率因数
     *power_c;//C相功率因数
     *temp_machine;//电机温度
     *gravity_x;//x轴重力
     *gravity_y;//y轴重力
     *gravity_z;//z轴重力
	 * @param config
	 * @return
	 */
	@ResponseBody
	@RequestMapping("sync900Config.v1")
	public Message<?> Sync900Config(eric900_config config){
		config.setGravity((byte)(config.getGravity() & 0xff));
		try {
			if(XWDataEntry.Sync900Config(config)){
				return Message.success();
			}
		}catch (Exception e){
			throw new BizException(Code.ERR_DIANJISYNC);
		}

		 return Message.success();

	}

	/**
	 *  id集合的所有最新一条数据	 * @param Ids
	 * @param token
	 * @param pointName
	 * @return
	 */
	@ResponseBody
	@RequestMapping("dianJiListData.v2")
	public Message<List<Motor_data>> getMotorList(String token,String pointName,String status){
		PageData<PointModel> dianJiListByPrincipal = monitorPointService.getDianJiListWebUser(token,pointName,status);

        if(dianJiListByPrincipal.getList()==null||dianJiListByPrincipal.getList().isEmpty())
            return  Message.success(new ArrayList<>());

        List<String> pointFlagList = dianJiListByPrincipal.getList().stream().map(PointModel::getPointFlag).collect(Collectors.toList());
        List<Motor_data> result = pointFlagList == null ? new ArrayList<Motor_data>() : XWDataEntry.getLastmotorData(pointFlagList);
        
            return  Message.success(result);
	}
	
	
	/**
	 * 获取电机某一段时间的所有数据
	 * @param monitorFlag
	 * @param timeStart
	 * @param timeEnd
	 * @return
	 */
	@ResponseBody
	@RequestMapping("get900DataList.v2")
	public Message<List<Motor_data>> getLastmotorData(String monitorFlag, Long timeStart, Long timeEnd){
		if(StringUtils.isEmpty(monitorFlag))
			throw new BizException(Code.NOT_PARAM);
		 List<Motor_data> dataList = XWDataEntry.getMotorList(monitorFlag, timeStart==null?0:timeStart,timeEnd==null?System.currentTimeMillis():timeEnd);
		return Message.success(dataList);
	}

	/**
	 * 获取某个设备的告警列表
	 * @param monitorFlag
	 * @param page
	 * @param rows
	 * @return
	 */
	@ResponseBody
	@RequestMapping("getFaultInfoForId.v1")
	public Message<List<fault_data_new>> getFaultInfoForId(String monitorFlag, Integer page, Integer rows){
        if(StringUtils.isEmpty(monitorFlag))
            throw new BizException(Code.NOT_PARAM);
		List<fault_data_new> faultInfoForId = XWDataEntry.getFaultInfoForId(monitorFlag, page, rows);
		return Message.success(faultInfoForId);
	}




	public static void main(String[] args){

		List<fault_data_new> faultInfoForId = XWDataEntry.getFaultInfoForId("ERIC0900191100000001", 0, 10);
		System.out.println(JSON.toJSON(faultInfoForId));
		
	}
}
