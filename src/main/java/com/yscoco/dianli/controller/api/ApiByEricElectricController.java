package com.yscoco.dianli.controller.api;

import com.yscoco.dianli.common.aspect.SystemLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.batise.device.eric900b.eric900b_data;
import com.batise.device.set.config_eric800;
import com.batise.device.temper.Temper420_data;
import com.batise.device.temper.Temper_data;
import com.hyc.smart.XWDataEntry;
import com.hyc.smart.bean.ItemFault1Data;
import com.hyc.smart.bean.ItemFault2Data;
import com.hyc.smart.bean.ItemFaultData;
import com.hyc.smart.bean.ItemTHD;
import com.hyc.smart.eric420.FaultInfrared;
import com.yscoco.dianli.common.base.BaseController;
import com.yscoco.dianli.common.exception.BizException;
import com.yscoco.dianli.common.handler.PageData;
import com.yscoco.dianli.common.utils.DateUtils;
import com.yscoco.dianli.common.vo.Code;
import com.yscoco.dianli.common.vo.Message;
import com.yscoco.dianli.constants.AlarmLeve;
import com.yscoco.dianli.constants.BidConst;
import com.yscoco.dianli.constants.FaultType;
import com.yscoco.dianli.dto.company.FaultGroundInfoDto;
import com.yscoco.dianli.dto.company.FaultMsgDesc;
import com.yscoco.dianli.dto.company.FaultMsgDto;
import com.yscoco.dianli.dto.company.LinesOnMasterDto;
import com.yscoco.dianli.entity.company.LinesOnMaster;
import com.yscoco.dianli.pojo.MasterModel;
import com.yscoco.dianli.pojo.PointModel;
import com.yscoco.dianli.service.comon.GetResponsibleDeviceService;
import com.yscoco.dianli.service.comon.Transition;
import com.yscoco.dianli.service.company.FaultGroundInfoService;
import com.yscoco.dianli.service.company.LinesOnMasterService;
import com.yscoco.dianli.service.company.ServerFaultService;
import com.yscoco.dianli.service.company.Sync800ConfigService;


@Controller
@RequestMapping("/eric/")
@Api(tags = "后台业务:6、通过客户接口获取的数据", description = "母线线路+母线电流+四相电+保护器+波形图", produces = MediaType.APPLICATION_JSON_VALUE, position = 1)
public class ApiByEricElectricController extends BaseController {
	private static Logger log = Logger.getLogger(ApiByEricElectricController.class);


	@Autowired
	private FaultGroundInfoService faultGroundInfoService;

	@Autowired
	private Sync800ConfigService sync800ConfigService;

	@Autowired
	private ServerFaultService serverFaultService;
	
	@Autowired
	private GetResponsibleDeviceService getResponsibleDeviceService;
	
	@Autowired
	private LinesOnMasterService linesOnMasterService;
	
	
	/**
	 * 获取鹰眼一段时间内的数据
	 * @param deviceId
	 * @param timeStart
	 * @param timeEnd
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "getTemperature420List.v1")
	public Message<List<Temper420_data>> getTemperature420List(
            String deviceId, Long timeStart, Long timeEnd) {
		List<Temper420_data> temperature420List  =null;
        try {
            if(StringUtils.isBlank(deviceId)){
                throw new BizException(Code.NOT_PARAM);
            }
           temperature420List = XWDataEntry.getTemperature420List(deviceId, timeStart, (timeEnd == null ? System.currentTimeMillis() : timeEnd));

        }catch (Exception e){
            throw new BizException(Code.XWDataEntry_ERROR);
        }
        return new Message<>(Code.SUCCESS,temperature420List);
	}

	@ResponseBody
	@RequestMapping("getTemper_data.v1")
	@ApiOperation(value = "获取温度采集器数据", httpMethod = "POST")
	public Message<?> getTemper_data(
			@ApiParam(required = false, value = "采集器的id:sensorid = B11,B27,B43") String monitorFlag,
			@ApiParam(required = false, value = "timeStart：开始时间戳,日期例子:1970-01-01 00:00:00:000") String timeStart,
			@ApiParam(required = false, value = "timeEnd:结束时间时间戳:System.currentTimeMillis(),日期例子:2018-04-26 01:00:00:000") String timeEnd)
			throws Exception {
		// System.out.println("monitorFlag:" + monitorFlag);
				List<Temper_data> data = null;
				try{
					if (StringUtils.isNotEmpty(monitorFlag)) {
						long timeStart_ = DateUtils.dateToUnixTimestamp(timeStart, "yyyy-MM-dd HH:mm:ss:sss");
						long timeEnd_ = DateUtils.dateToUnixTimestamp(timeEnd, "yyyy-MM-dd HH:mm:ss:sss");
							data = XWDataEntry.getTemperatureList(monitorFlag, timeStart_, timeEnd_);
					} else {
						throw new BizException(Code.NOT_PARAM);
					}
				}catch(Exception e){
					data = new ArrayList<>();
					log.error(e.getMessage());
				}
				System.out.println("获取温度采集器最新的数据:getTemper_data:" + JSON.toJSONString(data));
				return Message.success(data);
	}

	// ==================== 从接口那里获取母线相关数据：获取母线相关数据
	@ResponseBody
	@RequestMapping("getEricDataForIds.v1")
	@ApiOperation(value = "获取母线线路详情", httpMethod = "POST")
	public Message<List<Map<String, Object>>> getEricDataForIds(
			@ApiParam(required = false, value = "母线的id集合:用逗号隔开:母线ID例子：00000000000000002017") String deviceIds) {
		List<Map<String, Object>> ericDataList = null;
		if (StringUtils.isNotEmpty(deviceIds)) {
			String[] devidsArr = deviceIds.split(",");
			List<String> devidsList = Arrays.asList(devidsArr);	
			ericDataList = XWDataEntry.getEricDataForIds(devidsList);
		} else {
			throw new BizException(Code.NOT_PARAM);
		}
		return Message.success(ericDataList);
	}

	
	
	/*
	      //设置响应格式
		@RequestMapping(value = "/push", produces = "text/event-stream;charset=UTF-8")
		public @ResponseBody String push() {
			Random r = new Random();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	      //SSE返回数据格式是固定的以data:开头，以\n\n结束
			return "data:Testing 1,2,3" + r.nextInt() + "\n\n";
		}*/


	
	
	@ResponseBody
	@RequestMapping("getEricElectric.v1")
	@ApiOperation(value = "根据id查询母线四相电流波形", httpMethod = "POST")
	public Message<List<Map<String, Object>>> getEricElectric(
			@ApiParam(required = false, value = "母线的id:00000000000000002017") String deviceId,
			@ApiParam(required = false, value = "timeStart：开始时间戳,日期例子:1970-01-01 00:00:00:000") String timeStart,
			@ApiParam(required = false, value = "timeEnd:结束时间时间戳:System.currentTimeMillis(),日期例子:2018-04-26 01:00:00:000") String timeEnd)
			throws Exception {
		List<Map<String, Object>> ericElectricList = null;
		if (StringUtils.isNotEmpty(deviceId)) {
			timeStart = StringUtils.isEmpty(timeStart) ? DateUtils.getPlanStart() : timeStart;
			timeEnd = StringUtils.isEmpty(timeEnd) ? DateUtils.getPlanEnd() : timeEnd;

			long timeStart_ = DateUtils.dateToUnixTimestamp(timeStart, "yyyy-MM-dd HH:mm:ss:sss");
			long timeEnd_ = DateUtils.dateToUnixTimestamp(timeEnd, "yyyy-MM-dd HH:mm:ss:sss");
			/*System.out.println("timeStart_" + timeStart_);
			System.out.println("timeEnd_" + timeEnd_);*/
			ericElectricList = XWDataEntry.getEricElectric(deviceId, timeStart_, timeEnd_); // DateUtils.unixTimestampToDate(1524143300898l)
			for (Map<String, Object> map : ericElectricList) {
				map.put("time", DateUtils.unixTimestampToDate_data(Long.valueOf(map.get("timestamp").toString())));
					
//				map.put("Electric_0", map.get("Electric_0").toString().equalsIgnoreCase("0.0") ? 0
//						: Double.valueOf(map.get("Electric_0").toString()) * BidConst.ERIC_CONSTANT);
				map.put("Electric_0", map.get("Electric_0").toString().equalsIgnoreCase("0.0") ? 0
						: Double.valueOf(map.get("Electric_0").toString()));
				map.put("Electric_a", map.get("Electric_a").toString().equalsIgnoreCase("0.0") ? 0
						: Double.valueOf(map.get("Electric_a").toString()) * BidConst.ERIC_CONSTANT);
				map.put("Electric_b", map.get("Electric_b").toString().equalsIgnoreCase("0.0") ? 0
						: Double.valueOf(map.get("Electric_b").toString()) * BidConst.ERIC_CONSTANT);
				map.put("Electric_c", map.get("Electric_c").toString().equalsIgnoreCase("0.0") ? 0
						: Double.valueOf(map.get("Electric_c").toString()) * BidConst.ERIC_CONSTANT);
			}

		} else {
			throw new BizException(Code.NOT_PARAM);
		}
		return Message.success(ericElectricList);
	}

	@ResponseBody
	@RequestMapping("getEricVoltage.v1")
	@ApiOperation(value = "根据id查询母线四相电压波形", httpMethod = "POST")
	public Message<List<Map<String, Object>>> getEricVoltage(
			@ApiParam(required = false, value = "母线的id:00000000000000002017") String deviceId,
			@ApiParam(required = false, value = "timeStart：开始时间戳,日期例子:1970-01-01 00:00:00:000") String timeStart,
			@ApiParam(required = false, value = "timeEnd:结束时间时间戳:System.currentTimeMillis(),日期例子:2018-04-26 01:00:00:000") String timeEnd)
			throws Exception {
		List<Map<String, Object>> ericVoltageList = null;
		if (StringUtils.isNotEmpty(deviceId)) {
			timeStart = StringUtils.isEmpty(timeStart) ? DateUtils.getPlanStart() : timeStart;
			timeEnd = StringUtils.isEmpty(timeEnd) ? DateUtils.getPlanEnd() : timeEnd;

			long timeStart_ = DateUtils.dateToUnixTimestamp(timeStart, "yyyy-MM-dd HH:mm:ss:sss");
			long timeEnd_ = DateUtils.dateToUnixTimestamp(timeEnd, "yyyy-MM-dd HH:mm:ss:sss");
			ericVoltageList = XWDataEntry.getEricVoltage(deviceId, timeStart_, timeEnd_);
			if(ericVoltageList!=null){
				for (Map<String, Object> map : ericVoltageList) {
					map.put("time", DateUtils.unixTimestampToDate_data(Long.valueOf(map.get("timestamp").toString())));
//					map.put("voltage_0", map.get("voltage_0").toString().equalsIgnoreCase("0.0") ? 0
//							: Double.valueOf(map.get("voltage_0").toString()) * BidConst.VOL_CONSTANT);
					map.put("voltage_0", map.get("voltage_0").toString().equalsIgnoreCase("0.0") ? 0
							: Double.valueOf(map.get("voltage_0").toString()));
					map.put("voltage_a", map.get("voltage_a").toString().equalsIgnoreCase("0.0") ? 0
							: Double.valueOf(map.get("voltage_a").toString()) * BidConst.VOL_CONSTANT);
					map.put("voltage_b", map.get("voltage_b").toString().equalsIgnoreCase("0.0") ? 0
							: Double.valueOf(map.get("voltage_b").toString()) * BidConst.VOL_CONSTANT);
					map.put("voltage_c", map.get("voltage_c").toString().equalsIgnoreCase("0.0") ? 0
							: Double.valueOf(map.get("voltage_c").toString()) * BidConst.VOL_CONSTANT);
				}
			}
		} else {
			throw new BizException(Code.NOT_PARAM);
		}
		return Message.success(ericVoltageList);
	}

	// ==================== 从接口那里获取母线相关数据：获取保护器相关数据
	@ResponseBody
	@RequestMapping("getActionCount.v1")
	@ApiOperation(value = "获取支线保护器的动作总次数", httpMethod = "POST")
	public Message<List<Map<String, Object>>> getActionCount(
			@ApiParam(required = false, value = "采集器的id集合：用逗号隔开,sensorid = B11,B27,B43") String sensorIds) {
		List<Map<String, Object>> actionCountList = null;
		if (StringUtils.isNotEmpty(sensorIds)) {
			String[] sensorIdsArr = sensorIds.split(",");
			List<String> sensorIdsList = Arrays.asList(sensorIdsArr);
			actionCountList = XWDataEntry.getActionCount(sensorIdsList);
		} else {
			throw new BizException(Code.NOT_PARAM);
		}
		return Message.success(actionCountList);
	}

	@ResponseBody
	@RequestMapping("getEricTHDMaxAndMinByABC.v1")
	@ApiOperation(value = "获取母线的电压谐波和电流谐波最大值+最小值", httpMethod = "POST")
	public Message<Map<String, Object>> getEricTHDMaxAndMinByABC(
			@ApiParam(required = false, value = "母线的id:00000000000000002017") String deviceId,
			@ApiParam(required = false, value = "timeStart：开始时间戳,日期例子:1970-01-01 00:00:00:000") String timeStart,
			@ApiParam(required = false, value = "timeEnd:结束时间时间戳:System.currentTimeMillis(),日期例子:2018-04-26 01:00:00:000") String timeEnd)
			throws Exception {
		List<ItemTHD> ericVoltageTHDList = null;
		List<ItemTHD> ericElectricTHDList = null;
		if (StringUtils.isNotEmpty(deviceId)) {
			timeStart = StringUtils.isEmpty(timeStart) ? DateUtils.getPlanStart() : timeStart;
			timeEnd = StringUtils.isEmpty(timeEnd) ? DateUtils.getPlanEnd() : timeEnd;

			long timeStart_ = DateUtils.dateToUnixTimestamp(timeStart, "yyyy-MM-dd HH:mm:ss");
			long timeEnd_ = DateUtils.dateToUnixTimestamp(timeEnd, "yyyy-MM-dd HH:mm:ss");
			ericVoltageTHDList = XWDataEntry.getEricVoltageTHD(deviceId, timeStart_, timeEnd_);

			ericElectricTHDList = XWDataEntry.getEricElectricTHD(deviceId, timeStart_, timeEnd_);

		} else {
			throw new BizException(Code.NOT_PARAM);
		}

		// 组织数据
		Map<String, Object> result = new HashMap<>();
		this.method1(ericVoltageTHDList, ericElectricTHDList, result);
		return Message.success(result);
	}

	// 方法一：
	private Map<String, Object> method1(List<ItemTHD> ericVoltageTHDList, List<ItemTHD> ericElectricTHDList,
			Map<String, Object> result) {
		// ============================= 电压 =================================
		List<Float> Voltage_list_A = new ArrayList<Float>();
		List<Float> Voltage_list_B = new ArrayList<Float>();
		List<Float> Voltage_list_C = new ArrayList<Float>();

		Float Voltage_Max_A_SUM = 0.0f;
		Float Voltage_Max_B_SUM = 0.0f;
		Float Voltage_Max_C_SUM = 0.0f;
		for (int index = 0; index < ericVoltageTHDList.size(); index++) {
			for (float f_A : ericVoltageTHDList.get(index).getParamA()) {
				Voltage_list_A.add(f_A);
				Voltage_Max_A_SUM = Voltage_Max_A_SUM + f_A;
			}
			for (float f_B : ericVoltageTHDList.get(index).getParamB()) {
				Voltage_list_B.add(f_B);
				Voltage_Max_B_SUM = Voltage_Max_B_SUM + f_B;
			}
			for (float f_C : ericVoltageTHDList.get(index).getParamC()) {
				Voltage_list_C.add(f_C);
				Voltage_Max_C_SUM = Voltage_Max_C_SUM + f_C;
			}
		}

		Float Voltage_A_Max = (Voltage_list_A.size() == 0) ? 0.0f : Collections.max(Voltage_list_A);
		Float Voltage_A_Min = (Voltage_list_A.size() == 0) ? 0.0f : Collections.min(Voltage_list_A);
		Float Voltage_A_Average = (Voltage_list_A.size() == 0) ? 0.0f : Voltage_Max_A_SUM / Voltage_list_A.size();

		Float Voltage_B_Max = (Voltage_list_B.size() == 0) ? 0.0f : Collections.max(Voltage_list_B);
		Float Voltage_B_Min = (Voltage_list_B.size() == 0) ? 0.0f : Collections.min(Voltage_list_B);
		Float Voltage_B_Average = (Voltage_list_B.size() == 0) ? 0.0f : Voltage_Max_B_SUM / Voltage_list_B.size();

		Float Voltage_C_Max = (Voltage_list_C.size() == 0) ? 0.0f : Collections.max(Voltage_list_C);
		Float Voltage_C_Min = (Voltage_list_C.size() == 0) ? 0.0f : Collections.min(Voltage_list_C);
		Float Voltage_C_Average = (Voltage_list_C.size() == 0) ? 0.0f : Voltage_Max_C_SUM / Voltage_list_C.size();

		result.put("Voltage_A_Max", Voltage_A_Max * BidConst.VOL_CONSTANT);
		result.put("Voltage_A_Min", Voltage_A_Min * BidConst.VOL_CONSTANT);
		result.put("Voltage_A_Average", Voltage_A_Average * BidConst.VOL_CONSTANT);

		result.put("Voltage_B_Max", Voltage_B_Max * BidConst.VOL_CONSTANT);
		result.put("Voltage_B_Min", Voltage_B_Min * BidConst.VOL_CONSTANT);
		result.put("Voltage_B_Average", Voltage_B_Average * BidConst.VOL_CONSTANT);

		result.put("Voltage_C_Max", Voltage_C_Max * BidConst.VOL_CONSTANT);
		result.put("Voltage_C_Min", Voltage_C_Min * BidConst.VOL_CONSTANT);
		result.put("Voltage_C_Average", Voltage_C_Average * BidConst.VOL_CONSTANT);

		// ============================= 电流 =================================
		List<Float> Electric_list_A = new ArrayList<Float>();
		List<Float> Electric_list_B = new ArrayList<Float>();
		List<Float> Electric_list_C = new ArrayList<Float>();

		Float Electric_Max_A_SUM = 0.0f;
		Float Electric_Max_B_SUM = 0.0f;
		Float Electric_Max_C_SUM = 0.0f;
		for (int index = 0; index < ericVoltageTHDList.size(); index++) {
			for (float f_A : ericVoltageTHDList.get(index).getParamA()) {
				Electric_list_A.add(f_A);
				Electric_Max_A_SUM = Electric_Max_A_SUM + f_A;
			}
			for (float f_B : ericVoltageTHDList.get(index).getParamB()) {
				Electric_list_B.add(f_B);
				Electric_Max_B_SUM = Electric_Max_B_SUM + f_B;
			}
			for (float f_C : ericVoltageTHDList.get(index).getParamC()) {
				Electric_list_C.add(f_C);
				Electric_Max_C_SUM = Electric_Max_C_SUM + f_C;
			}
		}

		Float Electric_A_Max = (Electric_list_A.size() == 0) ? 0.0f : Collections.max(Electric_list_A);
		Float Electric_A_Min = (Electric_list_A.size() == 0) ? 0.0f : Collections.min(Electric_list_A);
		Float Electric_A_Average = (Electric_list_A.size() == 0) ? 0.0f : Electric_Max_A_SUM / Electric_list_A.size();

		Float Electric_B_Max = (Electric_list_B.size() == 0) ? 0.0f : Collections.max(Electric_list_B);
		Float Electric_B_Min = (Electric_list_B.size() == 0) ? 0.0f : Collections.min(Electric_list_B);
		Float Electric_B_Average = (Electric_list_B.size() == 0) ? 0.0f : Electric_Max_B_SUM / Electric_list_B.size();

		Float Electric_C_Max = (Electric_list_C.size() == 0) ? 0.0f : Collections.max(Electric_list_C);
		Float Electric_C_Min = (Electric_list_C.size() == 0) ? 0.0f : Collections.min(Electric_list_C);
		Float Electric_C_Average = (Electric_list_C.size() == 0) ? 0.0f : Electric_Max_C_SUM / Electric_list_C.size();

		result.put("Electric_A_Max", Electric_A_Max * BidConst.VOL_CONSTANT);
		result.put("Electric_A_Min", Electric_A_Min * BidConst.VOL_CONSTANT);
		result.put("Electric_A_Average", Electric_A_Average * BidConst.VOL_CONSTANT);

		result.put("Electric_B_Max", Electric_B_Max * BidConst.VOL_CONSTANT);
		result.put("Electric_B_Min", Electric_B_Min * BidConst.VOL_CONSTANT);
		result.put("Electric_B_Average", Electric_B_Average * BidConst.VOL_CONSTANT);

		result.put("Electric_C_Max", Electric_C_Max * BidConst.VOL_CONSTANT);
		result.put("Electric_C_Min", Electric_C_Min * BidConst.VOL_CONSTANT);
		result.put("Electric_C_Average", Electric_C_Average * BidConst.VOL_CONSTANT);

		return result;
	}

	@ResponseBody
	@RequestMapping("getEricVoltageTHD.v1")
	@ApiOperation(value = "获取母线的电压谐波波形", httpMethod = "POST")
	public Message<List<ItemTHD>> getEricVoltageTHD(
			@ApiParam(required = false, value = "母线的id:00000000000000002017") String deviceId,
			@ApiParam(required = false, value = "timeStart：开始时间戳,日期例子:1970-01-01 00:00:00:000") String timeStart,
			@ApiParam(required = false, value = "timeEnd:结束时间时间戳:System.currentTimeMillis(),日期例子:2018-04-26 01:00:00:000") String timeEnd)
			throws Exception {
	//	long startTime_ = System.currentTimeMillis();
		List<ItemTHD> ericVoltageTHDList = null;
		if (StringUtils.isNotEmpty(deviceId)) {
			timeStart = StringUtils.isEmpty(timeStart) ? DateUtils.getPlanStart() : timeStart;
			timeEnd = StringUtils.isEmpty(timeEnd) ? DateUtils.getPlanEnd() : timeEnd;

			long timeStart_ = DateUtils.dateToUnixTimestamp(timeStart, "yyyy-MM-dd HH:mm:ss:sss");
			long timeEnd_ = DateUtils.dateToUnixTimestamp(timeEnd, "yyyy-MM-dd HH:mm:ss:sss");
			ericVoltageTHDList = XWDataEntry.getEricVoltageTHD(deviceId, timeStart_, timeEnd_);
			for (ItemTHD itemTHD : ericVoltageTHDList) {
				itemTHD.setTimestamp(DateUtils.unixTimestampToDate_data(Long.valueOf(itemTHD.getTimestamp())));

				List<Float> vol_AList = new ArrayList<Float>();
				List<Float> vol_BList = new ArrayList<Float>();
				List<Float> vol_CList = new ArrayList<Float>();

				float[] paramAArr = itemTHD.getParamA();
				for (float paramAone : paramAArr) {
					paramAone = paramAone * BidConst.VOL_CONSTANT;
					vol_AList.add(paramAone);
				}
				float[] paramA_Arr = new float[vol_AList.size()];
				for (int i = 0; i < vol_AList.size(); i++) {
					paramA_Arr[i] = vol_AList.get(i);
				}
				itemTHD.setParamA(paramA_Arr);

				float[] paramBArr = itemTHD.getParamB();
				for (float paramBone : paramBArr) {
					paramBone = paramBone * BidConst.VOL_CONSTANT;
					vol_BList.add(paramBone);
				}
				float[] paramB_Arr = new float[vol_BList.size()];
				for (int i = 0; i < vol_BList.size(); i++) {
					paramB_Arr[i] = vol_BList.get(i);
				}
				itemTHD.setParamB(paramB_Arr);

				float[] paramCArr = itemTHD.getParamC();
				for (float paramCone : paramCArr) {
					paramCone = paramCone * BidConst.VOL_CONSTANT;
					vol_CList.add(paramCone);
				}
				float[] paramC_Arr = new float[vol_CList.size()];
				for (int i = 0; i < vol_CList.size(); i++) {
					paramC_Arr[i] = vol_CList.get(i);
				}
				itemTHD.setParamC(paramC_Arr);

			}
		} else {
			throw new BizException(Code.NOT_PARAM);
		}
	//	long endTime_ = System.currentTimeMillis();
	//	System.out.println("=============请求接口的时间：" + (endTime_ - startTime_));
		return Message.success(ericVoltageTHDList);
	}

	@ResponseBody
	@RequestMapping("getEricElectricTHD.v1")
	@ApiOperation(value = "获取母线的电流谐波波形", httpMethod = "POST")
	public Message<List<ItemTHD>> getEricElectricTHD(
			@ApiParam(required = false, value = "母线的id:00000000000000002017") String deviceId,
			@ApiParam(required = false, value = "timeStart：开始时间戳,日期例子:1970-01-01 00:00:00:000") String timeStart,
			@ApiParam(required = false, value = "timeEnd:结束时间时间戳:System.currentTimeMillis(),日期例子:2018-04-26 01:00:00:000") String timeEnd)
			throws Exception {
	//	long startTime_ = System.currentTimeMillis();
		List<ItemTHD> ericElectricTHDList = null;
		if (StringUtils.isNotEmpty(deviceId)) {
			timeStart = StringUtils.isEmpty(timeStart) ? DateUtils.getPlanStart() : timeStart;
			timeEnd = StringUtils.isEmpty(timeEnd) ? DateUtils.getPlanEnd() : timeEnd;

			long timeStart_ = DateUtils.dateToUnixTimestamp(timeStart, "yyyy-MM-dd HH:mm:ss:sss");
			long timeEnd_ = DateUtils.dateToUnixTimestamp(timeEnd, "yyyy-MM-dd HH:mm:ss:sss");
			ericElectricTHDList = XWDataEntry.getEricElectricTHD(deviceId, timeStart_, timeEnd_);
			if (ericElectricTHDList == null) {
				throw new BizException(Code.NO_DATAS);
			}

			for (ItemTHD itemTHD : ericElectricTHDList) {

				List<Float> ele_AList = new ArrayList<Float>();
				List<Float> ele_BList = new ArrayList<Float>();
				List<Float> ele_CList = new ArrayList<Float>();

				float[] paramAArr = itemTHD.getParamA();
				for (float paramAone : paramAArr) {
					paramAone = paramAone * BidConst.ERIC_CONSTANT;
					ele_AList.add(paramAone);
				}
				float[] paramA_Arr = new float[ele_AList.size()];
				for (int i = 0; i < ele_AList.size(); i++) {
					paramA_Arr[i] = ele_AList.get(i);
				}
				itemTHD.setParamA(paramA_Arr);

				float[] paramBArr = itemTHD.getParamB();
				for (float paramBone : paramBArr) {
					paramBone = paramBone * BidConst.ERIC_CONSTANT;
					ele_BList.add(paramBone);
				}
				float[] paramB_Arr = new float[ele_BList.size()];
				for (int i = 0; i < ele_BList.size(); i++) {
					paramB_Arr[i] = ele_BList.get(i);
				}
				itemTHD.setParamB(paramB_Arr);

				float[] paramCArr = itemTHD.getParamC();
				for (float paramCone : paramCArr) {
					paramCone = paramCone * BidConst.ERIC_CONSTANT;
					ele_CList.add(paramCone);
				}
				float[] paramC_Arr = new float[ele_CList.size()];
				for (int i = 0; i < ele_CList.size(); i++) {
					paramC_Arr[i] = ele_CList.get(i);
				}
				itemTHD.setParamC(paramC_Arr);

				itemTHD.setTimestamp(DateUtils.unixTimestampToDate_data(Long.valueOf(itemTHD.getTimestamp())));
			}

		} else {
			throw new BizException(Code.NOT_PARAM);
		}
	//	long endTime_ = System.currentTimeMillis();
	//	System.out.println("==============请求接口的时间：" + (endTime_ - startTime_));
		return Message.success(ericElectricTHDList);
	}

	@ResponseBody
	@RequestMapping("getTemperatureForVoltage.v1")
	@ApiOperation(value = "获取过电压保护器的温度波形", httpMethod = "POST")
	public Message<List<Map<String, Object>>> getTemperatureForVoltage(
			@ApiParam(required = false, value = "采集器的id,sensorid = B11,B27,B43") String sensorId,
			@ApiParam(required = false, value = "timeStart：开始时间戳,日期例子:1970-01-01 00:00:00:000") String timeStart,
			@ApiParam(required = false, value = "timeEnd:结束时间时间戳:System.currentTimeMillis(),日期例子:2018-04-26 01:00:00:000") String timeEnd)
			throws Exception {
	//	System.out.println("getTemperatureForVoltage.v1:" + "执行了");
		List<Map<String, Object>> temperatureForVoltageList = null;
		if (StringUtils.isNotEmpty(sensorId)) {
			timeStart = StringUtils.isEmpty(timeStart) ? DateUtils.getPlanStart() : timeStart;
			timeEnd = StringUtils.isEmpty(timeEnd) ? DateUtils.getPlanEnd() : timeEnd;
			long timeStart_ = DateUtils.dateToUnixTimestamp(timeStart, "yyyy-MM-dd HH:mm:ss:sss");
			long timeEnd_ = DateUtils.dateToUnixTimestamp(timeEnd, "yyyy-MM-dd HH:mm:ss:sss");
			temperatureForVoltageList = XWDataEntry.getTemperatureForVoltage(sensorId, timeStart_, timeEnd_);
		//	System.out.println("temperatureForVoltageList:"+JSON.toJSON(temperatureForVoltageList));
			for (Map<String, Object> map : temperatureForVoltageList) {
				map.put("time", DateUtils.unixTimestampToDate_data(Long.valueOf(map.get("timestamp").toString())));
				map.put("temper_a", map.get("temper_a").toString());
				map.put("temper_b", map.get("temper_b").toString());
				map.put("temper_c", map.get("temper_c").toString());
			}
		} else {
			throw new BizException(Code.NOT_PARAM);
		}
		return Message.success(temperatureForVoltageList);// Double.valueOf(map.get("temper_a").toString())
	}

	public double mul(String value) {
		BigDecimal b1 = new BigDecimal(value);
		BigDecimal b2 = new BigDecimal(10);
		BigDecimal b3 = new BigDecimal(500);
		return (b1.subtract(b3)).divide(b2, 3, BigDecimal.ROUND_HALF_UP).doubleValue();
	}

	@ResponseBody
	@RequestMapping("gethumidityForVoltage.v1")
	@ApiOperation(value = "获取过电压保护器的湿度波形", httpMethod = "POST")
	public Message<List<Map<String, Object>>> gethumidityForVoltage(
			@ApiParam(required = false, value = "采集器的id,sensorid = B11,B27,B43") String sensorId,
			@ApiParam(required = false, value = "timeStart：开始时间戳,日期例子:1970-01-01 00:00:00:000") String timeStart,
			@ApiParam(required = false, value = "timeEnd:结束时间时间戳:System.currentTimeMillis(),日期例子:2018-04-26 01:00:00:000") String timeEnd)
			throws Exception {
		List<Map<String, Object>> humidityForVoltageList = null;
		if (StringUtils.isNotEmpty(sensorId)) {
			timeStart = StringUtils.isEmpty(timeStart) ? DateUtils.getPlanStart() : timeStart;
			timeEnd = StringUtils.isEmpty(timeEnd) ? DateUtils.getPlanEnd() : timeEnd;

			long timeStart_ = DateUtils.dateToUnixTimestamp(timeStart, "yyyy-MM-dd HH:mm:ss:sss");
			long timeEnd_ = DateUtils.dateToUnixTimestamp(timeEnd, "yyyy-MM-dd HH:mm:ss:sss");
			humidityForVoltageList = XWDataEntry.gethumidityForVoltage(sensorId, timeStart_, timeEnd_);
			for (Map<String, Object> map : humidityForVoltageList) {
				map.put("time", DateUtils.unixTimestampToDate_data(Long.valueOf(map.get("timestamp").toString())));
			}
		} else {
			throw new BizException(Code.NOT_PARAM);
		}
		return Message.success(humidityForVoltageList);
	}

	@ResponseBody
	@RequestMapping("getAcionCountForValue.v1")
	@ApiOperation(value = "获取保护器的所有相位动作次数", httpMethod = "POST")
	public Message<List<Map<String, Object>>> getAcionCountForValue(
			@ApiParam(required = false, value = "采集器的id,sensorid = B11,B27,B43") String sensorId,
			@ApiParam(required = false, value = "timeStart：开始时间戳,日期例子:1970-01-01 00:00:00:000") String timeStart,
			@ApiParam(required = false, value = "timeEnd:结束时间时间戳:System.currentTimeMillis(),日期例子:2018-04-26 01:00:00:000") String timeEnd)
			throws Exception {
		List<Map<String, Object>> acionCountForValueList = null;
		if (StringUtils.isNotEmpty(sensorId)) {
			timeStart = StringUtils.isEmpty(timeStart) ? DateUtils.getPlanStart() : timeStart;
			timeEnd = StringUtils.isEmpty(timeEnd) ? DateUtils.getPlanEnd() : timeEnd;
			long timeStart_ = DateUtils.dateToUnixTimestamp(timeStart, "yyyy-MM-dd HH:mm:ss:sss");
			long timeEnd_ = DateUtils.dateToUnixTimestamp(timeEnd, "yyyy-MM-dd HH:mm:ss:sss");
			acionCountForValueList = XWDataEntry.getAcionCountForValue(sensorId, timeStart_, timeEnd_);
			for (Map<String, Object> map : acionCountForValueList) {
				int valueflag_int = Integer.valueOf(map.get("valueflag").toString());
				if (valueflag_int == 1) {
					map.put("valueflag_value", "A相");
				} else if (valueflag_int == 2) {
					map.put("valueflag_value", "B相");
				} else if (valueflag_int == 4) {
					map.put("valueflag_value", "C相");
				}
			}

		} else {
			throw new BizException(Code.NOT_PARAM);
		}
		return Message.success(acionCountForValueList);
	}

	@ResponseBody
	@RequestMapping("getAcionTimesForValue.v1")
	@ApiOperation(value = "查询保护器某个相别的动作时长波形", httpMethod = "POST")
	public Message<List<Map<String, Object>>> getAcionTimesForValue(
			@ApiParam(required = false, value = "采集器的id,sensorid = B11,B27,B43") String sensorId,
			@ApiParam(required = false, value = "flag相别：1：A相； 2：B相；4：C相") Integer flag,
			@ApiParam(required = false, value = "timeStart：开始时间戳,日期例子:1970-01-01 00:00:00:000") String timeStart,
			@ApiParam(required = false, value = "timeEnd:结束时间时间戳:System.currentTimeMillis(),日期例子:2018-04-26 01:00:00:000") String timeEnd) {
		List<Map<String, Object>> acionTimesForValueList = null;
		if (StringUtils.isNotEmpty(sensorId) && StringUtils.isNotEmpty(timeStart) && StringUtils.isNotEmpty(timeEnd)) {
			long timeStart_ = DateUtils.dateToUnixTimestamp(timeStart, "yyyy-MM-dd HH:mm:ss:sss");
			long timeEnd_ = DateUtils.dateToUnixTimestamp(timeEnd, "yyyy-MM-dd HH:mm:ss:sss");
			acionTimesForValueList = XWDataEntry.getAcionTimesForValue(sensorId, flag, timeStart_, timeEnd_);
		} else {
			throw new BizException(Code.NOT_PARAM);
		}
		return Message.success(acionTimesForValueList);
	}

	// 2018年4月20号 ： List<Map<String, Object>> map =
	// getLeakElecForId("B11",0l,System.currentTimeMillis());
	@ResponseBody
	@RequestMapping("getLeakElecForId.v1")
	@ApiOperation(value = "查询保护器泄露电流波形", httpMethod = "POST")
	public Message<List<Map<String, Object>>> getLeakElecForId(
			@ApiParam(required = false, value = "采集器的id:sensorid = B11,B27,B43") String sensorId,
			@ApiParam(required = false, value = "timeStart：开始时间戳,日期例子:1970-01-01 00:00:00:000") String timeStart,
			@ApiParam(required = false, value = "timeEnd:结束时间时间戳:System.currentTimeMillis(),日期例子:2018-04-26 01:00:00:000") String timeEnd)
			throws Exception {
		List<Map<String, Object>> leakElecForIdList = null;
		if (StringUtils.isNotEmpty(sensorId)) {
			timeStart = StringUtils.isEmpty(timeStart) ? DateUtils.getPlanStart() : timeStart;
			timeEnd = StringUtils.isEmpty(timeEnd) ? DateUtils.getPlanEnd() : timeEnd;
			long timeStart_ = DateUtils.dateToUnixTimestamp(timeStart, "yyyy-MM-dd HH:mm:ss:sss");
			long timeEnd_ = DateUtils.dateToUnixTimestamp(timeEnd, "yyyy-MM-dd HH:mm:ss:sss");
			leakElecForIdList = XWDataEntry.getLeakElecForId(sensorId, timeStart_, timeEnd_);
			for (Map<String, Object> map : leakElecForIdList) {
				map.put("time", DateUtils.unixTimestampToDate_data(Long.valueOf(map.get("timestamp").toString())));
			}
		} else {
			throw new BizException(Code.NOT_PARAM);
		}
		return Message.success(leakElecForIdList);
	}

	@SuppressWarnings("unused")
	private void baseData(Map<String, Object> result, short[][] baseData, String key, StringBuffer sb) {
		sb.setLength(0);
		sb = new StringBuffer();
		for (short[] s : baseData) {
			Arrays.sort(s);
			short a = (short) ((s[s.length - 1] + s[0]) / 2);
			sb.append(a).append(",");
		}
		if (sb.length() > 0) {
			sb.deleteCharAt(sb.length() - 1);
		}
		result.put(key, sb.toString());
	}

	@ResponseBody
	@RequestMapping("getFaultGround.v1")
	@ApiOperation(value = "查询母线接地故障的波形", httpMethod = "POST")
	public Message<ItemFaultData> getFaultGround(
			@ApiParam(required = false, value = "母线对应的设备id:00000000000000002017") String devid,
			@ApiParam(required = false, value = "timeEnd:时间戳:System.currentTimeMillis(),日期例子:2018-04-26 01:00:00:000") String time) {
		ItemFaultData ItemFaultData = null;
		if (StringUtils.isNotEmpty(devid) && StringUtils.isNotEmpty(time)) {
			long time_ = DateUtils.dateToUnixTimestamp(time, "yyyy-MM-dd HH:mm:ss:sss");
			ItemFaultData = XWDataEntry.getFaultGround(devid, time_);
		} else {
			throw new BizException(Code.NOT_PARAM);
		}
		return Message.success(ItemFaultData);
	}


	@ResponseBody
	@RequestMapping("queryServerFaultIds.v1")
	@ApiOperation(value = "查询故障提醒消息列表", httpMethod = "POST")
	public Message<PageData<String>> queryServerFaultIds(@ApiParam("页码") Integer page, @ApiParam("页数") Integer rows) {
		PageData<String> faultIds_onDeviceIds = serverFaultService.queryServerFaultIds(page, rows);
		return Message.success(faultIds_onDeviceIds);
	}

	@ResponseBody
	@RequestMapping("updateFaultState.v1")
	@ApiOperation(value = "解除报警接口", httpMethod = "POST")
	public Message<?> updateFaultState(
			@ApiParam(required = false, value = "type: 故障类型；1:保护器异常，2:过电压故障，3：接地故障") String type,
			@ApiParam(required = false, value = "id: 数据库中的自增id(即数据Id)") String id) {
//		int result = XWDataEntry.updateFaultState(Integer.valueOf(type), Integer.valueOf(id));
		try{
			XWDataEntry.updateFaultStateNew(Integer.parseInt(id));
		}catch(Exception e){
			throw new BizException(Code.SYS_ERR);
		}
		return Message.success();
	}

	//
	// @ResponseBody
	// @RequestMapping("syncTopol.v1")
	// @ApiOperation(value = "同步一次接线图到终端:" ,httpMethod = "POST" )
	// public Message<?> SyncTopol(
	// @ApiParam(required = false, value = "母线的id") String deviceId,
	// @ApiParam(required = false, value = "柜号") String cabin_no,
	// @ApiParam(required = false, value = "位置") String position,
	// @ApiParam(required = false, value = "采集器类型") String type,
	// @ApiParam(required = false, value = "采集器的id,sensorid = B11,B27,B43")
	// String sensorId) {
	//
	// config_topol entity = new config_topol();
	// entity.setCabin_no(Byte.valueOf(cabin_no));
	// entity.setPosition(Byte.valueOf(position));
	// entity.setType(Byte.valueOf(type));
	// entity.setSensorid(sensorId);
	//
	// List<config_topol> entityList = new ArrayList<>();
	// entityList.add(entity);
	// XWDataEntry.SyncTopol(deviceId,entityList);
	// return Message.success();
	// }
	//

	@ResponseBody
	@RequestMapping("Sync800Config.v1")
	@ApiOperation(value = "同步设置数据到终端", httpMethod = "POST")
	public Message<?> Sync800Config(@ApiParam(required = false, value = "自动生成的母线的id") String sync_By_linesOnMasterId,
			@ApiParam(required = false, value = "设备Id:标识") String monitorFlag,
			@ApiParam(required = false, value = "电池电量报警下限") String batteryMin,
			@ApiParam(required = false, value = "欠压门系数") String undervoltage,
			@ApiParam(required = false, value = "电气节点温度上限") String tempMax_Line,
			@ApiParam(required = false, value = "电气节点温度下限") String tempMin_Line,
			@ApiParam(required = false, value = "保护器腔体温度上限") String tempMax_Pro,
			@ApiParam(required = false, value = "保护器腔体温度下限") String tempMin_Pro,
			@ApiParam(required = false, value = "保护器腔体湿度上限") String humdityMax,
			@ApiParam(required = false, value = "保护器腔体湿度下限") String humdityMin,
			@ApiParam(required = false, value = "泄露电流上限") String leakageCurMax,
			@ApiParam(required = false, value = "过压门限系数") String modulus,
			@ApiParam(required = false, value = "一次额定电压") String maxRateValue,
			@ApiParam(required = false, value = "PT变比") String ratioPT,
			@ApiParam(required = false, value = "CT变比") String ratioCT,
			@ApiParam(required = false, value = "传动实验；1:开启；2:关闭") String driveTestFlag,
			@ApiParam(required = false, value = "就地和远方；1:就地；2:远方") String distanceFlag) {
		String driveTestFlagTemp = (StringUtils.isNotEmpty(driveTestFlag))==true?driveTestFlag:"1";
		String distanceFlagTemp = (StringUtils.isNotEmpty(distanceFlag))==true?distanceFlag:"1";
		config_eric800 entity = new config_eric800();
			entity.setDevid(monitorFlag);// 设备标识
			entity.setTempMax_Line(Float.valueOf(tempMax_Line));
			entity.setTempMin_Line(Float.valueOf(tempMin_Line));
			entity.setTempMax_Pro(Float.valueOf(tempMax_Pro));
			entity.setTempMin_Pro(Float.valueOf(tempMin_Pro));
			entity.setHumdityMax(Float.valueOf(humdityMax));
			entity.setHumdityMin(Float.valueOf(humdityMin));
			entity.setLeakageCurMax(Float.valueOf(leakageCurMax));
			entity.setBatteryMin(Float.valueOf(batteryMin));
			entity.setVoltageMin(Float.valueOf(undervoltage));
			entity.setModulus(Float.valueOf(modulus));
			entity.setMaxRateValue(Integer.valueOf(maxRateValue));
			entity.setRatioPT(Integer.valueOf(ratioPT));
			entity.setRatioCT(Integer.valueOf(ratioCT));
			entity.setDriveTestFlag(Byte.valueOf(driveTestFlagTemp));
			entity.setDistanceFlag(Byte.valueOf(distanceFlagTemp));
			try{
				XWDataEntry.Sync800Config(entity);
			}catch(Exception e){
				System.out.println("XWDataEntry.Sync800Config(entity);出现异常");
			}
		this.sync800ConfigService.saveSync800Config(entity, sync_By_linesOnMasterId);// 作为一次我们这边同步记录的一次备份
		return Message.success(entity);
	}

	// app端产生数据交互的部分
	@ResponseBody
	@RequestMapping("addOrUpdateFaultGroundInfo.v1")
	@ApiOperation(value = "故障处理记录信息提交", httpMethod = "POST")
	public Message<?> addOrUpdateFaultGroundInfo(
			@ApiParam(required = false, value = "故障处理记录信息Id") String faultGroundInfoId,
			@ApiParam(required = false, value = "母线Id") String deviceId,
			@ApiParam(required = false, value = "故障信息自增Id") String autoId,
			@ApiParam(required = false, value = "记录填写") String recordMessage,
			@ApiParam(required = false, value = "故障类型；1:保护器异常，2:过电压故障，3：接地故障") String type,
			@ApiParam(required = false, value = "故障发生的时间戳") String timestamp,
			@ApiParam(required = false, value = "发生故障的相位；1：A相； 2：B相；4：C相") String valueFlag) {
		return this.faultGroundInfoService.addOrUpdateFaultGroundInfo(faultGroundInfoId, deviceId, autoId,
				recordMessage, type, timestamp, valueFlag, this.getLoginMemberId());
	}
	
	// web端产生数据交互的部分
		@ResponseBody
		@RequestMapping("webAddOrUpdateFaultGroundInfo.v1")
		@ApiOperation(value = "故障处理记录信息提交")
		public Message<?> webAddOrUpdateFaultGroundInfo(
				@ApiParam(required = false, value = "故障处理记录信息Id") String faultGroundInfoId,
				@ApiParam(required = false, value = "母线Id") String deviceId,
				@ApiParam(required = false, value = "故障信息自增Id") String autoId,
				@ApiParam(required = false, value = "记录填写") String recordMessage,
				@ApiParam(required = false, value = "故障类型；1:保护器异常，2:过电压故障，3：接地故障") String type, //字段已弃用
				@ApiParam(required = false, value = "故障发生的时间戳") String timestamp,//字段已弃用
				@ApiParam(required = false, value = "发生故障的相位；1：A相； 2：B相；4：C相") String valueFlag) {//字段已弃用
			String userId=(StringUtils.isEmpty(this.getLoginManagerId()))==true?this.getLoginMemberId():this.getLoginManagerId();
			return this.faultGroundInfoService.webAddOrUpdateFaultGroundInfo(faultGroundInfoId, deviceId, autoId,
					recordMessage, "1", timestamp, "1", userId);
		}

	@ResponseBody
	@RequestMapping("queryFaultGroundInfoList.v1")
	@ApiOperation(value = "查询故障处理记录信息列表", httpMethod = "POST")
	public Message<PageData<FaultGroundInfoDto>> queryFaultGroundInfoList(
			@ApiParam("消息id:查看详情") String faultGroundInfoId,
			@ApiParam(required = false, value = "故障信息自增Id") String autoId, @ApiParam("设备Id") String deviceId,
			@ApiParam("页码") Integer page, @ApiParam("页数") Integer rows) {
		PageData<FaultGroundInfoDto> pageData = faultGroundInfoService.listPage(faultGroundInfoId, autoId, deviceId,
				page, rows);
		return Message.success(pageData);
	}

//	// @ResponseBody
//	@SuppressWarnings("null")
//	// @RequestMapping("getServerFaultPush.v1")
//	// @ApiOperation(value = "推送测试" ,httpMethod = "POST" )
//	// public Message<?> getServerFaultPush() {
//	// serverFaultService.getServerFault("00000000000000002018",2);
//	// return Message.success();
//	// }
	
	@ResponseBody    
	@RequestMapping("getFaultGroundInfoForPageByConditions")
	@ApiOperation(value = "后台按条件查询分页母线故障的基本信息", httpMethod = "POST")
	public Message<PageData<FaultMsgDesc>> getFaultGroundInfoForPageByConditions(
			@ApiParam(required = false, value = "companyId:当前账户人的公司Id") String companyId,
			@ApiParam(required = false, value = "母线名称:点击back13_0_line页面里的目线详情") String masterName,
			@ApiParam(required = false, value = "母线的监测设备标识 集合：00000000000000002017,00000000000000002018") String monitorFlag,
			@ApiParam(required = false, value = "monitorStationId:监测站Id") String monitorStationId,
			@ApiParam(required = false, value = "beginTime:报警开始时间") Long beginTime,
			@ApiParam(required = false, value = "endTime:报警结束时间") Long endTime,
			@ApiParam(required = false, value = "isSolve是否已处理:false：未处理的故障;true：已处理的故障") String isSolve,
			@ApiParam(required = false, value = "faultType:故障类型 :0:全部故障,接地故障:10,系统过电压:11 ,过电压:12,系统断线:13,欠压数据包:14,系统短路15,弧光接地故障16,20,21,22,23") Integer faultType,
			@ApiParam(required = false, value = "alarmLeve：故障等级： 0全部 1中级，2高级") Integer alarmLeve,
			@ApiParam(required = false, value = "页数:默认0") Integer page,
			@ApiParam(required = false, value = "行数:默认10") Integer rows,
			@ApiParam(required = false, value = "登入验证") String token) {
		PageData<FaultMsgDesc> data = new PageData<FaultMsgDesc>();
		List<PointModel> pointModelByLoginUser =null;
		List<MasterModel> masterModelByLoginUser=null;
		List<String> deviceIds=new ArrayList<>();
	    List<String> masterFlagList=null;
	    List<String> pointFlagList=null;
	 	if (monitorStationId != null && monitorStationId != "") {
			List<PointModel> pointModelByLoginUserTemp =new ArrayList<>();
			List<MasterModel> masterModelByLoginUserTemp=new ArrayList<>();
			pointModelByLoginUser = getResponsibleDeviceService.getPointModelByLoginUser(token);
			pointModelByLoginUser.forEach(p->{
				if(monitorStationId.equals(p.getStationId())){
					pointModelByLoginUserTemp.add(p);
				}
			});
		    masterModelByLoginUser = getResponsibleDeviceService.getMasterModelByLoginUser(token);
		    masterModelByLoginUser.forEach(m->{
		    	if(monitorStationId.equals(m.getStationId())){
		    		masterModelByLoginUserTemp.add(m);
				}
		    });
		    pointFlagList = pointModelByLoginUserTemp.stream().map(PointModel::getPointFlag).collect(Collectors.toList());
		    masterFlagList = masterModelByLoginUserTemp.stream().map(MasterModel::getMonitorFlag).collect(Collectors.toList());
		}else{
			pointModelByLoginUser = getResponsibleDeviceService.getPointModelByLoginUser(token);
		    masterModelByLoginUser = getResponsibleDeviceService.getMasterModelByLoginUser(token);
			pointFlagList = pointModelByLoginUser.stream().map(PointModel::getPointFlag).collect(Collectors.toList());
			masterFlagList = masterModelByLoginUser.stream().map(MasterModel::getMonitorFlag).collect(Collectors.toList());
		}
	 	if(!pointFlagList.isEmpty()&&pointFlagList!=null){
	 		deviceIds.addAll(pointFlagList);
	 	}
		if(!masterFlagList.isEmpty()&&masterFlagList!=null){
			deviceIds.addAll(masterFlagList);
	 	}
	    //log.info("pointModelByLoginUser:"+JSON.toJSONString(pointModelByLoginUser));
	 //  System.out.println("pointModelByLoginUser:"+JSON.toJSONString(pointModelByLoginUser)); 
	//   System.out.println("masterModelByLoginUser:"+JSON.toJSONString(masterModelByLoginUser)); 
		page = (page == null || page == 0) ? 0 : page - 1;
		rows = (rows == null) ? 10 : rows;
		boolean flag= (isSolve==null)?false:(Boolean.valueOf(isSolve));
		faultType=(faultType == null)?0:faultType;
		beginTime=(beginTime == null)?0l:beginTime;
		endTime=(endTime == null)?System.currentTimeMillis():endTime;
		
		List<FaultMsgDesc> faultMsgDescList = new ArrayList<FaultMsgDesc>();  
		FaultMsgDesc faultMsgDesc = null;
		if (deviceIds!=null&&!deviceIds.isEmpty()) {
			long start = System.currentTimeMillis();
			
			List<Map<String, Object>> faultInfoForPage = XWDataEntry.getFaultInfoForTimePage(deviceIds, faultType,flag, beginTime, endTime, page, rows);
			
			long end=System.currentTimeMillis();
			System.out.println("XWDataEntry.getFaultInfoForTimePage查询耗时:" + (end - start) + "ms");
			
	//		System.out.println("faultInfoForPage:" + JSON.toJSONString(faultInfoForPage));	
			if (faultInfoForPage == null) {throw new BizException(Code.NO_DATA);}
			for (Map<String, Object> faultMsg : faultInfoForPage) {
				//System.out.println("faultMsg:" + JSON.toJSONString(faultMsg));	
				faultMsgDesc = new FaultMsgDesc();
				faultMsgDesc.setAutoId(Integer.valueOf(faultMsg.get("id").toString()));
				String devid = faultMsg.get("devid").toString();
				faultMsgDesc.setDevid(devid);
				Integer faultTypes = Integer.valueOf(faultMsg.get("fault_type").toString());
				faultMsgDesc.setFaultType(faultTypes);
				faultMsgDesc.setTimes(DateUtils.unixTimestampToDate(Long.valueOf(faultMsg.get("timestamp").toString())));
				faultMsgDesc.setTimestamp(Long.valueOf(faultMsg.get("timestamp").toString()));
				faultMsgDesc.setPhase(faultMsg.get("phase")==null?0:Integer.valueOf(faultMsg.get("phase").toString()));
				// faultType:故障类型 :系统过电压:11   电量不足:23 属于中级报警 
				if (faultTypes == FaultType.FaultTypeFor11.getValue()||faultTypes == FaultType.FaultTypeFor23.getValue()) {
					faultMsgDesc.setAlarmLeve(AlarmLeve.zhongji.getName());
				} else {
					// faultType:故障类型
					// :接地故障:10,过电压:12,系统断线:13,欠压数据包:14,系统短路15,弧光接地故障16,过热20,湿度超标21,泄漏 22)
					faultMsgDesc.setAlarmLeve(AlarmLeve.gaoji.getName());
				}
				if (faultMsg.get("distimestamp") != null && faultMsg.get("distimestamp") != "") {
					String distimes= DateUtils.unixTimestampToDate(Long.valueOf(faultMsg.get("distimestamp").toString()));
					faultMsgDesc.setDistimes(distimes);
					faultMsgDesc.setDistimestamp(Long.valueOf(faultMsg.get("distimestamp").toString()));
				} 
				faultMsgDesc.setFault(Transition.changFaultTypeByTypeId(faultTypes));
				faultMsgDescList.add(faultMsgDesc);
				continue;
			}
			
			for (FaultMsgDesc fault : faultMsgDescList) {
				for (PointModel pointModel : pointModelByLoginUser) {
					if(fault.getDevid().equals(pointModel.getPointFlag())){
						fault.setMasterName(pointModel.getMasterName());
						fault.setMonitorStationName(pointModel.getStationName());
						fault.setMasterAddress(pointModel.getStationAddress());
						fault.setProtectorType(pointModel.getHitchType());
						fault.setType(pointModel.getType());
						fault.setDeviceName(pointModel.getPointName());
						fault.setBranchName(pointModel.getBranchName());
						fault.setIsSolve(flag);
						continue;
					}
				}
			}
			
			for (FaultMsgDesc fault : faultMsgDescList) {
				for (MasterModel masterModel : masterModelByLoginUser) {
					if(fault.getDevid().equals(masterModel.getMonitorFlag())){
						fault.setMasterName(masterModel.getMasterName());
						fault.setMonitorStationName(masterModel.getStationName());
						fault.setMasterAddress(masterModel.getStationAddress());
						fault.setProtectorType("智能网关");
						fault.setType("智能网关");
						fault.setDeviceName(masterModel.getMasterName());
						fault.setIsSolve(flag);
						continue;
					}
				}
			}
		//	System.out.println(JSON.toJSON(faultMsgDescList));

			List<FaultMsgDesc> list=null;
			// 1中级报警
			if (alarmLeve == AlarmLeve.zhongji.getValue()) {
			        list = new ArrayList<FaultMsgDesc>();
				for (FaultMsgDesc faultMsgDesc2 : faultMsgDescList) {
					if (faultMsgDesc2.getAlarmLeve().equals(AlarmLeve.zhongji.getName())) {
						list.add(faultMsgDesc2);
					}
				}
				data.setList(list);
				if(faultType!=FaultType.FaultTypeFor0.getValue()){
					//data.setCount(XWDataEntry.getFaultNewCount(deviceIds, faultType, flag));
					//中级报警数量
					data.setCount(XWDataEntry.getMiddFaulCountForTime(deviceIds, flag, beginTime, endTime));
					
				}else{
					int faultNewCount11 = XWDataEntry.getFaultNewCount(deviceIds, FaultType.FaultTypeFor11.getValue(), flag);
					int faultNewCount23 = XWDataEntry.getFaultNewCount(deviceIds, FaultType.FaultTypeFor23.getValue(),flag);
					data.setCount(faultNewCount11+faultNewCount23);
				}		
				 return Message.success(data);
			} else if (alarmLeve == AlarmLeve.gaoji.getValue()) { // 2高级报警
			       list = new ArrayList<FaultMsgDesc>();
				for (FaultMsgDesc faultMsgDesc2 : faultMsgDescList) {
					if (faultMsgDesc2.getAlarmLeve().equals(AlarmLeve.gaoji.getName())) {
						list.add(faultMsgDesc2);
					}
				}
				data.setList(list);
				if(faultType!=FaultType.FaultTypeFor0.getValue()){
					//data.setCount(XWDataEntry.getFaultNewCount(deviceIds, faultType, flag));
					//全部报警数
					data.setCount(XWDataEntry.getFaultCountForTime(deviceIds,faultType, flag, beginTime, endTime));
					
				}else{
					int faultNewCount10 = XWDataEntry.getFaultNewCount(deviceIds, FaultType.FaultTypeFor10.getValue(), flag);
					int faultNewCount12 = XWDataEntry.getFaultNewCount(deviceIds, FaultType.FaultTypeFor12.getValue(), flag);			
					int faultNewCount13 = XWDataEntry.getFaultNewCount(deviceIds, FaultType.FaultTypeFor13.getValue(), flag);
					int faultNewCount14 = XWDataEntry.getFaultNewCount(deviceIds, FaultType.FaultTypeFor14.getValue(), flag);
					int faultNewCount15 = XWDataEntry.getFaultNewCount(deviceIds, FaultType.FaultTypeFor15.getValue(), flag);
					int faultNewCount16 = XWDataEntry.getFaultNewCount(deviceIds, FaultType.FaultTypeFor16.getValue(), flag);
					int faultNewCount20 = XWDataEntry.getFaultNewCount(deviceIds, FaultType.FaultTypeFor20.getValue(),
							flag);
					int faultNewCount21 = XWDataEntry.getFaultNewCount(deviceIds, FaultType.FaultTypeFor21.getValue(),
							flag);
					int faultNewCount22 = XWDataEntry.getFaultNewCount(deviceIds, FaultType.FaultTypeFor22.getValue(),
							flag);
					int faultNewCount24 = XWDataEntry.getFaultNewCount(deviceIds, FaultType.FaultTypeFor24.getValue(), flag);
					data.setCount(faultNewCount10 + faultNewCount12 + faultNewCount13 + faultNewCount14
							+ faultNewCount15 + faultNewCount16+faultNewCount20+faultNewCount21+faultNewCount22+faultNewCount24);
				}				
				return Message.success(data);
			}
			// 查询全部报警信息
			data.setList(faultMsgDescList);
		   	//data.setCount(XWDataEntry.getFaultNewCount(deviceIds, faultType, flag));
			data.setCount(XWDataEntry.getFaultCountForTime(deviceIds, faultType, flag, beginTime, endTime));//按时间查询全部报警信息总条目数
			
		} else {
			throw new BizException(Code.NO_DATA);
		}
		return Message.success(data);
	}
	
	
	@ResponseBody    
	@RequestMapping("getFaultGroundInfoForPageByMonitorFlag")
	@ApiOperation(value = "后台按设备标识查询母线分页故障的基本信息")
	public Message<PageData<FaultMsgDesc>> getFaultGroundInfoForPageByMonitorFlag(
			@ApiParam(required = false, value = "母线的监测设备标识 集合：00000000000000002017,00000000000000002018") String monitorFlag,
			@ApiParam(required = false, value = "monitorStationId:监测站Id") String monitorStationId,
			@ApiParam(required = false, value = "beginTime:报警开始时间") Long beginTime,
			@ApiParam(required = false, value = "endTime:报警结束时间") Long endTime,
			@ApiParam(required = false, value = "isSolve是否已处理:false：未处理的故障;true：已处理的故障") String isSolve,
			@ApiParam(required = false, value = "faultType:故障类型 :0:全部故障,接地故障:10,系统过电压:11 ,过电压:12,系统断线:13,欠压数据包:14,系统短路15,弧光接地故障16,20,21,22,23") Integer faultType,
			@ApiParam(required = false, value = "alarmLeve：故障等级： 0全部 1中级，2高级") Integer alarmLeve,
			@ApiParam(required = false, value = "页数:默认0") Integer page,
			@ApiParam(required = false, value = "行数:默认10") Integer rows,
			@ApiParam(required = false, value = "登入验证") String token) {
		PageData<FaultMsgDesc> data = new PageData<FaultMsgDesc>();
		List<String> deviceIds=new ArrayList<>();
			if(monitorFlag!=null&&!monitorFlag.isEmpty()){
		 		deviceIds.add(monitorFlag);
		 	}
		List<PointModel>	pointModelByLoginUser = getResponsibleDeviceService.getPointModelByLoginUser(token);
		List<MasterModel>   masterModelByLoginUser = getResponsibleDeviceService.getMasterModelByLoginUser(token);
		 	
		faultType=(faultType == null)?0:faultType;
		beginTime=(beginTime == null)?0l:beginTime;
		endTime=(endTime == null)?System.currentTimeMillis():endTime;
	 	alarmLeve=(alarmLeve==null)?0:alarmLeve;
		page = (page == null || page == 0) ? 0 : page - 1;
		rows = (rows == null) ? 10 : rows;
		boolean flag = (isSolve==null)?false:Boolean.valueOf(isSolve);
		
		List<FaultMsgDesc> faultMsgDescList = new ArrayList<FaultMsgDesc>();  
		FaultMsgDesc faultMsgDesc = null;
		if (deviceIds!=null&&!deviceIds.isEmpty()) {
			List<Map<String, Object>> faultInfoForPage = XWDataEntry.getFaultInfoForTimePage(deviceIds, faultType,flag, beginTime, endTime, page, rows);
			//System.out.println("faultInfoForPage:" + JSON.toJSONString(faultInfoForPage));	
			if (faultInfoForPage == null) {throw new BizException(Code.NO_DATA);}
			for (Map<String, Object> faultMsg : faultInfoForPage) {
				//System.out.println("faultMsg:" + JSON.toJSONString(faultMsg));	
				faultMsgDesc = new FaultMsgDesc();
				faultMsgDesc.setAutoId(Integer.valueOf(faultMsg.get("id").toString()));
				String devid = faultMsg.get("devid").toString();
				faultMsgDesc.setDevid(devid);
				Integer faultTypes = Integer.valueOf(faultMsg.get("fault_type").toString());
				faultMsgDesc.setFaultType(faultTypes);
				faultMsgDesc.setTimes(DateUtils.unixTimestampToDate(Long.valueOf(faultMsg.get("timestamp").toString())));
				faultMsgDesc.setTimestamp(Long.valueOf(faultMsg.get("timestamp").toString()));
				faultMsgDesc.setPhase(Integer.valueOf(faultMsg.get("phase").toString()));
				// faultType:故障类型 :系统过电压:11   电量不足:23 属于中级报警 
				if (faultTypes == FaultType.FaultTypeFor11.getValue()||faultTypes == FaultType.FaultTypeFor23.getValue()) {
					faultMsgDesc.setAlarmLeve(AlarmLeve.zhongji.getName());
				} else {
					// faultType:故障类型
					// :接地故障:10,过电压:12,系统断线:13,欠压数据包:14,系统短路15,弧光接地故障16,过热20,湿度超标21,泄漏 22)
					faultMsgDesc.setAlarmLeve(AlarmLeve.gaoji.getName());
				}
				if (faultMsg.get("distimestamp") != null && faultMsg.get("distimestamp") != "") {
					String distimes= DateUtils.unixTimestampToDate(Long.valueOf(faultMsg.get("distimestamp").toString()));
					faultMsgDesc.setDistimes(distimes);
					faultMsgDesc.setDistimestamp(Long.valueOf(faultMsg.get("distimestamp").toString()));
				} 
				faultMsgDesc.setFault(Transition.changFaultTypeByTypeId(faultTypes));
				faultMsgDescList.add(faultMsgDesc);
				continue;
			}
			
			for (FaultMsgDesc fault : faultMsgDescList) {
				for (PointModel pointModel : pointModelByLoginUser) {
					if(fault.getDevid().equals(pointModel.getPointFlag())){
						fault.setMasterName(pointModel.getMasterName());
						fault.setMonitorStationName(pointModel.getStationName());
						fault.setMasterAddress(pointModel.getStationAddress());
						fault.setProtectorType(pointModel.getHitchType());
						fault.setType(pointModel.getType());
						continue;
					}
				}
			}
			
			for (FaultMsgDesc fault : faultMsgDescList) {
				for (MasterModel masterModel : masterModelByLoginUser) {
					if(fault.getDevid().equals(masterModel.getMonitorFlag())){
						fault.setMasterName(masterModel.getMasterName());
						fault.setMonitorStationName(masterModel.getStationName());
						fault.setMasterAddress(masterModel.getStationAddress());
						fault.setProtectorType("智能网关");
						fault.setType("智能网关");
						continue;
					}
				}
			}
		//	System.out.println(JSON.toJSON(faultMsgDescList));

			List<FaultMsgDesc> list=null;
			// 1中级报警
			if (alarmLeve == AlarmLeve.zhongji.getValue()) {
			        list = new ArrayList<FaultMsgDesc>();
				for (FaultMsgDesc faultMsgDesc2 : faultMsgDescList) {
					if (faultMsgDesc2.getAlarmLeve().equals(AlarmLeve.zhongji.getName())) {
						list.add(faultMsgDesc2);
					}
				}
				data.setList(list);
				if(faultType!=FaultType.FaultTypeFor0.getValue()){
					//data.setCount(XWDataEntry.getFaultNewCount(deviceIds, faultType, flag));
					//中级报警数量
					data.setCount(XWDataEntry.getMiddFaulCountForTime(deviceIds, flag, beginTime, endTime));
					
				}else{
					int faultNewCount11 = XWDataEntry.getFaultNewCount(deviceIds, FaultType.FaultTypeFor11.getValue(), flag);
					int faultNewCount23 = XWDataEntry.getFaultNewCount(deviceIds, FaultType.FaultTypeFor23.getValue(),flag);
					data.setCount(faultNewCount11+faultNewCount23);
				}		
				 return Message.success(data);
			} else if (alarmLeve == AlarmLeve.gaoji.getValue()) { // 2高级报警
			       list = new ArrayList<FaultMsgDesc>();
				for (FaultMsgDesc faultMsgDesc2 : faultMsgDescList) {
					if (faultMsgDesc2.getAlarmLeve().equals(AlarmLeve.gaoji.getName())) {
						list.add(faultMsgDesc2);
					}
				}
				data.setList(list);
				if(faultType!=FaultType.FaultTypeFor0.getValue()){
					//data.setCount(XWDataEntry.getFaultNewCount(deviceIds, faultType, flag));
					//全部报警数
					data.setCount(XWDataEntry.getFaultCountForTime(deviceIds,faultType, flag, beginTime, endTime));
					
				}else{
					int faultNewCount10 = XWDataEntry.getFaultNewCount(deviceIds, FaultType.FaultTypeFor10.getValue(), flag);
					int faultNewCount12 = XWDataEntry.getFaultNewCount(deviceIds, FaultType.FaultTypeFor12.getValue(), flag);			
					int faultNewCount13 = XWDataEntry.getFaultNewCount(deviceIds, FaultType.FaultTypeFor13.getValue(), flag);
					int faultNewCount14 = XWDataEntry.getFaultNewCount(deviceIds, FaultType.FaultTypeFor14.getValue(), flag);
					int faultNewCount15 = XWDataEntry.getFaultNewCount(deviceIds, FaultType.FaultTypeFor15.getValue(), flag);
					int faultNewCount16 = XWDataEntry.getFaultNewCount(deviceIds, FaultType.FaultTypeFor16.getValue(), flag);
					int faultNewCount20 = XWDataEntry.getFaultNewCount(deviceIds, FaultType.FaultTypeFor20.getValue(),
							flag);
					int faultNewCount21 = XWDataEntry.getFaultNewCount(deviceIds, FaultType.FaultTypeFor21.getValue(),
							flag);
					int faultNewCount22 = XWDataEntry.getFaultNewCount(deviceIds, FaultType.FaultTypeFor22.getValue(),
							flag);
					int faultNewCount24 = XWDataEntry.getFaultNewCount(deviceIds, FaultType.FaultTypeFor24.getValue(), flag);
					data.setCount(faultNewCount10 + faultNewCount12 + faultNewCount13 + faultNewCount14
							+ faultNewCount15 + faultNewCount16+faultNewCount20+faultNewCount21+faultNewCount22+faultNewCount24);
				}				
				return Message.success(data);
			}
			// 查询全部报警信息
			data.setList(faultMsgDescList);
		   	//data.setCount(XWDataEntry.getFaultNewCount(deviceIds, faultType, flag));
			data.setCount(XWDataEntry.getFaultCountForTime(deviceIds, faultType, flag, beginTime, endTime));//按时间查询全部报警信息总条目数
			
		} else {
			throw new BizException(Code.NO_DATA);
		}
		return Message.success(data);
	}

	/**
	 * 
	 * @param devid
	 * @param time
	 * @return
	 * 
	 */
	@ResponseBody
	@RequestMapping("getFaultOverVoltage.v1")
	@ApiOperation(value = "查询母线过电压故障的波形", httpMethod = "POST")
	public Message<?> getFaultOverVoltage(
			@ApiParam(required = false, value = "母线对应的设备id:00000000000000002017") String devid,
			@ApiParam(required = false, value = "timeEnd:时间戳:System.currentTimeMillis(),日期例子:2018-04-26 01:00:00:000") String time) {
		if (StringUtils.isNotEmpty(devid) && StringUtils.isNotEmpty(time)) {
			long time_ = DateUtils.dateToUnixTimestamp(time, "yyyy-MM-dd HH:mm:ss");
			ItemFaultData ItemFaultData = XWDataEntry.getFaultOverVoltage(devid, time_);
			return Message.success(ItemFaultData);
		} else {
			throw new BizException(Code.NOT_PARAM);
		}
	}

	/**
	 * 获取金属接地,弧光接地,系统过电压故障详情 (fault_type 故障类型 10,11,16)
	 */
	@ResponseBody
	@RequestMapping("getFaultOverVoltage.v2")
	@ApiOperation(value = "查询母线金属接地,弧光接地,系统过电压故障详情 波形(fault_type 故障类型 10,11,16)", httpMethod = "POST")
	public Message<?> getFaultInfo1(@ApiParam(required = false, value = "母线对应的设备id:00000000000000002017") String devid,
			@ApiParam(required = false, value = "timeEnd:故障发生时间戳:System.currentTimeMillis(),日期例子:2018-04-26 01:00:00:000") String time) {
		// Map<String, Object> result = new HashMap<String, Object>();// 装数据的
			/*System.out.println("time:"+time);*/
		if (StringUtils.isNotEmpty(devid) && StringUtils.isNotEmpty(time)) {
			long time_ = DateUtils.dateToUnixTimestamp(time, "yyyy-MM-dd HH:mm:ss");
			/*System.out.println("time_:"+time_);*/
			ItemFault1Data fault1Detail = XWDataEntry.getFault1Detail(devid, time_);
			return Message.success(fault1Detail);
		} else {
			throw new BizException(Code.NOT_PARAM);
		}
	}

	/**
	 * 获过电压,PT断线,欠压,系统短路,故障详情(fault_type 故障类型 12,13,14,15)
	 */
	@ResponseBody
	@RequestMapping("getFaultOverVoltage.v3")
	@ApiOperation(value = "查询母线电压,PT断线,欠压,系统短路,故障详情(fault_type 故障类型 12,13,14,15)", httpMethod = "POST")
	public Message<?> getFaultInfo2(@ApiParam(required = false, value = "母线对应的设备id:00000000000000002017") String devid,
			@ApiParam(required = false, value = "timeEnd:时间戳:System.currentTimeMillis(),日期例子:2018-04-26 01:00:00:000") String time) {
		if (StringUtils.isNotEmpty(devid) && StringUtils.isNotEmpty(time)) {
			Long time_ = DateUtils.dateToUnixTimestamp(time, "yyyy-MM-dd HH:mm:ss");
			//System.out.println(".v3time_:"+time_);
			ItemFault2Data fault2Detail = XWDataEntry.getFault2Detail(devid, time_);
			return Message.success(fault2Detail);
		} else {
			throw new BizException(Code.NOT_PARAM);
		}
	}
	
	/**
	 * 获取保护器电池电量曲线
	 */
	@ResponseBody
	@RequestMapping("getBatteryForVoltage.v1")
	@ApiOperation(value = "护器电池电量曲线", httpMethod = "POST")
	public Message<?> getBatteryForVoltage(@ApiParam(required = false, value = "母线对应的设备id:B12") String devid,
			@ApiParam(required = false, value = "timeStart:时间戳:System.currentTimeMillis(),日期例子:2018-04-26 01:00:00:000") String timeStart,
			@ApiParam(required = false, value = "timeEnd:时间戳:System.currentTimeMillis(),日期例子:2018-04-26 01:00:00:000") String timeEnd) {
		if (StringUtils.isNotEmpty(devid)&&StringUtils.isNotEmpty(timeStart)&&StringUtils.isNotEmpty(timeEnd)) {
			Long timeStarts = DateUtils.dateToUnixTimestamp(timeStart, "yyyy-MM-dd HH:mm:ss");
			Long timeEnds = DateUtils.dateToUnixTimestamp(timeEnd, "yyyy-MM-dd HH:mm:ss");
			 List<Map<String, Object>> batteryForVoltage = XWDataEntry.getBatteryForVoltage(devid, timeStarts, timeEnds);
			return Message.success(batteryForVoltage);
		} else {
			throw new BizException(Code.NOT_PARAM);
		}
	}
	

	@ResponseBody
	@RequestMapping("getConfig800")
	@ApiOperation(value = "设置初始化", httpMethod = "POST")
	@SystemLog(module="sys/getConfig800",methods = "800设置初始化")
	public Message<?> getConfig800(@ApiParam(required = false, value = "母线对应的设备id:00000000000000002017") String devid) {
		config_eric800 config800 = XWDataEntry.getConfig800(devid);
		if(config800==null){
			throw new BizException(Code.DEVICE_NOTEXIST);
		}
		return Message.success(config800);
	}
	
	/**
	 * 获取红外线测温的故障详情 对应故障类型：24
	 */
	@ResponseBody
	@RequestMapping("getFaultInfraredDetail.v1")
	@ApiOperation(value = "红外线测温的故障详情")
	public Message<?> getFaultInfraredDetail(@ApiParam(required = false, value = "母线对应的设备id:B12") String devid,
			@ApiParam(required = false, value = "time:时间戳:System.currentTimeMillis(),日期例子:2018-04-26 01:00:00:000") String time) {
		if (StringUtils.isNotEmpty(devid)&&StringUtils.isNotEmpty(time)) {
			 Long timeStarts = DateUtils.dateToUnixTimestamp(time, "yyyy-MM-dd HH:mm:ss");
			 FaultInfrared faultInfraredDetail = XWDataEntry.getFaultInfraredDetail(devid,timeStarts);
			 return Message.success(faultInfraredDetail);
		} else {
			throw new BizException(Code.NOT_PARAM);
		}
	}
		
		/**
		 * 开始获取终端红外图像
		 */
		@ResponseBody
		@RequestMapping("startCamera.v1")
		@ApiOperation(value = "获取终端红外图像")
		@SystemLog(module="sys/startCamera",methods = "获取终端红外图像")
		public Message<?> startCamera(@ApiParam(required = false, value = "设备id:B12") String devid) {
			  System.out.println("devid:-----------:"+devid);
			if (StringUtils.isNotEmpty(devid)) {
					XWDataEntry.startCamera(devid);
				 return Message.success();
			} else {
				throw new BizException(Code.NOT_PARAM);
			}
		}
	
		@ResponseBody
		@RequestMapping("getFaultGroundInfoForPage.v1")
		@ApiOperation(value = "查询分页母线故障的基本信息" ,httpMethod = "POST" )
		public Message<PageData<FaultMsgDto>> getFaultGroundInfoForPage(
				@ApiParam(required = false,  value = "母线名称:点击back13_0_line页面里的目线详情") String  masterName,
				@ApiParam(required = false,  value = "companyId:当前账户人的公司Id") String  companyId,
				@ApiParam(required = false,  value = "devids()：母线的id集合,用英文逗号表示：00000000000000002017,00000000000000002018") String  deviceIds,
				@ApiParam(required = false,  value = "isSolve是否已处理:false：未处理的故障;true：已处理的故障") String  isSolve,
				@ApiParam(required = false,  value = "页数:默认0") Integer page, 
				@ApiParam(required = false,  value = "行数:默认10") Integer rows) {
				
				PageData<FaultMsgDto> data = new PageData<FaultMsgDto>();
				StringBuilder sb = new StringBuilder();
				List<LinesOnMasterDto> linesOnMasterList = linesOnMasterService.queryLinesOnMasterIds(null,companyId,null,null,null, null);
				for (LinesOnMasterDto linesOnMasterDto : linesOnMasterList) {
					sb.append(linesOnMasterDto.getMonitorFlag()).append(",");//  获取母线标识集合
				}
				if(deviceIds == null || deviceIds.length() == 0){
					deviceIds = sb.toString();
				}
			
				page = (page == null || page==0)?0:page-1;
				rows = (rows == null)?10:rows;
				LinesOnMaster linesOnMaster = null;
				List<FaultMsgDto> faultMsgDtoList = new ArrayList<FaultMsgDto>();
				FaultMsgDto dto = null;
			//	List<FaultMsg> faultMsgList = null;	
				boolean flag = Boolean.valueOf(isSolve);
				
				//List<Map<String, Object>> faultInfoForPage = XWDataEntry.getFaultInfoForTimePage(deviceIds, 0,flag, 0, System.currentTimeMillis(), page, rows);
				if(StringUtils.isNotEmpty(deviceIds)){
					String[] devidsArr = deviceIds.split(",");
					List<String> devidsList = Arrays.asList(devidsArr);
					//faultMsgList = XWDataEntry.getFaultGroundInfoForPage(devidsList,flag,page,rows);
					List<Map<String, Object>> faultInfoForPage = XWDataEntry.getFaultInfoForTimePage(devidsList, 0,flag, 0, System.currentTimeMillis(), page, rows);
				//	for (FaultMsg faultMsg : faultMsgList) {
						for (Map<String, Object> faultMsg : faultInfoForPage) {
						dto = new FaultMsgDto();
//						dto.setAutoId(faultMsg.getId());
//						dto.setDevid(faultMsg.getDevid());
//						dto.setType(faultMsg.getType());
//						dto.setTimestamp(faultMsg.getTimestamp());
//						dto.setTimes(DateUtils.unixTimestampToDate(faultMsg.getTimestamp()));
//						dto.setValueFlag(faultMsg.getValueFlag());
						
						dto.setAutoId(Integer.valueOf(faultMsg.get("id").toString()));
						String devid = faultMsg.get("devid").toString();
						dto.setDevid(devid);
						Integer faultTypes = Integer.valueOf(faultMsg.get("fault_type").toString());
						dto.setType(faultTypes);
						dto.setTimes(DateUtils.unixTimestampToDate(Long.valueOf(faultMsg.get("timestamp").toString())));
						dto.setTimestamp(Long.valueOf(faultMsg.get("timestamp").toString()));
						//dto.setPhase(Integer.valueOf(faultMsg.get("phase").toString()));
						
						
						if(StringUtils.isNotEmpty(masterName)){
							dto.setMasterName(masterName);
							linesOnMaster = this.linesOnMasterService.getEntityByMasterName(masterName);
							dto.setMasterAddress(linesOnMaster.getMasterAddress());
						}else{
							linesOnMaster = this.linesOnMasterService.getEntityById(devid);
							dto.setMasterName(linesOnMaster.getMasterName());
							dto.setMasterAddress(linesOnMaster.getMasterAddress());
						}
						
						faultMsgDtoList.add(dto);
					}
					data.setList(faultMsgDtoList);
				//	data.setCount(XWDataEntry.getFaultCount(devidsList,flag));
					data.setCount(XWDataEntry.getFaultNewCount(devidsList, 0, flag));
				}else{
					throw new BizException(Code.NOT_PARAM);
				}
				return Message.success(data);
		}
		
		
	  public static void main(String[] args) {
		  /* config_eric800 config800 = XWDataEntry.getConfig800("ERIC08001809B0000001");
		   System.out.println(config800);
		  */
		  FaultInfrared faultInfraredDetail = XWDataEntry.getFaultInfraredDetail("ERIC08001905B0000001",1562139934000l);
		  System.out.println(JSON.toJSON(faultInfraredDetail));
		   
		   
	    }
   
	
}
