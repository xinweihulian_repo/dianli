package com.yscoco.dianli.controller.api.mobile;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import com.yscoco.dianli.common.aspect.SystemLog;
import com.yscoco.dianli.dto.app.ModelMixDto;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.batise.device.set.config_eric800;
import com.batise.device.temper.Temper_data;
import com.hyc.smart.XWDataEntry;
import com.hyc.smart.bean.ItemFault1Data;
import com.hyc.smart.bean.ItemFault2Data;
import com.hyc.smart.bean.ItemFaultData;
import com.hyc.smart.bean.ItemTHD;
import com.hyc.smart.eric420.FaultInfrared;
import com.yscoco.dianli.common.base.BaseController;
import com.yscoco.dianli.common.exception.BizException;
import com.yscoco.dianli.common.handler.PageData;
import com.yscoco.dianli.common.utils.DateUtils;
import com.yscoco.dianli.common.utils.HttpClient;
import com.yscoco.dianli.common.vo.Code;
import com.yscoco.dianli.common.vo.Message;
import com.yscoco.dianli.constants.AlarmLeve;
import com.yscoco.dianli.constants.BidConst;
import com.yscoco.dianli.constants.FaultType;
import com.yscoco.dianli.constants.WxConstant;
import com.yscoco.dianli.controller.api.ApiByEricElectricController;
import com.yscoco.dianli.dto.app.WxLoginDto;
import com.yscoco.dianli.dto.company.FaultGroundInfoDto;
import com.yscoco.dianli.dto.company.FaultMsgDesc;
import com.yscoco.dianli.dto.company.LinesOnMasterDto;
import com.yscoco.dianli.entity.member.MemberInfo;
import com.yscoco.dianli.other.cache.RedisCacheUtil;
import com.yscoco.dianli.pojo.MasterModel;
import com.yscoco.dianli.pojo.PointModel;
import com.yscoco.dianli.service.comon.GetResponsibleDeviceService;
import com.yscoco.dianli.service.company.CompanyService;
import com.yscoco.dianli.service.company.FaultGroundInfoService;
import com.yscoco.dianli.service.company.LinesOnMasterService;
import com.yscoco.dianli.service.company.ServerFaultService;
import com.yscoco.dianli.service.company.Sync800ConfigService;
import com.yscoco.dianli.service.member.MemberService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Controller
@RequestMapping("/eric/mobile/")
@Api(tags = "后台业务:7、app通过接口获取数据", description = "母线线路+母线电流+四相电+保护器+波形图", produces = MediaType.APPLICATION_JSON_VALUE, position = 1)
public class ApiMoblieEricControl extends BaseController {
	private static Logger logger = Logger.getLogger(ApiByEricElectricController.class);

	@Resource
    private RedisCacheUtil redisCache;
	
	@Autowired
	private LinesOnMasterService linesOnMasterService;

	@Autowired
	private FaultGroundInfoService faultGroundInfoService;

	@Autowired
	private Sync800ConfigService sync800ConfigService;

	@Autowired
	private ServerFaultService serverFaultService;
	@Autowired
	private CompanyService companyService;
	@Autowired
	private MemberService memberService;
	@Autowired
	private GetResponsibleDeviceService	 getResponsibleDeviceService;


	@ResponseBody
	@RequestMapping("allDeviceInfo.v1")
	public Message<List<ModelMixDto>> getAllDeviceInfo(String token,String companyId,String stationId) {

		List<ModelMixDto> res  = new ArrayList<>();
		if (StringUtils.isNotEmpty(token)) {
			ModelMixDto  mixDto =null;
			List<MasterModel> masterModelByLoginUser = getResponsibleDeviceService.getMasterModelByLoginUser(token);
				for (int i = 0; i < masterModelByLoginUser.size(); i++) {
					mixDto = new ModelMixDto();
						mixDto.setCompanyId(masterModelByLoginUser.get(i).getCompanyId());
						mixDto.setCompanyName(masterModelByLoginUser.get(i).getCompanyName());
						mixDto.setHitchType("800");
						mixDto.setId(masterModelByLoginUser.get(i).getId());
						mixDto.setMonitorFlag(masterModelByLoginUser.get(i).getMonitorFlag());
						mixDto.setName(masterModelByLoginUser.get(i).getMasterName());
						mixDto.setStationAddress(masterModelByLoginUser.get(i).getStationAddress());
						mixDto.setStationId(masterModelByLoginUser.get(i).getStationId());
						mixDto.setStationName(masterModelByLoginUser.get(i).getStationName());
						mixDto.setStationType(masterModelByLoginUser.get(i).getStationType());

					res.add(mixDto);
				}

			 List<PointModel> pointModelByLoginUser = getResponsibleDeviceService.getPointModelByLoginUser(token);
				for (int i = 0; i < pointModelByLoginUser.size(); i++) {
					mixDto = new ModelMixDto();
						mixDto.setCompanyId(pointModelByLoginUser.get(i).getCompanyId());
						mixDto.setCompanyName(pointModelByLoginUser.get(i).getCompanyName());
						mixDto.setHitchType(pointModelByLoginUser.get(i).getHitchType());
						mixDto.setId(pointModelByLoginUser.get(i).getId());
						mixDto.setMonitorFlag(pointModelByLoginUser.get(i).getPointFlag());
						mixDto.setName(pointModelByLoginUser.get(i).getPointName());
						mixDto.setStationAddress(pointModelByLoginUser.get(i).getStationAddress());
						mixDto.setStationId(pointModelByLoginUser.get(i).getStationId());
						mixDto.setStationName(pointModelByLoginUser.get(i).getStationName());
						mixDto.setStationType(pointModelByLoginUser.get(i).getStationType());

					res.add(mixDto);
				}

			return Message.success(res);
		} else {
			throw new BizException(Code.NOT_PARAM);
		}
	}

	@ResponseBody
	@RequestMapping("getTemper_data.v1")
	@ApiOperation(value = "获取温度采集器数据", httpMethod = "POST")
	public Message<?> getTemper_data(
			@ApiParam(required = false, value = "采集器的id:sensorid = B11,B27,B43") String monitorFlag,
			@ApiParam(required = false, value = "timeStart：开始时间戳") Long timeStart,
			@ApiParam(required = false, value = "timeEnd:结束时间时间戳:System.currentTimeMillis()") Long timeEnd)
			throws Exception {
	//	System.out.println("monitorFlag:" + monitorFlag);
		List<Temper_data> data = null;
		try{
			if (StringUtils.isNotEmpty(monitorFlag)) {
					data = XWDataEntry.getTemperatureList(monitorFlag, timeStart, timeEnd);
			} else {
				throw new BizException(Code.NOT_PARAM);
			}
		}catch(Exception e){
			data = new ArrayList<>();
			logger.error(e.getMessage());
		}
		//logger.info("获取温度采集器最新的数据:getTemper_data:" + JSON.toJSONString(data));
		return Message.success(data);
	}

	// ==================== 从接口那里获取母线相关数据：获取母线相关数据
	@ResponseBody
	@RequestMapping("getEricDataForIds.v1")
	@ApiOperation(value = "获取母线线路详情", httpMethod = "POST")
	public Message<List<Map<String, Object>>> getEricDataForIds(
			@ApiParam(required = false, value = "母线的id集合:用逗号隔开:母线ID例子：00000000000000002017") String deviceIds) {
		List<Map<String, Object>> ericDataList = null;
		if (StringUtils.isNotEmpty(deviceIds)) {
			String[] devidsArr = deviceIds.split(",");
			List<String> devidsList = Arrays.asList(devidsArr);
			ericDataList = XWDataEntry.getEricDataForIds(devidsList);
		} else {
			throw new BizException(Code.NOT_PARAM);
		}
		return Message.success(ericDataList);
	}

	@ResponseBody
	@RequestMapping("getEricElectric.v1")
	@ApiOperation(value = "根据id查询母线四相电流波形", httpMethod = "POST")
	public Message<List<Map<String, Object>>> getEricElectric(
			@ApiParam(required = false, value = "母线的id:00000000000000002017") String deviceId,
			@ApiParam(required = false, value = "timeStart：开始时间戳") Long timeStart,
			@ApiParam(required = false, value = "timeEnd:结束时间时间戳:System.currentTimeMillis()") Long timeEnd)
			throws Exception {
		List<Map<String, Object>> ericElectricList = null;
		if (StringUtils.isNotEmpty(deviceId)) {
			ericElectricList = XWDataEntry.getEricElectric(deviceId, timeStart, timeEnd); // DateUtils.unixTimestampToDate(1524143300898l)
			for (Map<String, Object> map : ericElectricList) {
//				map.put("Electric_0", map.get("Electric_0").toString().equalsIgnoreCase("0.0") ? 0
//						: Double.valueOf(map.get("Electric_0").toString()) * BidConst.ERIC_CONSTANT);
				map.put("Electric_0", map.get("Electric_0").toString().equalsIgnoreCase("0.0") ? 0
						: Double.valueOf(map.get("Electric_0").toString()));
				map.put("Electric_a", map.get("Electric_a").toString().equalsIgnoreCase("0.0") ? 0
						: Double.valueOf(map.get("Electric_a").toString()) * BidConst.ERIC_CONSTANT);
				map.put("Electric_b", map.get("Electric_b").toString().equalsIgnoreCase("0.0") ? 0
						: Double.valueOf(map.get("Electric_b").toString()) * BidConst.ERIC_CONSTANT);
				map.put("Electric_c", map.get("Electric_c").toString().equalsIgnoreCase("0.0") ? 0
						: Double.valueOf(map.get("Electric_c").toString()) * BidConst.ERIC_CONSTANT);
			}
		} else {
			throw new BizException(Code.NOT_PARAM);
		}
		return Message.success(ericElectricList);
	}
	
	
	@ResponseBody
	@RequestMapping("getEricVoltage.v1")
	@ApiOperation(value = "根据id查询母线四相电压波形", httpMethod = "POST")
	public Message<List<Map<String, Object>>> getEricVoltage(
			@ApiParam(required = false, value = "母线的id:00000000000000002017") String deviceId,
			@ApiParam(required = false, value = "timeStart：开始时间戳") Long timeStart,
			@ApiParam(required = false, value = "timeEnd:结束时间时间戳:System.currentTimeMillis()") Long timeEnd)
			throws Exception {
		List<Map<String, Object>> ericVoltageList = null;
		if (StringUtils.isNotEmpty(deviceId)) {
			ericVoltageList = XWDataEntry.getEricVoltage(deviceId, timeStart, timeEnd);
			for (Map<String, Object> map : ericVoltageList) {
//				map.put("voltage_0", map.get("voltage_0").toString().equalsIgnoreCase("0.0") ? 0
//						: Double.valueOf(map.get("voltage_0").toString()) * BidConst.VOL_CONSTANT);
				map.put("voltage_0", map.get("voltage_0").toString().equalsIgnoreCase("0.0") ? 0
						: Double.valueOf(map.get("voltage_0").toString()));
				map.put("voltage_a", map.get("voltage_a").toString().equalsIgnoreCase("0.0") ? 0
						: Double.valueOf(map.get("voltage_a").toString()) * BidConst.VOL_CONSTANT);
				map.put("voltage_b", map.get("voltage_b").toString().equalsIgnoreCase("0.0") ? 0
						: Double.valueOf(map.get("voltage_b").toString()) * BidConst.VOL_CONSTANT);
				map.put("voltage_c", map.get("voltage_c").toString().equalsIgnoreCase("0.0") ? 0
						: Double.valueOf(map.get("voltage_c").toString()) * BidConst.VOL_CONSTANT);
			}
		} else {
			throw new BizException(Code.NOT_PARAM);
		}
		return Message.success(ericVoltageList);
	}

	// ==================== 从接口那里获取母线相关数据：获取保护器相关数据
	@ResponseBody
	@RequestMapping("getActionCount.v1")
	@ApiOperation(value = "获取支线保护器的动作总次数", httpMethod = "POST")
	public Message<List<Map<String, Object>>> getActionCount(
			@ApiParam(required = false, value = "采集器的id集合：用逗号隔开,sensorid = B11,B27,B43") String sensorIds) {
		List<Map<String, Object>> actionCountList = null;
		if (StringUtils.isNotEmpty(sensorIds)) {
			String[] sensorIdsArr = sensorIds.split(",");
			List<String> sensorIdsList = Arrays.asList(sensorIdsArr);
			actionCountList = XWDataEntry.getActionCount(sensorIdsList);
		} else {
			throw new BizException(Code.NOT_PARAM);
		}
		return Message.success(actionCountList);
	}

	@ResponseBody
	@RequestMapping("getEricTHDMaxAndMinByABC.v1")
	@ApiOperation(value = "获取母线的电压谐波和电流谐波最大值+最小值", httpMethod = "POST")
	public Message<Map<String, Object>> getEricTHDMaxAndMinByABC(
			@ApiParam(required = false, value = "母线的id:00000000000000002017") String deviceId,
			@ApiParam(required = false, value = "timeStart：开始时间戳") Long timeStart,
			@ApiParam(required = false, value = "timeEnd:结束时间时间戳:System.currentTimeMillis()") Long timeEnd)
			throws Exception {

		List<Map<String, Object>> ericmaxMinList = null;

		if (StringUtils.isNotEmpty(deviceId)) {
			List<String> ids = new ArrayList<>();
			ids.add(deviceId);
			ericmaxMinList = XWDataEntry.getMaxMinValueEric(ids, timeStart, timeEnd);
		} else {
			throw new BizException(Code.NOT_PARAM);
		}
		// 组织数据
		Map<String, Object> result = new HashMap<>();
		this.method1(ericmaxMinList, result);
		return Message.success(result);
	}

	private Map<String, Object> method1(List<Map<String, Object>> ericMaxMinList, Map<String, Object> result) {

		if (ericMaxMinList == null || ericMaxMinList.size() == 0)
			return result;

		Float Voltage_A_Max = Float.parseFloat(ericMaxMinList.get(0).get("voltage_max_a").toString());
		Float Voltage_A_Min = Float.parseFloat(ericMaxMinList.get(0).get("voltage_min_a").toString());
		Float Voltage_A_Average = Float.parseFloat(ericMaxMinList.get(0).get("voltage_avg_a").toString());

		Float Voltage_B_Max = Float.parseFloat(ericMaxMinList.get(0).get("voltage_max_b").toString());
		Float Voltage_B_Min = Float.parseFloat(ericMaxMinList.get(0).get("voltage_min_b").toString());
		Float Voltage_B_Average = Float.parseFloat(ericMaxMinList.get(0).get("voltage_avg_b").toString());

		Float Voltage_C_Max = Float.parseFloat(ericMaxMinList.get(0).get("voltage_max_c").toString());
		Float Voltage_C_Min = Float.parseFloat(ericMaxMinList.get(0).get("voltage_min_c").toString());
		Float Voltage_C_Average = Float.parseFloat(ericMaxMinList.get(0).get("voltage_avg_c").toString());

		// result.put("Voltage_A_Max", Voltage_A_Max * BidConst.VOL_CONSTANT);
		result.put("Voltage_A_Max", Voltage_A_Max);
		result.put("Voltage_A_Min", Voltage_A_Min);
		result.put("Voltage_A_Average", Voltage_A_Average);

		result.put("Voltage_B_Max", Voltage_B_Max);
		result.put("Voltage_B_Min", Voltage_B_Min);
		result.put("Voltage_B_Average", Voltage_B_Average);

		result.put("Voltage_C_Max", Voltage_C_Max);
		result.put("Voltage_C_Min", Voltage_C_Min);
		result.put("Voltage_C_Average", Voltage_C_Average);

		Float Electric_A_Max = Float.parseFloat(ericMaxMinList.get(0).get("Electric_max_a").toString());
		Float Electric_A_Min = Float.parseFloat(ericMaxMinList.get(0).get("Electric_min_a").toString());
		Float Electric_A_Average = Float.parseFloat(ericMaxMinList.get(0).get("Electric_avg_a").toString());

		Float Electric_B_Max = Float.parseFloat(ericMaxMinList.get(0).get("Electric_max_b").toString());
		Float Electric_B_Min = Float.parseFloat(ericMaxMinList.get(0).get("Electric_min_b").toString());
		Float Electric_B_Average = Float.parseFloat(ericMaxMinList.get(0).get("Electric_avg_b").toString());

		Float Electric_C_Max = Float.parseFloat(ericMaxMinList.get(0).get("Electric_max_c").toString());
		Float Electric_C_Min = Float.parseFloat(ericMaxMinList.get(0).get("Electric_min_c").toString());
		Float Electric_C_Average = Float.parseFloat(ericMaxMinList.get(0).get("Electric_avg_c").toString());

		result.put("Electric_A_Max", Electric_A_Max);
		result.put("Electric_A_Min", Electric_A_Min);
		result.put("Electric_A_Average", Electric_A_Average);

		result.put("Electric_B_Max", Electric_B_Max);
		result.put("Electric_B_Min", Electric_B_Min);
		result.put("Electric_B_Average", Electric_B_Average);

		result.put("Electric_C_Max", Electric_C_Max);
		result.put("Electric_C_Min", Electric_C_Min);
		result.put("Electric_C_Average", Electric_C_Average);

		return result;
	}

	@ResponseBody
	@RequestMapping("getEricVoltageTHD.v1")
	@ApiOperation(value = "获取母线的电压谐波波形", httpMethod = "POST")
	public Message<List<ItemTHD>> getEricVoltageTHD(
			@ApiParam(required = false, value = "母线的id:00000000000000002017") String deviceId,
			@ApiParam(required = false, value = "timeStart：开始时间戳") Long timeStart,
			@ApiParam(required = false, value = "timeEnd:结束时间时间戳:System.currentTimeMillis()") Long timeEnd)
			throws Exception {
	//	long startTime_ = System.currentTimeMillis();

		List<ItemTHD> ericVoltageTHDList = null;
		if (StringUtils.isNotEmpty(deviceId)) {
			ericVoltageTHDList = XWDataEntry.getEricVoltageTHD(deviceId, timeStart, timeEnd);
			if (ericVoltageTHDList == null) {
				throw new BizException(Code.NO_DATAS);
			}
			for (ItemTHD itemTHD : ericVoltageTHDList) {
				for (int i = 0; i < itemTHD.getParamA().length; i++) {
					itemTHD.getParamA()[i] *= BidConst.VOL_CONSTANT;
				}
				for (int i = 0; i < itemTHD.getParamB().length; i++) {
					itemTHD.getParamB()[i] *= BidConst.VOL_CONSTANT;
				}
				for (int i = 0; i < itemTHD.getParamC().length; i++) {
					itemTHD.getParamC()[i] *= BidConst.VOL_CONSTANT;
				}
			}
		}
	//	long endTime_ = System.currentTimeMillis();
	//	System.out.println("=============请求接口的时间：" + (endTime_ - startTime_));
		return Message.success(ericVoltageTHDList);
	}

	@ResponseBody
	@RequestMapping("getEricElectricTHD.v1")
	@ApiOperation(value = "获取母线的电流谐波波形", httpMethod = "POST")
	public Message<List<ItemTHD>> getEricElectricTHD(
			@ApiParam(required = false, value = "母线的id:00000000000000002017") String deviceId,
			@ApiParam(required = false, value = "timeStart：开始时间戳") Long timeStart,
			@ApiParam(required = false, value = "timeEnd:结束时间时间戳:System.currentTimeMillis()") Long timeEnd)
			throws Exception {
	//	long startTime_ = System.currentTimeMillis();
		List<ItemTHD> ericElectricTHDList = null;
		if (StringUtils.isNotEmpty(deviceId)) {
			ericElectricTHDList = XWDataEntry.getEricElectricTHD(deviceId, timeStart, timeEnd);
			if (ericElectricTHDList == null) {
				throw new BizException(Code.NO_DATAS);
			}
			for (ItemTHD itemTHD : ericElectricTHDList) {
				for (int i = 0; i < itemTHD.getParamA().length; i++) {
					itemTHD.getParamA()[i] *= BidConst.ERIC_CONSTANT;
				}
				for (int i = 0; i < itemTHD.getParamB().length; i++) {
					itemTHD.getParamB()[i] *= BidConst.ERIC_CONSTANT;
				}
				for (int i = 0; i < itemTHD.getParamC().length; i++) {
					itemTHD.getParamC()[i] *= BidConst.ERIC_CONSTANT;
				}
			}
		}
		//long endTime_ = System.currentTimeMillis();
	//	System.out.println("==============请求接口的时间：" + (endTime_ - startTime_));
		return Message.success(ericElectricTHDList);
	}

	@ResponseBody
	@RequestMapping("getTemperatureForVoltage.v1")
	@ApiOperation(value = "获取过电压保护器的温度波形", httpMethod = "POST")
	public Message<List<Map<String, Object>>> getTemperatureForVoltage(
			@ApiParam(required = false, value = "采集器的id,sensorid = B11,B27,B43") String sensorId,
			@ApiParam(required = false, value = "timeStart：开始时间戳") Long timeStart,
			@ApiParam(required = false, value = "timeEnd:结束时间时间戳:System.currentTimeMillis()") Long timeEnd)
			throws Exception {
	//	System.out.println("getTemperatureForVoltage.v1:" + "执行了");
		List<Map<String, Object>> temperatureForVoltageList = null;
		if (StringUtils.isNotEmpty(sensorId)) {
			temperatureForVoltageList = XWDataEntry.getTemperatureForVoltage(sensorId, timeStart, timeEnd);
		} else {
			throw new BizException(Code.NOT_PARAM);
		}
		return Message.success(temperatureForVoltageList);// Double.valueOf(map.get("temper_a").toString())
	}

	public double mul(String value) {
		BigDecimal b1 = new BigDecimal(value);
		BigDecimal b2 = new BigDecimal(10);
		BigDecimal b3 = new BigDecimal(500);
		return (b1.subtract(b3)).divide(b2, 3, BigDecimal.ROUND_HALF_UP).doubleValue();
	}

	@ResponseBody
	@RequestMapping("gethumidityForVoltage.v1")
	@ApiOperation(value = "获取过电压保护器的湿度波形", httpMethod = "POST")
	public Message<List<Map<String, Object>>> gethumidityForVoltage(
			@ApiParam(required = false, value = "采集器的id,sensorid = B11,B27,B43") String sensorId,
			@ApiParam(required = false, value = "timeStart：开始时间戳") Long timeStart,
			@ApiParam(required = false, value = "timeEnd:结束时间时间戳:System.currentTimeMillis()") Long timeEnd)
			throws Exception {
		List<Map<String, Object>> humidityForVoltageList = null;
		if (StringUtils.isNotEmpty(sensorId)) {
			humidityForVoltageList = XWDataEntry.gethumidityForVoltage(sensorId, timeStart, timeEnd);
		} else {
			throw new BizException(Code.NOT_PARAM);
		}
		return Message.success(humidityForVoltageList);
	}

	@ResponseBody
	@RequestMapping("getAcionCountForValue.v1")
	@ApiOperation(value = "获取保护器的所有相位动作次数", httpMethod = "POST")
	public Message<List<Map<String, Object>>> getAcionCountForValue(
			@ApiParam(required = false, value = "采集器的id,sensorid = B11,B27,B43") String sensorId,
			@ApiParam(required = false, value = "timeStart：开始时间戳") Long timeStart,
			@ApiParam(required = false, value = "timeEnd:结束时间时间戳:System.currentTimeMillis()") Long timeEnd)
			throws Exception {
		List<Map<String, Object>> acionCountForValueList = null;
		if (StringUtils.isNotEmpty(sensorId)) {
			acionCountForValueList = XWDataEntry.getAcionCountForValue(sensorId, timeStart, timeEnd);
		} else {
			throw new BizException(Code.NOT_PARAM);
		}
		return Message.success(acionCountForValueList);
	}

	@ResponseBody
	@RequestMapping("getAcionTimesForValue.v1")
	@ApiOperation(value = "查询保护器某个相别的动作时长波形", httpMethod = "POST")
	public Message<List<Map<String, Object>>> getAcionTimesForValue(
			@ApiParam(required = false, value = "采集器的id,sensorid = B11,B27,B43") String sensorId,
			@ApiParam(required = false, value = "flag相别：1：A相； 2：B相；4：C相") Integer flag,
			@ApiParam(required = false, value = "timeStart：开始时间戳") Long timeStart,
			@ApiParam(required = false, value = "timeEnd:结束时间时间戳:System.currentTimeMillis()") Long timeEnd) {
		List<Map<String, Object>> acionTimesForValueList = null;
		if (StringUtils.isNotEmpty(sensorId)) {
			acionTimesForValueList = XWDataEntry.getAcionTimesForValue(sensorId, flag, timeStart, timeEnd);
		} else {
			throw new BizException(Code.NOT_PARAM);
		}
		return Message.success(acionTimesForValueList);
	}

	// 2018年4月20号 ： List<Map<String, Object>> map =
	// getLeakElecForId("B11",0l,System.currentTimeMillis());
	@ResponseBody
	@RequestMapping("getLeakElecForId.v1")
	@ApiOperation(value = "查询保护器泄露电流波形", httpMethod = "POST")
	public Message<List<Map<String, Object>>> getLeakElecForId(
			@ApiParam(required = false, value = "采集器的id:sensorid = B11,B27,B43") String sensorId,
			@ApiParam(required = false, value = "timeStart：开始时间戳") Long timeStart,
			@ApiParam(required = false, value = "timeEnd:结束时间时间戳:System.currentTimeMillis()") Long timeEnd)
			throws Exception {
		List<Map<String, Object>> leakElecForIdList = null;
		if (StringUtils.isNotEmpty(sensorId)) {
			leakElecForIdList = XWDataEntry.getLeakElecForId(sensorId, timeStart, timeEnd);
		} else {
			throw new BizException(Code.NOT_PARAM);
		}
		return Message.success(leakElecForIdList);
	}

	@ResponseBody
	@RequestMapping("getFaultOverVoltage.v1")
	@ApiOperation(value = "查询母线过电压故障的波形", httpMethod = "POST")
	public Message<?> getFaultOverVoltage(
			@ApiParam(required = false, value = "母线对应的设备id:00000000000000002017") String devid,
			@ApiParam(required = false, value = "timeEnd:时间戳:System.currentTimeMillis()") Long time) {
		if (StringUtils.isNotEmpty(devid)) {
			ItemFaultData ItemFaultData = XWDataEntry.getFaultOverVoltage(devid, time);
			return Message.success(ItemFaultData);
		} else {
			throw new BizException(Code.NOT_PARAM);
		}
	}

	@SuppressWarnings("unused")
	private void baseData(Map<String, Object> result, short[][] baseData, String key, StringBuffer sb) {
		sb.setLength(0);
		sb = new StringBuffer();
		for (int i = 0; i < baseData.length; i++) {
			for (int j = 0; j < baseData[i].length; j++) {
				sb.append(baseData[i][j]).append(",");
			}

		}
		// for (short[] s : baseData) {
		// Arrays.sort(s);
		// short a = (short) ((s[s.length - 1] + s[0]) / 2);
		// sb.append(a).append(",");
		// }
		// if (sb.length() > 0) {
		// sb.deleteCharAt(sb.length() - 1);
		// }
		result.put(key, sb.toString());
	}

	@ResponseBody
	@RequestMapping("getFaultGround.v1")
	@ApiOperation(value = "查询母线接地故障的波形", httpMethod = "POST")
	public Message<ItemFaultData> getFaultGround(
			@ApiParam(required = false, value = "母线对应的设备id:00000000000000002017") String devid,
			@ApiParam(required = false, value = "timeEnd:时间戳:System.currentTimeMillis()") long time) {
		ItemFaultData ItemFaultData = null;
		if (StringUtils.isNotEmpty(devid)) {
			ItemFaultData = XWDataEntry.getFaultGround(devid, time);
		} else {
			throw new BizException(Code.NOT_PARAM);
		}
		return Message.success(ItemFaultData);
	}

	@ResponseBody
	@RequestMapping("queryServerFaultIds.v1")
	@ApiOperation(value = "查询故障提醒消息列表", httpMethod = "POST")
	public Message<PageData<String>> queryServerFaultIds(@ApiParam("页码") Integer page, @ApiParam("页数") Integer rows) {
		PageData<String> faultIds_onDeviceIds = serverFaultService.queryServerFaultIds(page, rows);
		return Message.success(faultIds_onDeviceIds);
	}

	@ResponseBody
	@RequestMapping("updateFaultState.v1")
	@SystemLog(module="/eric/mobile/updateFaultState",methods = "解除报警接")
	public Message<?> updateFaultState(
			@ApiParam(required = false, value = "type: 故障类型；1:保护器异常，2:过电压故障，3：接地故障") String type,
			@ApiParam(required = false, value = "id: 数据库中的自增id(即数据Id)") String id) {
		// int result = XWDataEntry.updateFaultState(Integer.valueOf(type),
		// Integer.valueOf(id));
		try {
			XWDataEntry.updateFaultStateNew(Integer.parseInt(id));
		} catch (Exception e) {
			throw new BizException(Code.SYS_ERR);
		}
		return Message.success();
	}



	@ResponseBody
	@RequestMapping("Sync800Config.v1")
	@ApiOperation(value = "同步设置数据到终端", httpMethod = "POST")
	public Message<?> Sync800Config(@ApiParam(required = false, value = "自动生成的母线的id") String sync_By_linesOnMasterId,
			@ApiParam(required = false, value = "设备Id:标识") String monitorFlag,
			@ApiParam(required = false, value = "线路温度最大值") String tempMax_Line,
			@ApiParam(required = false, value = "线路温度最小值") String tempMin_Line,
			@ApiParam(required = false, value = "保护器温度最大值") String tempMax_Pro,
			@ApiParam(required = false, value = "保护器温度最小值") String tempMin_Pro,
			@ApiParam(required = false, value = "湿度最大值") String humdityMax,
			@ApiParam(required = false, value = "湿度最小值") String humdityMin,
			@ApiParam(required = false, value = "泄露电流最大值") String leakageCurMax,
			@ApiParam(required = false, value = "电压门限系数") String modulus,
			@ApiParam(required = false, value = "一次额定电压") String maxRateValue,
			@ApiParam(required = false, value = "PT") String ratioPT,
			@ApiParam(required = false, value = "CT") String ratioCT,
			@ApiParam(required = false, value = "传动实验；1:开启；2:关闭") String driveTestFlag,
			@ApiParam(required = false, value = "就地和远方；1:就地；2:远方") String distanceFlag) {
		config_eric800 entity = new config_eric800();
			entity.setDevid(monitorFlag);// 设备标识
			entity.setTempMax_Line(Float.valueOf(tempMax_Line));
			entity.setTempMin_Line(Float.valueOf(tempMin_Line));
			entity.setTempMax_Pro(Float.valueOf(tempMax_Pro));
			entity.setTempMin_Pro(Float.valueOf(tempMin_Pro));
			entity.setHumdityMax(Float.valueOf(humdityMax));
			entity.setHumdityMin(Float.valueOf(humdityMin));
			entity.setLeakageCurMax(Float.valueOf(leakageCurMax));
			entity.setModulus(Float.valueOf(modulus));
			entity.setMaxRateValue(Integer.valueOf(maxRateValue));
			entity.setRatioPT(Integer.valueOf(ratioPT));
			entity.setRatioCT(Integer.valueOf(ratioCT));
			entity.setDriveTestFlag(Byte.valueOf(driveTestFlag));
			entity.setDistanceFlag(Byte.valueOf(distanceFlag));
		XWDataEntry.Sync800Config(entity);
		this.sync800ConfigService.saveSync800Config(entity, sync_By_linesOnMasterId);// 作为一次我们这边同步记录的一次备份
		return Message.success(entity);
	}

	// app端产生数据交互的部分
	@ResponseBody
	@RequestMapping("addOrUpdateFaultGroundInfo.v1")
	@ApiOperation(value = "故障处理记录信息提交", httpMethod = "POST")
	@SystemLog(module="/eric/mobile/addOrUpdateFaultGroundInfo",methods = "故障处理记录信息提交")
	public Message<?> addOrUpdateFaultGroundInfo(
			@ApiParam(required = false, value = "故障处理记录信息Id") String faultGroundInfoId,
			@ApiParam(required = false, value = "母线Id") String deviceId,
			@ApiParam(required = false, value = "故障信息自增Id") String autoId,
			@ApiParam(required = false, value = "记录填写") String recordMessage,
			@ApiParam(required = false, value = "故障类型；1:保护器异常，2:过电压故障，3：接地故障") String type,
			@ApiParam(required = false, value = "故障发生的时间戳") String timestamp,
			@ApiParam(required = false, value = "发生故障的相位；1：A相； 2：B相；4：C相") String valueFlag) {
		return this.faultGroundInfoService.addOrUpdateFaultGroundInfo(faultGroundInfoId, deviceId, autoId,
				recordMessage, type, timestamp, valueFlag, this.getLoginMemberId());
	}

	@ResponseBody
	@RequestMapping("queryFaultGroundInfoList.v1")
	@ApiOperation(value = "查询故障处理记录信息列表", httpMethod = "POST")
	public Message<PageData<FaultGroundInfoDto>> queryFaultGroundInfoList(
			@ApiParam("消息id:查看详情") String faultGroundInfoId,
			@ApiParam(required = false, value = "故障信息自增Id") String autoId, @ApiParam("设备Id") String deviceId,
			@ApiParam("页码") Integer page, @ApiParam("页数") Integer rows) {
		PageData<FaultGroundInfoDto> pageData = faultGroundInfoService.listPage(faultGroundInfoId, autoId, deviceId,
				page, rows);
		return Message.success(pageData);
	}

	

	/**
	 * 获取金属接地,弧光接地,系统过电压故障详情 (fault_type 故障类型 10,11,16)
	 */
	@ResponseBody
	@RequestMapping("getFaultOverVoltage.v2")
	@ApiOperation(value = "查询母线金属接地,弧光接地,系统过电压故障详情 波形(fault_type 故障类型 10,11,16)", httpMethod = "POST")
	public Message<?> getFaultInfo1(@ApiParam(required = false, value = "母线对应的设备id:00000000000000002017") String devid,
			@ApiParam(required = false, value = "timeEnd:时间戳:System.currentTimeMillis(),") Long time) {
		if (StringUtils.isNotEmpty(devid)) {
			ItemFault1Data fault1Detail = XWDataEntry.getFault1Detail(devid, time);
			return Message.success(fault1Detail);
		} else {
			throw new BizException(Code.NOT_PARAM);
		}
	}

	/**
	 * 获过电压,PT断线,欠压,系统短路,故障详情(fault_type 故障类型 12,13,14,15)
	 */
	@ResponseBody
	@RequestMapping("getFaultOverVoltage.v3")
	@ApiOperation(value = "查询母线电压,PT断线,欠压,系统短路,故障详情(fault_type 故障类型 12,13,14,15)", httpMethod = "POST")
	public Message<?> getFaultInfo2(@ApiParam(required = false, value = "母线对应的设备id:00000000000000002017") String devid,
			@ApiParam(required = false, value = "timeEnd:时间戳:System.currentTimeMillis(),") Long time) {
		if (StringUtils.isNotEmpty(devid)) {
			ItemFault2Data fault2Detail = XWDataEntry.getFault2Detail(devid, time);
			return Message.success(fault2Detail);
		} else {
			throw new BizException(Code.NOT_PARAM);
		}
	}

	/**
	 * 获取一段时间所有设备分类故障的总数
	 */
	@ResponseBody
	@RequestMapping("getAllCountByFaultType.v1")
	@ApiOperation(value = "获取一段时间所有设备未处理状态分类故障的总数", httpMethod = "POST")
	public Message<?> getAllCountByFaultType(@ApiParam(required = false, value = "当前登陆的用户id") String userId,
			@ApiParam(required = false, value = "timeStart:时间戳:精确到毫秒,") Long timeStart,
			@ApiParam(required = false, value = "timeEnd:时间戳:System.currentTimeMillis(),") Long timeEnd
	,String token) {
		Map<String, Object> resultData = new HashMap<>();
		List<String> responsibleDevice = new ArrayList<>();

		MemberInfo member = memberService.getById(userId);
		String[] monitorStationIds = member.getMonitorStationId().split(",");
		int monitorStationCount = monitorStationIds.length;// 监测站数量

		List<MasterModel> masterModelByLoginUser = getResponsibleDeviceService.getMasterModelByLoginUser(token);
		List<PointModel> pointModelByLoginUser = getResponsibleDeviceService.getPointModelByLoginUser(token);
	    List<String> masterMonitorFlag = masterModelByLoginUser.stream().map(MasterModel::getMonitorFlag).collect(Collectors.toList());
	    List<String> pointMonitorFlag = pointModelByLoginUser.stream().map(PointModel::getPointFlag).collect(Collectors.toList());

		responsibleDevice.addAll(masterMonitorFlag);
		responsibleDevice.addAll(pointMonitorFlag);

		timeStart=(timeStart==null||timeStart.equals(""))?0l:timeStart;
		timeEnd=(timeEnd==null||timeEnd.equals(""))?System.currentTimeMillis():timeEnd;
		// 报警统计 获取一段时间所有设备分类故障的已处理 未处理 总数 isSolve false -未处理 true-已处理
		List<Map<String, Object>> allCountByFaultType = XWDataEntry.getAllCountByFaultTypeAndStatus(responsibleDevice, false, timeStart,timeEnd);

		if(allCountByFaultType!=null&&!allCountByFaultType.isEmpty()){
			Integer sumCount = 0;
			for (Map<String, Object> m : allCountByFaultType) {
				sumCount += Integer.valueOf(m.get("cnt").toString());
			}
			resultData.put("sumCount", sumCount);
			resultData.put("allCountByFaultType", allCountByFaultType);
		}else{
			resultData.put("sumCount", 0);
			resultData.put("allCountByFaultType", new ArrayList<Map<String,Object>>());
		}
		resultData.put("monitorStationCount", monitorStationCount);

//		List<String> responsibleMasterList = new ArrayList<>(); // 所负责的母线设备标识
//		Map<String, Object> resultData = new HashMap<>(); //
//		MemberInfo member = memberService.getById(userId);
//		List<MasterModel> masterList = this.companyService.queryAllMasterFlag(member.getCompanyId());
//		List<PointModel> pointList = this.companyService.queryAllPointFlag(member.getCompanyId());
//		String[] monitorStationIds = member.getMonitorStationId().split(",");
//		int monitorStationCount = monitorStationIds.length;// 监测站数量
//		if(masterList!=null&&!masterList.isEmpty()){
//			for (String ids : monitorStationIds) {
//				for (MasterModel master : masterList) {
//					if (ids.equalsIgnoreCase(master.getStationId())) {
//						// 获取所负责的母线集合
//						responsibleMasterList.add(master.getMonitorFlag());
//						continue;
//					}
//				}
//			}
//		}
//		if(pointList!=null&&!pointList.isEmpty()){
//			for (String ids : monitorStationIds) {
//				for (PointModel point : pointList) {
//					if (ids.equalsIgnoreCase(point.getStationId())) {
//						// 获取所负责的监测点集合
//						responsibleMasterList.add(point.getPointFlag());
//						continue;
//					}
//				}
//			}
//		}
//		timeStart=(timeStart==null||timeStart.equals(""))?0l:timeStart;
//		timeEnd=(timeEnd==null||timeEnd.equals(""))?System.currentTimeMillis():timeEnd;
//		// 报警统计 获取一段时间所有设备分类故障的已处理 未处理 总数 isSolve false -未处理 true-已处理
//		List<Map<String, Object>> allCountByFaultType = XWDataEntry.getAllCountByFaultTypeAndStatus(responsibleMasterList, false, timeStart,timeEnd);
//
//		if(allCountByFaultType!=null&&!allCountByFaultType.isEmpty()){
//				Integer sumCount = 0;
//				for (Map<String, Object> m : allCountByFaultType) {
//					sumCount += Integer.valueOf(m.get("cnt").toString());
//				}
//				resultData.put("sumCount", sumCount);
//				resultData.put("allCountByFaultType", allCountByFaultType);
//			}else{
//				resultData.put("sumCount", 0);
//				resultData.put("allCountByFaultType", new ArrayList<Map<String,Object>>());
//			}
//		resultData.put("monitorStationCount", monitorStationCount);
		
		return Message.success(resultData);
	}

	@ResponseBody
	@RequestMapping("getFaultGroundInfoForPageByConditions")
	@ApiOperation(value = "按条件查询分页母线故障的基本信息", httpMethod = "POST")
	public Message<PageData<FaultMsgDesc>> getFaultGroundInfoForPageByConditions(
			@ApiParam(required = false, value = "companyId:当前账户人的公司Id") String companyId,
			@ApiParam(required = false, value = "母线名称:点击back13_0_line页面里的目线详情") String masterName,
			@ApiParam(required = false, value = "母线的监测设备标识 集合：00000000000000002017,00000000000000002018") String monitorFlag,
			@ApiParam(required = false, value = "monitorStationId:监测站Id") String monitorStationId,
			@ApiParam(required = false, value = "母线名称用于搜索") String keyQuery,
			@ApiParam(required = false, value = "beginTime:报警开始时间") Long beginTime,
			@ApiParam(required = false, value = "endTime:报警结束时间") Long endTime,
			@ApiParam(required = false, value = "isSolve是否已处理:false：未处理的故障;true：已处理的故障") String isSolve,
			@ApiParam(required = false, value = "faultType:故障类型 :0全部类型,接地故障:10,系统过电压:11 ,过电压:12,系统断线:13,欠压数据包:14,系统短路15,弧光接地故障16,过热20,湿度超标21,泄漏22,电量不足23") Integer faultType,
			@ApiParam(required = false, value = "0全部，1中级，2高级") Integer alarmLeve,
			@ApiParam(required = false, value = "用来查询出登陆用户的信息") String token,
			@ApiParam(required = false, value = "页数:默认0") Integer page,
			@ApiParam(required = false, value = "行数:默认10") Integer rows) {
		PageData<FaultMsgDesc> data = new PageData<FaultMsgDesc>();
		List<PointModel> pointModelByLoginUser =null;
		List<MasterModel> masterModelByLoginUser=null;
		List<String> deviceIds=new ArrayList<>();
	    List<String> masterFlagList=null;
	    List<String> pointFlagList=null;
	 	if (monitorStationId != null && monitorStationId != "") {
			List<PointModel> pointModelByLoginUserTemp =new ArrayList<>();
			List<MasterModel> masterModelByLoginUserTemp=new ArrayList<>();
			pointModelByLoginUser = getResponsibleDeviceService.getPointModelByLoginUser(token);
			pointModelByLoginUser.forEach(p->{
				if(monitorStationId.equals(p.getStationId())){
					pointModelByLoginUserTemp.add(p);
				}
			});
		    masterModelByLoginUser = getResponsibleDeviceService.getMasterModelByLoginUser(token);
		    masterModelByLoginUser.forEach(m->{
		    	if(monitorStationId.equals(m.getStationId())){
		    		masterModelByLoginUserTemp.add(m);
				}
		    });
		    pointFlagList = pointModelByLoginUserTemp.stream().map(PointModel::getPointFlag).collect(Collectors.toList());
		    masterFlagList = masterModelByLoginUserTemp.stream().map(MasterModel::getMonitorFlag).collect(Collectors.toList());
		}else{
			pointModelByLoginUser = getResponsibleDeviceService.getPointModelByLoginUser(token);
			pointFlagList = pointModelByLoginUser.stream().map(PointModel::getPointFlag).collect(Collectors.toList());
		    masterModelByLoginUser = getResponsibleDeviceService.getMasterModelByLoginUser(token);
			masterFlagList = masterModelByLoginUser.stream().map(MasterModel::getMonitorFlag).collect(Collectors.toList());
		}
	 	if(!pointFlagList.isEmpty()&&pointFlagList!=null){
	 		deviceIds.addAll(pointFlagList);
	 	}
		if(!masterFlagList.isEmpty()&&masterFlagList!=null){
			deviceIds.addAll(masterFlagList);
	 	}
	    //log.info("pointModelByLoginUser:"+JSON.toJSONString(pointModelByLoginUser));
	  // System.out.println("APPpointModelByLoginUser:"+JSON.toJSONString(pointModelByLoginUser)); 
		page = (page == null || page == 0) ? 0 : page - 1;
		rows = (rows == null) ? 10 : rows;
		FaultMsgDesc faultMsgDesc = null;
		List<FaultMsgDesc> faultMsgDescList = new ArrayList<FaultMsgDesc>();
		boolean flag = Boolean.valueOf(isSolve);
		if (!deviceIds.isEmpty()&&deviceIds!=null) {
			if (faultType == null) {
				faultType = 0;
			}
			if (beginTime == null) {
				beginTime = 0l;
			}
			if (endTime == null) {
				endTime = System.currentTimeMillis();
			}
			List<Map<String, Object>> faultInfoForPage = XWDataEntry.getFaultInfoForTimePage(deviceIds, faultType,flag, beginTime, endTime, page, rows);
			if (faultInfoForPage == null) {throw new BizException(Code.NO_DATA);}
			for (Map<String, Object> faultMsg : faultInfoForPage) {
				//System.out.println("faultMsg:" + JSON.toJSONString(faultMsg));
				faultMsgDesc = new FaultMsgDesc();
					faultMsgDesc.setAutoId(Integer.valueOf(faultMsg.get("id").toString()));
					String devid = faultMsg.get("devid").toString();
					faultMsgDesc.setDevid(devid);
					Integer faultTypes = Integer.valueOf(faultMsg.get("fault_type").toString());
					faultMsgDesc.setFaultType(faultTypes);
					faultMsgDesc.setTimes(DateUtils.unixTimestampToDate(Long.valueOf(faultMsg.get("timestamp").toString())));
					faultMsgDesc.setTimestamp(Long.valueOf(faultMsg.get("timestamp").toString()));
					faultMsgDesc.setPhase(faultMsg.get("phase")==null?0:Integer.valueOf(faultMsg.get("phase").toString()));
				// faultType:故障类型 :系统过电压:11   电量不足:23 属于中级报警 
				if (faultTypes == FaultType.FaultTypeFor11.getValue()||faultTypes == FaultType.FaultTypeFor23.getValue()) {
					faultMsgDesc.setAlarmLeve(AlarmLeve.zhongji.getName());
				} else {
					// faultType:故障类型
					// :接地故障:10,过电压:12,系统断线:13,欠压数据包:14,系统短路15,弧光接地故障16,过热20,湿度超标21,泄漏 22)
					faultMsgDesc.setAlarmLeve(AlarmLeve.gaoji.getName());
				}
				if (faultMsg.get("distimestamp") != null && faultMsg.get("distimestamp") != "") {
					String distimes= DateUtils.unixTimestampToDate(Long.valueOf(faultMsg.get("distimestamp").toString()));
					faultMsgDesc.setDistimes(distimes);
					faultMsgDesc.setDistimestamp(Long.valueOf(faultMsg.get("distimestamp").toString()));
				} 
				faultMsgDescList.add(faultMsgDesc);
				continue;
			}
			
			for (FaultMsgDesc fault : faultMsgDescList) {
				for (PointModel pointModel : pointModelByLoginUser) {
					if(fault.getDevid().equals(pointModel.getPointFlag())){
						fault.setMasterName(pointModel.getMasterName());
						fault.setMonitorStationName(pointModel.getStationName());
						fault.setMasterAddress(pointModel.getStationAddress());
						fault.setProtectorType(pointModel.getHitchType());
						fault.setType(pointModel.getType());
						fault.setDeviceName(pointModel.getPointName());
						fault.setBranchName(pointModel.getBranchName());
						continue;
					}
				}
			}
			for (FaultMsgDesc fault : faultMsgDescList) {
				for (MasterModel masterModel : masterModelByLoginUser) {
					if(fault.getDevid().equals(masterModel.getMonitorFlag())){
						fault.setMasterName(masterModel.getMasterName());
						fault.setMonitorStationName(masterModel.getStationName());
						fault.setMasterAddress(masterModel.getStationAddress());
						fault.setProtectorType("智能网关");
						fault.setType("智能网关");
						fault.setDeviceName(masterModel.getMasterName());
						continue;
					}
				}
			}
			
			// 如果传入母线名称则按名称匹配故障列表
			if (StringUtils.isNotBlank(keyQuery)) {
				List<FaultMsgDesc> queryByMasterNameList = new ArrayList<>();
				List<String> devidsListTemp = new ArrayList<String>();
				for (FaultMsgDesc masterNameQuery : faultMsgDescList) {
					if (masterNameQuery.getMasterName().contains(keyQuery)) {
						queryByMasterNameList.add(masterNameQuery);
						devidsListTemp.add(masterNameQuery.getDevid());
					}
					continue;
				}
				if (queryByMasterNameList.isEmpty()) {
					throw new BizException(Code.NO_DATAS);
				}
				data.setCount(XWDataEntry.getFaultNewCount(devidsListTemp, faultType, flag));
				data.setList(queryByMasterNameList);
				return Message.success(data);
			}

			List<FaultMsgDesc> list = null;
			// 1中级报警
			if (alarmLeve == AlarmLeve.zhongji.getValue()) {
				list = new ArrayList<FaultMsgDesc>();
				for (FaultMsgDesc faultMsgDesc2 : faultMsgDescList) {
					if (faultMsgDesc2.getAlarmLeve().equals(AlarmLeve.zhongji.getName())) {
						list.add(faultMsgDesc2);
					}
					continue;
				}
				data.setList(list);
				// data.setCount(list.size());
				if (faultType != FaultType.FaultTypeFor0.getValue()) {
					data.setCount(XWDataEntry.getFaultNewCount(deviceIds, faultType, flag));
				} else {
					int faultNewCount11 = XWDataEntry.getFaultNewCount(deviceIds, FaultType.FaultTypeFor11.getValue(),flag);
					int faultNewCount23 = XWDataEntry.getFaultNewCount(deviceIds, FaultType.FaultTypeFor23.getValue(),flag);
					data.setCount(faultNewCount11+faultNewCount23);
				}
				return Message.success(data);
				 // 2高级报警
			}else if (alarmLeve == AlarmLeve.gaoji.getValue()) {
				list = new ArrayList<FaultMsgDesc>();
				for (FaultMsgDesc faultMsgDesc2 : faultMsgDescList) {
					if (faultMsgDesc2.getAlarmLeve().equals(AlarmLeve.gaoji.getName())) {
						list.add(faultMsgDesc2);
					}
					continue;
				}
				data.setList(list);
				// data.setCount(list.size());
				if (faultType != FaultType.FaultTypeFor0.getValue()) {
					data.setCount(XWDataEntry.getFaultNewCount(deviceIds, faultType, flag));
				} else {
					int faultNewCount10 = XWDataEntry.getFaultNewCount(deviceIds, FaultType.FaultTypeFor10.getValue(),
							flag);
					int faultNewCount12 = XWDataEntry.getFaultNewCount(deviceIds, FaultType.FaultTypeFor12.getValue(),
							flag);
					int faultNewCount13 = XWDataEntry.getFaultNewCount(deviceIds, FaultType.FaultTypeFor13.getValue(),
							flag);
					int faultNewCount14 = XWDataEntry.getFaultNewCount(deviceIds, FaultType.FaultTypeFor14.getValue(),
							flag);
					int faultNewCount15 = XWDataEntry.getFaultNewCount(deviceIds, FaultType.FaultTypeFor15.getValue(),
							flag);
					int faultNewCount16 = XWDataEntry.getFaultNewCount(deviceIds, FaultType.FaultTypeFor16.getValue(),
							flag);
					int faultNewCount20 = XWDataEntry.getFaultNewCount(deviceIds, FaultType.FaultTypeFor20.getValue(),
							flag);
					int faultNewCount21 = XWDataEntry.getFaultNewCount(deviceIds, FaultType.FaultTypeFor21.getValue(),
							flag);
					int faultNewCount22 = XWDataEntry.getFaultNewCount(deviceIds, FaultType.FaultTypeFor22.getValue(),
							flag);
					int faultNewCount24 = XWDataEntry.getFaultNewCount(deviceIds, FaultType.FaultTypeFor24.getValue(),flag);
					data.setCount(faultNewCount10 + faultNewCount12 + faultNewCount13 + faultNewCount14
							+ faultNewCount15 + faultNewCount16+faultNewCount20+faultNewCount21+faultNewCount22+faultNewCount24);
				}
				return Message.success(data);
			}
			// 查询全部报警信息
			data.setList(faultMsgDescList);
			data.setCount(XWDataEntry.getFaultNewCount(deviceIds, faultType, flag));
		} else {
			throw new BizException(Code.NO_DATAS);
		}
		return Message.success(data);
	}
	
	/**
	 * 获取保护器电池电量曲线  /eric/mobile/getBatteryForVoltage.v1
	 */
	@ResponseBody
	@RequestMapping("getBatteryForVoltage.v1")
	@ApiOperation(value = "保护器电池电量曲线", httpMethod = "POST")
	public Message<?> getBatteryForVoltage(@ApiParam(required = false, value = "母线对应的设备id:B12") String sensorId,
			@ApiParam(required = false, value = "timeStart:时间戳:System.currentTimeMillis(),日期例子:2018-04-26 01:00:00:000") Long timeStart,
			@ApiParam(required = false, value = "timeEnd:时间戳:System.currentTimeMillis(),日期例子:2018-04-26 01:00:00:000") Long timeEnd) {
		if (StringUtils.isNotEmpty(sensorId)) {
			 List<Map<String, Object>> batteryForVoltage = XWDataEntry.getBatteryForVoltage(sensorId, timeStart, timeEnd);
			return Message.success(batteryForVoltage);
		} else {
			throw new BizException(Code.NOT_PARAM);
		}
	}

	/**
	 * 获取母线设备标识
	 * 
	 * @param companyId
	 * @param linesOnMasterDtoList
	 * @param deviceIds
	 * @param sb
	 * @return
	 */
	@SuppressWarnings("unused")
	private String getMasterMonitorFlagList(String companyId, List<LinesOnMasterDto> linesOnMasterDtoList,
			String deviceIds, StringBuilder sb) {
		List<String> queryAllCompanyId = new ArrayList<>();
		String loginMemberId = this.getLoginMemberId();
		String loginManagerId = this.getLoginManagerId();
		if (loginMemberId == null && loginManagerId == null) {
			throw new BizException(Code.NOT_LOGIN);
		}

		String memberId = null;
		if (loginMemberId != null) {
			memberId = loginMemberId;
		}
		if (StringUtils.isNotEmpty(loginMemberId)) {
			queryAllCompanyId = companyService.queryAllCompanyId(loginMemberId);
		} else if (StringUtils.isNotEmpty(loginManagerId)) {
			// 根据token 查询出当前负责人的id; 根据用户id查询所创建的公司id 集合
			queryAllCompanyId = companyService.queryAllCompanyId(loginManagerId);
		}

		if (queryAllCompanyId != null) {
			List<LinesOnMasterDto> list = new ArrayList<>();
			for (String companyIds : queryAllCompanyId) {
				list = linesOnMasterService.queryLinesOnMasterIds(null, companyIds, null, null, null, memberId);
				linesOnMasterDtoList.addAll(list);
			}
		} else {
			linesOnMasterDtoList = linesOnMasterService.queryLinesOnMasterIds(null, companyId, null, null, null,
					memberId);

			//System.out.println("不传公司ID查询结果:linesOnMasterDtoList:" + JSON.toJSONString(linesOnMasterDtoList));
		}

		for (LinesOnMasterDto linesOnMasterDto : linesOnMasterDtoList) {
			sb.append(linesOnMasterDto.getMonitorFlag()).append(",");// 获取母线标识集合
		}
		if (deviceIds == null || deviceIds.length() == 0) {
			deviceIds = sb.toString();
		}
		return deviceIds;
	}
	
	/**
	 * 获取红外线测温的故障详情 对应故障类型：24
	 */
	@ResponseBody
	@RequestMapping("getFaultInfraredDetail.v1")
	@ApiOperation(value = "红外线测温的故障详情")
	public Message<?> getFaultInfraredDetail(@ApiParam(required = false, value = "设备id:B12") String devid,
			@ApiParam(required = false, value = "time:时间戳")Long time) {
		if (StringUtils.isNotEmpty(devid)&&time!=null) {
			FaultInfrared faultInfraredDetail = XWDataEntry.getFaultInfraredDetail(devid,time);
			 return Message.success(faultInfraredDetail);
		} else {
			throw new BizException(Code.NOT_PARAM);
		}
	}
	
	/**
	 * 开始获取终端红外图像
	 */
	@ResponseBody
	@RequestMapping("startCamera.v1")
	@ApiOperation(value = "获取终端红外图像")
	public Message<?> startCamera(@ApiParam(required = false, value = "设备id:B12") String devid) {
		if (StringUtils.isNotEmpty(devid)) {
				XWDataEntry.startCamera(devid);
			 return Message.success();
		} else {
			throw new BizException(Code.NOT_PARAM);
		}
	}
	
	
	@ResponseBody
	@RequestMapping("getConfig800")
	@ApiOperation(value = "设置初始化", httpMethod = "POST")
	public Message<?> getConfig800(@ApiParam(required = false, value = "母线对应的设备id:00000000000000002017") String devid) {
		config_eric800 config800 = XWDataEntry.getConfig800(devid);
		if(config800==null){
			throw new BizException(Code.DEVICE_NOTEXIST);
		}
		return Message.success(config800);
	}
	
	@ResponseBody
	@RequestMapping("/wxLogin")
    public  Message<?> wxLogin(String code) {
        Map<String, String> param = new HashMap<>();
        param.put("appid", WxConstant.appid);
        param.put("secret", WxConstant.secret);
        param.put("grant_type", WxConstant.grantType);
        param.put("js_code", code);
        WxLoginDto wxLoginDto=null;
        try{
        	 String wxResult = HttpClient.get(WxConstant.authCode2SessionURL, param);
             System.out.println("wxResult:" + JSON.toJSONString(wxResult));
     		 wxLoginDto = JSON.parseObject(wxResult, WxLoginDto.class);
     		 System.out.println("wxLoginDto:" + JSON.toJSONString(wxLoginDto));
     		 
     		if(wxLoginDto!=null&&!StringUtils.isEmpty(wxLoginDto.getOpenid())){
     			redisCache.hset("user-Openid-session:"+  wxLoginDto.getOpenid(),  wxLoginDto.getOpenid(), JSON.toJSONString(wxLoginDto));
     		}
        }catch(Exception e){
        	wxLoginDto = new WxLoginDto();
        	wxLoginDto.setErrmsg("系统错误");
        }
       


        return Message.success(wxLoginDto);
    }



}
