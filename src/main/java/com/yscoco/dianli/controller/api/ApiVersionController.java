package com.yscoco.dianli.controller.api;

import com.yscoco.dianli.common.aspect.SystemLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yscoco.dianli.common.base.BaseController;
import com.yscoco.dianli.common.exception.BizException;
import com.yscoco.dianli.common.handler.PageData;
import com.yscoco.dianli.common.vo.Code;
import com.yscoco.dianli.common.vo.Message;
import com.yscoco.dianli.entity.app.AppVersion;
import com.yscoco.dianli.service.app.AppVersionService;



@Controller
@RequestMapping("/api/version/")
@Api(tags="app+后台 ：2、版本",description="软件版本信息", produces = MediaType.APPLICATION_JSON_VALUE)
public class ApiVersionController extends BaseController {
	
	@Autowired
	AppVersionService appVersionService;
	
	@ResponseBody
	@RequestMapping(value="queryAppVersion.v1")
	@SystemLog(module="api/version",methods = "查看APP版本信息")
	public Message<PageData<AppVersion>> queryAppVersion(
			@ApiParam(required = false,  value = "版本标识Id") String appVersionId, Integer page, Integer rows) {
			return new Message<>(Code.SUCCESS,appVersionService.listPage(appVersionId,page,rows));
	}
	
	
	@ResponseBody
	@RequestMapping("addOrUpdateAppVersion.v1")
	@ApiOperation(value = "APP版本新增和更新" ,httpMethod = "POST" )
	@SystemLog(module="api/addOrUpdateAppVersion",methods = "APP版本新增和更新")
	public Message<?> addOrUpdateAppVersion(
			@ApiParam(required = false,  value = "版本标识Id") String appVersionId , 
			@ApiParam(required = false,  value = "版本名称") String versionName,
			@ApiParam(required = false,  value = "是否必须更新") String isMust,
			@ApiParam(required = false,  value = "链接地址") String url,
			@ApiParam(required = false,  value = "版本更新内容") String remarks,
			@ApiParam(required = false,  value = "应用标识 1 安卓，2 IOS") String identify,
			@ApiParam(required = false,  value = "版本号") String version){
			return this.appVersionService.addOrUpdateAppVersion(appVersionId,versionName,isMust,
					url,remarks,identify,version,this.getLoginManagerId(),request.getRemoteAddr());
	}
	
	
	
	@ResponseBody
	@RequestMapping("getNewestAppVersion.v1")
	@ApiOperation(value="获取最新APP版本信息",httpMethod="POST")
	@SystemLog(module="api/getNewestAppVersion",methods = "获取最新APP版本信息")
	public Message<AppVersion> getNewestAppVersion(
			@ApiParam(value = "应用  1 安卓 2 IOS")  String  identify){
		 if(null==identify||StringUtils.isBlank(identify)){
          	 throw new BizException(Code.NOT_PARAM); 
		}
			return new Message<>(Code.SUCCESS,appVersionService.getNewest(identify));
	}
	
	
	@ResponseBody
	@RequestMapping("deleteAppVersion.v1")
	@ApiOperation(value = "APP版本删除" ,httpMethod = "POST" )
	@SystemLog(module="api/deleteAppVersion",methods = "APP版本删除")
	public Message<?> deleteAppVersion(
			@ApiParam(required = false,  value = "app版本集合ids") String appIds){
			this.appVersionService.delete(appIds,this.getLoginManagerId(),request.getRemoteAddr());
			return new Message<>(Code.SUCCESS);
	}
	
}
