package com.yscoco.dianli.controller.sys;

import com.yscoco.dianli.common.aspect.SystemLog;
import com.yscoco.dianli.common.exception.BizException;
import com.yscoco.dianli.common.vo.Code;
import com.yscoco.dianli.entity.sys.SysUser;
import com.yscoco.dianli.other.cache.RedisCacheUtil;
import com.yscoco.dianli.service.comon.NosqlKeyManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.yscoco.dianli.common.base.BaseController;
import com.yscoco.dianli.common.vo.Message;
import com.yscoco.dianli.dto.sys.SysUserDto;
import com.yscoco.dianli.service.member.MemberService;
import com.yscoco.dianli.service.sys.SysUserService;

@Controller
@RequestMapping("back/login/")
@Api(tags="后台管理-1、登录",description="后台登录,登出", produces = MediaType.APPLICATION_JSON_VALUE,position=1)
public class LoginController extends BaseController{
	
	@Autowired
	SysUserService sysUserService;
	
	@Autowired
	MemberService memberService;

	private static final int  MAX_COUNT=3;

	@Resource
	private RedisCacheUtil redisCache;
	
	@ResponseBody
	@RequestMapping("login.v1")
	@ApiOperation(value = "管理员登录" ,httpMethod = "POST" )
	public Message<SysUserDto> login(
			@ApiParam(required = true,  value = "账户") String account ,
			@ApiParam(required = true,  value = "密码") String password){
			this.onGetRequestParse();
		Message<SysUserDto> login =null;
		login = getSysUserDtoMessage(account, password);
		this.session.setAttribute("user",  login.getData());
		return login;
	}

	private Message<SysUserDto> getSysUserDtoMessage(@ApiParam(required = true, value = "账户") String account, @ApiParam(required = true, value = "密码") String password) {
		Message<SysUserDto> login;
		String loginMaxCount = redisCache.get(NosqlKeyManager.LOGIN_MAX_COUNT + account);


		if(loginMaxCount!=null&&Integer.valueOf(loginMaxCount)==MAX_COUNT){
			throw new BizException(Code.LOGIN_MAX_COUNT);
		}
		login = this.sysUserService.login(account, password,request.getRemoteAddr());
		if(null==login.getData()){
			if(StringUtils.isNotEmpty(loginMaxCount)) {
				int i = loginMaxCount == null ? 0 : Integer.valueOf(loginMaxCount);
				redisCache.set(NosqlKeyManager.LOGIN_MAX_COUNT + account,String.valueOf(++i),1, TimeUnit.DAYS);

				redisCache.hset(NosqlKeyManager.LOGIN_MAX_COUNT,account,String.valueOf(i));
			}else{
				int count =(loginMaxCount==null?0:Integer.valueOf(loginMaxCount));
				redisCache.set(NosqlKeyManager.LOGIN_MAX_COUNT + account,String.valueOf(++count),1, TimeUnit.DAYS);
				redisCache.hset(NosqlKeyManager.LOGIN_MAX_COUNT,account,String.valueOf(count));
			}
			throw new BizException(Code.ERR_PWD);
		}
		return login;
	}

	@ResponseBody
	@RequestMapping("loginSession.v1")
	@ApiOperation(value = "管理员登录" ,httpMethod = "POST" )
	public SysUserDto loginSession(){
		SysUserDto attribute =(SysUserDto)this.session.getAttribute("user");
			return attribute;
	}
	
	
	@ResponseBody
	@RequestMapping("loginOut.v1")
	@SystemLog(module="back/login/loginOut",methods = "管理员登出")
	@ApiOperation(value = "管理员登出" ,httpMethod = "POST" )
	public Message<?> loginOut(
		@ApiParam(required = true,  value = "token") String token){
		return this.sysUserService.loginOut(this.getLoginManagerId());
	}
	
	@ResponseBody
	@RequestMapping("memberLogin.v1")
	@ApiOperation(value ="巡检员登陆" ,httpMethod ="POST")
	public Message<?> memberLogin(
			@ApiParam(required = true,  value = "账户") String account ,
			@ApiParam(required = true,  value = "密码") String password,
			@ApiParam(required = false,  value = "session") HttpSession session) throws IOException{
		return this.memberService.login(account, password,request.getRemoteAddr(),null);
	}
	
	@ResponseBody
	@RequestMapping("memberLoginOut.v1")
	@SystemLog(module="back/login/memberLoginOut",methods = "巡检员登出")
	@ApiOperation(value = "巡检员登出" ,httpMethod = "POST" )
	public Message<?> memberLoginOut(
		@ApiParam(required = true,  value = "token") String token){
		return this.memberService.loginOut(this.getLoginMemberId());
	}


	/**
	 * 根据账户名 解冻
	 * @param account
	 * @return
	 */
	@ResponseBody
	@RequestMapping("unfreezeAccount.v1")
	public Message<?> unfreezeAccount(String account){
		try {
			redisCache.hdel(NosqlKeyManager.LOGIN_MAX_COUNT,account);
			redisCache.delte(NosqlKeyManager.LOGIN_MAX_COUNT+account);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Message.success();
	}

	/**
	 *  冻结账户列表
	 * @return
	 */
	@ResponseBody
	@RequestMapping("freezeAccountList.v1")
	public Message<?> freezeAccountList(){
		ArrayList<SysUser> usersList =null;

		ArrayList<Object> arrayList = redisCache.hgetList(NosqlKeyManager.LOGIN_MAX_COUNT);

        if(arrayList!=null&&arrayList.size()>0){
			usersList = new ArrayList<>();
			for (int i = 0; i < arrayList.size(); i++) {
				SysUser user = sysUserService.getByAccount(arrayList.get(i).toString());
				usersList.add(user);
			}
		}

		return Message.success(usersList);
	}

	/**
	 *  解冻所有账户
	 * @param
	 * @return
	 */
	@ResponseBody
	@RequestMapping("unfreezeAllAccount.v1")
	public Message<?> unfreezeAllAccount(){
		try {
			ArrayList<Object> arrayList = redisCache.hgetList(NosqlKeyManager.LOGIN_MAX_COUNT);
			for (int i = 0; i < arrayList.size(); i++) {
				redisCache.hdel(NosqlKeyManager.LOGIN_MAX_COUNT,arrayList.get(i).toString());
				redisCache.delte(NosqlKeyManager.LOGIN_MAX_COUNT+arrayList.get(i).toString());
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return Message.success();
	}

	
}
