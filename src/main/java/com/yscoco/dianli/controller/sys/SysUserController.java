package com.yscoco.dianli.controller.sys;

import cn.hutool.crypto.SecureUtil;
import com.yscoco.dianli.common.aspect.SystemLog;
import com.yscoco.dianli.entity.sys.SysUser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yscoco.dianli.common.base.BaseController;
import com.yscoco.dianli.common.exception.BizException;
import com.yscoco.dianli.common.handler.PageData;
import com.yscoco.dianli.common.utils.MatcheUtils;
import com.yscoco.dianli.common.vo.Code;
import com.yscoco.dianli.common.vo.Message;
import com.yscoco.dianli.common.vo.VcodeType;
import com.yscoco.dianli.dto.sys.SysUserDto;
import com.yscoco.dianli.service.app.VcodeVerifyService;
import com.yscoco.dianli.service.sys.SysUserService;

@Controller
@RequestMapping(value="/back/sysUser/",name="后台用户管理")
//@Api(tags="后台管理-2、后台管理员管理",description="管理员增删查看", produces = MediaType.APPLICATION_JSON_VALUE,position=1)
public class SysUserController extends BaseController{
  
	@Autowired
	SysUserService sysUserService;

	@Autowired
	private VcodeVerifyService vcodeVerifyService;
	
	
	
	@ResponseBody
	@RequestMapping("sendSmsCode.v1")
	@ApiOperation(value="web端发送验证码")
	public Message<?> sendSmsCode(
			@ApiParam(required = true,  value = "手机号码或者账号")  String mobileOrAccount,
			@ApiParam(required = true,  value = "验证类型1:注册，2:改绑定手机 3.忘记密码") String type) throws Exception{
			if(StringUtils.isEmpty(mobileOrAccount)||StringUtils.isEmpty(type)){
				return new Message<>(Code.NOT_PARAM);
			}
			if(!MatcheUtils.isMobile(mobileOrAccount)){
				return new Message<>(Code.Error_MOBILE);
			}
			if(sysUserService.getByUserNameOrMobileOrEmail(mobileOrAccount)==null){
				return new Message<>(Code.NOTEXIST_MOBILE);
			}
			VcodeType codeType = VcodeType.fromInt(Integer.valueOf(type));
			vcodeVerifyService.saveAndSendVcode(mobileOrAccount, codeType);
			return Message.success();
	}
	
	@ResponseBody
	@RequestMapping("updatePasswordBySmsCode.v1")
	@SystemLog(module="/back/sysUser/updatePasswordBySmsCode",methods = "web巡检员根据验证码修改密码")
	public Message<?> updatePasswordBySmsCode(
			@ApiParam(required = true,  value = "验证码") String smsCode,
			@ApiParam(required = true,  value = "手机号码或者账号") String mobileOrAccount,
			@ApiParam(required = true,  value = "新密码--必须md5加密,小写") String password){
			memberService.updatePasswordBySmsCode(mobileOrAccount, password, smsCode);
			return Message.success();
	}
	
	@ResponseBody
	@RequestMapping("updatePasswordBySmsCode.v2")
	@SystemLog(module="/back/sysUser/updatePasswordBySmsCode",methods = "web管理员根据验证码修改密码")
	public Message<?> updatePasswordBySmsCode2(
			@ApiParam(required = true,  value = "验证码") String smsCode,
			@ApiParam(required = true,  value = "手机号码或者账号") String mobileOrAccount,
			@ApiParam(required = true,  value = "新密码--必须md5加密,小写") String password){
		sysUserService.updatePasswordBySmsCode(mobileOrAccount, password, smsCode);
			return Message.success();
	}
	
	
	
	@ResponseBody
	@RequestMapping("addOrUpdateSysUser.v1")
	@SystemLog(module="/back/sysUser/addOrUpdateSysUser",methods = "后台用户添加或更新")
	@ApiOperation(value = "后台用户添加或更新" ,httpMethod = "POST" )
	public Message<?> addOrUpdateSysUser(
			@ApiParam(required = false,  value = "用户id") String id, 
			@ApiParam(required = false,  value = "职位") String position,
			@ApiParam(required = false,  value = "账号(会进行账号重复判断)") String account,
			@ApiParam(required = false,  value = "密码") String password,
			@ApiParam(required = false,  value = "联系方式") String mobilePhone,
			@ApiParam(required = false,  value = "权限") String permission){
		    permission="添加、授权巡检员";
		    if(StringUtils.isBlank(getLoginManagerId())){
	          	 throw new BizException(Code.NOT_LOGIN);
	        }
		    
			return this.sysUserService.addOrUpdateSysUser(id,position,account,password,mobilePhone,this.getLoginManagerId(),permission);
	}
	
	
	@ResponseBody
	@RequestMapping(value="querySysUser.v1")
	@SystemLog(module="/back/sysUser/querySysUser",methods = "后台用户查询")
	public Message<PageData<SysUserDto>> querySysUser(
		 String sysUserId,
		 String account,
		 String mobilePhone,
		 Integer page,
		 Integer rows) {
			page = (page == null)?1:page;
			rows = (rows == null)?10:rows;
			Map<String,Object> params = new HashMap<>();
			if(StringUtils.isNotEmpty(account)){
				params.put("accountLike", account);
			}
			if(StringUtils.isNotEmpty(mobilePhone)){
				params.put("mobilePhoneLike", mobilePhone);
			}
			if(StringUtils.isNotEmpty(sysUserId)){
				params.put("id", sysUserId);
			}
			params.put("page", page);
			params.put("rows", rows);
			PageData<SysUserDto> data = new PageData<SysUserDto>();
			          
			data.setList(sysUserService.listPage(params));
			data.setCount(sysUserService.count(params));
			return new Message<>(Code.SUCCESS,data);
	}

	@ResponseBody
	@RequestMapping("ppp.v1")
	public Message<?> test11(){
		List<SysUser> result = new ArrayList<>();
//
//		List<SysUser> list = sysUserService.list();
//
//		for (SysUser sysUser : list) {
//			//if(!"admin".equals(sysUser.getPermission())){
//				sysUser.setPassword(SecureUtil.md5(sysUser.getPassword()));
//				sysUserService.edit(sysUser);
//		//	}
//			result.add(sysUser);
//		}

		return new Message<>(Code.SUCCESS,result);
	}
	
	
	@ResponseBody
	@RequestMapping("deleteSysUser.v1")
	@SystemLog(module="/back/sysUser/deleteSysUser",methods = "后台用户删除")
	public Message<?> deleteSysUser(
			@ApiParam(required = false,  value = "用户id集合") String ids){
		if(StringUtils.isBlank(getLoginManagerId())){
         	 throw new BizException(Code.NOT_LOGIN);   }
			this.sysUserService.delete(ids,request.getRemoteAddr(),this.getLoginManagerId());
			return new Message<>(Code.SUCCESS);
	}
	
	
}