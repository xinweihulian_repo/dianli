package com.yscoco.dianli.controller.test;



import com.alibaba.fastjson.JSONObject;
import com.yscoco.dianli.common.utils.HttpClient;
import com.yscoco.dianli.dao.test.StudentDao;
import org.apache.http.protocol.HTTP;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.yscoco.dianli.common.base.BaseController;
import com.yscoco.dianli.common.vo.Message;
import com.yscoco.dianli.entity.test.Student;
import com.yscoco.dianli.entity.test.Teacher;
import com.yscoco.dianli.entity.test.TeacherDto;
import com.yscoco.dianli.service.test.StudentService;
import com.yscoco.dianli.service.test.TeacherService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class TestController extends BaseController {
	
	@Autowired
	private TeacherService teacherService;
	
	@Autowired
	private StudentService studentService;

	@Autowired
	private StudentDao studentDao;


	@ResponseBody
	@RequestMapping("/test111")
	public Message<?> test111(String id){
		Map map = new HashMap();
		map.put("","");
		//List list = studentDao.list(null);
		
		Student entity = studentDao.getEntity("qq");
		return  Message.success(entity);
	}



	@ResponseBody
	@RequestMapping("/test.v2")
	public Message<?> test(String id){
		/*Teacher byId = teacherService.getById(id);*/
		 TeacherDto byId = teacherService.getByIds(id);
		System.out.println(	JSON.toJSON(byId));
        return  Message.success(byId);
	}
	
	
	@ResponseBody
	@RequestMapping("/test.v3")
	public Message<?> test3(String id){
		 Student byId = studentService.getById(id);
		System.out.println(	byId.toString());
		System.out.println(	JSON.toJSON(byId));
        return  Message.success(byId);
	}
	
	
	@ResponseBody
	@RequestMapping("/test.v4")
	public Message<?> test4(String id){
		//http://120.78.171.137:8081/mobile-api/index/smartgld?debug=yes
		HttpClient httpClient = new HttpClient();
		String url="http://120.78.171.137:8081/mobile-api/index/smartgld";
		Map param = new HashMap();
		param.put("debug","yes");
		String post = httpClient.get(url, param);
		JSONObject jsonObject = JSONObject.parseObject(post);

		return  Message.success(jsonObject);
	}

	public static void main(String[] args) {
		int i  = 1<<30;
		System.out.println(i);

	}

}
