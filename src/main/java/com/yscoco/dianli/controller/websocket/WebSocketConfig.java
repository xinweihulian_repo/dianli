package com.yscoco.dianli.controller.websocket;

import java.util.logging.Logger;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;



@Configuration
@EnableWebMvc
@EnableWebSocket
//@EnableAsync
public class WebSocketConfig extends WebMvcConfigurerAdapter implements WebSocketConfigurer {
	private static final Logger logger = Logger.getLogger(WebSocketConfig.class.getName());

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
    	logger.info(String.format("==========================注册socket================================"));
        registry.addHandler(msgSocketHandle(),"/webSocketServer").addInterceptors(new WebSocketHandshakeInterceptor()).setAllowedOrigins("*");
      //  registry.addHandler(msgSocketHandle(),"/webSocketServerForTemper420_data").addInterceptors(new WebSocketHandshakeInterceptor()).setAllowedOrigins("*");
        //使用socketjs的注册方法
        registry.addHandler(msgSocketHandle(),"/sockjs/webSocketServer").addInterceptors(new WebSocketHandshakeInterceptor()).withSockJS();
    }

    /**
     *
     * @return 消息发送的Bean
     */
    @Bean(name = "msgSocketHandle")
    public WebSocketHandler msgSocketHandle(){
        return new MsgScoketHandle();
    }
}
