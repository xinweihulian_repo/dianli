package com.yscoco.dianli.controller.websocket;

import java.util.Map;
import java.util.logging.Logger;

import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.HandshakeInterceptor;


import javax.servlet.http.HttpServletRequest;


public class WebSocketHandshakeInterceptor implements  HandshakeInterceptor{

	private static final Logger logger = Logger.getLogger(WebSocketHandshakeInterceptor.class.getName());
	    /**
	     * 握手前
	     * @param request
	     * @param response
	     * @param webSocketHandler
	     * @param attributes
	     * @return
	     * @throws Exception
	     */
	    @Override
	    public boolean beforeHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler webSocketHandler, Map<String, Object> attributes) throws Exception {
			// 将ServerHttpRequest转成ServletServerHttpRequest
			ServletServerHttpRequest servletServerHttpRequest = (ServletServerHttpRequest) request;

			// 获取请求参数，先获取HttpServletRequest对象才能获取请求参数
			HttpServletRequest httpServletrequest = servletServerHttpRequest.getServletRequest();
			logger.info(String.format( "握手前 httpServletrequest Interceptor， sessionID：%s",
					httpServletrequest.getSession().getId()));

			// 获取请求的数据
			String clientID = httpServletrequest.getParameter(WebSocketConstant.CLIENT_ID);
			if (null == clientID || "".equals(clientID)) {
				return false;
			}
			// 把获取的请求数据绑定到session的map对象中（attributes）
			attributes.put(WebSocketConstant.CLIENT_ID, clientID);

			return true;
	    }

	    /**
	     *  握手完成后调用。响应状态和头指示握手的结果，即握手是否成功
	     * @param serverHttpRequest
	     * @param serverHttpResponse
	     * @param webSocketHandler
	     * @param e
	     */
	    @Override
	    public void afterHandshake(ServerHttpRequest serverHttpRequest, ServerHttpResponse serverHttpResponse, WebSocketHandler webSocketHandler, Exception e) {
			logger.info(String.format("握手后  Websocket Handshake Interceptor， Exception：%s",
					e.getMessage()));

	    }

}
