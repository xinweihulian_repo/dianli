package com.yscoco.dianli.controller.websocket;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;


@Component
public class MsgScoketHandle implements  WebSocketHandler {

    /**
     * 获取日志对象（全类名）
     */
    private static final Logger logger = Logger.getLogger(WebSocketHandler.class.getName());

    /**
     * 在线用户列表
     */
    public static final Map<String, List<WebSocketSession>> onlineUsers;
    //public static final Map<String, List<WebSocketSession>> sensorOnlines;

    static {
        onlineUsers = Collections.synchronizedMap(new HashMap<>());
      //  sensorOnlines=Collections.synchronizedMap(new HashMap<>());
    }



    
    /**
     * 建立链接
     * @param webSocketSession
     * @throws Exception
     */
    @Override
    public void afterConnectionEstablished(WebSocketSession webSocketSession) throws Exception {
        logger.info(String.format("==========================建立连接成功================================"));
    // 获取WebSocket客户端ID
        String clientID = getWebSocketClientID(webSocketSession);
        if(StringUtils.isBlank(clientID)){
            recordLog("Websocket clientID ","is null..");
            return;
        }
    	List<String> clientIDs= Arrays.asList(clientID.split(","));
        for (int i = 0; i < clientIDs.size(); i++) {
        	 List<WebSocketSession> webSocketSessionList = onlineUsers.get(clientIDs.get(i));
            if (null != webSocketSessionList) {
                webSocketSessionList.add(webSocketSession);
                onlineUsers.put(clientIDs.get(i), webSocketSessionList);
            } else {
                webSocketSessionList = new ArrayList<WebSocketSession>();
                webSocketSessionList.add(webSocketSession);
                onlineUsers.put(clientIDs.get(i), webSocketSessionList);
            }
        }
        recordLog("Websocket connection successful， wsClientID：%s，wsSessionID：%s。",
                clientID, webSocketSession.getId());
        

      //这块会实现自己业务，比如，当用户登录后，会把离线消息推送给用户
      /*  TextMessage returnMessage = new TextMessage("成功建立socket连接，你将收到的离线");
        webSocketSession.sendMessage(returnMessage);*/
    }

    /**
     * 接收前端发送消息
     * @param webSocketSession
     * @param webSocketMessage
     * @throws Exception
     */
    @Override
    public void handleMessage(WebSocketSession webSocketSession, WebSocketMessage<?> webSocketMessage) throws Exception {
    	String message = webSocketMessage.getPayload().toString();
    	   recordLog("Websocket connection successful， 收到：%s，wsSessionID：%s。",
    			  webSocketSession.getId(),  message);
    }

    /**
     * 异常处理
     * @param webSocketSession
     * @param throwable
     * @throws Exception
     */
    @Override
    public void handleTransportError(WebSocketSession webSocketSession, Throwable throwable){
        // 获取WebSocket客户端ID
        String clientID = getWebSocketClientID(webSocketSession);

        List<WebSocketSession> webSocketSessionList = onlineUsers.get(clientID);
        if (null != webSocketSessionList) {
            webSocketSessionList.remove(webSocketSession);
        }
        recordLog("Websocket handles connection exceptions， wsClientID：%s，wsSessionID：%s。",
                clientID, webSocketSession.getId());
    }

    /**
     * 断开链接
     * @param webSocketSession
     * @param closeStatus
     * @throws Exception
     */
    @Override
    public void afterConnectionClosed(WebSocketSession webSocketSession, CloseStatus closeStatus) throws Exception {
// 获取WebSocket客户端ID
        String clientID = getWebSocketClientID(webSocketSession);

        List<WebSocketSession> webSocketSessionList = onlineUsers.get(clientID);
        if (null != webSocketSessionList) {
            webSocketSessionList.remove(webSocketSession);
        }
        recordLog("Websocket disconnected successfully， wsClientID：%s，wsSessionID：%s。",
                clientID, webSocketSession.getId()+"   断开连接");
    }

    @Override
    public boolean supportsPartialMessages() {
        return false;
    }


    /**
     * 获取WebSocket客户端ID
     * @param session WebSocketSession
     * @return WebSocket客户端ID
     */
    private String getWebSocketClientID(WebSocketSession session) {
        return (String) session.getAttributes().get(WebSocketConstant.CLIENT_ID);
    }

    /**
     * 发送消息
     * @param clientID 客户端ID
     * @param textMessage 消息对象
     */
    private static void sendMessage(String clientID, String sessionID, TextMessage textMessage) {
        // 根据客户端ID查询webSocketSession集合
        List<WebSocketSession> webSocketSessionList = onlineUsers.get(clientID);
        
        if (null == webSocketSessionList) {
           recordLog("Not find webSocketSession by clientID， wsClientID：%s。", clientID);
            return;
        }

        for (WebSocketSession webSocketSession : webSocketSessionList) {
            // 得到webSocketSessionID
            String webSocketSessionID = webSocketSession.getId();

            boolean flag = false;
            // 当sessionID为null时，表示给所有根据clientID查询出来的客户端都发送；当sessionID不为null时，表示只给和sessionID相同的客户端单独发送；
            if (null == sessionID || sessionID.equals(webSocketSessionID)) {
                flag = true;
            }

            // 判断连接是否仍然处于连接状态
            if (webSocketSession.isOpen()) {
                if(flag) {
                    try {
                        webSocketSession.sendMessage(textMessage);
                        recordLog("Send webSocket message success， wsClientID：%s，wsSessionID：%s。",
                                clientID, webSocketSessionID);
                    } catch (IOException e) {
                        recordLog(
                                "Send webSocket message exception，wsClientID：%s，wsSessionID：%s，exceptionMessage：%s。",
                                clientID, webSocketSessionID, e.getMessage());
                        e.printStackTrace();
                    }
                }
            } else {
                recordLog(
                        "Send webSocket message faile，client state is disconnected，wsClientID：%s，wsSessionID：%s。",
                        clientID, webSocketSessionID);
            }
             //webSocketSessionList.iterator().remove();
        }
    }

    /**
     * 发送信息
     * @param clientID 客户端ID
     * @param message 消息
     */
    public static void sendMessage(String clientID, String message) {
        sendMessage(clientID, null, new TextMessage(message));
    }

    /**
     * 记录日志
     * @param message 日志信息
     * @param args 日志信息中的参数值
     */
    private static void recordLog(String message, Object... args) {
        logger.info(String.format(message, args));
    }


}
