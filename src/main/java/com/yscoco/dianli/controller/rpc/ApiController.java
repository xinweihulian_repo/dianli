package com.yscoco.dianli.controller.rpc;

import com.batise.device.temper.Temper_record;
import com.hyc.smart.XWDataEntry;
import com.yscoco.dianli.common.vo.Message;
import com.yscoco.dianli.dto.company.MonitorStationDto;
import com.yscoco.dianli.pojo.PointModel;
import com.yscoco.dianli.service.company.MonitorPointService;
import com.yscoco.dianli.service.company.MonitorStationService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

/**
 * @Author dong .
 */
@Controller
public class ApiController {

    @Autowired
    private MonitorStationService monitorStationService;
    
    @Autowired
    private MonitorPointService monitorPointService;


	@ResponseBody
    @RequestMapping("/eric/exportData.v1")
    public Message<List<Temper_record>> getTemper_record(
    	String stationID,
        Long timeStart,
        Integer count) {
        List<Temper_record>  data = null;
        if (StringUtils.isNotEmpty(stationID)) {
        	List<PointModel> pointModelList = monitorPointService.getPointModelList();
        	if(pointModelList!=null&&pointModelList.size()>0){
        		Set<String> set = new HashSet<>();
            	pointModelList.forEach(m->{
            		if(m.getStationId().equals(stationID)){
                        set.add(m.getPointFlag());
            		}
            	});
                  data = XWDataEntry.exportDataForIds(new ArrayList<>(set), timeStart, count);
        	}else{
        		 Message.success(new ArrayList<>());
        	}
        } else {
            Message.success(new ArrayList<>());
        }
        return Message.success(data);
    }
	
    @ResponseBody
    @RequestMapping("/eric/monitorStationInfo.v1")
    public Message<MonitorStationDto> getmonitorStationInfo(String stationID) {
    	MonitorStationDto entityById = monitorStationService.getEntityById(stationID);
        return Message.success(entityById);
    }

   /**
    * 
    * @param stationID
    * @param timeStart
    * @param count
    * @return
    */
    @ResponseBody
    @RequestMapping("/eric/exportFaultInfoForTime.v1")
    public Message< List<Map<String, Object>>> faultInfoForTime(String stationID, long timeStart, int count) {
        List<Map<String, Object>> maps =null;
        if (StringUtils.isNotEmpty(stationID)) {
            maps  =new ArrayList<>();
            List<PointModel> pointModelList = monitorPointService.getPointModelList();
            if(pointModelList!=null&&pointModelList.size()>0){
                Set<String> set = new HashSet<>();
                pointModelList.forEach(m->{
                    if(m.getStationId().equals(stationID)){
                        set.add(m.getPointFlag());
                    }
                });
                maps = XWDataEntry.ExportFaultInfoForTime(new ArrayList<>(set), timeStart, count);
            }
        } 
        return Message.success(maps);
    }

    public static void main(String[] args) {
    	List list= new ArrayList<>();
        list.add("A3929");
        list.add("ERIC08001906B0001006");
       
        List list1 = XWDataEntry.ExportFaultInfoForTime(list, 0, 100);

        System.out.println(list1);

    }
}
