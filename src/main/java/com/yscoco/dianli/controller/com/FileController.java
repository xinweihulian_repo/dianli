package com.yscoco.dianli.controller.com;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.yscoco.dianli.common.base.BaseController;
import com.yscoco.dianli.common.utils.Log;
import com.yscoco.dianli.common.utils.UploadUtils;
import com.yscoco.dianli.common.vo.Code;
import com.yscoco.dianli.common.vo.Message;

@Controller
@Api(tags="文件管理",description="图片，文件上传管理", produces = MediaType.APPLICATION_JSON_VALUE,position=1)
public class FileController extends BaseController {

	@ResponseBody
	@RequestMapping("/api/file/uploadImg.v1")
	@ApiOperation(value = "app上传图片" ,httpMethod = "POST" )
	public Message<String> upload(MultipartFile img){
      //保存  
        try {
        	 String realPath = UploadUtils.uploadImg(img.getInputStream());
        	 return new Message<String>(Code.SUCCESS, realPath);
        } catch (Exception e) {
        	Log.getCommon().error("上传文件失败",e);
        }
        return new Message<>(Code.ERROR);
	}
	
	
	@ResponseBody
	@RequestMapping("/back/file/uploadImg")
	@ApiOperation(value = "后台上传图片" ,httpMethod = "POST" )
	public Message<String> backupload(HttpServletRequest request,String type){
      
      //保存  
        try {
        	 MultipartHttpServletRequest multipartRequest  =  (MultipartHttpServletRequest) request;
             //  获得第1张图片（根据前台的name名称得到上传的文件)
             MultipartFile imgFile  =  multipartRequest.getFile("img");
        	 String realPath = UploadUtils.uploadImg(imgFile.getInputStream());
        	 return new Message<String>(Code.SUCCESS, realPath);
        } catch (Exception e) {
        	Log.getCommon().error("上传文件失败",e);
        }
        return new Message<>(Code.ERROR);
       
	}
	@ResponseBody
	@RequestMapping("/back/file/uploadFile")
	@ApiOperation(value = "后台上传文件.bin , .apk等格式" ,httpMethod = "POST" )
	public Message<String> uploadFile(HttpServletRequest request,String type){
		 String realPath =  null;
		  //保存  
	    try {
	    	 MultipartHttpServletRequest multipartRequest  =  (MultipartHttpServletRequest) request;
	         //  获得第1张图片（根据前台的name名称得到上传的文件)
	         MultipartFile imgFile  =  multipartRequest.getFile("file");
	         String[] ss = imgFile.getOriginalFilename().split("\\.");
	         System.out.println("原文件名称:"+ss);
	         realPath=UploadUtils.uploadFile(imgFile.getInputStream(),ss[ss.length-1]);
	    } catch (Exception e) {
	    	Log.getCommon().error("上传文件失败",e);
	    	return new Message<>(Code.ERROR);
	    }
		return new Message<>(Code.SUCCESS,realPath);
		
	}
}
