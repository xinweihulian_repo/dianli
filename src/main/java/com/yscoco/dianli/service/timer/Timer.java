package com.yscoco.dianli.service.timer;



import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.Resource;

import com.yscoco.dianli.dto.company.CompanyDto;
import com.yscoco.dianli.entity.company.*;
import com.yscoco.dianli.service.comon.NosqlKeyManager;
import com.yscoco.dianli.service.company.MonitorStationDeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;

import com.alibaba.fastjson.JSON;
import com.yscoco.dianli.common.utils.Log;
import com.yscoco.dianli.controller.websocket.MsgScoketHandle;
import com.yscoco.dianli.dao.company.CompanyDao;
import com.yscoco.dianli.entity.member.MemberInfo;
import com.yscoco.dianli.other.cache.RedisCacheUtil;
import com.yscoco.dianli.other.push.PushClientI;
import com.yscoco.dianli.service.member.MemberService;
import com.yscoco.dianli.service.sys.SysUserService;

 

@Component
public class Timer {
	
	private static final Logger logger = Logger.getLogger(Timer.class.getName());
	
	@Autowired
	private CompanyDao companyDao;

	@Autowired
	private MonitorStationDeviceService monitorStationDeviceService;

	@Resource
	private RedisCacheUtil redisCache;
	
	@Autowired
	private MemberService memberService;
	
	@Autowired
	private PushClientI pushClientI;
	
	@Autowired
    private MsgScoketHandle msgScoketHandle;
	
	@Autowired
	private SysUserService sysUserService;


	  @Scheduled(cron = "0/20 * * * * *")//20秒钟执行一次
	 // @Scheduled(cron="0 0/2 * * * ? ")//表示每2钟执行一次
	 public void testSerachData1(){
	  	long start =System.currentTimeMillis();
		  List<Company> list = companyDao.list(null);
		  List<CompanyDto> companyDtoList = new ArrayList();
			  list.forEach(m->{
				 int count=0;
				  for (int i = 0; i < m.getMonitorStations().size(); i++) {
					  MonitorStation monitorStation = m.getMonitorStations().get(i);
					  if(monitorStation.getStationType().equals("2")||monitorStation.getStationType().equals("3")){
						  count += monitorStationDeviceService.getByStationId(monitorStation.getId()).size();
						  continue;
					  }
					  for (int i1 = 0; i1 < monitorStation.getLinesOnMasters().size(); i1++) {
						  LinesOnMaster linesOnMaster = monitorStation.getLinesOnMasters().get(i1);
						  for (int i2 = 0; i2 < linesOnMaster.getLinesOnBranchs().size(); i2++) {
							  LinesOnBranch linesOnBranch = linesOnMaster.getLinesOnBranchs().get(i2);
							  List<MonitorPoint> monitorPoints = linesOnBranch.getMonitorPoints();
							  count+=monitorPoints.size();
						  }
					  }
				  }
				  CompanyDto  companyDto = this.companyDao.toDto(m, CompanyDto.class);
				  companyDto.setMonitorPointCount(count);
				//  logger.info(companyDto.getCompanyName()+":"+companyDto.getMonitorPointCount());
				  companyDtoList.add(companyDto);

			  });
		  //logger.warning("time comsuming :"+(System.currentTimeMillis()-start));
		  System.out.println("time comsuming :"+(System.currentTimeMillis()-start));
		  redisCache.set(NosqlKeyManager.CompanyList,JSON.toJSONString(companyDtoList));
	  }
	
	
	//@Scheduled(cron = "0/5 * * * * *")
    public void webSocket(){ 
		MemberInfo byId = memberService.getById("402881eb643a98b701643acf3ffb000e");
		System.out.println("user:"+JSON.toJSONString(byId));
		try {
		  TextMessage returnMessage = new TextMessage(JSON.toJSONString(byId));
		//	msgScoketHandle.sendMessageToUsers(returnMessage);
		} catch (Exception e) {
			Log.getCommon().error("test--websocket ",e);
		}
	 }
	


}
