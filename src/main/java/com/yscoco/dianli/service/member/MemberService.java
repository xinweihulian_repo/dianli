package com.yscoco.dianli.service.member;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.yscoco.dianli.common.base.Base;
import com.yscoco.dianli.common.exception.BizException;
import com.yscoco.dianli.common.handler.PageData;
import com.yscoco.dianli.common.handler.QueryHandler;
import com.yscoco.dianli.common.utils.MatcheUtils;
import com.yscoco.dianli.common.utils.Md5;
import com.yscoco.dianli.common.vo.Code;
import com.yscoco.dianli.common.vo.Message;
import com.yscoco.dianli.common.vo.VcodeType;
import com.yscoco.dianli.dao.app.AppLogDao;
import com.yscoco.dianli.dao.company.LinesOnBranchDao;
import com.yscoco.dianli.dao.company.MonitorPointDao;
import com.yscoco.dianli.dao.company.MonitorStationDao;
import com.yscoco.dianli.dao.member.MemberInfoDao;
import com.yscoco.dianli.dto.member.MemberInfoDto;
import com.yscoco.dianli.entity.company.LinesOnBranch;
import com.yscoco.dianli.entity.company.MonitorStation;
import com.yscoco.dianli.entity.member.MemberInfo;
import com.yscoco.dianli.other.cache.CacheClientI;
import com.yscoco.dianli.other.cache.RedisCacheUtil;
import com.yscoco.dianli.pojo.MemberModel;
import com.yscoco.dianli.service.app.VcodeVerifyService;

@Service
public class MemberService extends Base {
	
	@Resource
    private RedisCacheUtil redisCache;

	@Autowired
	private MemberInfoDao memberInfoDao;
	
	@Autowired
	private AppLogDao appLogDao;
	
	@Autowired
	private MonitorPointDao monitorPointDao;
	
	@Autowired
	private CacheClientI cacheClientI;
	
	@Autowired
	private LinesOnBranchDao linesOnBranchDao;
	
	@Autowired
	private MonitorStationDao monitorStationDao;
	
	@Autowired
	private VcodeVerifyService vcodeVerifyService;
	
	public MemberInfo getByToken(String token){
		MemberInfo memberInfo = memberInfoDao.getByToken(token);
		return memberInfo;
	}
	
	public Message<MemberInfoDto> wxLogin(String phoneNumner,String openId){
		if(empty(phoneNumner)||empty(openId) ){
			throw new BizException(Code.NOT_PARAM);
		}
		MemberInfo mi = null;
		if(StringUtils.isNotEmpty(phoneNumner)){
			MemberInfo queryOne1 = memberInfoDao.queryOne("userName", phoneNumner);
			if(!empty(queryOne1)){
				mi=queryOne1;
			}
			MemberInfo queryOne2 = memberInfoDao.queryOne("mobile", phoneNumner);
			if(!empty(queryOne2)){
				mi=queryOne2;
			}
			MemberInfo queryOne3 = memberInfoDao.queryOne("email", phoneNumner);
			if(!empty(queryOne3)){
				mi=queryOne3;
			}
		}

		if(mi == null ){
			throw new BizException(Code.NOT_ACCOUNT);
		}
		if(StringUtils.isEmpty(mi.getToken())){
			mi.setToken(createToken(mi));
		}
		// 整理返回给前端，否则出问题
		MemberInfoDto dto = new MemberInfoDto();
		//设置openId
		mi.setOpenId(openId);
		dto = this.memberInfoDao.toDto(mi, MemberInfoDto.class);
		redisCache.hset(dto.getOpenId(),  dto.getOpenId(), JSON.toJSONString(dto));
		
		return new Message<MemberInfoDto>(Code.SUCCESS,dto);
	}
	
	public Message<MemberInfoDto> login(String account ,String password,String ipAddress,String openId){
		if(empty(account) || empty(password)){
			throw new BizException(Code.NOT_PARAM);
		}
		MemberInfo mi = null;
		if(StringUtils.isNotEmpty(account)){
			MemberInfo queryOne1 = memberInfoDao.queryOne("userName", account);
			if(!empty(queryOne1)){
				mi=queryOne1;
			}
			MemberInfo queryOne2 = memberInfoDao.queryOne("mobile", account);
			if(!empty(queryOne2)){
				mi=queryOne2;
			}
			MemberInfo queryOne3 = memberInfoDao.queryOne("email", account);
			if(!empty(queryOne3)){
				mi=queryOne3;
			}
		}
//		if(empty(mi = getByUserNameOrMobileOrEmail(account))){
//			Map<String,Object> map = new HashMap<>();
//			   map.put("mobile", account);
//			   mi = memberInfoDao.queryOne(map);
//		}
		
		if(mi == null ){
			throw new BizException(Code.NOT_ACCOUNT);
		}
		if(!checkPwd(password,mi)){
			throw new BizException(Code.ERR_PWD);
		}
		if(StringUtils.isEmpty(mi.getToken())){
			mi.setToken(createToken(mi));
		}
		// 整理返回给前端，否则出问题
		MemberInfoDto dto = new MemberInfoDto();
		mi.setOpenId(openId);
		/*dto.setId(mi.getId());
		dto.setCreateTime(mi.getCreateTime());
		dto.setCreateBy(mi.getCreateBy());
		
		dto.setUserName(mi.getUserName());
		dto.setAvatar(mi.getAvatar());
		dto.setAccount(mi.getAccount());
		dto.setMobile(mi.getMobile());
		dto.setEmail(mi.getEmail());
		dto.setPassword(mi.getPassword());
		dto.setSex(mi.getSex());
		dto.setToken(mi.getToken());
		dto.setOpenId(mi.getOpenId());
		dto.setSetting(mi.getSetting());
		dto.setPushId(mi.getPushId());
		dto.setCompanyId(mi.getCompanyId());
		Set<MonitorPointDto> monitorPointDtoSet = new HashSet<MonitorPointDto>();
		if(mi != null && mi.getMonitorPoints() != null ){
			for (MonitorPoint monitorPoint : mi.getMonitorPoints()) {
				monitorPointDtoSet.add(this.monitorPointDao.toDto(monitorPoint, MonitorPointDto.class));
			}
		}*/
//		dto.setMonitorPointDtos(monitorPointDtoSet);
		
		
		dto = this.memberInfoDao.toDto(mi, MemberInfoDto.class);
		
		appLogDao.recordIpInfo(ipAddress, mi,null);// 记录日志
		return new Message<MemberInfoDto>(Code.SUCCESS,dto);
	}
	
	/**
	 * 根据 account 判断是手机、邮箱、或者用户名类型
	 * @param account
	 * @return
	 */
	public MemberInfo getByUserNameOrMobileOrEmail(String account){
		MemberInfo mi = null;
		if(MatcheUtils.isUserName(account)){
			mi = memberInfoDao.queryOne("userName", account);
		}else if(MatcheUtils.isMobile(account)){
			mi = memberInfoDao.queryOne("mobile", account);
		}else if(MatcheUtils.isEmail(account)){
			mi = memberInfoDao.queryOne("email", account);
		}
		return  mi;
	}
	
	
	public MemberInfoDto register(String userName , String password, String nickName , String loginDevice,String mobileOrEmail,String smsCode) {
		vcodeVerifyService.verifyVcode(mobileOrEmail, VcodeType.REG_MOBILE, smsCode);
		if(StringUtils.isNotEmpty(userName)){
			if(!MatcheUtils.isUserName(userName)){
				throw new BizException(Code.ERR_USERNAME);
			}
			if(memberInfoDao.queryOne("userName", userName)!=null){
				throw new BizException(Code.EXIST_USERNAME);
			}
		}
		MemberInfo mi = new MemberInfo();
		mi.setUserName(userName);
		mi.setPassword(password);
        mi.setToken(createToken(mi));
		if(MatcheUtils.isMobile(mobileOrEmail)){
			if(memberInfoDao.queryOne("mobile", mobileOrEmail)!=null){
				throw new BizException(Code.EXIST_MOBILE);
			}
			mi.setMobile(mobileOrEmail);
		} else if(MatcheUtils.isEmail(mobileOrEmail)){
			if(memberInfoDao.queryOne("eamil", mobileOrEmail)!=null){
				throw new BizException(Code.EXIST_EAMIL);
			}
			mi.setEmail(mobileOrEmail);
		}else{
			throw new BizException(Code.ERR_MOBILE_EMAIL);
		}
		memberInfoDao.save(mi);
		return this.memberInfoDao.toDto(mi, MemberInfoDto.class);
		
	}
	
	public void updateMobileOrEmail(String mobileOrEmail,String id,String pwd,String smsCode){
		vcodeVerifyService.verifyVcode(mobileOrEmail, VcodeType.BIND_MOBILE, smsCode);
		MemberInfo mi = memberInfoDao.getEntity(id);
		if(!mi.getPassword().equals(pwd)){
			throw new BizException(Code.ERR_PWD);
		}
		if(MatcheUtils.isMobile(mobileOrEmail)){
			if( memberInfoDao.queryOne("mobile", mobileOrEmail) != null){ 
				throw new BizException(Code.EXIST_MOBILE);
			}
			mi.setMobile(mobileOrEmail);
		} else if(MatcheUtils.isEmail(mobileOrEmail)){
			if(memberInfoDao.queryOne("eamil", mobileOrEmail)!=null){
				throw new BizException(Code.EXIST_EAMIL);
			}
			mi.setEmail(mobileOrEmail);
		}
		
		mi.setToken(createToken(mi));
	}
     
	public void updateMemberManageMonitorStationId(String monitorStationId){
		List<MemberInfo> list = memberInfoDao.list(null);
		 for (MemberInfo mi : list) {
			 String newStr ="";
			   String tempMonitorStationId = mi.getMonitorStationId();
			     if(tempMonitorStationId!=null){
			    	 String replace = tempMonitorStationId.replace(monitorStationId, "");
			    	 String[] split = replace.split(",");
					 for (int i = 0; i < split.length; i++) {
					//	System.out.println("I:"+split[i]);
						if(split[i].length()!=0){
							newStr+=split[i]+",";
						}
					}
			     }
		    	mi.setMonitorStationId(newStr);
		}
	}
	
	public Message<MemberInfoDto> update(MemberInfoDto dto) {
		MemberInfo mi = memberInfoDao.getEntity(dto.getId());
		if(mi == null){
			throw new BizException(Code.ERR_PARAM);
		}
		if(StringUtils.isNotEmpty(dto.getPassword())){//修改密码
			if(mi.getPassword().equals(dto.getOldPassword())){
				mi.setPassword(dto.getPassword());
				mi.setToken(createToken(mi));
			}else{
				return new Message<>(Code.ERR_PWD);
			}
		}
		if(notEmpty(dto.getAccount())){//修改账号
			mi.setAccount(dto.getAccount());
		}
		if(notEmpty(dto.getUserName())){//修改用户名
			mi.setUserName(dto.getUserName());
		}
		if(notEmpty(dto.getEmail())){//修改邮箱
			mi.setEmail(dto.getEmail());
		}
		if(StringUtils.isNotEmpty(dto.getSetting())){
			if(dto.getSetting().startsWith("{") && dto.getSetting().endsWith("}")){
				mi.setSetting(dto.getSetting());
			}
		}
		return new Message<MemberInfoDto>(Code.SUCCESS,this.memberInfoDao.toDto(mi, MemberInfoDto.class));
	}
	
	private boolean checkPwd (String password,MemberInfo mi ) {
		return password.equals(mi.getPassword());
	}
	private String createToken(MemberInfo mi){
		String src = mi.getMobile() + System.currentTimeMillis();
		try {
			String token = Md5.En(src);
			if(StringUtils.isNotEmpty(mi.getId())){
			    cacheClientI.set(token, mi.getId());
			    cacheClientI.delete(mi.getToken());
			}
			return token;
		} catch (IOException e) {
			return UUID.randomUUID().toString();
		}
	}

	public Message<MemberInfoDto> updatePassword(String password, String oldPassword,String memberId) {
		if(StringUtils.isEmpty(password) || StringUtils.isEmpty(oldPassword)){
			throw new BizException(Code.NOT_PARAM);
		}
		if(password.equalsIgnoreCase(oldPassword)){
			throw new BizException(Code.NOT_SAME);
		}
		MemberInfo memberInfo = this.memberInfoDao.getEntity(memberId);
		memberInfo.setPassword(password);
		memberInfo.setOldPassword(oldPassword);
		return Message.success(this.memberInfoDao.toDto(memberInfo, MemberInfoDto.class));
	}
	
	public void updatePasswordBySmsCode(String mobileOrEmail,String password,String smsCode) {
		if(empty(password) || empty(mobileOrEmail)){
			throw new BizException(Code.NOT_PARAM);
		}
		MemberInfo queryOne = memberInfoDao.queryOne("mobile", mobileOrEmail);
		if(queryOne==null){
			throw new BizException(Code.ERR_MOBILE_EMAIL);
		}else if(queryOne.getPassword().equals(password)){
			throw new BizException(Code.NOT_SAME);
		}
		vcodeVerifyService.verifyVcode(mobileOrEmail, VcodeType.REST_PWD_MOBILE, smsCode);
		MemberInfo ma = getByUserNameOrMobileOrEmail(mobileOrEmail);
		ma.setToken(createToken(ma));
		ma.setPassword(password);
	}

	public boolean checkToken(String token){
		return getIdByToken(token) == null? false:true;
	}
	
	public String getIdByToken(String token) {
		String id = cacheClientI.get(token);
		if(id==null ){
			MemberInfo mi = memberInfoDao.getByToken(token);
			if(mi != null){
				id = mi.getId();
				//cacheClientI.set(token, mi.getId());
			}
		}
		return id;
	}
	

	public void checkMobileOrEmail(String mobileOrEmail) {
		if(MatcheUtils.isMobile(mobileOrEmail)){
			if(memberInfoDao.queryOne("mobile", mobileOrEmail)!=null){
				throw new BizException(Code.EXIST_MOBILE);
			}
		} else if(MatcheUtils.isEmail(mobileOrEmail)){
			if(memberInfoDao.queryOne("eamil", mobileOrEmail)!=null){
				throw new BizException(Code.EXIST_EAMIL);
			}
		}
	}
	
	public PageData<MemberInfoDto> queryPage(String memberId,String mobile,String userName,String companyId,Integer rows,Integer page){
		if(page == null){page = 1;}
		if(rows == null){rows = 10;}
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("page", page);
		params.put("rows", rows);
		if(StringUtils.isNotEmpty(memberId)){
			params.put("id", memberId);
		}
		if(StringUtils.isNotEmpty(mobile)){
			params.put("mobileLike", mobile);
		}
		if(StringUtils.isNotEmpty(userName)){
			params.put("userNameLike", userName);
		}
		if(StringUtils.isNotEmpty(companyId)){
			params.put("companyId", companyId);
		}
		params.put("order", "t.createTime desc");
		
		PageData<MemberInfoDto> data = new PageData<MemberInfoDto>();
		
	//	List<MemberInfoDto> listToDto = memberInfoDao.listToDto(params,MemberInfoDto.class);
		
		List<MemberInfo> listPage = memberInfoDao.listPage(params);
		List<MemberInfoDto> listToDto = memberInfoDao.toDto(listPage, MemberInfoDto.class);
		
		for (MemberInfoDto memberInfoDto : listToDto) {
//			String linesOnBranchName="";
//			String linesOnBranchIdStr = memberInfoDto.getLinesOnBranchId();
			String monitorStationName="";
			String monitorStationIdStr = memberInfoDto.getMonitorStationId();
			if(notEmpty(monitorStationIdStr)){
				String[] monitorStationIdArr = monitorStationIdStr.split(",");
				for (String monitorStationId : monitorStationIdArr) {
					 MonitorStation monitorStation = this.monitorStationDao.getEntity(monitorStationId);
					if(monitorStation != null){
						monitorStationName += monitorStation.getStationName()+",";
					}
				}
			}
			memberInfoDto.setMonitorStationName(monitorStationName);
			
			
			//===================== 下面可以去掉 ===========================================//
//			if(notEmpty(linesOnBranchIdStr)){
//				String[] linesOnBranchIdArr = linesOnBranchIdStr.split(",");
//				for (String linesOnBranchId : linesOnBranchIdArr) {
//					LinesOnBranch linesOnBranch = this.linesOnBranchDao.getEntity(linesOnBranchId);
//					if(linesOnBranch != null){
//						linesOnBranchName += linesOnBranch.getBranchName()+",";
//					}
//				}
//			}
//			memberInfoDto.setLinesOnBranchName(linesOnBranchName);
			//=====================    dong      =========================================//
		}
		
		data.setList(listToDto);
		data.setCount(memberInfoDao.count(params));
		return data;
	}

	public Message<?> loginOut(String loginMemberId) {
		MemberInfo memberInfo = this.memberInfoDao.getEntity(loginMemberId);
		memberInfo.setToken(null);
		return  Message.success();
	}
	
	
	public Message<MemberInfoDto> pushById(String token, String pushId) {
		MemberInfo info = null;
		if(notEmpty(pushId)){
			 info = this.memberInfoDao.queryOne("pushId", pushId);
			 if(info != null){
				 info.setPushId(null);
			 }
		}
		if(notEmpty(token)){
			info = this.memberInfoDao.getByToken(token);
			if(info != null){
				info.setPushId(null);
				info.setPushId(pushId);
			}
		}else{
			return new Message<MemberInfoDto>(Code.NULL_TOKEN);
		}
		return Message.success(this.memberInfoDao.toDto(info, MemberInfoDto.class));
		
	}

	
	
	public List<String> query2dayNotLogin(){
		return memberInfoDao.query2dayNotLogin();
	}

	public Message<?> deleteMembers(String memberIds) {
		if(notEmpty(memberIds)){
			String[] memberIdArr = memberIds.split(",");
			for (String memberId : memberIdArr) {
				MemberInfo memberInfo = this.memberInfoDao.getEntity(memberId);
				String linesOnBranchIdStr = memberInfo.getLinesOnBranchId();
				if(StringUtils.isBlank(linesOnBranchIdStr)){
					this.memberInfoDao.delete(memberInfo);
					return Message.success(memberIds);
				}
				String[] linesOnBranchIdArr = linesOnBranchIdStr.split(",");
				for (String linesOnBranchId : linesOnBranchIdArr) {
					LinesOnBranch linesOnBranch = this.linesOnBranchDao.getEntity(linesOnBranchId);
					linesOnBranch.setMemberInfo(null);
				}
				this.memberInfoDao.delete(memberInfo);
			}
		}
		return Message.success(memberIds);
		
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	public MemberInfo getById(String id){
		MemberInfo mi = new MemberInfo();
		if(notEmpty(id)){
			mi = memberInfoDao.queryOne("id", id);
		}
		return  mi;
	}
	
	public List<MemberInfo> list(){
		List<MemberInfo> list = memberInfoDao.list(null);
		return list;
	}
	
	public Message<List<MemberModel>> getMemberInfoListByCompanyId(String companyId){
		List<MemberModel> queryAllMemberByCompanyId = memberInfoDao.queryAllMemberByCompanyId(companyId);
		return Message.success(queryAllMemberByCompanyId);
	}


    public MemberInfo edit(MemberInfo memberInfo){
       memberInfoDao.update(memberInfo);
         MemberInfo memberInfo1 = memberInfoDao.queryOne("id", memberInfo.getId());
        return memberInfo1;
    }

    public MemberInfo getByMobile(String phoneNum){
        return   memberInfoDao.queryOne("mobile", phoneNum);
    }


}
