package com.yscoco.dianli.service.sys;

import com.yscoco.dianli.dao.sys.LogEntityDao;
import com.yscoco.dianli.entity.sys.LogEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 * @author onion
 */
@Service
public class LogEntityService {
	
	@Autowired
	private LogEntityDao logEntityDao;
	
	
	public LogEntity getById(String id){return logEntityDao.getEntity(id);}

	public LogEntity save(LogEntity entity){
		LogEntity entity1 = logEntityDao.getEntity(logEntityDao.save(entity));
		return entity1;
	}
	public void delte(LogEntity entity){
		logEntityDao.delete(entity);
	}

}
