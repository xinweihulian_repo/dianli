package com.yscoco.dianli.service.sys;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.symmetric.AES;
import com.yscoco.dianli.other.cache.RedisCacheUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yscoco.dianli.common.base.Base;
import com.yscoco.dianli.common.exception.BizException;
import com.yscoco.dianli.common.utils.MatcheUtils;
import com.yscoco.dianli.common.utils.Md5;
import com.yscoco.dianli.common.vo.Code;
import com.yscoco.dianli.common.vo.Message;
import com.yscoco.dianli.common.vo.VcodeType;
import com.yscoco.dianli.constants.BidConst;
import com.yscoco.dianli.dao.app.AppLogDao;
import com.yscoco.dianli.dao.company.MonitorPointDao;
import com.yscoco.dianli.dao.company.MonitorStationDao;
import com.yscoco.dianli.dao.member.MemberInfoDao;
import com.yscoco.dianli.dao.sys.SysUserDao;
import com.yscoco.dianli.dto.member.MemberInfoDto;
import com.yscoco.dianli.dto.sys.SysUserDto;
import com.yscoco.dianli.entity.company.MonitorPoint;
import com.yscoco.dianli.entity.company.MonitorStation;
import com.yscoco.dianli.entity.member.MemberInfo;
import com.yscoco.dianli.entity.sys.SysUser;
import com.yscoco.dianli.other.cache.CacheClientI;
import com.yscoco.dianli.service.app.VcodeVerifyService;

/**
 * @author lanston
 * @Description: 用户业务服务类
 * @date 2016-7-17 下午11:32:10
 */
@Service
public class SysUserService extends Base {

	@Autowired
	private SysUserDao sysUserDao;

	@Autowired
	private MemberInfoDao memberInfoDao;

	@Autowired
	private MonitorPointDao monitorPointDao;

	@Autowired
	private MonitorStationDao monitorStationDao;

	@Autowired
	private CacheClientI cacheClientI;
	
	@Autowired
	private VcodeVerifyService vcodeVerifyService;

	@Autowired
	private RedisCacheUtil redisCacheUtil;
	
	
	public SysUser getByToken(String token){
		SysUser user = sysUserDao.getByToken(token);
		return user;
	}

	public void delete(String ids, String addressIp, String managerId) {
		String[] idArr = ids.split(",");
		for (String id : idArr) {
			SysUser sysUser = this.sysUserDao.getEntity(id);
			if (notEmpty(sysUser.getPermission()) && sysUser.getAccount().equalsIgnoreCase("admin")) {
				throw new BizException(Code.ERR_DELETE);
			}
			sysUserDao.delete(id);
		}
	}


	public List<SysUserDto> listPage(Map<String, Object> params) {
		/* return sysUserDao.listToDto(params, SysUserDto.class); */
		// this.companyDao.toDto(company, CompanyDto.class);
		List<SysUser> listPage = sysUserDao.listPage(params);
		List<SysUserDto> dto = sysUserDao.toDto(listPage, SysUserDto.class);
		/*
		 * List<SysUserDto> ss= new ArrayList<>(); for (SysUser sysUser :
		 * listPage) { SysUserDto dto = this.sysUserDao.toDto(sysUser,
		 * SysUserDto.class); ss.add(dto); }
		 */
		return dto;
	}

	// 判断是否存在
	public int count(Map<String, Object> params) {
		return sysUserDao.count(params);
	}

	/**
	 * @discription 用户登录 appLogDao.recordIpInfo(ipAddress,null, user);
	 * @author lansiming
	 * @created 2016-7-18 上午8:59:17
	 */
	public Message<SysUserDto> login(String account, String pwd, String addressIp) {
		if (empty(account) || empty(pwd)) {
			return new Message<>(Code.NOT_PARAM);
		}
		SysUser user = new SysUser();
		user = sysUserDao.getByAccount(account);
		if (empty(user)) {
			Map<String, Object> map = new HashMap<>();
			map.put("mobilePhone", account);
			user = sysUserDao.queryOne(map);
		}
		if (empty(user)) {
			return new Message<>(Code.NOT_ACCOUNT);
		}

		if (!checkPwd(pwd, user.getPassword())) {
			return new Message<>(Code.ERR_PWD);
		}
		user.setToken(createToken(user));
	   

		return new Message<SysUserDto>(Code.SUCCESS, this.sysUserDao.toDto(user, SysUserDto.class));
	}

	public static void main(String[] args) {
		String str="123456";
		for (int i = 0; i <100000 ; i++) {
			String sm = SecureUtil.md5(str);
			System.out.println("SecureUtil:"+sm);
			try {
				String en = Md5.En(str);
				System.out.println("Md5:"+en);
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}


	}

	public Message<SysUserDto> addOrUpdateSysUser(String userId, String position, String account, String password,
			String mobilePhone, String sysUserId, String permission) {
		// 先检查是否重名
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("account", account);
//		params.put("mobilePhone", mobilePhone);
		// 创建管理员用户
		SysUser sysUser = null;
		if (notEmpty(userId)) {//编辑
			sysUser = this.sysUserDao.getEntity(userId);
			if (notEmpty(sysUser.getPermission()) && sysUser.getPermission().equals("admin")) {
				throw new BizException(Code.ERR_INFO);
			}
			SysUser queryOne = sysUserDao.queryOne("id", userId);
//			if(notEmpty(account)&&!queryOne.getPermission().equals(account)){
//				SysUser queryOneByaccount = sysUserDao.queryOne("account", account);
//				if(queryOneByaccount!=null){
//					throw new BizException(Code.EXIST_USERNAME);
//				}
//			}
			if(!queryOne.getMobilePhone().equals(mobilePhone)){
				if(notEmpty(mobilePhone)){
					SysUser queryOneBymobilePhone = sysUserDao.queryOne("mobilePhone", mobilePhone);
					if(queryOneBymobilePhone!=null){
						throw new BizException(Code.EXIST_MOBILE);
					}
				}
			}
			sysUser.setModifiedBy(sysUserId);//修改用户人ID
			sysUser.setModifyTime(getDate());
			sysUser.setAccount(queryOne.getAccount());
			saveOrUpdateBean(position, account, password, mobilePhone, permission, sysUser);
		} else {//新增
			if(notEmpty(account)){
				SysUser queryOneByaccount = sysUserDao.queryOne("account", account);
				if(queryOneByaccount!=null){
					throw new BizException(Code.EXIST_USERNAME);
				}
			}
			if(notEmpty(mobilePhone)){
				SysUser queryOneBymobilePhone = sysUserDao.queryOne("mobilePhone", mobilePhone);
				if(queryOneBymobilePhone!=null){
					throw new BizException(Code.EXIST_MOBILE);
				}
			}
			sysUser = new SysUser();
			sysUser.setCreateBy(sysUserId);
			sysUser.setCreateTime(getDate());
			sysUser = saveOrUpdateBean(position, account, password, mobilePhone, permission, sysUser);
			this.sysUserDao.save(sysUser);
		}
		return new Message<SysUserDto>(Code.SUCCESS, this.sysUserDao.toDto(sysUser, SysUserDto.class));// this.sysUserDao.toDto(sysUser,
																										// SysUserDto.class)
	}

	public SysUser saveOrUpdateBean(String position, String account, String password, String mobilePhone,String permission, SysUser sysUser) {
		if (sysUser == null) {
			throw new BizException(Code.ERR_OTHER);
		}
		sysUser.setPosition(position);
		sysUser.setAccount(account);
		sysUser.setPassword( SecureUtil.md5(password));
		sysUser.setMobilePhone(mobilePhone);
		sysUser.setPermission(permission);
		return sysUser;
	}

	// ======================== 配置超级管理员监听器 ===================
	public void initAdmin() throws Exception {
		// 检查系统是否有管理员
		Boolean count = this.sysUserDao.countByMemberType();
		// 如果没有就创建一个默认的,并添加
		if (!count) {
			SysUser sysUser = new SysUser();
			sysUser.setAccount(BidConst.DEFAULT_ADMIN_USERNAME);
			sysUser.setPassword(SecureUtil.md5(BidConst.DEFAULT_ADMIN_PASSWORD));
			sysUser.setPosition("超级管理员");
			sysUser.setPermission("admin");
			this.sysUserDao.save(sysUser);
		}
	}

	public String getIdByToken(String token) {
		String id = cacheClientI.get(token);
		if (id == null) {
			SysUser mi = sysUserDao.getByToken(token);
			if (mi != null) {
				id = mi.getId();
			//	cacheClientI.set(token, mi.getId());
			}
		}
		return id;
	}

	public boolean checkToken(String token) {
		return getIdByToken(token) == null ? false : true;
	}

	public SysUserDto getById(String id) {
		SysUser mi = sysUserDao.getEntity(id);
		return this.sysUserDao.toDto(mi, SysUserDto.class);
	}

	private boolean checkPwd(String reqPwd, String userPwd) {
		return userPwd.equals(SecureUtil.md5(reqPwd));
	}

	private String createToken(SysUser su) {
		String src = su.getAccount() + System.currentTimeMillis();
		try {
			return Md5.En(src);
		} catch (IOException e) {
			return UUID.randomUUID().toString();
		}
	}

	public Message<MemberInfoDto> addOrUpdateAPPUser(String memberId, String account, String userName, String mobile,
			String email, String password, String companyId, String monitorStationId, String linesOnBranchId,
			String loginManagerId, String addressIp) {
		MemberInfo mi = null;// 先检查是否重名
		SysUser sysUserManager = this.sysUserDao.getEntity(loginManagerId);// 创建管理员用户
		MemberInfo memberInfo = null;
		if (notEmpty(memberId)) {//编辑
			memberInfo = this.memberInfoDao.getEntity(memberId);
			memberInfo.setCreateByAccount(sysUserManager.getAccount());
			memberInfo.setCreateByMobile(sysUserManager.getMobilePhone());
			memberInfo.setModifyTime(getDate());
			memberInfo.setModifiedBy(loginManagerId);
			memberInfo.setModifyTime(getDate());
			saveOrUpdateAppBean(account, userName, mobile, email, password, companyId, monitorStationId,
					linesOnBranchId, memberInfo);
		} else {//新增
			if (MatcheUtils.isUserName(userName)) {
				mi = memberInfoDao.queryOne("userName", userName);
				if (mi != null) {
					return new Message<>(Code.EXIST_USERNAME);
				}
			} else if (MatcheUtils.isMobile(mobile)) {
				mi = memberInfoDao.queryOne("mobile", mobile);
				if (mi != null) {
					return new Message<>(Code.EXIST_MOBILE);
				}
			} else if (MatcheUtils.isEmail(email)) {
				mi = memberInfoDao.queryOne("email", email);
				if (mi != null) {
					return new Message<>(Code.EXIST_EAMIL);
				}
			}

			memberInfo = new MemberInfo();
				memberInfo.setCreateBy(loginManagerId);
				memberInfo.setCreateTime(getDate());
				memberInfo.setCreateByAccount(sysUserManager.getAccount());
				memberInfo.setCreateByMobile(sysUserManager.getMobilePhone());
			    memberInfo = saveOrUpdateAppBean(account, userName, mobile, email, password, companyId, monitorStationId,
					linesOnBranchId, memberInfo);
			this.memberInfoDao.save(memberInfo);
		}

		MemberInfoDto dto = new MemberInfoDto();
			dto.setId(memberInfo.getId());
			dto.setCreateTime(memberInfo.getCreateTime());
			dto.setCreateBy(memberInfo.getCreateBy());
			dto.setAccount(memberInfo.getAccount());
			dto.setUserName(memberInfo.getUserName());
			dto.setMobile(memberInfo.getMobile());
			dto.setEmail(memberInfo.getEmail());
			dto.setSex(memberInfo.getSex());
			dto.setPassword(memberInfo.getPassword());
			dto.setToken(memberInfo.getToken());
			dto.setOpenId(memberInfo.getOpenId());
			dto.setSetting(memberInfo.getSetting());
			dto.setPushId(memberInfo.getPushId());
			dto.setCompanyId(memberInfo.getCompanyId());
			dto.setMonitorStationId(memberInfo.getMonitorStationId());
		String monitorStationId2 = memberInfo.getMonitorStationId();
		StringBuilder sb  = new StringBuilder();
		if (notEmpty(monitorStationId2)) {
			if (monitorStationId2.contains(",")) {
				String[] monitorStationIdArr= monitorStationId2.split(",");
				  for (String monitorStationIds: monitorStationIdArr) {
					 MonitorStation entity = this.monitorStationDao.getEntity(monitorStationIds);
					  sb.append(entity.getStationName()).append(",");
				}
					// String[] arr_new = linesOnBranchId.split(",");
					// for (String linesOnBranchId_new : arr_new) {
					// LinesOnBranch linesOnBranch =
					// this.linesOnBranchDao.getEntity(linesOnBranchId_new);
					// linesOnBranch.setMemberInfo(memberInfo);
					// }
			}

		}
		dto.setMonitorStationName(sb.toString());
		// Set<MonitorPointDto> monitorPointDtoSet = new
		// HashSet<MonitorPointDto>();
		// if(mi != null && mi.getMonitorPoints() != null ) {
		// for (MonitorPoint monitorPoint : mi.getMonitorPoints()){
		// monitorPointDtoSet.add(this.monitorPointDao.toDto(monitorPoint,MonitorPointDto.class));
		// }
		// }
		// dto.setMonitorPointDtos(monitorPointDtoSet);
		return new Message<MemberInfoDto>(Code.SUCCESS, dto);// this.sysUserDao.toDto(sysUser,
																// SysUserDto.class)
	}

	public MemberInfo saveOrUpdateAppBean(String account, String userName, String mobile, String email, String password,
			String companyId, String monitorStationId, String linesOnBranchId, MemberInfo memberInfo) {
		if (memberInfo == null) {
			throw new BizException(Code.ERR_OTHER);
		}
		if (notEmpty(account)) {
			memberInfo.setAccount(account);
		}
		if (notEmpty(userName)) {
			memberInfo.setUserName(userName);
		}
		if (notEmpty(mobile)) {
			memberInfo.setMobile(mobile);
		}
		if (notEmpty(email)) {
			memberInfo.setEmail(email);
		}

		if (notEmpty(password)) {
			if (notEmpty(memberInfo.getId())) {
				if (!password.equalsIgnoreCase(memberInfo.getPassword())) {
//					 try {
//					     memberInfo.setPassword(Md5.En(password));
//					 } catch (IOException e) {
//					 e.printStackTrace();
//					 }
				}
			} else {
				memberInfo.setPassword(password);
				System.out.println("password:" + password);
			}
			 memberInfo.setPassword(password);
		}

		if (notEmpty(companyId)) {
			memberInfo.setCompanyId(companyId);
		}
		if(empty(monitorStationId)){
			memberInfo.setMonitorStationId("");
		}else{
			memberInfo.setMonitorStationId(monitorStationId);
		}
			
			
		// **************************************************************************************
		//if (empty(monitorStationId)) {
			// String monitorStationIdId_old_str =
			// memberInfo.getMonitorStationId();
			// if (notEmpty(monitorStationIdId_old_str)) {
			// String[] arr_old = monitorStationIdId_old_str.split(",");
			// for (String monitorStationIdId_old : arr_old) {
			// MonitorStation entity =
			// this.monitorStationDao.getEntity(monitorStationIdId_old);
			// //linesOnBranch.setMemberInfo(null);
			// }
			// }
			//memberInfo.setMonitorStationId(null);
	//	} else if (!monitorStationId.equalsIgnoreCase(memberInfo.getMonitorStationId())) {
			// 先清除自身的，再把新的注入进去
			// String linesOnBranchId_old_str = memberInfo.getLinesOnBranchId();
			// if (notEmpty(linesOnBranchId_old_str)) {
			// String[] arr_old = linesOnBranchId_old_str.split(",");
			// for (String linesOnBranchId_old : arr_old) {
			// LinesOnBranch linesOnBranch =
			// this.linesOnBranchDao.getEntity(linesOnBranchId_old);
			// linesOnBranch.setMemberInfo(null);
			// }
			// }

			//memberInfo.setMonitorStationId(monitorStationId);
			// 清除之前绑定到支线上的信息

			// if (linesOnBranchId.contains(",")) {
			// String[] arr_new = linesOnBranchId.split(",");
			// for (String linesOnBranchId_new : arr_new) {
			// LinesOnBranch linesOnBranch =
			// this.linesOnBranchDao.getEntity(linesOnBranchId_new);
			// linesOnBranch.setMemberInfo(memberInfo);
			// }
			// }

	//	}

		// **************************************************************************************
		// if (empty(linesOnBranchId)) {
		// String linesOnBranchId_old_str = memberInfo.getLinesOnBranchId();
		// if (notEmpty(linesOnBranchId_old_str)) {
		// String[] arr_old = linesOnBranchId_old_str.split(",");
		// for (String linesOnBranchId_old : arr_old) {
		// LinesOnBranch linesOnBranch =
		// this.linesOnBranchDao.getEntity(linesOnBranchId_old);
		// linesOnBranch.setMemberInfo(null);
		// }
		// }
		// memberInfo.setLinesOnBranchId(linesOnBranchId);
		// } else if
		// (!linesOnBranchId.equalsIgnoreCase(memberInfo.getLinesOnBranchId()))
		// {
		// // 先清除自身的，再把新的注入进去
		// String linesOnBranchId_old_str = memberInfo.getLinesOnBranchId();
		// if (notEmpty(linesOnBranchId_old_str)) {
		// String[] arr_old = linesOnBranchId_old_str.split(",");
		// for (String linesOnBranchId_old : arr_old) {
		// LinesOnBranch linesOnBranch =
		// this.linesOnBranchDao.getEntity(linesOnBranchId_old);
		// linesOnBranch.setMemberInfo(null);
		// }
		// }
		//
		// memberInfo.setLinesOnBranchId(linesOnBranchId);
		// // 清除之前绑定到支线上的信息
		//
		// if (linesOnBranchId.contains(",")) {
		// String[] arr_new = linesOnBranchId.split(",");
		// for (String linesOnBranchId_new : arr_new) {
		// LinesOnBranch linesOnBranch =
		// this.linesOnBranchDao.getEntity(linesOnBranchId_new);
		// linesOnBranch.setMemberInfo(memberInfo);
		// }
		// }
		//
		// }
		return memberInfo;
	}

	// 获取监测点列表
	public Set<MonitorPoint> getMonitorPointByIds(String monitorPointIds, MemberInfo memberInfo) {
		Set<MonitorPoint> monitorPointSet = new HashSet<>();
		if (notEmpty(monitorPointIds)) {
			String[] ids = monitorPointIds.split(",");
			for (String id : ids) {
				MonitorPoint monitorPoint = this.monitorPointDao.getEntity(id);
				monitorPointSet.add(monitorPoint);
			}
		}
		return monitorPointSet;
	}

	public Message<?> loginOut(String loginManagerId) {
		SysUser sysUser = this.sysUserDao.getEntity(loginManagerId);
		sysUser.setToken(null);
		return Message.success();
	}
	
	/**
	 * 根据 account 判断是手机、邮箱、或者用户名类型
	 * @param account
	 * @return
	 */
	public SysUser getByUserNameOrMobileOrEmail(String account){
		SysUser user = null;
		if(MatcheUtils.isUserName(account)){
			user = sysUserDao.queryOne("account", account);
		}else if(MatcheUtils.isMobile(account)){
			user = sysUserDao.queryOne("mobilePhone", account);
		}else if(MatcheUtils.isEmail(account)){
			user = sysUserDao.queryOne("email", account);
		}
		return  user;
	}
	
	public void updatePasswordBySmsCode(String mobileOrEmail,String password,String smsCode) {
		if(empty(password) || empty(mobileOrEmail)){
			throw new BizException(Code.NOT_PARAM);
		}
		SysUser queryOne = sysUserDao.queryOne("mobilePhone", mobileOrEmail);
		if(queryOne==null){
			throw new BizException(Code.ERR_MOBILE_EMAIL);
		}else if(queryOne.getPassword().equals(password)){
			throw new BizException(Code.NOT_SAME);
		}
		vcodeVerifyService.verifyVcode(mobileOrEmail, VcodeType.REST_PWD_MOBILE, smsCode);
		SysUser user = getByUserNameOrMobileOrEmail(mobileOrEmail);
		user.setToken(createToken(user));
		user.setPassword(password);
	}

	public SysUser getByAccount(String account) {
		SysUser user = null;
			user = sysUserDao.queryOne("account", account);
			if(null==user){
				user = sysUserDao.queryOne("mobilePhone", account);
			}
		return  user;
	}

	public void edit(SysUser sysUser){
		sysUserDao.update(sysUser);
	}


}
