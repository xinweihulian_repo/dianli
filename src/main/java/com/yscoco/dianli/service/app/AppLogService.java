package com.yscoco.dianli.service.app;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yscoco.dianli.common.handler.PageData;
import com.yscoco.dianli.dao.app.AppLogDao;
import com.yscoco.dianli.entity.app.AppLog;

@Service
public class AppLogService {
	
	@Autowired
	AppLogDao appLogDao;
	
	public void save(AppLog log){
		log.setId(null);
		appLogDao.save(log);
	}
	
	public PageData<AppLog> queryToPage(Integer page,Integer rows){
		PageData<AppLog> pageData = new PageData<AppLog>();
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("page", page);
		params.put("rows", rows);
		params.put("order", "t.createTime desc");
		pageData.setList(appLogDao.listPage(params));
		pageData.setCount(appLogDao.count(params));
		return pageData;
	}
	
	public void delete(String ids){
		String[] idsArr = ids.split(",");
		for (String id : idsArr) {
			this.appLogDao.delete(id);
		}
	}
	
}
