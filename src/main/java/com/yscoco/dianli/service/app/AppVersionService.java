package com.yscoco.dianli.service.app;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yscoco.dianli.common.base.Base;
import com.yscoco.dianli.common.exception.BizException;
import com.yscoco.dianli.common.handler.PageData;
import com.yscoco.dianli.common.vo.Code;
import com.yscoco.dianli.common.vo.Message;
import com.yscoco.dianli.constants.AppLogType;
import com.yscoco.dianli.dao.app.AppLogDao;
import com.yscoco.dianli.dao.app.AppVersionDao;
import com.yscoco.dianli.dto.app.AppVersionDto;
import com.yscoco.dianli.entity.app.AppVersion;

@Service
public class AppVersionService extends Base{
	
	@Autowired
	AppVersionDao appVersionDao;
	
	@Autowired
	private AppLogDao appLogDao;
	
	public Message<AppVersionDto> addOrUpdateAppVersion(String appVersionId, String versionName,String isMust,String url, String remarks,String identify,String version, String managerId, String addressIp) {
		AppVersion appVersion = null;
		if(notEmpty(appVersionId)){
			if(!notEmpty(url)){
				throw new BizException(Code.Input_Address);
			}
			appVersion = this.appVersionDao.getEntity(appVersionId);
			addOrUpdateDetails(versionName, isMust, url,remarks, identify,version, appVersion);
			// 记录日志
			appLogDao.recordActionOnAppVersion(addressIp, managerId,AppLogType.UPDATE,appVersion);
		}else{
			if(!notEmpty(url)){
				throw new BizException(Code.Input_Address);
			}
			appVersion = new AppVersion();
			appVersion = addOrUpdateDetails(versionName, isMust, url,remarks,identify, version, appVersion);
			this.appVersionDao.save(appVersion);
			// 记录日志
			appLogDao.recordActionOnAppVersion(addressIp, managerId,AppLogType.ADD,appVersion);
		}
		return new Message<AppVersionDto>(Code.SUCCESS,this.appVersionDao.toDto(appVersion, AppVersionDto.class));
	}
	
	private AppVersion addOrUpdateDetails(String versionName,String isMust, String url,String remarks,String identify,String version,AppVersion appVersion) {
		if(appVersion != null && notEmpty(versionName)){
			appVersion.setVersionName(versionName);
		}
		if(appVersion != null && notEmpty(isMust)){
			appVersion.setIsMust(isMust);
		}
		if(appVersion != null && notEmpty(url)){
			appVersion.setUrl(url);
		}
		if(appVersion != null && notEmpty(remarks)){
			appVersion.setRemarks(remarks);
		}
		if(appVersion != null && notEmpty(version)){
			appVersion.setVersion(version);
		}
		if(appVersion != null && notEmpty(identify)){
			appVersion.setIdentify(identify);
		}
		return appVersion;
	}
	
	public void delete(String ids,String managerId, String addressIp) {
		String[] idArr = ids.split(",");
		for (String id : idArr) {
			AppVersion appVersion = this.appVersionDao.getEntity(id);
		    appLogDao.recordActionOnAppVersion(addressIp, managerId,AppLogType.DELETE,appVersion);
			appVersionDao.delete(id);
		}
	}
	
	
	public AppVersion getNewest(String identify){
		return appVersionDao.getNewest(identify);
	}
	
	public PageData<AppVersion> listPage( String appVersionId, Integer page,Integer rows){
		page = (page == null)?0:page;
		rows = (rows == null)?10:rows;
		Map<String,Object> params = new HashMap<>();
		if(StringUtils.isNotEmpty(appVersionId)){
			params.put("id", appVersionId);
		}
		params.put("page", page);
		params.put("rows", rows);
		params.put("order", "createTime desc");	
		PageData<AppVersion> pageData = new PageData<AppVersion>();
		pageData.setList(appVersionDao.listPage(params));
		pageData.setCount(appVersionDao.count(params));
		return pageData;
	}

	
}
