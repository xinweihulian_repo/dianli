package com.yscoco.dianli.service.app;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.yscoco.dianli.common.exception.BizException;
import com.yscoco.dianli.common.utils.DateUtils;
import com.yscoco.dianli.common.utils.Log;
import com.yscoco.dianli.common.utils.MatcheUtils;
import com.yscoco.dianli.common.utils.RandomUtils;
import com.yscoco.dianli.common.vo.Code;
import com.yscoco.dianli.common.vo.VcodeType;
import com.yscoco.dianli.constants.ConfigConts;
import com.yscoco.dianli.dao.app.VcodeVerifyDao;
import com.yscoco.dianli.entity.app.VcodeVerify;
import com.yscoco.dianli.other.sms.SmsClient;
import com.yscoco.dianli.other.sms.SmsVerification;


@Service
public class VcodeVerifyService {

	String TEST= ConfigConts.getString("SMS_TEST");
	Integer SMS_LIMIT= Integer.valueOf(ConfigConts.getString("SMS_LIMIT"));
	Integer SMS_LIMIT_HOUR= Integer.valueOf(ConfigConts.getString("SMS_LIMIT_HOUR"));
	
	@Autowired
	VcodeVerifyDao vcodeVerifyDao;
	
	@Autowired
	JavaMailSender javaMailSender;
	/**
	 * 发送验证码
	 */
	public void saveAndSendVcode(String key, VcodeType vcodeType) {
		checkSendLimit(key);
		
		String vcode = RandomUtils.getNumCode(6);
		if("true".equals(TEST)){
			vcode="666666";
		}else{
			if(MatcheUtils.isMobile(key)){//手机
				sendSms(key, vcode, String.valueOf(vcodeType.getExpire()));
			}
			else if (MatcheUtils.isEmail(key)) {
				StringBuffer content = new StringBuffer("");
				content.append("<html><table><tr><td>Dear user, your verification code is:").append(vcode)
						.append("</td></tr></table></html>");

				try {
					MimeMessage message = javaMailSender.createMimeMessage();
					MimeMessageHelper helper = new MimeMessageHelper(message, true);
					helper.setFrom(ConfigConts.MAIL_FROM, "Repair");
					helper.setTo(key);
					helper.setSubject("Verification code");
					helper.setText(content.toString(), true);
					javaMailSender.send(message);
				} catch (Exception e) {
					Log.getCommon().error("send email error",e);
				}
			}
		}

		VcodeVerify vcodeVerify = new VcodeVerify();
		
		Calendar c = Calendar.getInstance();
		c.add(Calendar.MINUTE, vcodeType.getExpire());
		vcodeVerify.setExpireDate(c.getTime());
		vcodeVerify.setVcodeType(vcodeType);
		vcodeVerify.setVerifyKey(key);
		vcodeVerify.setVcodeValue(vcode);
		vcodeVerifyDao.save(vcodeVerify);
	}
	
	
	public void checkSendLimit(String verifyKey){
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("verifyKey", verifyKey);
		params.put("createTimeGt", DateUtils.addHour(new Date(), -SMS_LIMIT_HOUR));
		int limitCount = vcodeVerifyDao.count(params);
		if(limitCount>= SMS_LIMIT){
			throw new BizException(Code.ERR_LIMIT);
		}
		
	}
	
	private void sendSms(String mobile,String code,String type ){
//		SmsClient.getInstance().sendSmsCode(mobile, code);
		SmsVerification.SendCode(mobile,code);
	}

	/**
	 * 验证验证码
	 * 
	 * @param key
	 * @param vcode
	 * @return
	 */

	public void verifyVcode(String verifyKey, VcodeType vcodeType, String vcode) {
	
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("verifyKey", verifyKey);
		params.put("vcodeValue", vcode);
		params.put("vcodeType", vcodeType);
		params.put("page", 1);
		params.put("rows", 1);
		params.put("order", "t.createTime desc");
		VcodeVerify vcodeVerify = vcodeVerifyDao.queryOne(params);
		if (vcodeVerify == null || vcodeVerify.getExpireDate().before(new Date())) {
			throw new BizException(Code.ERR_SMSCODE);
		}
	}

}
