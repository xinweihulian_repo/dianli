package com.yscoco.dianli.service.comon;


import com.alibaba.fastjson.JSON;
import com.yscoco.dianli.entity.sys.SysUser;
import lombok.Data;

import java.util.List;

/**
 * @Author: onion..
 * @Description: TODO
 * @Date: 2020/10/23 10:57
 */


@Data
public class CacheItem<T> extends CacheImpl<T> {

    private List<T> data;


    public static void main(String[] args) {

       // String text="[{\"id\":\"2c90121d742ec654017468066b4502bb\",\"createTime\":\"2020-09-07 18:04:45\",\"createBy\":\"402881eb643a98b701643aa6ef310002\",\"token\":\"07328c1eaab621c661e4a0c8385ed5ab\",\"mobilePhone\":\"13000000001\",\"position\":\"山西屯兰\",\"account\":\"shanxitunlan\",\"password\":\"123456\",\"permission\":\"添加、授权巡检员\"},{\"id\":\"2c90121d742ec65401742f1c4b140014\",\"createTime\":\"2020-08-27 16:50:17\",\"createBy\":\"402881eb643a98b701643aa6ef310002\",\"token\":\"d5198b99910bb4dcbd8350db96bb722b\",\"mobilePhone\":\"13100000000\",\"position\":\"延长石油管理员\",\"account\":\"ycsy\",\"password\":\"123456\",\"permission\":\"添加、授权巡检员\"},{\"id\":\"2c90121d7375f1e6017393666cfb00ec\",\"createTime\":\"2020-07-28 11:10:30\",\"createBy\":\"402881eb643a98b701643aa6ef310002\",\"token\":\"e7020825a50309c4614c006e3a7084af\",\"mobilePhone\":\"18168184498\",\"position\":\"冠益管理员\",\"account\":\"18168184498\",\"password\":\"123456\",\"permission\":\"添加、授权巡检员\"},{\"id\":\"2c90121d735694dc01735a4212aa0000\",\"createTime\":\"2020-07-17 08:52:26\",\"createBy\":\"402881eb643a98b701643aa6ef310002\",\"token\":\"e70dc72c86197d237a1f356291a446ad\",\"mobilePhone\":\"13000000000\",\"position\":\"唐山宝创\",\"account\":\"tsbc\",\"password\":\"123456\",\"permission\":\"添加、授权巡检员\"},{\"id\":\"2c90121d7346ca2101734760a1a60001\",\"createTime\":\"2020-07-13 16:53:02\",\"createBy\":\"402881eb643a98b701643aa6ef310001\",\"token\":\"5151fffda6b8e987d77d6134711ee776\",\"mobilePhone\":\"15071413152\",\"position\":\"天德技术部\",\"account\":\"15071413152\",\"password\":\"123456\",\"permission\":\"添加、授权巡检员\"},{\"id\":\"2c90121d6f8d40e5016f8d6056e80001\",\"createTime\":\"2020-01-10 10:54:59\",\"createBy\":\"402881eb643a86a701643a86bc3c0000\",\"token\":\"21b0d4accea9a65734bde72a63db5af5\",\"mobilePhone\":\"13912345678\",\"position\":\"本钢北营焦化焦炉\",\"account\":\"bgby\",\"password\":\"123456\",\"permission\":\"添加、授权巡检员\"},{\"id\":\"2c90121d6f20e6e4016f83d6086800ac\",\"createTime\":\"2020-01-08 14:27:20\",\"createBy\":\"402881eb643a86a701643a86bc3c0000\",\"token\":\"8870fda18a9c6f50647804158c60dc16\",\"mobilePhone\":\"13999999999\",\"position\":\"冠益荣信\",\"account\":\"GYRX\",\"password\":\"123456\",\"permission\":\"添加、授权巡检员\"},{\"id\":\"2c90121d6cb2ff2e016d0521ea10008b\",\"createTime\":\"2019-09-06 13:52:52\",\"createBy\":\"402881eb643a86a701643a86bc3c0000\",\"token\":\"5940228b98a28a8b2f2baf6ef88ea445\",\"mobilePhone\":\"13585806386\",\"position\":\"东洲罗顿通讯\",\"account\":\"13585806386\",\"password\":\"806386\",\"permission\":\"添加、授权巡检员\"},{\"id\":\"402881eb643a98b701643aa6ef310010\",\"createTime\":\"2019-08-30 11:51:48\",\"createBy\":\"\",\"token\":\"3816e1476621d8892741d90617f609a7\",\"mobilePhone\":\"\",\"position\":\"超级管理员\",\"account\":\"super\",\"password\":\"666\",\"permission\":\"admin\"},{\"id\":\"402881eb643a98b701643aa6ef310006\",\"createTime\":\"2019-06-28 16:03:44\",\"token\":\"4020a8919ef231db403ced5ef6921f52\",\"mobilePhone\":\"\",\"position\":\"超级管理员\",\"account\":\"admin6\",\"password\":\"666\",\"permission\":\"admin\"}]";

        String text="{\"createBy\":\"402881eb643a98b701643aa6ef310002\",\"password\":\"123456\",\"mobilePhone\":\"13000000001\",\"createTime\":1599473085000,\"permission\":\"添加、授权巡检员\",\"id\":\"2c90121d742ec654017468066b4502bb\",\"position\":\"山西屯兰\",\"delFlag\":\"N\",\"account\":\"shanxitunlan\",\"token\":\"07328c1eaab621c661e4a0c8385ed5ab\"}";


        CacheItem<SysUser> invok = new CacheItem<SysUser>().invok(text, SysUser.class);

        System.out.println(JSON.toJSON(invok));


        //CacheItem cacheItem = new CacheItem(SysUser.class).invok(text);

        //System.out.println(JSON.toJSON(cacheItem));







    }


}
