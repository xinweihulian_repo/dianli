package com.yscoco.dianli.service.comon;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.yscoco.dianli.dao.company.MonitorStationDao;
import com.yscoco.dianli.entity.company.MonitorStation;
import com.yscoco.dianli.entity.company.MonitorStationDevice;
import com.yscoco.dianli.service.company.MonitorStationDeviceService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yscoco.dianli.common.base.BaseController;
import com.yscoco.dianli.entity.member.MemberInfo;
import com.yscoco.dianli.entity.sys.SysUser;
import com.yscoco.dianli.pojo.MasterModel;
import com.yscoco.dianli.pojo.PointModel;
import com.yscoco.dianli.service.company.CompanyService;
import com.yscoco.dianli.service.member.MemberService;
import com.yscoco.dianli.service.sys.SysUserService;


/**
 * @Author  dong
 */
@Service
public class GetResponsibleDeviceService  extends BaseController{

	@Autowired
	private MemberService memberService;
	
	@Autowired
	private CompanyService companyService;
	
	@Autowired
	private SysUserService sysUserService;

	@Autowired
	private MonitorStationDao monitorStationDao;

	@Autowired
	private MonitorStationDeviceService monitorStationDeviceService;

	
	
	@SuppressWarnings("null")
	public  List<MasterModel> getMasterModelByLoginUser(String token) {
		return getMasterModels(token);
	}

	protected List<MasterModel> getMasterModels(String token) {
		List<MasterModel> masterModelList = new ArrayList<>();

		List<String> companyByUserId = null;
		MemberInfo meberInfo = memberService.getByToken(token);
		if (meberInfo != null) {
			List<MasterModel> queryAllMasterFlag = companyService.queryAllMasterFlag(meberInfo.getCompanyId());
			String[] monitorStationIdArr = meberInfo.getMonitorStationId().split(",");
			if (monitorStationIdArr == null && monitorStationIdArr.length < 0) {
				return  masterModelList;
			}
			for (String monitorStationIds : monitorStationIdArr) {
				queryAllMasterFlag.stream().forEach(m -> {
					if (StringUtils.equals(m.getStationId(), monitorStationIds)) {
						masterModelList.add(m);
					}
				});
			}
		} else {

			SysUser sysUser = sysUserService.getByToken(token);

			 if(sysUser==null){
				 return  masterModelList;
			 }
			if (sysUser.getPermission().trim().equalsIgnoreCase("admin")) {
				List<MasterModel> queryAllMasterFlag = companyService.queryAllMasterFlag(null);
				masterModelList.addAll(queryAllMasterFlag);
			} else {// 除Admin以外的普通管理员
				companyByUserId = companyService.getCompanyByUserId(this.getLoginManagerId());
				if (!companyByUserId.isEmpty()) { // 普通管理员
					for (String ids : companyByUserId) {
						List<MasterModel> queryAllMasterFlag = companyService.queryAllMasterFlag(ids);
						masterModelList.addAll(queryAllMasterFlag);
					}
				} else {
					return  masterModelList;
				}
			}
		}
		return masterModelList;
	}

	@SuppressWarnings("null")
	public  List<PointModel> getPointModelByLoginUser(String token) {
		return getPointModels(token);
	}

	protected List<PointModel> getPointModels(String token) {
		Map<String, Object> params = new HashMap<>();
		List<PointModel> pointModelList = new ArrayList<>();
		List<String> companyByUserId = null;
		MemberInfo meberInfo = memberService.getByToken(token);
		if (meberInfo != null) {
			List<PointModel> queryAllPointFlag = companyService.queryAllPointFlag(meberInfo.getCompanyId());

			String[] monitorStationIdArr = meberInfo.getMonitorStationId().split(",");
			if (monitorStationIdArr == null && monitorStationIdArr.length < 0) {
				return pointModelList ;

			}
            getMonitorStationDevice(queryAllPointFlag,params);
			for (String monitorStationIds : monitorStationIdArr) {
				queryAllPointFlag.stream().forEach(m -> {
					if (StringUtils.equals(m.getStationId(), monitorStationIds)) {
						pointModelList.add(m);
					}
				});
			}
		} else {
			SysUser sysUser = sysUserService.getByToken(token);

			if (sysUser!=null&&sysUser.getPermission().trim().equalsIgnoreCase("admin")) {
				List<PointModel> queryAllPointFlag = companyService.queryAllPointFlag(null);
				pointModelList.addAll(queryAllPointFlag);
                getMonitorStationDevice(pointModelList,params);

			} else {// 除Admin以外的普通管理员
				companyByUserId = companyService.getCompanyByUserId(this.getLoginManagerId());

				if (!companyByUserId.isEmpty()) { // 普通管理员
					for (String ids : companyByUserId) {
						List<PointModel> queryAllPointFlag = companyService.queryAllPointFlag(ids);
						pointModelList.addAll(queryAllPointFlag);
					}
                    params.put("createBy",sysUser.getId());
                    getMonitorStationDevice(pointModelList,params);
				} else {
					return pointModelList ;
				}
			}
		}

		return pointModelList;
	}

	private void getMonitorStationDevice(List<PointModel> queryAllPointFlag,Map<String, Object> params) {
        List<MonitorStation> list = monitorStationDao.list(params);
        
        List<String> stationId = new ArrayList<>();

		list.forEach(m->{
			if(!"1".equals(m.getStationType())){
				stationId.add(m.getId());
			}
        });

		if(stationId.isEmpty()){return;}

        stationId.forEach(m->{
            PointModel model = null;
            List<MonitorStationDevice> byStationId = monitorStationDeviceService.getByStationId(m);
			for (int i = 0; i < byStationId.size(); i++) {
			     model = new PointModel();
                 model.setPointFlag( byStationId.get(i).getDeviceId());
                 model.setPointName(byStationId.get(i).getDeviceName());
                 model.setPosition(byStationId.get(i).getPosition());
                 model.setType(byStationId.get(i).getDeviceType());
                 model.setStationId(byStationId.get(i).getStationId());
                 model.setId(byStationId.get(i).getId());
                 model.setStationType(byStationId.get(i).getStationType());
                 model.setMasterName(" ");
                 model.setBranchName(" ");
                 model.setCabin_no(" ");
                 model.setMasterId(" ");
                 model.setHitchType(Transition.parseMonitorStationType(byStationId.get(i).getStationType()));
                 model.setMasterFlag(" ");
                 model.setBranchId(" ");

                 queryAllPointFlag.add(model);
			}
		});

            queryAllPointFlag.forEach(m->{
                list.forEach(re->{
                    if(m.getStationId().equals(re.getId())){
                        m.setStationName(re.getStationName());
                        m.setStationAddress(re.getStationAddress());
                        m.setCompanyId(re.getCompany().getId());
						m.setCompanyName(re.getCompany().getCompanyName());
                    }
                });
            });
	}

}
