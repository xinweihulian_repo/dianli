package com.yscoco.dianli.service.comon;

import com.alibaba.fastjson.JSON;
import com.yscoco.dianli.common.base.BaseController;
import com.yscoco.dianli.other.cache.RedisCacheUtil;
import com.yscoco.dianli.pojo.MasterModel;
import com.yscoco.dianli.pojo.PointModel;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.TimeUnit;


/**
 * @Author: onion..
 * @Description: TODO
 * @Date: 2020/10/22 9:27
 */
@Service
public class GetResponsibleDeviceInnerCache extends BaseController {

    @Resource
    private RedisCacheUtil redisCache;
    @Autowired
    private GetResponsibleDeviceService getResponsibleDeviceService;

    public List<MasterModel> getMasterModelByLoginUser(String token) {

        DeviceInfo invoke = new DeviceInfo(token,NosqlKeyManager.MasterModelListByLoginUser).invoke(MasterModel.class);

        if (invoke.is()) return invoke.getDeviceList();

        List<MasterModel> masterModels = getResponsibleDeviceService.getMasterModels(token);

        redisCache.set(NosqlKeyManager.MasterModelListByLoginUser,token, JSON.toJSONString(masterModels),1, TimeUnit.MINUTES);

        return  masterModels;
    }

    public  List<PointModel> getPointModelByLoginUser(String token) {

        DeviceInfo invoke = new DeviceInfo<PointModel>(token,NosqlKeyManager.PointModelListByLoginUser).invoke(PointModel.class);

        if (invoke.is()) return invoke.getDeviceList();

        List<PointModel> pointModels = getResponsibleDeviceService.getPointModels(token);

        redisCache.set(NosqlKeyManager.PointModelListByLoginUser,token, JSON.toJSONString(pointModels),1, TimeUnit.MINUTES);

        return  pointModels;
    }


    private  class DeviceInfo<T> {
        private boolean myResult;
        private String token;
        private List<T> deviceList;
        private String key;

        public DeviceInfo(String token, String key) {
            this.token = token;
            this.key = key;
        }



        boolean is() {
            return myResult;
        }

        public List<T> getDeviceList() {
            return deviceList;
        }

        public GetResponsibleDeviceInnerCache.DeviceInfo invoke(Class<T> clzz) {
        	
            String deviceListString = redisCache.get(key,token);

            if(StringUtils.isNotEmpty(deviceListString)){
                deviceList = JSON.parseArray(deviceListString,clzz);
                myResult = true;
                return this;
            }
            myResult = false;
            return this;
        }
    }


}
