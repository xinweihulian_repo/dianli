package com.yscoco.dianli.service.comon;

/**
 *  转换类
 *  根据故障类型ID转为具体故障 
 *  监测设备位置代码转换
 * @author liuDong.
 *
 */
public class Transition {
     
	/**
	 *  根据故障类型ID转为具体故障
	 * @param faultType
	 * @return
	 */
	public static String changFaultTypeByTypeId(Integer faultType){
		String fault ="";
		switch(faultType){
		case 10 :
			fault="金属接地故障";
			break;
		case 11 :
			fault="系统过电压";
			break;
		
		case 12 :
			fault="过电压故障";
			break;
		
		case 13 :
			fault="PT断线故障";
			break;
		
		case 14 :
			fault="欠压数据包";
			break;
		
		case 15 :
			fault="系统短路";
			break;
		case 16 :
			fault="弧光接地故障";
			break;
		case 20:
			fault="过热";
			break;
		case 21:
			fault="湿度超标";
			break;
			
		case 22:
			fault="泄漏";
			break;
		case 23:
			fault="电量不足";
			break;
			default:
				fault="未知";
				break;
      	}
		return fault;
	}
	
	
	/**
	 * 监测设备位置代码转换
	 * 
	 * @param position
	 *            字符串"0"-"18"
	 * @return
	 */
	public static String getConvertPositionName(String position) {
		switch (position) {
		case "0":
			position = "母排A相";
			break;
		case "1":
			position = "母排B相";
			break;
		case "2":
			position = "母排C相";
			break;
		case "3":
			position = "上动触头A相";
			break;
		case "4":
			position = "上动触头B相";
			break;
		case "5":
			position = "上动触头C相";
			break;
		case "6":
			position = "上静触头A相";
			break;
		case "7":
			position = "上静触头B相";
			break;
		case "8":
			position = "上静触头C相";
			break;
		case "9":
			position = "下动触头A相";
			break;
		case "10":
			position = "下动触头B相";
			break;
		case "11":
			position = "下动触头C相";
			break;
		case "12":
			position = "下静触头A相";
			break;
		case "13":
			position = "下静触头B相";
			break;
		case "14":
			position = "下静触头C相";
			break;
		case "15":
			position = "三相一体保护器";
			break;
		case "16":
			position = "保护器A相";
			break;
		case "17":
			position = "保护器B相";
			break;
		case "18":
			position = "保护器C相";
			break;
		default:
			position = "未知";
			break;
		}

		return position;
	}


	/**
	 * 监测设备位置代码转换
	 *
	 * @param name
	 * @return
	 */
	public static String getConvertPositionNumberByName(String name) {
		String numberStr="";
		switch (name.trim()) {
			case "母排A相":
				numberStr = "0";
				break;
			case "母排B相":
				numberStr = "1";
				break;
			case "母排C相":
				numberStr = "2";
				break;
			case "上动触头A相":
				numberStr = "3";
				break;
			case "上动触头B相":
				numberStr = "4";
				break;
			case "上动触头C相":
				numberStr = "5";
				break;
			case "上静触头A相":
				numberStr = "6";
				break;
			case "上静触头B相":
				numberStr = "7";
				break;
			case "上静触头C相":
				numberStr = "8";
				break;
			case "下动触头A相":
				numberStr = "9";
				break;
			case "下动触头B相":
				numberStr = "10";
				break;
			case "下动触头C相":
				numberStr = "11";
				break;
			case "下静触头A相":
				numberStr = "12";
				break;
			case "下静触头B相":
				numberStr = "13";
				break;
			case "下静触头C相":
				numberStr = "14";
				break;
			case "三相一体保护器":
				numberStr = "15";
				break;
			case "保护器A相":
				numberStr = "16";
				break;
			case "保护器B相":
				numberStr = "17";
				break;
			case "保护器C相":
				numberStr = "18";
				break;
			default:
				numberStr = "未知";
				break;
		}

		return numberStr;
	}
	/**
	 * (1.进线柜，2.PT柜，3.出线柜，4.联络柜，5.提升柜)
	 * @param branchTypeName
	 * @return
	 */
		public static String getConvertBranchTypeNameStrToNumberStr(String branchTypeName) {
			String numberStr="";
			switch (branchTypeName.trim()) {
				case "进线柜":
					numberStr = "1";
					break;
				case "PT柜":
					numberStr = "2";
					break;
				case "出线柜":
					numberStr = "3";
					break;
				case "联络柜":
					numberStr = "4";
					break;
				case "提升柜":
					numberStr = "5";
					break;
				default:
					numberStr = "未知";
					break;
			}
			return numberStr;
		}

	//1.800,2.耐张线夹测温,3机电健康管理4充电柜
	public static String parseMonitorStationType(String type){
		String res;
		switch (type){
			case "1":
				res="800";
				break;
			case "2":
				res="温度传感器";
				break;
			case "3":
				res="机电健康管理";
				break;
			case "4":
				res="充电柜";
				break;
			default:
				res="其他";
		}
		return  res;
	}
}
