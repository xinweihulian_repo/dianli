package com.yscoco.dianli.service.comon;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: onion..
 * @Description: TODO
 * @Date: 2020/10/23 14:40
 */
public class CacheImpl<T> implements Serializable {


    public CacheItem<T> invok( String text,Class<T> clzz){
        CacheItem cacheItem = null;
        List<T> data =null;

            try{
                cacheItem = new CacheItem();
                data = new ArrayList<>();
                data  = JSON.parseArray(text,clzz);
            }catch (JSONException ex){
                // ex.printStackTrace();
                data.add(JSON.parseObject(text, clzz));
            }
            cacheItem.setData(data);

            return  cacheItem;
        }






}
