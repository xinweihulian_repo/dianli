package com.yscoco.dianli.service.server;

import java.util.*;

import cn.hutool.http.HttpUtil;
import com.batise.device.eric900.eric900_data;
import com.batise.device.eric900b.eric900b_data;
import com.batise.device.fault.fault_data_new;
import com.batise.device.motor.Motor_data;
import com.yscoco.dianli.common.utils.DateUtils;
import com.yscoco.dianli.common.utils.SpringUtils;
import com.yscoco.dianli.controller.wx.SubscribeBean;
import com.yscoco.dianli.controller.wx.SubscribeDataBean;
import com.yscoco.dianli.controller.wx.WXAPIConts;
import com.yscoco.dianli.controller.wx.WxApi;
import com.yscoco.dianli.dao.company.MonitorStationDao;
import com.yscoco.dianli.entity.company.MonitorStation;
import com.yscoco.dianli.entity.company.MonitorStationDevice;
import com.yscoco.dianli.entity.member.MemberInfo;
import com.yscoco.dianli.pojo.PointModel;
import com.yscoco.dianli.service.company.MonitorPointService;
import com.yscoco.dianli.service.company.MonitorStationDeviceService;
import com.yscoco.dianli.service.member.MemberService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSON;
import com.batise.device.temper.Temper420_data;
import com.batise.device.temper.Temper_data;
import com.google.gson.Gson;
import com.hyc.smart.DeviceServer;
import com.hyc.smart.IOnRecListener;
import com.hyc.smart.log.ExLogUtil;
import com.yscoco.dianli.controller.websocket.MsgScoketHandle;
import com.yscoco.dianli.service.company.ServerFaultService;


//@Slf4j
public  class startserver implements IOnRecListener{
	
	@Autowired
	ServerFaultService serverFaultService;

	public volatile static startserver instance;

	private static Object object = new Object();
	
	public startserver() {
	}
	public static startserver getInstance() {
		if (instance == null) {
			synchronized (object) {
				if (instance == null) {
					instance = new startserver();
				}
			}
		}
		return instance;
	}
	
	public void onStart()
	{
		DeviceServer ser = new DeviceServer();
		ser.onSetListener(this);
		Thread thread = new Thread(ser);
		thread.start();
	}
	
	@Override
	public void onRecive(byte[] arg0) {
		ExLogUtil.getLogger().info("data:"+Arrays.toString(arg0));
	}
	
	@Override
	public void onFault(String arg0, int arg1, int arg2, Object arg3) {
		fault_data_new fault= (fault_data_new)arg3;

		List<String> aliasList=new ArrayList<>();;

		List<MemberInfo> pushMembers   =  new ArrayList<>();

		List<PointModel> pointList = SpringUtils.getBean(MonitorPointService.class).getPointModelList();

		List<MemberInfo> memberModels = SpringUtils.getBean(MemberService.class).list();

		List<PointModel>  pointRes = new ArrayList<>();

        getMonitorStationDevice(pointList,new HashMap<>());

            String devid = fault.getDevid();

            pointList.forEach(p->{
                if(	p.getPointFlag().equals(devid)){
                    pointRes.add(p);
                }
            });

            if(pointRes==null||pointRes.size()<1){
                ExLogUtil.getLogger().info("devId: value not exist  in  'dianli' db  of table 't_monitor_point' ");
                return;
            }

            pointRes.forEach(p->{
                    memberModels.forEach(m->{
                    if(!StringUtils.isBlank(m.getMonitorStationId())){
                        List<String> monitorStationIds = Arrays.asList(m.getMonitorStationId().split(","));
                        monitorStationIds.forEach(k->{
                            if(p.getStationId().equals(k)){
                                pushMembers.add(m);
                            }
                        });
                    }
                });

            });

           List<MemberInfo> memberInfoList =  new ArrayList<>(new HashSet<>(pushMembers));

        jpushSend(fault, aliasList, pointRes, memberInfoList);

        wxSubscribeSend(memberInfoList,pointRes ,fault);
	}

    // 极光推送
    private void jpushSend(fault_data_new fault, List<String> aliasList, List<PointModel> pointRes, List<MemberInfo> memberInfoList) {
        try{
            Jpush jpush = new Jpush();

            memberInfoList.forEach(m->{
                aliasList.add(m.getId());
                ExLogUtil.getLogger().info("pushMember ID:"+ JSON.toJSON(m.getId()));
            });

            /**
             * autoId":1630742,"monitorStationName":"11","devid":"ERIC08001906B0001006","masterName":"14"
             * ,"masterAddress":"合肥市科技馆","phase":1,"timestamp":1576224961000,"times":"2019-12-13 16:16:01:001",
             * "faultType":10,"alarmLeve":"高级报警","distimestamp":0,"distimes":"1970-01-01 08:00:00:000",
             * "protectorType":"智能网关","type":"智能网关","deviceName":"14"}
             */
            Map<String, String> extras = new HashMap<>();
            extras.put("autoId",fault.getId()+"");
            extras.put("devid",fault.getDevid()+"");
            extras.put("phase",fault.getPhase()+"");
            extras.put("faultType",fault.getFault_type()+"");
            extras.put("distimestamp",fault.getDistimestamp()+"");
            extras.put("timestamp",fault.getTimestamp()+"");
            extras.put("times",fault.getTimes()+"");
            extras.put("monitorStationName",pointRes.get(0).getStationName()+"");
            extras.put("masterName",pointRes.get(0).getMasterName()+"");
            extras.put("masterAddress",pointRes.get(0).getStationAddress()+"");
            extras.put("deviceName",pointRes.get(0).getPointName()+"");
            extras.put("branchName",pointRes.get(0).getBranchName()+"");
            extras.put("position",pointRes.get(0).getPosition()+"");

            extras.put("protectorType","温度传感器");
            extras.put("type","温度传感器");
            extras.put("alarmLeve","高级报警");

//			extras.put("id", arg0.getId()+"");
//			extras.put("sensorId", arg0.getSensorId());
//			extras.put("pathBitmap", arg0.getPathBitmap());
//			extras.put("maxTemper", arg0.getMaxTemper()+"");
//			extras.put("maxTemperX", arg0.getMaxTemperX()+"");
//			extras.put("maxTemperY", arg0.getMaxTemperY()+"");
//			extras.put("minTemper", arg0.getMinTemper()+"");
//			extras.put("minTemperX", arg0.getMinTemperX()+"");
//			extras.put("minTemperY", arg0.getMinTemperY()+"");
//			extras.put("averageTemper", arg0.getAverageTemper()+"");
//			extras.put("timestamp", arg0.getTimestamp()+"");

            jpush.sendAlias("您有新的提醒", extras, aliasList);
        }catch(Exception ex){
            ExLogUtil.getLogger().info("推送失败..............."+ex.getMessage());
        }
    }

    @Override
	public void onReciveInfrared(Temper420_data arg0) {
		System.out.println("Temper420_data:"+ new Gson().toJson(arg0) );
		pushTemper420DataInfo(arg0);
		
	}
	
	  
	  private void pushTemper420DataInfo(Temper420_data arg0) {
			if(arg0!=null){
				try {
                   ExLogUtil.getLogger().info("websocket推送arg3:::"+new Gson().toJson(arg0));

				   MsgScoketHandle.sendMessage(arg0.getSensorId(), JSON.toJSONString(arg0));
				} catch (Exception e) {
					ExLogUtil.getLogger().info("websocket推送异常:::"+e.getMessage());
				}

				try{
					Jpush jpush = new Jpush();

					List<String> aliasList = new ArrayList<>();
						aliasList.add(arg0.getSensorId());
						
					Map<String, String> extras = new HashMap<>();
						extras.put("id", arg0.getId()+"");
					    extras.put("sensorId", arg0.getSensorId());
					    extras.put("pathBitmap", arg0.getPathBitmap());
					    extras.put("maxTemper", arg0.getMaxTemper()+"");
					    extras.put("maxTemperX", arg0.getMaxTemperX()+"");
					    extras.put("maxTemperY", arg0.getMaxTemperY()+"");
					    extras.put("minTemper", arg0.getMinTemper()+"");
					    extras.put("minTemperX", arg0.getMinTemperX()+"");
					    extras.put("minTemperY", arg0.getMinTemperY()+"");
					    extras.put("averageTemper", arg0.getAverageTemper()+"");
					    extras.put("timestamp", arg0.getTimestamp()+"");
					    
					jpush.sendAlias("报警提醒", extras, aliasList);
					
				}catch(Exception ex){
					ExLogUtil.getLogger().info("推送失败.......Jpush........."+ex.getMessage());
				}
			}
		}

    /**
     *
     * @param dev_type 设备类型 1：温度传感器 Temper_data；2:电机 eric900_data 3:900B eric900b_data 4:电机传感器 Motor_data
     * @param object 对应的对象
     */
	@Override
	public void UpdateData(int dev_type,Object object) {
	    Map <String,Object> puhsMap = new HashMap<>(3);

        Temper_data temper_data = null;

        eric900b_data eric900b = null;

        Motor_data motor_data =null;

        eric900_data eric900_datas =null;

        /**
         * 1.对应耐张夹监测站和温度传感器；2.对应机电健康管理类型监测站，3.对应充电柜类型监测站，4.对应电机传感器；
         */
	    switch (dev_type){
            case 1:
                if(object instanceof Temper_data){
                    temper_data =  (Temper_data)object;
                    puhsMap.put("data",temper_data);
                    puhsMap.put("dev_type",dev_type);
                    MsgScoketHandle.sendMessage(temper_data.getSensorId(), JSON.toJSONString(puhsMap));
                    ExLogUtil.getLogger().info("Temper_data:"+JSON.toJSONString(puhsMap));
                }
                break;
            case 2:
                if(object instanceof eric900_data){
                    eric900_datas =  (eric900_data)object;
                    puhsMap.put("data",eric900_datas);
                    puhsMap.put("dev_type",dev_type);
                    MsgScoketHandle.sendMessage(eric900_datas.getDevid(), JSON.toJSONString(puhsMap));
                    ExLogUtil.getLogger().info("eric900_data:"+JSON.toJSONString(puhsMap));
                }

                break;
            case 3:
                if(object instanceof eric900b_data){
                    eric900b =  (eric900b_data)object;
                    puhsMap.put("data",eric900b);
                    puhsMap.put("dev_type",dev_type);
                    MsgScoketHandle.sendMessage(eric900b.getDevid(), JSON.toJSONString(puhsMap));
                    ExLogUtil.getLogger().info("eric900b_data:"+JSON.toJSONString(puhsMap));
                }
                break;
            case 4:
                if(object instanceof Motor_data){
                    motor_data =  (Motor_data)object;
                    puhsMap.put("data",motor_data);
                    puhsMap.put("dev_type",dev_type);
                    MsgScoketHandle.sendMessage(motor_data.getSensorId(), JSON.toJSONString(puhsMap));
                    ExLogUtil.getLogger().info("Motor_data:"+JSON.toJSONString(object));
                }
                break;
                default:
                    ExLogUtil.getLogger().warning("数据解析异常: "+JSON.toJSONString(puhsMap));
        }
	}

  //微信订阅消息
	private void wxSubscribeSend( List<MemberInfo> memberInfoList, List<PointModel> pointRes,fault_data_new fault){

        String url = WXAPIConts.SEND_URL.replace("ACCESS_TOKEN",  SpringUtils.getBean(WxApi.class).getAccessToken());

		String time = DateUtils.unixTimestampToDate_data(fault.getTimestamp());

		SubscribeDataBean bean = new SubscribeDataBean();
			bean.setDate1(new SubscribeDataBean.Date1(null==time?"1970-01-01 23:00:00":time));
			bean.setThing4(new SubscribeDataBean.Thing4(null==pointRes.get(0).getPosition()?"未知":pointRes.get(0).getPosition()));
			bean.setThing5(new SubscribeDataBean.Thing5("高级报警"));
			bean.setThing6(new SubscribeDataBean.Thing6(null==pointRes.get(0).getPointName()?"未知":pointRes.get(0).getPointName()));
			bean.setNumber2(new SubscribeDataBean.Number2(null==fault.getDevid()?"0000":fault.getDevid()));

		//组装接口所需对象
		SubscribeBean sendBean = new SubscribeBean();
		sendBean.setData(bean);//这里的订阅消息对象 不需要额外转json

		sendBean.setTemplate_id(WXAPIConts.TEMPLATE_ID);
		sendBean.setPage("pages/police/police");
		//sendBean.setMiniprogram_state("developer");

		for (int i = 0; i < memberInfoList.size(); i++) {
			if(StringUtils.isEmpty(memberInfoList.get(i).getOpenId()))
              	continue;

			sendBean.setTouser(memberInfoList.get(i).getOpenId());

			String result = HttpUtil.post(url, JSON.toJSONString(sendBean));

			ExLogUtil.getLogger().info("wx订阅消息:"+result);
		}

	}


    private void getMonitorStationDevice(List<PointModel> queryAllPointFlag,Map<String, Object> params) {
        List<MonitorStation> list =  SpringUtils.getBean(MonitorStationDao.class).list(params);

        List<String> stationIdList = new ArrayList<>();

        list.forEach(m->{
            if(!m.getStationType().equals("1")){
            	stationIdList.add(m.getId());
            }
        });

        if(stationIdList.isEmpty()){return;}

        stationIdList.forEach(m->{
            PointModel model = null;
            List<MonitorStationDevice> byStationId = SpringUtils.getBean(MonitorStationDeviceService.class).getByStationId(m);
            for (int i = 0; i < byStationId.size(); i++) {
                model = new PointModel();
                model.setPointFlag( byStationId.get(i).getDeviceId());
                model.setPointName(byStationId.get(i).getDeviceName());
                model.setPosition(byStationId.get(i).getPosition());
                model.setType(byStationId.get(i).getDeviceType());
                model.setStationId(byStationId.get(i).getStationId());
                model.setId(byStationId.get(i).getId());
                model.setStationType(byStationId.get(i).getStationType());
                model.setMasterName(" ");
                model.setBranchName(" ");
                model.setCabin_no(" ");
                model.setMasterId(" ");
                model.setHitchType(parseType(byStationId.get(i).getStationType()));
                model.setMasterFlag(" ");
                model.setBranchId(" ");

                queryAllPointFlag.add(model);
            }
        });

        queryAllPointFlag.forEach(m->{
            list.forEach(re->{
                if(m.getStationId().equals(re.getId())){
                    m.setStationName(re.getStationName());
                    m.setStationAddress(re.getStationAddress());
                    m.setCompanyId(re.getCompany().getId());
                    m.setCompanyName(re.getCompany().getCompanyName());
                }
            });
        });
    }

    //1.800,2.耐张线夹测温,3机电健康管理4充电柜
    public String parseType(String type){
        String res;
        switch (type){
            case "1":
                res="800";
                break;
            case "2":
                res="温度传感器";
                break;
            case "3":
                res="机电健康管理";
                break;
            case "4":
                res="充电柜";
                break;
            default:
                res="其他";
        }
        return  res;
    }

	public static void main(String[] args) {
		startserver ss  = new startserver();
		fault_data_new fault = new fault_data_new();
		fault.setDevid("C902428");
		ss.onFault("1",1,1,fault);
	}
	  	
}
