package com.yscoco.dianli.service.server;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import cn.jpush.api.JPushClient;
import cn.jpush.api.common.ClientConfig;
import cn.jpush.api.common.resp.APIConnectionException;
import cn.jpush.api.common.resp.APIRequestException;
import cn.jpush.api.push.PushResult;
import cn.jpush.api.push.model.Message;
import cn.jpush.api.push.model.Options;
import cn.jpush.api.push.model.Platform;
import cn.jpush.api.push.model.PushPayload;
import cn.jpush.api.push.model.audience.Audience;
import cn.jpush.api.push.model.notification.AndroidNotification;
import cn.jpush.api.push.model.notification.IosNotification;
import cn.jpush.api.push.model.notification.Notification;

/**
 * setApnsProduction(false) 为IOS开发环境
 * setApnsProduction(true) 为IOS生产环境
 * @author Mate
 *
 */
@Service
public class Jpush {
	public static final String masterSecret = "1c6af6cfc69b50d1c09c869d";
	public static final String Appkey = "db895f353a518995f3dd843a";
	/**
	 * 给所有平台的所有用户发通知
	 */
	public void sendAllsetNotification(String message) {
		JPushClient jpushClient = new JPushClient(masterSecret, Appkey);// 第一个参数是masterSecret
																		// 第二个是appKey
		Map<String, String> extras = new HashMap<String, String>();
		// 添加附加信息
		extras.put("extMessage", "我是额外的通知");
		PushPayload payload = buildPushObject_all_alias_alert(message, extras);
		try {
			PushResult result = jpushClient.sendPush(payload);
			System.out.println(result);
		} catch (APIConnectionException e) {
			System.out.println(e);
		} catch (APIRequestException e) {
			System.out.println(e);
			System.out.println("Error response from JPush server. Should review and fix it. " + e);
			System.out.println("HTTP Status: " + e.getStatus());
			System.out.println("Error Code: " + e.getErrorCode());
			System.out.println("Error Message: " + e.getErrorMessage());
			System.out.println("Msg ID: " + e.getMsgId());
		}
	}
	  
	/**
	 * 给所有平台的所有用户发消息
	 * 
	 * @param message
	 */
	public void sendAllMessage(String message) {
		ClientConfig clientConfig = ClientConfig.getInstance();
		JPushClient jpushClient = new JPushClient(masterSecret, Appkey, null, clientConfig);
		Map<String, String> extras = new HashMap<String, String>();
		// 添加附加信息
		extras.put("extMessage", "我是额外透传的消息");
		PushPayload payload = buildPushObject_all_alias_Message(message, extras);
		try {
			PushResult result = jpushClient.sendPush(payload);
			System.out.println(result);
		} catch (APIConnectionException e) {
			System.out.println(e);
		} catch (APIRequestException e) {
			System.out.println(e);
			System.out.println("Error response from JPush server. Should review and fix it. " + e);
			System.out.println("HTTP Status: " + e.getStatus());
			System.out.println("Error Code: " + e.getErrorCode());
			System.out.println("Error Message: " + e.getErrorMessage());
			System.out.println("Msg ID: " + e.getMsgId());
		}
	}

	/**
	 * 发送通知
	 * 
	 * @param message
	 * @param extras
	 * @return
	 */
	private PushPayload buildPushObject_all_alias_alert(String message, Map<String, String> extras) {
										// 设置平台
		return PushPayload.newBuilder().setPlatform(Platform.all())
				// 按什么发送 tag alia
				.setAudience(Audience.all())
				.setNotification(Notification.newBuilder().setAlert(message)
						.addPlatformNotification(AndroidNotification.newBuilder().addExtras(extras).build())
						.addPlatformNotification(IosNotification.newBuilder().addExtras(extras).build()).build())
				// 发送消息
				.setOptions(Options.newBuilder().setApnsProduction(false).build()).build();
		// 设置ios平台环境 True 表示推送生产环境，False 表示要推送开发环境 默认是开发
	}

	/**
	 * 发送透传消息
	 * 
	 * @param message
	 * @param extras
	 * @return
	 * @date 2017年1月13日
	 */
	private PushPayload buildPushObject_all_alias_Message(String message, Map<String, String> extras) {
						// 设置平台
		return PushPayload.newBuilder().setPlatform(Platform.all())
				// 按什么发送 tag alia
				.setAudience(Audience.all())
				.setMessage(Message.newBuilder().setMsgContent(message).addExtras(extras).build())
				// 发送通知
				.setOptions(Options.newBuilder().setApnsProduction(false).build()).build();
		// 设置ios平台环境 True 表示推送生产环境，False 表示要推送开发环境 默认是开发
	}

	/**
	 * 客户端 给所有平台的一个或者一组用户发送信息
	 */
	public void sendAlias(String message,Map<String, String> extras, List<String> aliasList) {
		JPushClient jpushClient = new JPushClient(masterSecret, Appkey);
//		Map<String, String> extras = new HashMap<String, String>();
//		// 添加附加信息
//		extras.put("extMessage", "我是额外的消息--sendAlias");
                
		PushPayload payload = allPlatformAndAlias(message, extras, aliasList);
		try {
			PushResult result = jpushClient.sendPush(payload);
			System.out.println(result);
		} catch (APIConnectionException e) {
			System.out.println(e);
		} catch (APIRequestException e) {
			System.out.println(e);
			System.out.println("Error response from JPush server. Should review and fix it. " + e);
			System.out.println("HTTP Status: " + e.getStatus());
			System.out.println("Error Code: " + e.getErrorCode());
			System.out.println("Error Message: " + e.getErrorMessage());
			System.out.println("Msg ID: " + e.getMsgId());
		}
	}

	/**
	 * 极光推送：生成向一个或者一组用户发送的消息。
	 */
	private PushPayload allPlatformAndAlias(String alert, Map<String, String> extras, List<String> aliasList) {
		return PushPayload.newBuilder().setPlatform(Platform.all()).setAudience(Audience.alias(aliasList))
				.setNotification(Notification.newBuilder().setAlert(alert)
						.addPlatformNotification(AndroidNotification.newBuilder().addExtras(extras).build())
						.addPlatformNotification(IosNotification.newBuilder().addExtras(extras).build()).build())
				.setOptions(Options.newBuilder().setApnsProduction(false).build()).build();
	}

	/**
	 * 客户端 给平台的一个或者一组标签发送消息。
	 */
	public void sendTag(String message, String messageId, String type, List<String> tagsList) {
		JPushClient jpushClient = new JPushClient(masterSecret, Appkey);
		// 附加字段
		Map<String, String> extras = new HashMap<String, String>();
		extras.put("messageId", messageId);
		extras.put("typeId", type);
		 
		PushPayload payload = allPlatformAndTag(message, extras, tagsList);
		try {
			PushResult result = jpushClient.sendPush(payload);
			System.out.println(result);
		} catch (APIConnectionException e) {
			System.out.println(e);
		} catch (APIRequestException e) {
			System.out.println(e);
			System.out.println("Error response from JPush server. Should review and fix it. " + e);
			System.out.println("HTTP Status: " + e.getStatus());
			System.out.println("Error Code: " + e.getErrorCode());
			System.out.println("Error Message: " + e.getErrorMessage());
			System.out.println("Msg ID: " + e.getMsgId());
		}
	}

	/**
	 * 极光推送：生成向一组标签进行推送的消息。
	 */
	private PushPayload allPlatformAndTag(String alert, Map<String, String> extras, List<String> tagsList) {

		return PushPayload.newBuilder().setPlatform(Platform.android_ios()).setAudience(Audience.tag(tagsList))
				.setNotification(Notification.newBuilder().setAlert(alert)
						.addPlatformNotification(AndroidNotification.newBuilder().addExtras(extras).build())
						.addPlatformNotification(IosNotification.newBuilder().addExtras(extras).build()).build())
				.setOptions(Options.newBuilder().setApnsProduction(false).build()).build();
	}
	
	public static void main(String[] args) {

		/*new Jpush().sendAllMessage("这是后台发送的透传消息");*/
		Jpush jpush = new Jpush();
/*		List<String> list = new ArrayList<>();
		list.add("1000000007");
		jpush.sendAlias("This,is....test", list);
		*/
		
		Map<String, String> map = new HashMap<String, String>();
		map.put("devCode", "1234");
		map.put("id", "0000");
		map.put("type", "6");
		map.put("address", "深圳");
		map.put("pname", "温度传感器");
		
		List<String> aliasList = new ArrayList<>();
		 aliasList.add("1000000007");
		 jpush.sendAlias("测试推送",map,aliasList);

	}
		

}
