package com.yscoco.dianli.service.msg;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yscoco.dianli.common.base.Base;
import com.yscoco.dianli.common.handler.PageData;
import com.yscoco.dianli.constants.AppLogType;
import com.yscoco.dianli.constants.YN;
import com.yscoco.dianli.dao.app.AppLogDao;
import com.yscoco.dianli.dao.msg.MsgPushDao;
import com.yscoco.dianli.dto.msg.MsgPushDto;
import com.yscoco.dianli.entity.msg.MsgPush;
import com.yscoco.dianli.other.push.PushClientI;
@Service
public class MsgPushService extends Base {

	@Autowired
	private PushClientI pushClient;
	
	@Autowired
	private MsgPushDao msgPushDao;
	
	@Autowired
	private AppLogDao appLogDao;

	public void delete(String idsStr,String createBy, String managerId, String addressIp) {
	    String[] ids = idsStr.split(",");
	    for (String id : ids) {
	        MsgPush push = msgPushDao.getEntity(id);
	        // 记录日志
		    appLogDao.recordActionOnMsgPush(addressIp, managerId,AppLogType.DELETE,push);
	        msgPushDao.delete(push);
        }
	  
	}
	
	public void view(String id,String createBy, String managerId, String addressIp) {
	    MsgPush push = msgPushDao.getEntity(id);
	    // 记录日志
	    appLogDao.recordActionOnMsgPush(addressIp, managerId,AppLogType.VIEW,push);
        push.setIsRead(YN.Y);
	}
	
	public PageData<MsgPushDto> listPage(String msgId,String loginId,Integer page, Integer rows){
	 	page = (page == null)?0:page;
		rows = (rows == null)?10:rows;
		Map<String,Object> params = new HashMap<>();
		if(StringUtils.isNotEmpty(msgId)){
			params.put("id", msgId);
		}
		if(StringUtils.isNotEmpty(loginId)){
			params.put("createBy", loginId);
		}
		
		params.put("page", page);
		params.put("rows", rows);
		params.put("order", "createTime desc");	
		PageData<MsgPushDto> data = new PageData<MsgPushDto>();
		data.setList(msgPushDao.listToDto(params,MsgPushDto.class));
		data.setCount(msgPushDao.count(params));
		return data;
	}
	
}
