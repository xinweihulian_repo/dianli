package com.yscoco.dianli.service.company;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.batise.device.device_info;
import com.batise.device.temper.Temper_data;
import com.batise.device.voltage.Voltage_data;
import com.hyc.smart.XWDataEntry;
import com.yscoco.dianli.common.base.Base;
import com.yscoco.dianli.common.exception.BizException;
import com.yscoco.dianli.common.vo.Code;
import com.yscoco.dianli.common.vo.Message;
import com.yscoco.dianli.common.vo.PageData;
import com.yscoco.dianli.constants.AppLogType;
import com.yscoco.dianli.dao.app.AppLogDao;
import com.yscoco.dianli.dao.company.CompanyDao;
import com.yscoco.dianli.dao.company.LinesOnBranchDao;
import com.yscoco.dianli.dao.company.LinesOnMasterDao;
import com.yscoco.dianli.dao.company.MonitorPointDao;
import com.yscoco.dianli.dao.company.MonitorStationDao;
import com.yscoco.dianli.dao.member.MemberInfoDao;
import com.yscoco.dianli.dao.sys.SysUserDao;
import com.yscoco.dianli.dto.company.CompanyDto;
import com.yscoco.dianli.dto.company.LinesOnBranchDto;
import com.yscoco.dianli.dto.company.LinesOnMasterDto;
import com.yscoco.dianli.dto.company.MonitorPointDto;
import com.yscoco.dianli.dto.company.MonitorStationDto;
import com.yscoco.dianli.entity.company.Company;
import com.yscoco.dianli.entity.company.LinesOnBranch;
import com.yscoco.dianli.entity.company.LinesOnMaster;
import com.yscoco.dianli.entity.company.MonitorPoint;
import com.yscoco.dianli.entity.company.MonitorStation;
import com.yscoco.dianli.entity.member.MemberInfo;
import com.yscoco.dianli.entity.sys.SysUser;

@Service
public class MonitorStationService extends Base {

	//private static Logger logger = Logger.getLogger(MonitorStationService.class);

	@Autowired
	private MonitorStationDao monitorStationDao;

	@Autowired
	private CompanyDao companyDao;

	@Autowired
	private MonitorPointDao monitorPointDao;

	@Autowired
	private LinesOnBranchDao linesOnBranchDao;

	@Autowired
	private LinesOnMasterDao linesOnMasterDao;
	@Autowired
	private MemberInfoDao memberInfoDao;

	@Autowired
	private AppLogDao appLogDao;
	
	@Autowired
	private MonitorStationDeviceService  monitorStationDeviceService;
	

	@Autowired
	private SysUserDao sysUserDao;

	private static boolean acceptMaster = false;
	private static boolean acceptBranch = false;
	private static boolean acceptPoint = false;

	public PageData<MonitorStationDto> listPage(String companyId, String monitorStationId, String stationAddress,
			String stationName, Integer page, Integer rows,String flag, String createBy, String annotation, String city) {
		page = (page == null) ? 0 : page;
		rows = (rows == null) ? 10 : rows;
		flag = (flag == null) ? "true" : flag;
		Map<String, Object> params = new HashMap<>();
		List<MonitorStation>  monitorStationList =new ArrayList<>();
		boolean memberLoginData =false;
		if (StringUtils.isNotEmpty(monitorStationId)) {
			params.put("id", monitorStationId);
		}
		if (StringUtils.isNotEmpty(stationAddress)) {
			params.put("stationAddressLike", stationAddress);
		}
		if (StringUtils.isNotEmpty(stationName)) {
			params.put("stationNameLike", stationName);
		}
		if (StringUtils.isNotBlank(city)) {
			params.put("cityLike", city);
		//	System.out.println("city///////----------------:"+city);
		}
		if (StringUtils.isNotEmpty(companyId)) {
			Company company = this.companyDao.getEntity(companyId);
			params.put("company", company);
		}
		if (StringUtils.isNotEmpty(createBy)) { // 除了admin超级管理员外 管理员只能看到自己创建的公司
			SysUser SysUser_create = this.sysUserDao.queryOne("id", createBy);
			if (SysUser_create != null && notEmpty(SysUser_create.getPermission())
					&& !SysUser_create.getPermission().equalsIgnoreCase("admin")) {
				params.put("createBy", createBy);
			}
			//==================================================================
			MemberInfo memberInfo = this.memberInfoDao.queryOne("id", createBy); 
			if (memberInfo != null) {
				memberLoginData= true;
				String monitorStationIds = memberInfo.getMonitorStationId();
				String[] monitorStationIdArr = monitorStationIds.split(",");
				monitorStationList = new ArrayList<MonitorStation>();
				for (String str : monitorStationIdArr) {
					 MonitorStation queryOne = this.monitorStationDao.queryOne("id",str);
					 monitorStationList.add(queryOne);
				}
		}
	}
		params.put("page", page);
		params.put("rows", rows);
		params.put("order", "t.createTime desc");
		PageData<MonitorStationDto> data = new PageData<MonitorStationDto>();

		List<MonitorStationDto> monitorStationDtoList = new ArrayList<MonitorStationDto>();// 用来装东西的
		MonitorStationDto monitorStationDto = null;// 站点

		List<LinesOnMasterDto> linesOnMasterDtoList = null;// 母线
		LinesOnMasterDto linesOnMasterDto = null;

		List<LinesOnBranchDto> linesOnBranchDtoList = null;// 支线
		LinesOnBranchDto linesOnBranchDto = null;

		List<MonitorPointDto> monitorPointDtoList = null;// 监测点
		MonitorPointDto monitorPointDto = null;

		List<LinesOnMaster> linesOnMasterList = null;
		List<LinesOnMasterDto> linesOnMasterList_result = null;
		List<MonitorStation> queryMonitorStationList = null;
		
		
		acceptFlag(flag);
		// queryMonitorStationList = monitorStationDao.listPage(params);// 求出bean结果集
		 
		 if(memberLoginData){  //如果是巡检员登陆监测站数据
			 queryMonitorStationList=monitorStationList;
		 }else{
			 queryMonitorStationList = monitorStationDao.listPage(params);// 求出bean结果集
		 }
		annotation = annotation == null ? "500" : annotation;
		if (annotation.equals("MonitorStation")) {
			for (MonitorStation monitorStation : queryMonitorStationList) {
               int point=0,branch=0;
				monitorStationDto = this.monitorStationDao.toDto(monitorStation, MonitorStationDto.class);// 提前转化Dto方便装各种线路种类
				List<LinesOnMaster> linesOnMasters = null;

				if (acceptMaster) {
					linesOnMasterDtoList = new ArrayList<LinesOnMasterDto>();// 母线初始化
					linesOnMasters = monitorStation.getLinesOnMasters(); // 第三步：获取母线并装进母线Set中
					for (LinesOnMaster linesOnMaster : linesOnMasters) {
						// 在这里设置障碍：避免数据量多大，如果需要查询母线和支线下面的数据就传true,否则传false,
						linesOnMasterDto = this.linesOnMasterDao.toDto(linesOnMaster, LinesOnMasterDto.class);
							
						if (acceptBranch) {
							linesOnBranchDtoList = new ArrayList<LinesOnBranchDto>();// 支线初始化
							List<LinesOnBranch> linesOnBranchs = linesOnMaster.getLinesOnBranchs(); // 第二步：获取支线并装进支线Set中
							branch+=linesOnBranchs.size();
							for (LinesOnBranch linesOnBranch : linesOnBranchs) {
								if (acceptPoint) {
									monitorPointDtoList = new ArrayList<MonitorPointDto>();// 监测点初始化
									List<MonitorPoint> monitorPoints = linesOnBranch.getMonitorPoints(); // 第一步：现获取站点并装进站点Set中
									point+=monitorPoints.size();
									for (MonitorPoint monitorPoint : monitorPoints) {
										monitorPointDto = this.monitorPointDao.toDto(monitorPoint,MonitorPointDto.class);
										monitorPointDtoList.add(monitorPointDto);
									}
								}
								monitorStationDto.setMonitorPointCount(point);// 注值监测点数量
								
								if(monitorStation.getStationType().equals("2")||monitorStation.getStationType().equals("3")||monitorStation.getStationType().equals("4")){
									monitorStationDto.setMonitorPointCount(monitorStationDeviceService.getByStationId(monitorStationDto.getId()).size());
								}
								
								linesOnBranchDto = this.linesOnBranchDao.toDto(linesOnBranch, LinesOnBranchDto.class);
								linesOnBranchDto.setMonitorPointDtos(monitorPointDtoList);
								linesOnBranchDtoList.add(linesOnBranchDto);
								monitorStationDto.setLinesOnBranchCount(branch);// 注值支线数量
								
							}
							linesOnMasterDto.setLinesOnBranchDtos(linesOnBranchDtoList);
						}

						linesOnMasterDtoList.add(linesOnMasterDto);
						monitorStationDto.setLinesOnMasterCount(linesOnMasterDtoList.size());// 注值母线数量
					}

					monitorStationDto.setLinesOnMasterDtos(linesOnMasterDtoList);// 获取母线
				}

				LinesOnMaster nextMaster_ = null;
				// 为排序做处理
				linesOnMasterList = new ArrayList<LinesOnMaster>();// 母线排序
				if (notEmpty(flag) && flag.equalsIgnoreCase("three")) {

					for (LinesOnMaster master_old : linesOnMasters) {
						if (master_old != null && notEmpty(master_old.getPreMasterId())) {
							continue;
						} else if (master_old != null && empty(master_old.getPreMasterId())) {
							linesOnMasterList.add(master_old);// 先将首部装进去
							nextMaster_ = this.linesOnMasterDao.getEntity(master_old.getNextMasterId());
							if (nextMaster_ != null) {
								linesOnMasterList.add(nextMaster_);// 装首部的下一段母线
							}
							while (nextMaster_ != null && notEmpty(nextMaster_.getNextMasterId())) { // 下一段母线不为空并且下一段母线一直有下一段母线id,
								nextMaster_ = this.linesOnMasterDao.getEntity(nextMaster_.getNextMasterId());
								if (nextMaster_ != null) {
									linesOnMasterList.add(nextMaster_);// 这个地方仅取首部
								}
							}
							if (linesOnMasterList.size() == linesOnMasters.size()) {
								break;
							}
						}
					}

					// 给所有参加排序的母线和非排序母线指定支线
					int totalLines = 0;// 线路总数
					List<String> lineOnMasterMonitorFlagList = new ArrayList<String>();
					linesOnMasterList_result = new ArrayList<LinesOnMasterDto>();// 母线初始化
					totalLines = totalLines + linesOnMasterList.size();// 线路母线总数
					for (LinesOnMaster linesOnMaster_new : linesOnMasterList) {
						lineOnMasterMonitorFlagList.add(linesOnMaster_new.getMonitorFlag());// 获取母线标识：即
																							// MonitorFlag
						linesOnMasterDto = new LinesOnMasterDto();

						linesOnBranchDtoList = new ArrayList<LinesOnBranchDto>();// 支线初始化
						List<LinesOnBranch> linesOnBranchs = linesOnMaster_new.getLinesOnBranchs(); // 第二步：获取支线并装进支线Set中
						totalLines = totalLines + linesOnBranchs.size();// 线路支线总数

						linesOnMasterList_result.add(linesOnMasterDto);
					}
					monitorStationDto.setLinesOnMasterSortList(linesOnMasterList_result);

					monitorStationDto.setTotalLines(totalLines); // 线路总数

				}
				
				//System.err.print("MonitorStation:"+JSON.toJSONString(monitorStation));
				if(monitorStation.getStationType().equals("2")||monitorStation.getStationType().equals("3")||monitorStation.getStationType().equals("4")){
					monitorStationDto.setMonitorPointCount(monitorStationDeviceService.getByStationId(monitorStationDto.getId()).size());
				}
				//System.err.print("monitorStationDtoCount:"+monitorStationDto.getMonitorPointCount());
				monitorStationDtoList.add(monitorStationDto);// 获取监测站
			}
			/**
			 * -----------------------------------------------------------------
			 * -------------------------------------------------
			 * -----------------------------------------------------------------
			 * -------------------------------------------------
			 */
		} else {
			for (MonitorStation monitorStation : queryMonitorStationList) {
				if(monitorStation!=null){
					monitorStationDto = this.monitorStationDao.toDto(monitorStation, MonitorStationDto.class);// 提前转化Dto方便装各种线路种类
				}
				List<LinesOnMaster> linesOnMasters = null;

				if (acceptMaster) {
					linesOnMasterDtoList = new ArrayList<LinesOnMasterDto>();// 母线初始化
					linesOnMasters = monitorStation.getLinesOnMasters(); // 第三步：获取母线并装进母线Set中
					for (LinesOnMaster linesOnMaster : linesOnMasters) {
						// 在这里设置障碍：避免数据量多大，如果需要查询母线和支线下面的数据就传true,否则传false,
						linesOnMasterDto = this.linesOnMasterDao.toDto(linesOnMaster, LinesOnMasterDto.class);

						if (acceptBranch) {
							linesOnBranchDtoList = new ArrayList<LinesOnBranchDto>();// 支线初始化
							List<LinesOnBranch> linesOnBranchs = linesOnMaster.getLinesOnBranchs(); // 第二步：获取支线并装进支线Set中

							for (LinesOnBranch linesOnBranch : linesOnBranchs) {

								if (acceptPoint) {
									monitorPointDtoList = new ArrayList<MonitorPointDto>();// 监测点初始化
									// .data.list["0"].linesOnMasterSortList["0"].linesOnBranchDtos[1].monitorPointDtos;
									List<MonitorPoint> monitorPoints = linesOnBranch.getMonitorPoints(); // 第一步：现获取站点并装进站点Set中
									for (MonitorPoint monitorPoint : monitorPoints) {
										monitorPointDto = this.monitorPointDao.toDto(monitorPoint,MonitorPointDto.class);
										monitorPointDtoList.add(monitorPointDto);
										monitorStationDto.setMonitorPointCount(monitorPointDtoList.size());// 注值监测点数量
										
									}
								}

								linesOnBranchDto = this.linesOnBranchDao.toDto(linesOnBranch, LinesOnBranchDto.class);
								linesOnBranchDto.setMonitorPointDtos(monitorPointDtoList);
								linesOnBranchDtoList.add(linesOnBranchDto);
								monitorStationDto.setLinesOnBranchCount(linesOnBranchDtoList.size());// 注值支线数量
							}
							linesOnMasterDto.setLinesOnBranchDtos(linesOnBranchDtoList);
						}
						if(monitorStation.getStationType().equals("2")||monitorStation.getStationType().equals("3")||monitorStation.getStationType().equals("4")){
							monitorStationDto.setMonitorPointCount(monitorStationDeviceService.getByStationId(monitorStationDto.getId()).size());
						}
						linesOnMasterDtoList.add(linesOnMasterDto);
						monitorStationDto.setLinesOnMasterCount(linesOnMasterDtoList.size());// 注值母线数量
					}

					monitorStationDto.setLinesOnMasterDtos(linesOnMasterDtoList);// 获取母线
				}

				LinesOnMaster nextMaster_ = null;
				// 为排序做处理
				linesOnMasterList = new ArrayList<LinesOnMaster>();// 母线排序
				if (notEmpty(flag) && flag.equalsIgnoreCase("three")) {

					for (LinesOnMaster master_old : linesOnMasters) {
						if (master_old != null && notEmpty(master_old.getPreMasterId())) {
							continue;
						} else if (master_old != null && empty(master_old.getPreMasterId())) {
							linesOnMasterList.add(master_old);// 先将首部装进去
							nextMaster_ = this.linesOnMasterDao.getEntity(master_old.getNextMasterId());
							if (nextMaster_ != null) {
								linesOnMasterList.add(nextMaster_);// 装首部的下一段母线
							}
							while (nextMaster_ != null && notEmpty(nextMaster_.getNextMasterId())) { // 下一段母线不为空并且下一段母线一直有下一段母线id,
								nextMaster_ = this.linesOnMasterDao.getEntity(nextMaster_.getNextMasterId());
								if (nextMaster_ != null) {
									linesOnMasterList.add(nextMaster_);// 这个地方仅取首部
								}
							}
							if (linesOnMasterList.size() == linesOnMasters.size()) {
								break;
							}
						}
					}

					// 给所有参加排序的母线和非排序母线指定支线
					int totalLines = 0;// 线路总数
					List<String> lineOnMasterMonitorFlagList = new ArrayList<String>();
					linesOnMasterList_result = new ArrayList<LinesOnMasterDto>();// 母线初始化
					totalLines = totalLines + linesOnMasterList.size();// 线路母线总数
					List<String> devidsList = null;
					for (LinesOnMaster linesOnMaster_new : linesOnMasterList) {
						lineOnMasterMonitorFlagList.add(linesOnMaster_new.getMonitorFlag());// 获取母线标识：即
																							// MonitorFlag
						linesOnMasterDto = new LinesOnMasterDto();
						linesOnBranchDtoList = new ArrayList<LinesOnBranchDto>();// 支线初始化
						List<LinesOnBranch> linesOnBranchs = linesOnMaster_new.getLinesOnBranchs(); // 第二步：获取支线并装进支线Set中
						totalLines = totalLines + linesOnBranchs.size();// 线路支线总数
						for (LinesOnBranch linesOnBranch : linesOnBranchs) {

							monitorPointDtoList = new ArrayList<MonitorPointDto>();// 监测点初始化
							List<MonitorPoint> monitorPoints = linesOnBranch.getMonitorPoints(); // 第一步：现获取站点并装进站点Set中
							for (MonitorPoint monitorPoint : monitorPoints) {
								monitorPointDto = new MonitorPointDto();
								monitorPointDto.setId(monitorPoint.getId());
								monitorPointDto.setPointName(monitorPoint.getPointName());
								monitorPointDto.setMonitorFlag(monitorPoint.getMonitorFlag());
								monitorPointDto.setPositionName(monitorPoint.getPosition());
								// System.out.println("monitorPoint"+monitorPoint.getMonitorFlag());
								List<String> list = new ArrayList<>();
								String monitorFlag = monitorPoint.getMonitorFlag();
								; // 监测设备标识
								list.add(monitorFlag);
								if (monitorPoint.getHitchType().equals("温度采集器")) {
									//System.out.println(monitorFlag + ":获取温度采集器最新的数据");
									list.add(monitorFlag);
									List<Temper_data> lastTemperData = XWDataEntry.getLastTemperData(list);// 获取温度采集器最新的数据
									//System.out.println("lastTemperData:" + JSON.toJSONString(lastTemperData));
									monitorPointDto.setTemper_data(lastTemperData);
								} else {
									//System.out.println(monitorFlag + ":获取保护器最新的数据");
									list.add(monitorFlag);
									List<Voltage_data> lastVoltageData = XWDataEntry.getLastVoltageData(list);// 获取保护器最新的数据
									//System.out.println("lastVoltageData:" + JSON.toJSONString(lastVoltageData));
									monitorPointDto.setVoltage_data(lastVoltageData);
								}
								monitorPointDtoList.add(monitorPointDto);
							}
							linesOnBranchDto = new LinesOnBranchDto();
							linesOnBranchDto.setId(linesOnBranch.getId());
							/* linesOnBranchDto.setId(linesOnBranch.getId()); */
							linesOnBranchDto.setBranchName(linesOnBranch.getBranchName());
							linesOnBranchDto.setBranchType(linesOnBranch.getBranchType());
							linesOnBranchDto.setCabin_no(linesOnBranch.getCabin_no());
							linesOnBranchDto.setMonitorPointDtos(monitorPointDtoList);
							linesOnBranchDtoList.add(linesOnBranchDto);
						}

						linesOnMasterDto.setLinesOnBranchDtos(linesOnBranchDtoList);
						linesOnMasterDto.setId(linesOnMaster_new.getId());
						linesOnMasterDto.setMasterName(linesOnMaster_new.getMasterName());
						linesOnMasterDto.setMonitorFlag(linesOnMaster_new.getMonitorFlag());
						linesOnMasterDto.setNextMasterId(linesOnMaster_new.getNextMasterId());
						linesOnMasterDto.setRelatedType(linesOnMaster_new.getRelatedType());
						// 判断母线是否有故障：
						devidsList = new ArrayList<>();
						if (notEmpty(linesOnMaster_new.getMonitorFlag())) {
							devidsList.add(linesOnMaster_new.getMonitorFlag());
						//	List<FaultMsg> faultMessage = XWDataEntry.getFaultMessage(devidsList, false);
							List<Map<String, Object>> faultMessage = XWDataEntry.getFaultMessage(devidsList, false);
							if (faultMessage != null && faultMessage.size() > 0) {
								linesOnMasterDto.setIsProblem("Y");
							} else {
								linesOnMasterDto.setIsProblem("N");
							}

						}

						linesOnMasterList_result.add(linesOnMasterDto);
					}
					monitorStationDto.setLinesOnMasterSortList(linesOnMasterList_result);

					monitorStationDto.setTotalLines(totalLines); // 线路总数

					if (lineOnMasterMonitorFlagList.size() != 0) { // 避免电力那边集合为空而
					//	monitorStationDto.setTotalProblem(XWDataEntry.getFaultCount(lineOnMasterMonitorFlagList, false));// 故障总数
						monitorStationDto.setTotalProblem(XWDataEntry.getFaultNewCount(lineOnMasterMonitorFlagList, 0, false));// 故障总数
					} else {
						monitorStationDto.setTotalProblem(0);// 故障总数
					}
				}

				monitorStationDtoList.add(monitorStationDto);// 获取监测站
			}
		}
		data.setList(monitorStationDtoList);// 打包带走
		data.setCount(monitorStationDao.count(params));
		return data;
	}

	public void acceptFlag(String flag) { // 判断传输标志
		switch (flag) {
		case "one":
			acceptMaster = true;
			break;
		case "two":
			acceptBranch = true;
			acceptMaster = true;
			break;
		case "three":
			acceptBranch = true;
			acceptMaster = true;
			acceptPoint = true;
			break;
		default:
			acceptBranch = false;
			acceptMaster = false;
			acceptPoint = false;
			break;
		}
	}

	public Message<MonitorStationDto> addOrUpdateMonitorStation(String monitorStationId, String stationName,
			String stationAddress, String longitudeAndlatitude, String companyId, String linesOnMastersIds,
			String managerId, String addressIp,String city,String cityCode,String stationType) {
		MonitorStation monitorStation = null;
		if (notEmpty(monitorStationId)) {//编辑
			MonitorStation queryOne = monitorStationDao.queryOne("id", monitorStationId);
			if(super.notEmpty(stationName)&&!queryOne.getStationName().equals(stationName)){
				MonitorStation queryOneBystationName = monitorStationDao.queryOne("stationName", stationName);
				if(queryOneBystationName!=null){
					throw new BizException(Code.EXIST_MONITORSTATION_NAME);
				}
			}
			monitorStation = this.monitorStationDao.getEntity(monitorStationId);
			monitorStation.setModifiedBy(managerId);
			monitorStation.setModifyTime(getDate());
			addOrUpdateDetails(stationName, stationAddress, longitudeAndlatitude, companyId, linesOnMastersIds,
					monitorStation,city,cityCode,stationType);
			// 记录日志
			appLogDao.recordAddOrUpdateMonitorStation(addressIp, managerId, AppLogType.UPDATE, monitorStation);
		} else {//新增
			  MonitorStation queryOne = monitorStationDao.queryOne("stationName", stationName);
			if(queryOne!=null){
				throw new BizException(Code.EXIST_MONITORSTATION_NAME);
			}
			monitorStation = new MonitorStation();
			monitorStation.setCreateBy(managerId);
			monitorStation.setCreateTime(getDate());
			addOrUpdateDetails(stationName, stationAddress, longitudeAndlatitude, companyId, linesOnMastersIds,
					monitorStation,city,cityCode,stationType);
			this.monitorStationDao.save(monitorStation);
			// 记录日志
			appLogDao.recordAddOrUpdateMonitorStation(addressIp, managerId, AppLogType.ADD, monitorStation);
		}

		// 整理返回给前端，否则出问题
		List<LinesOnMasterDto> LinesOnMasterDtoSet = new ArrayList<LinesOnMasterDto>();
		MonitorStationDto dto = this.monitorStationDao.toDto(monitorStation, MonitorStationDto.class);

		List<LinesOnMaster> linesOnMasterSet = monitorStation.getLinesOnMasters();
		if (linesOnMasterSet != null) {
			for (LinesOnMaster linesOnMaster : linesOnMasterSet) {
				LinesOnMasterDtoSet.add(this.linesOnMasterDao.toDto(linesOnMaster, LinesOnMasterDto.class));
			}
			dto.setLinesOnMasterDtos(LinesOnMasterDtoSet);
		}

		// dto.setCompanyDto(this.companyDao.toDto(monitorStation.getCompany(),
		// CompanyDto.class));
		return new Message<MonitorStationDto>(Code.SUCCESS, dto);
	}

	private void addOrUpdateDetails(String stationName, String stationAddress, String longitudeAndlatitude,
			String companyId, String linesOnMastersIds, MonitorStation monitorStation,String city,String cityCode,String stationType) {
		List<LinesOnMaster> linesOnMasterSet = null;
		monitorStation.setStationName(stationName);
		monitorStation.setStationAddress(stationAddress);
		monitorStation.setLongitudeAndlatitude(longitudeAndlatitude);
		monitorStation.setCity(city);
		monitorStation.setCityCode(cityCode);
		
		if (stationType != null && notEmpty(stationType)) {
			monitorStation.setStationType(stationType);
		}else{
			monitorStation.setStationType("1");//默认800类型
		}


		if (monitorStation != null && notEmpty(companyId)) {
			Company company = this.companyDao.getEntity(companyId);
			monitorStation.setCompany(company);
		}

		if (monitorStation != null && notEmpty(linesOnMastersIds)) {
			String[] LinesOnMasterIdArr = linesOnMastersIds.split(",");
			if (LinesOnMasterIdArr.length > 0) {
				linesOnMasterSet = new ArrayList<LinesOnMaster>();
			}
			for (String id : LinesOnMasterIdArr) {
				LinesOnMaster LinesOnMaster = this.linesOnMasterDao.getEntity(id);
				linesOnMasterSet.add(LinesOnMaster);
			}
			monitorStation.setLinesOnMasters(linesOnMasterSet);
		}
	}

	public Message<?> delete(String monitorStationIds, String managerId, String addressIp) {
		String[] ids = monitorStationIds.split(",");
		for (String id : ids) {
			MonitorStation monitorStation = this.monitorStationDao.getEntity(id);
			List<LinesOnMaster> linesOnMasters = monitorStation.getLinesOnMasters();
			for (LinesOnMaster linesOnMaster : linesOnMasters) {
				/*
				 * Set<LinesOnBranch> linesOnBranchs =
				 * linesOnMaster.getLinesOnBranchs(); for (LinesOnBranch
				 * linesOnBranch : linesOnBranchs) { Set<MonitorPoint>
				 * monitorPoints = linesOnBranch.getMonitorPoints();
				 * this.monitorPointDao.deleteMonitorPoints(monitorPoints); }
				 * this.linesOnBranchDao.deleteLinesOnBranchs(linesOnBranchs);
				 */
				List<LinesOnBranch> linesOnBranchs = linesOnMaster.getLinesOnBranchs();

				// 删除支线操作
				for (LinesOnBranch linesOnBranch : linesOnBranchs) {
					// 去除支线上的人员讯息
					MemberInfo memberInfo = linesOnBranch.getMemberInfo();
					if (memberInfo != null) {
						String linesOnBranchId_my = memberInfo.getLinesOnBranchId();
						linesOnBranchId_my = linesOnBranchId_my.replace(id + ",", "");

						memberInfo.setLinesOnBranchId(linesOnBranchId_my);// 挑选出来
						linesOnBranch.setMemberInfo(null);
					}

					List<MonitorPoint> monitorPoints = linesOnBranch.getMonitorPoints();
					this.monitorPointDao.deleteMonitorPoints(monitorPoints);
					linesOnBranchDao.delete(linesOnBranch);
				}

				// 判断当前母线不为空并且当前母线上一段母线存在或者下一段母线存在的话就需要先把上下母线关联的id删除,
				if (linesOnMaster != null
						&& (notEmpty(linesOnMaster.getPreMasterId()) || notEmpty(linesOnMaster.getNextMasterId()))) {
					// 如果有上一段母线则取出上一段母线并把上一段母线的下一段母线id删除
					if (notEmpty(linesOnMaster.getPreMasterId())) {
						LinesOnMaster preLinesOnMaster = this.linesOnMasterDao
								.getEntity(linesOnMaster.getPreMasterId());
						if (preLinesOnMaster != null && notEmpty(preLinesOnMaster.getNextMasterId())) {
							preLinesOnMaster.setNextMasterId(null);
						}
					}
					// 如果有下一段母线则取出下一段母线并把下一段母线的上一段母线id删除
					if (notEmpty(linesOnMaster.getNextMasterId())) {
						LinesOnMaster nextLinesOnMaster = this.linesOnMasterDao
								.getEntity(linesOnMaster.getNextMasterId());
						if (nextLinesOnMaster != null && notEmpty(nextLinesOnMaster.getPreMasterId())) {
							nextLinesOnMaster.setPreMasterId(null);
						}
					}
				}

				// 最后把当前母线实体删除
				linesOnMasterDao.delete(linesOnMaster);
			}
			monitorStationDao.delete(monitorStation);

		}
		return Message.success();
	}

	public Set<String> queryMonitorAddress(String createBy, String companyId) {
		List<MonitorStation> list = null;
		Set<String> resultProvicne = new HashSet<>();
		Map<String, Object> params = new HashMap<>();
		if (empty(companyId) && notEmpty(createBy)) { // 除了admin超级管理员外
														// 管理员只能看到自己创建的公司
			SysUser SysUser_create = this.sysUserDao.queryOne("id", createBy);
			if (SysUser_create != null && notEmpty(SysUser_create.getPermission())
					&& !SysUser_create.getPermission().equalsIgnoreCase("admin")) {
				params.put("createBy", createBy);
			}
			//***************************************************************************//
			//***************************************************************************//
			MemberInfo memberInfo = this.memberInfoDao.queryOne("id", createBy); 
//			System.out.println("SysUser:"+JSON.toJSON(SysUser_create));
//			System.out.println("memberInfo:"+JSON.toJSON(memberInfo));
			if (memberInfo != null) {
				String monitorStationId = memberInfo.getMonitorStationId();
				String[] monitorStationIds = monitorStationId.split(",");
				    list = new ArrayList<MonitorStation>();
				for (String str : monitorStationIds) {
					 MonitorStation queryOne = this.monitorStationDao.queryOne("id",str);
					 if(queryOne!=null){
						 list.add(queryOne);
					 }
				}
				//params.put("createBy", createBy);
				for (MonitorStation monitorStation : list) {
//					if (notEmpty(monitorStation.getStationAddress()) && notEmpty(monitorStation.getLongitudeAndlatitude())) {
					if (notEmpty(monitorStation.getCity()) && notEmpty(monitorStation.getLongitudeAndlatitude())) {
						//String provicne = monitorStation.getStationAddress().substring(0, 2);
						String provicne = monitorStation.getCity();
						resultProvicne.add(provicne);
					}
				}
				return resultProvicne;
			}
			//***************************************************************************//
			//***************************************************************************//
		} else {
			Company company = this.companyDao.getEntity(companyId);
			params.put("company", company);
		}
		list = this.monitorStationDao.list(params);

		for (MonitorStation monitorStation : list) {
//			if (notEmpty(monitorStation.getStationAddress()) && notEmpty(monitorStation.getLongitudeAndlatitude())) {
			if (notEmpty(monitorStation.getCity()) && notEmpty(monitorStation.getLongitudeAndlatitude())) {
				//String provicne = monitorStation.getStationAddress().substring(0, 2);
				String provicne = monitorStation.getCity();
				resultProvicne.add(provicne);
			}
		}
		return resultProvicne;
	}
     
	public PageData<MonitorStationDto> listPageForApp(String companyId, String monitorStationId, String stationAddress,
			String stationName, Integer page, Integer rows, String flag, String createBy, String annotation,String city) {
		page = (page == null) ? 0 : page;
		rows = (rows == null) ? 10 : rows;
		flag = (flag == null) ? "true" : flag;
		Map<String, Object> params = new HashMap<>();
		if (StringUtils.isNotEmpty(monitorStationId)) {
			params.put("id", monitorStationId);
		}
//		if (StringUtils.isNotEmpty(stationAddress)) {
//			params.put("stationAddressLike", stationAddress);
//		}
		if (StringUtils.isNotEmpty(stationAddress)) {
			params.put("cityLike", stationAddress);
		}
//		if (StringUtils.isNotBlank(city)) {
//			params.put("cityLike", city);
//		}
		if (StringUtils.isNotEmpty(stationName)) {
			params.put("stationNameLike", stationName);
		}
		if (StringUtils.isNotEmpty(companyId)) {
			Company company = this.companyDao.getEntity(companyId);
			params.put("company", company);
		}
		if (StringUtils.isNotEmpty(createBy)) { // 除了admin超级管理员外 管理员只能看到自己创建的公司
			SysUser SysUser_create = this.sysUserDao.queryOne("id", createBy);
			if (SysUser_create != null && notEmpty(SysUser_create.getPermission())
					&& !SysUser_create.getPermission().equalsIgnoreCase("admin")) {
				params.put("createBy", createBy);
			}
		}
		params.put("page", page);
		params.put("rows", rows);
		params.put("order", "t.createTime desc");
		PageData<MonitorStationDto> data = new PageData<MonitorStationDto>();

		List<MonitorStationDto> monitorStationDtoList = new ArrayList<MonitorStationDto>();// 用来装东西的
		MonitorStationDto monitorStationDto = null;// 站点

		List<LinesOnMasterDto> linesOnMasterDtoList = null;// 母线
		LinesOnMasterDto linesOnMasterDto = null;

		List<LinesOnBranchDto> linesOnBranchDtoList = null;// 支线
		LinesOnBranchDto linesOnBranchDto = null;

		List<MonitorPointDto> monitorPointDtoList = null;// 监测点
		MonitorPointDto monitorPointDto = null;

		List<MonitorStation> queryMonitorStationList =null;
		
		MemberInfo memberInfo = this.memberInfoDao.queryOne("id", createBy); 
		if (memberInfo != null) {
			String monitorStationIds = memberInfo.getMonitorStationId();
			String[] monitorStationIdArr = monitorStationIds.split(",");
			queryMonitorStationList = new ArrayList<MonitorStation>();
			for (String str : monitorStationIdArr) {
				 MonitorStation queryOne = this.monitorStationDao.queryOne("id",str);
				 queryMonitorStationList.add(queryOne);
			}
			
	   }else{
			queryMonitorStationList = monitorStationDao.list(params);// 求出bean结果集
		}
		
		acceptFlag(flag);//fk ....
		List<String> list_temper = new ArrayList<>();
		List<String> list_voltage = new ArrayList<>();
		
		for (MonitorStation monitorStation : queryMonitorStationList) {
			monitorStationDto = this.monitorStationDao.toDto(monitorStation, MonitorStationDto.class);// 提前转化Dto方便装各种线路种类
			List<LinesOnMaster> linesOnMasters = null;

			if (acceptMaster) {
				linesOnMasterDtoList = new ArrayList<LinesOnMasterDto>();// 母线初始化
				linesOnMasters = monitorStation.getLinesOnMasters(); // 第三步：获取母线并装进母线Set中
				for (LinesOnMaster linesOnMaster : linesOnMasters) {
					// 在这里设置障碍：避免数据量多大，如果需要查询母线和支线下面的数据就传true,否则传false,
					linesOnMasterDto = this.linesOnMasterDao.toDto(linesOnMaster, LinesOnMasterDto.class);

					if (acceptBranch) {
						linesOnBranchDtoList = new ArrayList<LinesOnBranchDto>();// 支线初始化
						List<LinesOnBranch> linesOnBranchs = linesOnMaster.getLinesOnBranchs(); // 第二步：获取支线并装进支线Set中
					
						for (LinesOnBranch linesOnBranch : linesOnBranchs) {
							if (acceptPoint) {
								monitorPointDtoList = new ArrayList<MonitorPointDto>();// 监测点初始化
								List<MonitorPoint> monitorPoints = linesOnBranch.getMonitorPoints(); // 第一步：现获取站点并装进站点Set中
						//	    System.out.println("monitorPoints数量"+monitorPoints.size());
							    
								for (MonitorPoint monitorPoint : monitorPoints) {
									monitorPointDto = this.monitorPointDao.toDto(monitorPoint, MonitorPointDto.class);
								
									if(monitorPointDto==null)
										continue;
									
									String monitorFlag2 = monitorPointDto.getMonitorFlag();
									// 监测设备标识
									if (monitorPoint.getHitchType().equals("温度采集器")) {
										
										list_temper.add(monitorFlag2);
										List<Temper_data> lastTemperData = new ArrayList<Temper_data>();
										lastTemperData.clear();
										Temper_data temper = new Temper_data();
										temper.setSensorId(monitorFlag2);
										lastTemperData.add(temper);
//										List<Temper_data> lastTemperData = XWDataEntry.getLastTemperData(list);// 获取温度采集器最新的数据
										monitorPointDto.setTemper_data(lastTemperData);										
									} else {
										
										list_voltage.add(monitorFlag2);										
										List<Voltage_data> lastVoltageData = new ArrayList<Voltage_data>();
										lastVoltageData.clear();
										Voltage_data voltage = new Voltage_data();
										voltage.setSensorId(monitorFlag2);
										lastVoltageData.add(voltage);
//										List<Voltage_data> lastVoltageData = XWDataEntry.getLastVoltageData(list);// 获取保护器最新的数据
										monitorPointDto.setVoltage_data(lastVoltageData);
									}

									monitorPointDtoList.add(monitorPointDto);
									// monitorStationDto.setMonitorPointCount(monitorPointDtoList.size());//
									// 注值监测点数量
								}
							}

							linesOnBranchDto = this.linesOnBranchDao.toDto(linesOnBranch, LinesOnBranchDto.class);
							linesOnBranchDto.setMonitorPointDtos(monitorPointDtoList);
							linesOnBranchDtoList.add(linesOnBranchDto);
							// monitorStationDto.setLinesOnBranchCount(linesOnBranchDtoList.size());//
							// 注值支线数量
						}
						linesOnMasterDto.setLinesOnBranchDtos(linesOnBranchDtoList);
					}

					linesOnMasterDtoList.add(linesOnMasterDto);
					monitorStationDto.setLinesOnMasterCount(linesOnMasterDtoList.size());// 注值母线数量
				}

				monitorStationDto.setLinesOnMasterDtos(linesOnMasterDtoList);// 获取母线
			}			
			monitorStationDtoList.add(monitorStationDto);// 获取监测站
		}
		List<Temper_data> listTemperData = null;
		if(list_temper.size()>0)
		{
			listTemperData = XWDataEntry.getLastTemperData(list_temper);
		}
		List<Voltage_data> listVoltageData = null;
		if (list_voltage.size() > 0) {
			listVoltageData = XWDataEntry.getLastVoltageData(list_voltage);
		}
		for(MonitorStationDto station:monitorStationDtoList)
		{
			for (LinesOnMasterDto linesOnMaster : station.getLinesOnMasterDtos()) {
				for (LinesOnBranchDto linesOnBrach : linesOnMaster.getLinesOnBranchDtos()) {
					for (MonitorPointDto point : linesOnBrach.getMonitorPointDtos()) {
						
						if (point.getTemper_data() != null && point.getTemper_data().size() > 0) {
							for (int i = 0; i < listTemperData.size(); i++) {
								if (point.getTemper_data().get(0).getSensorId().equals(listTemperData.get(i).getSensorId()))
								{
									point.getTemper_data().set(0, listTemperData.get(i));
									break;
								}
							}
						}
						
						if (point.getVoltage_data() != null && point.getVoltage_data().size() > 0) {
							for (int i = 0; i < listVoltageData.size(); i++) {
								if (point.getVoltage_data().get(0).getSensorId().equals(listVoltageData.get(i).getSensorId()))
								{
									point.getVoltage_data().set(0, listVoltageData.get(i));
									break;
								}
							}
						}
						
					}
				}
			}
		}
		data.setList(monitorStationDtoList);
		data.setCount(monitorStationDao.count(params));
		return data;
	}
	
	

	/**
	 * 一次接线图
	 * 
	 * @param companyId
	 * @param monitorStationId
	 * @param stationAddress
	 * @param stationName
	 * @param page
	 * @param rows
	 * @param flag
	 * @param createBy
	 * @param annotation
	 * @return
	 */
	public PageData<MonitorStationDto> queryWiringDiagram(String companyId, String monitorStationId,
			String stationAddress, String stationName, Integer page, Integer rows, String flag, String createBy,
			String annotation,String loginMemberId) {
		page = (page == null) ? 0 : page;
		rows = (rows == null) ? 10 : rows;
		flag = (flag == null) ? "true" : flag;
		Map<String, Object> params = new HashMap<>();
		if (StringUtils.isNotEmpty(monitorStationId)) {
			params.put("id", monitorStationId);
		}
		if (StringUtils.isNotEmpty(stationAddress)) {
			params.put("stationAddressLike", stationAddress);
		}
		if (StringUtils.isNotEmpty(stationName)) {
			params.put("stationNameLike", stationName);
		}
		if (StringUtils.isNotEmpty(companyId)) {
			Company company = this.companyDao.getEntity(companyId);
			params.put("company", company);
		}
		
		if (StringUtils.isNotEmpty(createBy)) { // 除了admin超级管理员外 管理员只能看到自己创建的公司
			SysUser SysUser_create = this.sysUserDao.queryOne("id", createBy);
			if (SysUser_create != null && notEmpty(SysUser_create.getAccount())
					&& !SysUser_create.getAccount().equalsIgnoreCase("admin")) {
				params.put("createBy", createBy);
			}
		}
		params.put("page", page);
		params.put("rows", rows);
		params.put("order", "t.createTime desc");
		PageData<MonitorStationDto> data = new PageData<MonitorStationDto>();

		List<MonitorStationDto> monitorStationDtoList = new ArrayList<MonitorStationDto>();// 用来装东西的
		MonitorStationDto monitorStationDto = null;// 站点

		// Set<LinesOnMasterDto> linesOnMasterDtoList = null;// 母线
		LinesOnMasterDto linesOnMasterDto = null;

		List<LinesOnBranchDto> linesOnBranchDtoList = null;// 支线
		LinesOnBranchDto linesOnBranchDto = null;

		List<MonitorPointDto> monitorPointDtoList = null;// 监测点
		MonitorPointDto monitorPointDto = null;

		List<LinesOnMaster> linesOnMasterList = null;
		List<LinesOnMasterDto> linesOnMasterList_result =null;
		List<MonitorStation> queryMonitorStationList =null;
		acceptFlag(flag);
		MemberInfo memberInfo = this.memberInfoDao.queryOne("id", createBy); 
		if (memberInfo != null) {
			String monitorStationIds = memberInfo.getMonitorStationId();
			String[] monitorStationIdArr = monitorStationIds.split(",");
			queryMonitorStationList = new ArrayList<MonitorStation>();
			for (String str : monitorStationIdArr) {
				 MonitorStation queryOne = this.monitorStationDao.queryOne("id",str);
				 queryMonitorStationList.add(queryOne);
			}
	   }else{
		   queryMonitorStationList = monitorStationDao.list(params);// 求出bean结果集
	   }
		
		/**
		 * w-------------------------------fuck-------------------------------------------
		 */
			
			
		
		/**
		 * w-------------------------------fuck-------------------------------------------
		 */
		
		
		for (MonitorStation monitorStation : queryMonitorStationList) {
			// linesOnMasterDtos
			monitorStationDto = this.monitorStationDao.toDto(monitorStation, MonitorStationDto.class);// 提前转化Dto方便装各种线路种类
			List<LinesOnMaster> linesOnMasters = null;
			if (acceptMaster) {
				linesOnMasters = monitorStation.getLinesOnMasters(); // 第三步：获取母线并装进母线Set中
			}

			LinesOnMaster nextMaster_ = null;
			// 为排序做处理
			linesOnMasterList = new ArrayList<LinesOnMaster>();// 母线排序
			if (notEmpty(flag) && flag.equalsIgnoreCase("three")) {
				for (LinesOnMaster master_old : linesOnMasters) {
					if (master_old != null && notEmpty(master_old.getPreMasterId())) {
						continue;
					} else if (master_old != null && empty(master_old.getPreMasterId())) {
						linesOnMasterList.add(master_old);// 先将首部装进去
						nextMaster_ = this.linesOnMasterDao.getEntity(master_old.getNextMasterId());
						if (nextMaster_ != null) {
							linesOnMasterList.add(nextMaster_);// 装首部的下一段母线
						}
						while (nextMaster_ != null && notEmpty(nextMaster_.getNextMasterId())) { // 下一段母线不为空并且下一段母线一直有下一段母线id,
							nextMaster_ = this.linesOnMasterDao.getEntity(nextMaster_.getNextMasterId());
							if (nextMaster_ != null) {
								linesOnMasterList.add(nextMaster_);// 这个地方仅取首部
							}
						}
						if (linesOnMasterList.size() == linesOnMasters.size()) {
							break;
						}
					}
				}

				// 给所有参加排序的母线和非排序母线指定支线
				int totalLines = 0;// 线路总数
				List<String> lineOnMasterMonitorFlagList = new ArrayList<String>();
				linesOnMasterList_result = new ArrayList<LinesOnMasterDto>();// 母线初始化
				totalLines = totalLines + linesOnMasterList.size();// 线路母线总数
				List<String> devidsList  = new ArrayList<>();
				int point=0; //监测数量
				for (LinesOnMaster linesOnMaster_new : linesOnMasterList) {
					lineOnMasterMonitorFlagList.add(linesOnMaster_new.getMonitorFlag());// 获取母线标识：即// MonitorFlag
					linesOnMasterDto = new LinesOnMasterDto();
					linesOnBranchDtoList = new ArrayList<LinesOnBranchDto>();// 支线初始化
					List<LinesOnBranch> linesOnBranchs = linesOnMaster_new.getLinesOnBranchs(); // 第二步：获取支线并装进支线Set中
					totalLines = totalLines + linesOnBranchs.size();// 线路支线总数
					for (LinesOnBranch linesOnBranch : linesOnBranchs) {
						
						monitorPointDtoList = new ArrayList<MonitorPointDto>();// 监测点初始化

						List<MonitorPoint> monitorPoints = linesOnBranch.getMonitorPoints(); // 第一步：现获取站点并装进站点Set中
					    point+=	monitorPoints.size();
						for (MonitorPoint monitorPoint : monitorPoints) {
							lineOnMasterMonitorFlagList.add(monitorPoint.getMonitorFlag());//加入监测点设备标识 ，用于查询故障数量
							
							monitorPointDto = new MonitorPointDto();
								monitorPointDto.setId(monitorPoint.getId());
								monitorPointDto.setPointName(monitorPoint.getPointName());
								monitorPointDto.setHitchType(monitorPoint.getHitchType());
								monitorPointDto.setMonitorFlag(monitorPoint.getMonitorFlag());
								monitorPointDto.setPositionName(monitorPoint.getPosition());
								monitorPointDto.setType(monitorPoint.getType());
								monitorPointDtoList.add(monitorPointDto);
						}
					
						monitorStationDto.setMonitorPointCount(point);
						
						linesOnBranchDto = new LinesOnBranchDto();
							linesOnBranchDto.setId(linesOnBranch.getId());
							linesOnBranchDto.setBranchName(linesOnBranch.getBranchName());
							linesOnBranchDto.setBranchType(linesOnBranch.getBranchType());
							linesOnBranchDto.setCabin_no(linesOnBranch.getCabin_no());
							linesOnBranchDto.setSensorNumber(linesOnBranch.getSensorNumber());
							linesOnBranchDto.setMonitorPointDtos(monitorPointDtoList);
							linesOnBranchDtoList.add(linesOnBranchDto);
					}

					linesOnMasterDto.setLinesOnBranchDtos(linesOnBranchDtoList);
					linesOnMasterDto.setId(linesOnMaster_new.getId());
					linesOnMasterDto.setMasterName(linesOnMaster_new.getMasterName());
					linesOnMasterDto.setMonitorFlag(linesOnMaster_new.getMonitorFlag());
					linesOnMasterDto.setHitchType(linesOnMaster_new.getHitchType());
					linesOnMasterDto.setNextMasterId(linesOnMaster_new.getNextMasterId());
					linesOnMasterDto.setRelatedType(linesOnMaster_new.getRelatedType());
					//判断母线是否有故障：
					
					if (notEmpty(linesOnMaster_new.getMonitorFlag())) {
						devidsList.add(linesOnMaster_new.getMonitorFlag());
//						List<Map<String, Object>> faultMessage = XWDataEntry.getFaultMessage(devidsList, false);
//
//						if (faultMessage != null && faultMessage.size() > 0) {
//							linesOnMasterDto.setIsProblem("Y");
//						} else {
//							linesOnMasterDto.setIsProblem("N");
//						}

					}
					linesOnMasterList_result.add(linesOnMasterDto);
				}
				List<Map<String, Object>> faultMessage = null;
				if (devidsList.size() > 0) {
					faultMessage = XWDataEntry.getFaultMessage(devidsList, false);
				}
				if (faultMessage != null && faultMessage.size() > 0) {
					for (LinesOnMasterDto linesOnMaster : linesOnMasterList_result) {
						for (int i = 0; i < faultMessage.size(); i++) {
							if (linesOnMaster.getMonitorFlag().equals((String) faultMessage.get(i).get("devid"))) {
								linesOnMaster.setIsProblem("Y");
								break;
							}
						}
					}
				}
			//	System.out.println("faultMessage:"+JSON.toJSONString(faultMessage));
				
				//*******************************************************
				 List<String> devCodeList = linesOnMasterList_result.stream().map(LinesOnMasterDto::getMonitorFlag).collect(Collectors.toList());
				   if(!devCodeList.isEmpty()&&devCodeList!=null){
					List<device_info> deviceInfo = XWDataEntry.getDeviceInfo(devCodeList);
						if(deviceInfo!=null&&!deviceInfo.isEmpty()){
							for (device_info device_info : deviceInfo) {
								for (LinesOnMasterDto xx : linesOnMasterList_result) {
									if(device_info.getDevid().equals(xx.getMonitorFlag())){
										//linesOnMasterDto.setIsOnline(device_info.isOnline());
										xx.setIsOnline(device_info.isOnline());
										break;
									}	
								}
							}
					   }	
				   }
				//*******************************************************
				monitorStationDto.setLinesOnMasterSortList(linesOnMasterList_result);

				monitorStationDto.setTotalLines(totalLines); // 线路总数
				if (lineOnMasterMonitorFlagList.size() != 0) { // 避免电力那边集合为空而
					//=======
					monitorStationDto.setTotalProblem(XWDataEntry.getFaultNewCount(lineOnMasterMonitorFlagList, 0, false));// 故障总数
					//=======
				} else {
					monitorStationDto.setTotalProblem(0);// 故障总数
				}
			}
			MemberInfo appUser = this.memberInfoDao.queryOne("id", loginMemberId); 
	//		System.out.println(JSON.toJSON(appUser));
			if (appUser != null) {
				String monitorStationIds = appUser.getMonitorStationId();
				String[] monitorStationIdArr = monitorStationIds.split(",");
				for (String monitorStationIdArrs : monitorStationIdArr) {
					boolean equals = monitorStation.getId().equals(monitorStationIdArrs);
					if(equals){
						monitorStationDtoList.add(monitorStationDto);// 获取监测站
					}
				}
			}else{
					monitorStationDtoList.add(monitorStationDto);// 获取监测站
			}
		}
        
		data.setList(monitorStationDtoList);
		data.setCount(monitorStationDao.count(params));
		return data;
	}

	/**
	 * 一次接线图数据：
	 * 
	 * @param companyId
	 * @param monitorStationId
	 * @param stationAddress
	 * @param stationName
	 * @param page
	 * @param rows
	 * @param flag
	 * @param createBy
	 * @param annotation
	 * @return
	 */
	public PageData<MonitorPointDto> getWiringDiagramData(String companyId, String monitorStationId,
			String stationAddress, String stationName, Integer page, Integer rows, String flag, String createBy,
			String annotation) {
		page = (page == null) ? 0 : page;
		rows = (rows == null) ? 10 : rows;
		flag = (flag == null) ? "true" : flag;
		Map<String, Object> params = new HashMap<>();
		if (StringUtils.isNotEmpty(monitorStationId)) {
			params.put("id", monitorStationId);
		}
		if (StringUtils.isNotEmpty(stationAddress)) {
			params.put("stationAddressLike", stationAddress);
		}
		if (StringUtils.isNotEmpty(stationName)) {
			params.put("stationNameLike", stationName);
		}
		if (StringUtils.isNotEmpty(companyId)) {
			Company company = this.companyDao.getEntity(companyId);
			params.put("company", company);
		}
		if (StringUtils.isNotEmpty(createBy)) { // 除了admin超级管理员外 管理员只能看到自己创建的公司
			SysUser SysUser_create = this.sysUserDao.queryOne("id", createBy);
			if (SysUser_create != null && notEmpty(SysUser_create.getAccount())
					&& !SysUser_create.getAccount().equalsIgnoreCase("admin")) {
				params.put("createBy", createBy);
			}
		}
		params.put("page", page);
		params.put("rows", rows);
		params.put("order", "t.createTime desc");
		PageData<MonitorPointDto> data = new PageData<MonitorPointDto>();

		List<MonitorStationDto> monitorStationDtoList = new ArrayList<MonitorStationDto>();// 用来装东西的
		MonitorStationDto monitorStationDto = null;// 站点

		LinesOnMasterDto linesOnMasterDto = null;

		List<LinesOnBranchDto> linesOnBranchDtoList = null;// 支线
		LinesOnBranchDto linesOnBranchDto = null;

		List<MonitorPointDto> monitorPointDtoList = null;// 监测点
		MonitorPointDto monitorPointDto = null;

		List<LinesOnMaster> linesOnMasterList = null;
		List<LinesOnMasterDto> linesOnMasterList_result = null;

		acceptFlag(flag);
		// List<MonitorStation> queryMonitorStationList =
		// monitorStationDao.list(params);// 求出bean结果集
		MonitorStation queryOne = monitorStationDao.queryOne(params);

		monitorStationDto = this.monitorStationDao.toDto(queryOne, MonitorStationDto.class);// 提前转化Dto方便装各种线路种类
		List<LinesOnMaster> linesOnMasters = null;
		if (acceptMaster) {
			linesOnMasters = queryOne.getLinesOnMasters(); // 第三步：获取母线并装进母线Set中
		}

		// monitorStationDao.queryOne(params);

		LinesOnMaster nextMaster_ = null;
		// 为排序做处理
		linesOnMasterList = new ArrayList<LinesOnMaster>();// 母线排序
		if (notEmpty(flag) && flag.equalsIgnoreCase("three")) {
			List<String> list_temper = new ArrayList<>();
			List<String> list_voltage = new ArrayList<>();
			for (LinesOnMaster master_old : linesOnMasters) {
				if (master_old != null && notEmpty(master_old.getPreMasterId())) {
					continue;
				} else if (master_old != null && empty(master_old.getPreMasterId())) {
					linesOnMasterList.add(master_old);// 先将首部装进去
					nextMaster_ = this.linesOnMasterDao.getEntity(master_old.getNextMasterId());
					if (nextMaster_ != null) {
						linesOnMasterList.add(nextMaster_);// 装首部的下一段母线
					}
					while (nextMaster_ != null && notEmpty(nextMaster_.getNextMasterId())) { // 下一段母线不为空并且下一段母线一直有下一段母线id,
						nextMaster_ = this.linesOnMasterDao.getEntity(nextMaster_.getNextMasterId());
						if (nextMaster_ != null) {
							linesOnMasterList.add(nextMaster_);// 这个地方仅取首部
						}
					}
					if (linesOnMasterList.size() == linesOnMasters.size()) {
						break;
					}
				}
			}

			List<String> lineOnMasterMonitorFlagList = new ArrayList<String>();
			linesOnMasterList_result = new ArrayList<LinesOnMasterDto>();// 母线初始化
			for (LinesOnMaster linesOnMaster_new : linesOnMasterList) {
				lineOnMasterMonitorFlagList.add(linesOnMaster_new.getMonitorFlag());// 获取母线标识：即
																					// MonitorFlag
				linesOnMasterDto = new LinesOnMasterDto();
				linesOnBranchDtoList = new ArrayList<LinesOnBranchDto>();// 支线初始化
				List<LinesOnBranch> linesOnBranchs = linesOnMaster_new.getLinesOnBranchs(); // 第二步：获取支线并装进支线Set中
				for (LinesOnBranch linesOnBranch : linesOnBranchs) {

					monitorPointDtoList = new ArrayList<MonitorPointDto>();// 监测点初始化
					List<MonitorPoint> monitorPoints = linesOnBranch.getMonitorPoints(); // 第一步：现获取站点并装进站点Set中
					long start, end;
					for (MonitorPoint monitorPoint : monitorPoints) {
						start = System.currentTimeMillis();
						monitorPointDto = new MonitorPointDto();
						monitorPointDto.setId(monitorPoint.getId());
						monitorPointDto.setPointName(monitorPoint.getPointName());
						monitorPointDto.setMonitorFlag(monitorPoint.getMonitorFlag());
						monitorPointDto.setPositionName(monitorPoint.getPosition());
						
						String monitorFlag = monitorPoint.getMonitorFlag();
						// 监测设备标识
//						list.add(monitorFlag);
						if (monitorPoint.getHitchType().equals("温度采集器")) {
							list_temper.add(monitorFlag);
							List<Temper_data> lastTemperData = new ArrayList<Temper_data>();
							lastTemperData.clear();
							Temper_data temper = new Temper_data();
							temper.setSensorId(monitorFlag);
							lastTemperData.add(temper);
//							List<Temper_data> lastTemperData = XWDataEntry.getLastTemperData(list);// 获取温度采集器最新的数据
							monitorPointDto.setTemper_data(lastTemperData);
						} else {
							list_voltage.add(monitorFlag);
							
							List<Voltage_data> lastVoltageData = new ArrayList<Voltage_data>();
							lastVoltageData.clear();
							Voltage_data voltage = new Voltage_data();
							voltage.setSensorId(monitorFlag);
							lastVoltageData.add(voltage);
							
//							List<Voltage_data> lastVoltageData = XWDataEntry.getLastVoltageData(list);// 获取保护器最新的数据
							monitorPointDto.setVoltage_data(lastVoltageData);
						}
						monitorPointDtoList.add(monitorPointDto);
						end = System.currentTimeMillis();
						System.out.println("<<<获取温度采集器和保护器最新数据>>>start time:" + start + "; end time:" + end + "; Run Time:" + (end - start) + "(ms)");
					}
					linesOnBranchDto = new LinesOnBranchDto();
					linesOnBranchDto.setId(linesOnBranch.getId());
					linesOnBranchDto.setBranchName(linesOnBranch.getBranchName());
					linesOnBranchDto.setBranchType(linesOnBranch.getBranchType());
					linesOnBranchDto.setCabin_no(linesOnBranch.getCabin_no());
					linesOnBranchDto.setSensorNumber(linesOnBranch.getSensorNumber());
					// linesOnBranchDto.setMonitorPointDtos(monitorPointDtoList);
					linesOnBranchDtoList.add(linesOnBranchDto);
				}

				linesOnMasterDto.setLinesOnBranchDtos(linesOnBranchDtoList);
				linesOnMasterDto.setId(linesOnMaster_new.getId());
				linesOnMasterDto.setMasterName(linesOnMaster_new.getMasterName());
				linesOnMasterDto.setMonitorFlag(linesOnMaster_new.getMonitorFlag());
				linesOnMasterDto.setHitchType(linesOnMaster_new.getHitchType());
				linesOnMasterDto.setNextMasterId(linesOnMaster_new.getNextMasterId());
				linesOnMasterDto.setRelatedType(linesOnMaster_new.getRelatedType());
				linesOnMasterList_result.add(linesOnMasterDto);
			}
			
			List<Temper_data> listTemperData = null;
			if(list_temper.size()>0)
			{
				listTemperData = XWDataEntry.getLastTemperData(list_temper);
			}
			List<Voltage_data> listVoltageData = null;
			if (list_voltage.size() > 0) {
				listVoltageData = XWDataEntry.getLastVoltageData(list_voltage);
			}
			for (LinesOnMasterDto linesOnMaster : linesOnMasterList_result) {
				for (LinesOnBranchDto linesOnBrach : linesOnMaster.getLinesOnBranchDtos()) {
					for (MonitorPointDto point : linesOnBrach.getMonitorPointDtos()) {
						
						if (point.getTemper_data() != null && point.getTemper_data().size() > 0) {
							for (int i = 0; i < listTemperData.size(); i++) {
								if (point.getTemper_data().get(0).getSensorId()
										.equals(listTemperData.get(i).getSensorId()))
								{
									point.getTemper_data().set(0, listTemperData.get(i));
									break;
								}
							}
						}
						
						if (point.getVoltage_data() != null && point.getVoltage_data().size() > 0) {
							for (int i = 0; i < listVoltageData.size(); i++) {
								if (point.getVoltage_data().get(0).getSensorId()
										.equals(listVoltageData.get(i).getSensorId()))
								{
									point.getVoltage_data().set(0, listVoltageData.get(i));
									break;
								}
							}
						}
						
					}
				}
			}

			monitorStationDto.setLinesOnMasterSortList(linesOnMasterList_result);

		}

		monitorStationDtoList.add(monitorStationDto);// 获取监测站

		data.setList(monitorPointDtoList);
		return data;
	}

	/**
	 * 监测点数据（接线图表格数据）
	 * @param temperIds  温度采集器设备标识，多个逗号隔开
	 * @param voltageIds 过电压保护器设备标识 ，多个逗号隔开
	 * @return
	 */
	public PageData<MonitorPointDto> getWiringDiagramDataV2(String temperIds, String voltageIds) {
		//System.out.println("temperIds:"+temperIds);
		//System.out.println("voltageIds:"+voltageIds);
		MonitorPointDto monitorPointDto = new MonitorPointDto();
		List<MonitorPointDto> MonitorPointDtos = new ArrayList<>();
		PageData<MonitorPointDto> data = new PageData<MonitorPointDto>();
		if(StringUtils.isNotEmpty(temperIds)){
			String[] devidsArr = temperIds.split(",");
			List<String> temperIdslist  = Arrays.asList(devidsArr);
		//	System.out.println("temperIdslist:>>>>>>>>>"+JSON.toJSONString(temperIdslist));
			Set<String> set = new HashSet<>(temperIdslist);
		//	System.out.println("temperIdslist:>>>>>>>>>"+JSON.toJSONString(new ArrayList<>(set)));
			List<Temper_data> lastTemperData = XWDataEntry.getLastTemperData(new ArrayList<>(set));// 获取温度采集器最新的数据
		//	System.out.println("lastTemperData:>>>>>>>>>"+JSON.toJSONString(lastTemperData));
			monitorPointDto.setTemper_data(lastTemperData);
		}
		if(StringUtils.isNotEmpty(voltageIds)){
			String[] devidsArr = voltageIds.split(",");
			List<String> voltageIdslist  = Arrays.asList(devidsArr);
			List<Voltage_data> lastVoltageData = XWDataEntry.getLastVoltageData(voltageIdslist);// 获取保护器最新的数据
		//	System.out.println("lastVoltageData:>>>>>>>>>"+JSON.toJSONString(lastVoltageData));
			monitorPointDto.setVoltage_data(lastVoltageData);
		}




		MonitorPointDtos.add(monitorPointDto);
		data.setList(MonitorPointDtos);
		return data;
	}
	
	public PageData<MonitorStationDto> listPage2(String companyId, String monitorStationId, String stationAddress,
			String stationName, Integer page, Integer rows,String flag, String createBy, String annotation, String city) {
		page = (page == null) ? 0 : page;
		rows = (rows == null) ? 10 : rows;
		Map<String, Object> params = new HashMap<>();
		List<MonitorStation>  monitorStationList =new ArrayList<>();
		boolean memberLoginData =false;
		if (StringUtils.isNotEmpty(monitorStationId)) {
			params.put("id", monitorStationId);
		}
		if (StringUtils.isNotEmpty(stationAddress)) {
			params.put("stationAddressLike", stationAddress);
		}
		if (StringUtils.isNotEmpty(stationName)) {
			params.put("stationNameLike", stationName);
		}
		if (StringUtils.isNotBlank(city)) {
			params.put("cityLike", city);
		//	System.out.println("city///////----------------:"+city);
		}
		if (StringUtils.isNotEmpty(companyId)) {
			Company company = this.companyDao.getEntity(companyId);
			params.put("company", company);
		}
		if (StringUtils.isNotEmpty(createBy)) { // 除了admin超级管理员外 管理员只能看到自己创建的公司
			SysUser SysUser_create = this.sysUserDao.queryOne("id", createBy);
			if (SysUser_create != null && notEmpty(SysUser_create.getPermission())
					&& !SysUser_create.getPermission().equalsIgnoreCase("admin")) {
				params.put("createBy", createBy);
			}
			//==================================================================
			MemberInfo memberInfo = this.memberInfoDao.queryOne("id", createBy); 
			if (memberInfo != null) {
				memberLoginData= true;
				String monitorStationIds = memberInfo.getMonitorStationId();
				String[] monitorStationIdArr = monitorStationIds.split(",");
				monitorStationList = new ArrayList<MonitorStation>();
				for (String str : monitorStationIdArr) {
					 MonitorStation queryOne = this.monitorStationDao.queryOne("id",str);
					 monitorStationList.add(queryOne);
				}
		}
	}
		params.put("page", page);
		params.put("rows", rows);
		params.put("order", "t.createTime desc");
		PageData<MonitorStationDto> data = new PageData<MonitorStationDto>();
		List<MonitorStationDto> res = new ArrayList<MonitorStationDto>();
		List<MonitorStation> queryMonitorStationList = null;
			 
		 if(memberLoginData){  //如果是巡检员登陆监测站数据
			 queryMonitorStationList=monitorStationList;
		 }else{
			 queryMonitorStationList = monitorStationDao.listPage(params);// 求出bean结果集
		 }
		
		 queryMonitorStationList.forEach(m->{
			// System.out.println("company:"+JSON.toJSONString(m.getCompany()));
			 MonitorStationDto	 monitorStationDto = this.monitorStationDao.toDto(m, MonitorStationDto.class);
			 monitorStationDto.setCompanyDto(companyDao.toDto(m.getCompany(),CompanyDto.class));
			 res.add(monitorStationDto);
		 });
		 
		 	data.setList(res);
			data.setCount(monitorStationDao.count(params));
		
		return data;
	}
	
	public MonitorStationDto getEntityById(String id){
		List<LinesOnMasterDto>  masterDtoRes = new ArrayList<>();
		MonitorStation entity = monitorStationDao.getEntity(id);
		List<LinesOnMaster> linesOnMasters = entity.getLinesOnMasters();
		linesOnMasters.forEach(m->{
			List<LinesOnBranchDto>   branchDtoRes = new ArrayList<>();
			LinesOnMasterDto dto = linesOnMasterDao.toDto(m, LinesOnMasterDto.class);
			List<LinesOnBranch> linesOnBranchs = m.getLinesOnBranchs();
			linesOnBranchs.forEach(b->{
				List<MonitorPoint> monitorPoints = b.getMonitorPoints();
				List<MonitorPointDto> dto2 = monitorPointDao.toDto(monitorPoints, MonitorPointDto.class);
					
				LinesOnBranchDto dto3 = linesOnBranchDao.toDto(b, LinesOnBranchDto.class);
				dto3.setMonitorPointDtos(dto2);
				branchDtoRes.add(dto3);
				dto.setLinesOnBranchDtos(branchDtoRes);
			});
			
			masterDtoRes.add(dto);
		});
		
		MonitorStationDto dto = this.monitorStationDao.toDto(entity, MonitorStationDto.class);
		dto.setLinesOnMasterDtos(masterDtoRes);
		return dto;
	}
	
	public static void main(String args[]){
		String 	temperIds="C855";
		List<String> list = new ArrayList<>();
		list.add(temperIds);
		List<Temper_data> lastTemperData = XWDataEntry.getLastTemperData(list);// 获取温度采集器最新的数据
		System.out.println("lastTemperData:"+JSON.toJSONString(lastTemperData));
		List<device_info> deviceInfo = XWDataEntry.getDeviceInfo(null);
			
		System.out.println("deviceInfo:"+JSON.toJSONString(deviceInfo));
		
	}
	
//	public String getLineMasterIdByMemberId(String memberId){
//		MemberInfo member = memberInfoDao.getEntity(memberId);
//		String monitorStationId = member.getMonitorStationId();
//	
//		linesOnMasterDao.list(params);      
//		return null;
//		
//	}
	
}