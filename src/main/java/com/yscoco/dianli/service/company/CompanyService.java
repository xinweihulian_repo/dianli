package com.yscoco.dianli.service.company;

import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import com.alibaba.fastjson.JSON;
import com.yscoco.dianli.dto.company.*;
import com.yscoco.dianli.other.cache.RedisCacheUtil;
import com.yscoco.dianli.service.comon.NosqlKeyManager;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.batise.device.device_info;
import com.hyc.smart.XWDataEntry;
import com.yscoco.dianli.common.base.Base;
import com.yscoco.dianli.common.utils.PoiUtil;
import com.yscoco.dianli.common.vo.Code;
import com.yscoco.dianli.common.vo.Message;
import com.yscoco.dianli.common.vo.PageData;
import com.yscoco.dianli.constants.YN;
import com.yscoco.dianli.dao.company.CompanyDao;
import com.yscoco.dianli.dao.company.LinesOnBranchDao;
import com.yscoco.dianli.dao.company.LinesOnMasterDao;
import com.yscoco.dianli.dao.company.MonitorPointDao;
import com.yscoco.dianli.dao.company.MonitorStationDao;
import com.yscoco.dianli.dao.member.MemberInfoDao;
import com.yscoco.dianli.dao.sys.SysUserDao;
import com.yscoco.dianli.entity.company.Company;
import com.yscoco.dianli.entity.company.LinesOnBranch;
import com.yscoco.dianli.entity.company.LinesOnMaster;
import com.yscoco.dianli.entity.company.MonitorPoint;
import com.yscoco.dianli.entity.company.MonitorStation;
import com.yscoco.dianli.entity.member.MemberInfo;
import com.yscoco.dianli.entity.sys.SysUser;
import com.yscoco.dianli.pojo.MasterModel;
import com.yscoco.dianli.pojo.PointModel;

import javax.annotation.Resource;


@Service
public class CompanyService extends Base {
	private static final Logger logger = Logger.getLogger(CompanyService.class.getName());

	@Autowired
	private CompanyDao companyDao;

	@Autowired
	private MonitorStationDao monitorStationDao;

	@Autowired
	private MonitorPointDao monitorPointDao;

	@Autowired
	private LinesOnBranchDao linesOnBranchDao;

	@Autowired
	private LinesOnMasterDao linesOnMasterDao;
	
	@Autowired
	private MonitorStationDeviceService  monitorStationDeviceService;

	@Resource
	private RedisCacheUtil redisCache;

	@Autowired
	private SysUserDao sysUserDao;
	@Autowired
	private MemberInfoDao memberInfoDao;
	@Autowired
	private LinesOnMasterService linesOnMasterService;
	
	
	public List<String> getCompanyByUserId(String userId){
		return sysUserDao.getCompanyIdsByUserId(userId);
	}
	

	public PageData<CompanyDto> listPage(String companyId, String companyName, String createBy, Integer page,
			Integer rows) {
		if (page == null) {
			page = 1;
		}
		if (rows == null) {
			rows = 10;
		}
		Map<String, Object> params = new HashMap<>();
		if (StringUtils.isNotEmpty(companyName)) {
			params.put("companyNameLike", companyName);
		}
		if (StringUtils.isNotEmpty(companyId)) {
			params.put("id", companyId);
		}

		if (StringUtils.isNotEmpty(createBy)) { // 除了admin超级管理员外 管理员只能看到自己创建的公司
			SysUser SysUser_create = this.sysUserDao.queryOne("id", createBy);
			if (SysUser_create != null && notEmpty(SysUser_create.getPermission())
					&& !SysUser_create.getPermission().equalsIgnoreCase("admin")) {
				params.put("createBy", createBy);
			}
		}
		// params.put("createBy", "2c907c825fe2963e015fe29678da0003");
		params.put("page", page);
		params.put("rows", rows);
		params.put("order", "createTime desc");

		PageData<CompanyDto> data = new PageData<CompanyDto>();
		List<CompanyDto> companyDtoList = new ArrayList<CompanyDto>();

		CompanyDto companyDto = null;

		List<MonitorStationDto> monitorStationDtoList = null;// 站点
		MonitorStationDto monitorStationDto = null;

		List<LinesOnMasterDto> linesOnMasterDtoList = null;// 母线
		LinesOnMasterDto linesOnMasterDto = null;

		List<LinesOnBranchDto> linesOnBranchDtoList = null;// 支线
		LinesOnBranchDto linesOnBranchDto = null;

		List<MonitorPointDto> monitorPointDtoList = null;// 监测点
		MonitorPointDto monitorPointDto = null;

		List<Company> dtoList = companyDao.list(params);
		for (Company company : dtoList) {

			int monitorPointCount = 0;
			monitorStationDtoList = new ArrayList<MonitorStationDto>();// 站点初始化
			linesOnMasterDtoList = new ArrayList<LinesOnMasterDto>();// 母线初始化
			linesOnBranchDtoList = new ArrayList<LinesOnBranchDto>();// 支线初始化
			monitorPointDtoList = new ArrayList<MonitorPointDto>();// 监测点初始化
			companyDto = this.companyDao.toDto(company, CompanyDto.class);
			List<MonitorStation> monitorStations = company.getMonitorStations(); // 第四步：获取监测站并装进监测站Set中
			for (MonitorStation monitorStation : monitorStations) {

				List<LinesOnMaster> linesOnMasters = monitorStation.getLinesOnMasters(); // 第三步：获取母线并装进母线Set中
				for (LinesOnMaster linesOnMaster : linesOnMasters) {

					List<LinesOnBranch> linesOnBranchs = linesOnMaster.getLinesOnBranchs(); // 第二步：获取支线并装进支线Set中
					for (LinesOnBranch linesOnBranch : linesOnBranchs) {
						List<MonitorPoint> monitorPoints = linesOnBranch.getMonitorPoints(); // 第一步：现获取站点并装进站点Set中
						monitorPointCount += monitorPoints.size();
						for (MonitorPoint monitorPoint : monitorPoints) {
							monitorPointDto = this.monitorPointDao.toDto(monitorPoint, MonitorPointDto.class);
							monitorPointDtoList.add(monitorPointDto);
						}
						linesOnBranchDto = this.linesOnBranchDao.toDto(linesOnBranch, LinesOnBranchDto.class);
						linesOnBranchDto.setMonitorPointDtos(monitorPointDtoList);
						linesOnBranchDtoList.add(linesOnBranchDto);
					}
					linesOnMasterDto = this.linesOnMasterDao.toDto(linesOnMaster, LinesOnMasterDto.class);
					linesOnMasterDto.setLinesOnBranchDtos(linesOnBranchDtoList);
					linesOnMasterDtoList.add(linesOnMasterDto);
				}
				monitorStationDto = this.monitorStationDao.toDto(monitorStation, MonitorStationDto.class);
				monitorStationDto.setLinesOnMasterDtos(linesOnMasterDtoList);
				monitorStationDtoList.add(monitorStationDto);
			}
			companyDto.setMonitorPointCount(monitorPointCount);
			companyDto.setMonitorStationDtos(monitorStationDtoList); // 第五步：监测站Set装进单个公司中
			companyDtoList.add(companyDto);
		}
		data.setList(companyDtoList);
		data.setCount(companyDao.count(params));
		return data;
	}

	public Message<CompanyDto> addOrUpdateCompany(String companyId, String companyName, String companyNameShort,
			String companyAddress, String industry, String businessType, String companySize, String mainProducts,
			String monitorStationsIds, String managerId, String addressIp) {
		Company company = null;
		if (notEmpty(companyId)) {
			company = this.companyDao.getEntity(companyId);
			company.setModifiedBy(managerId);
			company.setModifyTime(getDate());
			addOrUpdateDetails(companyName, companyNameShort, companyAddress, industry, businessType, companySize,
					mainProducts, monitorStationsIds, company);
		} else {
			company = new Company();
			company.setCreateBy(managerId);
			company.setCreateTime(getDate());
			addOrUpdateDetails(companyName, companyNameShort, companyAddress, industry, businessType, companySize,
					mainProducts, monitorStationsIds, company);
			this.companyDao.save(company);
		}

		CompanyDto dto = this.companyDao.toDto(company, CompanyDto.class);
		/*
		 * Set<MonitorStation> monitorStations = company.getMonitorStations();
		 * for (MonitorStation monitorStation : monitorStations) {
		 * monitorStationDtoList.add(this.monitorStationDao.toDto(
		 * monitorStation, MonitorStationDto.class)); }
		 * dto.setMonitorStationDtos(monitorStationDtoList);
		 */

		return new Message<CompanyDto>(Code.SUCCESS, dto);
	}

	private void addOrUpdateDetails(String companyName, String companyNameShort, String companyAddress, String industry,
			String businessType, String companySize, String mainProducts, String monitorStationsIds, Company company) {
		List<MonitorStation> monitorStationSet = null;
		if (company != null && notEmpty(companyName)) {
			company.setCompanyName(companyName);
		}
		if (company != null && notEmpty(companyNameShort)) {
			company.setCompanyNameShort(companyNameShort);
		}
		if (company != null && notEmpty(companyAddress)) {
			company.setCompanyAddress(companyAddress);
		}
		if (company != null && notEmpty(industry)) {
			company.setIndustry(industry);
		}
		if (company != null && notEmpty(businessType)) {
			company.setBusinessType(businessType);
		}
		if (company != null && notEmpty(companySize)) {
			company.setCompanySize(companySize);
		}
		if (company != null && notEmpty(mainProducts)) {
			company.setMainProducts(mainProducts);
		}
		if (company != null && notEmpty(monitorStationsIds)) {
			String[] monitorStationsIdArr = monitorStationsIds.split(",");
			if (monitorStationsIdArr.length > 0) {
				monitorStationSet = new ArrayList<MonitorStation>();
			}
			for (String id : monitorStationsIdArr) {
				MonitorStation monitorStation = this.monitorStationDao.getEntity(id);
				monitorStationSet.add(monitorStation);
			}
			company.setMonitorStations(monitorStationSet);
		}
	}

	public Message<?> delete(String companyIds, String managerId, String addressIp) {
		String[] ids = companyIds.split(",");
		for (String id : ids) {
			Company company = this.companyDao.getEntity(id);
			List<MonitorStation> monitorStations = company.getMonitorStations();
			for (MonitorStation monitorStation : monitorStations) {
				List<LinesOnMaster> linesOnMasters = monitorStation.getLinesOnMasters();
				for (LinesOnMaster linesOnMaster : linesOnMasters) {
					List<LinesOnBranch> linesOnBranchs = linesOnMaster.getLinesOnBranchs();
					// 1、删除支线操作
					for (LinesOnBranch linesOnBranch : linesOnBranchs) {
						// 1.1、去除支线上的人员讯息
						MemberInfo memberInfo = linesOnBranch.getMemberInfo();
						if (memberInfo != null) {
							String linesOnBranchId_my = memberInfo.getLinesOnBranchId();
							linesOnBranchId_my = linesOnBranchId_my.replace(id + ",", "");

							memberInfo.setLinesOnBranchId(linesOnBranchId_my);// 挑选出来
							linesOnBranch.setMemberInfo(null);
							memberInfoDao.delete(memberInfo);
						}
						// 1.2、删除监测点信息
						List<MonitorPoint> monitorPoints = linesOnBranch.getMonitorPoints();
						this.monitorPointDao.deleteMonitorPoints(monitorPoints);
						// 1.3、删除支线
						linesOnBranchDao.delete(linesOnBranch);
					}

					// 2.0、清除被删除母线的上下段母线id信息才能删除当前母线的信息：
					// 2.1、判断当前母线不为空并且当前母线上一段母线存在或者下一段母线存在的话就需要先把上下母线关联的id删除,
					if (linesOnMaster != null && (notEmpty(linesOnMaster.getPreMasterId())
							|| notEmpty(linesOnMaster.getNextMasterId()))) {
						// 如果有上一段母线则取出上一段母线并把上一段母线的下一段母线id删除
						if (notEmpty(linesOnMaster.getPreMasterId())) {
							LinesOnMaster preLinesOnMaster = this.linesOnMasterDao
									.getEntity(linesOnMaster.getPreMasterId());
							if (preLinesOnMaster != null && notEmpty(preLinesOnMaster.getNextMasterId())) {
								preLinesOnMaster.setNextMasterId(null);
							}
						}
						// 2.2、如果有下一段母线则取出下一段母线并把下一段母线的上一段母线id删除
						if (notEmpty(linesOnMaster.getNextMasterId())) {
							LinesOnMaster nextLinesOnMaster = this.linesOnMasterDao
									.getEntity(linesOnMaster.getNextMasterId());
							if (nextLinesOnMaster != null && notEmpty(nextLinesOnMaster.getPreMasterId())) {
								nextLinesOnMaster.setPreMasterId(null);
							}
						}
					}

					// 最后把当前母线实体删除
					linesOnMasterDao.delete(linesOnMaster);
				} // 母线
				monitorStationDao.delete(monitorStation);
			} // 监测站
			 List<String> queryAllMemberIds = memberInfoDao.queryAllMemberIds(id);
			 if(!queryAllMemberIds.isEmpty()){
				 	for(String menberIds :queryAllMemberIds){
				 		 memberInfoDao.delete(menberIds);
				 	}
			 }
			this.companyDao.delete(company);
		}
		return Message.success(companyIds);
	}

	public CompanyDto queryStaticApp(String companyId, String loginMemberId) {
		Company company = this.companyDao.getEntity(companyId);
		List<MonitorStationDto> monitorStationDtoList = null;// 站点
		MonitorStationDto monitorStationDto = null;

		List<LinesOnMasterDto> linesOnMasterDtoList = null;// 母线
		LinesOnMasterDto linesOnMasterDto = null;

		List<LinesOnBranchDto> linesOnBranchDtoList = null;// 支线
		LinesOnBranchDto linesOnBranchDto = null;

		List<MonitorPointDto> monitorPointDtoByDtoList = null;// 监测点
		MonitorPointDto monitorPointDto = null;

		CompanyDto companyDto = new CompanyDto();
		companyDto.setId(company.getId());
		companyDto.setCompanyName(company.getCompanyName());
		companyDto.setCompanyNameShort(company.getCompanyNameShort());
		companyDto.setCompanyAddress(company.getCompanyAddress());
		companyDto.setIndustry(company.getIndustry());
		companyDto.setBusinessType(company.getBusinessType());
		companyDto.setCompanySize(company.getCompanySize());
		companyDto.setMainProducts(company.getMainProducts());

		// 温度和过电压记数
		int tempCount = 0; // 温度 统计总数初始化
		int elecCount = 0; // 过电压 统计总数初始化
		int polarElecCount = 0; // 极地短路 统计总数初始化

		int jiediProblemCount = 0; // 接地故障数量
		int tempProblemCount = 0; // 温度故障数量
		int elecProblemCount = 0; // 过电压保护器数量

		monitorStationDtoList = new ArrayList<MonitorStationDto>();// 站点初始化
		List<MonitorStation> monitorStations = null;
		MemberInfo memberInfo = this.memberInfoDao.queryOne("id", loginMemberId);
		if (memberInfo != null) {
			String monitorStationIds = memberInfo.getMonitorStationId();
			if (monitorStationIds == null) {
				return null; // 如果巡检员没有负责的监测站则返回，否则空指针异常
			}
			String[] monitorStationIdArr = monitorStationIds.split(",");
			monitorStations = new ArrayList<>();
			for (String str : monitorStationIdArr) {
				MonitorStation queryOne = this.monitorStationDao.queryOne("id", str);
				monitorStations.add(queryOne);
			}
		} else {
			monitorStations = company.getMonitorStations(); // 第四步：获取监测站并装进监测站Set中
		}
		for (MonitorStation monitorStation : monitorStations) {

			linesOnMasterDtoList = new ArrayList<LinesOnMasterDto>();// 母线初始化
			List<LinesOnMaster> linesOnMasters = monitorStation.getLinesOnMasters(); // 第三步：获取母线并装进母线Set中
			for (LinesOnMaster linesOnMaster : linesOnMasters) {

				linesOnBranchDtoList = new ArrayList<LinesOnBranchDto>();// 支线初始化
				List<LinesOnBranch> linesOnBranchs = linesOnMaster.getLinesOnBranchs(); // 第二步：获取支线并装进支线Set中
				for (LinesOnBranch linesOnBranch : linesOnBranchs) {

					monitorPointDtoByDtoList = new ArrayList<MonitorPointDto>();// 监测点初始化
																				// （温度）
					List<MonitorPoint> monitorPoints = linesOnBranch.getMonitorPoints(); // 第一步：现获取站点并装进站点Set中
					for (MonitorPoint monitorPoint : monitorPoints) {
						monitorPointDto = this.monitorPointDao.toDto(monitorPoint, MonitorPointDto.class);
						if (monitorPointDto.getHitchType().equalsIgnoreCase("温度采集器")) {
							tempCount++; // 温度 统计总数
						} else if (monitorPointDto.getHitchType().equalsIgnoreCase("过电压保护器")) {
							elecCount++; // 过电压 统计总数
						} else if (monitorPointDto.getHitchType().equalsIgnoreCase("极地短路")) {
							polarElecCount++; // 极地短路 统计总数
						}
					}
					linesOnBranchDto = this.linesOnBranchDao.toDto(linesOnBranch, LinesOnBranchDto.class);
					linesOnBranchDto.setMonitorPointDtos(monitorPointDtoByDtoList);
					linesOnBranchDtoList.add(linesOnBranchDto);
				}
				linesOnMasterDto = this.linesOnMasterDao.toDto(linesOnMaster, LinesOnMasterDto.class);
				linesOnMasterDto.setLinesOnBranchDtos(linesOnBranchDtoList);
				linesOnMasterDtoList.add(linesOnMasterDto);
			}

			monitorStationDto = this.monitorStationDao.toDto(monitorStation, MonitorStationDto.class);
			monitorStationDto.setLinesOnMasterDtos(linesOnMasterDtoList);
			monitorStationDtoList.add(monitorStationDto);
		}
		companyDto.setMonitorStationDtos(monitorStationDtoList); // 第五步：监测站Set装进单个公司中

		String memberId = null;
		if (StringUtils.isNotEmpty(loginMemberId)) {
			memberId = loginMemberId;
		}

		// 获取母线标识Id集合:得到故障类型消息
		List<LinesOnMasterDto> linesOnMasterList = linesOnMasterService.queryLinesOnMasterIds(null, companyId, null,
				null, null, memberId);

		List<String> devidsList = new ArrayList<>();
		for (LinesOnMasterDto dto : linesOnMasterList) {
			devidsList.add(dto.getMonitorFlag());
		}
		if (devidsList != null && devidsList.size() > 0) {
//			List<FaultMsg> faultMsgList = XWDataEntry.getFaultMessage(devidsList, Boolean.FALSE);
//			for (FaultMsg faultMsg : faultMsgList) {
//				if (faultMsg.getType() == 1) {
//					tempProblemCount++;
//				} else if (faultMsg.getType() == 2) {
//					elecProblemCount++;
//				} else if (faultMsg.getType() == 3) {
//					jiediProblemCount++;
//
//				}
//			}
			//{devid=ERIC0800180900000001, phase=2, isSolve=false, fault_type=14, id=216, timestamp=1540265741000}
			 List<Map<String, Object>> faultMessage = XWDataEntry.getFaultMessage(devidsList, Boolean.FALSE);
//			      for (Map<String, Object> map : faultMessage) {
//			    	  if (faultMessage.getType() == 1) {
//							tempProblemCount++;
//						} else if (faultMsg.getType() == 2) {
//							elecProblemCount++;
//						} else if (faultMsg.getType() == 3) {
//							jiediProblemCount++;
//		
//						}
//					}
			if (faultMessage.size() == 0) {
				companyDto.setFlagProblem(YN.N);
			} else {
				companyDto.setFlagProblem(YN.Y);
			}
			companyDto.setProblemCount(faultMessage.size());// 故障信息总数
		}

		companyDto.setJiediProblemCount(jiediProblemCount);
		companyDto.setTempProblemCount(tempProblemCount);
		companyDto.setElecProblemCount(elecProblemCount);

		companyDto.setMonitorStationCount(monitorStationDtoList.size());// 监测站总数
		companyDto.setLinesOnMasterAndBranchCount(linesOnMasterDtoList.size() + linesOnBranchDtoList.size());// 母线加上支线的总数
		companyDto.setTempPointCount(tempCount);// 温度采集器数量
		companyDto.setElecCount(elecCount);// 过电压保护器数量
		companyDto.setPolarElecCount(polarElecCount);// 极地电压保护器数量
		return companyDto;
	}

	/**
	 * 根据用户id查询所有所有负责公司的Id
	 * 
	 * @param createBy
	 * @return
	 */
	public List<String> queryAllCompanyId(String createBy) { // 根据用户id查询所有所有负责公司的Id
		return this.companyDao.queryAllCompanyId(createBy);
	}
	
	// companyId,createBy,type
	public List<CompanyDto> queryStaticThree(String check_companyId, String createBy, String type) {
		List<CompanyDto> result = new ArrayList<CompanyDto>();
		List<String> queryAllCompanyId = new ArrayList<String>();
		;
		if (notEmpty(check_companyId)) {
			queryAllCompanyId.add(check_companyId);
		} else {
			queryAllCompanyId = this.queryAllCompanyId(createBy);
		}

		List<MonitorStationDto> monitorStationDtoList = null;// 站点
		MonitorStationDto monitorStationDto = null;

		List<LinesOnMasterDto> linesOnMasterDtoList = null;// 母线
		LinesOnMasterDto linesOnMasterDto = null;

		List<LinesOnBranchDto> linesOnBranchDtoList = null;// 支线
		LinesOnBranchDto linesOnBranchDto = null;

		List<MonitorPointDto> monitorPointDtoList = null;// 监测点
		MonitorPointDto monitorPointDto = null;

		List<LinesOnMaster> linesOnMasterList = null;
		List<LinesOnMasterDto> linesOnMasterList_result = null;

		CompanyDto companyDto = null;
		for (String companyId : queryAllCompanyId) {

			companyDto = new CompanyDto();
			Company company = this.companyDao.getEntity(companyId);

			monitorStationDtoList = new ArrayList<MonitorStationDto>();// 站点初始化

			List<MonitorStation> monitorStations = new ArrayList<>();

			MemberInfo memberInfo = this.memberInfoDao.queryOne("id", createBy);
			if (memberInfo != null) {
				// 巡检员只查询所负责的监测站
				String monitorStationIds = memberInfo.getMonitorStationId();
				String[] monitorStationIdArr = monitorStationIds.split(",");
				for (String str : monitorStationIdArr) {
					MonitorStation queryOne = this.monitorStationDao.queryOne("id", str);
					monitorStations.add(queryOne);
				}
			} else {
				// 如果是公司管理员 则查出该公司下的所有监测站点
				monitorStations = company.getMonitorStations(); // 第四步：获取监测站并装进监测站Set中
			}
			
//****************************************
			List<String> devids = new ArrayList<>();
			monitorStations.forEach(mm->{
							mm.getLinesOnMasters().forEach(lm->{
								lm.getLinesOnBranchs().forEach(lb->{
									lb.getMonitorPoints().forEach(pm->{
										String monitorFlag = pm.getMonitorFlag();
										devids.add(monitorFlag);
									});
								});
							});			
			});
			long start = System.currentTimeMillis();
			List<device_info> deviceInfoIsOnline = XWDataEntry.getDeviceInfo(devids);
			long end = System.currentTimeMillis();
			logger.info("XWDataEntry.getDeviceInfo(devids).v1:" + (end - start) + "ms");
//****************************************				
			
			for (MonitorStation monitorStation : monitorStations) {

				linesOnMasterList = new ArrayList<LinesOnMaster>();// 母线排序

				linesOnMasterDtoList = new ArrayList<LinesOnMasterDto>();// 母线初始化
				List<LinesOnMaster> linesOnMasters = monitorStation.getLinesOnMasters(); // 第三步：获取母线并装进母线Set中
				for (LinesOnMaster linesOnMaster : linesOnMasters) {
					// 下拉框这里需要满足母线具备设备标识才能有数据
					if (empty(linesOnMaster.getMonitorFlag())) {
						continue;
					}

					if (notEmpty(type) && type.equalsIgnoreCase("5")) {
						linesOnBranchDtoList = new ArrayList<LinesOnBranchDto>();// 支线初始化
						List<LinesOnBranch> linesOnBranchs = linesOnMaster.getLinesOnBranchs(); // 第二步：获取支线并装进支线Set中
						for (LinesOnBranch linesOnBranch : linesOnBranchs) {

							monitorPointDtoList = new ArrayList<MonitorPointDto>();// 监测点初始化
							List<MonitorPoint> monitorPoints = linesOnBranch.getMonitorPoints(); // 第一步：现获取站点并装进站点Set中
							for (MonitorPoint monitorPoint : monitorPoints) {
								// 下拉框这里需要满足母线具备设备标识才能有数据
								if (empty(monitorPoint.getMonitorFlag())) {
									continue;
								}
								monitorPointDto = new MonitorPointDto();
								monitorPointDto.setId(monitorPoint.getId());
								monitorPointDto.setPointName(monitorPoint.getPointName());
								monitorPointDto.setMonitorFlag(monitorPoint.getMonitorFlag());
								monitorPointDtoList.add(monitorPointDto);
							}

							if (monitorPointDtoList.size() == 0) {
								continue;
							} // 过滤条件： 如果该支线下没有合格监测点就不显示在下拉框中

							linesOnBranchDto = new LinesOnBranchDto();
							linesOnBranchDto.setId(linesOnBranch.getId());
							linesOnBranchDto.setId(linesOnBranch.getId());
							linesOnBranchDto.setBranchName(linesOnBranch.getBranchName());
							linesOnBranchDto.setMonitorPointDtos(monitorPointDtoList);
							linesOnBranchDtoList.add(linesOnBranchDto);
						}
						if (linesOnBranchDtoList != null && linesOnBranchDtoList.size() == 0) {
							continue;
						} // 过滤条件： 如果该母线下没有合格支线就不显示在下拉框中
					}

					linesOnMasterDto = new LinesOnMasterDto();
					linesOnMasterDto.setId(linesOnMaster.getId());
					linesOnMasterDto.setLinesOnBranchDtos(linesOnBranchDtoList);
					linesOnMasterDto.setMasterName(linesOnMaster.getMasterName());
					linesOnMasterDto.setMonitorFlag(linesOnMaster.getMonitorFlag());
					linesOnMasterDto.setNextMasterId(linesOnMaster.getNextMasterId());
					linesOnMasterDto.setRelatedType(linesOnMaster.getRelatedType());
					linesOnMasterDtoList.add(linesOnMasterDto);
				}

				if (linesOnMasterDtoList.size() == 0) {
					continue;
				} // 过滤条件： 如果该监测站下没有合格母线就不显示在下拉框中
				monitorStationDto = new MonitorStationDto();
				monitorStationDto.setLinesOnMasterDtos(linesOnMasterDtoList);
				monitorStationDto.setStationName(monitorStation.getStationName());
				monitorStationDto.setId(monitorStation.getId());
				monitorStationDto.setCityCode(monitorStation.getCityCode());

				// 为排序做处理
				LinesOnMaster nextMaster_ = null;
				for (LinesOnMaster master_old : linesOnMasters) { // MonitorStationService
																	// 179行 同样使用
					if (master_old != null && notEmpty(master_old.getPreMasterId())) {
						continue;
					} else if (master_old != null && empty(master_old.getPreMasterId())) {
						linesOnMasterList.add(master_old);// 先将首部装进去
						nextMaster_ = this.linesOnMasterDao.getEntity(master_old.getNextMasterId());
						if (nextMaster_ != null) {
							linesOnMasterList.add(nextMaster_);// 装首部的下一段母线
						}
						while (nextMaster_ != null && notEmpty(nextMaster_.getNextMasterId())) { // 下一段母线不为空并且下一段母线一直有下一段母线id,
							nextMaster_ = this.linesOnMasterDao.getEntity(nextMaster_.getNextMasterId());
							if (nextMaster_ != null) {
								linesOnMasterList.add(nextMaster_);
							}
						}
						if (linesOnMasterList.size() == linesOnMasters.size()) {
							break;
						}
					}
				}

				// 给所有参加排序的母线和非排序母线指定支线
				linesOnMasterList_result = new ArrayList<LinesOnMasterDto>();// 母线初始化
				for (LinesOnMaster linesOnMaster_new : linesOnMasterList) {

					// 下拉框这里需要满足母线具备设备标识才能有数据
					if (empty(linesOnMaster_new.getMonitorFlag())) {
						continue;
					}

					linesOnMasterDto = new LinesOnMasterDto();

					linesOnBranchDtoList = new ArrayList<LinesOnBranchDto>();// 支线初始化
					List<LinesOnBranch> linesOnBranchs = linesOnMaster_new.getLinesOnBranchs(); // 第二步：获取支线并装进支线Set中
					for (LinesOnBranch linesOnBranch : linesOnBranchs) {

						monitorPointDtoList = new ArrayList<MonitorPointDto>();// 监测点初始化
						List<MonitorPoint> monitorPoints = linesOnBranch.getMonitorPoints(); // 第一步：现获取站点并装进站点Set中
						for (MonitorPoint monitorPoint : monitorPoints) {
							monitorPointDto = new MonitorPointDto();
							monitorPointDto.setId(monitorPoint.getId());
							monitorPointDto.setPointName(monitorPoint.getPointName());
							monitorPointDto.setMonitorFlag(monitorPoint.getMonitorFlag());
							monitorPointDto.setHitchType(monitorPoint.getHitchType());
							monitorPointDto.setType(monitorPoint.getType());
							devids.add(monitorPoint.getMonitorFlag());
							if(deviceInfoIsOnline!=null&&deviceInfoIsOnline.size()>0){
								for(int i=0;i<deviceInfoIsOnline.size();i++){
									if(deviceInfoIsOnline.get(i).getDevid().equals(monitorPoint.getMonitorFlag())){
										monitorPointDto.setOnline(deviceInfoIsOnline.get(i).isOnline());
									}
								}
							}else{
								monitorPointDto.setOnline(false);
							}
							
							monitorPointDtoList.add(monitorPointDto);
						}
						linesOnBranchDto = new LinesOnBranchDto();
						linesOnBranchDto.setId(linesOnBranch.getId());
						linesOnBranchDto.setId(linesOnBranch.getId());
						linesOnBranchDto.setBranchName(linesOnBranch.getBranchName());
						linesOnBranchDto.setMonitorPointDtos(monitorPointDtoList);
						linesOnBranchDtoList.add(linesOnBranchDto);
					}

					linesOnMasterDto.setLinesOnBranchDtos(linesOnBranchDtoList);
					linesOnMasterDto.setId(linesOnMaster_new.getId());
					linesOnMasterDto.setMasterName(linesOnMaster_new.getMasterName());
					linesOnMasterDto.setMonitorFlag(linesOnMaster_new.getMonitorFlag());
					linesOnMasterDto.setNextMasterId(linesOnMaster_new.getNextMasterId());
					linesOnMasterList_result.add(linesOnMasterDto);
				}
				monitorStationDto.setLinesOnMasterSortList(linesOnMasterList_result);

				monitorStationDtoList.add(monitorStationDto);
			}

			if (monitorStationDtoList.size() == 0) {
				continue;
			} // 过滤条件： 如果该公司下没有合格监测站就不显示在下拉框中
			companyDto.setMonitorStationDtos(monitorStationDtoList); // 第五步：监测站Set装进单个公司中

			companyDto.setId(company.getId());
			companyDto.setCompanyName(company.getCompanyName());

			// 做一个简单过滤

			result.add(companyDto);
		}
		return result;
	}

	public List<CompanyDto> queryStaticThree2(String check_companyId, String createBy, String type) {
		List<CompanyDto> result = new ArrayList<CompanyDto>();
		List<String> queryAllCompanyId = new ArrayList<String>();
		;
		if (notEmpty(check_companyId)) {
			queryAllCompanyId.add(check_companyId);
		} else {
			queryAllCompanyId = this.queryAllCompanyId(createBy);
		}

		List<MonitorStationDto> monitorStationDtoList = null;// 站点
		MonitorStationDto monitorStationDto = null;

		List<LinesOnMasterDto> linesOnMasterDtoList = null;// 母线
		LinesOnMasterDto linesOnMasterDto = null;

		List<LinesOnBranchDto> linesOnBranchDtoList = null;// 支线
		LinesOnBranchDto linesOnBranchDto = null;

		List<MonitorPointDto> monitorPointDtoList = null;// 监测点
		MonitorPointDto monitorPointDto = null;

		List<LinesOnMaster> linesOnMasterList = null;
		List<LinesOnMasterDto> linesOnMasterList_result = null;

		CompanyDto companyDto = null;
		for (String companyId : queryAllCompanyId) {

			companyDto = new CompanyDto();
			Company company = this.companyDao.getEntity(companyId);

			monitorStationDtoList = new ArrayList<MonitorStationDto>();// 站点初始化

			List<MonitorStation> monitorStations = new ArrayList<>();

			MemberInfo memberInfo = this.memberInfoDao.queryOne("id", createBy);
			if (memberInfo != null) {
				// 巡检员只查询所负责的监测站
				String monitorStationIds = memberInfo.getMonitorStationId();
				String[] monitorStationIdArr = monitorStationIds.split(",");
				for (String str : monitorStationIdArr) {
					MonitorStation queryOne = this.monitorStationDao.queryOne("id", str);
					monitorStations.add(queryOne);
				}
			} else {
				// 如果是公司管理员 则查出该公司下的所有监测站点
				monitorStations = company.getMonitorStations(); // 第四步：获取监测站并装进监测站Set中
			}

			for (MonitorStation monitorStation : monitorStations) {

				linesOnMasterList = new ArrayList<LinesOnMaster>();// 母线排序

				linesOnMasterDtoList = new ArrayList<LinesOnMasterDto>();// 母线初始化
				List<LinesOnMaster> linesOnMasters = monitorStation.getLinesOnMasters(); // 第三步：获取母线并装进母线Set中
				for (LinesOnMaster linesOnMaster : linesOnMasters) {
					// 下拉框这里需要满足母线具备设备标识才能有数据
					if (empty(linesOnMaster.getMonitorFlag())) {
						continue;
					}

					if (notEmpty(type) && type.equalsIgnoreCase("5")) {
						linesOnBranchDtoList = new ArrayList<LinesOnBranchDto>();// 支线初始化
						List<LinesOnBranch> linesOnBranchs = linesOnMaster.getLinesOnBranchs(); // 第二步：获取支线并装进支线Set中
						for (LinesOnBranch linesOnBranch : linesOnBranchs) {

							monitorPointDtoList = new ArrayList<MonitorPointDto>();// 监测点初始化
							List<MonitorPoint> monitorPoints = linesOnBranch.getMonitorPoints(); // 第一步：现获取站点并装进站点Set中
							for (MonitorPoint monitorPoint : monitorPoints) {
								// 下拉框这里需要满足母线具备设备标识才能有数据
								if (empty(monitorPoint.getMonitorFlag())) {
									continue;
								}
								monitorPointDto = new MonitorPointDto();
								monitorPointDto.setId(monitorPoint.getId());
								monitorPointDto.setPointName(monitorPoint.getPointName());
								monitorPointDto.setMonitorFlag(monitorPoint.getMonitorFlag());
								monitorPointDtoList.add(monitorPointDto);
							}

							if (monitorPointDtoList.size() == 0) {
								continue;
							} // 过滤条件： 如果该支线下没有合格监测点就不显示在下拉框中

							linesOnBranchDto = new LinesOnBranchDto();
							linesOnBranchDto.setId(linesOnBranch.getId());
							linesOnBranchDto.setId(linesOnBranch.getId());
							linesOnBranchDto.setBranchName(linesOnBranch.getBranchName());
							linesOnBranchDto.setMonitorPointDtos(monitorPointDtoList);
							linesOnBranchDtoList.add(linesOnBranchDto);
						}
						if (linesOnBranchDtoList != null && linesOnBranchDtoList.size() == 0) {
							continue;
						} // 过滤条件： 如果该母线下没有合格支线就不显示在下拉框中
					}

					linesOnMasterDto = new LinesOnMasterDto();
					linesOnMasterDto.setId(linesOnMaster.getId());
					linesOnMasterDto.setLinesOnBranchDtos(linesOnBranchDtoList);
					linesOnMasterDto.setMasterName(linesOnMaster.getMasterName());
					linesOnMasterDto.setMonitorFlag(linesOnMaster.getMonitorFlag());
					linesOnMasterDto.setNextMasterId(linesOnMaster.getNextMasterId());
					linesOnMasterDto.setRelatedType(linesOnMaster.getRelatedType());
					linesOnMasterDtoList.add(linesOnMasterDto);
				}

				if (linesOnMasterDtoList.size() == 0) {
					continue;
				} // 过滤条件： 如果该监测站下没有合格母线就不显示在下拉框中
				monitorStationDto = new MonitorStationDto();
				monitorStationDto.setLinesOnMasterDtos(linesOnMasterDtoList);
				monitorStationDto.setStationName(monitorStation.getStationName());
				monitorStationDto.setId(monitorStation.getId());
				monitorStationDto.setCityCode(monitorStation.getCityCode());

				// 为排序做处理
				LinesOnMaster nextMaster_ = null;
				for (LinesOnMaster master_old : linesOnMasters) { // MonitorStationService
					// 179行 同样使用
					if (master_old != null && notEmpty(master_old.getPreMasterId())) {
						continue;
					} else if (master_old != null && empty(master_old.getPreMasterId())) {
						linesOnMasterList.add(master_old);// 先将首部装进去
						nextMaster_ = this.linesOnMasterDao.getEntity(master_old.getNextMasterId());
						if (nextMaster_ != null) {
							linesOnMasterList.add(nextMaster_);// 装首部的下一段母线
						}
						while (nextMaster_ != null && notEmpty(nextMaster_.getNextMasterId())) { // 下一段母线不为空并且下一段母线一直有下一段母线id,
							nextMaster_ = this.linesOnMasterDao.getEntity(nextMaster_.getNextMasterId());
							if (nextMaster_ != null) {
								linesOnMasterList.add(nextMaster_);
							}
						}
						if (linesOnMasterList.size() == linesOnMasters.size()) {
							break;
						}
					}
				}

				// 给所有参加排序的母线和非排序母线指定支线
				linesOnMasterList_result = new ArrayList<LinesOnMasterDto>();// 母线初始化
				for (LinesOnMaster linesOnMaster_new : linesOnMasterList) {

					// 下拉框这里需要满足母线具备设备标识才能有数据
					if (empty(linesOnMaster_new.getMonitorFlag())) {
						continue;
					}

					linesOnMasterDto = new LinesOnMasterDto();

					linesOnBranchDtoList = new ArrayList<LinesOnBranchDto>();// 支线初始化
					List<LinesOnBranch> linesOnBranchs = linesOnMaster_new.getLinesOnBranchs(); // 第二步：获取支线并装进支线Set中
					for (LinesOnBranch linesOnBranch : linesOnBranchs) {

						monitorPointDtoList = new ArrayList<MonitorPointDto>();// 监测点初始化
						List<MonitorPoint> monitorPoints = linesOnBranch.getMonitorPoints(); // 第一步：现获取站点并装进站点Set中
						for (MonitorPoint monitorPoint : monitorPoints) {
							monitorPointDto = new MonitorPointDto();
							monitorPointDto.setId(monitorPoint.getId());
							monitorPointDto.setPointName(monitorPoint.getPointName());
							monitorPointDto.setMonitorFlag(monitorPoint.getMonitorFlag());
							monitorPointDto.setHitchType(monitorPoint.getHitchType());
							monitorPointDto.setType(monitorPoint.getType());

							monitorPointDtoList.add(monitorPointDto);
						}
						linesOnBranchDto = new LinesOnBranchDto();
						linesOnBranchDto.setId(linesOnBranch.getId());
						linesOnBranchDto.setId(linesOnBranch.getId());
						linesOnBranchDto.setBranchName(linesOnBranch.getBranchName());
						linesOnBranchDto.setMonitorPointDtos(monitorPointDtoList);
						linesOnBranchDtoList.add(linesOnBranchDto);
					}

					linesOnMasterDto.setLinesOnBranchDtos(linesOnBranchDtoList);
					linesOnMasterDto.setId(linesOnMaster_new.getId());
					linesOnMasterDto.setMasterName(linesOnMaster_new.getMasterName());
					linesOnMasterDto.setMonitorFlag(linesOnMaster_new.getMonitorFlag());
					linesOnMasterDto.setNextMasterId(linesOnMaster_new.getNextMasterId());
					linesOnMasterList_result.add(linesOnMasterDto);
				}
				monitorStationDto.setLinesOnMasterSortList(linesOnMasterList_result);

				monitorStationDtoList.add(monitorStationDto);
			}

			if (monitorStationDtoList.size() == 0) {
				continue;
			} // 过滤条件： 如果该公司下没有合格监测站就不显示在下拉框中
			companyDto.setMonitorStationDtos(monitorStationDtoList); // 第五步：监测站Set装进单个公司中

			companyDto.setId(company.getId());
			companyDto.setCompanyName(company.getCompanyName());

			// 做一个简单过滤

			result.add(companyDto);
		}
		return result;
	}


	/**
	 * -------------------------------------------------------------------------
	 * --------------------------------------------------------------------
	 * -------------------------------------------------------------------------
	 * --------------------------------------------------------------------
	 */
	public PageData<CompanyDto> getCompanylistPage(String companyId, String companyName, String createBy, Integer page,
			Integer rows) {

		//System.out.println("getCompanylistPage:→" + "page:" + page + "\t" + "rows:" + rows);
		if (page == null) {
			page = 1;
		}
		if (rows == null) {
			rows = 10;
		}
		Map<String, Object> params = new HashMap<>();
		if (StringUtils.isNotEmpty(companyName)) {
			params.put("companyNameLike", companyName);
		}
		if (StringUtils.isNotEmpty(companyId)) {
			params.put("id", companyId);
		}

		if (StringUtils.isNotEmpty(createBy)) { // 除了admin超级管理员外 管理员只能看到自己创建的公司
			SysUser SysUser_create = this.sysUserDao.queryOne("id", createBy);
			if (SysUser_create != null && notEmpty(SysUser_create.getPermission())
					&& !SysUser_create.getPermission().equalsIgnoreCase("admin")) {
				params.put("createBy", createBy);
			}
		}
		// params.put("createBy", "2c907c825fe2963e015fe29678da0003");
		params.put("page", page);
		params.put("rows", rows);
		params.put("order", "createTime desc");
		PageData<CompanyDto> data = new PageData<CompanyDto>();

		List<CompanyDto> companyDtoList = new ArrayList<CompanyDto>();

		CompanyDto companyDto = null;

		List<Company> dtoList = companyDao.listPage(params);

		if (getCompanyInnerCache(params, data, dtoList)) return data;

		//	System.out.println("Company数量:" + dtoList.size() + JSON.toJSONString(dtoList));
		for (Company company : dtoList) {

			int monitorPointCount = 0;

			companyDto = this.companyDao.toDto(company, CompanyDto.class);
			List<MonitorStation> monitorStations = company.getMonitorStations(); // 第四步：获取监测站并装进监测站Set中

			for (MonitorStation monitorStation : monitorStations) {

				List<LinesOnMaster> linesOnMasters = monitorStation.getLinesOnMasters(); // 第三步：获取母线并装进母线Set中
				for (LinesOnMaster linesOnMaster : linesOnMasters) {

					List<LinesOnBranch> linesOnBranchs = linesOnMaster.getLinesOnBranchs(); // 第二步：获取支线并装进支线Set中
					for (LinesOnBranch linesOnBranch : linesOnBranchs) {
						List<MonitorPoint> monitorPoints = linesOnBranch.getMonitorPoints(); // 第一步：现获取站点并装进站点Set中
						monitorPointCount += monitorPoints.size();
					}
				}
				if(monitorStation.getStationType().equals("2")||monitorStation.getStationType().equals("3")){
					monitorPointCount += monitorStationDeviceService.getByStationId(monitorStation.getId()).size();
				}
			}

			companyDto.setMonitorPointCount(monitorPointCount);
			companyDtoList.add(companyDto);
		}
		data.setList(companyDtoList);
		data.setCount(companyDao.count(params));
		return data;
	}

	private boolean getCompanyInnerCache(Map<String, Object> params, PageData<CompanyDto> data, List<Company> dtoList) {
		String companyDtoListJson = redisCache.get(NosqlKeyManager.CompanyList);
		if(StringUtils.isNotBlank(companyDtoListJson)){
			List<CompanyDto> companyDtoListRes = new ArrayList<>();
			List<CompanyDto> 	companyDtoLists = JSON.parseArray(companyDtoListJson, CompanyDto.class);
			for (int i = 0; i < dtoList.size(); i++) {
				for (int i1 = 0; i1 < companyDtoLists.size(); i1++) {
					if(dtoList.get(i).getId().equals(companyDtoLists.get(i1).getId())){
						companyDtoListRes.add(companyDtoLists.get(i1));
						continue;
					}
				}
			}
			data.setList(companyDtoListRes);
			data.setCount(companyDao.count(params));
			return true;
		}
		return false;
	}

	public List<MasterModel> queryAllMasterFlag(String companyId) {
		List<MasterModel> queryAllMasterFlag = companyDao.queryAllMasterFlag(companyId);
		return queryAllMasterFlag;
	}

	public List<PointModel> queryAllPointFlag(String companyId) {
		List<PointModel> queryAllPointFlag = companyDao.queryAllPointFlag(companyId);
		return queryAllPointFlag;
	}

	/**
	 * 监测设备位置代码转换
	 * 
	 * @param position
	 *            字符串"0"-"12"
	 * @return
	 */
	private String getConvertPositionName(String position) {
		switch (position) {
		case "0":
			position = "母排A相";
			break;
		case "1":
			position = "母排B相";
			break;
		case "2":
			position = "母排C相";
			break;
		case "3":
			position = "上动触头A相";
			break;
		case "4":
			position = "上动触头B相";
			break;
		case "5":
			position = "上动触头C相";
			break;
		case "6":
			position = "上静触头A相";
			break;
		case "7":
			position = "上静触头B相";
			break;
		case "8":
			position = "上静触头C相";
			break;
		case "9":
			position = "下动触头A相";
			break;
		case "10":
			position = "下动触头B相";
			break;
		case "11":
			position = "下动触头C相";
			break;
		case "12":
			position = "下静触头A相";
			break;
		case "13":
			position = "下静触头B相";
			break;
		case "14":
			position = "下静触头C相";
			break;
		case "15":
			position = "三相一体保护器";
			break;
		case "16":
			position = "保护器A相";
			break;
		case "17":
			position = "保护器B相";
			break;
		case "18":
			position = "保护器C相";
			break;
		default:
			position = "未知";
			break;
		}

		return position;
	}

	/**
	 * 巡检员登陆获取统计数据
	 * 
	 * @param monitorStationId
	 * @param masterId
	 * @param masterFlag
	 * @param voltageFlag
	 * @param temperFlag
	 * @param timeStart_
	 * @param timeEnd_
	 * @param mapData
	 * @param masterList
	 *            根据公司id查询下面所有母线设备标识list
	 * @param pointList
	 *            根据公司id查询下面监测点设备标识list
	 * @param responsibleMasterList
	 *            所负责母线设备标识list
	 * @param responsiblePointList
	 *            所负责监测点设备标识list
	 * @param member
	 * @return
	 */
	public Map<String, Object> memberGetStatisticsData(String monitorStationId, String masterId, String masterFlag,
			String voltageFlag, String temperFlag, long timeStart_, long timeEnd_, Map<String, Object> mapData,
			List<MasterModel> masterList, List<PointModel> pointList, List<String> responsibleMasterList,
			List<String> responsiblePointList, MemberInfo member) {
		// 如果是巡检员登陆后台，则获取所负责的监测站id
		if (StringUtils.isBlank(monitorStationId)) {
			// 如果没有进行监测站筛选
			monitorStationId = member.getMonitorStationId();
			String[] monitorStationIds = monitorStationId.split(",");
			for (String ids : monitorStationIds) {
				for (MasterModel master : masterList) {
					if (ids.equalsIgnoreCase(master.getStationId())) {
						// 获取所负责的母线集合
						responsibleMasterList.add(master.getMonitorFlag());
						continue;
					}
				}
			}

			// 获取母线设备信息
            List<Map<String, Object>> maxValueEric = getMaxValueEricOfMasterDevice(timeStart_, timeEnd_, masterList, responsibleMasterList);
//			List<Map<String, Object>> maxValueEric = XWDataEntry.getMaxValueEric(responsibleMasterList, timeStart_,timeEnd_);
//				for (Map<String, Object> map : maxValueEric) {
//					for (MasterModel master : masterList) {
//						if (map.get("devid").toString().equalsIgnoreCase(master.getMonitorFlag())) {
//							// 加入母线名称用于显示
//							map.put("masterName", master.getMasterName());
//							continue;
//						}
//					}
//				}
			//System.out.println("打印母线信息:maxValueEric:" + JSON.toJSON(maxValueEric));
			// 遍历公司下所有监测点的所有设备根据所负责的母线进行筛选
				for (PointModel point : pointList) {
					for (String masterFlags : responsibleMasterList) {
						if (point.getMasterFlag().equalsIgnoreCase(masterFlags)) {
							responsiblePointList.add(point.getPointFlag()); // 筛选出所负责的保护器设备标识，温度采集器设备标识
							continue;
						}
					}
				}
			// 获取过电压保护器信息
			List<Map<String, Object>> maxValueVoltage = XWDataEntry.getmaxValueVoltage(responsiblePointList, timeStart_,
					timeEnd_);
				for (Map<String, Object> map : maxValueVoltage) {
					String sensorId = map.get("sensorId").toString();
					for (PointModel point : pointList) {
						if (point.getPointFlag().equalsIgnoreCase(sensorId)) {
							map.put("pointName", point.getPointName()); // 设置监测设备名称
							map.put("type", point.getType());
							continue;
						}
					}
				}
			//System.out.println("打印过电压保护器:maxValueVoltage:" + JSON.toJSON(maxValueVoltage));
			// 获取温度保护器信息
			List<Map<String, Object>> maxValueTemper = XWDataEntry.getmaxValueTemper(responsiblePointList, timeStart_,
					timeEnd_);
			dataSplitting(pointList, maxValueTemper);
			//System.out.println("打印温度保护器:maxValueTemper:" + JSON.toJSON(maxValueTemper));
			// 报警统计
			List<Map<String, Object>> allCountByFaultType = XWDataEntry.getAllCountByFaultType(responsibleMasterList,timeStart_, timeEnd_);
			Integer sumCount = 0;
			for (Map<String, Object> m : allCountByFaultType) {
				sumCount += Integer.valueOf(m.get("cnt").toString());
			}
			mapData.put("sumCount", sumCount);
			//System.out.println("打印报警统计:allCountByFaultType:" + JSON.toJSON(allCountByFaultType));
			if (masterFlag.equals("Y")) {
				mapData.put("maxValueEric", maxValueEric);
			}
			if (voltageFlag.equals("Y")) {
				mapData.put("maxValueVoltage", maxValueVoltage);
			}
			if (temperFlag.equals("Y")) {
				mapData.put("maxValueTemper", maxValueTemper);
			}
			mapData.put("allCountByFaultType", allCountByFaultType);
			//重组温度采集器数据格式前端方便展示
			Set<Map<String, Object>> regroupMaxValueTemper = PoiUtil.convertDataFormat(maxValueTemper);
			List<Map<String,Object>> regroupMaxValueTemperList=   new ArrayList<>(regroupMaxValueTemper);

			regroupMaxValueTemperList = sortAfter(regroupMaxValueTemperList);
			mapData.put("regroupMaxValueTemper", regroupMaxValueTemperList);
			return mapData; // 返回数据给前端
		} else {
			/**
			 * 巡检员登陆后台如果筛选了指定监测站
			 */
			if (StringUtils.isBlank(masterId)) {// 如果没有选择母线id则查询该监测站下方的全部母线设备标识
			//	com.yscoco.dianli.common.utils.ExLogUtil.logger.info("monitorStationId:" + monitorStationId);
				for (MasterModel master : masterList) {
					if (master.getStationId().equalsIgnoreCase(monitorStationId)) {
						responsibleMasterList.add(master.getMonitorFlag()); // 获取指定监测站下面的全部母线设备标识
						continue;
					}
				}
			//	com.yscoco.dianli.common.utils.ExLogUtil.logger.info(masterList.toString());
				//System.out.println("xxxxxxxxxxresponsibleMasterList:" + responsibleMasterList);
				// 获取母线设备而信息
				List<Map<String, Object>> maxValueEric = XWDataEntry.getMaxValueEric(responsibleMasterList, timeStart_,
						timeEnd_);
					if (maxValueEric != null && !maxValueEric.isEmpty()) {
						for (Map<String, Object> map : maxValueEric) {
							for (MasterModel s : masterList) {
								if (map.get("devid").toString().equalsIgnoreCase(s.getMonitorFlag())) {
									// 加入母线名称用于显示
									map.put("masterName", s.getMasterName());
									continue;
								}
							}
						}
					}
				//System.out.println("打印母线信息:maxValueEric:" + JSON.toJSON(maxValueEric));

				// 遍历公司下所有监测点的设备根据所负责的母线进行筛选监测点的设备标识
					for (PointModel point : pointList) {
						for (String masterFlags : responsibleMasterList) {
							if (point.getMasterFlag().equalsIgnoreCase(masterFlags)) {
								responsiblePointList.add(point.getPointFlag()); // 筛选出所负责的保护器设备标识，温度采集器设备标识
							}
							continue;
						}
					}
				// 获取过电压保护器信息
				List<Map<String, Object>> maxValueVoltage = XWDataEntry.getmaxValueVoltage(responsiblePointList,
						timeStart_, timeEnd_);
					if (maxValueVoltage != null && !maxValueVoltage.isEmpty()) {
						for (Map<String, Object> map : maxValueVoltage) {
							String sensorId = map.get("sensorId").toString();
							for (PointModel point : pointList) {
								if (point.getPointFlag().equalsIgnoreCase(sensorId)) {
									map.put("pointName", point.getPointName()); // 设置监测设备名称
									map.put("type",point.getType());
								}
								continue;
							}
	
						}
					}
				//System.out.println("打印过电压保护器:maxValueVoltage:" + JSON.toJSON(maxValueVoltage));
				// 获取温度保护器信息
				List<Map<String, Object>> maxValueTemper = XWDataEntry.getmaxValueTemper(responsiblePointList,
						timeStart_, timeEnd_);
				if (maxValueTemper != null && !maxValueTemper.isEmpty()) {
					dataSplitting(pointList, maxValueTemper);
				}
				//System.out.println("打印温度保护器:maxValueTemper:" + JSON.toJSON(maxValueTemper));
				// 报警统计
				List<Map<String, Object>> allCountByFaultType = XWDataEntry.getAllCountByFaultType(responsibleMasterList, timeStart_, timeEnd_);
				Integer sumCount = 0;
				for (Map<String, Object> m : allCountByFaultType) {
					sumCount += Integer.valueOf(m.get("cnt").toString());
				}
				mapData.put("sumCount", sumCount);
				//System.out.println("打印报警统计:allCountByFaultType:" + JSON.toJSON(allCountByFaultType));
				if (masterFlag.equals("Y")) {
					mapData.put("maxValueEric", maxValueEric);
				}
				if (voltageFlag.equals("Y")) {
					mapData.put("maxValueVoltage", maxValueVoltage);
				}
				if (temperFlag.equals("Y")) {
					mapData.put("maxValueTemper", maxValueTemper);
				}
				mapData.put("allCountByFaultType", allCountByFaultType);
				
				//重组温度采集器数据格式前端方便展示
				Set<Map<String, Object>> regroupMaxValueTemper = PoiUtil.convertDataFormat(maxValueTemper);
				List<Map<String,Object>> regroupMaxValueTemperList=   new ArrayList<>(regroupMaxValueTemper);

				regroupMaxValueTemperList = sortAfter(regroupMaxValueTemperList);
				mapData.put("regroupMaxValueTemper", regroupMaxValueTemperList);

				return mapData; 

			} else {
				// 巡检员-如果前台传入指定筛选的母线id
				for (MasterModel masterModel : masterList) {
					if (masterModel.getId().equals(masterId)) {// 对比前端传入的母线id
						responsibleMasterList.add(masterModel.getMonitorFlag()); // 获取指定监测站下面的母线设备标识
						continue;
					}
				}

				// 获取母线设备而信息
				List<Map<String, Object>> maxValueEric = XWDataEntry.getMaxValueEric(responsibleMasterList, timeStart_,
						timeEnd_);
					for (Map<String, Object> map : maxValueEric) {
						for (MasterModel s : masterList) {
							if (map.get("devid").toString().equalsIgnoreCase(s.getMonitorFlag())) {
								// 加入母线名称用于显示
								map.put("masterName", s.getMasterName());
								continue;
							}
						}
					}
				//System.out.println("打印母线信息:maxValueEric:" + JSON.toJSON(maxValueEric));

				// 遍历公司下所有监测点的设备根据所负责的母线进行筛选监测点的设备标识
					for (PointModel point : pointList) {
						for (String masterFlags : responsibleMasterList) {
							if (point.getMasterFlag().equalsIgnoreCase(masterFlags)) {
								responsiblePointList.add(point.getPointFlag()); // 筛选出所负责的保护器设备标识，温度采集器设备标识
								continue;
							}
						}
					}
				//System.out.println("responsiblePointList:" + responsiblePointList);
				// 获取过电压保护器信息
				List<Map<String, Object>> maxValueVoltage = XWDataEntry.getmaxValueVoltage(responsiblePointList,
						timeStart_, timeEnd_);
					if (maxValueVoltage != null && !maxValueVoltage.isEmpty()) {
						for (Map<String, Object> map : maxValueVoltage) {
							String sensorId = map.get("sensorId").toString();
							for (PointModel point : pointList) {
								if (point.getPointFlag().equalsIgnoreCase(sensorId)) {
									map.put("pointName", point.getPointName()); // 设置监测设备名称
									map.put("type",point.getType());
									continue;
								}
							}
						}
					}
				//System.out.println("打印过电压保护器:maxValueVoltage:" + JSON.toJSON(maxValueVoltage));
				// 获取温度保护器信息
				List<Map<String, Object>> maxValueTemper = XWDataEntry.getmaxValueTemper(responsiblePointList,
						timeStart_, timeEnd_);
				if (maxValueTemper != null && !maxValueTemper.isEmpty()) {
					dataSplitting(pointList, maxValueTemper);
				}
				
				//System.out.println("打印温度保护器:maxValueTemper:" + JSON.toJSON(maxValueTemper));
				// 报警统计
				List<Map<String, Object>> allCountByFaultType = XWDataEntry
						.getAllCountByFaultType(responsibleMasterList, timeStart_, timeEnd_);
				Integer sumCount = 0;
				for (Map<String, Object> m : allCountByFaultType) {
					sumCount += Integer.valueOf(m.get("cnt").toString());
				}
				mapData.put("sumCount", sumCount);
				if (masterFlag.equals("Y")) {
					mapData.put("maxValueEric", maxValueEric);
				}
				if (voltageFlag.equals("Y")) {
					mapData.put("maxValueVoltage", maxValueVoltage);
				}
				if (temperFlag.equals("Y")) {
					mapData.put("maxValueTemper", maxValueTemper);
				}
				mapData.put("allCountByFaultType", allCountByFaultType);
				
				//重组温度采集器数据格式前端方便展示
				Set<Map<String, Object>> regroupMaxValueTemper = PoiUtil.convertDataFormat(maxValueTemper);
				List<Map<String,Object>> regroupMaxValueTemperList=   new ArrayList<>(regroupMaxValueTemper);

				regroupMaxValueTemperList = sortAfter(regroupMaxValueTemperList);
				mapData.put("regroupMaxValueTemper", regroupMaxValueTemperList);
				return mapData; 
			}
		}
	}

	private void dataSplitting(List<PointModel> pointList, List<Map<String, Object>> maxValueTemper) {
		//System.err.println("TEST："+JSON.toJSON(maxValueTemper));
		for (Map<String, Object> map : maxValueTemper) {
			for (PointModel model : pointList) {
				String pointFlag = model.getPointFlag();                  
				if (StringUtils.equalsIgnoreCase(pointFlag, map.get("sensorId").toString())) {
					// 拼接保护器所在的母线支线以及位置信息
					map.put("positionName", model.getMasterName() + "-" + model.getBranchName() + "-" + getConvertPositionName(model.getPosition()));
					map.put("type", model.getType());
					map.put("branchId", model.getBranchId());
					map.put("cabin_no",model.getCabin_no() );
					map.put("masterId", 	StringUtils.isEmpty(model.getMasterId())==true?"":model.getMasterId());
					map.put("masterFlag", StringUtils.isEmpty(model.getMasterFlag())==true?"":model.getMasterFlag());
				//	System.err.println("TEST:model.getCabin_no()："+model.getCabin_no());

					String position;
					switch (model.getPosition()) {
						case "0":
							position = "19";
							break;
						case "1":
							position = "20";
							break;
						case "2":
							position = "21";
							break;
						default:
							position = model.getPosition();
					}
					map.put("position", position);
				}
			}
		}
	}

	/**
	 * 管理员登陆获取统计数据
	 * 
	 * @param monitorStationId
	 * @param masterId
	 * @param masterFlag
	 * @param voltageFlag
	 * @param temperFlag
	 * @param timeStart_
	 * @param timeEnd_
	 * @param mapData
	 * @param masterList
	 * @param pointList
	 * @return
	 */
	public Map<String, Object> managerGetStatisticsData(String monitorStationId, String masterId, String masterFlag,
			String voltageFlag, String temperFlag, Long timeStart_, Long timeEnd_, Map<String, Object> mapData,
			List<MasterModel> masterList, List<PointModel> pointList) {
		//List<String> managerresponsibleMasterList = new ArrayList<>(); // 所负责的母线设备标识
		//List<String> managerresponsiblePointList = new ArrayList<>(); // 所负责的监测点设备标识
		Set<String> managerresponsibleMasterSet = new HashSet<>(); // 所负责的母线设备标识
		Set<String> managerresponsiblePointSet = new HashSet<>(); // 所负责的监测点设备标识
		
		if (StringUtils.isBlank(monitorStationId)) {
			// 如果没有进行监测站筛选
			for (MasterModel master : masterList) {
				managerresponsibleMasterSet.add(master.getMonitorFlag());
			}
			// 获取母线设备信息
            List<Map<String, Object>> maxValueEric = getMaxValueEricOfMasterDevice(timeStart_, timeEnd_, masterList, new ArrayList<>(managerresponsibleMasterSet));
//			List<Map<String, Object>> maxValueEric = XWDataEntry.getMaxValueEric(new ArrayList<>(managerresponsibleMasterSet),timeStart_, timeEnd_);
//			   for (Map<String, Object> map : maxValueEric) {
//					for (MasterModel master : masterList) {
//						if (map.get("devid").toString().equalsIgnoreCase(master.getMonitorFlag())) {
//							// 加入母线名称用于显示
//							map.put("masterName", master.getMasterName());
//							continue;
//						}
//					}
//				}

			//System.out.println("打印母线信息:maxValueEric:" + JSON.toJSON(maxValueEric));
			// 遍历公司下所有监测点的所有设备根据所负责的母线进行筛选
				for (PointModel point : pointList) {
					for (String masterFlags : managerresponsibleMasterSet) {
						if (point.getMasterFlag().equalsIgnoreCase(masterFlags)) {
							managerresponsiblePointSet.add(point.getPointFlag()); // 筛选出所负责的保护器设备标识，温度采集器设备标识
							continue;
						}
					}
				}
			// 获取过电压保护器信息
			//long start2 = System.currentTimeMillis();
			List<Map<String, Object>> maxValueVoltage = XWDataEntry.getmaxValueVoltage(new ArrayList<>(managerresponsiblePointSet),timeStart_, timeEnd_);
//			System.out.println("XWDataEntry.getmaxValueVoltage:"+(System.currentTimeMillis()-start2)+"ms");
//			System.out.println("managerresponsiblePointSet:" + managerresponsiblePointSet);
				for (Map<String, Object> map : maxValueVoltage) {
					String sensorId = map.get("sensorId").toString();
					for (PointModel point : pointList) {
						if (point.getPointFlag().equalsIgnoreCase(sensorId)) {
							map.put("pointName", point.getPointName()); // 设置监测设备名称
							map.put("type",point.getType());
							continue;
						}
					}
	
				}
			//System.out.println("打印过电压保护器:maxValueVoltage:" + JSON.toJSON(maxValueVoltage));
			// 获取温度保护器信息
			List<Map<String, Object>> maxValueTemper = XWDataEntry.getmaxValueTemper(new ArrayList<>(managerresponsiblePointSet),
					timeStart_, timeEnd_);
			dataSplitting(pointList, maxValueTemper);
			//System.out.println("打印温度保护器:maxValueTemper:" + JSON.toJSON(maxValueTemper));
			// 报警统计
			List<Map<String, Object>> allCountByFaultType = XWDataEntry
					.getAllCountByFaultType(new ArrayList<>(managerresponsibleMasterSet), timeStart_, timeEnd_);
			Integer sumCount = 0;
			for (Map<String, Object> m : allCountByFaultType) {
				sumCount += Integer.valueOf(m.get("cnt").toString());
			}
			mapData.put("sumCount", sumCount);

			//System.out.println("打印报警统计:allCountByFaultType:" + JSON.toJSON(allCountByFaultType));
			if (masterFlag.equals("Y")) {
				mapData.put("maxValueEric", maxValueEric);
			}
			if (voltageFlag.equals("Y")) {
				mapData.put("maxValueVoltage", maxValueVoltage);
			}
			if (temperFlag.equals("Y")) {
				mapData.put("maxValueTemper", maxValueTemper);
			}
			mapData.put("allCountByFaultType", allCountByFaultType);
			//重组温度采集器数据格式前端方便展示
			Set<Map<String, Object>> regroupMaxValueTemper = PoiUtil.convertDataFormat(maxValueTemper);
			List<Map<String,Object>> regroupMaxValueTemperList=   new ArrayList<>(regroupMaxValueTemper);

			regroupMaxValueTemperList = sortAfter(regroupMaxValueTemperList);

			mapData.put("regroupMaxValueTemper", regroupMaxValueTemperList);
			return mapData; 
		} else {
			/**
			 * 登陆后台如果筛选了指定监测站
			 */
			// 如果没有选择母线id则查询该监测站下方的全部母线设备标识
			if (StringUtils.isBlank(masterId)) {
				for (MasterModel master : masterList) {
					if (monitorStationId.equalsIgnoreCase(master.getStationId())) {
						managerresponsibleMasterSet.add(master.getMonitorFlag()); // 获取指定监测站下面的全部母线设备标识
						continue;
					}
				}
			
				// 获取母线设备而信息
				List<Map<String, Object>> maxValueEric = XWDataEntry.getMaxValueEric(new ArrayList<>(managerresponsibleMasterSet),
						timeStart_, timeEnd_);
				if (maxValueEric != null && !maxValueEric.isEmpty()) {
					for (Map<String, Object> map : maxValueEric) {
						for (MasterModel s : masterList) {
							if (map.get("devid").toString().equalsIgnoreCase(s.getMonitorFlag())) {
								// 加入母线名称用于显示
								map.put("masterName", s.getMasterName());
								continue;
							}
						}
					}
				}
				//System.out.println("打印母线信息:maxValueEric:" + JSON.toJSON(maxValueEric));

				// 遍历公司下所有监测点的设备根据所负责的母线进行筛选监测点的设备标识
				for (PointModel point : pointList) {
					for (String masterFlags : managerresponsibleMasterSet) {
						if (point.getMasterFlag().equalsIgnoreCase(masterFlags)) {
							managerresponsiblePointSet.add(point.getPointFlag()); // 筛选出所负责的保护器设备标识，温度采集器设备标识
							continue;
						}
					}
				}
				// 获取过电压保护器信息
                List<Map<String, Object>> maxValueVoltage = getmaxValueVoltageOfDevice(timeStart_, timeEnd_, pointList, managerresponsiblePointSet);
                //System.out.println("打印过电压保护器:maxValueVoltage:" + JSON.toJSON(maxValueVoltage));
				// 获取温度保护器信息
				List<Map<String, Object>> maxValueTemper = XWDataEntry.getmaxValueTemper(new ArrayList<>(managerresponsiblePointSet),
						timeStart_, timeEnd_);
				if (maxValueTemper != null && !maxValueTemper.isEmpty()) {
					dataSplitting(pointList, maxValueTemper);
				}
				//System.out.println("打印温度保护器:maxValueTemper:" + JSON.toJSON(maxValueTemper));
				// 报警统计
				List<Map<String, Object>> allCountByFaultType = XWDataEntry.getAllCountByFaultType(new ArrayList<>(managerresponsibleMasterSet), timeStart_, timeEnd_);
				Integer sumCount = 0;
				for (Map<String, Object> m : allCountByFaultType) {
					sumCount += Integer.valueOf(m.get("cnt").toString());
				}
				mapData.put("sumCount", sumCount);
				//System.out.println("打印报警统计:allCountByFaultType:" + JSON.toJSON(allCountByFaultType));
				if (masterFlag.equals("Y")) {
					mapData.put("maxValueEric", maxValueEric);
				}
				if (voltageFlag.equals("Y")) {
					mapData.put("maxValueVoltage", maxValueVoltage);
				}
				if (temperFlag.equals("Y")) {
					mapData.put("maxValueTemper", maxValueTemper);
				}
				mapData.put("allCountByFaultType", allCountByFaultType);
				//重组温度采集器数据格式前端方便展示
				Set<Map<String, Object>> regroupMaxValueTemper = PoiUtil.convertDataFormat(maxValueTemper);
				List<Map<String,Object>> regroupMaxValueTemperList=   new ArrayList<>(regroupMaxValueTemper);

				regroupMaxValueTemperList = sortAfter(regroupMaxValueTemperList);
				mapData.put("regroupMaxValueTemper", regroupMaxValueTemperList);
				return mapData; 

			} else {
				//com.yscoco.dianli.common.utils.ExLogUtil.logger.info("masterId:" + masterId);
				// 如果前台传入指定筛选的母线id
			//	System.out.println("dataSplitting:*******masterList:" + masterList);
				for (MasterModel masterModel : masterList) {
					if (masterModel.getId().equals(masterId) && masterModel.getStationId().equals(monitorStationId)) {// 对比前端传入的母线id
						managerresponsibleMasterSet.add(masterModel.getMonitorFlag()); // 获取指定监测站下面的母线设备标识
						continue;
					}
				}
				// 获取母线设备信息
                List<Map<String, Object>> maxValueEric = getMaxValueEricOfMasterDevice(timeStart_, timeEnd_, masterList, new ArrayList<>(managerresponsibleMasterSet));
                //System.out.println("打印母线信息:maxValueEric:" + JSON.toJSON(maxValueEric));

				// 遍历公司下所有监测点的设备根据所负责的母线进行筛选监测点的设备标识
				for (PointModel point : pointList) {
					for (String masterFlags : managerresponsibleMasterSet) {
						if (point.getMasterFlag().equalsIgnoreCase(masterFlags)) {
							managerresponsiblePointSet.add(point.getPointFlag()); // 筛选出所负责的保护器设备标识，温度采集器设备标识
							continue;
						}
					}
				}
				//System.out.println("responsiblePointList:" + managerresponsiblePointList);
				// 获取过电压保护器信息
                List<Map<String, Object>> maxValueVoltage = getmaxValueVoltageOfDevice(timeStart_, timeEnd_, pointList, managerresponsiblePointSet);
                //System.out.println("打印过电压保护器:maxValueVoltage:" + JSON.toJSON(maxValueVoltage));
				// 获取温度保护器信息
				List<Map<String, Object>> maxValueTemper = XWDataEntry.getmaxValueTemper(new ArrayList<>(managerresponsiblePointSet),timeStart_, timeEnd_);
			//	System.err.println("打印温度保护器:maxValueTemper:" + JSON.toJSON(maxValueTemper));
				
				if (maxValueTemper != null && !maxValueTemper.isEmpty()) {
					dataSplitting(pointList, maxValueTemper);
				}
				//System.out.println("打印温度保护器:maxValueTemper:" + JSON.toJSON(maxValueTemper));
				// 报警统计
				List<Map<String, Object>> allCountByFaultType = XWDataEntry
						.getAllCountByFaultType(new ArrayList<>(managerresponsibleMasterSet), timeStart_, timeEnd_);
				Integer sumCount = 0;
				for (Map<String, Object> m : allCountByFaultType) {
					sumCount += Integer.valueOf(m.get("cnt").toString());
				}
				mapData.put("sumCount", sumCount);
			//	System.out.println("打印报警统计:allCountByFaultType:" + JSON.toJSON(allCountByFaultType));
				if (masterFlag.equals("Y")) {
					mapData.put("maxValueEric", maxValueEric);
				}
				if (voltageFlag.equals("Y")) {
					mapData.put("maxValueVoltage", maxValueVoltage);
				}
				if (temperFlag.equals("Y")) {
					mapData.put("maxValueTemper", maxValueTemper);
				}
				mapData.put("allCountByFaultType", allCountByFaultType);
				//重组温度采集器数据格式前端方便展示
				Set<Map<String, Object>> regroupMaxValueTemper = PoiUtil.convertDataFormat(maxValueTemper);
				List<Map<String,Object>> regroupMaxValueTemperList=   new ArrayList<>(regroupMaxValueTemper);

				regroupMaxValueTemperList = sortAfter(regroupMaxValueTemperList);
				mapData.put("regroupMaxValueTemper", regroupMaxValueTemperList);
				return mapData; 
			}
		}
	}

    private List<Map<String, Object>> getmaxValueVoltageOfDevice(Long timeStart_, Long timeEnd_, List<PointModel> pointList, Set<String> managerresponsiblePointSet) {
		List<Map<String, Object>> maxValueVoltage = null;
		try {
			logger.info("XWDataEntry.getmaxValueVoltage，Ids：%s"+JSON.toJSONString(managerresponsiblePointSet));
				maxValueVoltage = XWDataEntry.getmaxValueVoltage(new ArrayList<>(managerresponsiblePointSet), timeStart_, timeEnd_);
				if (maxValueVoltage != null && !maxValueVoltage.isEmpty()) {
					for (Map<String, Object> map : maxValueVoltage) {
						String sensorId = map.get("sensorId").toString();
						for (PointModel point : pointList) {
							if (point.getPointFlag().equalsIgnoreCase(sensorId)) {
								map.put("pointName", point.getPointName()); // 设置监测设备名称
								map.put("type", point.getType());
								continue;
							}
						}

					}
				}
		}catch (Exception e){
			maxValueVoltage = new ArrayList<>();
			e.printStackTrace();
		}


        return maxValueVoltage;
    }

    // 获取母线设备信息
    private List<Map<String, Object>> getMaxValueEricOfMasterDevice(Long timeStart_, Long timeEnd_, List<MasterModel> masterList, List<String> managerresponsibleMasterSet) {
		List<Map<String, Object>> maxValueEric = null;
		try{
			logger.info("XWDataEntry.getMaxValueEric，Ids：%s"+JSON.toJSONString(managerresponsibleMasterSet));
			 maxValueEric = XWDataEntry.getMaxValueEric(managerresponsibleMasterSet, timeStart_, timeEnd_);
				if(maxValueEric!=null&&maxValueEric.size()>0){
					for (Map<String, Object> map : maxValueEric) {
						for (MasterModel s : masterList) {
							if (map.get("devid").toString().equalsIgnoreCase(s.getMonitorFlag())) {
								// 加入母线名称用于显示
								map.put("masterName", s.getMasterName());
								continue;
							}
						}
					}
				}
		}catch (Exception e){
			   maxValueEric = new ArrayList<>();
			   e.printStackTrace();
		}

        return maxValueEric;
    }


    private List<Map<String, Object>> sortAfter(List<Map<String, Object>> regroupMaxValueTemperList) {
	//	System.err.println("regroupMaxValueTemperList:"+regroupMaxValueTemperList);
		regroupMaxValueTemperList = regroupMaxValueTemperList.stream()
				 // .sorted(Comparator.comparing(CompanyService::comparingByBranchId)
				 //.thenComparing(Comparator.comparing(CompanyService::comparingByCabinNo)))
				.sorted(Comparator.comparing(CompanyService::comparingByCabinNo)
				   .thenComparing(Comparator.comparing(CompanyService::comparingByPosition)))
				   .collect(Collectors.toList());

		//出去重复数据
		   for (int i = 0; i < regroupMaxValueTemperList.size(); i++) {
			   Map<String,Object> m1 = regroupMaxValueTemperList.get(i);
				   for(int j=regroupMaxValueTemperList.size()-1;j>i;j--){
					   Map<String,Object> m2 = regroupMaxValueTemperList.get(j);
					   if (m1.get("positionName").toString().equals(m2.get("positionName").toString())) {
						   regroupMaxValueTemperList.remove(j);
						   continue;
					   }
				   }
		   }
	
		   return regroupMaxValueTemperList;
	}

	private static String comparingByBranchId(Map<String, Object> map) {
        return (String) map.get("branchId");
    }
    private static Integer comparingByPosition(Map<String, Object> map) {
        return Integer.valueOf(map.get("position").toString()) ;
    }
    private static Integer comparingByCabinNo(Map<String, Object> map) {
        return Integer.valueOf(map.get("cabin_no").toString()) ;
    }
    private static String comparingBypointName(Map<String, Object> map) {
        return map.get("positionName").toString() ;
    }

}
