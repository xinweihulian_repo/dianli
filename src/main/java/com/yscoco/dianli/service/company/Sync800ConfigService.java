package com.yscoco.dianli.service.company;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.batise.device.set.config_eric800;
import com.yscoco.dianli.common.base.Base;
import com.yscoco.dianli.dao.company.Sync800ConfigDao;
import com.yscoco.dianli.entity.company.Sync800Config;

@Service
public class Sync800ConfigService extends Base {
	
	@Autowired
	Sync800ConfigDao sync800ConfigDao;


	public void saveSync800Config(config_eric800 config_eric800,String sync_By_linesOnMasterId) {
		Sync800Config entity = new Sync800Config();
		
		if(config_eric800 != null){
			entity.setDeviceId(config_eric800.getDevid());// 设备标识
			entity.setTempMax_Line(config_eric800.getTempMax_Line());
			entity.setTempMin_Line(config_eric800.getTempMin_Line());
			entity.setTempMax_Pro(config_eric800.getTempMax_Pro());
			entity.setTempMin_Pro(config_eric800.getTempMin_Pro());
			entity.setHumdityMax(config_eric800.getHumdityMax());
			entity.setHumdityMin(config_eric800.getHumdityMin());
			entity.setLeakageCurMax(config_eric800.getLeakageCurMax());
			entity.setModulus(config_eric800.getModulus());
			entity.setMaxRateValue(config_eric800.getMaxRateValue());
			entity.setRatioPT(config_eric800.getRatioPT());
			entity.setRatioCT(config_eric800.getRatioCT());
			entity.setDriveTestFlag(config_eric800.getDriveTestFlag());
			entity.setDistanceFlag(config_eric800.getDistanceFlag());
			entity.setSync_By_linesOnMasterId(sync_By_linesOnMasterId);
		}
		this.sync800ConfigDao.save(entity);
	}
	
	
	
}
