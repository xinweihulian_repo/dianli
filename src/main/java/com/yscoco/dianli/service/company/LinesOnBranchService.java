package com.yscoco.dianli.service.company;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.batise.device.set.config_topol;
import com.hyc.smart.XWDataEntry;
import com.yscoco.dianli.common.base.Base;
import com.yscoco.dianli.common.vo.Code;
import com.yscoco.dianli.common.vo.Message;
import com.yscoco.dianli.common.vo.PageData;
import com.yscoco.dianli.constants.AppLogType;
import com.yscoco.dianli.dao.app.AppLogDao;
import com.yscoco.dianli.dao.company.CompanyDao;
import com.yscoco.dianli.dao.company.LinesOnBranchDao;
import com.yscoco.dianli.dao.company.LinesOnMasterDao;
import com.yscoco.dianli.dao.company.MonitorPointDao;
import com.yscoco.dianli.dao.company.MonitorStationDao;
import com.yscoco.dianli.dao.member.MemberInfoDao;
import com.yscoco.dianli.dto.company.LinesOnBranchDto;
import com.yscoco.dianli.dto.company.LinesOnMasterDto;
import com.yscoco.dianli.dto.company.MonitorPointDto;
import com.yscoco.dianli.dto.company.MonitorStationDto;
import com.yscoco.dianli.dto.member.MemberInfoDto;
import com.yscoco.dianli.entity.company.Company;
import com.yscoco.dianli.entity.company.LinesOnBranch;
import com.yscoco.dianli.entity.company.LinesOnMaster;
import com.yscoco.dianli.entity.company.MonitorPoint;
import com.yscoco.dianli.entity.company.MonitorStation;
import com.yscoco.dianli.entity.member.MemberInfo;

@Service
public class LinesOnBranchService extends Base { // LinesOnBranch LinesOnBranchDto
	
	@Autowired
	private CompanyDao companyDao;
	
	@Autowired
	private LinesOnBranchDao linesOnBranchDao;
	
	@Autowired
	private LinesOnMasterDao linesOnMasterDao;
	
	@Autowired
	private MonitorPointDao monitorPointDao;
	@Autowired
	private MonitorStationDao monitorStationDao;
	
	@Autowired
	private MemberInfoDao memberInfoDao;
    
	@Autowired
	private AppLogDao appLogDao;
	
	public PageData<LinesOnBranchDto> listPage(String flag,String memberId,String companyId,String linesOnMasterId,String linesOnBranchId,String branchName,Integer page, Integer rows){
		page = (page == null)?0:page;
		rows = (rows == null)?10:rows;
		
		List<LinesOnMaster> LinesOnMasterList = new ArrayList<LinesOnMaster>();// 用来装东西的
//		Company company = this.companyDao.getEntity(companyId);
//		//获取公司下的所有监测站
//		Set<MonitorStation> monitorStations = company.getMonitorStations();
		
		    List<MonitorStation>  monitorStations =new ArrayList<>();
			MemberInfo memberInfo = this.memberInfoDao.queryOne("id", memberId); 
			if (memberInfo != null) {
				String monitorStationIds = memberInfo.getMonitorStationId();
				String[] monitorStationIdArr = monitorStationIds.split(",");
				for (String str : monitorStationIdArr) {
					 MonitorStation queryOne = this.monitorStationDao.queryOne("id",str);
					 monitorStations.add(queryOne);
				}
		}else{
			        Company company = this.companyDao.getEntity(companyId);
			       //获取公司下的所有监测站
			        monitorStations = company.getMonitorStations();
		}
			

		for (MonitorStation monitorStation : monitorStations) {
			if(notEmpty(linesOnMasterId)){
				List<LinesOnMaster> linesOnMasters = monitorStation.getLinesOnMasters();
				for (LinesOnMaster linesOnMaster : linesOnMasters) {
					if(linesOnMaster.getId().equalsIgnoreCase(linesOnMasterId)){
						LinesOnMasterList.add(linesOnMaster);
					}
				}
			}else{
				LinesOnMasterList.addAll(monitorStation.getLinesOnMasters());
			}
		}
		
		PageData<LinesOnBranchDto> data = new PageData<LinesOnBranchDto>();
		List<LinesOnBranchDto> linesOnBranchDtoList = new ArrayList<LinesOnBranchDto>();// 用来装东西的
		List<MonitorPointDto> monitorPointDtoSet = null;//定义装many方的集合
		int pageCount = 0;
		for (LinesOnMaster linesOnMaster : LinesOnMasterList) {
//			if(notEmpty(linesOnMasterId) && linesOnMaster.getId().equalsIgnoreCase(linesOnMasterId)){
//				continue;
//			}
			Map<String,Object> params = new HashMap<>();
			if(StringUtils.isNotEmpty(linesOnBranchId)){
				params.put("id", linesOnBranchId);
			}
			if(StringUtils.isNotEmpty(branchName)){
				params.put("branchNameLike", branchName);
			}
			if(linesOnMaster != null){
				params.put("linesOnMaster", linesOnMaster);
			}
			params.put("page", page);
			params.put("rows", rows);
			params.put("order", "t.createTime desc");
			
			List<LinesOnBranch> beanList = linesOnBranchDao.listPage(params);// 求出bean结果集
			//System.out.println("beanList:"+JSON.toJSONString(beanList));
			pageCount += linesOnBranchDao.count(params);
			
			for (LinesOnBranch linesOnBranch : beanList) {
				if(empty(flag)){
					if(empty(memberId)){
						if(linesOnBranch.getMemberInfo() != null ){
							continue;
						}
//						if(linesOnBranch.getMemberInfo() != null && !linesOnBranch.getMemberInfo().getId().equalsIgnoreCase(memberId)){
//							break;
//						}
					}else{
						if(linesOnBranch.getMemberInfo() != null && !linesOnBranch.getMemberInfo().getId().equalsIgnoreCase(memberId)){
							continue;
						}
						
					}
				}
				
				
				monitorPointDtoSet =  new ArrayList<MonitorPointDto>();//初始化装many方的集合
				LinesOnBranchDto dto = this.linesOnBranchDao.toDto(linesOnBranch, LinesOnBranchDto.class);// 先转换自身的dto
				
				List<MonitorPoint> monitorPointList = linesOnBranch.getMonitorPoints();// 根据自身的dto得到 实体类的所有many
				
				for (MonitorPoint monitorPoint : monitorPointList) {  // 得到many方的dto集合
					monitorPointDtoSet.add(this.monitorPointDao.toDto(monitorPoint, MonitorPointDto.class));
				}
//				
				dto.setMonitorPointDtos(monitorPointDtoSet);
				if(linesOnBranch.getMemberInfo() !=null){
					dto.setMemberInfoDto(this.memberInfoDao.toDto(linesOnBranch.getMemberInfo(), MemberInfoDto.class));
				}
				if(linesOnBranch.getLinesOnMaster() !=null){
					dto.setLinesOnMasterDto(this.linesOnMasterDao.toDto(linesOnBranch.getLinesOnMaster(), LinesOnMasterDto.class));
				}
				
				if(linesOnBranch.getLinesOnMaster().getMonitorStation()!=null){
					dto.setLinesOnMasterDto(this.linesOnMasterDao.toDto(linesOnBranch.getLinesOnMaster(), LinesOnMasterDto.class));
					dto.setMonitorStationDto(this.monitorStationDao.toDto(linesOnBranch.getLinesOnMaster().getMonitorStation(), MonitorStationDto.class));
				}
				linesOnBranchDtoList.add(dto);
			}
			
		}
	
		data.setList(linesOnBranchDtoList);// 打包带走   
		data.setCount(pageCount);
		return data;
			
	}
	
	public int count(Map<String,Object> params ){
		return linesOnBranchDao.count(params);
	}
	
	@Transactional(rollbackForClassName = {"Exception"})
	public Message<LinesOnBranchDto> addOrUpdateLinesOnBranch(String linesOnBranchId,String branchName,String branchAddress,String longitudeAndlatitude,String memberInfoId,
			String linesOnMasterId, String monitorPointsIds,String  cabin_no,String branchType,String sensorNumber, String managerId, String addressIp) {
		LinesOnBranch linesOnBranch = null;
		if(notEmpty(linesOnBranchId)){
			linesOnBranch = this.linesOnBranchDao.getEntity(linesOnBranchId);
			linesOnBranch.setModifiedBy(managerId);
			linesOnBranch.setModifyTime(getDate());
			addOrUpdateDetails(linesOnBranch,branchName, branchAddress, longitudeAndlatitude,memberInfoId, linesOnMasterId, monitorPointsIds,cabin_no,branchType,sensorNumber,managerId,addressIp);
			// 记录日志
			appLogDao.recordActionOnLinesOnBranch(addressIp, managerId,AppLogType.UPDATE,linesOnBranch);
		}else{
			linesOnBranch = new LinesOnBranch();
			linesOnBranch.setCreateBy(managerId);
			linesOnBranch.setCreateTime(getDate());
			addOrUpdateDetails(linesOnBranch,branchName, branchAddress, longitudeAndlatitude,memberInfoId, linesOnMasterId, monitorPointsIds,cabin_no,branchType,sensorNumber,managerId,addressIp);
			this.linesOnBranchDao.save(linesOnBranch);
			// 记录日志
			appLogDao.recordActionOnLinesOnBranch(addressIp, managerId,AppLogType.ADD,linesOnBranch);
		}
	 
		// 整理返回给前端，否则出问题
		List<MonitorPointDto> monitorPointDtoSet = new ArrayList<MonitorPointDto>();
		LinesOnBranchDto dto = this.linesOnBranchDao.toDto(linesOnBranch, LinesOnBranchDto.class);
		
		List<MonitorPoint> monitorPointSet = linesOnBranch.getMonitorPoints();
		if(monitorPointSet != null && monitorPointSet.size()>0){
			for (MonitorPoint monitorPoint : monitorPointSet) {
				monitorPointDtoSet.add(this.monitorPointDao.toDto(monitorPoint, MonitorPointDto.class));
			}
		}
	
		dto.setMonitorPointDtos(monitorPointDtoSet);
		dto.setLinesOnMasterDto(this.linesOnMasterDao.toDto(linesOnBranch.getLinesOnMaster(), LinesOnMasterDto.class));
		if(linesOnBranch.getMemberInfo() != null){
			dto.setMemberInfoDto(this.memberInfoDao.toDto(linesOnBranch.getMemberInfo(), MemberInfoDto.class));
		}
		return new Message<LinesOnBranchDto>(Code.SUCCESS,dto);
	}

	
	private void addOrUpdateDetails(LinesOnBranch linesOnBranch ,String branchName,String branchAddress, String longitudeAndlatitude, 
			String memberInfoId,String linesOnMasterId, String monitorPointsIds,String cabin_no, String branchType, String sensorNumber,String addressIp, String managerId) {
		
		List<MonitorPoint> monitorPointSet = null ;
		if(linesOnBranch != null){
			linesOnBranch.setBranchName(branchName);
			linesOnBranch.setBranchAddress(branchAddress);
			linesOnBranch.setLongitudeAndlatitude(longitudeAndlatitude);
			linesOnBranch.setCabin_no(cabin_no);
			linesOnBranch.setBranchType(branchType);
			linesOnBranch.setSensorNumber(sensorNumber);
//			if(linesOnBranch != null && notEmpty(memberInfoId)){
//				MemberInfo memberInfo = this.memberInfoDao.getEntity(memberInfoId);
//				linesOnBranch.setMemberInfo(memberInfo);
//			}else{
//				if(linesOnBranch.getMemberInfo() != null){
//					linesOnBranch.getMemberInfo().setLinesOnBranchId(null);
//				}
//				linesOnBranch.setMemberInfo(null);
//			}
			
			if(linesOnBranch != null && notEmpty(linesOnMasterId)){
				LinesOnMaster linesOnMaster = this.linesOnMasterDao.getEntity(linesOnMasterId);
				linesOnBranch.setLinesOnMaster(linesOnMaster);
			}else{
				linesOnBranch.setLinesOnMaster(null);
			}
			
			if(linesOnBranch != null && notEmpty(monitorPointsIds)){
				String[] monitorPointsIdArr = monitorPointsIds.split(",");
				if( monitorPointsIdArr != null && monitorPointsIdArr.length>0){
					monitorPointSet = new ArrayList<MonitorPoint>();
				}
				for (String id : monitorPointsIdArr) {
					MonitorPoint monitorPoint = this.monitorPointDao.getEntity(id);
					monitorPointSet.add(monitorPoint);
				}
				linesOnBranch.setMonitorPoints(monitorPointSet);
			}else{
				linesOnBranch.setMonitorPoints(null);
			}
			
		}
	}
	
	
	public Message<?> delete(String linesOnBranchIds, String managerId, String addressIp) {
	    String[] ids = linesOnBranchIds.split(",");
	    for (String id : ids) {
    		LinesOnBranch linesOnBranch = this.linesOnBranchDao.getEntity(id);
    		// 去除支线上的人员讯息
    		MemberInfo memberInfo = linesOnBranch.getMemberInfo();
    		if(memberInfo !=null){
    			String linesOnBranchId_my = memberInfo.getLinesOnBranchId();
    			linesOnBranchId_my=linesOnBranchId_my.replace(id+",","");
    			
    			memberInfo.setLinesOnBranchId(linesOnBranchId_my);// 挑选出来
    			linesOnBranch.setMemberInfo(null);
    		}
    		
			List<MonitorPoint> monitorPoints = linesOnBranch.getMonitorPoints();
			this.monitorPointDao.deleteMonitorPoints(monitorPoints);
			linesOnBranchDao.delete(linesOnBranch);
        }
	    
	    return Message.success(linesOnBranchIds);
	    
	}

	public Message<?> SyncTopol(String linesOnBranchId, String loginManagerId) {
		LinesOnBranch linesOnBranch = this.linesOnBranchDao.getEntity(linesOnBranchId);
	
		Map<String,Object> params = new HashMap<>();
		if(StringUtils.isNotEmpty(linesOnBranchId)){
//			LinesOnBranch linesOnBranch = this.linesOnBranchDao.getEntity(linesOnBranchId);
			params.put("linesOnBranch", linesOnBranch);
		}
		params.put("order", "t.createTime desc");
		List<MonitorPoint> monitorPointList = monitorPointDao.list(params);
		List<config_topol>  entityList = new ArrayList<>();
		config_topol entity = null;
		for (MonitorPoint monitorPoint : monitorPointList) {
			entity = new config_topol();
			if(notEmpty(linesOnBranch.getCabin_no())){
				entity.setCabin_no(Byte.valueOf(linesOnBranch.getCabin_no()));
			}
			
			if(notEmpty(monitorPoint.getPosition())){
				entity.setPosition(Byte.valueOf(monitorPoint.getPosition()));
			}
			
			if(notEmpty(monitorPoint.getHitchType())){
				if(monitorPoint.getHitchType().equalsIgnoreCase("温度采集器")){
					entity.setType(Byte.valueOf("0"));
				}else if(monitorPoint.getHitchType().equalsIgnoreCase("过电压保护器")){
					entity.setType(Byte.valueOf("1"));
				}
			}
			entity.setSensorid(monitorPoint.getMonitorFlag());
			entityList.add(entity);
		}
		String monitorFlag = linesOnBranch.getLinesOnMaster().getMonitorFlag();
//		XWDataEntry.SyncTopol("00000000000000002017",entityList); 
		XWDataEntry.SyncTopol(monitorFlag,entityList);
	/*	System.out.println("测试1");
		System.out.println("测试2");
		System.out.println("测试3");
		System.out.println("测试4");
		System.out.println("测试5");*/
		return Message.success();
		
	}

	public List<MonitorPointDto> queryLinesOnBranchBySelect(String linesOnBranchId) {
			List<MonitorPointDto> result = new ArrayList<MonitorPointDto>();
			MonitorPointDto dto = null;
			LinesOnBranch linesOnBranch = this.linesOnBranchDao.getEntity(linesOnBranchId);
			List<MonitorPoint> monitorPointSet = linesOnBranch.getMonitorPoints();
			for (MonitorPoint monitorPoint : monitorPointSet) {
				dto = new MonitorPointDto();
				dto.setId(monitorPoint.getId());
				dto.setPointName(monitorPoint.getPointName());
				dto.setMonitorFlag(monitorPoint.getMonitorFlag());
				dto.setHitchType(monitorPoint.getHitchType());
				dto.setType(monitorPoint.getType());
				result.add(dto);
			}
			return result ;
	}

	public LinesOnBranch getByBranchName(String branchName){
		LinesOnBranch linesOnBranch = linesOnBranchDao.queryOne("branchName", branchName);
		return linesOnBranch;
	}
	
	
	public List<LinesOnBranchDto> getLinesOnBranchByOnMasterId(String masterId) {
		List<LinesOnBranchDto> res = new ArrayList<>();
		LinesOnMaster entity = linesOnMasterDao.getEntity(masterId);
		List<LinesOnBranch> linesOnBranchs = entity.getLinesOnBranchs();
		linesOnBranchs.forEach(b->{
			List<MonitorPointDto> mres=null;
			List<MonitorPoint> monitorPoints = b.getMonitorPoints();
			if(monitorPoints!=null&&monitorPoints.size()>0){
				mres= new ArrayList<>();
				for(int i=0;i<monitorPoints.size();i++){
					MonitorPointDto mdto = monitorPointDao.toDto(monitorPoints.get(i), MonitorPointDto.class);
					mres.add(mdto);
				}
				
			}
			LinesOnBranchDto dto = this.linesOnBranchDao.toDto(b, LinesOnBranchDto.class);
			dto.setMonitorPointDtos(mres);
			res.add(dto);
		});
		return res;
	}

	
}
