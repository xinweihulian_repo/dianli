package com.yscoco.dianli.service.company;


import java.util.*;


import com.alibaba.fastjson.JSON;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yscoco.dianli.common.base.Base;
import com.yscoco.dianli.common.vo.PageData;
import com.yscoco.dianli.dao.company.MonitorStationDao;
import com.yscoco.dianli.dao.company.MonitorStationDeviceDao;
import com.yscoco.dianli.dao.member.MemberInfoDao;
import com.yscoco.dianli.dao.sys.SysUserDao;
import com.yscoco.dianli.dto.company.LinesOnMasterDto;
import com.yscoco.dianli.dto.company.MonitorStationDeviceDto;
import com.yscoco.dianli.entity.company.MonitorStationDevice;

@Service
public class MonitorStationDeviceService extends Base {

	//private static Logger logger = Logger.getLogger(MonitorStationService.class);

	@Autowired
	private MonitorStationDao monitorStationDao;
	
	@Autowired
	private MonitorStationDeviceDao monitorStationDeviceDao;
	
	@Autowired
	private MonitorPointService monitorPointService;

	@Autowired
	private MemberInfoDao memberInfoDao;


	@Autowired
	private SysUserDao sysUserDao;



	public PageData<MonitorStationDeviceDto> listPage( String stationId,Integer page,Integer limit) {
		List<MonitorStationDeviceDto> res = new ArrayList<MonitorStationDeviceDto>();
		Map<String, Object> params = new HashMap<String, Object>();
		if(StringUtils.isNotEmpty(stationId))
			params.put("stationId", stationId);
			params.put("page", (page == null) ? 0 : page);
			params.put("rows", (limit == null) ? 10 : limit);
			params.put("order", "createTime desc");
			
		List<MonitorStationDevice> listPage = monitorStationDeviceDao.listPage(params);
		int count = monitorStationDeviceDao.count(params);

        //System.out.println("listPage:"+JSON.toJSON(listPage));
		PageData<MonitorStationDeviceDto> data = new PageData<MonitorStationDeviceDto>();
        if(listPage!=null&&listPage.size()>0){
            listPage.forEach(m->{
                MonitorStationDeviceDto dto =  new MonitorStationDeviceDto();
                 BeanUtils.copyProperties(m,dto);
                 res.add(dto);
             });
		 }
		 
		data.setCount(count);
		data.setList(res);
		return data;
	}

    public PageData<MonitorStationDevice> listPagePuls( String stationId,Integer page,Integer limit) {
        Map<String, Object> params = new HashMap<String, Object>();
        if(StringUtils.isNotEmpty(stationId))
            params.put("stationId", stationId);
            params.put("page", (page == null) ? 0 : page);
            params.put("rows", (limit == null) ? 10 : limit);
            params.put("order", "createTime desc");

        List<MonitorStationDevice> listPage = monitorStationDeviceDao.listPage(params);
        int count = monitorStationDeviceDao.count(params);
        //System.out.println("listPage:"+JSON.toJSON(listPage));
        PageData<MonitorStationDevice> data = new PageData<>();

        data.setCount(count);
        data.setList(listPage);
        return data;
    }



	public MonitorStationDevice add(MonitorStationDevice monitorStationDevice) {
		monitorStationDevice.setCreateTime(new Date());
		return  monitorStationDeviceDao.getEntity(monitorStationDeviceDao.save(monitorStationDevice));
	}



	public void del(String id) {
		monitorStationDeviceDao.delete(id);
	}


	public MonitorStationDevice addOrUpdate(MonitorStationDevice monitorStationDevice) {
		if(monitorStationDevice!=null&&monitorStationDevice.getDeviceId()!=null&&empty(monitorStationDevice.getId())){
			if(monitorPointService.getByPointFlag(monitorStationDevice.getId())!=null){
				return null;
			}
			return this.add(monitorStationDevice);
		}
		
		if(monitorStationDevice!=null&&monitorStationDevice.getDeviceId()!=null&&!empty(monitorStationDevice.getId())){
			monitorStationDevice.setModifyTime(new Date());
			monitorStationDeviceDao.update(monitorStationDevice);
		}
		
		return monitorStationDeviceDao.getEntity(monitorStationDevice.getId());
	}

	public List<MonitorStationDevice> getByStationId(String stationId){
		Map<String,Object> params = new HashMap<>();
		 params.put("stationId", stationId);

		List<MonitorStationDevice> list = monitorStationDeviceDao.list(params);
		return list;
	}
	

	public MonitorStationDevice getByDeviceId(String deviceId){
		Map<String,Object> params = new HashMap<>();
		 params.put("deviceId", deviceId);
		 MonitorStationDevice queryOne = monitorStationDeviceDao.queryOne(params);
		return queryOne;
	}
	

	public List<MonitorStationDevice> list(){
		List<MonitorStationDevice> list = monitorStationDeviceDao.list(null);
		return list;
	}

}