package com.yscoco.dianli.service.company;

import com.alibaba.fastjson.JSON;
import com.yscoco.dianli.common.base.Base;
import com.yscoco.dianli.common.vo.PageData;
import com.yscoco.dianli.dao.company.DianJiMaintainDao;
import com.yscoco.dianli.dao.member.MemberInfoDao;
import com.yscoco.dianli.dao.sys.SysUserDao;
import com.yscoco.dianli.entity.company.DianJiMaintainRecord;
import com.yscoco.dianli.entity.member.MemberInfo;
import com.yscoco.dianli.entity.sys.SysUser;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: onion..
 * @Description: TODO
 * @Date: 2020/8/19 17:19
 */
@Service
public class DianJiMaintainService extends Base {
    @Autowired
    private DianJiMaintainDao dianJiMaintainDao;
    @Autowired
    private SysUserDao sysUserDao;
    @Autowired
    private MemberInfoDao memberInfoDao;


    public PageData<DianJiMaintainRecord> listPage(String maintainTitle,String deviceId ,Integer page, Integer rows){
            Map<String,Object> params = new HashMap<>();
            page = (page == null)?0:page;
            rows = (rows == null)?10:rows;

            if(StringUtils.isNotEmpty(maintainTitle))
                params.put("maintainTitle", maintainTitle);

            if(StringUtils.isNotEmpty(deviceId))
                params.put("deviceId", deviceId);

            params.put("page", page);
            params.put("rows", rows);
            params.put("order", "t.createTime desc");

            PageData<DianJiMaintainRecord> data = new PageData<>();
            List<DianJiMaintainRecord> listPage = dianJiMaintainDao.listPage(params);
            data.setList(listPage);
            data.setCount(dianJiMaintainDao.count(params));
        return data;
    }

    public DianJiMaintainRecord addOrUpdate(DianJiMaintainRecord dianJiForm,String token ){

         SysUser su = sysUserDao.getByToken(token);
         MemberInfo mu = memberInfoDao.getByToken(token);
         

        DianJiMaintainRecord dianJi = null;
        if(notEmpty(dianJiForm.getId())){
            dianJi = this.dianJiMaintainDao.getEntity(dianJiForm.getId());
            dianJi.setModifyTime(getDate());
            addOrUpdate(dianJiForm,dianJi);
        }else{
            dianJi = new DianJiMaintainRecord();
            dianJi.setCreateTime(getDate());
            dianJi.setMaintainerName(su==null?mu.getUserName():su.getAccount());
            addOrUpdate(dianJiForm,dianJi);
            dianJi.setMaintainerId(su==null?mu.getId():su.getId());
            this.dianJiMaintainDao.save(dianJi);
        }
        return dianJi;
    }

    public void addOrUpdate(DianJiMaintainRecord dianJiForm ,DianJiMaintainRecord dianJi) {
        if (dianJi != null) {
            dianJi.setCreateBy(dianJiForm.getCreateBy() == null ? "" : dianJiForm.getCreateBy());
            dianJi.setModifiedBy(dianJiForm.getModifiedBy() == null ? "" : dianJiForm.getModifiedBy());
            dianJi.setModifyTime(dianJiForm.getModifyTime() == null ? (new Date()) : dianJiForm.getModifyTime());
            dianJi.setDeviceId(dianJiForm.getDeviceId());
            dianJi.setDeviceName(dianJiForm.getDeviceName());
            dianJi.setMaintainContent(dianJiForm.getMaintainContent());
            dianJi.setMaintainTitle(dianJiForm.getMaintainTitle());
            dianJi.setPosition(dianJiForm.getPosition());
            dianJi.setMaintainerId(dianJiForm.getMaintainerId());
            dianJi.setStationName(dianJiForm.getStationName());

        }
    }

    public int del(String id) {
        int res =1;
        try {
            dianJiMaintainDao.delete(id);
        }catch (Exception e){
            res=0;
        }
        return res;
    }
}
