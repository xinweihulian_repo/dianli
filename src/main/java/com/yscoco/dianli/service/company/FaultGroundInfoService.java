package com.yscoco.dianli.service.company;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yscoco.dianli.common.base.Base;
import com.yscoco.dianli.common.exception.BizException;
import com.yscoco.dianli.common.handler.PageData;
import com.yscoco.dianli.common.vo.Code;
import com.yscoco.dianli.common.vo.Message;
import com.yscoco.dianli.dao.company.FaultGroundInfoDao;
import com.yscoco.dianli.dao.member.MemberInfoDao;
import com.yscoco.dianli.dao.sys.SysUserDao;
import com.yscoco.dianli.dto.company.FaultGroundInfoDto;
import com.yscoco.dianli.entity.member.MemberInfo;
import com.yscoco.dianli.entity.option.FaultGroundInfo;
import com.yscoco.dianli.entity.sys.SysUser;
@Service
public class FaultGroundInfoService extends Base {
	
	@Autowired
	private FaultGroundInfoDao faultGroundInfoDao;
	
	@Autowired
	private MemberInfoDao memberInfoDao;
	
	@Autowired
	private SysUserDao sysUserInfoDao;
	
	public PageData<FaultGroundInfoDto> listPage(String faultGroundInfoId,String autoId,String deviceId,Integer page, Integer rows){
	 	page = (page == null)?0:page;
		rows = (rows == null)?10:rows;
		Map<String,Object> params = new HashMap<>();
		if(StringUtils.isNotEmpty(deviceId)){
			params.put("deviceId", deviceId);
		}
	
		if(StringUtils.isNotEmpty(autoId)){
			params.put("autoId", autoId);
		}
		params.put("page", page);
		params.put("rows", rows);
		params.put("order", "createTime desc");	
		PageData<FaultGroundInfoDto> data = new PageData<FaultGroundInfoDto>();
		List<FaultGroundInfoDto> dtoList = faultGroundInfoDao.listToDto(params,FaultGroundInfoDto.class);
		for (FaultGroundInfoDto faultGroundInfoDto : dtoList) {
			MemberInfo memberInfo = this.memberInfoDao.getEntity(faultGroundInfoDto.getCreateBy());
			if(memberInfo!=null){
				faultGroundInfoDto.setCreateByName(memberInfo.getUserName());
			}else{
				  SysUser entity = this.sysUserInfoDao.getEntity(faultGroundInfoDto.getCreateBy());
				faultGroundInfoDto.setCreateByName(entity.getAccount());
			}
		}
		data.setList(dtoList);
		data.setCount(faultGroundInfoDao.count(params));
		return data;
	}

	public Message<?> addOrUpdateFaultGroundInfo(String faultGroundInfoId,String deviceId, String autoId, String recordMessage, String type,String timestamp, String valueFlag, String loginId) {
			
		FaultGroundInfo faultGroundInfo = null ;
		if(notEmpty(faultGroundInfoId)){
			faultGroundInfo = this.faultGroundInfoDao.getEntity(faultGroundInfoId);
			faultGroundInfo.setModifyTime(getDate());
			faultGroundInfo.setModifiedBy(loginId);
			saveOrUpdateBean(faultGroundInfo,deviceId,autoId, recordMessage,type,timestamp,valueFlag);
		}else{
			faultGroundInfo = new FaultGroundInfo();
			faultGroundInfo = saveOrUpdateBean(faultGroundInfo,deviceId,autoId, recordMessage,type,timestamp,valueFlag);
			faultGroundInfo.setCreateBy(loginId);// 前台工作人员
			faultGroundInfo.setCreateTime(getDate());
			this.faultGroundInfoDao.save(faultGroundInfo);
		}
		FaultGroundInfoDto faultGroundInfoDto = this.faultGroundInfoDao.toDto(faultGroundInfo, FaultGroundInfoDto.class);
		MemberInfo memberInfo = this.memberInfoDao.getEntity(faultGroundInfoDto.getCreateBy());
		faultGroundInfoDto.setCreateByName((notEmpty(memberInfo.getUserName())?memberInfo.getUserName():memberInfo.getMobile()));
		return new Message<FaultGroundInfoDto>(Code.SUCCESS,faultGroundInfoDto);
	}
	
	public Message<?> webAddOrUpdateFaultGroundInfo(String faultGroundInfoId,String deviceId, String autoId, String recordMessage, String type,String timestamp, String valueFlag, String loginId) {
		
		FaultGroundInfo faultGroundInfo = null ;
		if(notEmpty(faultGroundInfoId)){
			faultGroundInfo = this.faultGroundInfoDao.getEntity(faultGroundInfoId);
			faultGroundInfo.setModifyTime(getDate());
			faultGroundInfo.setModifiedBy(loginId);
			saveOrUpdateBean(faultGroundInfo,deviceId,autoId, recordMessage,type,timestamp,valueFlag);
		}else{
			faultGroundInfo = new FaultGroundInfo();
			faultGroundInfo = saveOrUpdateBean(faultGroundInfo,deviceId,autoId, recordMessage,type,timestamp,valueFlag);
			faultGroundInfo.setCreateBy(loginId);// 前台工作人员
			faultGroundInfo.setCreateTime(getDate());
			this.faultGroundInfoDao.save(faultGroundInfo);
		}
		FaultGroundInfoDto faultGroundInfoDto = this.faultGroundInfoDao.toDto(faultGroundInfo, FaultGroundInfoDto.class);
		//MemberInfo memberInfo = this.memberInfoDao.getEntity(faultGroundInfoDto.getCreateBy());
		//faultGroundInfoDto.setCreateByName((notEmpty(memberInfo.getUserName())?memberInfo.getUserName():memberInfo.getMobile()));
		SysUser entity = sysUserInfoDao.getEntity(loginId);
		faultGroundInfoDto.setCreateByName((notEmpty(entity.getAccount())?entity.getAccount():entity.getMobilePhone()));
		
		return new Message<FaultGroundInfoDto>(Code.SUCCESS,faultGroundInfoDto);
	}
	
	public FaultGroundInfo saveOrUpdateBean(FaultGroundInfo faultGroundInfo,String deviceId, String autoId, String recordMessage, String type,String timestamp, String valueFlag) {
		if(faultGroundInfo == null){
			throw new BizException(Code.ERR_OTHER);
		}
		if(notEmpty(deviceId)){
			faultGroundInfo.setDeviceId(deviceId);
		}
		if(notEmpty(autoId)){
			faultGroundInfo.setAutoId(autoId);
		}
		if(notEmpty(recordMessage)){
			faultGroundInfo.setRecordMessage(recordMessage);
		}
		if(notEmpty(type)){
			faultGroundInfo.setType(type);
		}
		if(notEmpty(timestamp)){
			faultGroundInfo.setTimestamp(timestamp);
		}
		if(notEmpty(valueFlag)){
			faultGroundInfo.setValueFlag(valueFlag);
		}
		return faultGroundInfo;
	}
	
}
