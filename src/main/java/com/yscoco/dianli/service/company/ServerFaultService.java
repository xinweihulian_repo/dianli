package com.yscoco.dianli.service.company;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yscoco.dianli.common.base.Base;
import com.yscoco.dianli.common.handler.PageData;
import com.yscoco.dianli.constants.YN;
import com.yscoco.dianli.dao.company.LinesOnMasterDao;
import com.yscoco.dianli.dao.company.ServerFaultDao;
import com.yscoco.dianli.dao.member.MemberInfoDao;
import com.yscoco.dianli.dao.msg.MsgPushDao;
import com.yscoco.dianli.entity.company.LinesOnMaster;
import com.yscoco.dianli.entity.company.ServerFault;
import com.yscoco.dianli.entity.member.MemberInfo;
import com.yscoco.dianli.entity.msg.MsgPush;
import com.yscoco.dianli.other.push.PushClientI;

@Service
public class ServerFaultService extends Base {
	
	@Autowired
	ServerFaultDao serverFaultDao;
	
	@Autowired
	private MsgPushDao msgPushDao;
	
	@Autowired
	private MemberInfoDao memberInfoDao;
	
	@Autowired
	private LinesOnMasterDao linesOnMasterDao;
	
	@Autowired
	private PushClientI pushClient;

	public void getServerFault(String devid, int type) {
		ServerFault serverFault = new ServerFault();
		serverFault.setDevid(devid);
		serverFault.setType(type);
		this.serverFaultDao.save(serverFault);
		
		LinesOnMaster linesOnMaster = this.linesOnMasterDao.queryOne("monitorFlag", serverFault.getDevid());
		String faultName = "";
        switch(serverFault.getType()){
	        case 1:
	        	faultName = "保护器异常故障";break;
	        case 2:
	        	faultName = "过电压故障";break;
	        case 3:
	        	faultName = "接地故障";break;
        
        }
		// 做推送处理
		List<MemberInfo> memberList = this.memberInfoDao.list(null);
		
		for (MemberInfo memberInfo : memberList) {
			if(notEmpty(memberInfo.getPushId())){
				saveAndPushMsg(null, memberInfo.getPushId(),"通知："+linesOnMaster.getMasterAddress()+"的"+linesOnMaster.getMasterName()+"发生"+faultName+".",faultName,null);
			}
		}
	}
	
	//-----------------------   消息推送      ----------------------------
	 public void saveAndPushMsg(String fromId,String toMemberPushId,String title,String msgType,Map<String,Object> param){
	        Map<String,Object> params = new HashMap<String, Object>();
	        params.put("createBy", fromId);
	        params.put("msgType", msgType);
	        params.put("createTime", getDate());
	        MsgPush msg = null ;
	        if(msgPushDao.queryOne(params)==null){
	            msg = new MsgPush();
	            msg.setCreateBy(fromId);
	            msg.setToId(toMemberPushId);
	            msg.setToMemberId(this.memberInfoDao.getPushToMemberId(toMemberPushId));//对方的Id
	            msg.setTitle(title);
	            msg.setMsgType(msgType);
	            msgPushDao.save(msg);
	            Map<String,String> extra = new HashMap<String, String>();
	            extra.put("msgType", msgType.toString());
	            extra.put("title", title);
	            pushClient.push(toMemberPushId, null, title, extra);
	        }
	   }
	
	public PageData<String> queryServerFaultIds(Integer page, Integer rows){
	 	page = (page == null)?0:page;
		rows = (rows == null)?10:rows;
		Map<String,Object> params = new HashMap<>();
//		params.put("viewFlag", YN.N);
		params.put("page", page);
		params.put("rows", rows);
		params.put("order", "createTime desc");	
		
		PageData<String> data = new PageData<String>();
		List<ServerFault> list = serverFaultDao.list(params);
		List<String> resultIds = new ArrayList<>();;
		for (ServerFault serverFault : list) {
			resultIds.add(serverFault.getDevid());
		}
		data.setList(resultIds);
		data.setCount(this.serverFaultDao.count(params));
		return data;
	}
	
	
	public void setView(String devid) {
	    ServerFault serverFault = serverFaultDao.queryOne("devid", devid);
	    serverFault.setViewFlag(YN.Y);
	}
	
	
}
