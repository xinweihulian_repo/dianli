package com.yscoco.dianli.service.company;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.batise.device.device_info;
import com.hyc.smart.XWDataEntry;
import com.yscoco.dianli.common.base.Base;
import com.yscoco.dianli.common.vo.Code;
import com.yscoco.dianli.common.vo.Message;
import com.yscoco.dianli.common.vo.PageData;
import com.yscoco.dianli.constants.AppLogType;
import com.yscoco.dianli.dao.app.AppLogDao;
import com.yscoco.dianli.dao.company.CompanyDao;
import com.yscoco.dianli.dao.company.LinesOnBranchDao;
import com.yscoco.dianli.dao.company.LinesOnMasterDao;
import com.yscoco.dianli.dao.company.MonitorPointDao;
import com.yscoco.dianli.dao.company.MonitorStationDao;
import com.yscoco.dianli.dao.member.MemberInfoDao;
import com.yscoco.dianli.dto.company.LinesOnBranchDto;
import com.yscoco.dianli.dto.company.LinesOnMasterDto;
import com.yscoco.dianli.entity.company.Company;
import com.yscoco.dianli.entity.company.LinesOnBranch;
import com.yscoco.dianli.entity.company.LinesOnMaster;
import com.yscoco.dianli.entity.company.MonitorPoint;
import com.yscoco.dianli.entity.company.MonitorStation;
import com.yscoco.dianli.entity.member.MemberInfo;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class LinesOnMasterService extends Base { // LinesOnMaster
													// LinesOnMasterDto

	@Autowired
	private CompanyDao companyDao;

	@Autowired
	private LinesOnMasterDao linesOnMasterDao;

	@Autowired
	private LinesOnBranchDao linesOnBranchDao;

	@Autowired
	private MonitorStationDao monitorStationDao;

	@Autowired
	private MonitorPointDao monitorPointDao;
	@Autowired
	private MemberInfoDao memberInfoDao;


	public PageData<LinesOnMasterDto> listPage(String monitorStationId, String linesOnMasterId, String masterName,
			Integer page, Integer rows) {
		if (page == null) {
			page = 0;
		}
		if (rows == null) {
			rows = 10;
		}

		Map<String, Object> params = new HashMap<>();
		if (StringUtils.isNotEmpty(linesOnMasterId)) {
			params.put("id", linesOnMasterId);
		}
		if (StringUtils.isNotEmpty(masterName)) {
			params.put("masterNameLike", masterName);
		}

		if (StringUtils.isNotEmpty(monitorStationId)) {
			MonitorStation monitorStation = this.monitorStationDao.getEntity(monitorStationId);
			params.put("monitorStation", monitorStation);
		}

		params.put("page", page);
		params.put("rows", rows);
		params.put("order", " t.createTime desc ");
		PageData<LinesOnMasterDto> data = new PageData<LinesOnMasterDto>();

		List<LinesOnMasterDto> LinesOnMasterDtoList = new ArrayList<LinesOnMasterDto>();// 用来装东西的
		List<LinesOnMaster> beanList = linesOnMasterDao.listPage(params);// 求出bean结果集
		List<LinesOnBranchDto> linesOnBranchDtoSet = null;// 定义装many方的集合

		for (LinesOnMaster linesOnMaster : beanList) {
			linesOnBranchDtoSet = new ArrayList<LinesOnBranchDto>();// 初始化装many方的集合
			LinesOnMasterDto dto = this.linesOnMasterDao.toDto(linesOnMaster, LinesOnMasterDto.class);// 先转换自身的dto

			List<LinesOnBranch> LinesOnBranchSet = linesOnMaster.getLinesOnBranchs();// 根据自身的dto得到
																					// 实体类的所有many
				
			for (LinesOnBranch linesOnBranch : LinesOnBranchSet) { // 得到many方的dto集合
				linesOnBranchDtoSet.add(this.linesOnBranchDao.toDto(linesOnBranch, LinesOnBranchDto.class));
			}
			dto.setLinesOnBranchDtos(linesOnBranchDtoSet);
			if (notEmpty(dto.getPreMasterId())) {
				LinesOnMaster master = this.linesOnMasterDao.queryOne("id", dto.getPreMasterId());
				dto.setPreMasterName(master.getMasterName());
			}

			LinesOnMasterDtoList.add(dto);
		}
	   List<String> devCodeList = LinesOnMasterDtoList.stream().map(LinesOnMasterDto::getMonitorFlag).collect(Collectors.toList());
	   if(!devCodeList.isEmpty()&&devCodeList!=null){
		List<device_info> deviceInfo = XWDataEntry.getDeviceInfo(devCodeList);
		if(deviceInfo!=null&&!deviceInfo.isEmpty()){
			deviceInfo.forEach(dd->{
				for (LinesOnMasterDto linesOnMasterDto : LinesOnMasterDtoList) {
					if(dd.getDevid().equals(linesOnMasterDto.getMonitorFlag())){
						linesOnMasterDto.setIsOnline(dd.isOnline());
					}	
				}
			});
		}
		//System.out.println(JSON.toJSON(deviceInfo));
	   }
		data.setList(LinesOnMasterDtoList);// 打包带走
		data.setCount(linesOnMasterDao.count(params));
		return data;
	}

	public LinesOnMaster getEntityBymonitorFlag(String monitorFlag) {
		return this.linesOnMasterDao.queryOne("monitorFlag", monitorFlag);
	}

	public LinesOnMaster getEntityById(String id) {
		return this.linesOnMasterDao.queryOne("id", id);
	}

	public LinesOnMaster getEntityByMasterName(String masterName) {
		return this.linesOnMasterDao.queryOne("masterName", masterName);
	}

	/*
	 * // ：先处理移动处位置的数据，再处理当前母线原来的地方的上下段数据 if(empty(preMasterId)){//移动到首位：
	 * 1、选择上一段母线为空的时候，说明它是第一段母线 LinesOnMaster linesOnMaster_first_old =
	 * this.linesOnMasterDao.getOneFirstLinesOnMaster(monitorStationId);//
	 * 获取原来第一段母线 if(linesOnMaster_first_old != null){
	 * linesOnMaster_first_old.setPreMasterId(linesOnMasterId);//
	 * 当前操作母线的id设置为原来第一段母线的上一段母线id }
	 * linesOnMaster.setPreMasterId(null);//因为当前母线属于第一段母线，所以当前操作的额母线上一段母线id设置为空
	 * if(linesOnMaster_first_old != null){
	 * linesOnMaster.setNextMasterId(linesOnMaster_first_old.getId());//
	 * 原来第一段母线的id 设置为 当前操作母线的下一段母线id } }else{ // 如果不是移动到首位 LinesOnMaster
	 * linesOnMaster_pre_new = this.linesOnMasterDao.getEntity(preMasterId);//
	 * 获取上一段母线 if(linesOnMaster_pre_new != null){
	 * linesOnMaster.setPreMasterId(linesOnMaster_pre_new.getId());//
	 * 设置当前母线的上一段母线id
	 * linesOnMaster.setNextMasterId(linesOnMaster_pre_new.getNextMasterId());//
	 * 设置当前母线的下一段母线id：即当前上一段母线的下一段id即为当前母线的下一段id，如果上一段母线的下一段母线id为空，说明原来属于最后一段,
	 * 那么当前母线就将下一段设置为空，变成最后一段母线 }
	 * 
	 * if(notEmpty(linesOnMaster_pre_new.getNextMasterId())){
	 * //如果最新上一段的下一段母线id为空,说明原来属于最后一段母线,如果不为空,将下一段母线的上一段母线id设置为当前母线的id.
	 * LinesOnMaster linesOnMaster_pre_new_nextOld =
	 * this.linesOnMasterDao.getEntity(linesOnMaster_pre_new.getNextMasterId());
	 * // // 拿到最新上一段母线原来的下一段母线nextOld,并将nextOld的上一段母线id设置为当前操作的母线id，
	 * 当前操作的母线下一段id设置为nextOld的id
	 * linesOnMaster_pre_new_nextOld.setPreMasterId(linesOnMasterId); }
	 * if(linesOnMaster_pre_new != null){
	 * linesOnMaster_pre_new.setNextMasterId(linesOnMasterId);//
	 * 将上一段母线的id设置为当前母线的id,并且这步骤顺序不能换。 } }
	 * 
	 * // 2.1、处理当前操作母线的原来前面母线数据：当前操作母线的前一段母线id
	 * if(notEmpty(linesOnMaster_next_old_ID)){ // // linesOnMaster_next_old_ID
	 * 不为空 表示当前操作的母线原来不是最后一段母线 LinesOnMaster linesOnMaster_next_old =
	 * this.linesOnMasterDao.getEntity(linesOnMaster_next_old_ID);//获取原来下一段母线
	 * linesOnMaster_next_old.setPreMasterId(linesOnMaster_pre_old_ID);//
	 * 并将下一段母线的上一段母线设置为当前操作母线原来的上一段母线id
	 * 
	 * // 处理当前母线的前段母线,如果linesOnMaster_pre_old_ID
	 * 不为空，说明他不是第一条就操作值，如果是第一条就把当前上一段母线设置为null
	 * if(notEmpty(linesOnMaster_pre_old_ID)){ LinesOnMaster
	 * linesOnMaster_pre_old =
	 * this.linesOnMasterDao.getEntity(linesOnMaster_pre_old_ID);
	 * linesOnMaster_pre_old.setNextMasterId(linesOnMaster_next_old_ID); }
	 * 
	 * }else{ // linesOnMaster_next_old_ID为空 表示当前操作的母线原来是最后一段母线 //
	 * 1.1、处理当前操作母线的原来后面母线数据 LinesOnMaster linesOnMaster_pre_old =
	 * this.linesOnMasterDao.getEntity(linesOnMaster_pre_old_ID);
	 * if(linesOnMaster_pre_old != null){
	 * linesOnMaster_pre_old.setNextMasterId(null); } }
	 */

	public Message<LinesOnMasterDto> addOrUpdateLinesOnMaster(String linesOnMasterId, String masterName,
			String masterAddress, String longitudeAndlatitude, String preMasterId, String relatedType,
			String monitorFlag, String hitchType, String monitorStationId, String linesOnBranchsIds, String managerId,
			String addressIp) {
		LinesOnMaster linesOnMaster = null;
		if (notEmpty(linesOnMasterId)) {
			linesOnMaster = this.linesOnMasterDao.getEntity(linesOnMasterId);
			linesOnMaster.setModifiedBy(managerId);
			linesOnMaster.setModifyTime(getDate());

			String linesOnMaster_pre_old_ID = linesOnMaster.getPreMasterId(); // 当前操作母线的前一段母线id
			String linesOnMaster_next_old_ID = linesOnMaster.getNextMasterId();// 获取操作母线的后一段母线id

			// 判断当前上一段母线和原来的上一段是否发生改变
			if (!preMasterId.equalsIgnoreCase(linesOnMaster.getPreMasterId())) { // 当上一段母线发生id变化时才改变响应数据
				// ：先处理移动处位置的数据，再处理当前母线原来的地方的上下段数据
				if (empty(preMasterId)) {// 移动到首位： 1、选择上一段母线为空的时候，说明它是第一段母线
					LinesOnMaster linesOnMaster_first_old = this.linesOnMasterDao
							.getOneFirstLinesOnMaster(monitorStationId);// 获取原来第一段母线
					// 如果当前母线已经是第一段
					if (!linesOnMaster_first_old.getId().equalsIgnoreCase(linesOnMaster.getId())) { // 若新增的母线与查到的母线不一致，则不改变值
						linesOnMaster_first_old.setPreMasterId(linesOnMasterId);// 当前操作母线的id设置为原来第一段母线的上一段母线id
						linesOnMaster.setPreMasterId(null);// 因为当前母线属于第一段母线，所以当前操作的额母线上一段母线id设置为空
						linesOnMaster.setNextMasterId(linesOnMaster_first_old.getId());// 原来第一段母线的id
																						// 设置为
																						// 当前操作母线的下一段母线id
						// 2.1、处理当前操作母线的原来前面母线数据：当前操作母线的前一段母线id
						if (notEmpty(linesOnMaster_next_old_ID)) { 
																	// linesOnMaster_next_old_ID
																	// 不为空
																	// 表示当前操作的母线原来不是最后一段母线
							LinesOnMaster linesOnMaster_next_old = this.linesOnMasterDao
									.getEntity(linesOnMaster_next_old_ID);// 获取原来下一段母线
							linesOnMaster_next_old.setPreMasterId(linesOnMaster_pre_old_ID);// 并将下一段母线的上一段母线设置为当前操作母线原来的上一段母线id

							// 处理当前母线的前段母线,如果linesOnMaster_pre_old_ID
							// 不为空，说明他不是第一条就操作值，如果是第一条就把当前上一段母线设置为null
							if (notEmpty(linesOnMaster_pre_old_ID)) {
								LinesOnMaster linesOnMaster_pre_old = this.linesOnMasterDao
										.getEntity(linesOnMaster_pre_old_ID);
								linesOnMaster_pre_old.setNextMasterId(linesOnMaster_next_old_ID);
							}

						} else { // linesOnMaster_next_old_ID为空
									// 表示当前操作的母线原来是最后一段母线
							// 1.1、处理当前操作母线的原来后面母线数据
							LinesOnMaster linesOnMaster_pre_old = this.linesOnMasterDao
									.getEntity(linesOnMaster_pre_old_ID);
							log.info("linesOnMaster_pre_old_ID:"+linesOnMaster_pre_old_ID);
							linesOnMaster_pre_old.setNextMasterId(null);
						}

					}
				} else {
					// 如果不是移动到首位
					LinesOnMaster linesOnMaster_pre_new = this.linesOnMasterDao.getEntity(preMasterId);// 获取上一段母线
					linesOnMaster.setPreMasterId(linesOnMaster_pre_new.getId());// 设置当前母线的上一段母线id
					linesOnMaster.setNextMasterId(linesOnMaster_pre_new.getNextMasterId());// 设置当前母线的下一段母线id：即当前上一段母线的下一段id即为当前母线的下一段id，如果上一段母线的下一段母线id为空，说明原来属于最后一段,那么当前母线就将下一段设置为空，变成最后一段母线

					if (notEmpty(linesOnMaster_pre_new.getNextMasterId())) { // 如果最新上一段的下一段母线id为空,说明原来属于最后一段母线,如果不为空,将下一段母线的上一段母线id设置为当前母线的id.
						LinesOnMaster linesOnMaster_pre_new_nextOld = this.linesOnMasterDao
								.getEntity(linesOnMaster_pre_new.getNextMasterId());//
						// 拿到最新上一段母线原来的下一段母线nextOld,并将nextOld的上一段母线id设置为当前操作的母线id，当前操作的母线下一段id设置为nextOld的id
						linesOnMaster_pre_new_nextOld.setPreMasterId(linesOnMasterId);
					}
					linesOnMaster_pre_new.setNextMasterId(linesOnMasterId);// 将上一段母线的id设置为当前母线的id,并且这步骤顺序不能换。

					// 2.1、处理当前操作母线的原来前面母线数据：当前操作母线的前一段母线id
					if (notEmpty(linesOnMaster_next_old_ID)) { // //
																// linesOnMaster_next_old_ID
																// 不为空
																// 表示当前操作的母线原来不是最后一段母线
						LinesOnMaster linesOnMaster_next_old = this.linesOnMasterDao
								.getEntity(linesOnMaster_next_old_ID);// 获取原来下一段母线
						linesOnMaster_next_old.setPreMasterId(linesOnMaster_pre_old_ID);// 并将下一段母线的上一段母线设置为当前操作母线原来的上一段母线id

						// 处理当前母线的前段母线,如果linesOnMaster_pre_old_ID
						// 不为空，说明他不是第一条就操作值，如果是第一条就把当前上一段母线设置为null
						if (notEmpty(linesOnMaster_pre_old_ID)) {
							LinesOnMaster linesOnMaster_pre_old = this.linesOnMasterDao
									.getEntity(linesOnMaster_pre_old_ID);
							linesOnMaster_pre_old.setNextMasterId(linesOnMaster_next_old_ID);
						}

					} else { // linesOnMaster_next_old_ID为空 表示当前操作的母线原来是最后一段母线
						// 1.1、处理当前操作母线的原来后面母线数据
						
						LinesOnMaster linesOnMaster_pre_old = this.linesOnMasterDao.getEntity(linesOnMaster_pre_old_ID);
						if(linesOnMaster_pre_old!=null){
							linesOnMaster_pre_old.setNextMasterId(null);
						}
					}

				}

			}

			addOrUpdateDetails(linesOnMaster, masterName, masterAddress, longitudeAndlatitude, relatedType, monitorFlag,
					hitchType, monitorStationId, linesOnBranchsIds);
		} else {
			// 新增：先处理当卡母线的下一段母线id,再处理当前母线id 在处理上一段母线id
			linesOnMaster = new LinesOnMaster();
			linesOnMaster.setCreateBy(managerId);
			linesOnMaster.setCreateTime(getDate());
			addOrUpdateDetails(linesOnMaster, masterName, masterAddress, longitudeAndlatitude, relatedType, monitorFlag,
					hitchType, monitorStationId, linesOnBranchsIds);
			this.linesOnMasterDao.save(linesOnMaster);
			if (notEmpty(preMasterId)) { // 添加非首条母线
				LinesOnMaster preLinesOnMaster = this.linesOnMasterDao.getEntity(preMasterId);// 获取上一条母线
				if (notEmpty(preLinesOnMaster.getNextMasterId())) { // 获取上一条母线的下一段母线
					LinesOnMaster preLinesOnMaster_next = this.linesOnMasterDao
							.getEntity(preLinesOnMaster.getNextMasterId());
					preLinesOnMaster_next.setPreMasterId(linesOnMaster.getId());// 将上一条母线原来的下一段母线的上一段
																				// id设置为当前母线的id
				}

				linesOnMaster.setPreMasterId(preMasterId);// 设置当前母线的上一段母线id
				linesOnMaster.setNextMasterId(preLinesOnMaster.getNextMasterId());// 设置当前母线的下一段id,如果为空则为最后一段母线
				preLinesOnMaster.setNextMasterId(linesOnMaster.getId());// 设置上一段母线的下一段id为当前母线的id
			} else {
				// 新增首条母线
				// 1、获取该监测站原来的第一段母线
				LinesOnMaster linesOnMaster_first = this.linesOnMasterDao.getOneFirstLinesOnMaster(monitorStationId);
				if (!linesOnMaster_first.getId().equalsIgnoreCase(linesOnMaster.getId())) { // 若新增的母线与查到的母线不一致，则不改变值
					linesOnMaster_first.setPreMasterId(linesOnMaster.getId());// 将新增的母线id
																				// 设置为原来第一段母线的上一段母线
					linesOnMaster.setNextMasterId(linesOnMaster_first.getId());// 将新母线的下一段母线id设置为原来第一段母线的id
				}
			}

		}

		// 整理返回给前端，否则出问题
		List<LinesOnBranchDto> linesOnBranchDtoSet = new ArrayList<LinesOnBranchDto>();
		LinesOnMasterDto dto = this.linesOnMasterDao.toDto(linesOnMaster, LinesOnMasterDto.class);

		List<LinesOnBranch> linesOnBranchSet = linesOnMaster.getLinesOnBranchs();
		if (linesOnBranchSet != null && linesOnBranchSet.size() > 0) {
			for (LinesOnBranch linesOnBranch : linesOnBranchSet) {
				linesOnBranchDtoSet.add(this.linesOnBranchDao.toDto(linesOnBranch, LinesOnBranchDto.class));
			}
		}
		dto.setLinesOnBranchDtos(linesOnBranchDtoSet);
		return new Message<LinesOnMasterDto>(Code.SUCCESS, dto);
	}

	private void addOrUpdateDetails(LinesOnMaster linesOnMaster, String masterName, String masterAddress,
			String longitudeAndlatitude, String relatedType, String monitorFlag, String hitchType,
			String monitorStationId, String linesOnBranchsIds) {

		List<LinesOnBranch> linesOnBranchSet = null;
		linesOnMaster.setMasterName(masterName);
		linesOnMaster.setMasterAddress(masterAddress);
		linesOnMaster.setLongitudeAndlatitude(longitudeAndlatitude);
		// linesOnMaster.setPreMasterId(preMasterId);// 存上一段母线
		linesOnMaster.setRelatedType(relatedType);
		linesOnMaster.setMonitorFlag(monitorFlag);
		linesOnMaster.setHitchType(hitchType);
		if (linesOnMaster != null && notEmpty(linesOnBranchsIds)) {
			String[] linesOnBranchIdArr = linesOnBranchsIds.split(",");
			if (linesOnBranchIdArr != null && linesOnBranchIdArr.length > 0) {
				linesOnBranchSet = new ArrayList<LinesOnBranch>();
			}
			for (String id : linesOnBranchIdArr) {
				LinesOnBranch linesOnBranch = this.linesOnBranchDao.getEntity(id);
				linesOnBranchSet.add(linesOnBranch);
			}
			linesOnMaster.setLinesOnBranchs(linesOnBranchSet);
		} else {
			linesOnMaster.setLinesOnBranchs(null);
		}

		if (linesOnMaster != null && notEmpty(monitorStationId)) {
			MonitorStation monitorStation = this.monitorStationDao.getEntity(monitorStationId);
			linesOnMaster.setMonitorStation(monitorStation);
		} else {
			linesOnMaster.setMonitorStation(null);
		}

	}

	public Message<?> delete(String linesOnMasterIds, String managerId, String addressIp) {
		String[] ids = linesOnMasterIds.split(",");
		for (String id : ids) {
			LinesOnMaster linesOnMaster = this.linesOnMasterDao.getEntity(id);
			List<LinesOnBranch> linesOnBranchs = linesOnMaster.getLinesOnBranchs();

			// 删除支线操作
			for (LinesOnBranch linesOnBranch : linesOnBranchs) {
				// 去除支线上的人员讯息
				MemberInfo memberInfo = linesOnBranch.getMemberInfo();
				if (memberInfo != null) {
					String linesOnBranchId_my = memberInfo.getLinesOnBranchId();
					linesOnBranchId_my = linesOnBranchId_my.replace(id + ",", "");

					memberInfo.setLinesOnBranchId(linesOnBranchId_my);// 挑选出来
					linesOnBranch.setMemberInfo(null);
				}

				List<MonitorPoint> monitorPoints = linesOnBranch.getMonitorPoints();
				this.monitorPointDao.deleteMonitorPoints(monitorPoints);
				linesOnBranchDao.delete(linesOnBranch);
			}

			// 判断当前母线不为空并且当前母线上一段母线存在或者下一段母线存在的话就需要先把上下母线关联的id删除,
			if (linesOnMaster != null
					&& (notEmpty(linesOnMaster.getPreMasterId()) || notEmpty(linesOnMaster.getNextMasterId()))) {
				// 如果有上一段母线则取出上一段母线并把上一段母线的下一段母线id删除
				if (notEmpty(linesOnMaster.getPreMasterId())) {
					LinesOnMaster preLinesOnMaster = this.linesOnMasterDao.getEntity(linesOnMaster.getPreMasterId());
					if (preLinesOnMaster != null && notEmpty(preLinesOnMaster.getNextMasterId())) {
						preLinesOnMaster.setNextMasterId(null);
					}
				}
				// 如果有下一段母线则取出下一段母线并把下一段母线的上一段母线id删除
				if (notEmpty(linesOnMaster.getNextMasterId())) {
					LinesOnMaster nextLinesOnMaster = this.linesOnMasterDao.getEntity(linesOnMaster.getNextMasterId());
					if (nextLinesOnMaster != null && notEmpty(nextLinesOnMaster.getPreMasterId())) {
						nextLinesOnMaster.setPreMasterId(null);
					}
				}
			}

			// 最后把当前母线实体删除
			linesOnMasterDao.delete(linesOnMaster);
		}

		return Message.success(linesOnMasterIds);

	}

	public List<LinesOnMasterDto> queryLinesOnMasterIds(String type, String companyId, String monitorStationId,
			String linesOnMasterId, String linesOnMasterId_edit, String memberId) {
		Map<String, Object> params = new HashMap<>();
		List<LinesOnMasterDto> resultDtos = new ArrayList<LinesOnMasterDto>();// 装结果集的母线标示Id
		List<String> monitorStationIds = new ArrayList<>();// 监测站Ids
		LinesOnMasterDto dto = null;
		if (notEmpty(monitorStationId)) {
			monitorStationIds.add(monitorStationId);
		} else {
			if (StringUtils.isNotEmpty(companyId)) {
				Company company = this.companyDao.getEntity(companyId);
				if (company == null) {
					return null;
				}
				params.put("company", company);
			}
			List<MonitorStation> queryMonitorStationList = null;
			queryMonitorStationList = monitorStationDao.list(params);// 求出bean结果集
			if (memberId != null) {
				MemberInfo memberInfo = this.memberInfoDao.queryOne("id", memberId);
				if (memberInfo != null) {
					String monitorStationIdss = memberInfo.getMonitorStationId();
					String[] monitorStationIdArr = monitorStationIdss.split(",");
					queryMonitorStationList = new ArrayList<MonitorStation>();
					for (String str : monitorStationIdArr) {
						MonitorStation queryOne = this.monitorStationDao.queryOne("id", str);
						queryMonitorStationList.add(queryOne);
					}
				}
			}
			System.out.println("queryMonitorStationList:"+queryMonitorStationList);
			for (MonitorStation monitorStation : queryMonitorStationList) {
				if(monitorStationDao.queryOne("id", monitorStation.getId())!=null){
					monitorStationIds.add(monitorStation.getId());
				}
			}
		}

		LinesOnMaster now_LineOnMastrer = null;
		if (notEmpty(linesOnMasterId_edit)) { // 首先如果是自己那就continue
			now_LineOnMastrer = this.linesOnMasterDao.getEntity(linesOnMasterId_edit);

		}
		for (String monitorStationId_new : monitorStationIds) {
			MonitorStation monitorStation = monitorStationDao.getEntity(monitorStationId_new);
			List<LinesOnMaster> linesOnMasters = monitorStation.getLinesOnMasters(); // 第三步：获取母线并装进母线Set中
			if (linesOnMasters != null && linesOnMasters.size() > 0) {
				for (LinesOnMaster linesOnMaster : linesOnMasters) {
					// 查询单条母线信息
					if (notEmpty(linesOnMasterId) && !linesOnMasterId.equals(linesOnMaster.getId())) { // 首先如果是自己那就continue
						continue;
					}

					// 回显
					if (notEmpty(linesOnMasterId_edit) && linesOnMasterId_edit.equals(linesOnMaster.getId())) { // 首先如果是自己那就continue
						continue;
					}

					// 如果有当前母线id 和查询的母线的下一条母线id一致则显示
					// if(notEmpty(linesOnMaster.getNextMasterId()) &&
					// notEmpty(linesOnMasterId_edit) &&
					// !linesOnMasterId_edit.equals(linesOnMaster.getNextMasterId())){
					// // 首先如果是自己那就continue
					// continue;
					// }

					// 当前当前编辑的母线如果有下一段母线,并且下一段母线的id与当前遍历的母线Id一致则也不能显示
					// if(now_LineOnMastrer != null &&
					// notEmpty(now_LineOnMastrer.getNextMasterId()) &&
					// now_LineOnMastrer.getNextMasterId().equalsIgnoreCase(linesOnMaster.getId())){
					// continue;
					// }

					// if(notEmpty(type) && "pc".equalsIgnoreCase("pc") &&
					// notEmpty(linesOnMaster.getNextMasterId())){
					// continue;// pc端 添加 母线下拉框 如果存在下一段母线 说明被绑过,则不能再次被绑定
					// }

					dto = new LinesOnMasterDto();
					dto.setId(linesOnMaster.getId());
					dto.setMasterName(linesOnMaster.getMasterName());
					dto.setMasterAddress(linesOnMaster.getMasterAddress());
					if (notEmpty(linesOnMaster.getPreMasterId())) {
						dto.setPreMasterId(linesOnMaster.getPreMasterId());
						LinesOnMaster linesOnMaster_pre = this.linesOnMasterDao
								.getEntity(linesOnMaster.getPreMasterId());
						if (linesOnMaster_pre != null) {
							dto.setPreMasterName(linesOnMaster_pre.getMasterName());
						}
					}
					dto.setMonitorFlag(linesOnMaster.getMonitorFlag());// 获取母线标志集合：非母线Id集合
					dto.setLongitudeAndlatitude(linesOnMaster.getLongitudeAndlatitude());
					dto.setRelatedType(linesOnMaster.getRelatedType());
					dto.setHitchType(linesOnMaster.getHitchType());
					resultDtos.add(dto);
				}
			}

		}

		return resultDtos;
	}

	  /**
	   * 根据监测站id获取母线信息
	   * @param MonitorStationStationId
	   * @return
	   */
	public List<LinesOnMasterDto> getLinesOnMasterByMonitorStationStationId(String monitorStationStationId) {
		MonitorStation entity = monitorStationDao.getEntity(monitorStationStationId);
		List<LinesOnMaster> linesOnMasters = entity.getLinesOnMasters();
		List<LinesOnMasterDto> res = new ArrayList<>();
		linesOnMasters.forEach(m->{
			LinesOnMasterDto dto = this.linesOnMasterDao.toDto(m, LinesOnMasterDto.class);
			res.add(dto);
		});
		/*Map<String,Object> map = new HashMap<>();
		map.put("monitorStation", entity);
		List<LinesOnMaster> linesOnMasters = linesOnMasterDao.list(map);*/
		return res;
	}

}
