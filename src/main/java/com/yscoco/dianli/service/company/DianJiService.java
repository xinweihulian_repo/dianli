package com.yscoco.dianli.service.company;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yscoco.dianli.common.base.Base;
import com.yscoco.dianli.common.vo.PageData;
import com.yscoco.dianli.dao.company.DianJiDao;
import com.yscoco.dianli.dao.member.MemberInfoDao;
import com.yscoco.dianli.dao.sys.SysUserDao;
import com.yscoco.dianli.entity.company.DianJi;
import com.yscoco.dianli.entity.member.MemberInfo;
import com.yscoco.dianli.entity.sys.SysUser;

@Service
public class DianJiService  extends Base{
	
	@Autowired
	private SysUserDao sysUserDao;
	@Autowired
	private MemberInfoDao memberInfoDao;
	
	@Autowired
	private DianJiDao dianJiDao;
	
	public PageData<DianJi> listPage(String pointName,Integer page,Integer rows){
		page = (page == null)?0:page;
		rows = (rows == null)?10:rows;
		Map<String,Object> params = new HashMap<>();
		
		if(StringUtils.isNotEmpty(pointName)){
			params.put("pointNameLike", pointName);
		}
	
		params.put("page", page);
		params.put("rows", rows);
		params.put("order", "t.createTime desc");
		PageData<DianJi> data = new PageData<DianJi>();
		//公司监测点
		List<DianJi> listPage = dianJiDao.listPage(params);
		data.setList(listPage);
		data.setCount(dianJiDao.count(params));
		return data;
	}
	
	
	public DianJi getByDeviceNo(String deviceNo){
		DianJi dianJi = dianJiDao.queryOne("deviceNo", deviceNo);
		
		SysUser queryOne = sysUserDao.queryOne("id",dianJi.getPrincipalName());
		if(queryOne!=null){
			dianJi.setPrincipalName(queryOne.getAccount());
		}else{
			MemberInfo queryOne2 = memberInfoDao.queryOne("id",dianJi.getPrincipalName());
			dianJi.setPrincipalName(queryOne2.getUserName());
		}
		
//		if(empty(dianJi)){
//			dianJi = new DianJi();
//			dianJi.setCreateTime(getDate());
//			dianJi.setDeviceNo(deviceNo);
//		    dianJi = addOrUpdate(dianJi);
//		}
		return dianJi;
	}
	
	public DianJi getById(String id){
		//DianJi entity = dianJiDao.getEntity(id);
		DianJi queryOne = dianJiDao.queryOne("id", id);
		return queryOne;
	}
	
	public DianJi addOrUpdate(DianJi dianJiForm ){
		DianJi dianJi = null;
		if(notEmpty(dianJiForm.getId())){
			dianJi = this.dianJiDao.getEntity(dianJiForm.getId());
			dianJi.setModifyTime(getDate());
			addOrUpdate(dianJiForm,dianJi);
		}else{
			dianJi = new DianJi();
			dianJi.setCreateTime(getDate());
			addOrUpdate(dianJiForm,dianJi);
			this.dianJiDao.save(dianJi);
			
		}
		return dianJi;
	}
	
	public void addOrUpdate(DianJi dianJiForm ,DianJi dianJi){
		if(dianJi != null ){
			dianJi.setAddress(dianJiForm.getAddress()==null?"":dianJiForm.getAddress());
			dianJi.setAssetNumber(dianJiForm.getAssetNumber()==null?"":dianJiForm.getAssetNumber());
			dianJi.setDeviceNo(dianJiForm.getDeviceNo()==null?"":dianJiForm.getDeviceNo());
			dianJi.setDeviceType(dianJiForm.getDeviceType()==null?"":dianJiForm.getDeviceType());
			dianJi.setEGateWayIp(dianJiForm.getEGateWayIp()==null?"":dianJiForm.getEGateWayIp());
			dianJi.setHostAssetNumber(dianJiForm.getHostAssetNumber()==null?"":dianJiForm.getHostAssetNumber());
			dianJi.setHostName(dianJiForm.getHostName()==null?"":dianJiForm.getHostName());
			dianJi.setNo(dianJiForm.getNo()==null?"":dianJiForm.getNo());
			dianJi.setPrincipalName(dianJiForm.getPrincipalName()==null?"":dianJiForm.getPrincipalName());
			dianJi.setCreateBy(dianJiForm.getCreateBy()==null?"":dianJiForm.getCreateBy());
			dianJi.setModifiedBy(dianJiForm.getModifiedBy()==null?"":dianJiForm.getModifiedBy());
			dianJi.setModifyTime(dianJiForm.getModifyTime()==null?(new Date()):dianJiForm.getModifyTime());
		}
	}	
}
