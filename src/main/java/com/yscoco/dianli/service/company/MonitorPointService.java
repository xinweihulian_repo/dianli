package com.yscoco.dianli.service.company;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.yscoco.dianli.dao.member.MemberInfoDao;
import com.yscoco.dianli.dao.sys.SysUserDao;
import com.yscoco.dianli.entity.member.MemberInfo;
import com.yscoco.dianli.entity.sys.SysUser;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.yscoco.dianli.common.base.Base;
import com.yscoco.dianli.common.exception.BizException;
import com.yscoco.dianli.common.vo.Code;
import com.yscoco.dianli.common.vo.Message;
import com.yscoco.dianli.common.vo.PageData;
import com.yscoco.dianli.constants.AppLogType;
import com.yscoco.dianli.constants.ProtectorType;
import com.yscoco.dianli.dao.app.AppLogDao;
import com.yscoco.dianli.dao.company.LinesOnBranchDao;
import com.yscoco.dianli.dao.company.MonitorPointDao;
import com.yscoco.dianli.dto.company.MonitorPointDto;
import com.yscoco.dianli.entity.company.LinesOnBranch;
import com.yscoco.dianli.entity.company.MonitorPoint;
import com.yscoco.dianli.pojo.PointModel;


@Service
public class MonitorPointService extends Base {

	@Autowired
	private MonitorPointDao monitorPointDao;

	@Autowired
	private LinesOnBranchDao linesOnBranchDao;

	@Autowired
	private SysUserDao sysUserDao;

	@Autowired
	private MemberInfoDao memberInfoDao;

	@Autowired
	private CompanyService companyService;


	@Autowired
	private AppLogDao appLogDao;

	public PageData<MonitorPointDto> listPage(String linesOnBranchId, String monitorPointId,String pointName,Integer page,Integer rows){
		page = (page == null)?0:page;
		rows = (rows == null)?10:rows;
		Map<String,Object> params = new HashMap<>();
		if(StringUtils.isNotEmpty(monitorPointId)){
			params.put("id", monitorPointId);
		}
		if(StringUtils.isNotEmpty(pointName)){
			params.put("pointNameLike", pointName);
		}
		if(StringUtils.isNotEmpty(linesOnBranchId)){
			LinesOnBranch linesOnBranch = this.linesOnBranchDao.getEntity(linesOnBranchId);
			params.put("linesOnBranch", linesOnBranch);
		}
		params.put("page", page);
		params.put("rows", rows);
		params.put("order", "t.createTime desc");
		PageData<MonitorPointDto> data = new PageData<MonitorPointDto>();
		//公司监测点
		List<MonitorPoint> listPage = monitorPointDao.listPage(params);
		//List<MonitorPointDto> monitorPointDtoList = monitorPointDao.listToDto(listPage, MonitorPointDto.class);
		List<MonitorPointDto> monitorPointDtoList = monitorPointDao.toDto(listPage, MonitorPointDto.class);
		for (MonitorPointDto monitorPointDto : monitorPointDtoList) {

			if(notEmpty(monitorPointDto.getPosition())){
				String positionName = "";

				switch(monitorPointDto.getPosition()){
			        case "0":
			        	positionName="电缆头A相";break;
			        case "1":
			        	positionName="电缆头B相";break;
			        case "2":
			        	positionName="电缆头C相";break;
			        case "3":
			        	positionName="上触头A相";break;
			        case "4":
			        	positionName="上触头B相";break;
			        case "5":
			        	positionName="上触头C相";break;
			        case "6":
			        	positionName="上静触头A相";break;
			        case "7":
			        	positionName="上静触头B相";break;
			        case "8":
			        	positionName="上静触头C相";break;
			        case "9":
			        	positionName="下触头A相";break;
			        case "10":
			        	positionName="下触头B相";break;
			        case "11":
			        	positionName="下触头C相";break;
			        case "12":
			        	positionName="下静触头A相";break;
			        case "13":
			        	positionName="下静触头B相";break;
			        case "14":
			        	positionName="下静触头C相";break;
			        case "15":
			        	positionName="三相一体保护器";break;
			        case "16":
			        	positionName="保护器A相";break;
			        case "17":
			        	positionName="保护器B相";break;
			        case "18":
			        	positionName="保护器C相";break;
			        	default:
			        		positionName=monitorPointDto.getPosition();break;
				}
				monitorPointDto.setPositionName(positionName);
			}
		}

//		for (MonitorPoint monitorPoint : list) {
//			MonitorPointDto dto = this.monitorPointDao.toDto(monitorPoint, MonitorPointDto.class);// 先转换自身的dto
//			dto.setLinesOnBranchDto(this.linesOnBranchDao.toDto(monitorPoint.getLinesOnBranch(),LinesOnBranchDto.class));

//			if(notEmpty(dto.getPrePointId())){
//				MonitorPoint point = this.monitorPointDao.queryOne("prePointId", dto.getPrePointId());
//				dto.setPrePointName(point.getPointName());
//			}

//			monitorPointDtoList.add(dto);
//		}
		data.setList(monitorPointDtoList);
		data.setCount(monitorPointDao.count(params));
		return data;
	}


	@Transactional(rollbackForClassName = {"Exception"})
	public Message<MonitorPointDto> addOrUpdateMonitorPoint(String monitorPointId, String pointName,String pointAddress,String longitudeAndlatitude,
			String monitorFlag,String hitchType,String type,String position,String linesOnBranchId, String managerId, String addressIp) {

		MonitorPoint monitorPoint = null;
		if(notEmpty(monitorPointId)){
			if("温度采集器".equals(hitchType)){
				//15=三相一体保护器
				if("15".equals(position)){
					 throw new BizException(Code.SYS_ERR1);
				}
			}
			monitorPoint = this.monitorPointDao.getEntity(monitorPointId);
			monitorPoint.setModifiedBy(managerId);
			monitorPoint.setModifyTime(getDate());
			addOrUpdateDetails(pointName, pointAddress,longitudeAndlatitude, monitorFlag, hitchType,type,position, linesOnBranchId,monitorPoint);
			// 记录日志
			appLogDao.recordActionOnMonitorPoint(addressIp, managerId,AppLogType.UPDATE,monitorPoint);
		}else{
			if(StringUtils.isBlank(linesOnBranchId)||StringUtils.isBlank(monitorFlag)||StringUtils.isBlank(type)||StringUtils.isBlank(hitchType)){
				 throw new BizException(Code.NOT_PARAM);
			}else if("温度采集器".equals(hitchType)){
				//15=三相一体保护器
				if("15".equals(position)){
					 throw new BizException(Code.SYS_ERR1);
				}
			}
			monitorPoint = new MonitorPoint();
			monitorPoint.setCreateBy(managerId);
			monitorPoint.setCreateTime(getDate());
			addOrUpdateDetails(pointName, pointAddress,longitudeAndlatitude,monitorFlag, hitchType,type,position,linesOnBranchId,monitorPoint);
			this.monitorPointDao.save(monitorPoint);//新增监测设备

			// 记录日志
			appLogDao.recordActionOnMonitorPoint(addressIp, managerId,AppLogType.ADD,monitorPoint);
		}

		// 整理返回给前端，否则出问题
		MonitorPointDto dto = this.monitorPointDao.toDto(monitorPoint, MonitorPointDto.class);
//		dto.setLinesOnBranchDto(this.linesOnBranchDao.toDto(monitorPoint.getLinesOnBranch(), LinesOnBranchDto.class));
		return new Message<MonitorPointDto>(Code.SUCCESS,dto);
	}

	public Message<MonitorPointDto> addOrUpdateMonitorPointForApp(String monitorPointId, String pointName,String pointAddress,String longitudeAndlatitude,
			String monitorFlag,String hitchType,String type,String position,String linesOnBranchId, String managerId, String addressIp) {
		System.out.println("linesOnBranchId:"+linesOnBranchId);
		MonitorPoint monitorPoint = null;
		if(notEmpty(monitorPointId)){
			if("温度采集器".equals(hitchType)){
				//15=三相一体保护器
				if("15".equals(position)){
					 throw new BizException(Code.SYS_ERR1);
				}
			}
			monitorPoint = this.monitorPointDao.getEntity(monitorPointId);
			monitorPoint.setModifiedBy(managerId);
			monitorPoint.setModifyTime(getDate());
			addOrUpdateDetails(pointName, pointAddress,longitudeAndlatitude, monitorFlag, hitchType,type,position, linesOnBranchId,monitorPoint);
			// 记录日志
			appLogDao.recordActionOnMonitorPoint(addressIp, managerId,AppLogType.UPDATE,monitorPoint);
		}else{
			if(StringUtils.isBlank(linesOnBranchId)||StringUtils.isBlank(monitorFlag)||StringUtils.isBlank(type)||StringUtils.isBlank(hitchType)){
				 throw new BizException(Code.NOT_PARAM);
			}else if("温度采集器".equals(hitchType)){
				//15=三相一体保护器
				if("15".equals(position)){
					 throw new BizException(Code.SYS_ERR1);
				}
			}
			monitorPoint = new MonitorPoint();
			monitorPoint.setCreateBy(managerId);
			monitorPoint.setCreateTime(getDate());
			addOrUpdateDetails(pointName, pointAddress,longitudeAndlatitude,monitorFlag, hitchType,type,position,linesOnBranchId,monitorPoint);
			this.monitorPointDao.save(monitorPoint);
			// 记录日志
			appLogDao.recordActionOnMonitorPoint(addressIp, managerId,AppLogType.ADD,monitorPoint);
		}

		// 整理返回给前端，否则出问题
		MonitorPointDto dto = this.monitorPointDao.toDto(monitorPoint, MonitorPointDto.class);
		//dto.setLinesOnBranchDto(this.linesOnBranchDao.toDto(monitorPoint.getLinesOnBranch(), LinesOnBranchDto.class));
		return new Message<MonitorPointDto>(Code.SUCCESS,dto);
	}


	private void addOrUpdateDetails(String pointName,String pointAddress,String longitudeAndlatitude,
		String monitorFlag,String hitchType,String type,String position,String linesOnBranchId, MonitorPoint monitorPoint) {
		monitorPoint.setPointName(pointName);
		monitorPoint.setPointAddress(pointAddress);
		monitorPoint.setMonitorFlag(monitorFlag);
		monitorPoint.setHitchType(hitchType);
		monitorPoint.setLongitudeAndlatitude(longitudeAndlatitude);
		monitorPoint.setPosition(position);
		monitorPoint.setType(type);
		if(monitorPoint != null && notEmpty(linesOnBranchId)){
			LinesOnBranch linesOnBranch = this.linesOnBranchDao.getEntity(linesOnBranchId);
			monitorPoint.setLinesOnBranch(linesOnBranch);
		}else{
			monitorPoint.setLinesOnBranch(null);
		}

	}


	public Message<?> delete(String monitorPointIds, String managerId, String addressIp) {
	    String[] ids = monitorPointIds.split(",");
	    for (String id : ids) {
	    	MonitorPoint monitorPoint = this.monitorPointDao.getEntity(id);
	    	// 记录日志
		    appLogDao.recordActionOnMonitorPoint(addressIp, managerId,AppLogType.ADD,monitorPoint);
	        monitorPointDao.delete(id);
        }
		return Message.success(monitorPointIds);
	}





	public PageData<PointModel> getPointListByPrincipal(String token){
		PageData<PointModel> data = new PageData<PointModel>();
		List<PointModel> queryByPrincipal = monitorPointDao.queryByPrincipal(token);
		data.setCount(queryByPrincipal.size());
		data.setList(queryByPrincipal);
		return data;
	}

	public PageData<PointModel> getDianJiListByPrincipal(String token,String pointName){
		PageData<PointModel> data = new PageData<PointModel>();
		List<PointModel> dianJiResultModel = new ArrayList<>();

		List<PointModel> queryByPrincipal = monitorPointDao.queryByPrincipal(token);
		List<PointModel> dianJiPointModel = queryByPrincipal.stream().filter(a -> a.getHitchType().equals(ProtectorType.dianji.getName())).collect(Collectors.toList());

		getListPointModelByName(pointName, data, dianJiResultModel, dianJiPointModel);

		return data;
	}


	public MonitorPoint getByPointFlag(String pointFlag){
		MonitorPoint queryOne = monitorPointDao.queryOne("monitorFlag", pointFlag);
		return queryOne;
	}

	public MonitorPoint getByPointName(String pointName){
		MonitorPoint queryOne = monitorPointDao.queryOne("pointName", pointName);
		return queryOne;
	}

	public void delete(MonitorPoint monitorPoint){
		monitorPointDao.delete(monitorPoint.getId());

	}

    /**
     * 往这里写
     * @param token
     * @param pointName
     * @param page
     * @param rows
     * @return
     */
	public PageData<MonitorPointDto> getDianJiListOfAdmin(String token,String pointName,Integer page ,Integer rows){
		PageData<MonitorPointDto> data = new PageData<>();
        SysUser sysUser = sysUserDao.getByToken(token);

        Map<String, Object> params = new HashMap<>();

        if (!sysUser.getPermission().equals("admin"))
            params.put("","");

        if (StringUtils.isNotEmpty(pointName))
            params.put("pointNameLike", pointName);

            params.put("page", page==null?0:page);
            params.put("rows", rows==null?10:rows);
            params.put("hitchType", "电机");
            params.put("order", " t.createTime desc ");

        List<MonitorPoint> monitorPoints = monitorPointDao.listPage(params);
        List<MonitorPointDto> monitorPointDtoList = monitorPointDao.toDto(monitorPoints, MonitorPointDto.class);
        data.setList(monitorPointDtoList);
        data.setCount(monitorPointDao.count(params));
        return data;
	}

	/**
	 * web用户获取电机列表
	 * @param token
	 * @param pointName
	 * @return
	 */
	public PageData<PointModel> getDianJiListWebUser(String token,String pointName,String status){
		PageData<PointModel> data = new PageData<>();
		List<PointModel> dianJiResultModel = new ArrayList<>();
		SysUser sysUser = sysUserDao.getByToken(token);
		if(sysUser==null){
			List<PointModel> queryByPrincipal = monitorPointDao.queryByPrincipal(token);
			List<PointModel> dianJiPointModel = queryByPrincipal.stream().filter(a -> a.getHitchType().equals(ProtectorType.dianji.getName())).collect(Collectors.toList());
			getListPointModelByName(pointName, data, dianJiResultModel, dianJiPointModel);
		}else {
			List<PointModel> pointModels = monitorPointDao.queryAllPointFlag(null);
			List<PointModel> dianJiPointModel = pointModels.stream().filter(a -> a.getHitchType().equals(ProtectorType.dianji.getName())).collect(Collectors.toList());
			if(sysUser.getPermission().equals("admin")){
				getListPointModelByName(pointName, data, dianJiResultModel, dianJiPointModel);
			}else {
				//非admin管理员
				List<PointModel> dianJiPointModelTemp = new ArrayList<>();
				List<String> companyByUserId = companyService.getCompanyByUserId(sysUser.getId());
					companyByUserId.forEach(createUser->{
						dianJiPointModel.forEach(m->{
							if(m.getCompanyId().equals(createUser)){
								dianJiPointModelTemp.add(m);
							}
						});
					});
				getListPointModelByName(pointName, data, dianJiResultModel, dianJiPointModelTemp);

			}
		}
		return data;
	}

	private void getListPointModelByName(String pointName, PageData<PointModel> data, List<PointModel> dianJiResultModel, List<PointModel> dianJiPointModel) {
		if (StringUtils.isNotBlank(pointName)) {
			dianJiPointModel.forEach(m -> {
				if (StringUtils.contains(m.getPointName(), pointName)) {
					dianJiResultModel.add(m);
				}
			});
			data.setCount(dianJiResultModel.size());
			data.setList(dianJiResultModel);
		} else {
			data.setCount(dianJiPointModel.size());
			data.setList(dianJiPointModel);
		}
	}

	public List<PointModel> getPointModelList(){
		List<PointModel> queryAllPointFlag = monitorPointDao.queryAllPointFlag(null);
		return queryAllPointFlag;
	}

}
