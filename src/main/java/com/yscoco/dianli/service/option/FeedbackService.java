package com.yscoco.dianli.service.option;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yscoco.dianli.common.base.Base;
import com.yscoco.dianli.common.exception.BizException;
import com.yscoco.dianli.common.vo.Code;
import com.yscoco.dianli.common.vo.Message;
import com.yscoco.dianli.common.vo.PageData;
import com.yscoco.dianli.constants.AppLogType;
import com.yscoco.dianli.dao.app.AppLogDao;
import com.yscoco.dianli.dao.member.MemberInfoDao;
import com.yscoco.dianli.dao.option.FeedbackDao;
import com.yscoco.dianli.dao.sys.SysUserDao;
import com.yscoco.dianli.dto.option.FeedbackDto;
import com.yscoco.dianli.entity.member.MemberInfo;
import com.yscoco.dianli.entity.option.Feedback;
import com.yscoco.dianli.entity.sys.SysUser;

import antlr.collections.List;

@Service
public class FeedbackService extends Base{

	@Autowired
	private FeedbackDao qtBackDao;
	
	@Autowired
	private SysUserDao sysUserDao;
	
	@Autowired
	private MemberInfoDao memberInfoDao;
	
	@Autowired
	private AppLogDao appLogDao;
	
	public PageData<FeedbackDto> listPage(Integer page,Integer rows,String feedbackId){
		if(page == null){page = 1;}
		if(rows == null){rows = 10;}
		
		Map<String,Object> params = new HashMap<String, Object>();
		if(StringUtils.isNotEmpty(feedbackId)){
			params.put("id", feedbackId);
		}
		params.put("page", page);
		params.put("rows", rows);
		params.put("order", " t.createTime desc");
		PageData<FeedbackDto>  data = new PageData<FeedbackDto>();
		java.util.List<Feedback> listPage = qtBackDao.listPage(params);			
		java.util.List<FeedbackDto> dto = qtBackDao.toDto(listPage, FeedbackDto.class);
		//data.setList(qtBackDao.listToDto(params,FeedbackDto.class));
		data.setList(dto);
		data.setCount(qtBackDao.count(params));
		return data;
	}

	public Message<FeedbackDto> addOrUpdateQtBack(String feedbackId, String content,String reply,String mobileOremail,String managerId,String addressIp) {
		Feedback feedBack = null ;
		if(notEmpty(feedbackId)){
			feedBack = this.qtBackDao.getEntity(feedbackId);
//			saveOrUpdateBean(feedBack,content,reply, mobileOremail);
			if(notEmpty(reply)){
				feedBack.setReply(reply);
			}
			SysUser sysUser = this.sysUserDao.getEntity(managerId);// 后台工作人员
			feedBack.setReplyAccount(sysUser.getAccount());
		}else{
			feedBack = new Feedback();
			feedBack = saveOrUpdateBean(feedBack,content, reply,mobileOremail);
			feedBack.setCreateBy(managerId);// 前台工作人员
			MemberInfo memberInfo = this.memberInfoDao.getEntity(managerId);
			feedBack.setMobileOremail(memberInfo.getMobile());
			this.qtBackDao.save(feedBack);
		}
		return new Message<FeedbackDto>(Code.SUCCESS,this.qtBackDao.toDto(feedBack, FeedbackDto.class));
	}
	
	public Feedback saveOrUpdateBean(Feedback feedBack,String content,String reply, String mobileOremail) {
		if(feedBack ==null){
			throw new BizException(Code.ERR_OTHER);
		}
		feedBack.setContent(content);
		feedBack.setReply(reply);
		feedBack.setMobileOremail(mobileOremail);
		return feedBack;
	}
 

	public void delete(String qtBackIds, String managerId, String addressIp) {
		String[] ids = qtBackIds.split(",");
		for (String id : ids) {
			Feedback feedback = this.qtBackDao.getEntity(id);
			// 记录日志
		    appLogDao.recordActionOnFeedback(addressIp, managerId,AppLogType.DELETE,feedback);
			qtBackDao.delete(id);
		}
	}

 
}

