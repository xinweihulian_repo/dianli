package com.yscoco.dianli.service.option;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yscoco.dianli.common.base.Base;
import com.yscoco.dianli.common.vo.PageData;
import com.yscoco.dianli.dao.option.QuestionsDao;
import com.yscoco.dianli.dto.option.QuestionsDto;
import com.yscoco.dianli.entity.option.Questions;


@Service
public class QuestionsService extends Base{

	@Autowired
	private QuestionsDao questionsDao;
	
	public PageData<QuestionsDto> listPage(Integer page,Integer rows,String feedbackId){
		
		if(page == null){page = 1;}
		if(rows == null){rows = 10;}
		
		Map<String,Object> params = new HashMap<String, Object>();
		if(StringUtils.isNotEmpty(feedbackId)){
			params.put("id", feedbackId);
		}
		params.put("page", page);
		params.put("rows", rows);
		params.put("order", " t.createTime desc");
		
		PageData<QuestionsDto>  data = new PageData<QuestionsDto>();
		data.setList(questionsDao.listToDto(params,QuestionsDto.class));
		data.setCount(questionsDao.count(params));
		return data;
	}
	
	public void delete(String ids){
		String[] idArr = ids.split(",");
		for (String id : idArr) {
			questionsDao.delete(id);
		}
	}
	
	public void add(QuestionsDto dto){
		Questions qt = new Questions();
		qt.setHtml(dto.getHtml());
		BeanUtils.copyProperties(dto, qt);
		questionsDao.save(qt);
	}
	
	public void update(QuestionsDto dto){
		Questions qt = questionsDao.getEntity(dto.getId());
		BeanUtils.copyProperties(dto, qt,"id","createTime");
	}
	
	public QuestionsDto get(String id){
		Questions qt = questionsDao.getEntity(id);
		QuestionsDto dto = new QuestionsDto();
		if(qt!=null){
			BeanUtils.copyProperties(qt, dto);
		}
		return dto;
	}
}

