package com.yscoco.dianli.service.test;

import com.alibaba.fastjson.JSON;

import java.util.*;

public class MapSortTest {

    public static void main(String[] args) {

        List<Map<String, Object>> maxValueTemper = new ArrayList<>();
            Map map1 = new HashMap();
                map1.put("id", 1);
                map1.put("name", "nihao1");
             map1.put("guihao", 1);
                maxValueTemper.add(map1);

                Map map2 = new HashMap();
                map2.put("id", 3);
                map2.put("name", "nihao3");
                 map2.put("guihao", 2);
                maxValueTemper.add(map2);

                Map map3 = new HashMap();
                map3.put("id", 2);
                map3.put("name", "nihao2");
                maxValueTemper.add(map3);

                Map map4 = new HashMap();
                map4.put("id", 1);
                map4.put("name", "nihao4");
                 map4.put("guihao", 3);
                maxValueTemper.add(map4);

        System.out.println(JSON.toJSON(maxValueTemper));

        Collections.sort(maxValueTemper, new Comparator<Map<String, Object>>() {
            @Override
            public int compare(Map<String, Object> o1, Map<String, Object> o2) {
                Integer name1 = Integer.valueOf(o1.get("id").toString()) ;//name1是从你list里面拿出来的一个
                Integer name2 = Integer.valueOf(o2.get("id").toString()) ; //name1是从你list里面拿出来的第二个name
                return name1.compareTo(name2);
            }
        });
        System.out.println(JSON.toJSON(maxValueTemper));


    }
}
