package com.yscoco.dianli.service.test;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yscoco.dianli.common.base.Base;
import com.yscoco.dianli.dao.test.StudentDao;
import com.yscoco.dianli.dao.test.TeacherDao;
import com.yscoco.dianli.entity.test.Student;
import com.yscoco.dianli.entity.test.Teacher;
import com.yscoco.dianli.entity.test.TeacherDto;

@Service
public class TeacherService extends Base {

	@Autowired
	private TeacherDao teacherDao;
	
	@Autowired
	private StudentDao studentDao;
	
	
	
	public Teacher getById(String id){
		Teacher entity = teacherDao.getEntity(id);
		return entity;
		
	}
	
	public TeacherDto getByIds(String id){
		Teacher entity = teacherDao.getEntity(id);
		TeacherDto dto= new TeacherDto();
		if(entity!=null){
			 dto = teacherDao.toDto(entity, TeacherDto.class);
			 Map<String,Object> params = new HashMap<>();
				params.put("teacherId", entity.getId());
				List<Student> list = studentDao.list(params);
				dto.setStudent(list);
		}
		return dto;
	}
	
	



}
