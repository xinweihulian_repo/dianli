package com.yscoco.dianli.service.test;

import com.alibaba.fastjson.JSON;

import java.util.*;
import java.util.stream.Collectors;

public class Test {
    public static void main(String[] args) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("name", "ZK");
        map.put("age", 13);

        Map<String, Object> map4 = new HashMap<String, Object>();
        map4.put("name", "CX");
        map4.put("age", 18);

        Map<String, Object> map2 = new HashMap<String, Object>();
        map2.put("name", "ZA");
        map2.put("age", 15);

        Map<String, Object> map3 = new HashMap<String, Object>();
        map3.put("name", "CX");
        map3.put("age", 20);



        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        list.add(map);
        list.add(map4);
        list.add(map3);
        list.add(map2);

      //排序代码如下
        List<Map<String, Object>> collect = list.stream().sorted(Comparator.comparing(Test::comparingByName)
                .thenComparing(Comparator.comparing(Test::comparingByAge).reversed()))
                .collect(Collectors.toList());
        System.out.println(JSON.toJSON(collect));

    }

    private static String comparingByName(Map<String, Object> map) {
        return (String) map.get("name");
    }

    private static Integer comparingByAge(Map<String, Object> map) {
        return (Integer) map.get("age");
    }
}