package com.yscoco.dianli.service.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yscoco.dianli.dao.test.StudentDao;
import com.yscoco.dianli.entity.test.Student;

@Service
public class StudentService {
	
	@Autowired
	private StudentDao studentDao;
	
	
	public Student getById(String id){
		Student student = studentDao.getEntity(id);
		return student;
	}
	
	

}
