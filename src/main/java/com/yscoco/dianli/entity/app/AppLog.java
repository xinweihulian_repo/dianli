package com.yscoco.dianli.entity.app;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

import com.yscoco.dianli.common.base.BaseEntity;
import com.yscoco.dianli.constants.AppLogType;
@Entity
@Table(name = "t_app_log")
@Setter
@Getter
public class AppLog extends BaseEntity{

	private static final long serialVersionUID = 4220313534746444710L;
	
	@Column(columnDefinition = "varchar(255) comment '登录地方的Ip信息'")
	private String ipAddress;
	
	@Column(columnDefinition = "varchar(255) comment '日志记录人'")
	private String account;
	
	@Column(columnDefinition = "varchar(255) comment '前台还是后台登录用户'")
	private String loginType;// 前台还是后台  AppLogType
	
	@Column(name="mobilePhone",length=32)
	private String mobilePhone;
	
    @Column(length=20)
    @Enumerated(EnumType.STRING)
    private AppLogType appLogType;// 增加  删除 修改 登录
    
    @Column(columnDefinition = "varchar(255) comment '目标人姓名'")
	private String targetAccount;
    
    @Column(length=32)
	private String targetId;
    
    @Column(columnDefinition = "varchar(255) comment '具体内容'")
	private String content;
	
}
