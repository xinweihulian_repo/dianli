package com.yscoco.dianli.entity.app;

import io.swagger.annotations.ApiModel;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Index;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

import com.yscoco.dianli.common.base.BaseEntity;
import com.yscoco.dianli.common.vo.VcodeType;
import com.yscoco.dianli.constants.ConfigConts;

/**
 * 验证码校验
 * 
 *
 */
@Entity
@Table(name = "t_vcode_verify",schema = ConfigConts.DEFDB_SCHEMA, catalog = ConfigConts.DEFDB_CATALOG, indexes = {@Index(columnList = "createBy")})
@ApiModel("验证码")
@Setter
@Getter
public class VcodeVerify extends BaseEntity{

	private static final long serialVersionUID = 5853383960359632771L;

	@Column(length = 50)
	private String verifyKey;// 手机号、邮箱

	@Column(length = 10, nullable = false)
	private String vcodeValue;// 验证码

	@Column
	@Enumerated(EnumType.ORDINAL)
	private VcodeType vcodeType; // 验证码业务类型

	@Column
	private Date expireDate;// 过期时间
	
}
