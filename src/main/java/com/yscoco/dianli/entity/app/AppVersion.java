package com.yscoco.dianli.entity.app;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

import com.yscoco.dianli.common.base.BaseEntity;
import com.yscoco.dianli.constants.ConfigConts;

@Entity
@Table(name = "t_app_version",schema = ConfigConts.DEFDB_SCHEMA, catalog = ConfigConts.DEFDB_CATALOG, indexes = {@Index(columnList = "createBy")})

@Setter
@Getter
public class AppVersion extends BaseEntity{
	
	private static final long serialVersionUID = 7961290154493557392L;

	private String version;//版本号
	
	private String versionName;//版本名称
	
	@Column(name="isMust",length=1)
	private String isMust; //Y/N 是否必须更新
	
	private String url;//链接地址
	
	private String remarks;//版本更新内容
	
	private String identify;//应用标识   1安卓 2IOS 
	
}
