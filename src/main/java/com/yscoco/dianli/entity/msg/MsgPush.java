package com.yscoco.dianli.entity.msg;

import io.swagger.annotations.ApiModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Index;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

import com.yscoco.dianli.common.base.BaseEntity;
import com.yscoco.dianli.constants.ConfigConts;
import com.yscoco.dianli.constants.YN;
@Entity
@Table(name = "t_msg_push",schema = ConfigConts.DEFDB_SCHEMA, catalog = ConfigConts.DEFDB_CATALOG, indexes = {@Index(columnList = "createBy")})
@ApiModel("消息类型")
@Setter
@Getter
public class MsgPush extends BaseEntity{

    private static final long serialVersionUID = -2773383059983028698L;

    @Column(length=1024)
    private String content;//推送的内容
    
    private String jumpUrl;//推送跳转地址，预留
    
    private String img;//推送的图片，如用户头像等
    
    private String title;//推送的标题
    
    private String relId;//关联的id，如圈子的id
    
    private String msgType;// CIRCLE_LIKE,//圈子点赞  CIRCLE_COMMENT,//圈子评论 SYSTEM,//系统消息 FRIEND_ADD//添加好友申请
    
    @Column(length=1)
    @Enumerated(EnumType.STRING)
    private YN isRead = YN.N;//是否已读，N,Y
    
    private String toMemberId;
    
    private String toId;
    
 
    
}
