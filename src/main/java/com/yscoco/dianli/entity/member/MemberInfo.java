package com.yscoco.dianli.entity.member;

import io.swagger.annotations.ApiModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

import com.yscoco.dianli.common.base.BaseEntity;
import com.yscoco.dianli.constants.ConfigConts;
@Entity
@Table(name = "t_member_info",schema = ConfigConts.DEFDB_SCHEMA, catalog = ConfigConts.DEFDB_CATALOG, indexes = {@Index(columnList = "open_id")})
@ApiModel("个人信息")
@Setter
@Getter
public class MemberInfo extends BaseEntity{

	private static final long serialVersionUID = 6561842066638345901L;

	@Column(length=50)
	private String userName;
	
//	@Column(columnDefinition="varchar(255) comment '头像地址'")
///	private String avatar;
	
	@Column(columnDefinition="varchar(50)")
	private String account;
	
	@Column(length=20)
	private String mobile;
	@Column(length=100)
	private String email;
	
	@Column(columnDefinition = "varchar(32) comment '密码'")
	private String password;
	
	@Column(columnDefinition = "varchar(32) comment '老密码'")
	private String oldPassword;	
	
	// 用户基本信息
	@Column(columnDefinition = "varchar(4) comment '性别'")
	private String sex;
	
	@Column(columnDefinition = "varchar(32) comment '登录令'")
	private String token;
	
	@Column(name="open_id",columnDefinition = "varchar(100) comment '第三方登录id'")
	private String openId;//第三方id
	
	@Column(length=255)
	private String pushId;// 推送的pushId
	
	@Column(name="setting",columnDefinition = "varchar(1024) comment '用户其他设置'")
	private String setting;
	 
	@Column(length=255)
	private String companyId;// 所属公司
	
	 
	// 支线id,逗号隔开
	@Column(length=255)
	private String linesOnBranchId;// 支线id,逗号隔开
	
	/**
	 * 监测站id,逗号隔开
	 */
	@Column(length=255 , columnDefinition="comment '所负责的监测站id,用逗号隔开'")
	private String monitorStationId;
	
	  // 创建人信息
    @Column(name="createByAccount",length=50)
    private String createByAccount;
    
    
    @Column(name="createByMobile",length=50)
    private String createByMobile;
	
}
