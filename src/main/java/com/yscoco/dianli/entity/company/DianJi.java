package com.yscoco.dianli.entity.company;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

import com.yscoco.dianli.common.base.BaseEntity;
import com.yscoco.dianli.constants.ConfigConts;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "t_dianji",schema = ConfigConts.DEFDB_SCHEMA, catalog = ConfigConts.DEFDB_CATALOG, indexes = {@Index(columnList = "createBy")})
@Setter
@Getter
public class DianJi extends BaseEntity{
	private static final long serialVersionUID = 1L; 
	@Column(name="principalName",columnDefinition="varchar(32) comment '设备管理人'")
	private String principalName;
	@Column(name="address",columnDefinition="varchar(32) comment '地点'")
	private String address;
	@Column(name="deviceType",columnDefinition="varchar(32) comment '设备型号'")
	private String deviceType;
	@Column(name="deviceNo",columnDefinition="varchar(32) comment '设备编号'")
	private String deviceNo;
	@Column(name="assetNumber",columnDefinition="varchar(32) comment '资产编号'")
	private String assetNumber;
	@Column(name="hostName",columnDefinition="varchar(32) comment '主机名称'")
	private String hostName;
	@Column(name="hostAssetNumber",columnDefinition="varchar(32) comment '主机资产编号'")
	private String hostAssetNumber;
	@Column(name="eGateWayIp",columnDefinition="varchar(32) comment 'E GateWay IP'")
	private String 	eGateWayIp;
	@Column(name="no",columnDefinition="varchar(32) comment 'no'")
	private String no;

}
