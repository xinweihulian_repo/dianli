package com.yscoco.dianli.entity.company;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import com.yscoco.dianli.common.base.BaseEntity;
import com.yscoco.dianli.constants.ConfigConts;

/**
 * 母线
 * @author lingxiang
 */
@Entity
@Table(name = "t_lines_on_master",schema = ConfigConts.DEFDB_SCHEMA, catalog = ConfigConts.DEFDB_CATALOG, indexes = {@Index(columnList = "createBy")})
@Setter
@Getter
@ToString
public class LinesOnMaster extends BaseEntity{
	
	private static final long serialVersionUID = 6561842066638345901L;

	@Column(name="masterName",columnDefinition="varchar(255) comment '母线名称'")
	private String masterName;
	
	@Column(name="masterAddress",columnDefinition="varchar(255) comment '所在地区'")
	private String masterAddress ;
	
	@Column(name="longitudeAndlatitude",columnDefinition="varchar(255) comment '所在地区经纬度'")
	private String longitudeAndlatitude ;
	
	@Column(name="preMasterId",columnDefinition="varchar(255) comment '上一段母线'")
	private String preMasterId;
	
	@Column(name="nextMasterId",columnDefinition="varchar(255) comment '下一段母线'")
	private String nextMasterId;
	
	@Column(name="relatedType",columnDefinition="varchar(255) comment '关联方式'")
	private String relatedType;//关联方式：无关联 母线关联 支线关联
	
	@Column(name="monitorFlag",columnDefinition="varchar(255) comment '监测设备标识'")
	private String monitorFlag;
	/**
	 *  所属分类：0.38KV,3KV,6KN,10KV,13.8KV,20KV,30KV
	 */
	@Column(name="hitchType",columnDefinition="varchar(255) comment '电压等级分类'")
	private String hitchType;
	
	//**所属公司：可选属性optional=false,表示company不能为空；设置在employee表中的关联字段(外键)*//*
	@JoinColumn(name="monitorStationId")
	@ManyToOne(fetch=FetchType.EAGER,cascade=CascadeType.DETACH)
	private MonitorStation monitorStation;//所属监测站
	
     	
	//**公司拥有的监测站：拥有mappedBy注解的实体类为关系被维护端,mappedBy="company"中的company是Employee中的company属性*//*
	@OneToMany(mappedBy="linesOnMaster",fetch=FetchType.LAZY)
	private List<LinesOnBranch> linesOnBranchs ;// 支线
}
