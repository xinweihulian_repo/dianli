package com.yscoco.dianli.entity.company;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

import com.yscoco.dianli.common.base.BaseEntity;
import com.yscoco.dianli.constants.ConfigConts;

@Entity
@Table(name = "t_monitor_station_device",schema = ConfigConts.DEFDB_SCHEMA, catalog = ConfigConts.DEFDB_CATALOG, indexes = {@Index(columnList = "createBy")})
@Setter
@Getter
public class MonitorStationDevice extends BaseEntity{

	private static final long serialVersionUID = 6561842066638345901L;

	@Column(name="stationId",columnDefinition="varchar(32) comment '监测站名称'")
	private String stationId;
	
	@Column(name="deviceId",columnDefinition="varchar(32) comment '设备id'")
	private String deviceId ;
	
	@Column(name="deviceName",columnDefinition="varchar(32) comment '设备名字'")
	private String deviceName ;
	
	@Column(name="deviceType",columnDefinition="varchar(32) comment '设备类型'")
	private String deviceType ;
	
	@Column(name="position",columnDefinition="varchar(32) comment '所属位置'")
	private String position;
	
	@Column(name="stationType",columnDefinition="varchar(11) comment '(1.800,2.耐张线夹测温,3机电健康管理4充电柜)'")
	private String stationType ;
	
	
}
