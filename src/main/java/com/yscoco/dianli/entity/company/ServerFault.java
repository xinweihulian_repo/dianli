package com.yscoco.dianli.entity.company;

import io.swagger.annotations.ApiModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Index;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

import com.yscoco.dianli.common.base.BaseEntity;
import com.yscoco.dianli.constants.ConfigConts;
import com.yscoco.dianli.constants.YN;


@Entity
@Table(name = "t_server_ault",schema = ConfigConts.DEFDB_SCHEMA, catalog = ConfigConts.DEFDB_CATALOG, indexes = {@Index(columnList = "createBy")})
@ApiModel("监听信息")
@Setter
@Getter
public class ServerFault  extends BaseEntity{
		

	private static final long serialVersionUID = 1L;

	@Column(name="devid",length=255)
	private String devid;
	
	@Column(name="type",length=255)
	private int type;//  1：保护器异常；2:过电压故障；3:接地故障
	
	@Column(length=1)
	@Enumerated(EnumType.STRING)
	private YN viewFlag = YN.N;
}
