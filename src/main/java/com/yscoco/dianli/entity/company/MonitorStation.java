package com.yscoco.dianli.entity.company;

import io.swagger.annotations.ApiModel;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.yscoco.dianli.common.base.BaseEntity;
import com.yscoco.dianli.constants.ConfigConts;

@Entity
@Table(name = "t_monitor_station",schema = ConfigConts.DEFDB_SCHEMA, catalog = ConfigConts.DEFDB_CATALOG, indexes = {@Index(columnList = "createBy")})
@ApiModel("公司监测站")
@Setter
@Getter
public class MonitorStation extends BaseEntity{

	private static final long serialVersionUID = 6561842066638345901L;

	@Column(name="stationName",columnDefinition="varchar(255) comment '监测站名称'")
	private String stationName;
	
	@Column(name="stationAddress",columnDefinition="varchar(255) comment '所在地区'")
	private String stationAddress ;
	
	@Column(name="longitudeAndlatitude",columnDefinition="varchar(255) comment '所在地区经纬度'")
	private String longitudeAndlatitude ;
	
	@Column(name="city",columnDefinition="varchar(32) comment '所在城市'")
	private String city ;
	
	@Column(name="cityCode",columnDefinition="varchar(11) comment '所在地区城市编码'")
	private String cityCode ;
	 
	/* 所属公司：可选属性optional=false,表示company不能为空；设置在employee表中的关联字段(外键)
	manyToOne(多对一)单向,不产生中间表,使用JOinColumn(name="")来指定外键的名字,外键在多的一方表中产生*/
	//@JoinColumn(name="companyId")
	//@ManyToOne(fetch=FetchType.EAGER,cascade=CascadeType.DETACH)
	
	@JoinColumn(name="companyId")
	@ManyToOne
	private Company company;
	
	/*@JoinColumn(name="memberInfoId")
	@ManyToOne(fetch=FetchType.EAGER,cascade=CascadeType.DETACH)
	private MemberInfo memberInfo;// 管理人员:在当前表中*/	

	/*公司拥有的监测站：拥有mappedBy注解的实体类为关系被维护端,mappedBy="company"中的company是Employee中的company属性*/
	//@OneToMany(mappedBy="monitorStation",fetch=FetchType.EAGER,cascade=CascadeType.DETACH)
	//@OneToMany(mappedBy="monitorStation",fetch=FetchType.LAZY)
	@OneToMany(mappedBy="monitorStation",fetch=FetchType.EAGER,cascade=CascadeType.DETACH)
	private List<LinesOnMaster> linesOnMasters ;//母线条数
	@Column(name="stationType",columnDefinition="varchar(11) comment '监测站类型(1.800,2.耐张线夹测温,3机电健康管理4充电柜)'")
	private String stationType ;
	
}
