package com.yscoco.dianli.entity.company;

import com.yscoco.dianli.common.base.BaseEntity;
import com.yscoco.dianli.constants.ConfigConts;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

/**
 * @Author: onion..
 * @Description: TODO
 * @Date: 2020/8/19 16:53
 */
@Entity
@Table(name = "t_dian_ji_maintain_record",schema = ConfigConts.DEFDB_SCHEMA, catalog = ConfigConts.DEFDB_CATALOG, indexes = {@Index(columnList = "createBy")})
@Setter
@Getter
public class DianJiMaintainRecord extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 保养时间，保养内容，保养标题
     */
    @Column(name="maintainContent",columnDefinition="varchar(255) comment '保养内容'")
    private String maintainContent;
    @Column(name="maintainTitle",columnDefinition="varchar(255) comment '保养标题'")
    private String maintainTitle;
    @Column(name="deviceName",columnDefinition="varchar(32) comment '设备名称'")
    private String deviceName;
    @Column(name="deviceId",columnDefinition="varchar(32) comment '设备标识'")
    private String deviceId;
    @Column(name="maintainerName",columnDefinition="varchar(32) comment '维护人姓名'")
    private String maintainerName;
    @Column(name="maintainerId",columnDefinition="varchar(32) comment '维护人ID'")
    private String maintainerId;
    @Column(name="stationName",columnDefinition="varchar(32) comment '监测站名称'")
    private String stationName;
    @Column(name="position",columnDefinition="varchar(32) comment '位置'")
    private String position;

}
