package com.yscoco.dianli.entity.company;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

import com.yscoco.dianli.common.base.BaseEntity;
import com.yscoco.dianli.constants.ConfigConts;
import com.yscoco.dianli.entity.member.MemberInfo;


/**
 * 监测点
 * @author lingxiang
 *
 */
@Entity
@Table(name = "t_monitor_point",schema = ConfigConts.DEFDB_SCHEMA, catalog = ConfigConts.DEFDB_CATALOG, indexes = {@Index(columnList = "createBy")})
@Setter
@Getter
public class MonitorPoint extends BaseEntity{

	private static final long serialVersionUID = 6561842066638345901L;

	@Column(name="pointName",columnDefinition="varchar(255) comment '监测点名称'")
	private String pointName;
	
	@Column(name="pointAddress",columnDefinition="varchar(255) comment '所在地区'")
	private String pointAddress;
	
	@Column(name="prePointId",columnDefinition="varchar(255) comment '上一个监测点'")
	private String prePointId;
	
	@Column(name="longitudeAndlatitude",columnDefinition="varchar(255) comment '经纬度'")
	private String longitudeAndlatitude;
	
	@Column(name="monitorFlag",columnDefinition="varchar(255) comment '监测设备标识'")
	private String monitorFlag;
	/**
	 * 温度采集器    过电压保护器    电机
	 */
	@Column(name="hitchType",length=255)
	private String hitchType;// 所属分类
	/**
	 * 根据所属分类对应二级分类
	 * 
	 * 温度采集器：有源表带式B  无源表带式G 红外式H 微型W 磁吸附式C RFID射频R
	 * 过电压保护器：  电站型Z  电机型D  电容型R
	 */
	@Column(name="type",length=50)
	private String type; 
	
	/**
	 * 0-18      0-母排A相   ...17-保护器B相  18-保护器C相
	 */
	@Column(name="position",columnDefinition="varchar(255) comment '位置'")
	private String position;
	
	/**所属公司：可选属性optional=false,表示company不能为空；设置在employee表中的关联字段(外键)*/
	@JoinColumn(name="linesOnBranchId")
	@ManyToOne(fetch=FetchType.EAGER,cascade=CascadeType.DETACH)
	private LinesOnBranch linesOnBranch;// 监测点上面的支线
	
	@JoinColumn(name="memberInfoId")
	@ManyToOne(fetch=FetchType.EAGER,cascade=CascadeType.DETACH)
	private MemberInfo memberInfo;// 监测点所属监控人员
	
	
	
	
}
