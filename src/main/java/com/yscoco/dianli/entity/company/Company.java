package com.yscoco.dianli.entity.company;

import io.swagger.annotations.ApiModel;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.yscoco.dianli.common.base.BaseEntity;
import com.yscoco.dianli.common.utils.JsonDateSerialize;
import com.yscoco.dianli.constants.ConfigConts;

@Entity
@Table(name = "t_company",schema = ConfigConts.DEFDB_SCHEMA, catalog = ConfigConts.DEFDB_CATALOG, indexes = {@Index(columnList = "createBy")})
@ApiModel("公司信息")
@Setter
@Getter
public class Company extends BaseEntity{

	private static final long serialVersionUID = 6561842066638345901L;

	@Column(name="companyName",columnDefinition="varchar(255) comment '公司名称'")
	private String companyName;
	
	@Column(name="companyNameShort",columnDefinition="varchar(255) comment '公司简称'")
	private String companyNameShort ;
	
	@Column(name="companyAddress",columnDefinition="varchar(255) comment '公司地址'")
	private String companyAddress;
	
	@Column(name="industry",columnDefinition="varchar(255) comment '公司所属行业'")
	private String industry;
	
	@Column(name="businessType",length=255)
	private String businessType;//公司性质
	
	@Column(name="companySize",length=255)
	private String companySize;// 公司规模
	
	@Column(name="mainProducts",columnDefinition = "varchar(1024) comment '主要产品以及产能'")
	private String mainProducts;
	
	/** 所属公司：可选属性optional=false,表示company不能为空；设置在employee表中的关联字段(外键)
	 * (一对多)单向,会产生中间表
	 */
	@OneToMany(mappedBy="company",fetch=FetchType.LAZY)
	//@OneToMany(mappedBy="company",fetch=FetchType.EAGER)
	private List<MonitorStation> monitorStations ;//监测站
	
}
