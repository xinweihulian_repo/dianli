package com.yscoco.dianli.entity.company;

import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

import com.yscoco.dianli.common.base.BaseEntity;
import com.yscoco.dianli.constants.ConfigConts;

@Entity
@Table(name = "t_Sync800Config",schema = ConfigConts.DEFDB_SCHEMA, catalog = ConfigConts.DEFDB_CATALOG, indexes = {@Index(columnList = "createBy")})
@Setter
@Getter
public class Sync800Config extends BaseEntity{
	
	private static final long serialVersionUID = 7961290154493557392L;

	private String deviceId;//母线的id
	
	private float tempMax_Line;//线路温度最大值
	
	private float tempMin_Line;//线路温度最小值
	
	private float tempMax_Pro;//保护器温度最大值
	
	private float tempMin_Pro;//保护器温度最小值
	
	private float humdityMax;//湿度最大值
	
	private float humdityMin;//湿度最小值
	
	private float leakageCurMax;//泄露电流最大值
	
	private float modulus;//电压门限系数
	
	private int maxRateValue;//一次额定电压
	
	private int ratioPT;//PT
	
	private int ratioCT;//CT
	
	private byte driveTestFlag;//传动实验；1:开启；2:关闭
	
	private byte distanceFlag;//就地和远方；1:就地；2:远方
	
	private String sync_By_linesOnMasterId;//通过哪条母线进行的同步
	
	
}
