package com.yscoco.dianli.entity.company;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

import com.yscoco.dianli.common.base.BaseEntity;
import com.yscoco.dianli.constants.ConfigConts;
import com.yscoco.dianli.entity.member.MemberInfo;

/**
 * 支线
 * @author lingxiang
 *
 */
@Entity
@Table(name = "t_lines_on_branch",schema = ConfigConts.DEFDB_SCHEMA, catalog = ConfigConts.DEFDB_CATALOG, indexes = {@Index(columnList = "createBy")})
@Setter
@Getter
public class LinesOnBranch extends BaseEntity{

	private static final long serialVersionUID = 6561842066638345901L;
	
	/**
	 * 支线类型  1.进线柜，2.PT柜，3.出线柜，4.联络柜，5.提升柜
	 */
	@Column(name="branchType",columnDefinition="varchar(100) comment '支线类型  1.进线柜，2.PT柜，3.出线柜，4.联络柜，5.提升柜'")
	private String branchType;
	

	/**
	 * 0-19 排序
	 */
	@Column(name="cabin_no",columnDefinition="varchar(255) comment '柜号'")
	private String cabin_no;
	
	/**
	 * CT绕组数 : (无CT , 3 , 4 , 6-1 ,6-2 , 9 , 12 )
	 */
	@Column(name="sensorNumber",columnDefinition="varchar(100) comment '传感器动作次数'")
	private String sensorNumber;

	@Column(name="branchName",columnDefinition="varchar(255) comment '支线名称'")
	private String branchName;

	@Column(name="branchAddress",columnDefinition="varchar(255) comment '支线地址'")
	private String branchAddress;
	
	@Column(name="longitudeAndlatitude",columnDefinition="varchar(255) comment '经纬度'")
	private String longitudeAndlatitude;
	
	/**所属公司：可选属性optional=false,表示company不能为空；设置在employee表中的关联字段(外键)*/
	@JoinColumn(name="memberInfoId")
	@ManyToOne(fetch=FetchType.EAGER,cascade=CascadeType.DETACH)
	private MemberInfo memberInfo;// 管理人员:在当前表中
	
	/**所属公司：可选属性optional=false,表示company不能为空；设置在employee表中的关联字段(外键)*/
	@JoinColumn(name="linesOnMasterId")
	@ManyToOne(fetch=FetchType.EAGER,cascade=CascadeType.DETACH)
	private LinesOnMaster linesOnMaster;// 所属母线
	
	/**公司拥有的监测站：拥有mappedBy注解的实体类为关系被维护端,mappedBy="company"中的company是Employee中的company属性*/
	@OneToMany(mappedBy="linesOnBranch",fetch=FetchType.LAZY)
	private List<MonitorPoint> monitorPoints ;// 支线下的监测点
	
	
}
