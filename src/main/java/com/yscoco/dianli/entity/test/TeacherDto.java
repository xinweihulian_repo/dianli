package com.yscoco.dianli.entity.test;


import java.util.List;

import com.yscoco.dianli.dto.BaseDto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class TeacherDto extends BaseDto {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name;
	private String sub;
	private List<Student> student;
	
	
	public static void main(String args[]){
		
		String userPath = System.getProperties().getProperty("user.home") + "/mayday/";
		
		System.out.println(userPath);
	}
	
}
