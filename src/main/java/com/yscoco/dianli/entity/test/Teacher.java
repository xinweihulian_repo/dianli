package com.yscoco.dianli.entity.test;




import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;


import com.yscoco.dianli.common.base.BaseEntity;
import com.yscoco.dianli.constants.ConfigConts;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
//@Table(name = "t_teacher",schema = ConfigConts.DEFDB_SCHEMA, catalog = ConfigConts.DEFDB_CATALOG, indexes = {@Index(columnList = "id")})
@Table(name = "t_teacher")
@Setter
@Getter
@ToString
public class Teacher  extends BaseEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Column(name="name")
	private String name;
	@Column(name="sub")
	private String sub;

	

//	@JoinColumn(name="teacherId")@ManyToOne(fetch=FetchType.EAGER,cascade=CascadeType.DETACH)
//	private List<Student> student ;// 
//	@OneToMany(mappedBy="student",fetch=FetchType.LAZY)
//	private List<Student> student ;
	

}
