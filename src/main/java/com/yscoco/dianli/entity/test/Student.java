package com.yscoco.dianli.entity.test;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.yscoco.dianli.common.base.BaseEntity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
//@Table(name = "t_student",schema = ConfigConts.DEFDB_SCHEMA, catalog = ConfigConts.DEFDB_CATALOG, indexes = {@Index(columnList = "teacherId")})
@Table(name = "t_student")

@Setter
@Getter
@ToString
public class Student  extends BaseEntity{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Column(name="name")
	private String name;
	@Column(name="teacherId")
	private String teacherId;
	
	
}
