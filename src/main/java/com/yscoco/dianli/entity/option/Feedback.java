package com.yscoco.dianli.entity.option;

import io.swagger.annotations.ApiModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

import com.yscoco.dianli.common.base.BaseEntity;
import com.yscoco.dianli.constants.ConfigConts;



@Entity
@Table(name="t_feed_back",schema = ConfigConts.DEFDB_SCHEMA, catalog = ConfigConts.DEFDB_CATALOG, indexes = {@Index(columnList = "createBy")})
@ApiModel("问题反馈")
@Setter
@Getter
public class Feedback extends BaseEntity{

	private static final long serialVersionUID = 6943668866255221004L;
	
	@Column(name="content")
	private String content;//反馈内容
	
	@Column(name="mobileOremail")
	private String mobileOremail;// 反馈账号
	
	@Column(name="reply")
	private String reply;//回复内容 
	
	@Column(name="replyAccount")
	private String replyAccount;//回复账号
	
}
