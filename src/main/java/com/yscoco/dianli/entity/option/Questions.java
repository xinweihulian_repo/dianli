package com.yscoco.dianli.entity.option;

import io.swagger.annotations.ApiModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

import com.yscoco.dianli.common.base.BaseEntity;
import com.yscoco.dianli.constants.ConfigConts;
@Entity
@Table(name = "t_questions",schema = ConfigConts.DEFDB_SCHEMA, catalog = ConfigConts.DEFDB_CATALOG, indexes = {@Index(columnList = "createBy")})
@ApiModel("反馈问题")
@Setter
@Getter
public class Questions extends BaseEntity{

	private static final long serialVersionUID = -9058691294792177849L;
	
	@Column(length=32)
	private String title;
	
	@Column(length=514)
	private String digest;//摘要内容
	
	@Column(length=1)
	private String type;//1:连接不上,2:常见问题,3:健康咨询,4其他,5链接帮助
	
	@Column
	private String titleImg;
	
	@Column(columnDefinition = "longtext")
	private String html;//html 内容

}
