package com.yscoco.dianli.entity.option;

import io.swagger.annotations.ApiModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

import com.yscoco.dianli.common.base.BaseEntity;
import com.yscoco.dianli.constants.ConfigConts;



@Entity
@Table(name="t_fault_ground_info",schema = ConfigConts.DEFDB_SCHEMA, catalog = ConfigConts.DEFDB_CATALOG, indexes = {@Index(columnList = "createBy")})
@ApiModel("故障处理信息提交")
@Setter
@Getter
public class FaultGroundInfo extends BaseEntity{

	private static final long serialVersionUID = 6943668866255221004L;
	
	@Column(name="deviceId")
	private String deviceId;//母线Id
	
	@Column(name="autoId")
	private String autoId;//故障信息自增Id
	
	@Column(name="recordMessage")
	private String recordMessage;//记录填写
	
	@Column(name="type")
	private String type;// 故障类型；1:保护器异常，2:过电压故障，3：接地故障
	
	@Column(name="timestamp")
	private String timestamp;// 故障发生的时间戳
	
	@Column(name="valueFlag")
	private String valueFlag;//发生故障的相位；1：A相； 2：B相；4：C相
	
	
	
	
	
}
