package com.yscoco.dianli.entity.sys;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.yscoco.dianli.common.base.BaseEntity;
import com.yscoco.dianli.common.utils.JsonDateSerialize;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

/**
 * @Author: onion..
 * @Description: TODO
 * @Date: 2020/10/27 14:03
 */


@Entity
@Table(name = "t_log_entity")
@Setter
@Getter
@ToString
public class LogEntity  {

    private static final long serialVersionUID = 963758019652824529L;

    @Id
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator="uuid")
    @Column(length=32)
    private String id;

    @JsonSerialize(using= JsonDateSerialize.class)
    @Column(updatable = false,columnDefinition="datetime comment '创建时间'")
    private Date createTime;

    private String userId;

    private String userName;

    private String methodDesc;

    private String module;

    private String ip;

    private String reqParam;

    private String responseTime;

    private String commit;
    /**
     * 请求方法
     */
    private String method;

}
