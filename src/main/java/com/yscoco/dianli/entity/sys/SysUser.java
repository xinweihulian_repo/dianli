package com.yscoco.dianli.entity.sys;

import io.swagger.annotations.ApiModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import com.yscoco.dianli.common.base.BaseEntity;

@Entity
@Table(name = "t_sys_user")
@ApiModel("管理员")
@Setter
@Getter
@ToString
public class SysUser extends BaseEntity{
	private static final long serialVersionUID = -6002487819294889609L;
   
	@Column(name="token",length=32)
	private String token;
    
    @Column(name="mobilePhone",length=32)
    private String mobilePhone;
    
    @Column(name="position",length=255)
    private String position;
    
    @Column(name="account",length=32)
    private String account;
    
    @Column(name="password",length=255)
    private String password;
    
    @Column(name="permission",length=32)
    private String permission;

    
}