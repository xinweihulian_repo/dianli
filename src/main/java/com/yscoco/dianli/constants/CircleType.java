package com.yscoco.dianli.constants;

public enum CircleType {

	WALK_CIRCLE,//徒友    
	DIVE_CIRCLE,//潜友圈
	RUN_CIRCLE, // 跑友圈
	MY_CIRCLE // 我的社区
}
