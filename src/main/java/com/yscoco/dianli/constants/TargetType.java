package com.yscoco.dianli.constants;

public enum  TargetType { 
	TARGET_STEP ,//目标步数
	TARGET_DISTANCE,//目标距离
	TARGET_KILL,//燃烧脂肪
	TARGET_TIME,//目标时间
	TARGET_HEIGHT,//目标高度
    TARGET_FISH_TIME,//抛竿次数
    TARGET_WEIGHT,//渔获重量
    TARGET_FISH_AMOUNT,//目标尾数
    TARGET_DIVE_DEEP,//目标深度
    TARGET_DIVE_TIME//目标次数
}
