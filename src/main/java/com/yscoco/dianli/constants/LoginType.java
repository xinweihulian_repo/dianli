package com.yscoco.dianli.constants;

public enum LoginType {
	
	WX,
	QQ,
	WB,
	MOBILE,
	EMAIL
}
