package com.yscoco.dianli.constants;

public enum Status {
	/** 正常 */
	NORMAL,

	/** 审核 */
	REVIEW,

	/** 审核未通过 */
	REJECT,
	
	/** 审核通过 */
	PASS,

	/** 删除 */
	REMOVE,

	/** 禁用 */
	DISABLE,

	/** 完成 */
	FINISH,

	/** 取消 */
	CANCEL,

	/** 处理 */
	PROCESS,

	/** 未激活 */
	UNACTIVY,
	
	/** 重试 */
	RETRY,
	
	/** 失败 */
	FAIL
}
