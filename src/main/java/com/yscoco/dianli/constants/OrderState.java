package com.yscoco.dianli.constants;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("订单状态")
public enum OrderState {
	
	@ApiModelProperty("未支付")
	NOT_PAY,
	
	@ApiModelProperty("已支付，带发货")
	PAY,
	
	@ApiModelProperty("配送中")
	LOGISTICS,
	
	@ApiModelProperty("签收")
	SIGN_FOR,	
	
	@ApiModelProperty("订单完成")
	FINISH,	

	@ApiModelProperty("订单取消")
	CANCEL,
	

}
