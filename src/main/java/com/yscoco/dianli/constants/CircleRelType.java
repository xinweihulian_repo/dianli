package com.yscoco.dianli.constants;

public enum CircleRelType {
    LIKE,//点赞
    SHARE,//分享
    COLLECTION,//收藏
    COMMENT//评论
}
