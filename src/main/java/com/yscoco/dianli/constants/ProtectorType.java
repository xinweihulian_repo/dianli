package com.yscoco.dianli.constants;

import lombok.Getter;

/**
 * 
 * @author liuDong.
 *保护器类型
 */
@Getter
public enum ProtectorType {
	/**
	 * 过电压保护器
	 */
	guodianya(1, "过电压保护器"), 
	/**
	 * 温度采集器
	 */
	wendu(2, "温度采集器"),
	/**
	 * 电机设备
	 */
	dianji(3, "电机");
	private int value;
	private String name;

	private ProtectorType(int value, String name) {
		this.value = value;
		this.name = name;
	}

}
