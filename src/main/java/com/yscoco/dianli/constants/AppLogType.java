package com.yscoco.dianli.constants;

public enum AppLogType {
    LOGIN,
    ADD,
    UPDATE,
    DELETE,
    VIEW
}
