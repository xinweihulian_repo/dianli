package com.yscoco.dianli.constants;

public enum MsgType {
    CIRCLE_LIKE,//圈子点赞
    CIRCLE_COMMENT,//圈子收藏
    CIRCLE_COLLECT,//圈子评论
    SYSTEM,//系统消息
    FRIEND_ADD,//添加好友申请
    DRIVER_AUDIT_PASS,//车主认证通过
    DRIVER_AUDIT_FIELD,//车主认证失败
    PASSENGER_AUDIT_PASS,//乘客认证通过
    PASSENGER_AUDIT_FIELD,//乘客认证失败
    SYSTEM_MSG,//系统消息
    ORDER_MSG,//订单消息
    ACTIVE_MSG,//活动消息
    ORDER_CANCEL//取消订单
}
