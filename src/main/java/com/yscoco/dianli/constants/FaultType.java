package com.yscoco.dianli.constants;

/**
 * 故障类型
 * 
 * @author liuDong.
 *
 */
public enum FaultType {
	
	/**
	 * 0 ="全部报警类型"
	 */
	FaultTypeFor0(0,"全部报警类型"),

	/**
	 * 10,12,13,14,15,16 属于高级报警
	 */
	FaultTypeFor10(10, "金属接地故障"), 
	/**
	 * 11  系统过电压 属于中级报警
	 */
	FaultTypeFor11(11, "系统过电压"),
	/**
	 * 10,12,13,14,15,16 属于高级报警
	 */
	FaultTypeFor12(12, "过电压故障"),
	/**
	 * 10,12,13,14,15,16 属于高级报警
	 */
	FaultTypeFor13(13, "PT断线故障"), 
	/**
	 * 10,12,13,14,15,16 属于高级报警
	 */
	FaultTypeFor14(14, "欠压数据包"), 
	/**
	 * 10,12,13,14,15,16 属于高级报警
	 */
	FaultTypeFor15(15, "系统短路"),
	/**
	 * 10,12,13,14,15,16 属于高级报警
	 */
	FaultTypeFor16(16, "弧光接地故障"),
	/**
	 * 过热 属于高级报警
	 */
	FaultTypeFor20(20, "过热"),
	/**
	 * 湿度超标 属于高级报警
	 */
	FaultTypeFor21(21, "湿度超标"),
	/**
	 * 泄漏 属于高级报警
	 */
	FaultTypeFor22(22, "泄漏"),
	/**
	 * 	电量不足   属于中级报警
	 */
	FaultTypeFor23(23, "电量不足"),
	/**
	 *  红外线测温 24  属于高级报警
	 */
	FaultTypeFor24(24, "红外线测温");
	private int value;
	private String name;

	private FaultType(int value, String name) {
		this.value = value;
		this.name = name;
	}

	public int getValue() {
		return value;
	}

	public String getName() {
		return name;
	}
}
