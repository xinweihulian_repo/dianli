package com.yscoco.dianli.constants;

import java.util.ResourceBundle;

/**  
 * 路径管理
 * @Title: PathCons.java
 * @Description: TODO
 * @author lanston
 */
public class ConfigConts {
	private static final ResourceBundle rb = ResourceBundle.getBundle("config");
	public  static final String abUploadPath = rb.getString("FILE_AB_UPLOAD_PATH");


	public static final String MAIL_FROM = rb.getString("mail_from");
	public static final String weather_key = rb.getString("weather_key");
	public static final String weather_url = rb.getString("weather_url");
	
	public static final String GOEASY_HOST = rb.getString("GOEASY_HOST");
	public static final String GOEASY_KEYS = rb.getString("GOEASY_KEYS");
	
	// default
	public final static String DEFDB_SCHEMA = "";

	// default
	public final static String DEFDB_CATALOG = "";
	
	public static String getString(String key){
		return rb.getString(key);
	}
	
}
