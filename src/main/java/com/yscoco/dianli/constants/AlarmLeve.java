package com.yscoco.dianli.constants;

import lombok.Getter;

/**
 * 报警等级
 * @author liuDong.
 *
 */
@Getter
public enum AlarmLeve {
	/**
	 * 中级报警
	 */
	zhongji(1, "中级报警"), 
	/**
	 * 高级报警
	 */
	gaoji(2, "高级报警");

	private int value;
	private String name;

	private AlarmLeve(int value, String name) {
		this.value = value;
		this.name = name;
	}
}
