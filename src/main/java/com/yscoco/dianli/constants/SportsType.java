package com.yscoco.dianli.constants;

public enum  SportsType { 
	WALK ,//健步    
    RUN,//跑步
    BIKE,//骑行
    SWIM,//游泳
    MOUNTAINS,//登山
    FISH,//垂钓
    DIVE,//潜水
    HEART,//心率
    GPS//GPS
}
