package com.yscoco.dianli.constants;

public enum FriendStatus {
    BLACK,//黑名单
    APPLY,//申请
    AGREE,//同意
    CANCEL//取消
}
