package com.yscoco.dianli.constants;

/**
 * 电压等级
 * @author liuDong.
 *
 */
public enum voltageGradesType {
	GradesType0_38("0.38", "0.38KV"), GradesType3("3", "3KV"), GradesType6("6", "6KV"), GradesType10("10",
			"10KV"), GradesType13_8("13.8", "13.8KV"), GradesType20("20", "20KV"),GradesType35("35","35KV");
	private String value;
	private String name;
	private voltageGradesType(String value, String name) {
		this.value = value;
		this.name = name;
	}
	public String getValue() {
		return value;
	}
	
	public String getName() {
		return name;
	}
}
