package com.yscoco.dianli.constants;

/**
 * @author liuDong.
 *
 */
public class WxConstant {
	/**
	 * 登陆渠道标识
	 */
    public static final String  loginChannel="wx";
	public static final String  appid = "wx7a79ee109d5f3702";
    public static final String  secret = "abd7edf5d3958ff253c6c0666788ff84";
    public static final String  grantType="authorization_code";
    
    public static final String  authCode2SessionURL = "https://api.weixin.qq.com/sns/jscode2session";

    public static final String  AES = "AES";
    public static final String  AES_CBC_PADDING = "AES/CBC/PKCS7Padding";
    		
    	

}
