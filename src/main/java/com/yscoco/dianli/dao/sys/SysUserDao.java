package com.yscoco.dianli.dao.sys;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.yscoco.dianli.common.exception.BizException;
import com.yscoco.dianli.common.vo.Code;
import com.yscoco.dianli.dao.base.BaseDao;
import com.yscoco.dianli.entity.company.Company;
import com.yscoco.dianli.entity.member.MemberInfo;
import com.yscoco.dianli.entity.sys.SysUser;

@Repository
public class SysUserDao extends BaseDao<SysUser>{

	public SysUser getByToken(String token) {
		return queryOne("token", token);
	}
	
	public SysUser getByAccount(String account){
		Map<String,Object> params = new HashMap<String, Object>(1);
		params.put("account", account);
		
		return this.getEntity(this.getQueryHandler(" from SysUser t ").condition(params));
	}

	public Boolean countByMemberType() {
		Map<String,Object> params = new HashMap<String, Object>(1);
		params.put("account", "admin");
		List<SysUser> list = this.list(params);
		return (list != null && list.size()>0);
	}
	
	
	/**
	 * 查询出除ADMIN外管理员所负责的公司
	 * @param createBy
	 * @return
	 */
	public List<String> getCompanyIdsByUserId(String createBy){
			String queryData = " select t.id from t_company t  where 1=1 ";

			if(StringUtils.isNotBlank(createBy)){
				queryData += " and t.createBy='" + createBy + "'";
			}else{
				throw new BizException(Code.NO_DATA);
			}
			Session session = this.getSession();
			Query query = session.createSQLQuery(queryData);
			return query.list();
		
	}
 	
}
