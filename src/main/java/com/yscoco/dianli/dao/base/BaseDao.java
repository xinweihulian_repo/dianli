package com.yscoco.dianli.dao.base;

import static org.apache.commons.lang3.time.DateUtils.addDays;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.SystemException;
import javax.transaction.Transaction;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.yscoco.dianli.common.base.Base;
import com.yscoco.dianli.common.base.BaseEntity;
import com.yscoco.dianli.common.handler.QueryHandler;
import com.yscoco.dianli.common.utils.BeanUtils;
import com.yscoco.dianli.common.utils.Log;
@Repository
public abstract class BaseDao<E> extends Base {
    private Class<E> clazz;

    public  Date tomorrow(Date date) {
        return addDays(date, 1);
    }

    public  QueryHandler getQueryHandler(String sql) {
        return new QueryHandler(sql);
    }

    public  QueryHandler getDeleteQueryHandler(String sql) {
        return getQueryHandler("delete").append(sql);
    }

    public  QueryHandler getCountQueryHandler(String sql) {
        return getQueryHandler("select count(*)").append(sql);
    }

    public  QueryHandler getQueryHandler() {
        return new QueryHandler();
    }

    /**
     * @param id
     * @return
     */
    @SuppressWarnings("unchecked")
	public E getEntity(Serializable id) {
        return notEmpty(id) ? (E) getSession().get(getEntityClass(), id) : null;
    }

    /**
     * @param id
     * @return
     */
    public E getEntity(Serializable id, String pk) {
        QueryHandler queryHandler = getQueryHandler("from").append(getEntityClass().getSimpleName()).append("t");
        queryHandler.condition("t." + pk).append("= :id").setParameter("id", id);
        return getEntity(queryHandler);
    }

    /**
     * @param ids
     * @return
     */
    public List<E> getEntitys(Serializable[] ids) {
        return getEntitys(ids, "id");
    }

    /**
     * @param ids
     * @param pk
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<E> getEntitys(Serializable[] ids, String pk) {
        if (notEmpty(ids)) {
            QueryHandler queryHandler = getQueryHandler("from").append(getEntityClass().getSimpleName()).append("t");
            queryHandler.condition("t." + pk).append("in (:ids)").setParameter("ids", ids);
            return (List<E>) getList(queryHandler);
        }
        return new ArrayList<E>();
    }
    
    /**
     * @param ids
     * @param pk
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<E> getEntitys(QueryHandler queryHandler) {
        return (List<E>) getList(queryHandler);
    }

    /**
     * @param entity
     * @return
     */
    public void save(List<E> entitys) {
    	if(entitys==null) return ;
    	Session session = getSession();
    	for (int i = 0; i < entitys.size(); i++) {
    		session.save(init(entitys.get(i)));
    		if(i % 20 ==0){
    			 session.flush();  
                 session.clear(); 
    		}
		}
    }
    /**
     * @param entity
     * @return
     */
    public void save(E[] entitys) {
    	if(entitys==null) return ;
    	Session session = getSession();
    	for (int i = 0; i < entitys.length; i++) {
    		if(entitys[i]!=null){
    			session.save(init(entitys[i]));
    		}
    		
    		if(i % 20 ==0){
    			 session.flush();  
                 session.clear(); 
    		}
		}
    }
    
    /**
     * @param entity
     * @return
     */
    public Serializable save(E entity) {
    	if(entity instanceof BaseEntity){
    		BaseEntity be = (BaseEntity)entity;
    		be.setCreateTime(new Date());
    		be.setId(null);
    	}
        return getSession().save(init(entity));
    }

    /**
     * @param id
     * @return
     */
    public void delete(Serializable id) {
        E entity = getEntity(id);
        if (notEmpty(entity)) {
            getSession().delete(entity);
        }
    }
    /**
     * @param id
     * @return
     */
    public void delete(E entity) {
        if (notEmpty(entity)) {
            getSession().delete(entity);
        }
    }

    /**
     * @param queryHandler
     * @return
     */
    @SuppressWarnings("unchecked")
    public E getEntity(QueryHandler queryHandler) {
        try {
            return (E) getQuery(queryHandler).uniqueResult();
        } catch (Exception e) {
            return (E) getQuery(queryHandler).list().get(0);
        }
    }

    /**
     * @param query
     * @return
     */
    public int update(QueryHandler queryHandler) {
        return getQuery(queryHandler).executeUpdate();
    }

    /**
     * @param query
     * @return
     */
    protected int delete(QueryHandler queryHandler) {
        return update(queryHandler);
    }

    /**
     * @param query
     * @return
     */
    protected List<?> getList(QueryHandler queryHandler) {
        return getQuery(queryHandler).list();
    }
   
    /**
     * @param query
     * @return
     */
    public int countResult(QueryHandler queryHandler) {
    	Query query = getCountQuery(queryHandler);
    	System.out.println( query.iterate().hasNext());
        return ((Number) query.iterate().next()).intValue();
    }
 

    /**
     * @param query
     * @return
     */
    protected int count(QueryHandler queryHandler) {
        return ((Number) getQuery(queryHandler).iterate().next()).intValue();
    }

    private Query getQuery(QueryHandler queryHandler) {
        return queryHandler.getQuery(getSession());
    }

    private Query getCountQuery(QueryHandler queryHandler) {
        return queryHandler.getCountQuery(getSession());
    }
  

    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @SuppressWarnings("unchecked")
    public Class<E> getEntityClass() {
        return empty(clazz) ? this.clazz = (Class<E>) ((ParameterizedType) this.getClass().getGenericSuperclass())
                .getActualTypeArguments()[0] : clazz;
    }

    protected  E init(E entity){
    	return entity;
    }
    
    protected Class<E> getClazz(){
    	return getEntityClass();
    }

    @Autowired
    protected SessionFactory sessionFactory;
    
    
    public List<E> list(Map<String,Object> params){
    	
		QueryHandler query = getQueryHandler("from ") .append(getEntityClass().getSimpleName()).append(" t ");
		query.condition(params);
		query.order(params);
		List<E> list = this.getEntitys(query);
		return list;
	}
    public List<E> listPage(Map<String,Object> params){
    	
  		QueryHandler query = getQueryHandler("from ") .append(getEntityClass().getSimpleName()).append(" t ");
  		query.conditionAndSetPage(params);
  		params.put("order", "createTime DESC");
  		query.order(params);
  		List<E> list = this.getEntitys(query);
  		return list;
  	}
 
    
    public E queryOne(Map<String,Object> params){
    	
 		QueryHandler query = getQueryHandler("from ") .append(getEntityClass().getSimpleName()).append(" t ");
 		query.condition(params);
 		query.order(params);
 		List<E> list = this.getEntitys(query);
 		if(list != null && list.size()>0){
 			return list.get(0);
 		}
 		return null;
 	}
    public E queryOne(String key,String value){
    	if(StringUtils.isEmpty(value)) return null;
    	Map<String,Object> params = new HashMap<String, Object>();
    	params.put(key, value);
  		QueryHandler query = getQueryHandler("from ") .append(getEntityClass().getSimpleName()).append(" t ");
  		query.condition(params);
  		List<E> list = this.getEntitys(query);
  		if(list != null && list.size()>0){
  			return list.get(0);
  		}
  		return null;
  	}
    
    
  
    
	
	public int count(Map<String,Object> params){
		QueryHandler query = getQueryHandler("from ") .append(getEntityClass().getSimpleName()).append(" t ");
		query.condition(params);
		int count = countResult(query);
		return count;
	}
	
	public <T> T toDto(E vo,Class<T> clz){
		T dto = null;
		try {
			dto = clz.newInstance();
			BeanUtils.copyProperties(vo, dto);
		} catch (InstantiationException | IllegalAccessException e) {
			Log.getCommon().error("转换dto失败",e);
		}
		
		return dto;
	}
	public <T> List<T> toDto(List<E> vos,Class<T> clz){
		List<T> dtos = new ArrayList<T>();
		for (E e : vos) {
			T dto = toDto(e, clz);
			dtos.add(dto);
		}
		return dtos;
	}
	
   public <T> List<T> listToDto(Map<String,Object> params,Class<T> clz){
		QueryHandler query = getQueryHandler("from ") .append(getEntityClass().getSimpleName()).append(" t ");
		query.condition(params);
		query.order(params);
		List<E> list = this.getEntitys(query);
		return toDto(list, clz);
	}
   
   public void update(E entity) {
	   getSession().update(entity);
   }
}
