package com.yscoco.dianli.dao.app;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import com.yscoco.dianli.dao.base.BaseDao;
import com.yscoco.dianli.entity.app.AppVersion;

@Repository
public class AppVersionDao extends BaseDao<AppVersion> {
 
	public AppVersion getNewest(String identify){
		 Map<String,Object> params =new HashMap<>();
		if(identify!=null||StringUtils.isNotEmpty(identify)){
			params.put("identify", identify);
		}
		
		 params.put("order", "t.createTime desc");
		 AppVersion queryOne = this.queryOne(params);
		 return queryOne;
	}

	
}
