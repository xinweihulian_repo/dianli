package com.yscoco.dianli.dao.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.yscoco.dianli.constants.AppLogType;
import com.yscoco.dianli.dao.base.BaseDao;
import com.yscoco.dianli.dao.sys.SysUserDao;
import com.yscoco.dianli.entity.app.AppLog;
import com.yscoco.dianli.entity.app.AppVersion;
import com.yscoco.dianli.entity.company.Company;
import com.yscoco.dianli.entity.company.LinesOnBranch;
import com.yscoco.dianli.entity.company.LinesOnMaster;
import com.yscoco.dianli.entity.company.MonitorPoint;
import com.yscoco.dianli.entity.company.MonitorStation;
import com.yscoco.dianli.entity.member.MemberInfo;
import com.yscoco.dianli.entity.msg.MsgPush;
import com.yscoco.dianli.entity.option.Feedback;
import com.yscoco.dianli.entity.sys.SysUser;

@Repository
public class AppLogDao extends BaseDao<AppLog> {
	
	@Autowired
	SysUserDao sysUserDao ;

	public void recordIpInfo(String ipAddress, MemberInfo mi, SysUser user) {
		AppLog log = new AppLog();
		baseRecordIpInfo(ipAddress, user, log);
		if(mi != null ){
			log.setLoginType("前台");
		}else if(user != null ) {
			log.setLoginType("后台");
		}
		this.save(log);// 记录后天登录的日志
	}

	private void baseRecordIpInfo(String ipAddress, SysUser user, AppLog log) {
		if(user != null){
			log.setCreateBy(user.getId());
			log.setAccount(user.getAccount());
		}
		log.setCreateTime(getDate());
		log.setIpAddress(ipAddress);
	}
	
	public void recordAddOrUpdateSysUser(String ipAddress, MemberInfo mi, SysUser user,AppLogType appLogType) {
		AppLog log = new AppLog() ;
		// 记录前台登录的日志
		SysUser actionSysUer = null;
		if(user != null){
			if(appLogType == AppLogType.ADD){
				actionSysUer = this.sysUserDao.getEntity(user.getCreateBy());// 新增人员信息
				log = baseSaveAppLog(ipAddress, appLogType, actionSysUer,user);
				log.setLoginType("后台");
				this.save(log);
			}else if(appLogType == AppLogType.UPDATE){
				actionSysUer = this.sysUserDao.getEntity(user.getModifiedBy());// 修改信息
				log = baseSaveAppLog(ipAddress, appLogType, actionSysUer,user);
				log.setLoginType("后台");
				this.save(log);
			}
		} else if(mi != null){
			if(appLogType == AppLogType.ADD){
				actionSysUer = this.sysUserDao.getEntity(mi.getCreateBy());// 新增人员信息
				log = baseSaveAppLog(ipAddress, appLogType, actionSysUer,user);
			}else if(appLogType == AppLogType.UPDATE){
				actionSysUer = this.sysUserDao.getEntity(mi.getModifiedBy());// 修改信息
				log = baseSaveAppLog(ipAddress, appLogType, actionSysUer,user);
			}
			log.setLoginType("前台");
			this.save(log);
		}
	}

	private AppLog baseSaveAppLog(String ipAddress, AppLogType appLogType,SysUser actionSysUer,SysUser user) {
		AppLog log = new AppLog() ;
		log = baseRecordDeleteLog(ipAddress, actionSysUer, appLogType, log);
		if(user != null){
			log.setTargetId(user.getId());
			log.setTargetAccount(user.getAccount());
		}
		return log;
	}
	
	
	private AppLog baseRecordDeleteLog(String addressIp, SysUser actionSysUser,AppLogType appLogType, AppLog log) {
			log.setCreateTime(getDate());
			log.setCreateBy(actionSysUser.getId());
			log.setIpAddress(addressIp);
			log.setAccount(actionSysUser.getAccount());
			log.setMobilePhone(actionSysUser.getMobilePhone());
			log.setAppLogType(appLogType);
			return log;
	}
	
//	public void recordAddOrUpdateRole(String ipAddress,String managerId,AppLogType appLogType,SysRole role) {
//		AppLog log = new AppLog() ;
//		// 记录前台登录的日志
//		SysUser actionSysUser = this.sysUserDao.getEntity(managerId);
//		if(actionSysUser != null){
//			if(appLogType == AppLogType.ADD){
//				log = baseRecordDeleteLog(ipAddress,actionSysUser, appLogType,log);
//				log.setLoginType("后台");
//				log.setContent("角色新增");
//				
//			}else if(appLogType == AppLogType.UPDATE){
//				log = baseRecordDeleteLog(ipAddress,actionSysUser, appLogType,log);
//				log.setLoginType("后台");
//				log.setContent("角色更新");
//			 
//			}else if(appLogType == AppLogType.DELETE){
//				log = baseRecordDeleteLog(ipAddress,actionSysUser, appLogType,log);
//				log.setLoginType("后台");
//				log.setContent("角色删除");
//			}
//		} 
//		log.setTargetId(role.getId());
//		log.setTargetAccount(role.getName());
//		this.save(log);
//	}
	
	//    公司 
	public void recordAddOrUpdateCompany(String ipAddress,String managerId,AppLogType appLogType,Company company) {
		AppLog log = new AppLog() ;
		SysUser createManager = this.sysUserDao.getEntity(managerId);
		if(createManager != null){
			if(appLogType == AppLogType.ADD){
				log = baseRecordDeleteLog(ipAddress,createManager, appLogType,log);
				log.setLoginType("后台");
				log.setContent("公司新增");
				
			}else if(appLogType == AppLogType.UPDATE){
				log = baseRecordDeleteLog(ipAddress,createManager, appLogType,log);
				log.setLoginType("后台");
				log.setContent("公司更新");
			 
			}else if(appLogType == AppLogType.DELETE){
				log = baseRecordDeleteLog(ipAddress,createManager, appLogType,log);
				log.setLoginType("后台");
				log.setContent("公司删除");
			}
		} 
		log.setTargetId(company.getId());
		log.setTargetAccount(company.getCompanyName());
		this.save(log);
	}
	

	//   监测站
	public void recordAddOrUpdateMonitorStation(String addressIp,String managerId,AppLogType appLogType,MonitorStation monitorStation) {
		AppLog log = new AppLog() ;
		SysUser createManager = this.sysUserDao.getEntity(managerId);
		if(createManager != null){
			if(appLogType == AppLogType.ADD){
				log = baseRecordDeleteLog(addressIp,createManager, appLogType,log);
				log.setLoginType("后台");
				log.setContent("监测站新增");
				
			}else if(appLogType == AppLogType.UPDATE){
				log = baseRecordDeleteLog(addressIp,createManager, appLogType,log);
				log.setLoginType("后台");
				log.setContent("监测站更新");
			 
			}else if(appLogType == AppLogType.DELETE){
				log = baseRecordDeleteLog(addressIp,createManager, appLogType,log);
				log.setLoginType("后台");
				log.setContent("监测站删除");
			}
		} 
		log.setTargetId(monitorStation.getId());
		log.setTargetAccount(monitorStation.getStationName());
		this.save(log);
	}
	
	//  母线
	public void recordActionOnLinesOnMaster(String addressIp,String managerId,AppLogType appLogType,LinesOnMaster linesOnMaster) {
		AppLog log = new AppLog() ;
		SysUser createManager = this.sysUserDao.getEntity(managerId);
		if(createManager != null){
			if(appLogType == AppLogType.ADD){
				log = baseRecordDeleteLog(addressIp,createManager, appLogType,log);
				log.setLoginType("后台");
				log.setContent("母线新增");
				
			}else if(appLogType == AppLogType.UPDATE){
				log = baseRecordDeleteLog(addressIp,createManager, appLogType,log);
				log.setLoginType("后台");
				log.setContent("母线更新");
			 
			}else if(appLogType == AppLogType.DELETE){
				log = baseRecordDeleteLog(addressIp,createManager, appLogType,log);
				log.setLoginType("后台");
				log.setContent("母线删除");
			}
		} 
		log.setTargetId(linesOnMaster.getId());
		log.setTargetAccount(linesOnMaster.getMasterName());
		this.save(log);
	}
	
	// 支线
	public void recordActionOnLinesOnBranch(String addressIp,String managerId,AppLogType appLogType,LinesOnBranch linesOnBranch) {
		AppLog log = new AppLog() ;
		SysUser createManager = this.sysUserDao.getEntity(managerId);
		if(createManager != null){
			if(appLogType == AppLogType.ADD){
				log = baseRecordDeleteLog(addressIp,createManager, appLogType,log);
				log.setLoginType("后台");
				log.setContent("支线新增");
				
			}else if(appLogType == AppLogType.UPDATE){
				log = baseRecordDeleteLog(addressIp,createManager, appLogType,log);
				log.setLoginType("后台");
				log.setContent("支线更新");
			 
			}else if(appLogType == AppLogType.DELETE){
				log = baseRecordDeleteLog(addressIp,createManager, appLogType,log);
				log.setLoginType("后台");
				log.setContent("支线删除");
			}
		} 
		log.setTargetId(linesOnBranch.getId());
		log.setTargetAccount(linesOnBranch.getBranchName());
		this.save(log);
	}
	
	// 监测点
	public void recordActionOnMonitorPoint(String addressIp,String managerId,AppLogType appLogType,MonitorPoint monitorPoint) {
		AppLog log = new AppLog() ;
		SysUser createManager = this.sysUserDao.getEntity(managerId);
		if(createManager != null){
			if(appLogType == AppLogType.ADD){
				log = baseRecordDeleteLog(addressIp,createManager, appLogType,log);
				log.setLoginType("后台");
				log.setContent("监测点新增");
				
			}else if(appLogType == AppLogType.UPDATE){
				log = baseRecordDeleteLog(addressIp,createManager, appLogType,log);
				log.setLoginType("后台");
				log.setContent("监测点更新");
			 
			}else if(appLogType == AppLogType.DELETE){
				log = baseRecordDeleteLog(addressIp,createManager, appLogType,log);
				log.setLoginType("后台");
				log.setContent("监测点删除");
			}
		} 
		log.setTargetId(monitorPoint.getId());
		log.setTargetAccount(monitorPoint.getPointName());
		this.save(log);
	}
	
	// 监测点
	public void recordActionOnAppVersion(String addressIp,String managerId,AppLogType appLogType,AppVersion appVersion) {
		AppLog log = new AppLog() ;
		SysUser createManager = this.sysUserDao.getEntity(managerId);
		if(createManager != null){
			if(appLogType == AppLogType.ADD){
				log = baseRecordDeleteLog(addressIp,createManager, appLogType,log);
				log.setLoginType("后台");
				log.setContent("母线新增");
				
			}else if(appLogType == AppLogType.UPDATE){
				log = baseRecordDeleteLog(addressIp,createManager, appLogType,log);
				log.setLoginType("后台");
				log.setContent("母线更新");
			 
			}else if(appLogType == AppLogType.DELETE){
				log = baseRecordDeleteLog(addressIp,createManager, appLogType,log);
				log.setLoginType("后台");
				log.setContent("母线删除");
			}
		} 
		log.setTargetId(appVersion.getId());
		log.setTargetAccount(appVersion.getVersionName());
		this.save(log);
	}
	
	// 消息
	public void recordActionOnMsgPush(String addressIp,String managerId,AppLogType appLogType,MsgPush push) {
		AppLog log = new AppLog() ;
		SysUser createManager = this.sysUserDao.getEntity(managerId);
		if(createManager != null){
			if(appLogType == AppLogType.ADD){
				log = baseRecordDeleteLog(addressIp,createManager, appLogType,log);
				log.setLoginType("后台");
				log.setContent("消息新增");
				
			}else if(appLogType == AppLogType.VIEW){
				log = baseRecordDeleteLog(addressIp,createManager, appLogType,log);
				log.setLoginType("后台");
				log.setContent("消息查看");
			 
			}else if(appLogType == AppLogType.DELETE){
				log = baseRecordDeleteLog(addressIp,createManager, appLogType,log);
				log.setLoginType("后台");
				log.setContent("消息删除");
			}
		} 
		log.setTargetId(push.getId());
		log.setTargetAccount(push.getContent());
		this.save(log);
	}
	
	// 意见反馈
	public void recordActionOnFeedback(String addressIp,String managerId,AppLogType appLogType,Feedback feedBack) {
		AppLog log = new AppLog() ;
		SysUser createManager = this.sysUserDao.getEntity(managerId);
		if(createManager != null){
			if(appLogType == AppLogType.ADD){
				log = baseRecordDeleteLog(addressIp,createManager, appLogType,log);
				log.setLoginType("前台");
				log.setContent("意见反馈新增");
				
			}else if(appLogType == AppLogType.UPDATE){
				log = baseRecordDeleteLog(addressIp,createManager, appLogType,log);
				log.setLoginType("后台");
				log.setContent("意见反馈更新");
			 
			}else if(appLogType == AppLogType.DELETE){
				log = baseRecordDeleteLog(addressIp,createManager, appLogType,log);
				log.setLoginType("后台");
				log.setContent("意见反馈删除");
			}
		} 
		log.setTargetId(feedBack.getId());
		log.setTargetAccount(feedBack.getContent());
		this.save(log);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
