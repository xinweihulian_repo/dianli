package com.yscoco.dianli.dao.app;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.yscoco.dianli.common.vo.VcodeType;
import com.yscoco.dianli.dao.base.BaseDao;
import com.yscoco.dianli.entity.app.VcodeVerify;

@Repository
public class VcodeVerifyDao extends BaseDao<VcodeVerify> {

	@Override
	protected VcodeVerify init(VcodeVerify entity) {
		return entity;
	}

	@Override
	protected Class<VcodeVerify> getClazz() {
		return VcodeVerify.class;
	}

	public VcodeVerify findByVcode(String verifyKey, VcodeType vcodeType, String vcode) {
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("verifyKey", verifyKey);
		params.put("vcodeValue", vcode);
		List<VcodeVerify> l = this.list(params);
		if(l!=null && l.size()>0){
			return l.get(0);
		}
		return null;
	}
	
	public void deleteByKey(String verifyKey){
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("verifyKey", verifyKey);
		List<VcodeVerify> l = this.list(params);
		for (VcodeVerify vcodeVerify : l) {
			delete(vcodeVerify);
		}
	}

	
	
}
