package com.yscoco.dianli.dao.option;

import org.springframework.stereotype.Repository;

import com.yscoco.dianli.dao.base.BaseDao;
import com.yscoco.dianli.entity.option.Feedback;

@Repository
public class FeedbackDao extends BaseDao<Feedback> {

}
