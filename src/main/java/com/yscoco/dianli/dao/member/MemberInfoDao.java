package com.yscoco.dianli.dao.member;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.yscoco.dianli.common.exception.BizException;
import com.yscoco.dianli.common.utils.DateUtils;
import com.yscoco.dianli.common.vo.Code;
import com.yscoco.dianli.dao.base.BaseDao;
import com.yscoco.dianli.entity.member.MemberInfo;
import com.yscoco.dianli.pojo.MemberModel;

@Repository
public class MemberInfoDao extends BaseDao<MemberInfo> {
	
	public MemberInfo getByToken(String token) {
		return queryOne("token", token);
	}

	@SuppressWarnings("unchecked")
	public List<String> query2dayNotLogin(){
		Session session = this.getSession();
		
		Query query =session.createSQLQuery(" select t.id from t_member_info t where t.lastlogin_time <=?  and t.push_twoday_notlogin='Y'")
				.setString(0, DateUtils.getAfterDayDate(-2));
		List<String> l = query.list();
		return l;
	}
	
	// 通过pushId查询用户
	public String getPushToMemberId(String pushId){
		 MemberInfo toPeople = this.queryOne("pushId", pushId);
		 return toPeople.getId();
	}
	
	
	/**
	 * 根据公司id查询该公司下的所有巡检员ID 列表
	 * @param companyId
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<String> queryAllMemberIds(String companyId) {
		String queryData = " select t.id from t_member_info t  where 1=1 ";
		if (StringUtils.isNotEmpty(companyId)) { 
				queryData += " and t.companyId='" + companyId + "'";
		}else{
			throw new BizException(Code.NO_DATA);
		}
		Session session = this.getSession();
		Query query = session.createSQLQuery(queryData);
		return query.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<MemberModel> queryAllMemberByCompanyId(String companyId) {
		String queryData = " select t.id,t.userName,t.monitorStationId FROM t_member_info t  where 1=1 ";
		if (StringUtils.isNotEmpty(companyId)) {
				queryData += " and t.companyId='" + companyId + "'";
		}else{
			throw new BizException(Code.NO_DATAS);
		}

		List<MemberModel> result=new ArrayList<>();
		MemberModel mmm=null ;
	    List<Object> list = getSession().createSQLQuery(queryData).list();
		for(int i=0;i<list.size();i++){
			mmm = new MemberModel();
			Object[] obj = (Object[]) list.get(i);
			for (int j = 0; j < obj.length; j++) {
				mmm.setId(obj[0].toString());
				mmm.setUserName(obj[1].toString());
				mmm.setMonitorStationId(obj[2]==null?"":obj[2].toString());
			}
			result.add(mmm);
		}
		return result;
	}


	
}
