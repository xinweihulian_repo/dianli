package com.yscoco.dianli.dao.company;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.yscoco.dianli.common.exception.BizException;
import com.yscoco.dianli.common.vo.Code;
import com.yscoco.dianli.dao.base.BaseDao;
import com.yscoco.dianli.dao.member.MemberInfoDao;
import com.yscoco.dianli.entity.company.MonitorPoint;
import com.yscoco.dianli.entity.member.MemberInfo;
import com.yscoco.dianli.pojo.MasterModel;
import com.yscoco.dianli.pojo.PointModel;

@Repository
public class MonitorPointDao extends BaseDao<MonitorPoint> {
	
	@Autowired
	private MemberInfoDao memberInfoDao;
	
	public void deleteMonitorPoints(List<MonitorPoint> monitorPoints){
		for (MonitorPoint monitorPoint : monitorPoints) {
			delete(monitorPoint);
		}
	}
	
	
	
	public List<PointModel> queryByPrincipal(String token){
		List<PointModel> result =  new ArrayList<>();
		List<PointModel> queryAllPointFlag = queryAllPointFlag(null);
		MemberInfo member = memberInfoDao.getByToken(token);
		if(member==null){
			throw new BizException(Code.NOT_LOGIN);
		}
		List<String> monitorStationIds = Arrays.asList(member.getMonitorStationId().split(","));
		monitorStationIds.forEach(k->{
			for(int i=0;i<queryAllPointFlag.size();i++){
				if(queryAllPointFlag.get(i).getStationId().equals(k)){
					result.add(queryAllPointFlag.get(i));
				}
			}
		});	
		return result;
	}
	
	public List<PointModel> queryAllPointFlag(String companyId) {
		Session session = this.getSession();
		List<PointModel> pointList = null;
		try {
			String sql = " SELECT  s.id as stationId, s.stationName,m.id as masterId, m.monitorFlag as masterFlag,m.masterName,b.id as branchId,b.branchName,p.pointName ,p.monitorFlag as pointFlag , p.hitchType , p.position , p.type,c.id as companyId,c.companyName,s.stationAddress,p.id as id FROM t_monitor_point as p LEFT JOIN t_lines_on_branch b on(b.id=p.linesOnBranchId) LEFT JOIN t_lines_on_master m on(m.id=b.linesOnMasterId) LEFT JOIN t_monitor_station s on(s.id=m.monitorStationId) LEFT JOIN t_company c on (c.id=s.companyId) WHERE 1=1";
			//companyId = "402881eb6458a651016458c0cd770007";
			if(StringUtils.isNotBlank(companyId)){
			sql += " and s.companyId='" + companyId + "'";
			}
			pointList = new ArrayList<>();
			Query createSQLQuery = session.createSQLQuery(sql);
			@SuppressWarnings("unchecked")
			List<Object> list = createSQLQuery.list();
			PointModel 	model =null;
			if (list != null&&!list.isEmpty()) {
				for (int i = 0; i < list.size(); i++) {
					 	model = new PointModel();
					Object[] obj = (Object[]) list.get(i);// obj中保存的是查询出的对象的属性值
					for (int j = 0; j < obj.length; j++) {
						model.setStationId(obj[0].toString());
						model.setStationName(obj[1].toString());
						model.setMasterId(obj[2].toString());
						model.setMasterFlag(obj[3].toString());
						model.setMasterName(obj[4].toString());
						model.setBranchId(obj[5].toString());
						model.setBranchName(obj[6].toString());
						model.setPointName(obj[7].toString());
						model.setPointFlag(obj[8].toString());
						model.setHitchType(obj[9].toString());
						model.setPosition(obj[10].toString());
						if(obj[11]!=null){
							model.setType(obj[11].toString());
						}
						model.setCompanyId(obj[12].toString());
						model.setCompanyName(obj[13].toString());
						model.setStationAddress(obj[14].toString());
						model.setId(obj[15].toString());
					}
					pointList.add(model);
				}
				return pointList;
			} else {
//				throw new BizException(Code.NO_DATA);
				return new ArrayList<>();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		// System.out.println("pointList:" + pointList);
		return pointList;
	}
	
	
	public List<MasterModel> queryAllMasterFlag(String companyId) {
		Session session = this.getSession();
		List<MasterModel> masterList = null;
		try {
			String sql = " SELECT m.id,m.masterName,m.monitorFlag,s.id as stationId,s.stationName,c.id as companyId,c.companyName,s.stationAddress FROM t_lines_on_master m  LEFT JOIN t_monitor_station s on(s.id=m.monitorStationId) LEFT JOIN t_company c on (c.id=s.companyId) WHERE 1=1 ";
		//	companyId = "402881eb6458a651016458c0cd770007";
			if(StringUtils.isNotBlank(companyId)){
				sql += " and s.companyId='" + companyId + "'";
			}
			masterList = new ArrayList<>();
			Query createSQLQuery = session.createSQLQuery(sql);
			@SuppressWarnings("unchecked")
			List<Object> list = createSQLQuery.list();
			MasterModel model=null;
			if (!list.isEmpty() && list != null) {
				for (int i = 0; i < list.size(); i++) {
					   model = new MasterModel();
					Object[] obj = (Object[]) list.get(i);// obj中保存的是查询出的对象的属性值
					for (int j = 0; j < obj.length; j++) {
						model.setId(obj[0].toString());
						model.setMasterName(obj[1].toString());
						model.setMonitorFlag(obj[2].toString());
						model.setStationId(obj[3].toString());
						model.setStationName(obj[4].toString());
						model.setCompanyId(obj[5].toString());
						model.setCompanyName(obj[6].toString());
						model.setStationAddress(obj[7].toString());
					}
					masterList.add(model);
				}
				// System.out.println("masterList:" + masterList);
				return masterList;
			} else {
			//	throw new BizException(Code.NO_DATA);
				return new ArrayList<>();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 

		return masterList;
	}
	
	  
}
