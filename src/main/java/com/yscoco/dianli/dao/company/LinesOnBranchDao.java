package com.yscoco.dianli.dao.company;

import java.util.Set;

import org.springframework.stereotype.Repository;

import com.yscoco.dianli.dao.base.BaseDao;
import com.yscoco.dianli.entity.company.LinesOnBranch;

@Repository
public class LinesOnBranchDao extends BaseDao<LinesOnBranch> {
	
	public void deleteLinesOnBranchs(Set<LinesOnBranch> linesOnBranchs){
		for (LinesOnBranch linesOnBranch : linesOnBranchs) {
			delete(linesOnBranch);
		}
	}
	
   
}
