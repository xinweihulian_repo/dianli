package com.yscoco.dianli.dao.company;

import org.springframework.stereotype.Repository;

import com.yscoco.dianli.dao.base.BaseDao;
import com.yscoco.dianli.entity.company.ServerFault;

@Repository
public class ServerFaultDao extends BaseDao<ServerFault> {

}
