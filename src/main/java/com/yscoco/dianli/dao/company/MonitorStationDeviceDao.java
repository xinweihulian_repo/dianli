package com.yscoco.dianli.dao.company;

import java.util.Set;

import org.springframework.stereotype.Repository;

import com.yscoco.dianli.dao.base.BaseDao;
import com.yscoco.dianli.entity.company.MonitorStationDevice;

@Repository
public class MonitorStationDeviceDao extends BaseDao<MonitorStationDevice> {
   
	public void deleteMonitorStation(Set<MonitorStationDevice> monitorStations){
		for (MonitorStationDevice monitorStation : monitorStations) {
			delete(monitorStation);
		}
	}
	
}
