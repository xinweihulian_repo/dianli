package com.yscoco.dianli.dao.company;

import java.util.Set;

import org.springframework.stereotype.Repository;

import com.yscoco.dianli.dao.base.BaseDao;
import com.yscoco.dianli.entity.company.MonitorStation;

@Repository
public class MonitorStationDao extends BaseDao<MonitorStation> {
   
	public void deleteMonitorStation(Set<MonitorStation> monitorStations){
		for (MonitorStation monitorStation : monitorStations) {
			delete(monitorStation);
		}
	}
	
}
