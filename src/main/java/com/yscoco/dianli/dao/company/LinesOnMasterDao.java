package com.yscoco.dianli.dao.company;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.yscoco.dianli.dao.base.BaseDao;
import com.yscoco.dianli.entity.company.LinesOnMaster;
import com.yscoco.dianli.entity.company.MonitorStation;

@Repository
public class LinesOnMasterDao extends BaseDao<LinesOnMaster> {
	
	@Autowired
	private MonitorStationDao monitorStationDao;
   
	public void deleteLinesOnMaster(Set<LinesOnMaster> linesOnMasters){
		for (LinesOnMaster linesOnMaster : linesOnMasters) {
			delete(linesOnMaster);
		}
	}
	
	public LinesOnMaster getOneFirstLinesOnMaster(String monitorStationId){
		MonitorStation monitorStation = this.monitorStationDao.getEntity(monitorStationId);
		Map<String,Object> params = new HashMap<>();
		params.put("monitorStation", monitorStation);
		List<LinesOnMaster> list = this.list(params);
		for (LinesOnMaster linesOnMaster : list) {
			if(empty(linesOnMaster.getPreMasterId())){
				return linesOnMaster;
			}
		}
		return null;
	}
	
	public LinesOnMaster getOneOldLinesOnMaster(String monitorStationId){
		MonitorStation monitorStation = this.monitorStationDao.getEntity(monitorStationId);
		Map<String,Object> params = new HashMap<>();
		params.put("monitorStation", monitorStation);
		List<LinesOnMaster> list = this.list(params);
		for (LinesOnMaster linesOnMaster : list) {
			if(empty(linesOnMaster.getNextMasterId())){
				return linesOnMaster;
			}
		}
		return null;
	}
}
