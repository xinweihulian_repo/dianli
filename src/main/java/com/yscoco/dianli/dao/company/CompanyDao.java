package com.yscoco.dianli.dao.company;

import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSON;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.yscoco.dianli.common.exception.BizException;
import com.yscoco.dianli.common.vo.Code;
import com.yscoco.dianli.dao.base.BaseDao;
import com.yscoco.dianli.dao.member.MemberInfoDao;
import com.yscoco.dianli.dao.sys.SysUserDao;
import com.yscoco.dianli.entity.company.Company;
import com.yscoco.dianli.entity.member.MemberInfo;
import com.yscoco.dianli.entity.sys.SysUser;
import com.yscoco.dianli.pojo.MasterModel;
import com.yscoco.dianli.pojo.PointModel;

@Repository
public class CompanyDao extends BaseDao<Company> {

	@Autowired
	private SysUserDao sysUserDao;
	@Autowired
	private MemberInfoDao memberInfoDao;

	@SuppressWarnings("unchecked")
	public List<String> queryAllCompanyId(String createBy) {
		Session session = this.getSession();
		Query query =null;
		String queryData = " select t.id from t_company t  where 1=1 ";
		try{
			if (StringUtils.isNotEmpty(createBy)) { // 除了admin超级管理员外 管理员只能看到自己创建的公司
				SysUser SysUser_create = this.sysUserDao.queryOne("id", createBy);
				if (SysUser_create != null && notEmpty(SysUser_create.getPermission())
						&& !SysUser_create.getPermission().equalsIgnoreCase("admin")) {
					queryData += " and t.createBy='" + createBy + "'";
				}
				MemberInfo memberInfo = this.memberInfoDao.queryOne("id", createBy);
				if (memberInfo != null && !memberInfo.getUserName().equalsIgnoreCase("admin")) {
					queryData += " and t.id='" + memberInfo.getCompanyId() + "'";
				}
			}
			query = session.createSQLQuery(queryData);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return query.list();
	}

	public List<MasterModel> queryAllMasterFlag(String companyId) {
		Session session = this.getSession();
		List<MasterModel> masterList = null;
		try {
			String sql = "SELECT m.id,m.masterName,m.monitorFlag,s.id as stationId,s.stationName,c.id as companyId,c.companyName,s.stationAddress,s.stationType FROM t_lines_on_master m  LEFT JOIN t_monitor_station s on(s.id=m.monitorStationId) LEFT JOIN t_company c on (c.id=s.companyId) WHERE 1=1";
		//	companyId = "402881eb6458a651016458c0cd770007";
			if(StringUtils.isNotBlank(companyId)){
				sql += " and s.companyId='" + companyId + "'";
			}
			masterList = new ArrayList<>();
			Query createSQLQuery = session.createSQLQuery(sql);
			MasterModel model=null;
			@SuppressWarnings("unchecked")
			List<Object> list = createSQLQuery.list();
			if (!list.isEmpty() && list != null) {
				for (int i = 0; i < list.size(); i++) {
					    model = new MasterModel();
					Object[] obj = (Object[]) list.get(i);// obj中保存的是查询出的对象的属性值
					for (int j = 0; j < obj.length; j++) {
						model.setId(obj[0].toString());
						model.setMasterName(obj[1].toString());
						model.setMonitorFlag(obj[2].toString());
						model.setStationId(null==obj[3]?"":obj[3].toString());
						model.setStationName(null==obj[4]?"":obj[4].toString());
						model.setCompanyId(null==obj[5]?"":obj[5].toString());
						model.setCompanyName(null==obj[6]?"":obj[6].toString());
						model.setStationAddress(null==obj[7]?"":obj[7].toString());
						model.setStationType(null==obj[8]?"":obj[8].toString());
						//model.setStationName(obj[4].toString());
//						model.setCompanyId(obj[5].toString());
//						model.setCompanyName(obj[6].toString());
//						model.setStationAddress(obj[7].toString());
					}
					masterList.add(model);
				}
				// System.out.println("masterList:" + masterList);
				return masterList;
			} else {
			//	throw new BizException(Code.NO_DATA);
				return new ArrayList<>();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.err.println("masterList:" + JSON.toJSON(masterList));
		return masterList;
	}

	public List<PointModel> queryAllPointFlag(String companyId) {
		Session session = this.getSession();
		List<PointModel> pointList = null;
		try {
			String sql = " SELECT  s.id as stationId, s.stationName,m.id as masterId, m.monitorFlag as masterFlag,m.masterName,b.id as branchId,b.branchName,p.pointName ,p.monitorFlag as pointFlag , p.hitchType , p.position , p.type,c.id as companyId,c.companyName,s.stationAddress,b.cabin_no,s.stationType,p.id FROM t_monitor_point as p LEFT JOIN t_lines_on_branch b on(b.id=p.linesOnBranchId) LEFT JOIN t_lines_on_master m on(m.id=b.linesOnMasterId) LEFT JOIN t_monitor_station s on(s.id=m.monitorStationId) LEFT JOIN t_company c on (c.id=s.companyId) WHERE 1=1";
			//companyId = "402881eb6458a651016458c0cd770007";
			if(StringUtils.isNotBlank(companyId)){
			sql += " and s.companyId='" + companyId + "'";
			}
			pointList = new ArrayList<>();
			Query createSQLQuery = session.createSQLQuery(sql);
			PointModel model =null;
			@SuppressWarnings("unchecked")
			List<Object> list = createSQLQuery.list();
			if (!list.isEmpty() && list != null) {
				for (int i = 0; i < list.size(); i++) {
					   model = new PointModel();
					Object[] obj = (Object[]) list.get(i);// obj中保存的是查询出的对象的属性值
					for (int j = 0; j < obj.length; j++) {
						//model.setStationId(obj[0].toString());
						model.setStationId(null==obj[0]?"":obj[0].toString());
						model.setStationName(obj[1].toString());
						model.setMasterId(obj[2].toString());
						model.setMasterFlag(obj[3].toString());
						model.setMasterName(obj[4].toString());
						model.setBranchId(obj[5].toString());
						model.setBranchName(obj[6].toString());
						model.setPointName(obj[7].toString());
						model.setPointFlag(obj[8].toString());
						model.setHitchType(obj[9].toString());
						model.setPosition(obj[10].toString());
						//if(obj[11]!=null){
						//	model.setType(obj[11].toString());
							model.setType(null==obj[11]?"":obj[11].toString());
						//}
						model.setCompanyId(obj[12].toString());
						model.setCompanyName(obj[13].toString());
						model.setStationAddress(obj[14].toString());
						model.setCabin_no(obj[15].toString());
						model.setStationType(null==obj[16]?"":obj[16].toString());
						model.setId(obj[17].toString());
					}
					pointList.add(model);
				}
				return pointList;
			} else {
//				throw new BizException(Code.NO_DATA);
				return new ArrayList<>();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 

		 System.err.println("pointList:" + JSON.toJSON(pointList));
		return pointList;
	}

}
