package com.yscoco.dianli.dao.company;

import com.yscoco.dianli.dao.base.BaseDao;
import com.yscoco.dianli.entity.company.DianJiMaintainRecord;
import org.springframework.stereotype.Repository;


/**
 * @Author: onion..
 * @Description: TODO
 * @Date: 2020/8/19 16:53
 */
@Repository
public class DianJiMaintainDao extends BaseDao<DianJiMaintainRecord> {

}
