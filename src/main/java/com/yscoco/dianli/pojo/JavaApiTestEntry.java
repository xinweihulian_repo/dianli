package com.yscoco.dianli.pojo;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.yscoco.dianli.common.utils.BeanUtils;


/**
 * 
 * @author liuDong.
 *
 */
public class JavaApiTestEntry {
	public static void main(String[] args) throws InvocationTargetException, IllegalAccessException, IntrospectionException {
		MasterModel m1 = new MasterModel();
		m1.setId("11100");
		m1.setMasterName("dongtest");
		m1.setMonitorFlag("E90011200");
		m1.setStationId("Mstation:1");
		MasterModel m2 = new MasterModel();
		m2.setId("12100");
		m2.setMasterName("dongtest1");
		m2.setMonitorFlag("E91011200");
		m2.setStationId("Mstation:2");
		Map<String, Object> transBeanToMap = BeanUtils.transBeanToMap(m2);
		System.out.println("transBeanToMap"+transBeanToMap);
		
		List<MasterModel> m = new ArrayList<>();
		m.add(m1);
		m.add(m2);
		Map<String, List<MasterModel>> collect = m.stream().collect(Collectors.groupingBy(MasterModel::getMasterName));
		String collect2 = m.stream().map(MasterModel::getStationId).collect(Collectors.joining("-"));
		System.out.println(collect2);
		System.out.println(collect);
		MasterModel m3 = new MasterModel();
		Map<String,Object> maps = new HashMap<>();
		maps.put("id", "dong");
		maps.put("monitorFlag",11);
		maps.put("masterName","134556655");
		maps.put("stationId","1233");
		BeanUtils.transMapToBean(maps,m3);
		
		System.out.println(m3);
		
		List<Map<String,Object>> listMap = new ArrayList<>();
		listMap.add(maps);
	
		
		
		
	     //Map<Integer, List<CustFriend>> groupBy = taskCustFriendList.stream().collect(Collectors.groupingBy(CustFriend::getWxAccountId));
		//infoRelationTagsByInfoId.stream().map(InfoRelationTag::getTagName).collect(Collectors.joining(","))
		
		
		
				
		// foreachForJava8();	
		//substrMethod("ahxj-masterNo1-ahxj_bramchNo2-母排A相");	
	}
	
	
	/**
	 * java8 for 循环语法
	 */
	private static void foreachForJava8() {
		List<String> list = new ArrayList<>();
		list.add("a");
		list.add("b");
		list.add("c");
		list.add("d");
		list.add("e");
		list.add("f");
		list.forEach(System.out::println);
	}
     /**
      * 截取字符串对应的值
      */
	private static void substrMethod(String substr) {
		String str = "ahxj-masterNo1-ahxj_bramchNo2-母排A相";
		String substring = substr.substring(0, str.length() - 2);
		String substring2 = substr.substring(str.length() - 2);
		System.out.println("substring2:" + substring2);
		System.out.println("substring:" + substring);
	}

}
