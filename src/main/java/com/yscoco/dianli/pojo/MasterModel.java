package com.yscoco.dianli.pojo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 母线信息 AS （800设备信息）
 * @author liuDong.
 *
 */
@Getter
@Setter
@ToString
public class MasterModel {
	 private String id;
	 /**
	  * 母线唯一标识
	  */
	 private String monitorFlag;
	 /**
	  * 母线名称
	  */
	 private String masterName;
	 /**
	  * 所属监测站id
	  */
	 private String stationId;
	 /**
	  * 所属监测站名称
	  */
	 private String stationName;
	  /**
	   * 公司id
	   */
	 private String companyId;
	 /**
	   * 公司名称
	   */
	 private String companyName;
	 
	 /**
	  * 监测站地址
	  */
	private String stationAddress;

	/**
	 * 监测站类型(1.800,2.耐张线夹测温,3机电健康管理4充电柜)
	 */
	private String stationType;
	 
	 
}
