package com.yscoco.dianli.pojo;

import java.io.Serializable;

import lombok.Data;

/**
 * 统计信息母线信息
 * @author liuDong.
 *
 */
@Data
public class EricInfoModel  implements Serializable{
	private static final long serialVersionUID = 1L;
	//  String[] erickeys = { "masterName","相别", "voltage_a", "Electric_a", "Power_Factor_a", "Power_Apparent_a","Power_Use_a","Power_Nouse_a"};
	//	String[] erickeys = { "masterName","B相", "voltage_b", "Electric_b", "Power_Factor_b", "Power_Apparent_b","Power_Use_b","Power_Nouse_b"};
	//	String[] erickeys = { "masterName","C相", "voltage_c", "Electric_c", "Power_Factor_c", "Power_Apparent_c","Power_Use_c","Power_Nouse_c"};
	private String masterName;
	private String pash; //相位
	private String voltage_a;
	private String voltage_b;
	private String voltage_c;
	private String Electric_a;
	private String Electric_b;
	private String Electric_c;
	private String Power_Factor_a;
	private String Power_Factor_b;
	private String Power_Factor_c;
	private String Power_Apparent_a;
	private String Power_Apparent_b;
	private String Power_Apparent_c;
	private String Power_Use_a;
	private String Power_Use_b;
	private String Power_Use_c;
	private String Power_Nouse_a;
	private String Power_Nouse_b;
	private String Power_Nouse_c;
}
