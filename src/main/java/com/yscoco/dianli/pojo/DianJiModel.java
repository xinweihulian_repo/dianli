package com.yscoco.dianli.pojo;

import lombok.Data;

@Data
public class DianJiModel {
	private String id;
	/**
	 * 电机状态
	 */
	private String status="待定";
	/**
	 * 所属监测站id
	 */
	private String stationId;
	/**
	 * 所属监测站名称
	 */
	private String stationName;
	/**
	 * 所属母线名称
	 */
	private String masterName;

	/**
	 * 监测点名称
	 */
	private String pointName;
	/**
	 * 监测设备唯一标识
	 */
	private String pointFlag;
	/*
	 * 监测设备分类 （温度传感器，过电压保护器）
	 */
	private String hitchType;
	 /**
	  * 位置 0-17   (下静触头-上动触头 -母排 ....)
	  */
	private String position;
	/**
	 * 所属类型:电站型，电机型，电容型，有源表带式，无源表带式，红外式....
	 */
	private String type;
	

}
