package com.yscoco.dianli.pojo;

import lombok.Data;

/**
 * @author dong
 */
@Data
public class MemberModel {
	private String id;
	private String userName;
	private String monitorStationId;
	private String openId;
}
