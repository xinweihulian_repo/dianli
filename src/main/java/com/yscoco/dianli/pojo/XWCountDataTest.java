package com.yscoco.dianli.pojo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.hyc.smart.XWDataEntry;
import com.hyc.smart.bean.ItemFault2Data;
import com.yscoco.dianli.constants.FaultType;

/**
 * 
 * @author liuDong.
 *
 */
public class XWCountDataTest {


	public static void main(String[] args) throws IOException {
		//母线 id
		List<String> masterList = new ArrayList<>();
		masterList.add("00000000000000002018");
		masterList.add("00000000000000002019");
 		masterList.add("00000000000000002017");
		masterList.add("ERIC0800180900000001");
		//监测点设备id
		List<String> pointList = new ArrayList<>();
		pointList.add("A101");
     	pointList.add("B11");
     	pointList.add("B30");
     	int faultNewCount22 = XWDataEntry.getFaultNewCount(pointList, FaultType.FaultTypeFor22.getValue(),
				false);
     	int faultNewCount21 = XWDataEntry.getFaultNewCount(pointList, FaultType.FaultTypeFor21.getValue(),
				false);
     	List<Map<String, Object>> faultInfoForPage = XWDataEntry.getFaultInfoForTimePage(pointList, 0,false, 0, System.currentTimeMillis(), 1, 10);
     	  System.out.println("faultNewCount22:"+faultNewCount22);
     	 System.out.println("faultNewCount21:"+faultNewCount21);
     	System.out.println("faultInfoForPage:"+faultInfoForPage);
		//getMastetInfo(masterList);
		//getmaxValueVoltage(pointList);
		//getmaxValueTemper(pointList);
	    //getCountByFaultType(masterList);
	//  getAllCountByFaultType(masterList);
     	//getAllCountByFaultTypeAndStatus(masterList);
	}
  
	
	/**
	 * 获取一段时间所有设备分类故障的已处理 未处理 总数 isSolve false -未处理 true-已处理
	 */
	private static void getAllCountByFaultTypeAndStatus(List<String> masterList) {
	 List<Map<String, Object>> allCountByFaultTypeAndStatus = XWDataEntry.getAllCountByFaultTypeAndStatus(masterList,false, 0, System.currentTimeMillis());
		for (Map<String, Object> map : allCountByFaultTypeAndStatus) {
			System.out.println(JSON.toJSON(map));
		}
	}
	
	/**
	 * 获取一段时间所有设备分类故障的总数
	 */
	private static void getAllCountByFaultType(List<String> masterList) {
		List<Map<String,Object>> allCountByFaultType = XWDataEntry.getAllCountByFaultType(masterList, 0, System.currentTimeMillis());
		for (Map<String, Object> map : allCountByFaultType) {
			System.out.println(JSON.toJSON(map));
		}
	}
	
	
	/**
	 * 获取一段时间内个分类故障的总数
	 */
	private static void getCountByFaultType(List<String> masterList) {
		List<Map<String,Object>> countByFaultType = XWDataEntry.getCountByFaultType(masterList, 0, System.currentTimeMillis());
		for (Map<String, Object> map : countByFaultType) {
			System.out.println(JSON.toJSON(map));
		}
	}
	/**
	 * 获取一段时间内温度采集器对应字段的最大值集合
	 */
	private static void getmaxValueTemper(List<String> pointList) {
		List<Map<String,Object>> getmaxValueTemper = XWDataEntry.getmaxValueTemper(pointList, 0, System.currentTimeMillis());
		for (Map<String, Object> map : getmaxValueTemper) {
			System.out.println(JSON.toJSON(map));
		}
	}
	/**
	 * 获取一段时间内保护器对应字段的最大值集合
	 */
	private static void getmaxValueVoltage(List<String> pointList) {
		List<Map<String,Object>> getmaxValueVoltage = XWDataEntry.getmaxValueVoltage(pointList, 0, System.currentTimeMillis());
		for (Map<String, Object> map : getmaxValueVoltage) {
			System.out.println(JSON.toJSON(map));
		}
	}

	/**
	 * 获取一段时间内设备对应字段的最大值集合
	 */
	private static void getMastetInfo(List<String> list) {
		List<Map<String,Object>> maxValueEric = XWDataEntry.getMaxValueEric(list, 0, System.currentTimeMillis());
		   for (Map<String, Object> map : maxValueEric) {
			System.out.println(JSON.toJSON(map));
		}
	}


}
