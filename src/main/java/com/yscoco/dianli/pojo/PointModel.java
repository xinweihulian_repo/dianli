package com.yscoco.dianli.pojo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 监测点  AS 监测设备 pojo
 * @author liuDong.
 *
 */
@Getter
@Setter
@ToString
public class PointModel {
	
	private String id;
	/**
	 * 所属监测站id
	 */
	private String stationId;
	/**
	 * 所属监测站名称
	 */
	private String stationName;
	 /**
	  *所属母线id
	  */
	private String masterId;
	
	/**
	 * 母线监测设备标识
	 */
	private String masterFlag;
	/**
	 * 所属母线名称
	 */
	private String masterName;
	/**
	 * 所属支线id
	 */
	private String branchId;
	/**
	 * 所属支线名称
	 */
	private String branchName;
	/**
	 * 监测点名称
	 */
	private String pointName;
	/**
	 * 监测设备唯一标识
	 */
	private String pointFlag;
	/*
	 * 监测设备分类 （温度传感器，过电压保护器,电机）
	 */
	private String hitchType;
	 /**
	  * 位置 0-17   (下静触头-上动触头 -母排 ....)
	  */
	private String position;
	/**
	 * 所属类型:电站型，电机型，电容型，有源表带式，无源表带式，红外式....
	 */
	private String type;
	
	/**
	   * 公司id
	   */
	 private String companyId;
	 /**
	   * 公司名称
	   */
	 private String companyName;
	 
	 /**
	  * 监测站地址
	  */
	private String stationAddress;
	
	/**
	 * 柜号
	 */
	private String cabin_no;

	/**
	 * 监测站类型(1.800,2.耐张线夹测温,3机电健康管理4充电柜)
	 */
	private String stationType;
	

	
	
	
}
