package com.yscoco.dianli.form;

import lombok.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author dong
 */
@Getter
@Setter
@ToString
public class ParseSeneorData  implements Serializable {
	
   private String branchName;
   private String cabin_no;
   private String sensorNumber;
   private String branchType;
   private String pointName;
   private String hitchType;
   private String type;
   private String monitorFlag;
   private String position;

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (!(o instanceof ParseSeneorData)) return false;
      ParseSeneorData that = (ParseSeneorData) o;
      return Objects.equals(monitorFlag, that.monitorFlag);
   }

   @Override
   public int hashCode() {
      return Objects.hash(monitorFlag);
   }
}
