package com.yscoco.dianli.form;

import lombok.Getter;
import lombok.Setter;

/**
 * @author dong
 */
@Getter
@Setter
public class ImportSensorForm {

    private String token;
    private String masterId;

}
