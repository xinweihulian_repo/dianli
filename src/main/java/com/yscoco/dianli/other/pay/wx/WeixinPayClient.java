package com.yscoco.dianli.other.pay.wx;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;
import org.springframework.stereotype.Component;

import com.yscoco.dianli.common.exception.BizException;
import com.yscoco.dianli.common.utils.HttpClient;
import com.yscoco.dianli.common.utils.RandomUtils;
import com.yscoco.dianli.common.vo.Code;
import com.yscoco.dianli.constants.ConfigConts;


@Component
public class WeixinPayClient{
	Logger log = Logger.getLogger(WeixinPayClient.class);
	private static final String WEIXIN_APPID = ConfigConts.getString("ALIPAY_PARTNER");
	private static final String WEIXIN_MCHID = ConfigConts.getString("WEIXIN_MCHID");
	private static final String WXPAY_APIKEY = ConfigConts.getString("WXPAY_APIKEY");
	private static final String WEIXIN_API = ConfigConts.getString("WEIXIN_API");
	private static final String APP_NAME = ConfigConts.getString("APP_NAME");
	private static final String SERVICEIP = ConfigConts.getString("SERVICEIP");
	private static final String WEIXIN_NOTIFY = ConfigConts.getString("WEIXIN_NOTIFY");
	
	private WeixinPayClient(){}

	public static final WeixinPayClient instance = new WeixinPayClient();
	public static WeixinPayClient getInstance()
	{
		return instance;
	}
	
	/** 查询订单 */
	private static final String ORDERQUERY = "/pay/orderquery";

	/** 统一下单 */
	private static final String UNIFIEDORDER = "/pay/unifiedorder";
	
	/** 申请退款接口 */
	private static final String REFUND = "/secapi/pay/refund";

	private static final String charset = "UTF-8";
	
	/**
	 * 校验微信订单状态
	 * 
	 * @param escOrderId
	 * @param payAmount
	 * @return
	 */
	public boolean verif(String escOrderId, String state) {
		TreeMap<String, String> params = new TreeMap<>();
		params.put("appid", WEIXIN_APPID);
		params.put("mch_id", WEIXIN_MCHID);
		params.put("nonce_str", RandomUtils.getNumCode(6));
		params.put("out_trade_no", escOrderId);
		params.put("sign", sign(params, WXPAY_APIKEY));

		String body = buildParam(params);

		HttpClient client = new HttpClient();
		String resultStr = client.post(WEIXIN_API+ORDERQUERY, body , charset);
		TreeMap<String, String> resultParams = checkSign(resultStr, WXPAY_APIKEY);

		String trade_state = resultParams.get("trade_state");

		return StringUtils.isNotEmpty(trade_state) && trade_state.equals(state);
	}

	public static void main(String[] args) {
		WeixinPayClient.getInstance().unifiedorder("synsunfree", "T20160201", 0.01, "JSAPI");
	}
	
	/**
	 * 统一下单
	 * 
	 * @param name
	 * @param escOrderId
	 * @param payAmount
	 * @param clientIp
	 * @return
	 */
	public String unifiedorder(  String name,String escOrderId, Double payAmount,
			String tradeType ) {
		// 设置参数
		StringBuffer sb = new StringBuffer();
		sb.append(APP_NAME).append("-").append(name);

		TreeMap<String, String> params = new TreeMap<>();
		params.put("appid", WEIXIN_APPID);
		params.put("mch_id", WEIXIN_MCHID);
		params.put("nonce_str", RandomUtils.getNumCode(6));
		params.put("body", sb.toString());
		params.put("out_trade_no", escOrderId);
		params.put("total_fee", String.valueOf((int) (payAmount.doubleValue() * 100)));
		params.put("spbill_create_ip", SERVICEIP);
		params.put("notify_url", WEIXIN_NOTIFY);
		params.put("trade_type", tradeType);
		params.put("sign", sign(params,  WXPAY_APIKEY));

		String body = buildParam(params);

		HttpClient client = new HttpClient();
		String resultStr = client.post(WEIXIN_API+UNIFIEDORDER, body , charset);
		TreeMap<String, String> resultParams = checkSign(resultStr,  WXPAY_APIKEY);

		return resultParams.get("prepay_id");
	}

	
	
	/**
	 * 退款
	 * 
	 * @param escOrderId
	 * @param payAmount
	 * @param refundAmount
	 */
	public void refund(String escOrderId, Double payAmount, Double refundAmount) {
		// 设置参数
		TreeMap<String, String> params = new TreeMap<>();
		params.put("appid", WEIXIN_APPID);
		params.put("mch_id", WEIXIN_MCHID);
		params.put("nonce_str", RandomUtils.getNumCode(6));
		params.put("out_trade_no", escOrderId);
		params.put("out_refund_no", escOrderId);
		params.put("total_fee", String.valueOf((int) (payAmount.doubleValue() * 100)));
		params.put("refund_fee", String.valueOf((int) (refundAmount.doubleValue() * 100)));
		params.put("op_user_id", WEIXIN_MCHID);
		params.put("sign", sign(params,  WXPAY_APIKEY));

		String body = buildParam(params);

		HttpClient client = new HttpClient();
		String resultStr = client.post(WEIXIN_API+REFUND, body , charset);
		checkSign(resultStr,  WXPAY_APIKEY);
	}
	
	
	/**
	 * 生成支付XML参数
	 * 
	 * @param params
	 * @return
	 */
	private String buildParam(TreeMap<String, String> params) {
		Document document = DocumentHelper.createDocument();
		Element xml = document.addElement("xml");
		for (Map.Entry<String, String> entry : params.entrySet()) {
			Element ele = xml.addElement(entry.getKey());
			ele.addCDATA(entry.getValue());
		}

		StringWriter out = new StringWriter(1024);
		XMLWriter xw = new XMLWriter(out);
		try {
			xw.write(document);
		} catch (IOException e) {
			log.error("生成支付XML参数",e);
			throw new BizException(Code.ERROR);
		}

		return out.toString();
	}

	public Map<String, String> checkSign(InputStream inputStream) {
		Document result;
		try {
			SAXReader reader = new SAXReader();
			result = reader.read(inputStream);
		} catch (DocumentException e) {
			log.error("支付失败",e);
			throw new BizException(Code.ERR_PAY);
		} finally {
			IOUtils.closeQuietly(inputStream);
		}

		return checkSign(result, WXPAY_APIKEY);
	}

	private TreeMap<String, String> checkSign(String resultStr, String wxpayKey) {
		Document result;
		try {
			result = DocumentHelper.parseText(resultStr);
		} catch (DocumentException e) {
			log.error("支付失败",e);
			throw new BizException(Code.ERR_PAY);
		}

		return checkSign(result, wxpayKey);
	}

	
	/**
	 * 校验微信签名并解析返回数据
	 * 
	 * @param resultStr
	 * @param wxpayKey
	 * @return
	 */
	private TreeMap<String, String> checkSign(Document result, String wxpayKey) {
		log.error(result.asXML());
		boolean igSign = false;
		String resultSign = null;
		TreeMap<String, String> resultParams = new TreeMap<>();
		Element resultXml = result.getRootElement();
		for (Object resultObj : resultXml.elements()) {
			Element resultEle = (Element) resultObj;
			if (igSign || !"sign".equals(resultEle.getName())) {
				resultParams.put(resultEle.getName(), resultEle.getText());
			} else {
				resultSign = resultEle.getText();
				igSign = true;
			}
		}

		checkErr(resultParams);

		if (StringUtils.isEmpty(resultSign) || !resultSign.equals(sign(resultParams, wxpayKey))) {
			throw new BizException(Code.ERR_PAY);
		}

		return resultParams;
	}

	/**
	 * 签名
	 * 
	 * @param params
	 *            签名数据
	 * @param wxpayKey
	 *            密钥
	 * @return
	 */
	public String sign(TreeMap<String, String> params, String wxpayKey) {
		// 拼装参数
		StringBuffer psb = new StringBuffer(1024);
		for (Map.Entry<String, String> entry : params.entrySet()) {
			if (StringUtils.isBlank(entry.getValue())) {
				continue;
			}

			psb.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
		}
		// 拼接密钥
		psb.append("key=").append(wxpayKey);
		String psbStr = psb.toString();
		log.error(psbStr);

		String sign = DigestUtils.md5Hex(psbStr).toUpperCase();

		return sign;
	}

	private void checkErr(Map<String, String> result) {
		String returnCode = result.get("return_code");
		if (!"SUCCESS".equals(returnCode)) {
			throw new BizException(Code.ERR_PAY);
		}

	}

	

	/**
	 * APP加签
	 * 
	 * @param paySeq
	 * @return
	 */
	public Map<String, String> signAPP(String paySeq) {
		TreeMap<String, String> params = new TreeMap<>();
		params.put("appid", WEIXIN_APPID);
		params.put("partnerid", WEIXIN_MCHID);
		params.put("timestamp", String.valueOf(System.currentTimeMillis() / 1000));
		params.put("noncestr",  RandomUtils.getNumCode(6));
		params.put("prepayid", paySeq);
		params.put("package", "Sign=WXPay");

		String paySign = sign(params, WXPAY_APIKEY);
		params.put("sign", paySign);

		return params;
	}
	
	


}
