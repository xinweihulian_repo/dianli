package com.yscoco.dianli.other.pay.ali;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.alibaba.fastjson.JSONObject;
import com.yscoco.dianli.common.exception.BizException;
import com.yscoco.dianli.common.utils.HttpClient;
import com.yscoco.dianli.common.utils.Log;
import com.yscoco.dianli.common.utils.RSA;
import com.yscoco.dianli.common.vo.Code;
import com.yscoco.dianli.constants.ConfigConts;


public class AlipayClient {
	
	Logger log = Logger.getLogger(AlipayClient.class);
	
	public static final String ALIPAY_PARTNER = ConfigConts.getString("ALIPAY_PARTNER");
	public static final String ALIPAY_NOTIFY = ConfigConts.getString("ALIPAY_NOTIFY");
	public static final String ALIPAY_SELLER = ConfigConts.getString("ALIPAY_SELLER");
	public static final String ALIPAY_PRIVATE_KEY =  ConfigConts.getString("ALIPAY_PRIVATE_KEY");
	public static final String ALIPAY_PUBLIC_KEY =  ConfigConts.getString("ALIPAY_PUBLIC_KEY");
	
	public static final String ALIPAY_API = ConfigConts.getString("ALIPAY_API");
	public static final String INPUT_CHARSET = "UTF-8";
	
	
	private AlipayClient(){};
	private static final AlipayClient instance = new AlipayClient();
	public static AlipayClient getInstance(){
		return instance;
	}
	
	public static void main(String[] args) {
		AlipayClient.getInstance().signMobilePay("1705021658121000", "BUY STORE", "BUY 365 DAY STORE", 0.02);
		String s1="SeRq35wt3raj95CW6ZKp7atU0V%2F5sepVB%2BLEKBFXYAJDsIUlSUCc4zr1p4%2F4eKudabI2HdfEebLiPzSz7i26qprPVidNJbIWA11l2FKUBtl62zfTpv%2BW%2FK0IAuClu%2B6snA1fW2DbIJVp5A%2F4Qprqd3Hgtt%2BOl6GDY5RrzQlad%2F9D0LSth5WQ2Fna5GX86J4dw8YhgKFbYGc12ZUZAFde9aT5h5oVttdna1sNZjnLBbT8RAxFJotpqqZ7KZF2DceZHDONREGVd7RPeXy%2Fb1qTtaDiBG7RBl%2Fx7fNW9VDun9292VbKT5pSygStel82P%2FmPwDqI2RyzUM%2BSS%2FFeDM3HRw%3D%3D";
		String s2="SeRq35wt3raj95CW6ZKp7atU0V%2F5sepVB%2BLEKBFXYAJDsIUlSUCc4zr1p4%2F4eKudabI2HdfEebLiPzSz7i26qprPVidNJbIWA11l2FKUBtl62zfTpv%2BW%2FK0IAuClu%2B6snA1fW2DbIJVp5A%2F4Qprqd3Hgtt%2BOl6GDY5RrzQlad%2F9D0LSth5WQ2Fna5GX86J4dw8YhgKFbYGc12ZUZAFde9aT5h5oVttdna1sNZjnLBbT8RAxFJotpqqZ7KZF2DceZHDONREGVd7RPeXy%2Fb1qTtaDiBG7RBl%2Fx7fNW9VDun9292VbKT5pSygStel82P%2FmPwDqI2RyzUM%2BSS%2FFeDM3HRw%3D%3D";
		 
		System.out.println(s1.equals(s2));
	}

	/**
	 * 支付参数加签
	 * 
	 * @param escOrderId
	 * @param subject
	 * @param body
	 * @param fee
	 * @return
	 */
	public Map<String, String> signMobilePay(String escOrderId, String subject, String body, Double fee) {
		
		TreeMap<String, String> params = new TreeMap<>();
		params.put("partner", ALIPAY_PARTNER);
		params.put("seller_id", ALIPAY_SELLER);
		params.put("out_trade_no", escOrderId);
		params.put("subject", subject);
		params.put("body", body);
		params.put("total_fee", fee.toString());
		params.put("notify_url", ALIPAY_NOTIFY);
		params.put("service", "mobile.securitypay.pay");
		params.put("payment_type", "1");
		params.put("_input_charset", INPUT_CHARSET);
		params.put("sign", sign(params));
		params.put("sign_type", "RSA");
		Log.getCommon().error("test aliPay ： "+JSONObject.toJSONString(params));
		return params;
	}

	public Map<String, String> check(Map<String, String[]> reqMap) {
		// 组装待签名数据
		boolean igSign = false, igSignType = false;
		String resultSign = null;
		TreeMap<String, String> params = new TreeMap<>();
		for (Entry<String, String[]> entry : reqMap.entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue()[0];

			if (!igSign && "sign".equals(key)) {
				resultSign = value;
				igSign = true;
			} else if (!igSignType && "sign_type".equals(key)) {
				igSignType = true;
				continue;
			} else {
				if (StringUtils.isNotEmpty(value)) {
					params.put(key, value);
				}
			}
		}

		// 验证签名
		if (StringUtils.isEmpty(resultSign) || !signCheck(params, resultSign)) {
			throw new BizException(Code.ERR_PAY);
		}

		// 验证回调是否正确
		String notify_id = params.get("notify_id");
		if (!verif(notify_id)) {
			throw new BizException(Code.ERR_PAY);
		}

		return params;
	}

	/**
	 * 验证支付宝通知是否正确
	 * 
	 * @param payProId
	 * @param notify_id
	 * @return
	 */
	public boolean verif(String notify_id) {
		Map<String,String> params = new HashMap<String, String>();
		params.put("service", "notify_verify");
		params.put("partner", ALIPAY_PARTNER);
		params.put("notify_id", notify_id);
		HttpClient client = new HttpClient();
		String resultStr = client.post(ALIPAY_API, params);
		
		log.error("验证支付宝通知是否正确:" + resultStr);

		if ("true".equalsIgnoreCase(resultStr)) {
			return true;
		} else {
			
			return false;
		}
	}

	private String buildContent(TreeMap<String, String> params, boolean wrap) {
		// 拼装参数
		StringBuffer psb = new StringBuffer(1024);
		for (Map.Entry<String, String> entry : params.entrySet()) {
			if (StringUtils.isBlank(entry.getValue())) {
				continue;
			}

			if (psb.length() > 0) {
				psb.append("&");
			}
			psb.append(entry.getKey()).append("=");
			if (wrap) {
				psb.append("\"");
			}
			psb.append(entry.getValue());
			if (wrap) {
				psb.append("\"");
			}
		}

		String psbStr = psb.toString();
		log.error(psbStr);

		return psbStr;
	}

	private String sign(TreeMap<String, String> params) {
		return RSA.sign(buildContent(params, true), ALIPAY_PRIVATE_KEY, INPUT_CHARSET);
	}

	private boolean signCheck(TreeMap<String, String> params, String sign) {
		return RSA.verify(buildContent(params, false), sign, ALIPAY_PUBLIC_KEY, INPUT_CHARSET);
	
	}

	
}
