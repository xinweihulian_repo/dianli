package com.yscoco.dianli.other.cache;

import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * @Author: onion..
 * @Description: TODO
 * @Date: 2020/4/17 11:23
 */
@Component("redisCache")
public class RedisCacheUtil {
    @Resource
    private StringRedisTemplate  redisTemplate;


    public void set(String key, String value, int var3 , TimeUnit timeUnit){
        if(key == null || "".equals(key)){
            return ;
        }
        redisTemplate.opsForValue().set(key, value,var3,timeUnit);
    }
    
    public void set(String key, String value,String token, int var3 , TimeUnit timeUnit){
        if(key == null || "".equals(key)||token==null|| "".equals(token)){
            return ;
        }
        
        redisTemplate.opsForValue().set(new StringBuilder().append(key).append(token).toString(), value,var3,timeUnit);
    }
    
    public String get(String key,String token){
        if(key == null || "".equals(key)||token==null|| "".equals(token)){
            return null;
        }
        return redisTemplate.opsForValue().get(new StringBuilder().append(key).append(token).toString());
    }
    
    

    public String get(String key){
        if(key == null || "".equals(key)){
            return null;
        }
        return redisTemplate.opsForValue().get(key);
    }

    public void set(String key, String value){
        if(key == null || "".equals(key)){
            return ;
        }
        redisTemplate.opsForValue().set(key, value,1,TimeUnit.MINUTES);
    }



    /**
     * 删除
     * @param key
     */
    public void delte(String key) {
        if(key == null || "".equals(key)){
            return;
        }
        redisTemplate.delete(key);
    }

    /**
     * 向Hash中添加值
     * @param key      可以对应数据库中的表名
     * @param field    可以对应数据库表中的唯一索引
     * @param value    存入redis中的值
     */
    public void hset(String key, String field, String value) {
        if(key == null || "".equals(key)){
            return ;
        }
        redisTemplate.opsForHash().put(key, field, value);
    }


    /**
     * 从redis中取出值
     * @param key
     * @param field
     * @return
     */
    public String hget(String key, String field){
        if(key == null || "".equals(key)){
            return null;
        }

        return (String) redisTemplate.opsForHash().get(key, field);
    }

    /**
     * 判断 是否存在 key 以及 hash key
     * @param key
     * @param field
     * @return
     */
    public boolean hexists(String key, String field){
        if(key == null || "".equals(key)){
            return false;
        }
        return redisTemplate.opsForHash().hasKey(key, field);
    }

    /**
     * 查询 key中对应多少条数据
     * @param key
     * @return
     */
    public long hsize(String key) {
        if(key == null || "".equals(key)){
            return 0L;
        }
        return redisTemplate.opsForHash().size(key);
    }

    /**
     * 删除
     * @param key
     * @param field
     */
    public void hdel(String key, String field) {
        if(key == null || "".equals(key)){
            return;
        }
        redisTemplate.opsForHash().delete(key, field);
    }


    public ArrayList<Object> hgetList(String key){
        if(key == null || "".equals(key)){
            return null;
        }
        Set<Object> keys = redisTemplate.opsForHash().keys(key);
        return new ArrayList<Object>(keys);
    }


}
