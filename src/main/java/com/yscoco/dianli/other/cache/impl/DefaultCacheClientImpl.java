package com.yscoco.dianli.other.cache.impl;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.jvnet.hk2.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.yscoco.dianli.other.cache.CacheClientI;

/**  
 * @Title: DefaultCacheClientImpl.java
 * @Description: TODO 简单的临时缓存实现，将来用其他缓存机制替换.
 * @author lanston
 * @date 2016-9-21
 */
@Service
public class DefaultCacheClientImpl  implements CacheClientI{
	   Logger log = LoggerFactory.getLogger(DefaultCacheClientImpl.class);
	    /**
	     * 虚拟缓存 , 存json格式数据
	     */
	    private Map<String,String> cache = new ConcurrentHashMap<String, String>();
	    
	    /**
	     * 过期时间 单位s
	     */
	    private Map<String,Long> tasks = new ConcurrentHashMap<String,Long>();
	    
	    
	    public DefaultCacheClientImpl(){
	        
	    }
	 
	    /**  
	     * @Title: DefaultCacheClientImpl.java
	     * @Description: TODO
	     * @author lanston
	     * @date 2016-9-21
	     * @param timeout 单位秒
	     */
	    @Override
	    public void set(String key,Object value,long timeout){
	    	if(key == null) return ;
	        this.delete(key); 
	        String valueStr = null;
	        if(value instanceof String){
	        	valueStr = value.toString();
	        }else{
	        	 valueStr = JSONObject.toJSONString(value);
	        }
	        
	        this.cache.put(key, valueStr);
	        if(timeout > 0l){
	        	this.tasks.put(key,timeout*1000l + System.currentTimeMillis());
	        }
	        log.debug("插入缓存 key:"+key + " /n value:" +value +" /n timeout:"+timeout);
	    }
	    
	    @Override
		public void set(String key, Object value) {
	    	if(key == null) return;
			this.set(key, value ,-1l);
		}
	    
	    /**
	     * 获取缓存
	     */
	    @Override
	    public String get(String key){
	    	if(key == null) return null;
	    	Long timeout = this.tasks.get(key);
			if(timeout != null && timeout <= System.currentTimeMillis()){
				log.debug("超时缓存 key:"+key);
				this.delete(key);
				return null;
			}
			log.debug("取缓存 key:"+key);
			return cache.get(key);
	    }
	    
	    /**
	     * 获取缓存
	     * @param <T>
	     * @param <T>
	     */
	    @Override
	    public  <T> T get(String key , Class<T> clazz){
	    	if(key == null) return null;
			String v = get(key);
			if(v==null) return null;
			return JSONObject.parseObject(v,clazz );
	    }

	    /**
	     * 获取缓存
	     * @param <T>
	     */
	    @Override
	    public <T> T get(String key , TypeReference<T> type){
	    	if(key == null) return null;
			String v = get(key);
			if(v==null) return null;
			return JSONObject.parseObject(v, type);
	    }

	    
	    /**
	     * 删除缓存
	     */
	    @Override
	    public void delete(String key){
	    	log.debug("删除缓存 key:"+key);
	    	if(key == null) return;
	    	this.cache.remove(key);
	    	this.tasks.remove(key);
	    }
	    

	    /**
	     * 删除以keyStart 开头的缓存 
	     */
	    @Override
	    public void deleteStart(String keyStart){
	    	if(keyStart == null) return;
	    	log.debug("删除以 keyStart:"+keyStart +" 为开头的key 缓存");
	    	Set<String> keys = cache.keySet();
	    	for (String key : keys) {
				if(key.startsWith(keyStart)){
					this.delete(key);
				}
			}
	    }
	    
	    public static void main(String[] args) {
			DefaultCacheClientImpl c = new DefaultCacheClientImpl();
			c.set("1", "aaaa");
			System.out.println("1"+c.get("1").toString());
		}
	    
}

