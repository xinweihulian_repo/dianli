package com.yscoco.dianli.other.cache;

import com.alibaba.fastjson.TypeReference;

public interface CacheClientI {
    /**  
     * @Title: DefaultCacheClientImpl.java
     * @Description: TODO
     * @author lanston
     * @date 2016-9-21
     * @param timeout 单位秒  <0 永久保存
     */
	public void set(String key, Object value, long timeout);
	
	 /**  
     * @Title: DefaultCacheClientImpl.java
     * @Description: TODO
     * @author lanston
     * @date 2016-9-21
     */
	public void set(String key, Object value);

	/**
	 * 获取缓存
	 */
	public String get(String key);

	/**
	 * 获取缓存
	 * 
	 * @param <T>
	 * @param <T>
	 */
	public  <T> T get(String key , Class<T> clazz);

	/**
	 * 获取缓存
	 * 
	 * @param <T>
	 */
	public <T> T get(String key , TypeReference<T> type);

	/**
	 * 删除缓存
	 */
	public void delete(String key);

	/**  
	 * 删除   以 keyStart 开头的 缓存 
	 */
	public void deleteStart(String keyStart);
}
