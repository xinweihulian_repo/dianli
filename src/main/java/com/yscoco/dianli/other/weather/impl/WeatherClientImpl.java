package com.yscoco.dianli.other.weather.impl;

import org.apache.commons.lang3.StringUtils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.yscoco.dianli.common.utils.HttpClient;
import com.yscoco.dianli.constants.ConfigConts;
import com.yscoco.dianli.other.weather.WeatherClient;
import com.yscoco.dianli.other.weather.dto.Weather;


public class WeatherClientImpl implements WeatherClient{
	private final String LOCATIONS ="locations/v1/cities/geoposition/search";
	private final String FORECASTS1DAY ="forecasts/v1/daily/1day/";
	//private final String FORECASTS12HOUR ="forecasts/v1/hourly/12hour/";
	

	public Weather getWeather(String lat , String lng){
		String url = getParams(LOCATIONS,lat, lng, "false", "true", null);
		HttpClient client = new HttpClient();
		String resultStr = client.get(url, null);
		Weather dto = new Weather();
		System.out.println(url);
		System.out.println(resultStr);
		JSONObject json = JSONObject.parseObject(resultStr);
		dto.setKey(json.getString("Key"));
		dto.setLocalizedName(json.getString("LocalizedName"));
		if( dto.getKey() != null){
			//getForecasts12Hour(lat, lng, dto);
			
			getForecasts1Day(lat, lng,  dto);
		}
		return dto;
	}
	public void getForecasts12Hour(String lat , String lng,Weather dto){
		String params = getParams(dto.getKey(),lat, lng, "false", null, "true");

		/*JerseyWebTarget target = getTarget(client, yscocoProperties.getWeatherApiUrl(), FORECASTS12HOUR+dto.getKey()+"?"+params);
		String resultStr = request(target, null, null, MediaType.APPLICATION_FORM_URLENCODED_TYPE, HTTPMethod.GET,String.class);
		JsonNode jsons = JSONUtil.toJsonNode(resultStr);
		List<WeatherInfo> infos = new ArrayList<WeatherInfo>();
		WeatherInfo info = null;
		for (Iterator<JsonNode> iterator = jsons.elements(); iterator.hasNext();) {
			JsonNode json = iterator.next();
			info = new WeatherInfo();
			info.setDateTime(json.get("DateTime").asText());
			info.setDaylight(json.get("IsDaylight").asBoolean());
			info.setEpochDateTime(json.get("EpochDateTime").asText());
			info.setIconPhrase(json.get("IconPhrase").asText());
			info.setWeatherIcon(json.get("WeatherIcon").asText());
			infos.add(info);
		}
		dto.setWeatherInfos(infos);*/
		
	}
	public void getForecasts1Day(String lat , String lng,Weather dto){
		String url = getParams(FORECASTS1DAY+dto.getKey(),lat, lng, "true", null, "true");
		HttpClient client = new HttpClient();
		
		String resultStr = client.get(url, null);
		JSONObject json = JSONObject.parseObject(resultStr);
		JSONArray array = json.getJSONArray("DailyForecasts");
		for (int i = 0 ; i<array.size(); i++) {
			JSONObject j = array.getJSONObject(i);
			dto.setMaxTemperature(j.getJSONObject("Temperature").getJSONObject("Maximum").getString("Value"));
			dto.setMinTemperature(j.getJSONObject("Temperature").getJSONObject("Minimum").getString("Value"));
			dto.setDayIcon(j.getJSONObject("Day").getString("Icon"));
			dto.setNightIcon(j.getJSONObject("Night").getString("Icon"));
			dto.setDate(j.getString("Date"));
		}
	
	}
	
	
	public String getParams(String action , String lat , String lng,String details,String toplevel,String metric){
		StringBuilder sb = new StringBuilder(ConfigConts.weather_url).append(action).append("?");
		sb.append("apikey=").append(ConfigConts.weather_key).append("&");
		sb.append("language=").append("zh-tw").append("&");
		if(StringUtils.isNotEmpty(details)){
			sb.append("details=").append(details).append("&");
		}
		if(StringUtils.isNotEmpty(toplevel)){
			sb.append("toplevel=").append(toplevel).append("&");
		}
		if(StringUtils.isNotEmpty(metric)){
			sb.append("metric=").append(metric).append("&");
		}
		sb.append("q=").append(lat).append(",").append(lng);
		
		return sb.toString();
	}
	
	public static void main(String[] args) {
		WeatherClientImpl c= new WeatherClientImpl();
		Weather w= c.getWeather("22.628","113.832");
		System.out.println(JSONObject.toJSONString(w));
	}
	@Override
	public JSONArray getWeatherBy(String lat, String lng) {
		// TODO Auto-generated method stub
		return null;
	}
}
