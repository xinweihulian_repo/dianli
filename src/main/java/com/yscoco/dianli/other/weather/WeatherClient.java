package com.yscoco.dianli.other.weather;

import com.alibaba.fastjson.JSONArray;
import com.yscoco.dianli.other.weather.dto.Weather;
public interface WeatherClient {
	
	public Weather getWeather(String lat , String lng);
	
	//public JSONArray getWeatherByFeng(String lat , String lng);
	
	public JSONArray getWeatherBy(String lat , String lng); 
	
	
	
}
