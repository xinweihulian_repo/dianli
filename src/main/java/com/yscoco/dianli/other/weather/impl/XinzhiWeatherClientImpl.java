package com.yscoco.dianli.other.weather.impl;

import java.util.Date;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.yscoco.dianli.common.utils.HttpClient;
import com.yscoco.dianli.other.weather.WeatherClient;
import com.yscoco.dianli.other.weather.dto.Weather;
public class XinzhiWeatherClientImpl implements WeatherClient{
	private final String WEATHER_URL ="https://api.thinkpage.cn/v3/weather/daily.json?key=m4l7jy8jyaxjo1r1&language=zh-Hans&unit=f&start=0&days=1";
	//private final String FORECASTS12HOUR ="forecasts/v1/hourly/12hour/";
	private final String WEATHER_URL_FENGHE ="https://free-api.heweather.com/v5/forecast";
	private final String KEY ="d16bf2b31c0f45a4a20bec2091362314";
	// https://free-api.heweather.com/v5/forecast?city=31.233233,121.491900&key=d16bf2b31c0f45a4a20bec2091362314
	@Override
	public JSONArray getWeatherBy(String lat, String lng) {
		JSONArray results = null ;
		try {
			HttpClient client = new HttpClient();
			StringBuilder sb = new StringBuilder(WEATHER_URL_FENGHE);
			sb.append("?city=").append(lat).append(",").append(lng);
			sb.append("&key=").append(KEY);
			String r = client.get(sb.toString(), null);
			JSONObject json = JSONObject.parseObject(r);
			results = json.getJSONArray("HeWeather5");
			System.out.println(results);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return  results;
	}
	
	@Override
	public Weather getWeather(String lat , String lng){
		Weather dto = new Weather();
		try {
			HttpClient client = new HttpClient();
			StringBuilder sb = new StringBuilder(WEATHER_URL);
			sb.append("&location=").append(lat).append(":").append(lng);
			String r = client.get(sb.toString(), null);
			JSONObject json = JSONObject.parseObject(r);
			JSONArray results = json.getJSONArray("results");
			System.out.println(r);
			if(results!=null && results.size() > 0){
				JSONObject daily = results.getJSONObject(0).getJSONArray("daily").getJSONObject(0);
				JSONObject location = results.getJSONObject(0).getJSONObject("location");
				
				String name = location.getString("name");
				String codeDay = daily.getString("code_day");
				String codeNight = daily.getString("code_night");
				String high = daily.getString("high");
				String low = daily.getString("low");
				String lastUpdate = results.getJSONObject(0).getString("last_update");
				
				dto.setLocalizedName(name);
				dto.setMaxTemperature(high);
				dto.setMinTemperature(low);
				dto.setDayIcon(codeDay);
				dto.setNightIcon(codeNight);
				Date d = new Date();
				@SuppressWarnings("deprecation")
				int h = d.getHours();
				if(h>0 && h<18){
					dto.setDayIcon(codeDay);
				}else{
					dto.setDayIcon(codeNight);
				}
				dto.setDate(lastUpdate);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dto;
	}
	
	public static void main(String[] args) {
		XinzhiWeatherClientImpl s = new XinzhiWeatherClientImpl();
		//s.getWeather("39.93","116.40");
		s.getWeatherBy("31.233233","121.491900");
		
	}

	
	
	
}
