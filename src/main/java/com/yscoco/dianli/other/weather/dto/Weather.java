package com.yscoco.dianli.other.weather.dto;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Weather implements Serializable {

	private static final long serialVersionUID = -5282685290496981073L;
	
	@ApiModelProperty("当前日期")
	private String nowTime; 
	@ApiModelProperty("相对湿度")
	private Double hum; 
	@ApiModelProperty("最低温度")
	private String minTemperature;//
	@ApiModelProperty("最高温度")
	private String maxTemperature;//
	
	private String key;
	private String localizedName;//城市
	private String dayIcon;//早上预报
	private String nightIcon;//晚上预报
	private String date;//预报时间
	
	private List<WeatherInfo> WeatherInfos;//12小时信息
	
}
