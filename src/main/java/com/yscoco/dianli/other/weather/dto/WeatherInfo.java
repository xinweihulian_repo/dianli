package com.yscoco.dianli.other.weather.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class WeatherInfo {
	private String dateTime ;
	private String epochDateTime;
	private String weatherIcon;
	private String iconPhrase;
	private boolean isDaylight;
}
