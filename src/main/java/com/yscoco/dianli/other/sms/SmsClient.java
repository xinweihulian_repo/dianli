package com.yscoco.dianli.other.sms;

import java.util.HashMap;
import java.util.Map;

import com.yscoco.dianli.common.utils.HttpClient;
import com.yscoco.dianli.common.utils.Log;
import com.yscoco.dianli.constants.ConfigConts;

public class SmsClient {
	
	String URL = ConfigConts.getString("SMS_URL");
	String USERID = ConfigConts.getString("SMS_USERID");
	String ACCOUNT = ConfigConts.getString("SMS_ACCOUNT");
	String PASSWORD= ConfigConts.getString("SMS_PASSWORD");
	String SIGN= ConfigConts.getString("SMS_SIGN");

	public static final SmsClient instance = new SmsClient();
	
	private SmsClient(){}
	
	public static SmsClient getInstance(){
		return instance;
	}
	
	public void sendSmsCode(String mobile,String code){
		sendSmsMsg(mobile, "您的验证码："+code+",请及时完成输入验证,有效期10分钟。");
	}
	
	public void sendSmsMsg(String mobiles,String content){
		try {
			Map<String,String> params = new HashMap<String, String>();
			HttpClient client = new HttpClient();
			params.put("un", ACCOUNT);
			params.put("pw", PASSWORD);
			params.put("phone", mobiles);
			/*params.put("msg","【松露】"+ content);*/
			params.put("msg",SIGN+ content);
			String result = client.post(URL, params);
			System.out.println(result);
			Log.getCommon().error(result);
		} catch (Exception e) {
			Log.getCommon().error("发送验证码失败",e);
		}
	}
	
	public static void main(String[] args) {
		SmsClient.getInstance().sendSmsCode("18576728602", "123456");
	}
}
