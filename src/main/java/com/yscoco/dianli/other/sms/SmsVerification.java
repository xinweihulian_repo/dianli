package com.yscoco.dianli.other.sms;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.net.URL;

/**
 * 短信验证码
 * 
 * @author Mate
 *
 */
public class SmsVerification {

	//发送短信的地址
	private static final String URL = "http://api.sms7.cn/sms/?ac=send&uid=37813&pwd=bef9193d9b4684b54670e48be4d34768&template=100006&mobile=%s&content={\"code\":\"%s\"}";
	private static ConcurrentHashMap<String, String> Hash_Code = new ConcurrentHashMap<String, String>();

	/**
	 * 将手机号和对应验证码保存到hash
	 * 
	 * @param phone
	 * @param code
	 */
	public static void put(String phone, String code) {
		synchronized (Hash_Code) {
			Hash_Code.put(phone, code);
		}
	}

	/**
	 * 删除hash里的信息
	 * 
	 * @param phone
	 */
	public static void remove(String phone) {
		try {
			synchronized (Hash_Code) {
				Hash_Code.remove(phone);
			}
		} catch (Exception e) {
		}
	}

	/**
	 * 根据手机号，从hash获取验证码
	 * 
	 * @param phone
	 * @return
	 */
	public static String get(String phone) {
		String code;
		try {
			synchronized (Hash_Code) {
				code = Hash_Code.get(phone);
			}
		} catch (Exception e) {
			return null;
		}
		return code;
	}

	/**
	 * 发送验证码
	 * 
	 * @param phone
	 *            手机号
	 * @return 验证码
	 */
	public static String getCodeCheck(String phone) {
		String AUTH_CODE = "0123456789";
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < 4; i++) {
			int index = new Random().nextInt(AUTH_CODE.length());
			sb.append(AUTH_CODE.charAt(index));
		}
		String codeUrl = String.format(URL, phone, sb.toString());
		sendGet(codeUrl);
		Hash_Code.put(phone, sb.toString());
		return sb.toString();
	}
	public static void SendCode(String phone,String code)
	{
		String codeUrl = String.format(URL, phone, code);
		sendGet(codeUrl);
	}

	/**
	 * 验证短信验证码是否一致
	 * 
	 * @param phone
	 *            手机号 ,code 验证码
	 */
	public static boolean CheckCodeValid(String phone, String code) {
		String strCode = get(phone);
		if (strCode.equals(code)) {
			remove(phone);
			return true;
		} else
			return false;

	}

	private static String sendGet(String url) {
		String result = "";
		BufferedReader in = null;
		try {
			URL realUrl = new URL(url);
			// 打开和URL之间的连接
			URLConnection connection = realUrl.openConnection();
			// 设置通用的请求属性
			connection.setRequestProperty("accept", "*/*");
			connection.setRequestProperty("connection", "Keep-Alive");
			connection.setRequestProperty("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
			// 建立实际的连接
			connection.connect();
			// 获取所有响应头字段
			Map<String, List<String>> map = connection.getHeaderFields();
			// 遍历所有的响应头字段
			// for (String key : map.keySet()) {
			// System.out.println(key + "--->" + map.get(key));
			// }
			// 定义 BufferedReader输入流来读取URL的响应
			in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String line;
			while ((line = in.readLine()) != null) {
				result += line;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		// 使用finally块来关闭输入流
		finally {
			try {
				if (in != null) {
					in.close();
				}
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return result;
	}
	// public static void main(String[] args)
	// {
	// getCodeCheck("13480606785");
	//
	// }
	 /* public static void main(String[] args) {
		  boolean checkCodeValid = SmsVerification.CheckCodeValid("18665358053", getCodeCheck("18665358053"));
		  System.out.println(checkCodeValid);
	}*/

}
