package com.yscoco.dianli.other.email;

import java.util.HashMap;
import java.util.Map;

import com.yscoco.dianli.common.utils.HttpClient;
import com.yscoco.dianli.common.utils.Log;
import com.yscoco.dianli.constants.ConfigConts;

public class EmailClient {
    String MAIL_URL = ConfigConts.getString("mail_url");
    String MAIL_USERNAME = ConfigConts.getString("mail_username");
    String MAIL_PASSWORD = ConfigConts.getString("mail_password");
    String MAIL_FROM= ConfigConts.getString("mail_from");
    String MAIL_FROM_NAME= ConfigConts.getString("mail_from_name");
    String MAIL_SUBJECT= ConfigConts.getString("mail_subject");
    
    private static final EmailClient instance = new EmailClient();
    
    private EmailClient(){}
    
    public static EmailClient getInstance(){
        return instance;
    }
    
    public void sendCode(String eamil,String code){
        sendMsg(eamil, "您的验证码："+code+"");
        
    }
    
    public void sendMsg(String eamil,String content){
        try {
            Map<String,String> params = new HashMap<String, String>();
            params.put("apiUser", MAIL_USERNAME);
            params.put("apiKey", MAIL_PASSWORD);
            params.put("to", eamil);
            params.put("from", MAIL_FROM);
            params.put("fromName", MAIL_FROM_NAME);
            params.put("subject", MAIL_SUBJECT);
            params.put("html", content);
            HttpClient client = new HttpClient();
            String reslut =  client.post(MAIL_URL, params);
            Log.getCommon().debug(reslut);
            System.out.println("发送成功！"+eamil);
        } catch (Exception e) {
            Log.getCommon().error("发送邮箱验证码失败",e);
        }
    }
    
    public static void main(String[] args) {
        //EmailClient.getInstance().sendCode("382541649@qq.com", "789456");
        EmailClient.getInstance().sendMsg("lx18576728602@163.com", "lingaing");
        
    }
}
