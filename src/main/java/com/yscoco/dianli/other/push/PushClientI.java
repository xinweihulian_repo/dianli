package com.yscoco.dianli.other.push;

import java.util.Map;

public interface PushClientI {

	boolean push(String alias, String tag, String msg, Map<String, String> extra);

}
