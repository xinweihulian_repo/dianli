package com.yscoco.dianli.other.push.jpush;

import java.util.Map;
import java.util.HashMap;
import cn.jpush.api.JPushClient;
import cn.jpush.api.push.PushResult;
import cn.jpush.api.push.model.Message;
import cn.jpush.api.push.model.Options;
import cn.jpush.api.push.model.Platform;
import com.yscoco.dianli.common.base.Base;
import com.yscoco.dianli.common.utils.Log;
import cn.jpush.api.push.model.PushPayload;
import com.yscoco.dianli.other.push.PushClientI;
import cn.jpush.api.push.model.audience.Audience;
import cn.jpush.api.push.model.notification.AndroidNotification;
import cn.jpush.api.push.model.notification.IosNotification;
import cn.jpush.api.push.model.notification.Notification;

/**
 * 极光推送
 * @author lanston
 *
 */
public class JpushClient implements PushClientI {
	private String appKey="585dc35f247dbf20ef4dc20c";
	private String masterSecret="8e5f9cd307b07f0ab6f3bc7a";
	public void setAppKey(String appKey) {
		this.appKey = appKey;
	}
	public void setMasterSecret(String masterSecret) {
		this.masterSecret = masterSecret;
	}
	public static void main(String[] args) {
		Map<String,String> extra = new HashMap<String, String>();
        extra.put("title", "测试");
//		JpushClient j = new JpushClient();
//		j.push("140fe1da9ef83ac37fc", null, "测试", extra);
		
//aigo
		JpushClient j = new JpushClient();
		j.setAppKey("585dc35f247dbf20ef4dc20c");
		j.setMasterSecret("8e5f9cd307b07f0ab6f3bc7a");
		j.push("140fe1da9ef83ac37fc", null, "亲，您已经有两天未登陆【亲情守护APP】了哦，赶紧上去看看您的健康状态吧！", extra);
		
	}
	
	@Override
	public boolean push(String registrationId,String tag,String msg,Map<String,String> extra){
		JPushClient jpushClient = new JPushClient(masterSecret, appKey, 3); 
		try {
			PushResult result = null;
			if(!Base.empty(registrationId)){//根据push_id推送
				result = jpushClient.sendPush(buildPushByregistrationIds(msg, registrationId, extra));
			}
			return result.isResultOK();
		} catch (Exception e) {
			e.printStackTrace();
			Log.getCommon().error("推送失败");
			return false;
		}
	}
	
	
	
	
	
	

	private PushPayload buildPushAll(String msg,Map<String,String> extra) {
		 return PushPayload.newBuilder()
	                .setPlatform(Platform.all())
	                .setAudience(Audience.all())
	                .setNotification(Notification.newBuilder().setAlert(msg).build())
	                 .setMessage(Message.newBuilder()
	                        .setMsgContent("")
	                        .addExtras(extra)
	                        .build())
	                 .setOptions(Options.newBuilder().setApnsProduction(true).build())        
	                .build();
   }
	
	private PushPayload buildPushByTag(String msg,String tag,Map<String,String> extra) {
		 return PushPayload.newBuilder()
	                .setPlatform(Platform.all())
	                .setAudience(Audience.tag(tag))
	                 .setNotification(Notification.newBuilder().setAlert(msg).build())
	                 .setMessage(Message.newBuilder()
	                        .setMsgContent("")
	                        .addExtras(extra)
	                        .build())
	                 .setOptions(Options.newBuilder().setApnsProduction(true).build())         
	                .build();
    }
	
	// lingxiang 新增了声音 但是没效果setAlert(msg).build())//setNotification设置通知
	private PushPayload buildPushByAlias(String msg,String alias,Map<String,String> extra){
		 return PushPayload.newBuilder()
	                .setPlatform(Platform.all())//setPlatform设置平台
	                .setAudience(Audience.alias(alias))//setAudience设置受众
	                .setNotification(
	                		 Notification.newBuilder().setAlert(msg)
	                		 .addPlatformNotification(IosNotification.newBuilder()
                             .setAlert(msg)
                             //.setBadge(5)
                             .setSound("default")
                             .addExtra("from", "JPush")
                             .addExtras(extra)
                             .build())
                             .addPlatformNotification(AndroidNotification.newBuilder()
                             .addExtras(extra).setTitle(msg).build())
                      .build())
	                 .setMessage(Message.newBuilder()
	                        .setMsgContent("")
	                        .addExtras(extra)
	                        .build())
	                .setOptions(Options.newBuilder().setApnsProduction(true).build()) 
	                .build();
	}
	
	private PushPayload buildPushByregistrationIds(String msg,String registrationId,Map<String,String> extra){
		 return PushPayload.newBuilder()
	                .setPlatform(Platform.all())
	                .setAudience(Audience.registrationId(registrationId))
	                 .setNotification(Notification.newBuilder().setAlert(msg).build())
	                 .setMessage(Message.newBuilder()
	                        .setMsgContent("")
	                        .addExtras(extra)
	                        .build())
	                .build();
	}
	
}
