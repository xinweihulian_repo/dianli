package com.yscoco.dianli.common.form;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PointExportForm {
	private String userId;
	private String token;
	private String companyName;
	private String moritorStationName;
	private String masterName;
	private String branchName;
	private String pointName;
	private String sensorId; //设备标识
	private String protectorType;//保护器类型
	private String timeStart;
	private String timeEnd;
}
