package com.yscoco.dianli.common.vo;

import java.util.HashMap;
import java.util.Map;

/**
 * 验证码业务类型
 * 
 * @author dengjianjun
 *
 */
public enum VcodeType {

	/** 手机OTP登录 */
	LOGIN(10),

	/** 验证手机注册 */
	REG_MOBILE(10),
	
	/**重新绑定手机号 */
	BIND_MOBILE(10),

	/** 重置密码（找回密码） */
	REST_PWD_MOBILE(10);

	

	private int expire = 0;

	private VcodeType(int expire) {
		this.expire = expire;
	}

	public int getExpire() {
		return expire;
	}

	public void setExpire(int expire) {
		this.expire = expire;
	}
	 private static final Map<Integer, VcodeType> intToEnum = new HashMap<Integer, VcodeType>();
    static {
        intToEnum.put(0, LOGIN);
        intToEnum.put(1, REG_MOBILE);
        intToEnum.put(2, BIND_MOBILE);
        intToEnum.put(3, REST_PWD_MOBILE);
    }
    public static VcodeType fromInt(Integer expire) {
        return intToEnum.get(expire);
    }

}
