package com.yscoco.dianli.common.vo;

import java.io.Serializable;
import java.util.Set;

import lombok.Getter;
import lombok.Setter;

/**
 * @discription session缓存信息
 * @author lansiming
 * @created 2016-7-22  下午6:00:44  
 */
@Setter
@Getter
public class SessionInfo implements Serializable{
	public static final String KEY= "sessionInfo";
	public static final String LOGIN_CODE_KEY= "loginCode";
	private static final long serialVersionUID = 1346438020849476537L;
	private String token;
	private String userId;
	private String userName;
	private String userHeadImg;
	private Set<String> sysActions;
	private int loginErrorTimes;//登陆失败次数
	 
}
