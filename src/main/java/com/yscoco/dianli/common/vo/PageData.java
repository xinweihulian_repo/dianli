package com.yscoco.dianli.common.vo;

import java.util.List;

public class PageData<T> implements java.io.Serializable {
    private static final long serialVersionUID = 1L;

    public List<T> list ;
    private int count;
    
	public List<T> getList() {
		return list;
	}
	public void setList(List<T> list) {
		this.list = list;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}

	
	
    
    
}
