package com.yscoco.dianli.common.vo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;



@JsonInclude(Include.NON_NULL)
public class Message<T> implements Serializable{

	private static final long serialVersionUID = -3649275882164060087L;
	
	private String code;
	private String msg;
	private T data;
	
	
	public static Message<?> newError(String errorInfo){
		Message<?> msg = new Message<>();
		msg.setCode(Code.SYS_ERR.getCode());
		msg.setMsg(errorInfo);
		return msg;
	}
	
	public static Message<?> newError(String code ,String errorInfo){
		Message<?> msg = new Message<>(code,errorInfo);
		return msg;
	}
	
	public static Message<?> newError(Code code){
		Message<?> msg = new Message<>(code);
		return msg;
	}
	
	public static <T> Message<T> success(T data){
		Message<T> msg = new Message<T>(Code.SUCCESS,data);
		return msg;
	}
	public static <T> Message<T> success(){
		Message<T> msg = new Message<T>(Code.SUCCESS);
		return msg;
	}
	public Message() {
		super();
	}
	public Message(Code msgCode) {
		super();
		this.setCode(msgCode.getCode());
		this.setMsg(msgCode.getMsg());
	}

	public Message(Code msgCode, T data) {
		super();
		this.setCode(msgCode.getCode());
		this.setMsg(msgCode.getMsg());
		this.setData(data);
	}
	public Message(String code, String msg, T data) {
		super();
		this.setCode(code);
		this.setMsg(msg);
		this.setData(data);
	}
	public Message(String code, String msg) {
		super();
		this.setCode(code);
		this.setMsg(msg);
	}
	
	public String getCode() {
		return code.toString();
	}

	public void setCode(String msgCode) {
		this.code = msgCode;
	}

	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public T getData() {
		return data;
	}
	public void setData(T data) {
		this.data = data;
	}
}
