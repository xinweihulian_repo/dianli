package com.yscoco.dianli.common.vo;


public enum Code {
	SUCCESS("0","成功"),
	ERROR("1","失败"),
	NULL_TOKEN("2","token为空"),
	NOT_PARAM("11000","缺少必要参数"),
	NOT_ACCOUNT("11001","账号未注册,请联系管理员"),
	NOT_LOGIN("11002","未登录"),
	NOT_ACTION("11003","没有权限"),
	NOT_SELF("11004","不能是自己"),
	NOT_SAME("11005","新密码与旧密码不能相同"),
	NOT_PASSWORD("11006","密码设置不能为空"),
	NOT_CONFIRM_PASSWORD("11007","确认密码不能为空"),
	NOT_SAME_CONFIRM_PASSWORD("11008","密码设置与确认密码不一致!请重新输入!"),
	NO_DATALIST("11009","暂无提醒数据"),
	LOGIN_MAX_COUNT("11010","登陆次数受限,请明天再尝试登陆，或者联系管理"),
	NO_DATA("110","暂无故障数据"),
	NO_DATAS("119","暂无数据"),
	SYS_ERR("500","系统异常"),
	NOT_PARAM501("501","设备编号不能为空"),
	EXCEL_NOTEXIST("503","Excel文件不存在"),
	EXCEL_FORMATERRORS("506","Excel文件格式错误,请检查标题列"),
	EXCEL_FORMATERROR("504","Excel文件格式错误,检查monitorFlag是否为空"),
	XWDataEntry_ERROR("506","XWDataEntry_ERROR,联系管理员"),
	/**
	 * 温度采集不能设置三相一体保护器
	 */
	SYS_ERR1("10500","温度采集不能设置三相一体保护器"),
	ERR_OTHER("11999","未知错误"),
	ERR_USERNAME("00001","错误的用户名"),
	ERR_MOBILE_EMAIL("00001","错误的手机号码、邮箱"),
	ERR_PARAM("00001","参数错误"),
	ERR_DIANJISYNC("0001","失败"),
	ERR_PWD("00002","账户未注册或者密码错误"),
	ERR_SMSCODE("00003","验证码错误"),
	ERR_TOKEN("00004","验证密钥过期，请重新登录"),
	ERR_DEL("00006","已经被禁用"),
	ERR_PAY("00007","支付出错"), 
	ERR_LIMIT("00010","访问限制"), 
	ERR_DELETE("00011","超级管理员无法删除"), 
	ERR_INFO("00012","超级管理员资料无法被修改"), 
	NOTEXIST_MOBILE("20006","账号不存在"),
	Error_MOBILE("20005","手机格式错误"),
	EXIST_MOBILE("20001","手机号码已经存在"),
	EXIST_USERNAME("20002","该用户名已经存在"),
	EXIST_EAMIL("20003","邮箱已经存在"),
	EXIST_OPER("20004","重复操作"), 
	DEVICE_NOTEXIST("20005","该设备不存在"),
	EXIST_MONITORSTATION_NAME("20006","该监测站名称已存在"),
	Input_Address("20007","请输入app上传URL"),
	ERR_DIANJI_CONFIG("20008","获取电机配置失败"),
	NOTEXIST("20009","查询id不存在"),
	;
	private String code ;
	private String msg;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	private Code(String code,String msg){
		this.setCode(code);
		this.setMsg(msg);
	}
	
	@Override
	public String toString() {
		return code;
	}
}
