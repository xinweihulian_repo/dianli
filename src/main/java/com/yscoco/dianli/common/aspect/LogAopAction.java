package com.yscoco.dianli.common.aspect;

import com.alibaba.fastjson.JSON;
import com.yscoco.dianli.common.utils.IPUtil;
import com.yscoco.dianli.entity.member.MemberInfo;
import com.yscoco.dianli.entity.sys.LogEntity;
import com.yscoco.dianli.entity.sys.SysUser;
import com.yscoco.dianli.other.cache.RedisCacheUtil;
import com.yscoco.dianli.service.comon.NosqlKeyManager;
import com.yscoco.dianli.service.member.MemberService;
import com.yscoco.dianli.service.sys.LogEntityService;
import com.yscoco.dianli.service.sys.SysUserService;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @Author: onion..
 * @Description: TODO
 * @Date: 2020/10/27 13:57
 */
@Aspect
public class LogAopAction {
  
    @Resource
    private LogEntityService logservice;
    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private MemberService memberService;
    @Resource
    private RedisCacheUtil redisCache;


  /*  @Pointcut("execution(* com.yscoco.dianli.controller..*.*(..))" +
            "|| execution(* com.yscoco.dianli.service..*.*(..)))"*/
    @Pointcut("execution(* com.yscoco.dianli.controller..*.*(..))")
    private void controllerAspect(){}//定义一个切入点

    @Around("controllerAspect()")
    public Object around(ProceedingJoinPoint pjp) throws Throwable {
        //方法通知前获取时间,为什么要记录这个时间呢？当然是用来计算模块执行时间的
        long start = System.currentTimeMillis();

        //执行方法
        Object result =pjp.proceed();

        //常见日志实体对象
        LogEntity log = new LogEntity();

        MethodSignature signature = (MethodSignature) pjp.getSignature();
        Method method = signature.getMethod();

        //请求的方法名
        String className = pjp.getTarget().getClass().getName();
        // 拦截的方法名称。当前正在执行的方法
        String methodName = pjp.getSignature().getName();

        // 请求的参数
        Object[] args = pjp.getArgs();

        Object[] arguments = new Object[args.length];
        for (int i = 0; i < args.length; i++) {
            if (args[i] instanceof ServletRequest || args[i] instanceof ServletResponse || args[i] instanceof MultipartFile)
                continue;
            arguments[i] = args[i];
        }
        log.setMethod(className + "." + methodName + "()");

        if (null != method) {
            // 判断是否包含自定义的注解，说明一下这里的SystemLog就是我自己自定义的注解
            if (method.isAnnotationPresent(SystemLog.class)) {
                SystemLog systemlog = method.getAnnotation(SystemLog.class);
                //获取登录用户账户
                HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();

                String token = request.getParameter(NosqlKeyManager.TOKEN);

                if(null==token){
                    List<Map> maps = JSON.parseArray(JSON.toJSONString(arguments), Map.class);
                    for (int i = 0; i < maps.size(); i++) {
                        token = maps.get(i).get(NosqlKeyManager.TOKEN).toString();
                        break;
                    }
                }

                String userFlag = redisCache.get(NosqlKeyManager.User,token);

                if(!StringUtils.isEmpty(token)){getLoginUser(log, token, userFlag);}


                //获取系统ip,这里用的是我自己的工具类,可自行网上查询获取ip方法
                  log.setIp(IPUtil.getIpAddr(request));
                  log.setCreateTime(new Date());
                
                log.setMethodDesc(systemlog.methods());
                log.setModule(systemlog.module());
                try {
                    long end = System.currentTimeMillis();
                    log.setReqParam(JSON.toJSONString(arguments));
                    log.setResponseTime(""+(end-start));
                    log.setCommit("执行成功！");
                    logservice.save(log);
                } catch (Throwable e) {
                    long end = System.currentTimeMillis();
                    log.setReqParam(e.getMessage().substring(0,e.getMessage().length()>255?255:e.getMessage().length()));
                    log.setReqParam(e.getMessage());
                    log.setResponseTime(""+(end-start));
                    log.setCommit("执行失败");
                    logservice.save(log);
                }
            }
        }
        return result;
    }

    private void getLoginUser(LogEntity log, String token, String userFlag) {
        if(StringUtils.isEmpty(userFlag)){
            SysUser sysUser =sysUserService.getByToken(token);
                if(sysUser!=null){
                    log.setUserId(sysUser.getId());
                    log.setUserName(sysUser.getAccount()+":"+sysUser.getMobilePhone());
                    redisCache.set(NosqlKeyManager.SysUser, JSON.toJSONString(sysUser),token,60, TimeUnit.MINUTES);
                    redisCache.set(NosqlKeyManager.User,NosqlKeyManager.SysUserFlag,token,60, TimeUnit.MINUTES);
                }else {
                    MemberInfo member = memberService.getByToken(token);
                    log.setUserId(member.getId());
                    log.setUserName(member.getAccount()+":"+member.getMobile());
                    redisCache.set(NosqlKeyManager.MbmberUser,JSON.toJSONString(sysUser),token,60, TimeUnit.MINUTES);
                    redisCache.set(NosqlKeyManager.User,NosqlKeyManager.MemberFlag,token,60, TimeUnit.MINUTES);
                }
        }


        if(NosqlKeyManager.SysUserFlag.equals(userFlag)){
            SysUser sysUser = JSON.parseObject(redisCache.get(NosqlKeyManager.SysUser, token), SysUser.class);
                log.setUserId(sysUser.getId());
                log.setUserName(sysUser.getAccount()+":"+sysUser.getMobilePhone());
        }
        if(NosqlKeyManager.MemberFlag.equals(userFlag)){
            MemberInfo member = JSON.parseObject(redisCache.get(NosqlKeyManager.MbmberUser, token), MemberInfo.class);
            log.setUserId(member.getId());
            log.setUserName(member.getAccount()+":"+member.getMobile());
        }
    }
}
