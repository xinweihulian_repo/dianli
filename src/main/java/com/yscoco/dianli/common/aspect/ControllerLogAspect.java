package com.yscoco.dianli.common.aspect;

import java.util.Enumeration;
import javax.servlet.http.HttpServletRequest;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.yscoco.dianli.common.exception.BizException;
import com.yscoco.dianli.common.vo.Message;


/**
 * @discription controller  方法切面处理日志
 * @author lansiming
 * @created 2016-7-29  下午4:40:26  
 */
@Aspect
@Component
public class ControllerLogAspect {
	private static final Logger log = LoggerFactory.getLogger(ControllerLogAspect.class);
	public ControllerLogAspect() {
		log.info("========================new ControllerLogAspect() --- start log=====================");
	}

	@Pointcut("within(@org.springframework.stereotype.Controller *)")
	public void cutController() {

	}

	@AfterReturning(pointcut = "cutController()", returning = "r")
	public void afterMethod(JoinPoint point, Object r) {
		if (r instanceof Message<?>) {
			HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
			if(request != null){
				String reqUrl = request.getRequestURI();
				String reqConten = request.getContextPath();
				reqUrl = reqUrl.replace(reqConten, "");
				Message<?> m = (Message<?>) r;
				String reqParams = this.getParamterString(request);
				
				log.info("methodUrl-->"+reqUrl +";\n method reqParmas --> "+reqParams+" ;\n method result ---> " + m.getMsg());
			}
			
		}
	}
	@Around("execution (* com.yscoco.dianli.controller..*.*(..))")  
    public Object around(ProceedingJoinPoint point) throws Throwable{ 
		Object object =  null;
        try {
        	object = point.proceed();  
		} catch (Exception e) {
			HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
			if(request != null){
				String reqUrl = request.getRequestURI();
				String reqConten = request.getContextPath();
				reqUrl = reqUrl.replace(reqConten, "");
				String reqParams = this.getParamterString(request);
				
				BizException be = null;
				if(e instanceof BizException){
					be = (BizException)e;
					log.error("methodUrl-->"+reqUrl +";\n method reqParmas --> "+reqParams+" ;\n method result ---> "+be.getMsg());
				}else{
					log.error("methodUrl-->"+reqUrl +";\n method reqParmas --> "+reqParams+" ;\n method result ---> ",e);
					
				}
				
				throw e;
			}
		}
        
        return object;  
    }  
	public String getParamterString(HttpServletRequest req){
		Enumeration<String> keys =  req.getParameterNames();
		StringBuilder sb = new StringBuilder();
		while (keys.hasMoreElements()) {
			String key = keys.nextElement();
			sb.append(key).append(":").append(req.getParameter(key)).append(" ; ");
		}
		return sb.toString() ;
	}
}
