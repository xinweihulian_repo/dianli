package com.yscoco.dianli.common.base;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.yscoco.dianli.entity.member.MemberInfo;
import com.yscoco.dianli.entity.sys.SysUser;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.yscoco.dianli.common.exception.BizException;
import com.yscoco.dianli.common.utils.Log;
import com.yscoco.dianli.common.vo.Code;
import com.yscoco.dianli.service.member.MemberService;
import com.yscoco.dianli.service.sys.SysUserService;

public class BaseController {
	@Autowired
	protected HttpSession session;
	@Autowired
	protected HttpServletRequest request;
	@Autowired
	protected HttpServletResponse response;
	
	public volatile static BaseController instance;

	@Autowired
	protected MemberService memberService;

	@Autowired
	protected SysUserService sysUserService;
	
	public BaseController(){
	}
	
	public static BaseController getInstance() {
		if (instance == null) {
			synchronized (BaseController.class) {
				if (instance == null) {
					instance = new BaseController();
				}
			}
		}
		return instance;
	}

	protected SysUser getSysUserBytoken(){
		return sysUserService.getByToken( request.getParameter("token"));
	}

	protected MemberInfo getMemberInfoBytoken(){
		return memberService.getByToken( request.getParameter("token"));
	}

	public void checkLogionStatus() {
		String token = request.getParameter("token");
		String memberId = memberService.getIdByToken(token);
		String sysUserId = sysUserService.getIdByToken(token);
		if (StringUtils.isEmpty(memberId) && StringUtils.isEmpty(sysUserId)) {
			throw new BizException(Code.NOT_LOGIN);
		}
	}

	public String getLoginMemberId() {
		String token = request.getParameter("token");
		String memberId = memberService.getIdByToken(token);
		if (StringUtils.isEmpty(memberId)) {
			// throw new BizException(Code.NOT_LOGIN);
			return null;
		}
		if (memberId == null) {
			// throw new BizException(Code.NOT_LOGIN);
			return null;
		}
		return memberId;
	}

	public String getLoginManagerId() {
		String token = request.getParameter("token");
		/* token = "cb4b8fa4af4fdb25d35a2c82bc0b8a04"; */
		/* token="80ec05477d2d1bf74ad38f3f89f0b05a"; */
		String sysUserId = sysUserService.getIdByToken(token);
		if (StringUtils.isEmpty(sysUserId)) {
			// throw new BizException(Code.NOT_LOGIN);
			return null;
		}
		if (sysUserId == null) {
			// throw new BizException(Code.NOT_LOGIN);
			return null;
		}
		return sysUserId;
	}

	public Map<String, String> getParamterMap(HttpServletRequest req) {
		Enumeration<String> keys = req.getParameterNames();
		Map<String, String> params = new HashMap<String, String>();
		while (keys.hasMoreElements()) {
			String key = keys.nextElement();
			params.put(key, req.getParameter(key));
		}
		return params;
	}

	public static void setResponseHeader(HttpServletResponse response, String fileName) {
		try {
			fileName = new String(fileName.getBytes(), "UTF-8");
			response.setContentType("application/octet-stream;charset=UTF-8");
			response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
			response.addHeader("Pargam", "no-cache");
			response.addHeader("Cache-Control", "no-cache");
		} catch (Exception ex) {
			Log.getCommon().error("setResponseHeader失败", ex);
		}
	}

	protected HttpSession getSession() {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
				.getRequest();
		HttpSession session = request.getSession();
		return session;
	}

	protected HttpServletRequest getRequest() {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
				.getRequest();
		return request;
	}

	protected HttpServletResponse getResponse() {
		HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
				.getResponse();
		return response;
	}

	protected void onGetRequestParse() {
		com.yscoco.dianli.common.utils.ExLogUtil.getLogger()
				.info(this.request.getRequestURI() + "?" + request.getQueryString());
	}

}
