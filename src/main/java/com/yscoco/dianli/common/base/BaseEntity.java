package com.yscoco.dianli.common.base;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.yscoco.dianli.common.utils.JsonDateSerialize;
import com.yscoco.dianli.constants.YN;

@MappedSuperclass
/*@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)*/
public class BaseEntity implements Serializable {


	private static final long serialVersionUID = 963758019652824529L;

	@Id
	@GenericGenerator(name = "uuid", strategy = "uuid")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="uuid")
    @Column(length=32)
	private String id;
     
	@JsonSerialize(using=JsonDateSerialize.class)
	@Column(updatable = false,columnDefinition="datetime comment '创建时间'")
	private Date createTime;
		
	@Column(updatable = false,length=32)
	private String createBy;
	@JsonSerialize(using=JsonDateSerialize.class)
	@Column(columnDefinition="datetime comment '修改时间'")
	private Date modifyTime;

	@Column(length=32)
	private String modifiedBy;
	@Column(length=1)
	@Enumerated(EnumType.STRING)
	private YN delFlag = YN.N;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public YN getDelFlag() {
		return delFlag;
	}

	public void setDelFlag(YN delFlag) {
		this.delFlag = delFlag;
	}



}
