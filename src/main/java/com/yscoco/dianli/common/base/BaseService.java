package com.yscoco.dianli.common.base;

import java.io.Serializable;
import com.yscoco.dianli.dao.base.BaseDao;

public abstract class BaseService<E> extends Base {
	
    protected BaseDao<E> dao;
    

    /**
     * @param id
     * @return
     */
    public E getEntity(Serializable id) {
        return dao.getEntity(id);
    }


    /**
     * @param id
     * @return
     */
    public void delete(Serializable id) {
        dao.delete(id);
    }

    /**
     * @param id
     * @param newEntity
     * @return
     */
    public void update(Serializable id, E newEntity) {
        E entity = getEntity(id);
        if (notEmpty(entity)) {
          //  BeanUtils.copyProperties(dao.init(newEntity), entity);
        }
    }

    /**
     * @param entity
     * @return
     */
    public Serializable save(E entity) {
        return dao.save(entity);
    }



}
