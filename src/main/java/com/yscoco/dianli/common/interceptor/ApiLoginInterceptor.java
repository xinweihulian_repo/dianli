package com.yscoco.dianli.common.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.NamedThreadLocal;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.yscoco.dianli.common.exception.BizException;
import com.yscoco.dianli.common.utils.Log;
import com.yscoco.dianli.common.vo.Code;
import com.yscoco.dianli.service.member.MemberService;
import com.yscoco.dianli.service.sys.SysUserService;

/**
 * 登录拦截
 * 
 * @author lanston
 */
public class ApiLoginInterceptor extends HandlerInterceptorAdapter {

	@Autowired
	MemberService memberService;
	
	@Autowired
	SysUserService sysUserService;
	
	private static final ThreadLocal<Long> startTimeThreadLocal = new NamedThreadLocal<Long>("ThreadLocal StartTime");
	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		
		long beginTime = System.currentTimeMillis();//1、开始时间  
             startTimeThreadLocal.set(beginTime);		//线程绑定变量（该数据只有当前请求的线程可见）  
           //  System.out.println("========================test=====================================");
//        String token = request.getParameter("token");
//        if(StringUtils.isEmpty(token)){
//        	throw new BizException(Code.NOT_LOGIN);
//        }else if(!memberService.checkToken(token) && !sysUserService.checkToken(token)){
//        	throw new BizException(Code.NOT_LOGIN);
//        }
//        System.out.println("========================test=====================================");
		return super.preHandle(request, response, handler);
	}
	
	

	@Override
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		super.postHandle(request, response, handler, modelAndView);
	}
	
	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, 
			Object handler, Exception ex) throws Exception {
		long beginTime = startTimeThreadLocal.get();//得到线程绑定的局部变量（开始时间）  
		long endTime = System.currentTimeMillis(); 	//2、结束时间  
        Log.getCommon().info("  URI: {} 耗时：{}毫秒   最大内存: {}m  已分配内存: {}m  已分配内存中的剩余空间: {}m  最大可用内存: {}m",
				request.getRequestURI(), (endTime - beginTime),Runtime.getRuntime().maxMemory()/1024/1024, Runtime.getRuntime().totalMemory()/1024/1024, Runtime.getRuntime().freeMemory()/1024/1024, 
				(Runtime.getRuntime().maxMemory()-Runtime.getRuntime().totalMemory()+Runtime.getRuntime().freeMemory())/1024/1024); 
		
	}
}
