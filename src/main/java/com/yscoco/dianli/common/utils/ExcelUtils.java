package com.yscoco.dianli.common.utils;

import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletResponse;

import com.yscoco.dianli.common.exception.BizException;
import com.yscoco.dianli.common.vo.Code;

public class ExcelUtils {
	
	/** 
	 * 设置请求头和下载到浏览器的文件名称
	 * @param response
	 * @param fileName 文件名
	 */
	public static void setResponseHeader(HttpServletResponse response, String fileName) {  
		String sdf = new SimpleDateFormat("yyyyMMddhhss").format(System.currentTimeMillis());
		StringBuffer sb =new StringBuffer(fileName);
		String name = sb.append(sdf).append(".xls").toString();
        try {  
        	fileName = new String(name.getBytes(),"ISO8859-1");  
            response.setContentType("application/octet-stream;charset=ISO8859-1");  
            response.setHeader("Content-Disposition", "attachment;filename="  
                       + fileName);  
            response.addHeader("Pargam", "no-cache");  
            response.addHeader("Cache-Control", "no-cache");  
        } catch (Exception ex) {  
        	throw new BizException(Code.SYS_ERR);
        }  
   }  
	
}
