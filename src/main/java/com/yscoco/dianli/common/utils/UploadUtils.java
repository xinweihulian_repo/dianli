package com.yscoco.dianli.common.utils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.FileUtils;

import com.yscoco.dianli.constants.ConfigConts;

public class UploadUtils {
	private static String FILE_AB_UPLOAD_PATH = ConfigConts.getString("FILE_AB_UPLOAD_PATH");
	private static String FILE_URL = ConfigConts.getString("FILE_URL");
	public static String uploadImg(InputStream in) throws IOException{
		String photoName = RandomUtils.get32UUID();
		String realPath =  "img/" + DateUtils.getNowTime(DateUtils.DATE_SMALL_STR) + "/" + photoName + ".jpg";
		String tempPath = FILE_AB_UPLOAD_PATH+realPath.replace("/", File.separator);
		FileUtils.copyInputStreamToFile(in, new File(tempPath));
		return FILE_URL+realPath;
	}
	
	public static String uploadFile(InputStream in,String ext) throws IOException{
		String photoName = RandomUtils.get32UUID();
		String realPath = "file/" + DateUtils.getNowTime(DateUtils.DATE_SMALL_STR) + "/" + photoName + "."+ext;
		String tempPath = FILE_AB_UPLOAD_PATH+realPath.replace("/", File.separator);
		System.out.println("realPath:"+realPath);
		FileUtils.copyInputStreamToFile(in, new File(tempPath));
		
		return FILE_URL+realPath;
	}
	
	
	public static void main(String[] args) throws IOException {
		UploadUtils.uploadImg(null);
	}

}
