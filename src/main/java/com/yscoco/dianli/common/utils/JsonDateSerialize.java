package com.yscoco.dianli.common.utils;

import java.io.IOException;
import java.util.Date;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class JsonDateSerialize extends JsonSerializer<Object>{

	@Override
	public void serialize(Object value, JsonGenerator gen,
			SerializerProvider serializers) throws IOException,
			JsonProcessingException {
		if(value!=null){
			String r = null;
			if(value instanceof Date){
				r = DateUtils.format((Date)value, DateUtils.DATE_FULL_STR);
			}
			if(value instanceof String){
				
				r = DateUtils.format(new Date(Long.valueOf(value.toString())), DateUtils.DATE_FULL_STR);
			}
			if(value instanceof Long){
				r = DateUtils.format(new Date((Long)value), DateUtils.DATE_FULL_STR);
			}
			gen.writeObject(r);
		}else{
			gen.writeString("");
		}
	}

	

}
