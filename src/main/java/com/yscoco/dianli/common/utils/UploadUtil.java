package com.yscoco.dianli.common.utils;


import java.io.File;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.web.multipart.MultipartFile;

/**
 * 文件上传
 * @author lingxiang
 *
 */
public class UploadUtil {
	
	/**
	 * 处理文件上传
	 * @param file
	 * @param basePath  存放文件的 目录的绝对路径
	 * 					serviceContext.getRealPath("/upload")
	 * @return
	 */
	public static String upload(MultipartFile file,String basePath) {
		String orgFileName = file.getOriginalFilename();
		String fileName = UUID.randomUUID().toString()+"."+FilenameUtils.getExtension(orgFileName);
		try {
			File targetFile = new File(basePath,fileName);
			FileUtils.writeByteArrayToFile(targetFile, file.getBytes());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fileName;
	}
}