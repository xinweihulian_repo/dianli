package com.yscoco.dianli.common.utils;

import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Hex;

import com.yscoco.dianli.common.exception.BizException;
import com.yscoco.dianli.common.vo.Code;


/**
 * 加密工具
 * 
 * @author dengjianjun
 *
 */
public class EncryptUtil {

	/** RSA最大加密明文大小 */
	private static final int MAX_ENCRYPT_BLOCK = 117;

	/** RSA最大解密密文大小 */
	private static final int MAX_DECRYPT_BLOCK = 128;


	/**
	 * AES加密
	 * 
	 * @param content
	 *            待加密内容
	 * @param secretKey
	 *            密钥
	 * @param ivBytes
	 *            向量
	 * @return
	 */
	public static byte[] aesEncrypt(String content, byte[] secretKey, byte[] ivBytes) {
		if (ivBytes == null || ivBytes.length != 16) {
			throw new BizException(Code.ERROR);
		}

		try {
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			IvParameterSpec iv = new IvParameterSpec(ivBytes);
			cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(secretKey, "AES"), iv);

			return cipher.doFinal(content.getBytes(StandardCharsets.UTF_8));
		} catch (Exception e) {
			throw new BizException(Code.ERROR);
		}
	}

	/**
	 * AES解密
	 * 
	 * @param encryptBytes
	 *            加密内容
	 * @param secretKey
	 *            密钥
	 * @param ivBytes
	 *            向量
	 * @return
	 */
	public static String aesDecrypt(byte[] encryptBytes, byte[] secretKey, byte[] ivBytes) {
		if (ivBytes == null || ivBytes.length != 16) {
			throw new BizException(Code.ERROR);
		}

		try {
			Cipher deCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			IvParameterSpec iv = new IvParameterSpec(ivBytes);
			deCipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(secretKey, "AES"), iv);

			byte[] bytes = deCipher.doFinal(encryptBytes);
			return new String(bytes, StandardCharsets.UTF_8);
		} catch (Exception e) {
			throw new BizException(Code.ERROR);
		}
	}

	/**
	 * RSA签名
	 * 
	 * @param content
	 *            待签名内容
	 * @param privateKey
	 *            私钥
	 * @return
	 */
	public static byte[] rsaSign(String content, byte[] privateKey) {
		try {
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			PrivateKey priKey = keyFactory.generatePrivate(new PKCS8EncodedKeySpec(privateKey));
			Signature signature = Signature.getInstance("SHA1WithRSA");
			signature.initSign(priKey);
			signature.update(content.getBytes(StandardCharsets.UTF_8));

			return signature.sign();
		} catch (Exception e) {
			throw new BizException(Code.ERROR);
		}
	}

	/**
	 * RSA签名
	 * 
	 * @param content
	 *            待签名内容
	 * @param privateKey
	 *            私钥
	 * @return
	 */
	public static byte[] rsa256Sign(String content, byte[] privateKey) {
		try {
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			PrivateKey priKey = keyFactory.generatePrivate(new PKCS8EncodedKeySpec(privateKey));
			Signature signature = Signature.getInstance("SHA256WithRSA");
			signature.initSign(priKey);
			signature.update(content.getBytes(StandardCharsets.UTF_8));

			return signature.sign();
		} catch (Exception e) {
			throw new BizException(Code.ERROR);
		}
	}

	/**
	 * RSA验证签名
	 * 
	 * @param content
	 *            待验证内容
	 * @param signBytes
	 *            签名内容
	 * @param publicKey
	 *            公钥
	 * @return
	 */
	public static boolean rsaSignCheck(String content, byte[] signBytes, byte[] publicKey) {
		try {
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			PublicKey pubKey = keyFactory.generatePublic(new X509EncodedKeySpec(publicKey));
			Signature signature = Signature.getInstance("SHA1WithRSA");
			signature.initVerify(pubKey);
			signature.update(content.getBytes(StandardCharsets.UTF_8));

			return signature.verify(signBytes);
		} catch (Exception e) {
			
			throw new BizException(Code.ERROR);
		}
	}

	/**
	 * RSA验证签名
	 * 
	 * @param content
	 *            待验证内容
	 * @param signBytes
	 *            签名内容
	 * @param publicKey
	 *            公钥
	 * @return
	 */
	public static boolean rsa256SignCheck(String content, byte[] signBytes, byte[] publicKey) {
		try {
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			PublicKey pubKey = keyFactory.generatePublic(new X509EncodedKeySpec(publicKey));
			Signature signature = Signature.getInstance("SHA256WithRSA");
			signature.initVerify(pubKey);
			signature.update(content.getBytes(StandardCharsets.UTF_8));

			return signature.verify(signBytes);
		} catch (Exception e) {
			throw new BizException(Code.ERROR);
		}
	}

	/**
	 * RSA加密
	 * 
	 * @param content
	 *            待加密内容
	 * @param publicKey
	 *            公钥
	 * @return
	 */
	public static byte[] rsaEncrypt(String content, byte[] publicKey) {
		try {
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			PublicKey pubKey = keyFactory.generatePublic(new X509EncodedKeySpec(publicKey));
			Cipher cipher = Cipher.getInstance("RSA");
			cipher.init(Cipher.ENCRYPT_MODE, pubKey);
			byte[] data = content.getBytes(StandardCharsets.UTF_8);
			int inputLen = data.length;
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			int offSet = 0;
			byte[] cache;
			int i = 0;
			// 对数据分段加密
			while (inputLen - offSet > 0) {
				if (inputLen - offSet > MAX_ENCRYPT_BLOCK) {
					cache = cipher.doFinal(data, offSet, MAX_ENCRYPT_BLOCK);
				} else {
					cache = cipher.doFinal(data, offSet, inputLen - offSet);
				}
				out.write(cache, 0, cache.length);
				i++;
				offSet = i * MAX_ENCRYPT_BLOCK;
			}

			return out.toByteArray();
		} catch (Exception e) {
			throw new BizException(Code.ERROR);
		}
	}

	/**
	 * RSA解密
	 * 
	 * @param encryptedData
	 *            待解密内容
	 * @param privateKey
	 *            密钥
	 * @return
	 */
	public static String rsaDecrypt(byte[] encryptedData, byte[] privateKey) {
		try {
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			PrivateKey priKey = keyFactory.generatePrivate(new PKCS8EncodedKeySpec(privateKey));
			Cipher cipher = Cipher.getInstance("RSA");
			cipher.init(Cipher.DECRYPT_MODE, priKey);
			int inputLen = encryptedData.length;
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			int offSet = 0;
			byte[] cache;
			int i = 0;
			// 对数据分段解密
			while (inputLen - offSet > 0) {
				if (inputLen - offSet > MAX_DECRYPT_BLOCK) {
					cache = cipher.doFinal(encryptedData, offSet, MAX_DECRYPT_BLOCK);
				} else {
					cache = cipher.doFinal(encryptedData, offSet, inputLen - offSet);
				}
				out.write(cache, 0, cache.length);
				i++;
				offSet = i * MAX_DECRYPT_BLOCK;
			}

			byte[] decryptedData = out.toByteArray();
			return new String(decryptedData, StandardCharsets.UTF_8);
		} catch (Exception e) {
			throw new BizException(Code.ERROR);
		}
	}

	/**
	 * HmacMD5
	 * 
	 * @param data
	 * @param secret
	 * @return
	 */
	public static byte[] hmac(String data, byte[] secret) {
		try {
			SecretKey secretKey = new SecretKeySpec(secret, "HmacMD5");
			Mac mac = Mac.getInstance(secretKey.getAlgorithm());
			mac.init(secretKey);
			return mac.doFinal(data.getBytes(StandardCharsets.UTF_8));
		} catch (Exception e) {
			throw new BizException(Code.ERROR);
		}
	}

	/**
	 * HmacMD5
	 * 
	 * @param data
	 * @param secret
	 * @param toLowerCase
	 * @return
	 */
	public static String hmacHex(String data, byte[] secret, boolean toLowerCase) {
		return new String(Hex.encodeHex(hmac(data, secret), toLowerCase));
	}

	/**
	 * HmacMD5
	 * 
	 * @param data
	 * @param secret
	 * @return
	 */
	public static String hmacHex(String data, byte[] secret) {
		return new String(Hex.encodeHexString(hmac(data, secret)));
	}

	/**
	 * MD5
	 * 
	 * @param data
	 * @return
	 */
	public static byte[] md5(String data) {
		try {
			MessageDigest digest = MessageDigest.getInstance("MD5");
			return digest.digest(data.getBytes(StandardCharsets.UTF_8));
		} catch (Exception e) {
			throw new BizException(Code.ERROR);
		}
	}

	/**
	 * MD5
	 * 
	 * @param data
	 * @param toLowerCase
	 * @return
	 */
	public static String md5Hex(String data, boolean toLowerCase) {
		return new String(Hex.encodeHex(md5(data), toLowerCase));
	}

	/**
	 * MD5
	 * 
	 * @param data
	 * @return
	 */
	public static String md5Hex(String data) {
		return new String(Hex.encodeHexString(md5(data)));
	}

	/**
	 * SHA256
	 * 
	 * @param data
	 * @return
	 */
	public static byte[] sha256(String data) {
		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			return digest.digest(data.getBytes(StandardCharsets.UTF_8));
		} catch (Exception e) {
			throw new BizException(Code.ERROR);
		}
	}

	/**
	 * SHA256
	 * 
	 * @param data
	 * @param toLowerCase
	 * @return
	 */
	public static String sha256Hex(String data, boolean toLowerCase) {
		return new String(Hex.encodeHex(sha256(data), toLowerCase));
	}

	/**
	 * SHA256
	 * 
	 * @param data
	 * @return
	 */
	public static String sha256Hex(String data) {
		return new String(Hex.encodeHexString(sha256(data)));
	}

}
