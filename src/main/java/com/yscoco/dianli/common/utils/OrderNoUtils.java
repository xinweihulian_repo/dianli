package com.yscoco.dianli.common.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class OrderNoUtils {
	  public static final String DATE_KEY_STR = "yyMMddHHmmss";
	private static String time;
	private static int num = 1000;
	
	public synchronized static String getOrderNo(){
		 return getOrderNo("");
	}
	
	public synchronized static String getOrderNo(String type){
		 SimpleDateFormat df = new SimpleDateFormat(DATE_KEY_STR);
		 String nowTime = df.format(new Date());
		 if( time == null || !time.equals(nowTime)){
			 time = nowTime;
			 num = 1000;
		 }else{
			 num = num + 1;
		 }
		 return type+time+num;
	}
	
	public static void main(String[] args) {
		for (int i = 1 ; i< 2000 ; i++) {
			System.out.println(getOrderNo());
			
		}
	}
	
}
