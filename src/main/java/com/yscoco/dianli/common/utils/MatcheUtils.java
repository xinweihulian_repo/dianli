package com.yscoco.dianli.common.utils;

public class MatcheUtils {
	
	public static boolean isEmail(String str){
		return str.matches("[\\w-.]+@\\w+\\.\\w+");
	}
	
	public static boolean isMobile(String str){
		return str.matches("1\\d{10}");
	}
	
	public static boolean isUserName(String str){
		return str.matches("(?!([a-zA-Z]+|\\d+)$)[a-zA-Z\\d]{8,15}");
	}
	public static void main(String[] args) {
		System.out.println(MatcheUtils.isUserName("3333dab1"));
		
	}

}
