package com.yscoco.dianli.common.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class DateUtils {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/* System.out.println("当前时间："+ new Date().toLocaleString());
		 System.out.println("当天0点时间："+ getTimesmorning().toLocaleString());
		 System.out.println("当天24点时间："+ getTimesnight().toLocaleString());
		 System.out.println("本周周一0点时间："+ getTimesWeekmorning().toLocaleString());
		 System.out.println("本周周日24点时间："+ getTimesWeeknight().toLocaleString());
		 System.out.println("本月初0点时间："+ getTimesMonthmorning().toLocaleString());
		 System.out.println("本月未24点时间："+ getTimesMonthnight().toLocaleString());
		 String day = getAfterDay(getTimesWeekmorning(), -7).toLocaleString();
		 System.out.println(day);*/
		
		}

	    /**定义常量**/
	    public static final String DATE_JFP_STR="yyyyMM";
	    public static final String DATE_FULL_STR = "yyyy-MM-dd HH:mm:ss";
	    public static final String DATE_FULL_STR_II = "yyyy-MM-dd HH:mm:ss:sss";
	    public static final String DATE_YMDHM_STR = "yyyy-MM-dd HH:mm";
	    public static final String DATE_SMALL_STR = "yyyy-MM-dd";
	    public static final String DATE_KEY_STR = "yyMMddHHmmss";
	      
	    /**
	     * 使用预设格式提取字符串日期
	     * @param strDate 日期字符串
	     * @return
	     */
	    public static Date parse(String strDate) {
	        return parse(strDate,DATE_FULL_STR);
	    }
	      
	    /**
	     * 使用用户格式提取字符串日期
	     * @param strDate 日期字符串
	     * @param pattern 日期格式
	     * @return
	     */
	    public static Date parse(String strDate, String pattern) {
	        SimpleDateFormat df = new SimpleDateFormat(pattern);
	        try {
	            return df.parse(strDate);
	        } catch (ParseException e) {
	            e.printStackTrace();
	            return null;
	        }
	    }
	      
	    /**
	     * 两个时间比较
	     * @param date
	     * @return
	     */
	    public static int compareDateWithNow(Date date1){
	        Date date2 = new Date();
	        int rnum =date1.compareTo(date2);
	        return rnum;
	    }
	      
	    /**
	     * 两个时间比较(时间戳比较)
	     * @param date
	     * @return
	     */
	    public static int compareDateWithNow(long date1){
	        long date2 = dateToUnixTimestamp();
	        if(date1>date2){
	            return 1;
	        }else if(date1<date2){
	            return -1;
	        }else{
	            return 0;
	        }
	    }
	      
	  
	    /**
	     * 获取系统当前时间
	     * @return
	     */
	    public static String getNowTime() {
	        SimpleDateFormat df = new SimpleDateFormat(DATE_FULL_STR);
	        return df.format(new Date());
	    }
	    
	    /**
	     * 获取系统当前时间
	     * @return
	     */
	    public static String getNowTimeII() {
	        SimpleDateFormat df = new SimpleDateFormat(DATE_FULL_STR_II);
	        return df.format(new Date());
	    }
	      
	    /**
	     * 获取系统当前时间
	     * @return
	     */
	    public static String getNowTime(String type) {
	        SimpleDateFormat df = new SimpleDateFormat(type);
	        return df.format(new Date());
	    }
	    
	    /**
	     * 获取系统Date的当前时间
	     * @return
	     */
	    public static Date getNewTime() {
	        return new Date();
	    }
	      
	    /**
	     * 获取系统当前计费期
	     * @return
	     */
	    public static String getJFPTime() {
	        SimpleDateFormat df = new SimpleDateFormat(DATE_JFP_STR);
	        return df.format(new Date());
	    }
	      
	    /**
	     * 将指定的日期转换成Unix时间戳
	     * @param String date 需要转换的日期 yyyy-MM-dd HH:mm:ss
	     * @return long 时间戳
	     */
	    public static long dateToUnixTimestamp(String date) {
	        long timestamp = 0;
	        try {
	            timestamp = new SimpleDateFormat(DATE_FULL_STR).parse(date).getTime();
	        } catch (ParseException e) {
	            e.printStackTrace();
	        }
	        return timestamp;
	    }
	      
	    /**
	     * 将指定的日期转换成Unix时间戳   
	     * @param String date 需要转换的日期 yyyy-MM-dd    .parse(date).getTime()
	     * @return long 时间戳
	     */
	    public static long dateToUnixTimestamp(String date, String dateFormat) {
	        long timestamp = 0;
	        try {
	        	SimpleDateFormat  format = new SimpleDateFormat(dateFormat);
	            format.setTimeZone(TimeZone.getTimeZone("GMT+8"));
	        	timestamp = format.parse(date).getTime();
	        } catch (ParseException e) {
	            e.printStackTrace();
	        }
	        return timestamp;
	    }
	      
	    /**
	     * 将当前日期转换成Unix时间戳
	     * @return long 时间戳
	     */
	    public static long dateToUnixTimestamp() {
	        long timestamp = new Date().getTime();
	        return timestamp;
	    }
	      
	      
	    /**
	     * 将Unix时间戳转换成日期
	     * @param long timestamp 时间戳  
	     * @return String 日期字符串
	     */
	    public static String unixTimestampToDate(long timestamp) {
	        SimpleDateFormat sd = new SimpleDateFormat(DATE_FULL_STR_II);
	        sd.setTimeZone(TimeZone.getTimeZone("GMT+8"));
	        return sd.format(new Date(timestamp));
	    }
	    
	    public static String unixTimestampToDate_data(long timestamp) {
	        SimpleDateFormat sd = new SimpleDateFormat(DATE_FULL_STR);
	        sd.setTimeZone(TimeZone.getTimeZone("GMT+8"));
	        return sd.format(new Date(timestamp));
	    }
	    
	    /**
	     * 得到n天之后的日期
	     * @param days
	     * @return
	     */
	    public static String getAfterDayDate(int days) {
	        Calendar canlendar = Calendar.getInstance(); // java.util包
	        canlendar.add(Calendar.DATE, days); // 日期减 如果不够减会将月变动
	        Date date = canlendar.getTime();
	        
	        SimpleDateFormat sdfd = new SimpleDateFormat(DATE_SMALL_STR);
	        String dateStr = sdfd.format(date);
	        
	        return dateStr;
	    }
	    
	    
	    /**
	     * 得到n天之后的日期
	     * @param days
	     * @return
	     */
	    public static String getAfterMonthDate(int month) {
	        Calendar canlendar = Calendar.getInstance(); // java.util包
	        canlendar.add(Calendar.MONTH, month); // 日期减 如果不够减会将月变动
	        Date date = canlendar.getTime();
	        
	        SimpleDateFormat sdfd = new SimpleDateFormat("yyyy-MM");
	        String dateStr = sdfd.format(date);
	        
	        return dateStr;
	    }
	    
	    public static List<String> getdate(Integer page,Integer rows){
	    	List<String> ds = new ArrayList<>(rows);
	    	Calendar canlendar = Calendar.getInstance(); // java.util包
	    	canlendar.add(Calendar.DATE, -rows*(page -1)); 
	    	Date date = canlendar.getTime();
	    	SimpleDateFormat sdfd = new SimpleDateFormat(DATE_SMALL_STR);
    		ds.add(sdfd.format(date));
	    	for (int i = 1 ; i<rows; i++) {
	    		canlendar.add(Calendar.DATE, -1); // 日期减 如果不够减会将月变动
	    		 date = canlendar.getTime();
	    		 ds.add(sdfd.format(date));
			}
	    	return ds;
	    }
	    /**
	     * @return
	     */
	    public static String format(Date date , String type) {
	        SimpleDateFormat df = new SimpleDateFormat(type);
	        return df.format(date);
	    }
	    /**
	     * @return
	     */
	    public static Date formatToDate(Date date , String type) {
	        SimpleDateFormat df = new SimpleDateFormat(type);
	        String s = df.format(date);
	        try {
				return df.parse(s);
			} catch (ParseException e) {
				Log.getCommon().error("",e);
			}
	        return null;
	    }
		public static Date getAfterDay(Date now, int days) {
			Calendar canlendar = Calendar.getInstance(); // java.util包
			canlendar.setTime(now);
	        canlendar.add(Calendar.DATE, days); // 日期减 如果不够减会将月变动
	        Date date = canlendar.getTime();
	        
	        return date;
		}
		public static Date addHour(Date now, int hour) {
			Calendar canlendar = Calendar.getInstance(); // java.util包
			canlendar.setTime(now);
	        canlendar.add(Calendar.HOUR_OF_DAY, hour); // 
	        Date date = canlendar.getTime();
	        return date;
		}
		 /**
		  * 当天开始时间 00:00:00:000
		  * @return
		  */
		 public static String getPlanStart() { 
		        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd 00:00:00:000");
		        return df.format(new Date());
		  }
		 /**
		  * 当前结束时间 23:59:59:000
		  * @return
		  */
		 public static String getPlanEnd()  {
			  	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd 23:59:59:000");
			  	return df.format(new Date());
		  }
		 
		 public static String get1970() { 
		        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd 00:00:00:000");
		        Date date = new Date(0l);
		        return df.format(date);
		  }
		 //获取本月第一天开始时间
		 public static String getFirstDayOfTheMonth() { 
			  SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd 00:00:00:000");
			  Date timesMonthmorning = getTimesMonthmorning();
		        return df.format(timesMonthmorning);
		  }
		 
		 
		// lingxiang   Java获取当天的起始时间和结束时间  ：2017-06-12 00:00:00 和  2017-06-12 17:38:49  
		public static String getBeginDate(Date current) {
			SimpleDateFormat df = new SimpleDateFormat(DATE_FULL_STR);
	   		Calendar c = Calendar.getInstance();
	   		c.setTime(current);
	   		// 设置时分秒为0
	   		c.set(Calendar.HOUR_OF_DAY, 0);
	   		c.set(Calendar.MINUTE, 0);
	   		c.set(Calendar.SECOND, 0);
	   		return df.format(c.getTime());
	   	}
		public static String getEndDate(Date current) {
			SimpleDateFormat df = new SimpleDateFormat(DATE_FULL_STR);
			Calendar c = Calendar.getInstance();
			c.setTime(current);
			// 设置时分秒为0
			c.set(Calendar.HOUR_OF_DAY, 0);
			c.set(Calendar.MINUTE, 0);
			c.set(Calendar.SECOND, 0);
			// 把天数加1,再把秒数减1
			c.add(Calendar.DATE, 1);
			c.add(Calendar.SECOND, -1);
			return df.format(c.getTime());
	   	}
		 
		public static long  getTimes(Date createTime) throws Exception {   // 两个时间相隔多少分钟
			 SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			 Date now = df.parse(getNowTime());
		 	 Date date = df.parse(createTime.toString());
		 	 long l=now.getTime()-date.getTime();
		 	 long min=((l/(60*1000)));
			 return min;
		}
		 
		// -Java获取当天、本周、本月 开始及结束时间 
		// 获得当天0点时间
		public static Date getTimesmorning() {
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.MILLISECOND, 0);
			return cal.getTime();
		}


		// 获得当天24点时间
		public static Date getTimesnight() {
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.HOUR_OF_DAY, 24);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.MILLISECOND, 0);
			return  cal.getTime();
		}


		// 获得本周一0点时间
		public static Date getTimesWeekmorning() {
			Calendar cal = Calendar.getInstance();
			cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONDAY), cal.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
			cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
			return  cal.getTime();
		}


		// 获得本周日24点时间
		public  static Date getTimesWeeknight() {
			Calendar cal = Calendar.getInstance();
			cal.setTime(getTimesWeekmorning());
			cal.add(Calendar.DAY_OF_WEEK, 7);
			return cal.getTime();
		}


		// 获得本月第一天0点时间
		public static Date getTimesMonthmorning() {
			Calendar cal = Calendar.getInstance();
			cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONDAY), cal.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
			cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
			return  cal.getTime();
		}


		// 获得本月最后一天24点时间
		public static Date getTimesMonthnight() {
			Calendar cal = Calendar.getInstance();
			cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONDAY), cal.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
			cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
			cal.set(Calendar.HOUR_OF_DAY, 24);
			return cal.getTime();
		}
	 
		 
	   
}