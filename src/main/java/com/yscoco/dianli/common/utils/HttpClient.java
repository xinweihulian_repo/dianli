package com.yscoco.dianli.common.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

@SuppressWarnings("deprecation")
public class HttpClient {
	


	public String post(String url, Map<String, String> params) {
		 DefaultHttpClient client = new DefaultHttpClient();
		HttpPost post = httpPostForm(url, params);
		return invoke(client, post);  
	}
	
	public String post(String url, String body,String charset) {
		 DefaultHttpClient client = new DefaultHttpClient();
		HttpPost post = httpPostForm(url, body,charset);
		return invoke(client, post);  
	}

	public static String get(String url , Map<String,String> params){
		 DefaultHttpClient client = new DefaultHttpClient();
		HttpGet get = httpGetParams(url, params);
		return invoke(client, get);
	} 
	
	public String down(String url , String basepath) {
		DefaultHttpClient client = new DefaultHttpClient();
		HttpGet get = httpGetParams(url, null);
		
		HttpResponse response = sendRequest(client, get);
		 HttpEntity entity = response.getEntity();
         
         String[] us =url.split("/");
         String filename =  us[us.length-1];
         Calendar rightNow = Calendar.getInstance();
         int hour = rightNow.get(Calendar.HOUR_OF_DAY);
         filename =  filename.split("_")[0]+"_" + UUID.randomUUID().toString() +"."+ filename.split("\\.")[1];
         String path =basepath+ File.separator + hour +File.separator + filename;
         File file = new File(path);
         if(file.exists()){
        	 Log.getCommon().error("img_exists > " + url);
        	 return url; 
         }
         if(!file.getParentFile().exists())
        	 file.getParentFile().mkdirs();
         
         InputStream is = null;
         FileOutputStream fileout = null;
		try {
			is = entity.getContent();
			fileout = new FileOutputStream(file);  
	         /** 
	          * 根据实际运行效果 设置缓冲区大小 
	          */  
	         
	         byte[] buffer=new byte[1024 * 1024];  
	         int ch = 0;  
	         while ((ch = is.read(buffer)) != -1) {  
	             fileout.write(buffer,0,ch);  
	         }  
	         
		} catch (IllegalStateException | IOException e) {
			Log.getCommon().error("file exception",e);
		}finally{
			try {
				if(is!=null){
					is.close();  
				}
				if(fileout != null){
					 fileout.flush();  
			         fileout.close(); 
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}
		
			
	        
		}
         
         return path;
	}
	
	private static HttpGet httpGetParams(String url,Map<String,String> params){
		String aurl = url;
		if (params != null) {
			Set<String> keySet = params.keySet();
			
			for (String key : keySet) {
				if (params.get(key) != null) {
					if(!aurl.contains("?")){
						aurl+="?";
					}else{
						aurl+="&";
					}
					aurl+=key+"="+params.get(key).toString();
				}
			}
		}

		HttpGet get = new HttpGet(aurl);
		return get;
	}
	private HttpPost httpPostForm(String url, Map<String, String> params) {

		HttpPost httpost = new HttpPost(url);
		List<NameValuePair> nvps = new ArrayList<NameValuePair>();
		if (params != null) {
			Set<String> keySet = params.keySet();
			for (String key : keySet) {
				if (params.get(key) != null) {
					nvps.add(new BasicNameValuePair(key, params.get(key).toString()));
				}
			}
		}
		try {
			httpost.setEntity(new UrlEncodedFormEntity(nvps, HTTP.UTF_8));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return httpost;
	}
	
	private HttpPost httpPostForm(String url, String body,String charset) {

		HttpPost httpost = new HttpPost(url);
		httpost.setEntity(new StringEntity(body, Charset.forName(charset)));
		return httpost;
	}

	private static String invoke(DefaultHttpClient httpclient,HttpUriRequest httpost) {
		HttpResponse response = sendRequest(httpclient, httpost);
		String body = paseResponse(response);
		return body;
	}

	private static HttpResponse sendRequest(DefaultHttpClient httpclient,
			HttpUriRequest httpost) {
		HttpResponse response = null;
		try {
			response = httpclient.execute(httpost);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return response;
	}

	private static String paseResponse(HttpResponse response) {
		HttpEntity entity = response.getEntity();
		String body = "";
		try {
			body = EntityUtils.toString(entity, "UTF-8");
		} catch (ParseException | IOException e1) {
			Log.getCommon().error("请求解析反馈值失败",e1);
		}
	
		/*try {
			InputStream in = entity.getContent();
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String temp = "";
			while ((temp = br.readLine()) != null) {
				body += temp;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}*/
		return body;
	}
}
