package com.yscoco.dianli.common.utils;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class JsonStringSerialize extends JsonSerializer<String>{

	@Override
	public void serialize(String str, JsonGenerator gen,
			SerializerProvider arg2) throws IOException,
			JsonProcessingException {
		Object r = null;
			if(StringUtils.isNotEmpty(str)){
				if(str.startsWith("{") && str.endsWith("}")){
					r = JSONObject.parse(str);
				}
				else if(str.startsWith("[") && str.endsWith("]")){
					r = JSONObject.parse(str);
					
				}else{
					r=str;
				}
			}
	      
			gen.writeObject(r);
		
	}

}
