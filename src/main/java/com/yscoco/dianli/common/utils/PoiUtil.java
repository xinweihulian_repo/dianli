package com.yscoco.dianli.common.utils;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.util.CellRangeAddress;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.batise.device.temper.Temper_data;
import com.batise.device.voltage.Voltage_data;
import com.yscoco.dianli.common.exception.BizException;
import com.yscoco.dianli.common.form.PointExportForm;
import com.yscoco.dianli.common.vo.Code;
import com.yscoco.dianli.constants.ProtectorType;

@SuppressWarnings("deprecation")
public final class PoiUtil {
	
	private static Logger logger = Logger.getLogger(PoiUtil.class);

	/**
	 * 设置请求头和下载到浏览器的文件名称
	 * 
	 * @param response
	 * @param fileName
	 *            文件名
	 */
	public static void setResponseHeader(HttpServletResponse response, String fileName) {
		String sdf = new SimpleDateFormat("yyyyMMddhhss").format(System.currentTimeMillis());
		StringBuffer sb = new StringBuffer(fileName);
		String name = sb.append(sdf).append(".xls").toString();
		try {
			fileName = new String(name.getBytes(), "ISO8859-1");
			response.setContentType("application/octet-stream;charset=ISO8859-1");
			response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
			response.addHeader("Pargam", "no-cache");
			response.addHeader("Cache-Control", "no-cache");
		} catch (Exception ex) {
			throw new BizException(Code.SYS_ERR);
		}
	}

	/**
	 * 导出excel
	 * 
	 * @param sheetTitle
	 *            标题
	 * @param titles
	 *            标题头
	 * @param keys
	 *            实体类
	 * @param data
	 * @throws IOException
	 * @throws IllegalAccessException 
	 * @throws InvocationTargetException 
	 */
	@SuppressWarnings({ "unchecked", "deprecation" })
	public static void exportExcel(String sheetTitle, String sheetTitle2, String sheetTitle3, String sheetTitle4,
			Map<String, Object> data, HttpServletResponse respons,HttpServletRequest request) throws IOException, InvocationTargetException, IllegalAccessException {
		setResponseHeader(respons, "云电微视");
		OutputStream out = respons.getOutputStream();
		List<Map<String, Object>> maxValueEric = (List<Map<String, Object>>) data.get("maxValueEric");
		List<Map<String, Object>> maxValueVoltage = (List<Map<String, Object>>) data.get("maxValueVoltage");
		List<Map<String, Object>> maxValueTemper = (List<Map<String, Object>>) data.get("maxValueTemper");
		List<Map<String, Object>> allCountByFaultType = (List<Map<String, Object>>) data.get("allCountByFaultType");
		// 创建excel
		HSSFWorkbook wb = new HSSFWorkbook();
		// 创建sheet
		HSSFSheet sheet = wb.createSheet(sheetTitle);
		HSSFSheet sheet2 = wb.createSheet(sheetTitle2);
		HSSFSheet sheet3 = wb.createSheet(sheetTitle3);
		HSSFSheet sheet4 = wb.createSheet(sheetTitle4);
		//母线工作薄
		for(int i=0;i<=9;i++){sheet.setColumnWidth(i, 3766);} //第一个参数代表列id(从0开始),第2个参数代表宽度值 
		//过电压保护器工作薄
		for(int i=0;i<=7;i++){sheet2.setColumnWidth(i, 3766);} //第一个参数代表列id(从0开始),第2个参数代表宽度值 
		//温度采集器工作薄
		for(int i=0;i<=7;i++){sheet3.setColumnWidth(i, 3766);} //第一个参数代表列id(从0开始),第2个参数代表宽度值 
		 //报警列表工作薄
		for(int i=0;i<=12;i++){sheet4.setColumnWidth(i, 3766);} //第一个参数代表列id(从0开始),第2个参数代表宽度值
		
		// 设置字体
        HSSFFont styleTitles = wb.createFont();
       // styleTitles.setFontHeightInPoints((short) 20); //字体高度
        styleTitles.setColor(HSSFFont.COLOR_NORMAL); //字体颜色
        styleTitles.setFontName("黑体"); //字体
       // styleTitles.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD); //宽度
        styleTitles.setItalic(false); //是否使用斜体
		
		 // 设置单元格类型
        HSSFCellStyle cellStyle = wb.createCellStyle();
        cellStyle.setFont(styleTitles);
        cellStyle.setFillForegroundColor((short) 13);// 设置背景色
       // cellStyle.setAlignment(HSSFCellStyle); //水平布局：居中
        cellStyle.setWrapText(true);
       // temperFlag   masterFlag  voltageFlag
        if(request.getParameter("masterFlag").equalsIgnoreCase("y")){
        	getEricInfo(maxValueEric, sheet, cellStyle);
        }
        if(request.getParameter("temperFlag").equalsIgnoreCase("y")){
        	getTemperInfo(maxValueTemper, sheet3, cellStyle);
        }
        if(request.getParameter("voltageFlag").equalsIgnoreCase("y")){
        	 getVoltageInfo(maxValueVoltage,sheet2,cellStyle);
        }
		getAlarmInfo(allCountByFaultType, sheet4, cellStyle);
		wb.write(out);
		out.flush();
		out.close();
	}

	// **********************************************************************************************************************
	private static void getVoltageInfo(List<Map<String, Object>> maxValueVoltage, HSSFSheet sheet2,
			HSSFCellStyle styleTitle) {
		String[] voltageTitles = { "监测设备名称","类型", "相别", "温度（℃）", "湿度（%RH）", "泄漏电流（μA)", "动作次数", "动作时长" };
		String[]  voltagekeysA = { "pointName","type","相别", "temper_a", "humidity_a", "electric","temp1","temp2"};
		String[]  voltagekeysB = { "pointName","type","相别", "temper_b", "humidity_b", "electric","temp1","temp2"};
		String[]  voltagekeysC = { "pointName","type","相别", "temper_c", "humidity_c", "electric","temp1","temp2"};
		List<Map<String, Object>> newVoltage = new ArrayList<>();
     
		for (Map<String, Object> map : maxValueVoltage) {
			Map<String, Object> groupMap = new HashMap<>();
			Map<String, Object> VoltageA = new HashMap<>();
			Map<String, Object> VoltageB = new HashMap<>();
			Map<String, Object> VoltageC = new HashMap<>();
			
			VoltageA.put("pointName", map.get("pointName"));
			VoltageA.put("type", map.get("type"));
			VoltageA.put("相别","A相");    
			VoltageA.put("temper_a", (Double.valueOf(map.get("temper_a").toString())));    
			VoltageA.put("humidity_a", map.get("humidity_a"));
			VoltageA.put("electric", map.get("electric"));
			
			VoltageB.put("pointName", map.get("pointName"));
			VoltageB.put("type", map.get("type"));
			VoltageB.put("相别","B相");    
			VoltageB.put("temper_b", (Double.valueOf(map.get("temper_b").toString())));    
			VoltageB.put("humidity_b", map.get("humidity_b"));
			VoltageB.put("electric", map.get("electric")); 
			
			
			VoltageC.put("pointName", map.get("pointName"));
			VoltageC.put("type", map.get("type"));
			VoltageC.put("相别","C相");    
			VoltageC.put("temper_c", (Double.valueOf(map.get("temper_c").toString())));    
			VoltageC.put("humidity_c", map.get("humidity_c"));
			VoltageC.put("electric", map.get("electric")); 
			
			groupMap.put("VoltageA",VoltageA);
			groupMap.put("VoltageB",VoltageB);
			groupMap.put("VoltageC",VoltageC);
			
			newVoltage.add(groupMap);	
		}
		HSSFRow row; //创建行
		HSSFCell cell;//创建列
		// 创建标题栏
		row = sheet2.createRow(0);
		// 标题栏
		for (int i = 0; i < voltageTitles.length; i++) {
			String ericTitle = voltageTitles[i];
			// 在行上创建1列
			cell = row.createCell(i);
			cell.setCellValue(ericTitle);
			cell.setCellStyle(styleTitle);
			
		}
			// 添加数据
			int index = 1;
			try {
			for (Map<String, Object> map : newVoltage) {
				CellRangeAddress     cra = new CellRangeAddress(index, index+2, 0, 0); 
				CellRangeAddress     cra1 = new CellRangeAddress(index, index+2, 1, 1);
			//	CellRangeAddress     cra2 = new CellRangeAddress(index, index+2, 4, 4);
				CellRangeAddress	 cra3 = new CellRangeAddress(index, index+2, 5, 5);
				CellRangeAddress	 cra4 = new CellRangeAddress(index, index+2, 6, 6);
				CellRangeAddress	 cra5 = new CellRangeAddress(index, index+2, 7, 7);
				
				// 在sheet里增加合并单元格
				sheet2.addMergedRegion(cra);
				sheet2.addMergedRegion(cra1);
			//	sheet2.addMergedRegion(cra2);
				sheet2.addMergedRegion(cra3);
				sheet2.addMergedRegion(cra4);
				sheet2.addMergedRegion(cra5);
				
				Map<String,Object> objectA = (Map<String,Object>)map.get("VoltageA");
				Map<String,Object> objectB = (Map<String,Object>)map.get("VoltageB");
				Map<String,Object> objectC = (Map<String,Object>)map.get("VoltageC");
				
				row = sheet2.createRow(index++);
				for (int i = 0; i < voltagekeysA.length; i++) {
					String key = voltagekeysA[i];
					// 在行上创建1列
					cell = row.createCell(i);
					if (objectA.get(key) == null) {
						cell.setCellValue("-");
					} else {
						cell.setCellValue(objectA.get(key).toString());
					}
					cell.setCellStyle(styleTitle);
				}
				
				row = sheet2.createRow(index++);
				for (int i = 0; i < voltagekeysB.length; i++) {
					String key = voltagekeysB[i];
					// 在行上创建1列
					cell = row.createCell(i);
					if (objectB.get(key) == null) {
						cell.setCellValue("-");
					} else {
						cell.setCellValue(objectB.get(key).toString());
					}
					cell.setCellStyle(styleTitle);
				}
				
				row = sheet2.createRow(index++);
				for (int i = 0; i < voltagekeysC.length; i++) {
					String key = voltagekeysC[i];
					// 在行上创建1列
					cell = row.createCell(i);
					if (objectC.get(key) == null) {
						cell.setCellValue("-");
					} else {
						cell.setCellValue(objectC.get(key).toString());
					}
					cell.setCellStyle(styleTitle);
				}
				index++;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@SuppressWarnings("deprecation")
	private static void getEricInfo(List<Map<String, Object>> maxValueEric, HSSFSheet sheet, HSSFCellStyle styleTitle) throws InvocationTargetException, IllegalAccessException {
//		String[] erickeys = { "masterName","A相", "Power_Apparent_a", "voltage_a", "Power_Apparent_c", "voltage_b",
//				"voltage_c", "Power_Apparent_a", "Electric_a", "Power_Use_b", "Power_Use_c", "Power_Nouse_b",
//				"Power_Nouse_a", "Power_Factor_b", "Power_Factor_c", "Power_Nouse_c", "Electric_c", "Electric_b",
//				"Power_Use_a", "Power_Factor_a" };
		String[] ericTitles = { "母线名称", "相别", "电压", "电流", "功率因数", "视在功率", "有功功率", "无功功率", "电压总畸变率", "电流总畸变率" };
		String[] erickeysA = { "masterName","相别", "voltage_a", "Electric_a", "Power_Factor_a", "Power_Apparent_a","Power_Use_a","Power_Nouse_a","Voltage_Distorted ","Eric_Distorted"};
		String[] erickeysB = { "masterName","相别", "voltage_b", "Electric_b", "Power_Factor_b", "Power_Apparent_b","Power_Use_b","Power_Nouse_b","Voltage_Distorted ","Eric_Distorted"};
		String[] erickeysC = { "masterName","相别", "voltage_c", "Electric_c", "Power_Factor_c", "Power_Apparent_c","Power_Use_c","Power_Nouse_c","Voltage_Distorted ","Eric_Distorted"};
		List<Map<String, Object>> newMaxValueEric = new ArrayList<>();
    
		for (Map<String, Object> map : maxValueEric) {
			Map<String, Object> groupMap = new HashMap<>();
			Map<String, Object> maxValueEricA = new HashMap<>();
			Map<String, Object> maxValueEricB = new HashMap<>();
			Map<String, Object> maxValueEricC = new HashMap<>();
			
			maxValueEricA.put("masterName", map.get("masterName"));
			maxValueEricA.put("相别","A相");    
			maxValueEricA.put("voltage_a", map.get("voltage_a"));    
			maxValueEricA.put("Electric_a", map.get("Electric_a"));
			maxValueEricA.put("Power_Factor_a", map.get("Power_Factor_a")); 
			maxValueEricA.put("Power_Apparent_a", map.get("Power_Apparent_a"));    
			maxValueEricA.put("Power_Use_a", map.get("Power_Use_a"));
			maxValueEricA.put("Power_Nouse_a", map.get("Power_Nouse_a")); 

			maxValueEricB.put("masterName", map.get("masterName"));
			maxValueEricB.put("相别", "B相");    
			maxValueEricB.put("voltage_b", map.get("voltage_b"));    
			maxValueEricB.put("Electric_b", map.get("Electric_b"));
			maxValueEricB.put("Power_Factor_b", map.get("Power_Factor_b")); 
			maxValueEricB.put("Power_Apparent_b", map.get("Power_Apparent_b"));    
			maxValueEricB.put("Power_Use_b", map.get("Power_Use_b"));
			maxValueEricB.put("Power_Nouse_b", map.get("Power_Nouse_b")); 
			
			maxValueEricC.put("masterName", map.get("masterName"));
			maxValueEricC.put("相别", "C相");    
			maxValueEricC.put("voltage_c", map.get("voltage_c"));    
			maxValueEricC.put("Electric_c", map.get("Electric_c"));
			maxValueEricC.put("Power_Factor_c", map.get("Power_Factor_c")); 
			maxValueEricC.put("Power_Apparent_c", map.get("Power_Apparent_c"));    
			maxValueEricC.put("Power_Use_c", map.get("Power_Use_c"));
			maxValueEricC.put("Power_Nouse_c", map.get("Power_Nouse_c")); 
			
			groupMap.put("maxValueEricA",maxValueEricA);
			groupMap.put("maxValueEricB",maxValueEricB);
			groupMap.put("maxValueEricC",maxValueEricC);
			
			newMaxValueEric.add(groupMap);	
		}
//    System.out.println("newMaxValueEric:"+newMaxValueEric);
//       System.out.println("*********************************************");
//       newMaxValueEric.forEach(System.out::println);
		//母线工作薄设置列宽度
		for(int i=0;i<=ericTitles.length;i++){sheet.setColumnWidth(i, 3766);} //第一个参数代表列id(从0开始),第2个参数代表宽度值 
		HSSFRow row;
		HSSFCell cell;
		// 创建标题栏
		row = sheet.createRow(0);
		// 标题栏
		for (int i = 0; i < ericTitles.length; i++) {
			String ericTitle = ericTitles[i];
			// 在行上创建1列
			cell = row.createCell(i);
			cell.setCellValue(ericTitle);
			cell.setCellStyle(styleTitle);
		}
		// 添加数据
		int index = 1;
		try {
		for (Map<String, Object> map : newMaxValueEric) {
		//	System.out.println("index:"+index);
			CellRangeAddress     cra = new CellRangeAddress(index, index+2, 0, 0); 
			CellRangeAddress	 cra1 = new CellRangeAddress(index, index+2, 8, 8);
			CellRangeAddress	 cra2 = new CellRangeAddress(index, index+2, 9, 9);
			
			// 在sheet里增加合并单元格
			sheet.addMergedRegion(cra);
			sheet.addMergedRegion(cra1);
			sheet.addMergedRegion(cra2);
			
			Map<String,Object> objectA = (Map<String,Object>)map.get("maxValueEricA");
			Map<String,Object> objectB = (Map<String,Object>)map.get("maxValueEricB");
			Map<String,Object> objectC = (Map<String,Object>)map.get("maxValueEricC");
			
			row = sheet.createRow(index++);
			for (int i = 0; i < erickeysA.length; i++) {
				String key = erickeysA[i];
				// 在行上创建1列
				cell = row.createCell(i);
				if (objectA.get(key) == null) {
					cell.setCellValue("-");
				} else {
					cell.setCellValue(objectA.get(key).toString());
				}
				cell.setCellStyle(styleTitle);
			}
			
			row = sheet.createRow(index++);
			for (int i = 0; i < erickeysB.length; i++) {
				String key = erickeysB[i];
				// 在行上创建1列
				cell = row.createCell(i);
				if (objectB.get(key) == null) {
					cell.setCellValue("-");
				} else {
					cell.setCellValue(objectB.get(key).toString());
				}
				cell.setCellStyle(styleTitle);
			}
			
			row = sheet.createRow(index++);
			for (int i = 0; i < erickeysC.length; i++) {
				String key = erickeysC[i];
				// 在行上创建1列
				cell = row.createCell(i);
				if (objectC.get(key) == null) {
					cell.setCellValue("-");
				} else {
					cell.setCellValue(objectC.get(key).toString());
				}
				cell.setCellStyle(styleTitle);
			}
			index++; //++ 操作使数据间隔一行
		}
	} catch (Exception e) {
		e.printStackTrace();
	}
}

	private static void getTemperInfo(List<Map<String, Object>> maxValueTemper, HSSFSheet sheet3,
			HSSFCellStyle styleTitle) {
		String[] temperTitles = { "监测设备名称", "类型", "相别", "温度（℃）", "相别", "温度（℃）", "相别", "温度（℃）" };
		String[] temperKeys = { "positionName", "type", "A相", "temper_a", "B相", "temper_b", "C相", "temper_c" };
		// Set<Map<String, Object>> newMaxValueTemper =
		// convertDataFormat(maxValueTemper);
		Set<Map<String, Object>> newMaxValueTemper = new HashSet<>();
		for (Map<String, Object> temper : maxValueTemper) {
			Map<String, Object> newMap = new HashMap<>();
			String positionName = temper.get("positionName").toString(); // 获取到监测设备的详细名称
			String exceptPhaseName = positionName.substring(0, positionName.length() - 2); // 除去相位后的名称
			String phasePosition = positionName.substring(positionName.length() - 2); // 获取到设备所在相位
			for (Map<String, Object> compare : maxValueTemper) {
				String cPositionName = compare.get("positionName").toString(); // 获取到监测设备的详细名称
				String cExceptPhaseName = cPositionName.substring(0, cPositionName.length() - 2); // 除去相位后的名称
				String cPhasePosition = cPositionName.substring(cPositionName.length() - 2); // 获取到设备所在相位
				newMap.put("A相", "A相");
				newMap.put("B相", "B相");
				newMap.put("C相", "C相");
				if (exceptPhaseName.equals(cExceptPhaseName)) {
					newMap.put("positionName", cExceptPhaseName);
					newMap.put("type", compare.get("type"));
					Double result = Double.valueOf(compare.get("temper").toString());
					switch (cPhasePosition) {
					case "A相":
						newMap.put("temper_a", result);
						break;
					case "B相":
						newMap.put("temper_b", result);
						break;
					case "C相":
						newMap.put("temper_c", result);
						break;
					default:
						throw new BizException(Code.ERR_OTHER);
					}
				}
			}
			newMaxValueTemper.add(newMap);
		}
	//	System.out.println("newMaxValueTemper:" + newMaxValueTemper);
		HSSFRow row;
		HSSFCell cell;
		// 创建标题栏
		row = sheet3.createRow(0);
		// 标题栏
		for (int i = 0; i < temperTitles.length; i++) {
			String ericTitle = temperTitles[i];
			// 在行上创建1列
			cell = row.createCell(i);
			cell.setCellValue(ericTitle);
			cell.setCellStyle(styleTitle);
		}
		// 添加数据
		int index = 1;
		try {
			for (Map<String, Object> map : newMaxValueTemper) {
				row = sheet3.createRow(index);
				for (int i = 0; i < temperKeys.length; i++) {
					String key = temperKeys[i];
					// 在行上创建1列
					cell = row.createCell(i);
					if (map.get(key) == null) {
						cell.setCellValue("-");
					} else {
						cell.setCellValue(map.get(key).toString());
					}
					cell.setCellStyle(styleTitle);
				//	System.out.println(map.get(key));
				}
				index++;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static Set<Map<String, Object>> convertDataFormat(List<Map<String, Object>> maxValueTemper) {
		if (maxValueTemper == null) {
			return null;
		}
		Set<Map<String, Object>> newMaxValueTemper = new HashSet<>();
		for (Map<String, Object> temper : maxValueTemper) {
			Map<String, Object> newMap = new HashMap<>();
			newMap.put("cabin_no",temper.get("cabin_no").toString() );
			newMap.put("position",temper.get("position").toString() );
			newMap.put("branchId",temper.get("branchId").toString() );
			String positionName = temper.get("positionName").toString(); // 获取到监测设备的详细名称
			String exceptPhaseName = positionName.substring(0, positionName.length() - 2); // 除去相位后的名称
			String phasePosition = positionName.substring(positionName.length() - 2); // 获取到设备所在相位
			//System.out.println("@@*@@*@@**:"+JSON.toJSON(maxValueTemper));
			for (Map<String, Object> compare : maxValueTemper) {
				String cPositionName = compare.get("positionName").toString(); // 获取到监测设备的详细名称
				String cExceptPhaseName = cPositionName.substring(0, cPositionName.length() - 2); // 除去相位后的名称
				String cPhasePosition = cPositionName.substring(cPositionName.length() - 2); // 获取到设备所在相位
				newMap.put("A相", "A相");
				newMap.put("B相", "B相");
				newMap.put("C相", "C相");
				if (exceptPhaseName.equals(cExceptPhaseName)) {
					newMap.put("positionName", cExceptPhaseName);
					newMap.put("type", compare.get("type"));
					Double result = Double.valueOf(compare.get("temper").toString());
					switch (cPhasePosition) {
					case "A相":
						newMap.put("temper_a", result);
						break;
					case "B相":
						newMap.put("temper_b", result);
						break;
					case "C相":
						newMap.put("temper_c", result);
						break;
					default:
						throw new BizException(Code.ERR_OTHER);
					}
				}
			}
			newMaxValueTemper.add(newMap);
		}
		return newMaxValueTemper;
	}

	private static void getAlarmInfo(List<Map<String, Object>> allCountByFaultType, HSSFSheet sheet4,
			HSSFCellStyle styleTitle) {
		String[] alarmTitles = { "弧光接地", "金属接地", "系统过电压", "欠压", "泄漏", "PT断线", "过热", "湿度超标", "过电压故障", "电池电量不足",
				"系统短路", "异常", "报警总数" };
		String[] alarmKeys = { "16", "10", "11", "14", "22", "13", "20", "21", "12", "23", "15", "a", "sumCount" };
		Integer sumCount = 0;
		List<Map<String, Object>> allCountByFaultTypeNewest = new ArrayList<>();
		for (Map<String, Object> map : allCountByFaultType) {
			HashMap<String, Object> maps = new HashMap<>();
			int cnt = Integer.valueOf(map.get("cnt").toString());
			String type = map.get("fault_type").toString();
			maps.put(type, cnt);
			allCountByFaultTypeNewest.add(maps);
			sumCount += Integer.valueOf(map.get("cnt").toString());
		}
		Map<String, Object> countMap = new HashMap<>();
		countMap.put("sumCount", sumCount);
		HSSFRow row;
		HSSFCell cell;
		// 创建标题栏
		row = sheet4.createRow(0);
		// 标题栏
		for (int i = 0; i < alarmTitles.length; i++) {
			String alarmTitle = alarmTitles[i];
			// 在行上创建1列
			cell = row.createCell(i);
			cell.setCellValue(alarmTitle);
			cell.setCellStyle(styleTitle);
		}

		// 添加数据
		int index = 1;
		row = sheet4.createRow(index);
		for (int i = 0; i < alarmKeys.length; i++) {
			for (Map<String, Object> map : allCountByFaultTypeNewest) {
				String key = alarmKeys[i];
				for (Object o : map.keySet()) {
					if (o.toString().equals(key)) {
						// 在行上创建1列
						cell = row.createCell(i);
						if (map.get(key) == null) {
							cell.setCellValue(0);
						} else {
							cell.setCellValue(Double.valueOf(map.get(key).toString()));
						}
						//System.out.println(map.get(key));
						cell.setCellStyle(styleTitle);
					}
				}
			}
			// index++;
		}
		cell = row.createCell(alarmKeys.length - 1);
		cell.setCellValue(Integer.valueOf(countMap.get("sumCount").toString()));

	}

	// **********************************************************************************************************************

	/**
	 * 传入对应的参数 直接导出列表
	 * 
	 * @param respons
	 * @param memberList
	 *            结果集
	 * @param titles
	 *            标题头
	 * @param keys
	 *            实体类
	 */
	public static void printMethod(HttpServletResponse respons, List<Map<String, String>> memberList, String[] titles,
			String[] keys) {
		try {
			OutputStream out = respons.getOutputStream();
			respons.setContentType("application/vnd.ms-excel");
			respons.setHeader("Content-disposition", "attachment;filename=" + System.currentTimeMillis() + ".xls");
			// PoiUtil.exportExcel(out, "用户分析列表", titles, keys, memberList);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static List<JSONObject> inputExcel(InputStream is, Integer startRow, String[] fields) throws IOException {
		HSSFWorkbook hssfWorkbook = new HSSFWorkbook(is);
		startRow = startRow == null ? 0 : startRow;

		List<JSONObject> list = new ArrayList<JSONObject>();
		// 循环工作表Sheet
		for (int numSheet = 0; numSheet < hssfWorkbook.getNumberOfSheets(); numSheet++) {
			HSSFSheet hssfSheet = hssfWorkbook.getSheetAt(numSheet);
			if (hssfSheet == null) {
				continue;
			}
			// 循环行Row
			JSONObject json = null;
			for (int rowNum = 1; rowNum <= hssfSheet.getLastRowNum(); rowNum++) {
				HSSFRow hssfRow = hssfSheet.getRow(rowNum);
				json = new JSONObject();
				if (hssfRow != null) {
					for (int i = 0; i < fields.length && i < hssfRow.getLastCellNum(); i++) {
						HSSFCell cell = hssfRow.getCell(i);
						json.put(fields[i], getValue(cell));
					}
				}
				list.add(json);
			}
		}
		return list;
	}

	@SuppressWarnings("static-access")
	private static String getValue(HSSFCell hssfCell) {
		if (hssfCell.getCellType() == hssfCell.CELL_TYPE_BOOLEAN) {
			// 返回布尔类型的值
			return String.valueOf(hssfCell.getBooleanCellValue());
		} else if (hssfCell.getCellType() == hssfCell.CELL_TYPE_NUMERIC) {
			// 返回数值类型的值
			return String.valueOf(hssfCell.getNumericCellValue());
		} else {
			// 返回字符串类型的值
			return String.valueOf(hssfCell.getStringCellValue());
		}
	}
	
	
	
	
	
	
	public static void exportPointExcel(String sheetTitles[],
			Object data,PointExportForm form, HttpServletResponse respons,HttpServletRequest request) throws IOException {
		setResponseHeader(respons, "云电微视");
		OutputStream out = respons.getOutputStream();
		
		// 创建excel
		HSSFWorkbook wb = new HSSFWorkbook();
		// 创建sheet
		/*HSSFSheet sheet = wb.createSheet(sheetTitle);
		HSSFSheet sheet2 = wb.createSheet(sheetTitle2);
		HSSFSheet sheet3 = wb.createSheet(sheetTitle3);
		HSSFSheet sheet4 = wb.createSheet(sheetTitle4);*/
		 HSSFSheet createSheet=null;
		for(int i=0;i<sheetTitles.length;i++){
			  createSheet = wb.createSheet(sheetTitles[i]);
			 for(int j=0;j<=5;j++){createSheet.setColumnWidth(i, 3766);} 
		}
		//母线工作薄
	//	for(int i=0;i<=9;i++){sheet.setColumnWidth(i, 3766);} //第一个参数代表列id(从0开始),第2个参数代表宽度值 
		//过电压保护器工作薄
		//for(int i=0;i<=7;i++){sheet2.setColumnWidth(i, 3766);} //第一个参数代表列id(从0开始),第2个参数代表宽度值 
		//温度采集器工作薄
	//	for(int i=0;i<=7;i++){sheet3.setColumnWidth(i, 3766);} //第一个参数代表列id(从0开始),第2个参数代表宽度值 
		 //报警列表工作薄
		//for(int i=0;i<=12;i++){sheet4.setColumnWidth(i, 3766);} //第一个参数代表列id(从0开始),第2个参数代表宽度值
		
		// 设置字体
        HSSFFont styleTitles = wb.createFont();
       // styleTitles.setFontHeightInPoints((short) 20); //字体高度
        styleTitles.setColor(HSSFFont.COLOR_NORMAL); //字体颜色
        styleTitles.setFontName("黑体"); //字体
      //  styleTitles.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD); //宽度
        styleTitles.setItalic(false); //是否使用斜体
		
		 // 设置单元格类型
        HSSFCellStyle cellStyle = wb.createCellStyle();
        cellStyle.setFont(styleTitles);
        cellStyle.setFillForegroundColor((short) 13);// 设置背景色
       // cellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER); //水平布局：居中
        cellStyle.setWrapText(true);
        try {
        	if(form.getProtectorType().equals(ProtectorType.guodianya.getName())){
        		getGuoDianYaBaoHuQi(data,createSheet,cellStyle);
        		
        	}else if(form.getProtectorType().equals(ProtectorType.wendu.getName())){
        		getWenDuCaiJiQi(data,createSheet,cellStyle);
        	}
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}finally{
			if(out!=null){
				wb.write(out);
				out.flush();
				out.close();
			}
		}
			
		
	}
	
	
	private static void getWenDuCaiJiQi(Object obj, HSSFSheet sheet, HSSFCellStyle styleTitle){
        /**
         * 	"temper": 25,
			"id": 312541,
	        "sensorId": "A101",
	        "timestamp": 1544078293973
         */
		String[] wenDuCaiJiQiTitles = { "监测点", "时间", "温度"};
		String[] wenDuCaiJiQiKeys = { "sensorId","timestamp", "temper"};
		                              
		List<Map<String, Object>> wenDuCaiJiQi = new ArrayList<>();
		@SuppressWarnings("unchecked")
		List<Temper_data> temperatureList = (List<Temper_data>)obj;
			/*if(temperatureList==null||temperatureList.isEmpty()){
				throw new BizException(Code.NO_DATAS);
			}*/
			temperatureList.forEach(m->{
				HashMap<String, Object> maps = new HashMap<>();
					String sensorId = m.getSensorId();
					long timestamp = m.getTimestamp();
					float temper = m.getTemper();
					maps.put("sensorId", sensorId);
					maps.put("timestamp", DateUtils.unixTimestampToDate(timestamp));
					maps.put("temper", temper);
					wenDuCaiJiQi.add(maps);
			});
		
		HSSFRow row;
		HSSFCell cell;
		// 创建标题栏
		row = sheet.createRow(0);
		// 标题栏
		for (int i = 0; i < wenDuCaiJiQiTitles.length; i++) {
			String alarmTitle = wenDuCaiJiQiTitles[i];
			// 在行上创建1列
			cell = row.createCell(i);
			cell.setCellValue(alarmTitle);
			cell.setCellStyle(styleTitle);
		}
		
		// 添加数据
		int index = 1;
		for (Map<String, Object> map : wenDuCaiJiQi) {
			row = sheet.createRow(index++);
			for (int i = 0; i < wenDuCaiJiQiKeys.length; i++) {
				String key = wenDuCaiJiQiKeys[i];
				for (Object o : map.keySet()) {
					if (o.toString().equals(key)) {
						// 在行上创建1列
						cell = row.createCell(i);
						if (map.get(key) == null) {
							cell.setCellValue(0);
						} else {
							cell.setCellValue(map.get(key).toString());
						}
						cell.setCellStyle(styleTitle);
					}
				}
			}		
				index++;
		}
}
	
	
	
	@SuppressWarnings("unchecked")
	private static void getGuoDianYaBaoHuQi(Object obj, HSSFSheet sheet, HSSFCellStyle styleTitle) throws InvocationTargetException, IllegalAccessException {
		String[] getGuoDianYaBaoHuQiTitles = { "监测点-时间", "相别", "温度", "湿度", "泄漏电流"};
		String[] erickeysA = { "timestamp","相别", "temper_a", "humidity_a", "electric"};
		String[] erickeysB = { "timestamp","相别", "temper_b", "humidity_b", "electric"};
		String[] erickeysC = { "timestamp","相别", "temper_c", "humidity_c", "electric"};
		/**
		 * 	"temper_c": 0.01,
			"temper_b": 0.01,
			"temper_a": 0.01,
			"electric": 85,
			"humidity_a": 1,
			"humidity_c": 1,
			"humidity_b": 1,
			"id": 177453,
			"battery": 2.55,
			"sensorId": "B1",
			"timestamp": 1545364942027
		 */
		List<Map<String, Object>> newMaxValueEric = new ArrayList<>();
		List<Voltage_data> voltageList = (List<Voltage_data>)obj;
			/*if(voltageList==null||voltageList.isEmpty()){
				throw new BizException(Code.NO_DATAS);
			}*/
			for(int i=0;i<voltageList.size();i++){
	
				Map<String, Object> groupMap = new HashMap<>();
				Map<String, Object> maxValueEricA = new HashMap<>();
				Map<String, Object> maxValueEricB = new HashMap<>();
				Map<String, Object> maxValueEricC = new HashMap<>();
				
				voltageList.get(i).getBattery();
				
				
				maxValueEricA.put("timestamp",voltageList.get(i).getSensorId()+"\n"+ DateUtils.unixTimestampToDate(voltageList.get(i).getTimestamp()));
				maxValueEricA.put("相别","A相");    
				maxValueEricA.put("temper_a", voltageList.get(i).getTemper_a());    
				maxValueEricA.put("humidity_a", voltageList.get(i).getHumidity_a());    
				maxValueEricA.put("electric", voltageList.get(i).getElectric());
				
	
				maxValueEricB.put("timestamp", voltageList.get(i).getSensorId()+"\n"+DateUtils.unixTimestampToDate(voltageList.get(i).getTimestamp()));
				maxValueEricB.put("相别","B相");    
				maxValueEricB.put("temper_b", voltageList.get(i).getTemper_b());    
				maxValueEricB.put("humidity_b", voltageList.get(i).getHumidity_b());    
				maxValueEricB.put("electric", voltageList.get(i).getElectric());
				
				maxValueEricC.put("timestamp",voltageList.get(i).getSensorId()+"\n"+ DateUtils.unixTimestampToDate(voltageList.get(i).getTimestamp()));
				maxValueEricC.put("相别","C相");    
				maxValueEricC.put("temper_c", voltageList.get(i).getTemper_c());    
				maxValueEricC.put("humidity_c", voltageList.get(i).getHumidity_c());    
				maxValueEricC.put("electric", voltageList.get(i).getElectric());
				
				groupMap.put("maxValueEricA",maxValueEricA);
				groupMap.put("maxValueEricB",maxValueEricB);
				groupMap.put("maxValueEricC",maxValueEricC);
				
				newMaxValueEric.add(groupMap);	
			}
	
		//母线工作薄设置列宽度
		for(int i=0;i<=getGuoDianYaBaoHuQiTitles.length;i++){sheet.setColumnWidth(i, 3766);} //第一个参数代表列id(从0开始),第2个参数代表宽度值 
		HSSFRow row;
		HSSFCell cell;
		// 创建标题栏
		row = sheet.createRow(0);
		// 标题栏
		for (int i = 0; i < getGuoDianYaBaoHuQiTitles.length; i++) {
			String ericTitle = getGuoDianYaBaoHuQiTitles[i];
			// 在行上创建1列
			cell = row.createCell(i);
			cell.setCellValue(ericTitle);
			cell.setCellStyle(styleTitle);
		}
		// 添加数据
		int index = 1;
		try {
		for (Map<String, Object> map : newMaxValueEric) {
		//	System.out.println("index:"+index);
			CellRangeAddress     cra = new CellRangeAddress(index, index+2, 0, 0); 
			CellRangeAddress	 cra1 = new CellRangeAddress(index, index+2, 4, 4);
		//	CellRangeAddress	 cra2 = new CellRangeAddress(index, index+2, 9, 9);
			
			// 在sheet里增加合并单元格
			sheet.addMergedRegion(cra);
			sheet.addMergedRegion(cra1);
		//	sheet.addMergedRegion(cra2);
			
			Map<String,Object> objectA = (Map<String,Object>)map.get("maxValueEricA");
			Map<String,Object> objectB = (Map<String,Object>)map.get("maxValueEricB");
			Map<String,Object> objectC = (Map<String,Object>)map.get("maxValueEricC");
			
			row = sheet.createRow(index++);
			for (int i = 0; i < erickeysA.length; i++) {
				String key = erickeysA[i];
				// 在行上创建1列
				cell = row.createCell(i);
				if (objectA.get(key) == null) {
					cell.setCellValue("-");
				} else {
					cell.setCellValue(objectA.get(key).toString());
				}
				cell.setCellStyle(styleTitle);
			}
			
			row = sheet.createRow(index++);
			for (int i = 0; i < erickeysB.length; i++) {
				String key = erickeysB[i];
				// 在行上创建1列
				cell = row.createCell(i);
				if (objectB.get(key) == null) {
					cell.setCellValue("-");
				} else {
					cell.setCellValue(objectB.get(key).toString());
				}
				cell.setCellStyle(styleTitle);
			}
			
			row = sheet.createRow(index++);
			for (int i = 0; i < erickeysC.length; i++) {
				String key = erickeysC[i];
				// 在行上创建1列
				cell = row.createCell(i);
				if (objectC.get(key) == null) {
					cell.setCellValue("-");
				} else {
					cell.setCellValue(objectC.get(key).toString());
				}
				cell.setCellStyle(styleTitle);
			}
			index++; //++ 操作使数据间隔一行
		}
	} catch (Exception e) {
		e.printStackTrace();
	}
}
	
	
	

	public static void main(String[] args) throws Exception {
		String[] titles = new String[] { "序号", "货物运输批次号", "提运单号", "状态", "录入人", "录入时间" };
		String[] keys = new String[] { "11", "22", "33", "44", "55", "66" };
		List<Map<String, String>> data = new ArrayList<Map<String, String>>();
		for (int i = 0; i < 20; i++) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("11", "s1111" + i);
			map.put("22", "11222" + i);
			map.put("33", "1111" + i);
			map.put("44", "11333" + i);
			map.put("55", "11444" + i);
			map.put("66", "2018010" + i);
			data.add(map);
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhss");
		String format = sdf.format(System.currentTimeMillis());
		// OutputStream out = new
		// FileOutputStream("C:\\Users\\Administrator\\Desktop\\app.xls");
		OutputStream out = new FileOutputStream("C:\\Users\\Administrator\\Desktop\\" + format + ".xls");
		// exportExcel(out, "测试", titles, keys, data);
		// exportExcel(out, sheetTitle, titles, keys, data);

		// InputStream in = new
		// FileInputStream("C:\\Users\\Administrator\\Desktop\\app.xls");
		// String[] fields = new
		// String[]{"hacIp","deviceIp","deviceName","remark"};
		// List<JSONObject> data = PoiUtil.inputExcel(in, 1, fields);
		// System.out.println(data);

		/*
		 * List<HacDeviceContrast> contrasts = new
		 * ArrayList<HacDeviceContrast>(); for(JSONObject json : data){
		 * contrasts.add((HacDeviceContrast) JSON.parseObject(json.toString(),
		 * HacDeviceContrast.class)); }
		 * 
		 * System.out.println(data);
		 */
	}
}
