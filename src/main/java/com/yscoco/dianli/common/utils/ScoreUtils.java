package com.yscoco.dianli.common.utils;

public class ScoreUtils {
	
	/**  
	 * 	
	 * 			>=180	血压偏高	5
				160~179	血压偏高	10
				140~159	血压偏高	15
				136~139	正常高值	20   136？
				121~130	正常血压	22
				111~120	理想血压	25
				90~110	正常偏低	10
	 */
	public static int getBlood(Integer bloodnum){
		int score = 0 ;
			if(bloodnum == null){
				
			}
			else if(bloodnum >= 180 ){
				score = 5;
			}
			else if(bloodnum >= 160&&bloodnum<=179){
				score = 10;
			}
			else if(bloodnum >= 140&&bloodnum<=159 ){
				score = 15;
			}
			else if(bloodnum >= 131&&bloodnum<=139 ){
				score = 25;
			}
			else if(bloodnum >= 121&&bloodnum<=130 ){
				score = 22;
			}
			else if(bloodnum >= 111&&bloodnum<=120 ){
				score = 25;
			}
			else if(bloodnum >= 90&&bloodnum<=110 ){
				score = 10;
			}

		return score;
	}
	
	
	/**  
<60	心率偏慢	5
61~70	理想心率	30
71~80	正常心率	28
81~100	正常心率	20
101~160	心率偏快	15
>160	心率超快	5

	 */
	public static int getHrv(Integer hrv){
		int score = 0 ;
		
		if(hrv == null){
			
		}
		else if(hrv<60){
			score = 5;
		}
		else if(hrv>=61 && hrv<=70){
			score = 30;
		}
		else if(hrv>=71 && hrv<=80){
			score = 28;
		}
		else if(hrv>=81 && hrv<=100){
			score = 20;
		}
		else if(hrv>=101 && hrv<=160){
			score = 15;
		}
		else if(hrv>=161){
			score = 5;
		}
		return score;
	}
	
	/**  
	>70	0.2
	50~70	0.4
	30~49	0.5
	21~29	0.8
	10~20	1

	 */
	public static double getDiffHrv(Integer diffHrv){
		double score = 0;
		if(diffHrv == null){
			
		}else if(diffHrv>70){
			score = 0.2;
		}
		else if(diffHrv>=50 && diffHrv<=70){
			score = 0.4;
		}
		else if(diffHrv>=30 && diffHrv<=49){
			score = 0.5;
		}
		else if(diffHrv>=21 && diffHrv<=29){
			score = 0.8;
		}
		else if(diffHrv>=10 && diffHrv<=20){
			score =1;
		}
		return score;
	}
	
	/**  
BMI指数完成率*20	

	 */
	public static int getRun(double run){
		return (int)(20*run);
	}
	
	/**  
总时长<8h & 深睡眠<2h	一星	5
总时长8~10h & 深睡眠2~2.5h	二星	15
总时长>10h & 深睡眠2.5h~3h	三星	20
总时长>8H& &深睡眠3h~4h	四星	22
总时长>8H& &深睡眠>4h	五星	25

	 */
	public static int getSleep(Integer  sleepTime , Integer deepTime){
		int score = 0 ;
		if(sleepTime == null || deepTime == null){
			
		}else if( sleepTime < 480 && deepTime < 120){
			score = 5;
		}
		else if(sleepTime > 480 && (deepTime >= 120 || deepTime<=150)){
			score = 15;
		}
		else if(sleepTime > 480 && (deepTime >= 150 || deepTime<=180)){
			score = 20;
		}
		else if(sleepTime > 480 && (deepTime >= 180 || deepTime<=240)){
			score = 22;
		}
		else if(sleepTime > 480 && deepTime >240){
			score = 25;
		}
		else if(sleepTime < 480 && deepTime > 120){
			score = 10;
		}
		else if(sleepTime > 480 && deepTime < 120){
			score = 12;
		}
		return score;
	}
}
