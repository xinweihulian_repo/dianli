package com.yscoco.dianli.common.utils;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

public class BeanUtils {

	
	public static String[] getNullPropertyNames (Object source) {
		final BeanWrapper src = new BeanWrapperImpl(source);
		java.beans.PropertyDescriptor[] pds = src.getPropertyDescriptors();

		Set<String> emptyNames = new HashSet<String>();
		for(java.beans.PropertyDescriptor pd : pds) {
			Object srcValue = src.getPropertyValue(pd.getName());
	            if (srcValue == null) emptyNames.add(pd.getName());
		}
        String[] result = new String[emptyNames.size()];
        return emptyNames.toArray(result);
	}
	
	public static void copyProperties(Object source ,Object target , String... ignoreProperties){
		org.springframework.beans.BeanUtils.copyProperties(source, target, ignoreProperties);
	}
	
	public static void copyProperties(Object source ,Object target ){
		org.springframework.beans.BeanUtils.copyProperties(source, target);
	}
	public static void copyProperties(Object source, Object target, Class<?> editable){
		org.springframework.beans.BeanUtils.copyProperties(source, target, editable);
	}
	public static void copyNotNullProperties(Object source, Object target){
		copyProperties(source, target, getNullPropertyNames(source));
	}
	
	/**
	 * Map --> Bean 2: 利用org.apache.commons.beanutils 工具类实现 Map --> Bean
	 *
	 * @param map
	 * @param obj
	 */
	public static void transMapToBean(Map<String, Object> map, Object obj) {
	    if (map == null || obj == null) {
	        return;
	    }
	    try {
			org.apache.commons.beanutils.BeanUtils.populate(obj, map);
		} catch (IllegalAccessException | InvocationTargetException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Bean --> Map 1: 利用Introspector和PropertyDescriptor 将Bean --> Map
	 *
	 * @param obj dong
	 */
	public static Map<String, Object> transBeanToMap(Object obj) throws IntrospectionException, InvocationTargetException, IllegalAccessException {
	    if (obj == null) {
	        return null;
	    }
	    Map<String, Object> map = new HashMap<String, Object>();
	    BeanInfo beanInfo = Introspector.getBeanInfo(obj.getClass());
	    PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
	    for (PropertyDescriptor property : propertyDescriptors) {
	        String key = property.getName();
	        // 过滤class属性
	        if (!key.equals("class")) {
	            // 得到property对应的getter方法
	            Method getter = property.getReadMethod();
	            Object value = getter.invoke(obj);

	            map.put(key, value);
	        }
	    }
	    return map;
	}
	
}
