package com.yscoco.dianli.common.utils;

/**
 * @Author: onion..
 * @Description: TODO
 * @Date: 2020/10/19 9:41
 */
public class RunTimeCount {

    public static long start(){
       return System.currentTimeMillis();
    }

    public static long end(){
        return System.currentTimeMillis();
    }

    public  static void  print(String detailDesc,long end , long start){
        System.out.println(detailDesc +" start time: " + start+ "; end time:" + end+ "; Run Time:" + (end - start) + "(ms)");
    }
}
