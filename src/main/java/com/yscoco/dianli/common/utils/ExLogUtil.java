package com.yscoco.dianli.common.utils;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.hyc.smart.log.MyLogFormatter;

/**
 * 默认将log文件输出到C:\Logs\server\xxxx年\x月\xxxx-xx-xx.log
 * 路径不存在的话会自动创建
 * 可通过修改getLogFilePath修改生成的log路径
 */
public class ExLogUtil {
	private static Calendar now = Calendar.getInstance();

    private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd");

    private static final int YEAR = now.get(Calendar.YEAR);

    private static final int MONTH = now.get(Calendar.MONTH) + 1;

    private static final String LOG_FOLDER_NAME = "server";

    private static final String LOG_FILE_SUFFIX = ".log";

    public static Logger logger;

   /**
    * 使用唯一的fileHandler，保证当天的所有日志写在同一个文件里
    */
    private static FileHandler fileHandler = getFileHandler();

    private static MyLogFormatter myLogFormatter = new MyLogFormatter();
    private static Object object = new Object();
	public static Logger getLogger() {
		if (logger == null) {
			synchronized (object) {
				if (logger == null) {
					logger = setLoggerHanlder(Level.INFO);
				}
			}
		}
		return logger;
	}

    private synchronized static String getLogFilePath() {
        StringBuffer logFilePath = new StringBuffer();
//        logFilePath.append(System.getProperty("user.home"));
        logFilePath.append("C:\\Logs");
        logFilePath.append(File.separatorChar);
        logFilePath.append(LOG_FOLDER_NAME);
        logFilePath.append(File.separatorChar);
        logFilePath.append(YEAR);
        logFilePath.append(File.separatorChar);
        logFilePath.append(MONTH);

        File dir = new File(logFilePath.toString());
        if (!dir.exists()) {
            dir.mkdirs();
        }

        logFilePath.append(File.separatorChar);
        logFilePath.append(SDF.format(new Date()));
        logFilePath.append(LOG_FILE_SUFFIX);

//        System.out.println(logFilePath.toString());
        return logFilePath.toString();
    }

    private static FileHandler getFileHandler() {
        FileHandler fileHandler = null;
        boolean appendMode = true;
        try {
            //文件日志内容标记为可追加
            fileHandler = new FileHandler(getLogFilePath(), appendMode);
            return fileHandler;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public synchronized static Logger setLoggerHanlder() {
        return setLoggerHanlder(Level.ALL);
    }

    //    SEVERE > WARNING > INFO > CONFIG > FINE > FINER > FINESET
    public synchronized static Logger setLoggerHanlder(Level level) {

        try {
            //以文本的形式输出
//            fileHandler.setFormatter(new SimpleFormatter());
        	logger = Logger.getLogger("MyLogger");
            fileHandler.setFormatter(myLogFormatter);

            logger.addHandler(fileHandler);
            logger.setLevel(level);
        } catch (SecurityException e) {
            logger.severe(populateExceptionStackTrace(e));
        }
        return logger;
    }

    private synchronized static String populateExceptionStackTrace(Exception e) {
        StringBuilder sb = new StringBuilder();
        sb.append(e.toString()).append("\n");
        for (StackTraceElement elem : e.getStackTrace()) {
            sb.append("\tat ").append(elem).append("\n");
        }
        return sb.toString();
    }

//    public static void main(String [] args) {
//        Logger logger = ExLogUtil.setLoggerHanlder(Level.INFO);
//        logger.info("Hello, world!");
//        logger.severe("What are you doing?");
//        logger.warning("Warning !");
//
//        for(Handler h : logger.getHandlers()) {
//            h.close();   //must call h.close or a .LCK file will remain.
//        }
//    }
}
