package com.yscoco.dianli.common.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author lanston
 *
 */
public class Log{
	
	private static Logger common = LoggerFactory.getLogger("common");
	private static Logger biz =  LoggerFactory.getLogger("biz");
	private static Logger cache = LoggerFactory.getLogger("cache");
	
	public static Logger getCommon() {
		return common;
	}
	public static Logger getBiz() {
		return biz;
	}
	public static Logger getCache() {
		return cache;
	}
	
}
