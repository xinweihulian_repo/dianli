package com.yscoco.dianli.common.handler;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.yscoco.dianli.common.exception.BizException;
import com.yscoco.dianli.common.utils.Log;
import com.yscoco.dianli.common.vo.Code;
import com.yscoco.dianli.common.vo.Message;

public class ExceptionHandler implements HandlerExceptionResolver {
	Logger log = LoggerFactory.getLogger(ExceptionHandler.class);
	@Override
	public ModelAndView resolveException(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex) {
		if(ex instanceof BizException){
			BizException be = (BizException)ex;
			pringJson(new Message<>(be.getCode().getCode(),be.getMsg()), response);
		}else{
			//Log.getCommon().error("失败",ex.getMessage());
			Log.getCommon().error(JSON.toJSONString(Message.newError(ex.getMessage())));
            pringJson(Message.newError(ex.getMessage()), response);
		}
		return null;
	}
	
	public void pringJson(Message<?> msg ,HttpServletResponse response){
		PrintWriter writer = null;
		try {
			writer = response.getWriter();
			writer.write(JSONObject.toJSONString(msg));
		} catch (IOException e) {
			log.error("输出json错误",e);
		}finally{
			if(writer!=null){
				writer.flush();
				writer.close();
			}
		}
	}

}
