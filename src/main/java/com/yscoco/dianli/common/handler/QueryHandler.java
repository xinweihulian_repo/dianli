package com.yscoco.dianli.common.handler;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.hibernate.Query;
import org.hibernate.Session;

import com.yscoco.dianli.common.base.Base;



public class QueryHandler extends Base {
    public static final String COUNT_SQL = "select count(*) ";
    public static final String KEYWORD_FROM = " from ";
    public static final String KEYWORD_ORDER = " order by ";
    public static final String KEYWORD_GROUP = " group by ";
    boolean whereFlag = true;
    boolean orderFlag = true;
    boolean groupFlag = true;
    private StringBuilder sqlBuilder;
    private Map<String, Object> map;
    private Map<String, Object[]> arrayMap;
    private Integer firstResult;
    private Integer maxResults;
    private Boolean cacheable;

    public QueryHandler(String sql) {
        this.sqlBuilder = new StringBuilder(" ");
        sqlBuilder.append(sql);
    }
   
    public QueryHandler() {
        this.sqlBuilder = new StringBuilder();
    }

    public Query getQuery(Session session) {
        return getQuery(session, getSql());
    }
    public Query getSqlQuery(Session session) {
    	return getSqlQuery(session, getSql());
    }

    public Query getCountQuery(Session session) {
        return getQuery(session, getCountSql());
    }
  
    private static String getColunm(String str,String endStr){
        Pattern pattern = Pattern.compile(endStr+"$");
        Matcher matcher = pattern.matcher(str);
        String s = matcher.replaceAll("");
        if(!s.contains(".")){
			s = "t."+s;
		}
        return s;
    }
   
    
    
    public  String like(String var) {
        return "%" + var + "%";
    }

    public  String likeEnd(String var) {
        return "%" + var;
    }

    public  String likeStart(String var) {
        return var + "%";
    }
    public  QueryHandler conditionAndSetPage(Map<String,Object> params){
    	if(empty(params) || params.size() ==0) {
    		return this;
    	}
    	if(params.get("page") != null && params.get("rows")!=null) {
	    	int page = (Integer)params.get("page");
	    	int rows = (Integer)params.get("rows");
	    	setFirstResult((page-1) * rows);
	    	setMaxResults(rows);
    	}
    	condition(params);
    	return this;
    }
    
   
    public  QueryHandler condition(Map<String,Object> params){
    	if(empty(params) || params.size() ==0) {return this;}
    	
    	
		String colunm = null;
		for (String key : params.keySet()) {
			if(empty(params.get(key))
					|| key.equals("rows") 
					|| key.equals("page")
					|| key.equals("sort")
					|| key.equals("order")
					){
				continue;
			} 
			
			if (whereFlag) {
	            whereFlag = false;
	            sqlBuilder.append(" where ");
	        } else {
	            sqlBuilder.append(" and ");
	        }
			if(key.endsWith("Gte")){
				colunm = getColunm(key, "Gte");
				sqlBuilder.append(colunm).append(" >= :").append(key);
				setParameter(key, params.get(key));
			}
			else if(key.endsWith("Gt")){
				colunm = getColunm(key, "Gt");
				sqlBuilder.append(colunm).append(" > :").append(key);
				setParameter(key, params.get(key));
			}
			else if(key.endsWith("Lte")){
				colunm = getColunm(key, "Lte");
				sqlBuilder.append(colunm).append(" <= :").append(key);
				setParameter(key, params.get(key));
			}
			else if(key.endsWith("Lt")){
				colunm = getColunm(key, "Lt");
				sqlBuilder.append(colunm).append(" < :").append(key);
				setParameter(key, params.get(key));
			}
			else if(key.endsWith("Start")){
				colunm = getColunm(key, "Start");
				sqlBuilder.append(colunm).append(" like :").append(key);
				setParameter(key, likeStart(String.valueOf(params.get(key))));
			}
			else if(key.endsWith("End")){
				colunm = getColunm(key, "End");
				sqlBuilder.append(colunm).append(" like :").append(key);
				setParameter(key, likeEnd(String.valueOf(params.get(key))));
			}
			else if(key.endsWith("Like")){
				colunm = getColunm(key, "Like");
				sqlBuilder.append(colunm).append(" like :").append(key);
				setParameter(key, like(String.valueOf(params.get(key))));
			}
			else if(key.endsWith("In")){
				colunm = getColunm(key, "In");
				sqlBuilder.append(colunm).append(" in (:").append(key).append(")");
				if(params.get(key) instanceof String){
                    setParameter(key, String.valueOf(params.get(key)).split(","));
                }else{
                    setParameter(key, (Object[])params.get(key));
                }
			}
			else if(key.endsWith("Between")){
				colunm = getColunm(key, "Between");
				sqlBuilder.append(colunm).append(" between :").append(key+"1 and :").append(key+"2");
				String[] vs = String.valueOf(params.get(key)).split(",");
				setParameter(key+"1", vs[0]);
				setParameter(key+"2", vs[1]);
			}
			else{
				colunm = getColunm(key, "");;
				sqlBuilder.append(colunm).append(" = :").append(key.replace(".", ""));
				setParameter(key.replace(".", ""), params.get(key));
			}
    	}
    	return this;
    }
    
    
    public static void main(String[] args) {
		System.out.println("t.t.t".replace(".", ""));
	}
    
    
    public QueryHandler condition(String condition) {
        if (whereFlag) {
            whereFlag = false;
            sqlBuilder.append(" where ");
        } else {
            sqlBuilder.append(" and ");
        }
        sqlBuilder.append(condition);
        return this;
    }

    public QueryHandler order(Map<String,Object> params) {
    	if(params==null || !params.containsKey("order")) return this;
        String order = String.valueOf(params.get("order"));
    	if (orderFlag) {
            orderFlag = false;
            append(KEYWORD_ORDER);
        } else {
            sqlBuilder.append(',');
        }
        sqlBuilder.append(order);
        return this;
    }

    public QueryHandler group(String sqlString) {
        if (groupFlag) {
            groupFlag = false;
            sqlBuilder.append(KEYWORD_GROUP);
        } else {
            sqlBuilder.append(',');
        }
        sqlBuilder.append(sqlString);
        return this;
    }

    public QueryHandler append(String sqlString) {
        sqlBuilder.append(" ");
        sqlBuilder.append(sqlString);
        return this;
    }

    public QueryHandler setFirstResult(int firstResult) {
        this.firstResult = firstResult;
        return this;
    }

    public QueryHandler setMaxResults(int maxResults) {
        this.maxResults = maxResults;
        return this;
    }

    public QueryHandler setCacheable(boolean cacheable) {
        this.cacheable = cacheable;
        return this;
    }

    public QueryHandler setParameter(String key, Object value) {
        if (empty(map)) {
            map = new HashMap<String, Object>();
        }
        map.put(key, value);
        return this;
    }

    public QueryHandler setParameter(String key, Object[] value) {
        if (empty(arrayMap)) {
            arrayMap = new HashMap<String, Object[]>();
        }
        arrayMap.put(key, value);
        return this;
    }

    private Query getQuery(Session session, String sql) {
        Query query = session.createQuery(sql);
        if (notEmpty(map)) {
            for (String key : map.keySet()) {
                query.setParameter(key, map.get(key));
            }
        }
        if (notEmpty(arrayMap)) {
            for (String key : arrayMap.keySet()) {
                query.setParameterList(key, arrayMap.get(key));
            }
        }
        if (notEmpty(firstResult)) {
            query.setFirstResult(firstResult);
        }
        if (notEmpty(maxResults)) {
            query.setMaxResults(maxResults);
        }
        if (notEmpty(cacheable)) {
            query.setCacheable(cacheable);
        }
        return query;
    }
    
    private Query getSqlQuery(Session session, String sql) {
        Query query = session.createSQLQuery(sql);
        if (notEmpty(map)) {
            for (String key : map.keySet()) {
                query.setParameter(key, map.get(key));
            }
        }
        if (notEmpty(arrayMap)) {
            for (String key : arrayMap.keySet()) {
                query.setParameterList(key, arrayMap.get(key));
            }
        }
        if (notEmpty(firstResult)) {
            query.setFirstResult(firstResult);
        }
        if (notEmpty(maxResults)) {
            query.setMaxResults(maxResults);
        }
        if (notEmpty(cacheable)) {
            query.setCacheable(cacheable);
        }
        return query;
    }

    private String getSql() {
        return sqlBuilder.toString();
    }

    private String getCountSql() {
        String sql = getSql();
        sql = sql.substring(sql.toLowerCase().indexOf(KEYWORD_FROM));
        int groupIndex = sql.toLowerCase().indexOf(KEYWORD_GROUP);
        int orderIndex = sql.toLowerCase().indexOf(KEYWORD_ORDER);
        if (-1 != groupIndex) {
            sql = sql.substring(0, groupIndex);
        }
        if (-1 != orderIndex) {
            sql = sql.substring(0, orderIndex);
        }
        return COUNT_SQL + sql;
    }
}
