package com.yscoco.dianli.apidoc;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.UiConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
 				// http://blog.csdn.net/phantomes/article/details/52334591
@Configuration  // 注解是spring框架中本身就有的，它是一个被@Component元注解标识的注解，所以有了这个注解后，spring会自动把这个类实例化成一个bean注册到spring上下文中
@EnableWebMvc  // 启用srpingmvc了，在Eclipse中点到这个注解里面简单看一下，它就是通过元注解@Import(DelegatingWebMvcConfiguration.class)往spring context中塞入了一个DelegatingWebMvcConfiguration类型的bean
@EnableSwagger2 // 使swagger2生效 :集成swagger 2的，他通过元注解：@Import({Swagger2DocumentationConfiguration.class})，又引入了一个Swagger2DocumentationConfiguration类型的配置bean，而这个就是Swagger的核心配置
public class ApiDocConfig {  
	
	@Bean
	    public Docket createRestApi() {
	        return new Docket(DocumentationType.SWAGGER_2)
	                .apiInfo(apiInfo())
	                .select()
	                .apis(RequestHandlerSelectors.basePackage("com.yscoco.dianli.controller"))
	                .paths(PathSelectors.any())
	                .build();
	    }

	    private ApiInfo apiInfo() {
	        return new ApiInfoBuilder()
	                .title("接口文档")
	                .description("接口文档")
	                .termsOfServiceUrl("").contact(new Contact("凌想", "", "382541649@qq.com"))
	                .version("1.0")
	                .build();
	    }
	    
	    @Bean
	    public UiConfiguration getUiConfig() {
	        return new UiConfiguration(
	                null,// url,暂不用
	                "none",       // docExpansion          => none | list
	                "alpha",      // apiSorter             => alpha
	                "schema",     // defaultModelRendering => schema
	                false,        // enableJsonEditor      => true | false
	                true);        // showRequestHeaders    => true | false
	    }

}  