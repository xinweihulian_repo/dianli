package com.sh;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.hyc.smart.XWDataEntry;
import com.hyc.smart.bean.ItemFaultData;
import com.hyc.smart.packet.FaultMsg;

public class XwEntry {


	public static void main(String[] args) {
		getEricDataForIds();
	}
	
	public static void getEricDataForIds(){
		List<String> devids = new ArrayList<String>();		
		devids.add("10001");
		
		// 1.获取母线线路详情:devids：母线的id集合
		List<Map<String, Object>> map = XWDataEntry.getEricDataForIds(devids);	
		System.out.println("获取母线线路详情"+map.toString());
		//System.out.println("data: " + map.get(0).get("voltage_a"));
		
		// 2.获取某条母线的电流数据 :devid：       母线的id；timeStart：开始时间，时间戳 timeEnd：结束时间，时间戳；
		List<Map<String, Object>> ericElectricMap = XWDataEntry.getEricElectric("00000000000000002017",1523104302284l,1523104362284l);	
		System.out.println("获取母线线路详情"+ericElectricMap.toString());
		
		
		// 3. 根据id查询母线四相电压信息  devid：母线的id；timeStart：开始时间，时间戳；timeEnd：结束时间，时间戳；
		List<Map<String, Object>> ericVoltageMap = XWDataEntry.getEricVoltage("00000000000000002017",152310430200l,1523104362284l);	
		System.out.println("获取母线线路详情"+ericVoltageMap.toString());
		
		// 4. 获取支线保护器的动作总次数 sensorids：采集器的id集合；
		devids.clear();
		devids.add("10001");
		List<Map<String, Object>> actionCount = XWDataEntry.getActionCount(devids);	
		System.out.println("获取母线线路详情"+actionCount.toString());
		
		// 5、获取过电压保护器的温度波形  sensorId：采集器的id；timeStart：开始时间，时间戳；timeEnd：结束时间，时间戳；
		List<Map<String, Object>> temperatureForVoltage = XWDataEntry.getTemperatureForVoltage("00000000000000002017",15231043000l,1523104362284l);	
		System.out.println("获取母线线路详情"+temperatureForVoltage.toString());
		
		// 5、获取过电压保护器的湿度波形  sensorId：采集器的id；timeStart：开始时间，时间戳；timeEnd：结束时间，时间戳；
		List<Map<String, Object>> humidityForVoltage = XWDataEntry.gethumidityForVoltage("00000000000000002017",1523104302284l,1523104362284l);	
		System.out.println("获取过电压保护器的湿度波形"+humidityForVoltage.toString());
		
		// 5、获取保护器的所有相位动作次数  sensorId：采集器的id；timeStart：开始时间，时间戳；timeEnd：结束时间，时间戳；
		List<Map<String, Object>> acionCountForValue = XWDataEntry.getAcionCountForValue("00000000000000002017",1523104302284l,1523104362284l);	
		System.out.println("获取保护器的所有相位动作次数"+acionCountForValue.toString());
		
		// 5、查询保护器某个相别的动作时长波形  sensorId：采集器的id；timeStart：开始时间，时间戳；timeEnd：结束时间，时间戳；
		List<Map<String, Object>> acionTimesForValue = XWDataEntry.getAcionTimesForValue("00000000000000002017",1,1523104302284l,1523104362284l);	
		System.out.println("查询保护器某个相别的动作时长波形"+acionTimesForValue.toString());
		
		// 5、查询保护器泄露电流波形  sensorId：采集器的id；timeStart：开始时间，时间戳；timeEnd：结束时间，时间戳；
		List<Map<String, Object>> LeakElecForId = XWDataEntry.getLeakElecForId("00000000000000002017",1523104302284l,1523104302284l);	
		System.out.println("查询保护器某个相别的动作时长波形"+LeakElecForId.toString());
		
		// 5、查询母线过电压故障的波形  devid：母线对应的设备id ；time：故障发生的时间
		ItemFaultData leakElecForId = XWDataEntry.getFaultOverVoltage("00000000000000002017",1524675600000l);	
		System.out.println("查询母线过电压故障的波形："+leakElecForId);
		
		// 5、查询母线接地故障的波形  devid：母线对应的设备id ；time：故障发生的时间
		ItemFaultData itemFaultData = XWDataEntry.getFaultGround("00000000000000002017",1523104302284l);	
		System.out.println("查询保护器某个相别的动作时长波形"+itemFaultData.toString());
		
		// 5、查询母线接地故障的波形  devid：母线对应的设备id ；time：故障发生的时间
//		List<FaultMsg> faultMessageList = XWDataEntry.getFaultMessage(devids,true);	
//		System.out.println("查询保护器某个相别的动作时长波形"+faultMessageList);
				
	}
	
	
	
}
