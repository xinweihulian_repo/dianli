package com.sh;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;

import com.yscoco.dianli.common.utils.DateUtils;
import com.yscoco.dianli.other.cache.RedisCacheUtil;

public class AA {
	public static void main(String[] args) throws Exception {
		
		long timeStart_ = Long.parseLong("1523104362284");
		System.out.println(timeStart_);
		System.out.println(System.currentTimeMillis());
		
		String a1 = "123";
		double mul = mul("110");
		System.out.println("mul:"+mul);
		
		
		long timestamp = DateUtils.dateToUnixTimestamp();
		System.out.println(timestamp);
		
		String startTime = DateUtils.unixTimestampToDate(-28800000l);//2018-04-26 09:40:53
		String endTime = DateUtils.unixTimestampToDate(1524143300898l);//2018-04-26 09:40:53
		 
		System.out.println("开始时间"+startTime);
		System.out.println("结束时间"+endTime);
		
		System.out.println("2018-04-26 01:00:00".equalsIgnoreCase("2018-04-26 01:00:00"));
		System.out.println("2018-04-26 23:00:00".equalsIgnoreCase("2018-04-26 23:00:00"));
		
		long start = DateUtils.dateToUnixTimestamp("1970-01-01 00:00:00:000","yyyy-MM-dd HH:mm:ss:sss");
		long end =   DateUtils.dateToUnixTimestamp("2018-04-26 01:00:00:000","yyyy-MM-dd HH:mm:ss:sss");
		System.out.println("start："+start);
		System.out.println("end："+end);
		System.out.println("dateToStamp："+dateToStamp("1970-01-01 00:00:00"));
		
		dateToStamp2("2018-04-27 24:00:00:000");
		
		Date epoch = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2018-04-27 24:00:00");
		System.out.println(epoch.getTime());
		
	}
	
	 public static String dateToStamp(String s) throws Exception{
	        String res;
	        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	        Date date = simpleDateFormat.parse(s);
	        long ts = date.getTime();
	        res = String.valueOf(ts);
	        return res;
	    }
	 
	 public static void dateToStamp2(String s) throws Exception{
		 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
	     Date currentTime=new Date(); 
	     //将截取到的时间字符串转化为时间格式的字符串 
	     Date beginTime=sdf.parse("1970-01-01 00:00:00"); 
	     //默认为毫秒 
	     long interval=(currentTime.getTime()-beginTime.getTime());
	     System.out.println(currentTime.getTime());
	     System.out.println(beginTime.getTime());
	     System.out.println(interval);
	  }
	 
	 public static  double mul(String v1){
	        BigDecimal b1 = new BigDecimal(v1);
	        BigDecimal b2 = new BigDecimal(10);
	        BigDecimal b3 = new BigDecimal(50);
	        return (b1.subtract(b3)).divide(b2,3,BigDecimal.ROUND_HALF_UP).doubleValue();
	    }
	 
	
	 
}
